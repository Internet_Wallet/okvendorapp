//
//  AppDelegate.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 26/06/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import Reachability
import CoreData
import CoreTelephony
import UserNotifications
import SwiftyJSON
//import MobileRTC
import Firebase
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

        var window: UIWindow?
        var scheme: String!
        var currentLanguage = ""
        var currentFont = ""
        var localBundle: Bundle?
        let gcmMessageIDKey = "gcm.message_id"
        var FCMToken = ""

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        self.setInitialCurrentLang()
               //self.notifySimChange()
               self.getCurrentSimChangeStatus()
               _ = TownshipManager.shared
               _ = VMGeoLocationManager.shared
               
//               FirebaseApp.configure()
//               Messaging.messaging().delegate = self
               
               if #available(iOS 10.0, *) {
                   // For iOS 10 display notification (sent via APNS)
                   UNUserNotificationCenter.current().delegate = self
                   
                   let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                   UNUserNotificationCenter.current().requestAuthorization(
                       options: authOptions,
                       completionHandler: {_, _ in })
                   
               } else {
                   let settings: UIUserNotificationSettings =
                       UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                   application.registerUserNotificationSettings(settings)
               }
               
               application.registerForRemoteNotifications()
        return true
    }
    
    func setInitialCurrentLang() {
      /*
        UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
        UserDefaults.standard.set("en", forKey: "currentLanguage")
        self.currentFont = (UserDefaults.standard.object(forKey: "appFont") as? String)!
        appFont = self.currentFont
        appLang = "English"
        if let path = Bundle.main.path(forResource: "en", ofType: "lproj") {
            localBundle = Bundle(path: path)
        }
        */
        //*
        if UserDefaults.standard.value(forKey: "currentLanguage") == nil {
            currentLanguage = "uni"
            appLang = "Unicode"
            UserDefaults.standard.set("uni", forKey: "currentLanguage")
            UserDefaults.standard.set("Myanmar3", forKey: "appFont")
            UserDefaults.standard.synchronize()

            self.currentFont = (UserDefaults.standard.object(forKey: "appFont") as? String)!
            appFont = self.currentFont
            if let path = Bundle.main.path(forResource: "uni", ofType: "lproj") {
                localBundle = Bundle(path: path)
            }
        }else if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), lang == "en"{
            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
            UserDefaults.standard.set("en", forKey: "currentLanguage")
            self.currentFont = (UserDefaults.standard.object(forKey: "appFont") as? String)!
            appFont = self.currentFont
            appLang = "English"
            if let path = Bundle.main.path(forResource: "en", ofType: "lproj") {
                localBundle = Bundle(path: path)
            }
        }else {
            if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), let path =  Bundle.main.path(forResource: lang, ofType: "lproj") {
                currentLanguage = lang
                UserDefaults.standard.set("uni", forKey: "currentLanguage")
                UserDefaults.standard.set("Myanmar3", forKey: "appFont")
                // UserDefaults.standard.synchronize()
                self.currentFont = (UserDefaults.standard.object(forKey: "appFont") as? String)!
                appFont = self.currentFont
                appLang = "Unicode"
                localBundle = Bundle(path: path)
                if let path = Bundle.main.path(forResource: "uni", ofType: "lproj") {
                    localBundle = Bundle(path: path)
                }
            }
        }
 //*/
    }
    
    func setSeletedlocaLizationLanguage(language : String) {
        currentLanguage = language
        if let path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj") {
            localBundle = Bundle(path: path)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "stringLocalised"), object: nil)
        }
        if currentLanguage == "en"{
            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
            // UserDefaults.standard.synchronize()
            self.currentFont = (UserDefaults.standard.object(forKey: "appFont") as? String)!
            appFont = self.currentFont
            appLang = "English"
        }else {
            UserDefaults.standard.set("Myanmar3", forKey: "appFont")
            self.currentFont = (UserDefaults.standard.object(forKey: "appFont") as? String)!
            appFont = self.currentFont
            appLang = "Unicode"
        }
    }
    
    func getStringAccordingToLanguage(language : String, key: String) -> String {
        if let path = Bundle.main.path(forResource: language, ofType: "lproj") {
            let bundle  = Bundle(path: path)
            return NSLocalizedString(key, tableName: "Localization", bundle: bundle!, value: "", comment: "")
        }
        return key
    }
    
    func getSelectedLanguage() -> String {
        return currentLanguage
    }
    
    func getlocaLizationLanguage(key : String) -> String {
        if let bundl = localBundle {
            return NSLocalizedString(key, tableName: "Localization", bundle: bundl, value: "", comment: "")
        } else {
            return key
        }
    }
    
    func getNetworkCode() -> (String, String) {
        let networkInfo = CTTelephonyNetworkInfo.init()
        let carrier     = networkInfo.subscriberCellularProvider
        let networkCode = carrier?.mobileNetworkCode ?? ""
        let carrierName = carrier?.carrierName ?? ""
        return (networkCode, carrierName)
    }

    
    //MARK: CHECK SIM IN PHONE
       
//       func notifySimChange() {
//           networkInfo.subscriberCellularProviderDidUpdateNotifier = { carrier in
//               self.getCurrentSimChangeStatus()
//           }
//       }
//
//       func onMobileRTCAuthReturn(_ returnValue: MobileRTCAuthError) {
//           print("onMobileRTCAuthReturn \(returnValue)")
//           if (returnValue != MobileRTCAuthError_Success) {
//               let errorMessage = "SDK authentication failed, error code: \(returnValue)"
//               print(errorMessage)
//           }
//       }
       func getCurrentSimChangeStatus() {
           let (currentNetworkCode, currentCarrierName) = getNetworkCode()
           let device = UIDevice()
           if device.screenType == .iPhone_XSMax {
               if currentNetworkCode == "" && currentCarrierName == "" {
                   deleteAllObjects()
                   simChangeNotification()
                   return
               }
           }else {
               if currentNetworkCode == "" {
                   deleteAllObjects()
                   simChangeNotification()
                   return
               }
           }
           let previousNetWorkCode = UserDefaults.standard.value(forKey: "MobileNetworkCode") as? String ?? ""
           let previousCarrierName = UserDefaults.standard.value(forKey: "MobileCarrierName") as? String ?? ""
           if currentCarrierName != "" && currentNetworkCode != "" && previousNetWorkCode != "" && previousCarrierName != "" {
               if currentNetworkCode != previousNetWorkCode && currentCarrierName != previousCarrierName
               {
                   deleteAllObjects()
                   simChangeNotification()
                   UIApplication.shared.applicationIconBadgeNumber = 0
                   UserDefaults.standard.set(0, forKey: "ReceivedCount")
               }
           }
       }
       
       func deleteAllObjects(){
           UserDefaults.standard.set(false, forKey: "REGITRATION_OPENED")
           let entitesByName = self.persistentContainer.managedObjectModel.entitiesByName
           for (name, _) in entitesByName {
               deleteAllObjectsForEntity(entity: name)
           }
       }
       
       func deleteAllObjectsForEntity(entity: String){
           let managedContext  =  self.persistentContainer.viewContext
           let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
           let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchReq)
           do {
               try managedContext.execute(deleteRequest)
               try managedContext.save()
           } catch {
           }
       }
       
       func simChangeNotification() {
           DispatchQueue.main.async {
               UserDefaults.standard.set(false, forKey: "LoginStatus")
               let content = UNMutableNotificationContent()
               content.title = NSString.localizedUserNotificationString(forKey: "Notification", arguments: nil)
               content.body = NSString.localizedUserNotificationString(forKey: "You have changed the sim card in your device or ejected".localized, arguments: nil)
               content.sound = UNNotificationSound.default
               content.categoryIdentifier = "simChangeaAtionCategory"
               let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 0.5, repeats: false)
               let request = UNNotificationRequest.init(identifier: "notify-test", content: content, trigger: trigger)
               let center = UNUserNotificationCenter.current()
               center.add(request)
           }
       }
       
       
       func checkSIMAvailability() {
           let device = UIDevice()
           let (currentNetworkCode, currentCarrierName) = getNetworkCode()
           if device.screenType == .iPhone_XSMax {
               DispatchQueue.main.asyncAfter(deadline: .now() + 5.0 ) {
                   if currentNetworkCode == "" && currentCarrierName == ""{
                       UIView.animate(withDuration: 1.0, animations: {
                           DispatchQueue.main.async {
                               UserDefaults.standard.set(false, forKey: "LoginStatus")
                           }
                       }, completion: { (status) in
                           self.deleteAllObjects()
                           self.simChangeNotification()
                       })
                   }
               }
           } else {
               DispatchQueue.main.asyncAfter(deadline: .now() + 5.0 ) {
                   if currentNetworkCode == "" {
                       UIView.animate(withDuration: 1.0, animations: {
                           DispatchQueue.main.async {
                               UserDefaults.standard.set(false, forKey: "LoginStatus")
                           }
                       }, completion: { (status) in
                           self.deleteAllObjects()
                           self.simChangeNotification()
                       })
                   }
               }
           }
       }
    
    private func getStateTownship(statecode: String, townshipCode : String)-> (String,String,String) {
        var stateName = ""
        var townshipName = ""
        var cityName = ""
        let states = TownshipManager.allLocationsList
        for state in states {
            if state.stateOrDivitionCode == statecode {
                stateName = state.stateOrDivitionNameEn
                let township = state.townshipArryForAddress
                for town in township {
                    if town.townShipCode == townshipCode {
                        townshipName = town.townShipNameEN
                        
                        if town.GroupName == "" {
                            cityName = town.cityNameEN
                        }else {
                            if town.isDefaultCity == "1" {
                                cityName = town.DefaultCityNameEN
                            }else {
                                cityName = town.cityNameEN
                            }
                        }
                        break
                    }
                }
            }
        }
        return (stateName,townshipName,cityName)
    }
    
    
    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Core Data stack
    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
       
        let container = NSPersistentContainer(name: "OSKVendor")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    
    private func registerForRemoteNotifications() {
           let center = UNUserNotificationCenter.current()
           center.requestAuthorization(options: [.sound, .alert, .badge], completionHandler: { granted, error in
               if let error = error {
                   debugPrint("[AppDelegate] requestAuthorization error: \(error.localizedDescription)")
                   return
               }
               center.getNotificationSettings(completionHandler: { settings in
                   if settings.authorizationStatus != .authorized {
                       return
                   }
                   DispatchQueue.main.async(execute: {
                       UIApplication.shared.registerForRemoteNotifications()
                   })
               })
           })
       }
      
      // MARK: - Core Data Saving support
      func saveContext () {
          let context = persistentContainer.viewContext
          if context.hasChanges {
              do {
                  try context.save()
              } catch {
                  let nserror = error as NSError
                  fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
              }
          }
      }
}


extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        println_debug("=======> Did receive")
        if let aps = response.notification.request.content.userInfo["aps"] as? [String: Any] {
            println_debug("=======>\(aps)")
        }
        
        // QuickBlox
        let userInfo = response.notification.request.content.userInfo
        if UIApplication.shared.applicationState == .active {
            return
        }
        
        center.removeAllDeliveredNotifications()
        center.removeAllPendingNotificationRequests()
        
        guard let dialogID = userInfo["SA_STR_PUSH_NOTIFICATION_DIALOG_ID".localized] as? String,
            dialogID.isEmpty == false else {
                return
        }
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        println_debug("=======> willPresent")
        if let aps = notification.request.content.userInfo["aps"] as? [String: Any] {
            // self.notificationHandle(userInfo: aps)
            println_debug("=======>\(aps)")
            // self.checkCICONotificationCases(userInfo: aps, actionIdentifier: nil)
        }
        completionHandler([.alert, .badge, .sound])
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        var deviceTokenStr = ""
        print("%@",deviceTokenStr)
        let tokenData = deviceToken as NSData
        var token1 = tokenData.description
        
        // remove any characters once you have token string if needed
        token1 = token1.replacingOccurrences(of: " ", with: "")
        token1 = token1.replacingOccurrences(of: "<", with: "")
        token1 = token1.replacingOccurrences(of: ">", with: "")
        token1 = token1.uppercased()
        print("\nToken1 : \(token1)\n")
        deviceTokenStr = token1
        
      //  app start api call
    
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)
    }
}

////



