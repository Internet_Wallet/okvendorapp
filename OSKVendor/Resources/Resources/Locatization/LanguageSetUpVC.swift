//
//  LanguageSetUpVC.swift
//  VMart
//
//  Created by Mohit on 17/12/2019.
//  Copyright © 2019 Shobhit. All rights reserved.
//

import UIKit

protocol ChangeLanguageProtocol: class {
    func changeAppLanguage(index: Int)
}

class LanguageSetUpVC: UIViewController {
    
    var ListArray: [String]?
    var aPIManager = APIManager()
    var apiManagerClient: APIManagerClient!
    var checked = [Bool]()
    @IBOutlet var tbl : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManagerClient = APIManagerClient.sharedInstance
        self.loadInitCategoary()
        // Do any additional setup after loading the view.
        
        
        self.configLanguage()
        
        self.tbl.tableFooterView = UIView.init()
    }
    
    func configLanguage(){
        if let data = UserDefaults.standard.array(forKey: "checked") as? [Bool]{
            checked = data
        }else{
            if self.checked.isEmpty{
                checked = Array(repeating: false, count: 3)
                for (index,_) in checked.enumerated(){
                    
                    let data = UserDefaults.standard.value(forKey: "currentLanguage")
                    if data as! String == "en"{
                        if index == 0{
                        checked[index] = true
                        }
                        else{
                            checked[index] = false
                        }
                    }
                    else{
                        if index == 1{
                            checked[index] = true
                        }else{
                            checked[index] = false
                        }
                    }
                   
                }
                UserDefaults.standard.set(checked, forKey: "checked")
                UserDefaults.standard.synchronize()
            }
            if let data = UserDefaults.standard.array(forKey: "checked") as? [Bool]{
                checked = data
            }
        }
    }
    
    @IBAction func dismissTapAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    deinit {
        if self.view != nil {
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "AppLanguageChange"), object: nil)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "stringLocalised"), object: nil)
        }
    }
    
    
    func loadInitCategoary(){
        //ListArray =  ["English ( အင်္ဂလိပ်ဘာသာ )","Myanmar ( မြန်မာဘာသာ )","Unicode( ယူနီကုဒ္ )"]
        ListArray =  ["eng_code_lang".localized,"uni_code_lang".localized] //"bms_code_lang".localized,
    }
    
}

extension LanguageSetUpVC: UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else{
            return self.ListArray?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : CustomLangCell?
        if indexPath.section == 0{
            cell = tableView.dequeueReusableCell(withIdentifier: "LangCell", for: indexPath) as? CustomLangCell
            cell?.delegate = self
            cell?.lblheader.text = "Choose Language".localized
        }
        else{
            cell = (tableView.dequeueReusableCell(withIdentifier: "CustomLangCell", for: indexPath) as? CustomLangCell)
            cell?.delegate = self
            cell?.wrapData(str: ListArray?[indexPath.row], row: indexPath.row)
            if  checked[indexPath.row] == true {
                cell?.imgViewLeft.image = UIImage(named: "tick2")
            }
            else {
                cell?.imgViewLeft.image = UIImage(named: "circle")
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            if let cell = tableView.cellForRow(at: indexPath) as? CustomLangCell{
                if let _ = tableView.cellForRow(at: indexPath as IndexPath) {
                    for (index,_) in  checked.enumerated() {
                        checked[index] = false
                    }
                    checked[indexPath.row] = true
                    UserDefaults.standard.set(checked, forKey: "checked")
                    UserDefaults.standard.synchronize()
                    cell.imgViewLeft.image = UIImage(named: "tick2")
                }
                switch indexPath.row {
                case 0:
                    //                    if let language = UserDefaults.standard.value(forKey: "currentLanguage") as? String, language == "my" {
                    cell.delegate?.changeAppLanguage(index: 1)
                    //                    }
                    break
//                case 1:
//                    //                    if let language = UserDefaults.standard.value(forKey: "currentLanguage") as? String, language == "en" {
//                    cell.delegate?.changeAppLanguage(index: indexPath.row+1)
//                    //                    }
//                    break
                default:
                    //                    if let language = UserDefaults.standard.value(forKey: "currentLanguage") as? String, language == "uni" {
                    cell.delegate?.changeAppLanguage(index: 3)
                    //                    }
                    break
                }
            }
            tableView.reloadData()
            self.dismissTapAction(tableView)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 50.0
        }
        else{
            return 60.0
        }
    }
}

extension LanguageSetUpVC: ChangeLanguageProtocol{
    func changeAppLanguage(index: Int) {
        self.changeAppLanguageApiCall(index: index)
    }
    
    func changeAppLanguageApiCall(index: Int) {
        let type = index //(index == 0) ? 1 : 2
        let urlString = String(format: "%@/SetLanguage/%d", APIManagerClient.sharedInstance.base_url, type)
        guard let url = URL(string: urlString) else { return }
        let params = Dictionary<String, Any>()
        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, _) in
            if success {
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["StatusCode"] as? Int, code == 200 {
                        var selectedLanguage = ""
                        if type == 1 {
                            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
                            UserDefaults.standard.synchronize()
                            appFont = "Zawgyi-One"
                            appLang = "English"
                            selectedLanguage = "en"
                        }
                            //else if type == 2 {
//                            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
//                            UserDefaults.standard.synchronize()
//                            appFont = "Zawgyi-One"
//                            appLang = "Myanmar"
//                            selectedLanguage = "my"
//                        }
                        else{
                            UserDefaults.standard.set("Myanmar3", forKey: "appFont")
                            UserDefaults.standard.synchronize()
                            appFont = "Myanmar3"
                            appLang = "Unicode"
                            selectedLanguage = "uni"
                        }

                        UserDefaults.standard.set(selectedLanguage, forKey: "currentLanguage")
                        appDelegate.setSeletedlocaLizationLanguage(language: selectedLanguage)
                        appDelegate.setInitialCurrentLang()
                        UserDefaults.standard.synchronize()

                        NotificationCenter.default.post(name: NSNotification.Name("AppLanguageChange"), object: nil, userInfo: nil)
                    } else {
                        DispatchQueue.main.async {
                            AppUtility.showToastlocal(message: "Failure", view: self!.view)
                        }
                    }
                }
            }
        }
    }
}

class CustomLangCell: UITableViewCell{
    
    weak var delegate: ChangeLanguageProtocol?
    @IBOutlet var imgViewRight: UIImageView!
    @IBOutlet var imgViewLeft : UIImageView!
    @IBOutlet var lblheader: UILabel!{
        didSet{
            self.lblheader.font = UIFont(name: appFont, size: 15.0)
            self.lblheader.text = self.lblheader.text?.localized
            
        }
    }
    
    func wrapData(str: String?, row: Int){
        self.lblheader.text = str?.localized
        //self.imgViewLeft.image = UIImage(named: "circle")
        if row >= 1{
            self.imgViewRight.image = UIImage(named: "myanmar")
        }
        else{
            self.imgViewRight.image = UIImage(named: "ukf")
        }
    }
}

extension String {
    var localized: String {
        let appDel = UIApplication.shared.delegate as! AppDelegate
        return appDel.getlocaLizationLanguage(key: self)
    }
}
