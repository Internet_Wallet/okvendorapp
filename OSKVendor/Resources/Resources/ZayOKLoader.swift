//
//  ZayOKLoader.swift
//  VMart
//
//  Created by Ashish on 12/17/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class ZayOKLoader: UIView {
    
    static let uniqueTag = 9786
    
    @IBOutlet weak var loaderImage: UIImageView! {
        didSet {
            let gif = try! UIImage(gifName: "loader_w.gif")
            self.loaderImage.setGifImage(gif)
            self.loaderImage.startAnimating()
        }
    }
    
    @IBOutlet weak var loadingLabel: UILabel! {
        didSet {
            self.loadingLabel.font = UIFont(name: appFont, size: 15.0)
            self.loadingLabel.text = "Loading..."//.localized
            
        }
    }
    
    class func updateView() -> ZayOKLoader? {
        let nib  = UINib(nibName: String(describing: ZayOKLoader.self), bundle: Bundle.main)
        guard let view = nib.instantiate(withOwner: ZayOKLoader.self, options: nil)[0] as? ZayOKLoader else { return nil }
        view.frame = UIScreen.main.bounds
        view.tag = ZayOKLoader.uniqueTag
        view.layouting()
        return view
    }
    
    func layouting() {
        self.layoutIfNeeded()
    }
    
    deinit {
        println_debug("deinit loader")
    }
    
}
