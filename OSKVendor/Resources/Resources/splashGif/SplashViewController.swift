//
//  SplashViewController.swift
//  VMart
//
//  Created by Ashish on 12/12/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import Reachability
//import Motion
//import Material

//class SplashViewController: UIViewController, NoInternetDelegate {
    
//    let segueIdentifier = "firstScreen"
//    var aPIManager = APIManager()
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        APIManagerClient.sharedInstance.urlEnable(urlTo: APIManagerClient.sharedInstance.serverUrl)
//        APIManagerClient.sharedInstance.twoCtwoPCredential(urlTo: APIManagerClient.sharedInstance.serverUrl)
//        UserDefaults.standard.set(false, forKey: "GuestLogin")
//        //let gif = UIImage.init(gifName: "onestop_splash.gif")
//        let imageview = UIImageView(image: UIImage(named: "splash_mart1.png"))//UIImageView(gifImage: gif, loopCount: 1)
//        imageview.frame = view.bounds
//        imageview.contentMode = .scaleAspectFill
//        view.addSubview(imageview)
//
//        let time : DispatchTime = DispatchTime.now() + 0.5
//        DispatchQueue.main.asyncAfter(deadline: time) {
//            if AppUtility.isConnectedToNetwork() {
//                self.getInsideApp()
//                //imageview.removeFromSuperview()
//            } else {
//                self.view.backgroundColor = .black
//                self.loadInternetView()
//                //imageview.removeFromSuperview()
//            }
//        }
//
//        if let lang = UserDefaults.standard.value(forKey: "currentLanguage") as? String, lang == "en" {
//            changeAppLanguageApiCall(index: 1)
//        }
//        else if let lang = UserDefaults.standard.value(forKey: "currentLanguage") as? String, lang == "my" {
//            changeAppLanguageApiCall(index: 2)
//        }
//        else {
//            changeAppLanguageApiCall(index: 3)
//        }
//    }
//
//    func changeAppLanguageApiCall(index: Int) {
//        let type = index//(index == 0) ? 1 : 2
//        let urlString = String(format: "%@/SetLanguage/%d", APIManagerClient.sharedInstance.base_url, type)
//        guard let url = URL(string: urlString) else { return }
//        let params = Dictionary<String, Any>()
//        //AppUtility.showLoading(self.view)
//        print(aPIManager.getHeaderDic())
//        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, _) in
//            DispatchQueue.main.async {
//                if let viewLoc = self?.view {
//                    AppUtility.hideLoading(viewLoc)
//                } }
//            if success {
//                if let json = response as? Dictionary<String,Any> {
//                    if let code = json["StatusCode"] as? Int, code == 200 {
//                        if type == 1 {
//                            UserDefaults.standard.set("en", forKey: "currentLanguage")
//                            appLang = "English"
//                            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
//                            UserDefaults.standard.synchronize()
//
//                        } else if type == 2{
//                            UserDefaults.standard.set("my", forKey: "currentLanguage")
//                            appLang = "Myanmar"
//                            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
//                            UserDefaults.standard.synchronize()
//                        } else{
//                            UserDefaults.standard.set("uni", forKey: "currentLanguage")
//                            appLang = "Unicode"
//                            UserDefaults.standard.set("Myanmar3", forKey: "appFont")
//                            UserDefaults.standard.synchronize()
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    func loadInternetView() {
//        let noInternet = NoInternetConnectionViewController.init(nibName: String(describing: NoInternetConnectionViewController.self), bundle: Bundle.main)
//        noInternet.view.frame  = UIScreen.main.bounds
//        noInternet.delegate = self
//
//        let transition = CATransition()
//        transition.duration = 3.0
//        transition.type = .reveal //choose your animation
//        noInternet.view.layer.add(transition, forKey: nil)
//
//        noInternet.willMove(toParent: self)
//        self.view.addSubview(noInternet.view)
//        self.addChild(noInternet)
//        noInternet.didMove(toParent: self)
//    }
//
//    func getInsideApp() {
//        self.performSegue(withIdentifier: self.segueIdentifier, sender: self)
//    }
//
//    //MARK:- No Internet Delegate callback
//    func tryAgainButtonTapped(_ vc: NoInternetConnectionViewController) {
//        let transition = CATransition()
//        transition.duration = 1.0
//        transition.type = .reveal //choose your animation
//        vc.view.layer.add(transition, forKey: nil)
//        vc.view.removeFromSuperview()
//
//        self.getInsideApp()
//    }
//
//    func isNetworkReachable(_ network: Bool, vc: NoInternetConnectionViewController) {
//        DispatchQueue.main.async {
//            if network {
//                let transition = CATransition()
//                transition.duration = 1.0
//                transition.type = .reveal //choose your animation
//                vc.view.layer.add(transition, forKey: nil)
//                vc.view.removeFromSuperview()
//
//                self.getInsideApp()
//
//            } else {
//                if let topController = UIApplication.topViewController() {
//                    if !topController.isKind(of: NoInternetConnectionViewController.self) {
//                        self.loadInternetView()
//                    }
//                }
//            }
//        }
//    }
//}

//class InternetManager : NoInternetDelegate {
//    
//    static let main = InternetManager()
//    
//    private init() {}
//    
//    func syncing() {
//        do {
//            
//            let reachability = try Reachability()
//            let name = Notification.Name.reachabilityChanged
//            NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: name, object: reachability)
//            
//            try reachability.startNotifier()
//        } catch {
//            println_debug("could not start reachability notifier")
//        }
//    }
//    
//    @objc func reachabilityChanged(note: Notification) {
//        
//        let reachability = note.object as! Reachability
//        
//        switch reachability.connection {
//        case .none:
//            InternetManager.main.loadView()
//        case .wifi, .cellular:
//            println_debug("")
//        default :
//            println_debug("")
//        }
//        
//    }
//    
//    func loadView() {
//        let noInternet = NoInternetConnectionViewController.init(nibName: String(describing: NoInternetConnectionViewController.self), bundle: Bundle.main)
//        noInternet.view.frame  = UIScreen.main.bounds
//        noInternet.delegate = self
//        
//        let transition = CATransition()
//        transition.duration = 3.0
//        transition.type = .reveal //choose your animation
//        noInternet.view.layer.add(transition, forKey: nil)
//        
//        guard let keyWindow = UIApplication.shared.keyWindow else { return }
//        keyWindow.addSubview(noInternet.view)
//        keyWindow.makeKeyAndVisible()
//    }
//    
//    func tryAgainButtonTapped(_ vc: NoInternetConnectionViewController) {
//        vc.view.removeFromSuperview()
//    }
//    
//    func isNetworkReachable(_ network: Bool, vc: NoInternetConnectionViewController) {
//        DispatchQueue.main.async {
//            if network {
//                let transition = CATransition()
//                transition.duration = 1.0
//                transition.type = .reveal //choose your animation
//                vc.view.layer.add(transition, forKey: nil)
//                vc.view.removeFromSuperview()
//            } else {
//                if let topController = UIApplication.topViewController() {
//                    if !topController.isKind(of: NoInternetConnectionViewController.self) {
//                        InternetManager.main.loadView()
//                    }
//                }
//            }
//        }
//    }
//}
//
//class AnimatedNavigation : UINavigationController {
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        isMotionEnabled = true
//        motionNavigationTransitionType = .selectBy(presenting: .zoom, dismissing: .zoomOut)
//        //        motionNavigationTransitionType = .autoReverse(presenting: .zoomSlide(direction: .left))
//    }
//}
