//
//  SoundPlayer.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 30/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import AudioToolbox

final class MediaPlayer {
    static var player = AVAudioPlayer()

    class func play( fileName : String! , type : String) {
        do {
            
            let file = Bundle.main.url(forResource: fileName, withExtension: type)!
            player = try AVAudioPlayer(contentsOf: file)
            player.numberOfLoops = 0 // loop count, set -1 for infinite
            player.volume = 1
            player.prepareToPlay()

            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
            try AVAudioSession.sharedInstance().setActive(true)

            player.play()
        } catch _ {
            print("catch")
        }
    }
}

