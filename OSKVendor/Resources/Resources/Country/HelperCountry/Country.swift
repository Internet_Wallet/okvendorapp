//
//  Country.swift
//  BLEScanner
//
//  Created by Tushar Lama on 01/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation

class Country: NSObject {
    
    @objc let name: String
    
    let code: String
    var section: Int?
    let dialCode: String!
    
    init(name: String, code: String, dialCode: String = " - ") {
        self.name = name
        self.code = code
        self.dialCode = dialCode
    }
}

struct Section {
    var countries: [Country] = []
    
    mutating func addCountry(_ country: Country) {
        countries.append(country)
    }
}

protocol CountryViewControllerDelegate : class {
    func countryViewController(_ list: CountryViewController, country: Country)
    func countryViewControllerCloseAction(_ list: CountryViewController)
}
