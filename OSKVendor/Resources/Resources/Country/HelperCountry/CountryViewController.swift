//
//  CountryViewController.swift
//  BLEScanner
//
//  Created by Tushar Lama on 01/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit
import CoreTelephony
class CountryViewController:UIViewController, HelperFunctions {
    var regiCheck : String?
    public var customCountriesCode: [String]?
    
    @IBOutlet weak var lblTitle: UILabel!{
        didSet {
            lblTitle.text = "Select Your Country"//.localized
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!{
        didSet {
            searchBar.placeholder = "Search Country"//.localized
        }
    }
    @IBOutlet weak var noRecordsLabel : UILabel!
    {
        didSet {
            self.noRecordsLabel.isHidden = true
        }
    }
    
    fileprivate lazy var CallingCodes = { () -> [[String: Any]] in
        let array = revertCountryArray()
        return array as! [[String : Any]]
    }()
    
    fileprivate var searchController: UISearchController!
    fileprivate var filteredList = [Country]()
    fileprivate var unsourtedCountries : [Country] {
        var unsourtedCountries = [Country]()
        
        for item in CallingCodes {
            let displayName = item.safeValueForKey("CountryName") as! String
            let dialCode    = item.safeValueForKey("CountryCode") as! String
            let countryFlag = item.safeValueForKey("CountryFlagCode") as! String
            
            let country = Country.init(name: displayName, code: countryFlag, dialCode: dialCode)
            
            if unsourtedCountries.contains(where: {$0.dialCode == country.dialCode}) {
                
            } else {
                unsourtedCountries.append(country)
            }
        }
        if let check = regiCheck {
            if check == "removeMyanmarCountry"{
                var i = 0
                for item in unsourtedCountries {
                    if item.name == "Myanmar" {
                       unsourtedCountries.remove(at: i)
                        break
                    }
                    i += 1
                }
            }
        }
        return unsourtedCountries
    }
    
    fileprivate var _sections: [Section]?
    fileprivate var sections: [Section] {
        
        if _sections != nil {
            return _sections!
        }
        
        let countries: [Country] = unsourtedCountries.map { country in
            let country = Country(name: country.name, code: country.code, dialCode: country.dialCode)
            country.section = collation.section(for: country, collationStringSelector: #selector(getter: Country.name))
            return country
        }
        
        // create empty sections
        var sections = [Section]()
        for _ in 0..<self.collation.sectionIndexTitles.count {
            sections.append(Section())
        }
        
        // put each country in a section
        for country in countries {
            sections[country.section!].addCountry(country)
        }
        
        // sort each section
        for section in sections {
            var s = section
            s.countries = collation.sortedArray(from: section.countries, collationStringSelector: #selector(getter: Country.name)) as! [Country]
        }
        
        _sections = sections
        
        return _sections!
    }
    
    fileprivate let collation = UILocalizedIndexedCollation.current()
        as UILocalizedIndexedCollation
    
     weak var delegate: CountryViewControllerDelegate?
    
     var showCallingCodes = true
  
    func revertCountryArray() -> NSArray {
        var arrCountry = NSArray.init()
        do {
            let bundle = Bundle.init(for: CountryViewController.self)
            if let file = bundle.url(forResource: "mcc_mnc", withExtension: "txt") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? NSDictionary {
                    if let arr = object.object(forKey: "data") as? NSArray {
                        arrCountry = arr
                        return arrCountry
                    }
                } else if let object = json as? [Any] {
                    log(object)
                } else {
                    log("JSON is invalid")
                }
            } else {
                log("no file")
            }
        } catch {
            log(error.localizedDescription)
        }
        return arrCountry
    }

    override  func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.reloadData()
        tableView.tableFooterView = UIView(frame: CGRect.zero)

        definesPresentationContext = true
        let kYellowColor = UIColor.init(red: 245.0/255.0, green: 198.0/255.0, blue: 20.0/255.0, alpha: 1.0)
        
        
//        if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
//            statusbar.backgroundColor = kYellowColor
//        }
        
//        if #available(iOS 13, *) {
//        UIApplication.statusBarBackgroundColor = kYellowColor
//        } else {
//        if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
//        statusbar.backgroundColor = kYellowColor
//        }
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.searchBar.becomeFirstResponder()
//        lblTitle.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
//        if #available(iOS 13.0, *) {
//            self.searchBar.searchTextField.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
//        } else {
//
//        }
    }
    
    // MARK: Methods
    fileprivate func filter(_ searchText: String) -> [Country] {
        filteredList.removeAll()
        
        sections.forEach { (section) -> () in
            section.countries.forEach({ (country) -> () in
                if country.name.count >= searchText.count {
                    let result = country.name.compare(searchText, options: [.caseInsensitive, .diacriticInsensitive], range: searchText.startIndex ..< searchText.endIndex)
                    if result == .orderedSame {
                        filteredList.append(country)
                    }
                }
            })
        }
        
        return filteredList
    }
    
    @IBAction func back(_ sender: UIButton) {
        delegate?.countryViewControllerCloseAction(self)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
//        if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
//            statusbar.backgroundColor = UIColor.clear
//        }
    }
    
}

// MARK: - Table view data source

extension CountryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchBar.text!.count > 0 {
            return 1
        }
        return sections.count
    }
    
    
    
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchBar.text!.count > 0 {
            return filteredList.count
        }
        return sections[section].countries.count
    }
    
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tempCell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath) as? CountryCell
        
        let country: Country!
        if searchBar.text!.count > 0 {
            country = filteredList[(indexPath as NSIndexPath).row]
        } else {
            country = sections[(indexPath as NSIndexPath).section].countries[(indexPath as NSIndexPath).row]
        }
        
        tempCell?.wrapCountryCell(country: country)

        return tempCell!
        
    }
    
      func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if !sections[section].countries.isEmpty {
            return  ""//self.collation.sectionTitles[section] as String
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.00
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return collation.sectionIndexTitles
    }
    
    func tableView(_ tableView: UITableView,sectionForSectionIndexTitle title: String, at index: Int)
        -> Int {
            
            return collation.section(forSectionIndexTitle: index)
            
    }

    
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let country: Country!
        if searchBar.text!.count > 0 {
            country = filteredList[(indexPath as NSIndexPath).row]
        } else {
            country = sections[(indexPath as NSIndexPath).section].countries[(indexPath as NSIndexPath).row]
        }
        delegate?.countryViewController(self, country: country)
    }
    
}
//
// MARK: - UISearchDisplayDelegate
extension CountryViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let countryArray = filter(searchText)
        if countryArray.count == 0 && searchBar.text?.count != 0{
            
            tableView.isHidden = true
            self.noRecordsLabel.isHidden = false
            
        } else {
            
            tableView.isHidden = false
            self.noRecordsLabel.isHidden = true
            
        }
        tableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let text       = (searchBar.text! as NSString).replacingCharacters(in: range, with: text)
        let textCount  = text.count
        let char = text.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        let alphaNumeric = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: alphaNumeric).inverted
        let filteredSet = text.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
        
        if (text == filteredSet && textCount < 26) || isBackSpace == -92 {
            return true
        }
        return false
    }
    
}

class CountryCell : UITableViewCell {
    
    @IBOutlet weak var name : UILabel!
    @IBOutlet weak var flagView : UIImageView!
    @IBOutlet weak var countryCode: UILabel!
    
    func wrapCountryCell(country: Country) {
        
        self.name.text = country.name
        let bundle = Bundle.init(for: CountryCell.self)
        self.flagView.image = UIImage.init(named: country.code, in: bundle, compatibleWith: nil)
        self.flagView.layer.borderColor = UIColor.black.cgColor
        self.flagView.clipsToBounds = true
        self.flagView.contentMode = .scaleAspectFill
        self.flagView.layer.borderWidth = 1.0
        self.countryCode.text = country.dialCode
        
    }
    
}

//extension Dictionary {
//    func safeValueForKey(_ key: Key) -> Value? {
//        if self.contains(where: { $0.key == key }) {
//            if let _ = self[key] as? NSNull {
//                return nil
//            }
//            return self[key]
//        } else {
//            return nil
//        }
//    }
//
//}


func log<OK>(_ object: OK) {
    #if DEBUG
    print(object)
    #endif
}


