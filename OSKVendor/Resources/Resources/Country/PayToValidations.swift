//
//  PayToValidations.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/29/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PayToValidations: NSObject, PhValidationProtocol {
    var currentLanguage = "my"
    var localBundle : Bundle?
    let prefixRejected = ["0992",
                          "0993",
                          "0994",
                          "0982",
                          "0988",
                          "0980",
                          "0960",
                          "0961",
                          "0962",
                          "0971",
                          "0972",
                          "0974"] // "0975"
    
    let eightDigit = ["0941",
                      "0943",
                      "0947",
                      "0949",
                      "0973",
                      "0991"]
    
    let sevenDigit = ["0920",
                      "0921",
                      "0923",
                      "0924",
                      "095",
                      "096",
                      "098",
                      "0963",
                      "0968",
                      "0981",
                      "0983",
                      "0984",
                      "0985",
                      "0986",
                      "0987",
                      ]
    
     let tenDigit = ["093"]
    
    let specialValidation = ["0986"]
    
    func checkRejectedNumber(prefix: String) -> Bool {
        for numbers in self.prefixRejected {
            if numbers == prefix {
                return true
            }
        }
        
        return false
    }
    
    func checkStringHasRejectedPrefix(string: String) -> Bool {
        for numbers in self.prefixRejected {
            if string.hasPrefix(numbers) {
                return true
            }
        }
        return false
    }
    
    func checkMatchingNumber(string: String, withString str: String) -> Bool {
        if str.hasPrefix(string) {
            return true
        } else {
            return false
        }
    }
    
    func getSpecialPrefixValidation(prefix: String) -> Bool {
        for keys in self.specialValidation {
            if prefix.hasPrefix(keys) {
                return true
            }
        }
        return false
    }
    
    func checkEightDigit(prefix: String) -> Bool {
        for keys in self.eightDigit {
            if prefix.hasPrefix(keys) {
                return true
            }
        }
        
        return false
    }
    
    func checkSevenDigit(prefix: String) -> Bool {
        for keys in self.sevenDigit {
            if prefix.hasPrefix(keys) {
                return true
            }
        }
        return false
    }
    
    func getNumberRangeValidation(prefix: String) -> Int {
        
        if prefix == "0989" {
            return 11
        }
        
        for keys in self.sevenDigit {
            if prefix.hasPrefix(keys) {
                return 9
            }
        }
        
        for keys in self.eightDigit {
            if prefix.hasPrefix(keys) {
                return 10
            }
        }
        
        for keys in self.tenDigit {
            if prefix.hasPrefix(keys) {
                return 12
            }
        }

        return 11
    }
    

    func getNumberRangeValidation(_ prefix: String) -> (min: Int, max: Int, operator: String, isRejected: Bool, color: String) {
        let tuple = myanmarValidation(prefix)
        return tuple
    }
    
}

struct PhNumValidationList: Codable {
    let countryCode, countryName: String?
    let phOperators: [PhOperators]?
    
    enum CodingKeys: String, CodingKey {
        case countryCode, countryName
        case phOperators = "operators"
    }
}

struct ValidationListResponse: Codable {
    let code: Int?
    let msg: String?
    let validations: [PhNumValidationList]?
}


struct PhOperators: Codable {
    let numberSeries: [String]?
    let operatorName: String?
    let maxLenth, minLength: Int?
    let isActive: Bool?
    let operatorColor: String?
    
    enum CodingKeys: String, CodingKey {
        case numberSeries
        case operatorName = "OperatorName"
        case maxLenth, minLength, isActive
        case operatorColor = "OperatorColor"
    }
}


//Phone Number Validations
protocol PhValidationProtocol {
    func getDataFromJSONFile() -> [PhNumValidationList]?
}

extension PhValidationProtocol {
    func getDataFromJSONFile() -> [PhNumValidationList]? {
        
        
        if let path = Bundle.main.path(forResource: "MobNumValidation", ofType: "json") {
                do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    let decoder = JSONDecoder()
                    let mobNumDataResponse = try decoder.decode(ValidationListResponse.self, from: data)
                    return mobNumDataResponse.validations
                } catch {
                    return nil
                }
            }
            return nil
        
        
//        let bundle = Bundle(for: PreLoginViewControlelr.self)
//
//        if let path = bundle.path(forResource: "MobNumValidation", ofType: "json") {
//            do {
//                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
//                let decoder = JSONDecoder()
//                let mobNumDataResponse = try decoder.decode([PhNumValidationList].self, from: data)
//                return mobNumDataResponse
//            } catch {
//                return nil
//            }
//        }
//        return nil
        
    }
    
    
    
    func myanmarValidation(_ prefix: String) -> (min: Int, max: Int, operator: String, isRejected: Bool, color: String) {
        var validationList = [PhNumValidationList]()
        let validObject =  (11,11,"MPT",false, "#013d84")
    
        if phNumValidationsApi.count > 0 {
            //Api data
            validationList = phNumValidationsApi
        } else {
            //Json file data
            if phNumValidationsFile.count > 0 {
                validationList = phNumValidationsFile
            } else {
                return validObject
            }
        }
        validationList = validationList.filter {
            guard let cCode = $0.countryCode else { return false }
            return cCode == "95"
        }
        //Myanmar, India, China... But filter applied, so now Myanmar only
        for item in validationList {
            if let phOperators = item.phOperators, phOperators.count > 0 {
                var tempObject: (Int, Int, String, Bool, String, String)?
                var finalObject: (Int, Int, String, Bool, String, String)?
                //Mpt, Telenor...
                for operatorItem in phOperators {
                    //if let isActive = operatorItem.isActive, isActive == true {
                    if let nSeries = operatorItem.numberSeries, nSeries.count > 0 {
                        //928, 978, 995
                        for series in nSeries {
                            if prefix.hasPrefix(series) {
                                if let isActive = operatorItem.isActive, isActive == true {
                                    if let fObj = finalObject, fObj.5.count > series.count {
                                        continue
                                    } else {
                                        if let maxL = operatorItem.maxLenth, let minL = operatorItem.minLength, let operName = operatorItem.operatorName {
                                            tempObject = (minL, maxL, operName, false, operatorItem.operatorColor ?? "#013d84", series)
                                        } else {
                                            break
                                        }
                                    }
                                } else {
                                    finalObject = (operatorItem.minLength ?? 0, operatorItem.maxLenth ?? 0,  operatorItem.operatorName ?? "MPT", true, operatorItem.operatorColor ?? "#013d84", series)
                                    break
                                }
                            }
                        }
                        //////
                        if let safeTempObj = tempObject {
                            if let safeFinalObj = finalObject {
                                let prefixCount = prefix.count
                                if safeFinalObj.0 < safeTempObj.0 {
                                    if prefixCount <= safeFinalObj.0 {
                                        finalObject = safeFinalObj
                                    } else {
                                        finalObject = safeTempObj
                                    }
                                } else {
                                    if prefixCount <= safeTempObj.0 {
                                        finalObject = safeTempObj
                                    } else {
                                        finalObject = safeFinalObj
                                    }
                                }
                            } else {
                                finalObject = safeTempObj
                            }
                        } else {
                            continue
                        }
                    }
                    //}
                }
                ////////
                if let rtnObje = finalObject {
                    let opNa = rtnObje.2.lowercased().contains("mpt") ? "Mpt" : rtnObje.2
                    return (rtnObje.0, rtnObje.1, opNa, rtnObje.3, rtnObje.4)
                } else {
                    return validObject
                }
            } else {
                return validObject
            }
        }
        ///////
        return validObject
    }
}

public enum ToastAlignment {
    case top
    case center
    case bottom
}

extension UIViewController {
    
    func showToast(message : String) {
        
        
        let toastLabel = UILabel()
        toastLabel.text = message
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont(name: "Zawgyi-One",size: 14.0)
        toastLabel.textAlignment = .center
        toastLabel.lineBreakMode = .byWordWrapping
        toastLabel.textColor = .white
        toastLabel.backgroundColor = UIColor.clear
        let screenWidth = self.view.frame.width
        let maxMessageSize = CGSize(width:  screenWidth - 70, height: toastLabel.bounds.size.height)
        let messageSize = toastLabel.sizeThatFits(maxMessageSize)
        let actualWidth = min(messageSize.width, maxMessageSize.width)
        let actualHeight = min(messageSize.height, maxMessageSize.height)
        toastLabel.frame = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func showToast(message: String, align: ToastAlignment) {
        let bounds = self.view.bounds
        var frame: CGRect {
            if align == .top {
                return CGRect(x: bounds.size.width/2 - 75, y: bounds.origin.y + 150, width: 150, height: 35)
            }else if align == .center {
                return CGRect(x: bounds.size.width/2 - 75, y: bounds.size.height/2 - 100, width: 150, height: 35)
                
            }else {
                return CGRect(x: bounds.size.width/2 - 75, y: bounds.size.height-100, width: 150, height: 35)
            }
        }
        
        let toastLabel = UILabel(frame: frame)
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Zawgyi-One", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func showCICOToast(message: String) {
        let bounds = self.view.bounds
        let font = UIFont(name: "Zawgyi-One", size: 14)!
        let height = message.height(withConstrainedWidth: bounds.size.width - 30, font: font)
        var frame: CGRect {
            return CGRect(x: 10, y: bounds.size.height-100, width: bounds.size.width - 30, height: height + 15)
        }
        
        let toastLabel = UILabel(frame: frame)
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = font
        toastLabel.numberOfLines = 0
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 2.0, delay: 2.0, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
//    func showErrorAlert(errMessage: String) {
//        DispatchQueue.main.async {
//            alertViewObj.wrapAlert(title: nil, body: errMessage, img: #imageLiteral(resourceName: "alert-icon"))
//            alertViewObj.addAction(title: "OK", style: .target , action: {
//            })
//            alertViewObj.showAlert(controller: self)
//        }
//    }
}


extension String  {
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}



public enum Model : String {
    case simulator   = "simulator/sandbox",
    iPod1            = "iPod 1",
    iPod2            = "iPod 2",
    iPod3            = "iPod 3",
    iPod4            = "iPod 4",
    iPod5            = "iPod 5",
    iPad2            = "iPad 2",
    iPad3            = "iPad 3",
    iPad4            = "iPad 4",
    iPhone4          = "iPhone 4",
    iPhone4S         = "iPhone 4S",
    iPhone5          = "iPhone 5",
    iPhone5S         = "iPhone 5S",
    iPhone5C         = "iPhone 5C",
    iPadMini1        = "iPad Mini 1",
    iPadMini2        = "iPad Mini 2",
    iPadMini3        = "iPad Mini 3",
    iPadAir1         = "iPad Air 1",
    iPadAir2         = "iPad Air 2",
    iPadPro9_7       = "iPad Pro 9.7\"",
    iPadPro9_7_cell  = "iPad Pro 9.7\" cellular",
    iPadPro10_5      = "iPad Pro 10.5\"",
    iPadPro10_5_cell = "iPad Pro 10.5\" cellular",
    iPadPro12_9      = "iPad Pro 12.9\"",
    iPadPro12_9_cell = "iPad Pro 12.9\" cellular",
    iPhone6          = "iPhone 6",
    iPhone6plus      = "iPhone 6 Plus",
    iPhone6S         = "iPhone 6S",
    iPhone6Splus     = "iPhone 6S Plus",
    iPhoneSE         = "iPhone SE",
    iPhone7          = "iPhone 7",
    iPhone7plus      = "iPhone 7 Plus",
    iPhone8          = "iPhone 8",
    iPhone8plus      = "iPhone 8 Plus",
    iPhoneX          = "iPhone X",
    iPhoneXR         = "iPhone XR",
    iPhoneXS         = "iPhone XS",
    iPhoneXSMax      = "iPhone XS Max",
    unrecognized     = "?unrecognized?"
}

public extension UIDevice {
    public var type: Model {
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafePointer(to: &systemInfo.machine) {
            $0.withMemoryRebound(to: CChar.self, capacity: 1) {
                ptr in String.init(validatingUTF8: ptr)
                
            }
        }
        var modelMap : [ String : Model ] = [
            "i386"       : .simulator,
            "x86_64"     : .simulator,
            "iPod1,1"    : .iPod1,
            "iPod2,1"    : .iPod2,
            "iPod3,1"    : .iPod3,
            "iPod4,1"    : .iPod4,
            "iPod5,1"    : .iPod5,
            "iPad2,1"    : .iPad2,
            "iPad2,2"    : .iPad2,
            "iPad2,3"    : .iPad2,
            "iPad2,4"    : .iPad2,
            "iPad2,5"    : .iPadMini1,
            "iPad2,6"    : .iPadMini1,
            "iPad2,7"    : .iPadMini1,
            "iPhone3,1"  : .iPhone4,
            "iPhone3,2"  : .iPhone4,
            "iPhone3,3"  : .iPhone4,
            "iPhone4,1"  : .iPhone4S,
            "iPhone5,1"  : .iPhone5,
            "iPhone5,2"  : .iPhone5,
            "iPhone5,3"  : .iPhone5C,
            "iPhone5,4"  : .iPhone5C,
            "iPad3,1"    : .iPad3,
            "iPad3,2"    : .iPad3,
            "iPad3,3"    : .iPad3,
            "iPad3,4"    : .iPad4,
            "iPad3,5"    : .iPad4,
            "iPad3,6"    : .iPad4,
            "iPhone6,1"  : .iPhone5S,
            "iPhone6,2"  : .iPhone5S,
            "iPad4,1"    : .iPadAir1,
            "iPad4,2"    : .iPadAir2,
            "iPad4,4"    : .iPadMini2,
            "iPad4,5"    : .iPadMini2,
            "iPad4,6"    : .iPadMini2,
            "iPad4,7"    : .iPadMini3,
            "iPad4,8"    : .iPadMini3,
            "iPad4,9"    : .iPadMini3,
            "iPad6,3"    : .iPadPro9_7,
            "iPad6,11"   : .iPadPro9_7,
            "iPad6,4"    : .iPadPro9_7_cell,
            "iPad6,12"   : .iPadPro9_7_cell,
            "iPad6,7"    : .iPadPro12_9,
            "iPad6,8"    : .iPadPro12_9_cell,
            "iPad7,3"    : .iPadPro10_5,
            "iPad7,4"    : .iPadPro10_5_cell,
            "iPhone7,1"  : .iPhone6plus,
            "iPhone7,2"  : .iPhone6,
            "iPhone8,1"  : .iPhone6S,
            "iPhone8,2"  : .iPhone6Splus,
            "iPhone8,4"  : .iPhoneSE,
            "iPhone9,1"  : .iPhone7,
            "iPhone9,2"  : .iPhone7plus,
            "iPhone9,3"  : .iPhone7,
            "iPhone9,4"  : .iPhone7plus,
            "iPhone10,1" : .iPhone8,
            "iPhone10,2" : .iPhone8plus,
            "iPhone10,3" : .iPhoneX,
            "iPhone10,6" : .iPhoneX
        ]
        
        if let model = modelMap[String.init(validatingUTF8: modelCode!)!] {
            return model
        }
        return Model.unrecognized
    }
}
