//
//  MessageManager.swift
//  OKPaymentFramework
//
//  Created by Ashish on 11/26/18.
//  Copyright © 2018 Ashish. All rights reserved.
//

//import UIKit
//import MessageUI
// 
//
//
// protocol MessageManagerDelegate : class{
//    func respondFromMessageManager(result: MessageComposeResult)
//}
//
//
// class MessageManager : NSObject, MFMessageComposeViewControllerDelegate {
//    
//     var messageController : MFMessageComposeViewController?
//    
//     var messageDataTouple : (code: String, number: String)?
//    
//     var serverMobileNumbers = (myanmar: "+95930000044", overseas: "+14809009123")
//    
//     var urlType : URLType?
//    
//     weak var delegate : MessageManagerDelegate?
//    
//    init(_ touple: (code: String, number: String), withURL type: URLType) {
//        messageController = MFMessageComposeViewController()
//        messageDataTouple = touple
//        urlType           = type
//    }
//    
//    func publicMessage(_ object: PreLoginViewControlelr) {
//        guard let messageVC = messageController else { return }
//        guard let touple    = messageDataTouple else { return }
//        guard let manager   = EncryptionUUID.encryptionmanager() as? EncryptionUUID else { return }
//        guard let uType     = urlType else { return }
//        var mobileNumber = ""
//        
//        let encryptedMsg = manager.entryptNumber(OKPayConstants.global.uuid)
//
//        if MFMessageComposeViewController.canSendText() {
//            
//            mobileNumber = (touple.code == "+95") ? serverMobileNumbers.myanmar : serverMobileNumbers.overseas
//            
//            let messageBody = (uType == .production) ? String(format: "Registration Security #%@", encryptedMsg ?? "") : String(format: "Registration Security #%@#!TEST!#", encryptedMsg ?? "")
//            messageVC.body       = messageBody
//            messageVC.recipients = [mobileNumber]
//            messageVC.messageComposeDelegate = self
//            self.delegate = object
//            object.present(messageVC, animated: true, completion: nil)
//        }
//        
//    }
//    
//    func messageComposeViewController(_ controller: MFMessageComposeViewController,
//                                      didFinishWith result: MessageComposeResult) {
//        controller.dismiss(animated: true, completion: nil)
//        delegate?.respondFromMessageManager(result: result)
//    }
//}
