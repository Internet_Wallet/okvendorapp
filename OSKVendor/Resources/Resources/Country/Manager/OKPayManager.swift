//
//  OKPayManager.swift
//  OKPaymentFramework
//
//  Created by Ashish on 11/19/18.
//  Copyright © 2018 Ashish. All rights reserved.
//

//import UIKit
//
//public struct OKPayManager {
//    
//    public static var pay = OKPayManager()
//    
//     var model : OKPayModel?
//    
//     weak var delegate : OKPaymentDelegates?
//    
//     private var okTaxiKey : String = "be77d55cee1b1d61221aff932c3a7456"
//    
//     private var eCommerce : String = "e87acdd711d7417ab28919e0322de1db"
//    
//    public func start(_ model: OKPayModel, withDelegate delegate: OKPaymentDelegates?) -> UIViewController? {
//        let controller = self.get(model, withDelegate: delegate)
//        return controller
//    }
//    
//    private func get(_ model: OKPayModel, withDelegate delegate: OKPaymentDelegates?) -> UIViewController? {
//        
//        OKPayManager.pay.model        = model
//        
//        if let key = model.hashKey, let pType = model.applicationType {
//            switch pType {
//            case .okTaxi:
//                if key != okTaxiKey {
//                    #if DEBUG
//                    print(OKPayConstants.messagesKeys.wrongKeyWarning)
//                    #endif
//                }
//            case .okEcommerce:
//                if key != eCommerce {
//                    #if DEBUG
//                    print(OKPayConstants.messagesKeys.wrongKeyWarning)
//                    #endif
//                }
//            }
//        }
//
//        let storyboardBundle = Bundle(for: PreLoginViewControlelr.self)
//
//        let storyboard = UIStoryboard.init(name: OKPayConstants.helper.storyName, bundle: storyboardBundle)
//
//        let navigation = storyboard.instantiateViewController(withIdentifier: OKPayConstants.helper.navigationName)
//
//        if let navi = navigation as? UINavigationController {
//            
//            if let vc = navi.viewControllers.first as? PreLoginViewControlelr {
//                vc.delegate = delegate
//                vc.model = model
//            } else {
//                return nil
//            }
//            return navigation
//            
//        } else {
//            return nil
//        }
//    }
//    
//}
//
