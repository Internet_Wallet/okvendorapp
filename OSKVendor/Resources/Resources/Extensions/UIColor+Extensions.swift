//
//  UIColor+Extensions.swift
//  NopCommerce
//
//  Created by Mubassher on 10/25/16.
//  Copyright © 2016 bs23. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func navigationPrimaryColor() -> UIColor{
        //        return UIColor.init(red: 50/255.0, green: 132/255.0, blue: 179/255.0, alpha: 1.0)
        return UIColor.init(red: 40/255.0, green: 115/255.0, blue: 240/255.0, alpha: 1.0)
        
    }
    static func navigationSecondaryColor() -> UIColor{
        return UIColor.init(red: 61/255.0, green: 124/255.0, blue: 160/255.0, alpha: 1.0)
    }
    static func totalCartBackgroundColor() -> UIColor{
        //        return UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        return UIColor.init(red: 40/255.0, green: 115/255.0, blue: 240/255.0, alpha: 1.0)
    }
    static func totalCartTextColor() -> UIColor{
        return UIColor.init(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
    }
    static func messageBGColor() -> UIColor{
        return UIColor.init(red: 175/255.0, green: 64/255.0, blue: 68/255.0, alpha: 1.0)
    }
    static func categoryBGColor() -> UIColor{
        //return UIColor.init(red: 51/255.0, green: 51/255.0, blue: 51/255.0, alpha: 1.0)
        return UIColor.init(red: 40/255.0, green: 115/255.0, blue: 240/255.0, alpha: 1.0)
    }
    static func productPriceColor() -> UIColor{
        return UIColor.init(red: 249/255.0, green: 113/255.0, blue: 66/255.0, alpha: 1.0)
    }
    static func addToCartButtonTextColor() -> UIColor{
        return UIColor.white
    }
    static func addToCartButtonBackgroundColor() -> UIColor{
        return UIColor.init(red: 249/255.0, green: 113/255.0, blue: 66/255.0, alpha: 1.0)
    }
    static func addToWishListButtonTextColor() -> UIColor{
        return UIColor.init(red: 249/255.0, green: 113/255.0, blue: 66/255.0, alpha: 1.0)
    }
    static func addToWishListButtonBackgroundColor() -> UIColor{
        return UIColor.white
    }
    static func HomePageFooterButtonColor() -> UIColor{
        return UIColor.init(red: 249/255.0, green: 113/255.0, blue: 66/255.0, alpha: 1.0) //orange color
    }
    static func primaryButtonColor() -> UIColor{
        return self.navigationPrimaryColor() //navigation color
    }
    static func secondaryButtonColor() -> UIColor{
        return UIColor.init(red: 249/255.0, green: 113/255.0, blue: 66/255.0, alpha: 1.0) //orange color
    }
    static func homePageBGColor() -> UIColor{
        //return UIColor.init(red: 238/255.0, green: 238/255.0, blue: 238/255.0, alpha: 1.0)
        return UIColor.init(red: 40/255.0, green: 115/255.0, blue: 240/255.0, alpha: 1.0)
    }
    static func productPageBGColor() -> UIColor{
        //return UIColor.init(red: 234/255.0, green: 87/255.0, blue: 70/255.0, alpha: 1.0)
        return UIColor.init(red: 40/255.0, green: 115/255.0, blue: 240/255.0, alpha: 1.0)
    }
    static func subCategoryTableViewCell() -> UIColor{
        return UIColor.init(red: 204/255.0, green: 204/255.0, blue: 204/255.0, alpha: 1.0)
    }
    
    static func checkOutOptionBackgroundColor() -> UIColor{
        return UIColor.init(red: 8/255.0, green: 170/255.0, blue: 192/255.0, alpha: 1.0)
    }
    
    
    class func getColorWithHexString (_ hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.count != 6) {
            return UIColor.gray
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0
        var g:CUnsignedInt = 0
        var b:CUnsignedInt = 0
        
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        
        return UIColor(red: CGFloat(r)/255.0, green: CGFloat(g)/255.0, blue: CGFloat(b)/255.0, alpha: CGFloat(1))
    }
    
    static func colorWithRedValue(redValue: CGFloat, greenValue: CGFloat, blueValue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: redValue/255.0, green: greenValue/255.0, blue: blueValue/255.0, alpha: alpha)
    }
}
