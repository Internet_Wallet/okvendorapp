//
//  UIView+Extensions.swift
//  NopCommerce
//
//  Created by Mubassher on 10/25/16.
//  Copyright © 2016 bs23. All rights reserved.
//

import UIKit

extension UIView {
    
    //this will set rounded border with shadow
    func roundedBorderWithShadowNew(borderColor: UIColor,shadowColor: UIColor,backgroundColor: UIColor){
        self.layer.cornerRadius = 10
        self.layer.borderWidth = 1.0
        self.layer.borderColor = borderColor.cgColor
        self.layer.backgroundColor = backgroundColor.cgColor
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = CGSize(width: 2.0, height: 4.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        
    }
    
    func showSuccessMessageWithTitle(_ title: String, originY: Double){
        
        
        let messageView = UIView.init(frame: CGRect.init(x: 0, y: CGFloat(originY), width: self.frame.size.width, height: 40))
        let heightOfTextLabel = heightForView(title, font: UIFont.systemFont(ofSize: 17.0), width: self.frame.size.width)
        let messageTitleLabel = UILabel.init(frame: CGRect.init(x: 0, y: 15, width: self.frame.size.width, height: heightOfTextLabel))
        
        messageView.backgroundColor = UIColor.messageBGColor()
        messageTitleLabel.text = title
        messageTitleLabel.numberOfLines = 0
        messageTitleLabel.lineBreakMode = .byWordWrapping
        messageTitleLabel.textAlignment = .center
        messageTitleLabel.textColor = UIColor.white
        messageTitleLabel.font = UIFont.systemFont(ofSize: 17.0)
        
        //if appDelegate.langStr == "ar" && appDelegate.showCartUpdateArabicMessageInProductDetailsFlag {
        // messageTitleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0)
        //}
        
        messageView.addSubview(messageTitleLabel)
        messageView.frame.size.height = 0.0
        
        self.addSubview(messageView)
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            messageView.frame.size.height = 50
        }, completion: { (complete: Bool) in
            self.hideShowedMessageWithView(messageView)
        })
    }
    
    func showFailureMessageWithTitle(_ title: String, color: UIColor){
        
    }
    
    func hideShowedMessageWithView(_ messageView: UIView){
        
        UIView.animate(withDuration: 0.2, delay: 1.0, options: .curveEaseIn, animations: {
            messageView.frame.size.height = 0.0
        }, completion: { (complete: Bool) in
            messageView.removeFromSuperview()
        })
        
    }
    
    func heightForView(_ text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
    func hideViewWithAnimation(){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.frame.size.height = 0.0
        }, completion: { (complete: Bool) in
            self.isHidden = true
        })
    }
    
    func showAlertWith(_ title: String, message: String, buttons: [String]?, clickedItemIndex: ((_ index: Int) -> Void)?){
        
        let alertViewController = alertViewSubclass.init(title: title, message: message, preferredStyle: .alert)
        
        if buttons != nil {
            var i=0
            for btnTitle in buttons! {
                alertViewController.addAction(UIAlertAction(title: btnTitle, style: .default, handler: { (_) in
                    if clickedItemIndex != nil{
                        clickedItemIndex!(i)
                    }
                    i += 1
                }))
            }
        }
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
        
        alertViewController.viewToRemove = blurEffectView
        //self.viewController?.present(alertViewController, animated: true, completion: nil)
    }
    
}

class alertViewSubclass: UIAlertController{
    var viewToRemove: UIView?
}

extension UIAlertController{
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isKind(of: alertViewSubclass.self){
            let alertViewController = self as! alertViewSubclass
            alertViewController.viewToRemove?.removeFromSuperview()
        }
    }
    
}

extension UIApplication {

    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {

        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)

        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)

        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}

class ContentSizedTableView: UITableView {
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height + 100)
    }
}


@IBDesignable
public class Gradient: UIView {
    @IBInspectable var startColor:   UIColor = .black { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = .white { didSet { updateColors() }}
    @IBInspectable var borderColor:  UIColor =  .white { didSet { updateBorderColor() }}
    @IBInspectable var borderWidth:   Double =   1.0 { didSet { updateBorderWidth() }}
    @IBInspectable var cornerRadius:   Double =   1.0 { didSet { updateCornerRadius() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}

    override public class var layerClass: AnyClass { CAGradientLayer.self }

    var gradientLayer: CAGradientLayer { layer as! CAGradientLayer }

    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? .init(x: 1, y: 0) : .init(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 0, y: 1) : .init(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? .init(x: 0, y: 0) : .init(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 1, y: 1) : .init(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
    }
    func updateBorderColor() {
        self.layer.borderColor = borderColor.cgColor
    }
    func updateBorderWidth () {
        self.layer.borderWidth = CGFloat(borderWidth)
    }
    func updateCornerRadius () {
        self.layer.cornerRadius = CGFloat(cornerRadius)
    }
    
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updatePoints()
        updateLocations()
        updateColors()
    }

}
