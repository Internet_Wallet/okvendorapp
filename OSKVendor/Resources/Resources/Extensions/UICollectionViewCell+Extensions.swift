//
//  UICollectionViewCell+Extensions.swift
//  NopCommerce
//
//  Created by Mubassher on 10/25/16.
//  Copyright © 2016 bs23. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    
    func applyShadow(){
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowRadius = 1
        self.layer.shadowOpacity = 0.5
    }
    
    func hideBorder(){
        self.layer.borderWidth = 0.0
    }
    
    func showBorder(_ width: CGFloat, color: UIColor){
        self.layer.borderColor = color.withAlphaComponent(1.0).cgColor
        self.layer.borderWidth = width
    }
    
    //this will set rounded border with shadow
    func roundedBorderWithShadow(borderColor: UIColor,shadowColor: UIColor,backgroundColor: UIColor){
        self.layer.cornerRadius = 10
        self.layer.borderWidth = 1.0
        self.layer.borderColor = borderColor.cgColor
        self.layer.backgroundColor = backgroundColor.cgColor
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = CGSize(width: 2.0, height: 4.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        
    }
}
