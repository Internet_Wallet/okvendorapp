//
//  NotificationFile.swift
//  OSKVendor
//
//  Created by Sam' MacBook on 18/09/20.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let productDetailReload = Notification.Name("reloadProductDetailPage")
}
