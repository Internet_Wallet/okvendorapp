//
//  NSDate+Extensions.swift
//  NopCommerce
//
//  Created by Mubassher on 10/25/16.
//  Copyright © 2016 bs23. All rights reserved.
//

import UIKit

extension Date {
    
    func yearsFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.year, from: date, to: self, options: []).year!
    }
    func monthsFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.month, from: date, to: self, options: []).month!
    }
    func weeksFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.weekOfYear, from: date, to: self, options: []).weekOfYear!
    }
    func daysFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.day, from: date, to: self, options: []).day!
    }
    func hoursFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.hour, from: date, to: self, options: []).hour!
    }
    func minutesFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.minute, from: date, to: self, options: []).minute!
    }
    func secondsFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.second, from: date, to: self, options: []).second!
    }
//    func offsetFrom(_ date:Date) -> String {
//        if yearsFrom(date)   > 0 { return "\(yearsFrom(date)) "+"year\(yearsFrom(date)>1 ? "s" : "")".localized   }
//        if monthsFrom(date)  > 0 { return "\(monthsFrom(date)) "+"month\(monthsFrom(date)>1 ? "s" : "")".localized  }
//        if weeksFrom(date)   > 0 { return "\(weeksFrom(date)) "+"week\(weeksFrom(date)>1 ? "s" : "")".localized   }
//        if daysFrom(date)    > 0 { return "\(daysFrom(date)) "+"day\(daysFrom(date)>1 ? "s" : "")".localized    }
//        if hoursFrom(date)   > 0 { return "\(hoursFrom(date)) "+"hour\(hoursFrom(date)>1 ? "s" : "")".localized   }
//        if minutesFrom(date) > 0 { return "\(minutesFrom(date)) "+"minute\(minutesFrom(date)>1 ? "s" : "")".localized }
//        if secondsFrom(date) > 0 { return "\(secondsFrom(date)) "+"second\(secondsFrom(date)>1 ? "s" : "")".localized }
//        return ""
//    }
}
