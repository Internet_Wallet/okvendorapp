//
//  SAlertController.swift
//  VMart
//
//  Created by Imac on 2/15/19.
//  Copyright © 2019 Shobhit. All rights reserved.
//

import UIKit

enum SAlertButtonType {
    case defualt
    case custom
}

class SAlertController: UIViewController {
    
    var alrtType: SAlertButtonType = .defualt
    let bgView = UIView()
    let bgImage = UIImageView()
    let lblTitle = UILabel()
    let lblMessage = UILabel()
    let btnAction = UIButton()
    let stack = UIStackView()
    let fontWithSize =  UIFont(name: appFont, size: 16.0)
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        bgView.frame = CGRect(x: 25, y: (UIScreen.main.bounds.height/2) - 150  , width: UIScreen.main.bounds.width - 50, height: 200)
        bgView.layer.cornerRadius = 10
        bgView.layer.masksToBounds = true
        bgView.backgroundColor = UIColor.white
        //Image Added-Gauri
        bgImage.frame = CGRect(x: (self.bgView.frame.width / 2 - 50) , y: -25 , width: 100 , height: 100 )
        bgImage.image = UIImage(named: "logo_blue")
        bgImage.contentMode = .scaleToFill
        bgView.addSubview(bgImage)
        //
        lblTitle.frame = CGRect(x: 0, y: 0, width: bgView.frame.width, height: 45)
        lblTitle.textAlignment = .center
        lblTitle.font = fontWithSize
        bgView.addSubview(lblTitle)
        
        
        //
        let Line = UIButton(frame: CGRect(x: 0, y: 45, width: bgView.frame.width, height: 1))
        //        Line.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        
        Line.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 10, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 15.0/255.0, green: 182.0/255.0, blue: 80.0/255.0, alpha: 1.0)])
        //        Line.backgroundColor = UIColor.init(red: 36.0/255.0, green: 105.0/255.0, blue: 215.0/255.0, alpha: 1.0)
        bgView.addSubview(Line)
        
        let bgButon = UIButton()
        bgButon.frame = CGRect(x: 0, y: 150, width: bgView.frame.width, height: 50)
        //        bgButon.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        bgButon.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 10 , greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 15.0/255.0, green: 182.0/255.0, blue: 80.0/255.0, alpha: 1.0)])
        bgButon.backgroundColor = UIColor.init(red: 0.0/255.0, green: 119.0/255.0, blue: 183.0/255.0, alpha: 1.0)
        bgButon.isUserInteractionEnabled = false
        bgView.addSubview(bgButon)
        
        lblMessage.frame = CGRect(x: 0, y: 46, width: bgView.frame.width, height: 104)
        lblMessage.textAlignment = .center
        lblMessage.numberOfLines = 5
        lblMessage.lineBreakMode = .byWordWrapping
        lblMessage.font = fontWithSize
        bgView.addSubview(lblMessage)
        
        self.view.addSubview(bgView)
        
        self.view.backgroundColor = UIColor.init(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.6)
        
        let TapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.HandleTapGesture(_:)))
        
        //TapGesture.cancelsTouchesInView = false
        
        self.view.addGestureRecognizer(TapGesture)
    }
    
    @objc func HandleTapGesture(_:UIGestureRecognizer) {
        
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
    
    
    func hideAlert() {
        
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
    
    
    func addAction(action : [SAlertAction]) {
        
        guard action.count != 0 else {
            return
        }
        let w_width = bgView.frame.width / CGFloat(action.count)
        var x_Axis: CGFloat = 0
        
        for btn in action {
            btn.button.frame = CGRect(x: x_Axis, y: 150, width: w_width, height: 50)
            btn.button.setTitle(btn.button.titleLabel?.text, for: .normal)
            btn.button.titleLabel?.font = fontWithSize
            btn.button.backgroundColor = UIColor.clear
            //            if alrtType == .defualt {
            //                btn.button.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            //            }
            btn.button.addTarget(self, action: #selector(okAction), for: .touchUpInside)
            bgView.addSubview(btn.button)
            x_Axis += w_width
        }
    }
    
    func  ShowSAlert(title: String, withDescription message: String, onController controller: UIViewController) {
            controller.addChild(self)
            self.view.frame =  controller.view.bounds
            controller.view.addSubview(self.view)
            self.didMove(toParent: self)
            self.lblTitle.text = title
            self.lblMessage.text = message//.localized
    }
    
    @objc func okAction(sender: UIButton) {
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
    
}


class SAlertAction: NSObject {
    
    var alertType: SAlertButtonType?
    var button = UIButton()
    var bgColor: UIColor?
    var txtColor: UIColor?
    
    func action(name: String, AlertType type: SAlertButtonType, withComplition completion: @escaping ()->Void)  {
        alertType = type
        if type == .defualt {
            button.setTitleColor(UIColor.white, for: .normal)
            button.backgroundColor = UIColor.blue
        }else {
            button.backgroundColor  = bgColor
            button.setTitleColor(txtColor, for: .normal)
        }
        button.setTitle(name, for: .normal)
        button.addTargetClosure(closure: { (button)in
            completion()
        })
        
    }
}

class ReturnAfterDone: NSObject {
    let closure: UIButtonClosure
    init(_ closure: @escaping UIButtonClosure) {
        self.closure = closure
    }
}

typealias UIButtonClosure = (UIButton) -> ()
extension UIButton {
    private struct AssociatedKeys {
        static var targetClosure = "targetClosure"
    }
    private var targetClosure: UIButtonClosure? {
        get {
            guard let closureWrapper = objc_getAssociatedObject(self, &AssociatedKeys.targetClosure) as? ReturnAfterDone else { return nil }
            return closureWrapper.closure
        }
        set(newValue) {
            guard let newValue = newValue else { return }
            objc_setAssociatedObject(self, &AssociatedKeys.targetClosure, ReturnAfterDone(newValue), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    func addTargetClosure(closure: @escaping UIButtonClosure) {
        targetClosure = closure
        addTarget(self, action: #selector(UIButton.closureAction), for: .touchUpInside)
    }
    @objc func closureAction() {
        guard let targetClosure = targetClosure else { return }
        targetClosure(self)
    }
}
