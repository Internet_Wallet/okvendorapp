//
//  Validation.swift
//  OSKVendor
//
//  Created by vamsi on 7/1/20.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class Validation: NSObject {
    
    func isNameString(_ name: String?) -> Bool {

        let nameregex = "[a-zA-Z]*$"
        let nametest = NSPredicate(format: "SELF MATCHES %@", nameregex)
        return nametest.evaluate(with: name)
    }
    func isEmailString(_ name: String?) -> Bool {

         let nameregex = "\\A[A-Za-z0-9]+([-._][a-z0-9]+)*+@[A-Za-z]+\\.[A-Za-z]{2,4}"
         let nametest = NSPredicate(format: "SELF MATCHES %@", nameregex)
         return nametest.evaluate(with: name)
     }
    func ispasswordString(_ name: String?) -> Bool {

            let nameregex = "[a-zA-Z0-9]*$"
            let nametest = NSPredicate(format: "SELF MATCHES %@", nameregex)
            return nametest.evaluate(with: name)
     }
    func isaddressString(_ name: String?) -> Bool {

           let nameregex = "[A-Za-z0-9._!%@#&*-=$^ ]*$"
           let nametest = NSPredicate(format: "SELF MATCHES %@", nameregex)
           return nametest.evaluate(with: name)
    }
    

}
