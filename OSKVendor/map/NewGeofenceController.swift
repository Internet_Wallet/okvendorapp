//
//  NewGeofenceController.swift
//  BLEScanner
//
//  Created by iMac on 09/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit
import MapKit
import AVFoundation

class NewGeofenceController: UIViewController {
    
    var allRegions: [CircularRegion]! = nil
    let player = AVQueuePlayer()
    var state = ""
    let ENTERED_HOME_REGION_MESSAGE = "Welcome to Home geofence If the waves are good, you can try surfing!"
    let ENTERED_WORK_REGION_MESSAGE = "Welcome to work geofence If the waves are good, you can try surfing!"
    let ENTERED_REGION_NOTIFICATION_ID = "EnteredRegionNotification"
    let EXITED_Home_REGION_MESSAGE = "Bye! Hope you had a great day at the home grofence!"
    let EXITED_WORK_REGION_MESSAGE = "Bye! Hope you had a great day at the work grofence!"
    let EXITED_REGION_NOTIFICATION_ID = "ExitedRegionNotification"
    
    var CHARSET = ""
    var myLanguage = ""
     
    
    @IBOutlet weak var searchView: CardView!{
        didSet{
            self.searchView.isHidden = true
        }
    }
    
    @IBOutlet weak var customAnnotionView: CardView!{
        didSet{
            self.customAnnotionView.isHidden = true
        }
    }
    
    @IBOutlet var addAddressLocation: UIButton?{
        didSet {
            self.addAddressLocation?.setTitle("Add Address".localized, for: .normal)
             
        }
    }
    
    @IBOutlet var myLocation: UIButton?{
        didSet {
            self.myLocation?.setTitle("OK".localized, for: .normal)
        }
    }
    
    @IBOutlet var workLocation: UIButton?{
        didSet {
            self.workLocation?.setTitle("OK".localized, for: .normal)
        }
    }
    
    @IBOutlet var quarintineLocation: UIButton?{
        didSet {
            self.quarintineLocation?.setTitle("OK".localized, for: .normal)
        }
    }
    
    @IBOutlet var editLocationButton: UIButton?{
        didSet {
             
        }
    }
    
    @IBOutlet var deleteBtn: UIButton?{
        didSet {
            
            deleteBtn?.isHidden = true
        }
    }
    
    @IBOutlet var deleteLbl : UILabel?{
        didSet {
            self.deleteLbl?.text = self.deleteLbl?.text
        }
    }
    
    
    @IBOutlet var AddressLbl : UILabel?
    @IBOutlet var searchLocation : UITextField?{
        didSet {
            searchLocation?.placeholder = "Search Location".localized
        }
    }
    
    
    weak var addressDelegate : AddressDelegate?
    var locationLat : String = ""
    var locationLong : String = ""

    @IBOutlet var navigationView: UIView!
    
    @IBOutlet var backButton: UIButton!
    @IBOutlet var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "Add Address".localized
        }
    }
    
    @IBOutlet var myLocationBtnHeight: NSLayoutConstraint?
    @IBOutlet var workLocationBtnHeight: NSLayoutConstraint?
    @IBOutlet var quarintineBtnHeight: NSLayoutConstraint?
    
    @IBOutlet var deleteView : UIView!
    @IBOutlet var deleteBaseVeiw : UIView!
    @IBOutlet weak var mapView: MKMapView!
    var destinationString : String?
    
    var titleStr = ""
    var markerImageView: UIImageView?
    var annotation = MKPointAnnotation()
    var CustomMApView = MKAnnotationView()
    var AfterDragCoordinate: CLLocationCoordinate2D?
    
    
    func loadInitialize() {
        if myLanguage == "my"{
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀.@_ - "
        }else if myLanguage == "uni"{
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀.@_ - "
        }else {
            CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_ -"
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loadInitialize()
        self.loadMultipleRegion()
    }
    
    func checkForGeoFense(typeX : String) -> Bool {
        var isFound : Bool = false
        for obj in self.allRegions{
            if let type = obj.typeIdentifier as? String , type == typeX {
                isFound = true
                break
            }
        }
        return isFound
    }
    
    func loadMultipleRegion() {
        allRegions = CircularRegion.loadata()
        let closestRegion = GeofencingRegion.shared.evaluateClosestRegions(regions: allRegions)
        self.setUpGeofenceForPlayaGrandeBeach(regions: closestRegion)
    }
    
    
    func geoFenceload(){
        GeofencingRegion.shared.delegate = self
        GeofencingRegion.shared.startUpdatingLocation()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         loadMapIcon()
        self.mapView.delegate = self
        self.deleteBaseVeiw.isHidden = true
        
        let gestureTap = UITapGestureRecognizer.init(target: self , action: #selector(removeBaseView))
        gestureTap.numberOfTapsRequired = 1
        self.deleteBaseVeiw.addGestureRecognizer(gestureTap)
        self.setBackButton()
        searchLocation?.delegate = self
       
     //   ConfigBasic()
        self.geoFenceload()
//        self.setSearchButton()
//        self.searchView.isHidden = false
        
    }
    
    
    func ConfigBasic(){
        let firstDate = Date()
        let secondDate = Date.init(timeIntervalSinceNow: 86400)
        let thirdDate = Date.init(timeIntervalSinceNow: 86400*2)
        //Will return true
        let timeEqual1 = firstDate.time == secondDate.time
        let timeEqual2 = secondDate.time == thirdDate.time
        let isPassedMoreThan = self.isPassedMoreThan(days: 2, fromDate: firstDate, toDate: thirdDate)
        print(firstDate)
        print(secondDate)
        print(thirdDate)
        
        print(timeEqual1)
        print(timeEqual2)
        print(isPassedMoreThan)
       
    }
    
    
    func isPassedMoreThan(days: Int, fromDate date : Date, toDate date2 : Date) -> Bool {
        
        let unitFlags: Set<Calendar.Component> = [.day]
        let deltaD = Calendar.current.dateComponents( unitFlags, from: date, to: date2).day
        return (deltaD ?? 0 > days)
        
    }
    
    @objc func removeBaseView () {
        self.deleteBaseVeiw.isHidden = true
    }
    
    
    func location(lat: String,long:String){
        
        if AppUtility.isConnectedToNetwork() {
             AppUtility.showLoading(self.view)
            let isPermission =  VMGeoLocationManager.shared.setUpLocationManager()
            if isPermission {
                VMGeoLocationManager.shared.startUpdateLocation()
                VMGeoLocationManager.shared.getAddressFrom(lattitude:"\(lat)", longitude: "\(long)", language: "en")
                { (status, data) in
                    
                    if let addressDic = data as? Dictionary<String, String> {
                        DispatchQueue.main.async {
                            AppUtility.hideLoading(self.view)

                            print(addressDic)
                            var address = ""
                            var str = ""
                            if let street = addressDic["street"] {
                                print(street)
                                address += street
                            }
                            if let city = addressDic["city"]{
                                print(city)
                               address += " , \(city)"
                                
                                str = "1"
                                
                            }
                            if let region = addressDic["region"] {
                                print(region)
                                if str == "1"{
                                     address += " , \(region)"
                                }
                                else{
                                     address += "  \(region)"
                                }
                               
                            }
                            
                            let regionadd = addressDic["formatted_address"]
                            print("\(addressDic)")
                            self.AddressLbl?.text = " \(address),\(regionadd ?? "")"
                            
                            if let regionadd = addressDic["formatted_address"], address == ""  {
                                print(regionadd)
                                self.AddressLbl?.text = " \(address),\(regionadd)"
                            }
                            
                        }
                    }
                }
            }
        }else {
//            Utility.alert(message: "", title: "No Network Available", controller: self)
        }
    }
    
    func getCordination(PlaceId: String){
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let isPermission =  VMGeoLocationManager.shared.setUpLocationManager()
            if isPermission {
                VMGeoLocationManager.shared.startUpdateLocation()
                
                VMGeoLocationManager.shared.getCordinateFromPlaceId(placeId: PlaceId, language: "en") { (status, data) in
                    if let addressDic = data as? NSDictionary {
                        DispatchQueue.main.async {
                            AppUtility.hideLoading(self.view)
                            print(addressDic)
                            if let addressDic = addressDic["location"] as? NSDictionary {
                                let lat = addressDic.value(forKey: "lat") as? Double
                                let lon = addressDic.value(forKey: "lng") as? Double
                                
                                
                                
                                let coordinates = CLLocationCoordinate2D(latitude:lat ?? 0.0
                                    , longitude:lon ?? 0.0)
                                self.currentCordinateShow(coordinate: coordinates)
                            }
                        }
                    }
                }
            }
        }else {
//            Utility.alert(message: "", title: "No Network Available", controller: self)
        }
    }
    
    
    
    func loadMapIcon(){
        markerImageView = UIImageView(image: UIImage(named: "Marker"))
        //        markerImageView?.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        //        markerImageView?.center = mapView.center
        
        
        markerImageView?.frame = CGRect(x: (self.view.frame.size.width / 2)    , y: (self.view.frame.size.height / 2) - 50  , width: 45 , height: 45 )
        markerImageView?.center = CGPoint(x: (self.view.frame.size.width / 2)   , y: (self.view.frame.size.height / 2) - 50 )
        
        
        
        mapView.addSubview(markerImageView ?? UIImageView())
        markerImageView?.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.markerImageView?.transform = .identity
                       },
                       completion: nil)
    }
    
    
    func AfterDragInitialRegion(radius: Int){
        if let lat = self.AfterDragCoordinate?.latitude,let long = self.AfterDragCoordinate?.longitude{
            //            self.loadMultipleRegion(latitute: "\(lat)", longitute: "\(long)", radius: Float(radius), msg: "AfterDragInitialRegion")
            if let overlays = mapView?.overlays {
                for overlay in overlays {
                    
                    mapView?.removeOverlay(overlay)
                    
                }
            }
            
            let geofenceRegionCenter = CLLocationCoordinate2DMake(lat, long);
            let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: CLLocationDistance(radius), identifier: "CurrentLocation");
            geofenceRegion.notifyOnExit = true;
            geofenceRegion.notifyOnEntry = true;
            let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
            let mapRegion = MKCoordinateRegion(center: geofenceRegionCenter, span: span)
            self.mapView.setRegion(mapRegion, animated: true)
            let regionCircle = MKCircle(center: geofenceRegionCenter, radius: CLLocationDistance(radius))
            self.mapView.addOverlay(regionCircle)
            self.mapView.showsUserLocation = false;
        }
    }
    
    
    
    @objc func backClick(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func setBackButton(){
        let button = UIButton()
       // button.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        button.addTarget(self, action: #selector(backClick), for: .touchUpInside)
        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        
    }
    
    
    func setSearchButton(){
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "circle"), for: .normal)
        button.addTarget(self, action: #selector(searchClick), for: .touchUpInside)
        //        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = item
        
    }
    
    @objc func searchClick(){
        self.searchView.isHidden = false
    }
    
    
    func showLocationDisabledpopUp() {
        
        DispatchQueue.main.async {
            AppUtility.hideLoading(self.view)

            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "Location Services are disabled", withDescription: "Please enable Location Services in your Settings", onController: self)
            
            let cancelAction = SAlertAction()
            cancelAction.action(name: "Cancel".localized, AlertType: .defualt, withComplition: {
                self.dismiss(animated: true, completion: nil)

            })
            let openAction = SAlertAction()
            openAction.action(name: "Setting".localized, AlertType: .defualt, withComplition: {
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            })
            
            alertVC.addAction(action: [cancelAction,openAction])
        }
    }
    
    @IBAction func CurrentLocationClick(_ sender: UIButton){
        self.geoFenceload()
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

        //self.navigationController?.popViewController(animated: true)
    }
  
    @objc func addRegion() {
        
        
    }
    
    
    @IBAction func setAddAddressAction(_ sender: Any) {
        
        self.addressDelegate?.setTheAddress(adrress: self.AddressLbl?.text ?? "")
        self.addressDelegate?.setAddressWithLatLong(adrress: self.AddressLbl?.text ?? "", lat: locationLat, long: locationLong)
        self.dismiss(animated: true, completion: nil)

        //self.navigationController?.popViewController(animated: true)
    }
    
    func loadMultipleRegion(latitute: String, longitute: String, radius: Float, msg: String, txtType: String) {
        let allRegions = CircularRegion.setData(latitute: latitute, longitute: longitute, radius: radius, msg: msg, txtType: txtType)
        let closestRegion = GeofencingRegion.shared.evaluateClosestRegions(regions: allRegions)
        self.setUpGeofenceForPlayaGrandeBeach(regions: closestRegion)
    }
    
    
    func setUpGeofenceForPlayaGrandeBeach(regions: [CircularRegion]) {
        for (index, _region) in regions.enumerated() {
            let regiondata = regions[index] as? CircularRegion
            let type = regiondata?.typeIdentifier
            let annotation = MKPointAnnotation()
                               let Coordinates = CLLocationCoordinate2D(latitude: regions[index].coordinate.latitude, longitude: regions[index].coordinate.longitude)
                               annotation.coordinate = Coordinates
                               mapView.addAnnotation(annotation)
                               let geofenceRegionCenter = CLLocationCoordinate2DMake(_region.coordinate.latitude, _region.coordinate.longitude);
                               let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: regions[index].coordinate.radius, identifier: "CurrentLocation");
                               geofenceRegion.notifyOnExit = true;
                               geofenceRegion.notifyOnEntry = true;
                               let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
                               let mapRegion = MKCoordinateRegion(center: geofenceRegionCenter, span: span)
                               self.mapView.setRegion(mapRegion, animated: true)
                               let regionCircle = MKCircle(center: geofenceRegionCenter, radius: regions[index].coordinate.radius)
                               self.mapView.addOverlay(regionCircle)
                               self.mapView.showsUserLocation = false;
            }
        }
     
    
 
    
   
 
    
    func displayCustomAnnotation(_ mapView: MKMapView, didSelect view: MKAnnotationView){
        // 1
        if view.annotation is MKUserLocation{
            // Don't proceed with custom callout
            return
        }
        
        self.customAnnotionView.isHidden = false
        self.customAnnotionView.center = CGPoint(x: view.bounds.size.width / 2, y: -self.customAnnotionView.bounds.size.height*0.52)
        view.addSubview(self.customAnnotionView)
        mapView.setCenter((view.annotation?.coordinate)!, animated: true)
    }
    
}

extension NewGeofenceController: MKMapViewDelegate{
     
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        //        let identifier = "MyPin"
        //        if annotation is MKUserLocation {
        //            return nil
        //        }
        //        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        //        if annotationView == nil {
        //            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        //            annotationView?.canShowCallout = true
        //
        //            annotationView?.image = UIImage(named: "location_map")
        //            // annotationView?.frame = CGRect(x: (annotationView?.center.x)!, y: (annotationView?.center.y)! + 100, width: 60, height: 60)
        //
        //            let detailButton = UIButton(type: .detailDisclosure)
        //            annotationView?.rightCalloutAccessoryView = detailButton
        //        } else {
        //            annotationView?.annotation = annotation
        //        }
        //        self.CustomMApView = annotationView ?? MKAnnotationView()
        //        return annotationView
        return nil
    }
    
    
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        //print("regionDidChangeAnimated")
        
        self.AfterDragCoordinate = mapView.centerCoordinate
        
       // show
        if let lat = self.AfterDragCoordinate?.latitude,let long = self.AfterDragCoordinate?.longitude{
            self.locationLat = "\(lat)"
            self.locationLong = "\(long)"
            self.location(lat: "\(lat)", long: "\(long)")
        }
        
//        // hide
//                     if let lastAddress = UserDefaults.standard.value(forKey: "lastHomeAddress") as? String{
//                         self.AddressLbl?.text = lastAddress
//                     }
        
         
        
        
    }
    
    
    //MARK - MKMapViewDelegate
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let overlayRenderer : MKCircleRenderer = MKCircleRenderer(overlay: overlay);
        overlayRenderer.lineWidth = 4.0
        overlayRenderer.strokeColor = UIColor.red
        overlayRenderer.fillColor = UIColor(red: 76.0/255.0, green: 123.0/255.0, blue: 200.0/255.0, alpha: 0.7)
        return overlayRenderer
    }
}

extension NewGeofenceController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
   
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location == 0 && string == " " {
            return false
        }
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        if textField == searchLocation {
            
        }
        return restrictMultipleSpaces(str: string, textField: textField)
    }
    
    func restrictMultipleSpaces(str : String, textField: UITextField) -> Bool {
        if str == " " && textField.text?.last == " "{
            return false
        } else {
            if characterBeforeCursor(tf: textField) == " " && str == " " {
                return false
            } else {
                if characterAfterCursor(tf: textField) == " " && str == " " {
                    return false
                } else {
                    return true
                }
            }
        }
    }
    
    func characterBeforeCursor(tf : UITextField) -> String? {
        if let cursorRange = tf.selectedTextRange {
            if let newPosition = tf.position(from: cursorRange.start, offset: -1) {
                let range = tf.textRange(from: newPosition, to: cursorRange.start)
                return tf.text(in: range!)
            }
        }
        return nil
    }
    
    func characterAfterCursor(tf : UITextField) -> String? {
        if let cursorRange = tf.selectedTextRange {
            if let newPosition = tf.position(from: cursorRange.start, offset: 1) {
                let range = tf.textRange(from: newPosition, to: cursorRange.start)
                return tf.text(in: range!)
            }
        }
        return nil
    }
    
}




extension NewGeofenceController:GeofencingProtocol{
    
    
    func authorization(state: CLAuthorizationStatus) {
        
        if (state == CLAuthorizationStatus.authorizedAlways) {
            self.loadMultipleRegion()
            
        }else if (state == CLAuthorizationStatus.denied){
            showLocationDisabledpopUp()
        }
    }
    
    
    func currentCordinateShow(coordinate: CLLocationCoordinate2D){
        
        let geofenceRegionCenter = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
        let mapRegion = MKCoordinateRegion(center: geofenceRegionCenter, span: span)
        
        self.mapView.setRegion(mapRegion, animated: true)
        self.mapView.showsUserLocation = false;
    }
    
    
    func didEnterRegion() {
         
    }
    
    func didExitRegion() {
        
        
        
    }
     
    func getLatestCoordiante(location: CLLocation) {
         
        self.currentCordinateShow(coordinate: location.coordinate)
        GeofencingRegion.shared.stopUpdatingLocation()
         
    }
     
}


extension MKMapView {
    func topLeftCoordinate() -> CLLocationCoordinate2D {
        return convert(.zero, toCoordinateFrom: self)
    }
    
    func bottomRightCoordinate() -> CLLocationCoordinate2D {
        return convert(CGPoint(x: frame.width, y: frame.height), toCoordinateFrom: self)
    }
}
