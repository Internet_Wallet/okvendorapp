//
//  GeofencingProtocol.swift
//  BLEScanner
//
//  Created by iMac on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import CoreLocation


public protocol GeofencingProtocol: class {
    func authorization(state: CLAuthorizationStatus)
    func didEnterRegion()
    func didExitRegion()
    func getLatestCoordiante(location: CLLocation)
}
