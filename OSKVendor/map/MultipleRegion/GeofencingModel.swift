//
//  GeofencingModel.swift
//  BLEScanner
//
//  Created by iMac on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import CoreLocation


public struct Coordinate {
    
    public let latitude: CLLocationDegrees
    public let longitude: CLLocationDegrees
    let radius: CLLocationDistance
    var identifier: String
    
    
}

public class CircularRegion {
    
    public var coordinate: Coordinate
    public var distance: Double = 0.0
    public var typeIdentifier : String?
    
    
    lazy var region: CLCircularRegion = { [unowned self] in
        
        let geofenceRegionCenter = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude)
        let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter,
                                              radius: coordinate.radius,
                                              identifier: coordinate.identifier)
        
        return geofenceRegion
        }()
    
    public init(location: Coordinate , type : String) {
        self.coordinate = location
        self.typeIdentifier = type
        self.coordinate.identifier = type
    }
    
    //    public static func loadDummyData() -> [CircularRegion] {
    //        // Dummy data
    //
    //        let region1 = CircularRegion(location: Coordinate(latitude: 16.768452, longitude: 96.171956, radius: 200, identifier: "Botataung Pagoda"))
    //        let region2 = CircularRegion(location: Coordinate(latitude: 16.817489, longitude: 96.130965, radius: 200, identifier: "Junction Square"))
    //        let region3 = CircularRegion(location: Coordinate(latitude: 16.826201, longitude: 96.130248, radius: 200, identifier: "Hledan Centre"))
    //        let region4 = CircularRegion(location: Coordinate(latitude: 16.901718, longitude: 96.134663, radius: 200, identifier: "Yangon International Airport"))
    //        let region5 = CircularRegion(location: Coordinate(latitude: 20.783333, longitude: 97.033333, radius: 200, identifier: "Taunggyi"))
    //
    //        return [region1, region2, region3, region4, region5]
    //
    //    }
    
    
    public static func setData(latitute: String, longitute: String, radius: Float, msg: String, txtType: String) -> [CircularRegion] {
        
        var regionArr = [CircularRegion]()
        regionArr.removeAll()
        let regionObj = CircularRegion(location: Coordinate(latitude: Double(latitute)!, longitude: Double(longitute)!, radius: CLLocationDistance(radius), identifier: msg), type: txtType)
        regionArr.append(regionObj)
        
        
        let Coordinates = CLLocationCoordinate2DMake(CLLocationDegrees(latitute)!, CLLocationDegrees(longitute)!)
        //        ApplicationState.sharedAppState.geofenceModelObj.append(GeofenceDataModel(cordinate: Coordinates, region: "\(radius)", txtmsg: msg))
        print(txtType)
        
        
        //core data
        
        GeofencingRegion.shared.loadMonitoringRegions()
        return regionArr
        
    }
    
    
    
    public static func loadata() -> [CircularRegion]{
        
        var regionArr = [CircularRegion]()
        regionArr.removeAll()
//        let copyObj = ApplicationState.sharedAppState.geofenceModelObj
//        
//        if copyObj.count>0{
//            for i in 0..<copyObj.count{
//                print(copyObj)
//                let regionObj = CircularRegion(location: Coordinate(latitude: Double((copyObj[i].cordinate?.latitude)!), longitude: Double((copyObj[i].cordinate?.longitude)!), radius: CLLocationDistance((copyObj[i].region))!, identifier: copyObj[i].txtmsg), type: copyObj[i].txtType)
//               
//                regionArr.append(regionObj)
//            }
//        }
        return regionArr
    }
    
}
