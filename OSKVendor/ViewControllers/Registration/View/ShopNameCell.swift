//
//  ShopNameCell.swift
//  OSKVendor
//
//  Created by OK$ on 21/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol shopNameDelegate {
    func setTheshopName( shopName :String)
    func textediting()
}

class ShopNameCell: UITableViewCell {
    
    @IBOutlet weak var imageCat : UIImageView!
    @IBOutlet weak var txtShopName : SkyFloatingLabelTextField!{
        didSet{
            txtShopName.titleLabel.font = UIFont(name: appFont, size: 15.0)
            txtShopName.placeholder = "Enter Shop Name/Name".localized
            txtShopName.titleLabel.text = "SHOP NAME/NAME".localized
        }
    }
    
    @IBOutlet weak var ShopBannerView : UIView!
    @IBOutlet weak var gradView : Gradient!
    @IBOutlet weak var lblShopName : UILabel!
    @IBOutlet weak var btnInfo : UIButton!
    
    
    var shopDelegate:shopNameDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.txtShopName.delegate = self
        
        self.imageCat.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        self.imageCat.addGestureRecognizer(tap)
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        self.gradView.addGestureRecognizer(tap1)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        gesture.numberOfTapsRequired = 1
        self.ShopBannerView.addGestureRecognizer(gesture)
        self.txtShopName.placeholder = "Enter Shop Name/Name".localized
        self.txtShopName.selectedTitle = "SHOP NAME/NAME".localized
        // Configure the view for the selected state
        
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        gesture.numberOfTapsRequired = 1
        self.imageCat.addGestureRecognizer(gesture1)
        
       
    }
    
    
    @objc func handleTap () {
        self.txtShopName.isHidden = false
        let imageName = (try? UIImage(gifName: "shop_stay_gif")) ?? UIImage()
        self.imageCat.setGifImage(imageName)
        self.ShopBannerView.isHidden = true
        self.txtShopName.becomeFirstResponder()
        shopDelegate?.textediting()
    }
    
    func wrapContentData(shopName : String) {
        if shopName == "" {
            let imageName = (try? UIImage(gifName: "shop_stay_gif")) ?? UIImage()
            self.imageCat.setGifImage(imageName)
            self.ShopBannerView.isHidden = true
            self.lblShopName.text = ""
            txtShopName.isHidden = false
            self.txtShopName.placeholder = "Enter Shop Name/Name".localized
            self.txtShopName.selectedTitle = "SHOP NAME/NAME".localized
            self.txtShopName.text = ""
            self.txtShopName.becomeFirstResponder()
        }
        else {
            let imageName = (try? UIImage(imageName: "shop")) ?? UIImage()
            self.imageCat.setImage(imageName)
            txtShopName.isHidden = true
            self.ShopBannerView.isHidden = false
            self.lblShopName.text = shopName
        }
    }
}

extension ShopNameCell : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.ShopBannerView.isHidden = true
        let imageName = (try? UIImage(gifName: "shop_stay_gif")) ?? UIImage()
        self.imageCat.setGifImage(imageName)
        self.txtShopName.selectedTitle = "SHOP NAME/NAME".localized
        shopDelegate?.textediting()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField.text != "" {
            textField.isHidden = true
            self.lblShopName.text = textField.text
            let imageName = (try? UIImage(imageName: "shop")) ?? UIImage()
            self.imageCat.setImage(imageName)
            self.ShopBannerView.frame = CGRect(x: 0.0, y: self.ShopBannerView.frame.origin.y , width: self.ShopBannerView.frame.width, height: self.ShopBannerView.frame.height)
            self.shopDelegate?.setTheshopName(shopName: textField.text ?? "")
            UIView.transition(with: self.ShopBannerView, duration:1.3, options: [UIView.AnimationOptions.curveEaseOut], animations: {
                self.ShopBannerView.frame = CGRect(x: (self.gradView.frame.size.width / 2) - (self.ShopBannerView.frame.width / 2) , y: self.ShopBannerView.frame.origin.y , width: self.ShopBannerView.frame.width, height: self.ShopBannerView.frame.height)
                self.ShopBannerView.isHidden = false
             }, completion: { finished in
                                        
            })
        }
    }
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        if range.location == 0 && string == " " {
            return false
        }
        if (updatedString?.count ?? 0) > 40 {
            return false
        } else {
            if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), lang == "en"{
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace != -92 {
                let strTxt : Character = Character(string)
                if textField.text?.last == strTxt {
                    if textField.text?.count ?? 0 >= 2 {
                        if let index1 = textField.text?.index((textField.text?.startIndex)!, offsetBy: range.location - 2)
                        {//will call succ 2 times
                            if let lastChar: Character = (textField.text?[index1]) //now we can index!
                            {
                                if lastChar == strTxt
                                {
                                    return false
                                }
                                
                            }
                        }
                    }
                }
            }
            }
        }
        
        if textField.text?.last == " " && string == " " {
            return false
        }
        if let isEmoji = updatedString?.containsEmoji , isEmoji == true {
            return false
        }
        return true
    }
    
}


