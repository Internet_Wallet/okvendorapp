//
//  CategoryCollectionViewCell.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 27/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit



class CategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleName : UILabel! {
        didSet {
            titleName.text = titleName.text?.localized
        }
    }
    @IBOutlet weak var buttonInfoSelection : UIButton!
    @IBOutlet weak var imageCircle : UIImageView!
    @IBOutlet weak var imgGif : UIImageView!
    @IBOutlet weak var buttonSelection : UIButton! {
        didSet {
            buttonSelection.setTitle("".localized, for: .normal)
        }
    }
    @IBOutlet weak var gradView : Gradient!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.buttonSelection.setImage(UIImage(named: "tick_green"), for: .normal)
    }

    func wrapCelldata( name : String , image : String, isSelctedVal : Bool) {
        
        if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), lang == "en"{
            self.titleName.font = UIFont.boldSystemFont(ofSize: 18)
           
        } else {
            self.titleName.font =  UIFont(name: appFont, size: 18)
            
        }
     
        self.titleName.text = name.localized
        let imageName = (try? UIImage(gifName: image)) ?? UIImage()
        self.imgGif.setGifImage(imageName)
        self.buttonSelection.isHidden = true
        self.imageCircle.isHidden = false
        gradView.startColor = UIColor.white
        gradView.endColor = UIColor.white
        if isSelctedVal {
            self.buttonSelection.isHidden = false
            self.imageCircle.isHidden = true
            gradView.startColor = UIColor(red: (250.0/255.0), green: (3.0/255.0), blue: (3.0/255.0), alpha: 1.0)
            gradView.endColor = UIColor(red: (246.0/255.0), green: (130.0/255.0), blue: (37.0/255.0), alpha: 1.0)
        }
    }
    
}


