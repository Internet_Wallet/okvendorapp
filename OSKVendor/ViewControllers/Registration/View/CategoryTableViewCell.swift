//
//  CategoryTableViewCell.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 23/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

protocol marketTypeDelegate {
    
    func setMarketType(isWholeSaleMarket : Bool , marketType : Int , isMarketSelected : Bool)
    func setTheFormType( form : Int)
}

protocol InfoSelectionDelegate {
    func setInfoSelection(message : String)
}

class CategoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView : UICollectionView!
    
    var delegateMarket : marketTypeDelegate?
    var delegateInfo : InfoSelectionDelegate?

    var selectedArray : [Bool]!
    
    var isMarket : Bool = false
    var isWholeSaleMarket : Bool = false
    var marketTypeId = 1
    
    var collectionData = ["Shop in the market / Shopping centre", "Outside Market and Shop","Whole Sale Market","Personal Online Sales","Clinic & Pharmancy","Restaurant"]
    var imageData = ["A-Shop-in-Market", "A.-Outside-Market","A.-Whole-Sale-Market","A-Personal-Online-Shop","A.-Clinic","A.-Res"]
    var playSoundName = ["shop_and_shopping_center","outside_shop_and_market","wholesale_market","personal_onlineshopping","clinic_and_pharmacy","restaurant"]

    override func awakeFromNib() {
        super.awakeFromNib()
    
        self.collectionView.register(UINib(nibName: "CategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "categoryCollectionViewCell")
        self.collectionView.reloadData()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}

extension CategoryTableViewCell : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout , MapAlertDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCollectionViewCell", for: indexPath) as! CategoryCollectionViewCell
           let name = self.collectionData[indexPath.row]
           let imageName = self.imageData[indexPath.row]
           cell.buttonSelection.tag = indexPath.row
           cell.buttonSelection.addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
            cell.buttonInfoSelection.tag = indexPath.row
            cell.buttonInfoSelection.addTarget(self, action: #selector(buttonInfoClicked), for: .touchUpInside)
           cell.wrapCelldata(name: name, image: imageName, isSelctedVal : selectedArray[indexPath.row])
           return cell
       }
       
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                
        if indexPath.row == 2 {
            
            
            self.updateTheFormType(indexValue: 2)
            MediaPlayer.play(fileName: self.playSoundName[2], type: "m4a")
            DispatchQueue.main.async {
                if self.selectedArray[0] || self.selectedArray[1] {
                    self.updateUI(indexValue: 2)
                }
                else {
                    let sb = UIStoryboard(name: "Login", bundle: nil)
                    let mapViewController  = sb.instantiateViewController(withIdentifier: "MapAlertViewController") as! MapAlertViewController
                    mapViewController.strHeader = "OSK Vendor".localized
                    mapViewController.strDescription = "Where is your shop".localized
                    mapViewController.strBtn1Title = "Outside Market".localized
                    mapViewController.strBtn2Title = "Inside Market".localized
                    mapViewController.modalPresentationStyle = .overCurrentContext
                    let screenRect = UIScreen.main.bounds
                    let screenWidth = screenRect.size.width
                    let screenHeight = screenRect.size.height
                    mapViewController.view.frame = .init(x: 0, y: 0, width: screenWidth , height:screenHeight )
                    mapViewController.delegate = self
                    UIApplication.topViewController()?.present(mapViewController, animated: false, completion: nil)
                }
              
            }
         }
        else {
            self.updateUI(indexValue: indexPath.row)
            self.updateTheFormType(indexValue: indexPath.row)
            MediaPlayer.play(fileName: self.playSoundName[indexPath.row], type: "m4a")
        }
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let itemWidth = HomeConstant.getItemWidth(boundWidth: collectionView.bounds.size.width)
        if indexPath.row == 0 || indexPath.row == 1 {
            return CGSize(width: itemWidth  , height: itemWidth + 90)
        }
           return CGSize(width: itemWidth  , height: itemWidth + 90)
       }
    
    @objc func buttonInfoClicked(sender : UIButton) {
        self.delegateInfo?.setInfoSelection(message: collectionData[sender.tag])
    }
    
        @objc func buttonClicked(sender : UIButton) {
            
            self.updateUI(indexValue: sender.tag)
            self.updateTheFormType(indexValue: sender.tag)
        }
    
    func updateUI(indexValue : Int) {
        
        for ( indexA , _ ) in selectedArray.enumerated() {
            if (indexValue > 2) {
                if indexValue == indexA {
                    if selectedArray[indexA] {
                        selectedArray[indexA] = false
                        isMarket = false
                    }
                    else {
                        selectedArray[indexA] = true
                        isMarket = true
                        marketTypeId = 3
                        isWholeSaleMarket = false
                    }
                }
                else {
                    selectedArray[indexA] = false
                }
            }
            else {
                if indexValue == 2 {
                    if indexA == 0 || indexA == 1 {
                        if selectedArray[indexA] {
                            isWholeSaleMarket = true
                            selectedArray[indexA] = true
                        }
                    }
                    else  if (indexValue == indexA){
                        if selectedArray[indexA] {
                            selectedArray[indexA] = false
                            isMarket = false
                        }
                        else {
                            selectedArray[indexA] = true
                            isMarket = true
                        }
                    }
                    else {
                        selectedArray[indexA] = false
                    }
                }
                else {
                    if indexValue == indexA {
                        if selectedArray[indexA] {
                            selectedArray[indexA] = false
                            isMarket = false
                        }
                        else {
                            marketTypeId = indexA + 1
                            selectedArray[indexA] = true
                            isMarket = true
                        }
                    }
                    else {
                        selectedArray[indexA] = false
                    }
                }
            }
            DispatchQueue.main.async  {
                self.collectionView.reloadItems(at: [IndexPath(row: indexA, section: 0)])
             }
         }
        print(" wholesale Market : \(self.isWholeSaleMarket) and MarketType :\(self.marketTypeId) ismarket : \(self.isMarket)")
        self.delegateMarket?.setMarketType(isWholeSaleMarket: self.isWholeSaleMarket, marketType: self.marketTypeId , isMarketSelected: self.isMarket)
    }
    
    func updateTheFormType(indexValue : Int) {
        var formType = 0
        switch indexValue {
        case 0:
            formType = 1
        case 1:
            formType = 0
        case 3:
            formType = 2
        case 4:
            formType = 2
        case 5:
            formType = 0
        default:
            if indexValue == 2 {
                if selectedArray[0] == true {
                    formType = 1
                }
                else {
                    formType = 0
                }
            }
        }
        self.delegateMarket?.setTheFormType(form: formType)
    }
    
    
    func selectedOption(otpNumber: String) {
        if otpNumber == "Outside Market" {
            if selectedArray[1] != true {
                self.updateUI(indexValue: 1)
            }
            self.updateTheFormType(indexValue: 1)
            MediaPlayer.play(fileName: self.playSoundName[1], type: "m4a")
            
        }
        if otpNumber == "Inside Market" {
            if selectedArray[0] != true {
                self.updateUI(indexValue: 0)
            }
            self.updateTheFormType(indexValue: 0)
            MediaPlayer.play(fileName: self.playSoundName[0], type: "m4a")
        }
        self.updateUI(indexValue: 2)
    }
}
