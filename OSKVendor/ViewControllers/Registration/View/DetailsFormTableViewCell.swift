//
//  DetailsFormTableViewCell.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 22/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol RegisterVendor {
    func vendorRegister ( address:String , mobileNumber : String , marketid : Int , facebookID : String)
    func showAlert (message : String)
    func callMarketListShow( index: Int , frame : UIButton)
}

class DetailsFormTableViewCell: UITableViewCell,PhValidationProtocol {
    
//    lazy var marketListView : UIView = {
//        
//        
//    }
    
    var registerDelegate : RegisterVendor?
    var marketList : [[String : Any]]?
    var isTermsAccepted : Bool  = false
    
    var marketid = -10
    var isMarket : Bool = false
    private let mobileNumberAcceptedChars = "0123456789"
    
    @IBOutlet weak var merketView : UIView!
    @IBOutlet weak var addresstView : UIView!
    @IBOutlet weak var facebookView : UIView!
    @IBOutlet weak var numberView : UIView!
    
    @IBOutlet weak var btnCheckUncheck : UIButton!
    @IBOutlet weak var btnMarketSelect : UIButton!
    @IBOutlet weak var downImage : UIImageView!
    
//    {
//        didSet{
//            btnMarketSelect.setTitle("Select market name".localized, for: .normal)
//        }
//    }
    
    @IBOutlet var btnaboutApp: UIButton!{
        didSet{
            btnaboutApp.setTitle("How did you hear about our App?".localized, for: .normal)
        }
    }
    
    @IBOutlet weak var btnRegister : UIButton!{
        didSet{
            btnRegister.setTitle("REGISTER".localized, for: .normal)
        }
    }
    @IBOutlet weak var btnAddressSelect : UIButton!
    
    @IBOutlet weak var gradViewHeight : NSLayoutConstraint!
    
    @IBOutlet weak var txtAddressField : UITextField!{
        didSet {
        self.txtAddressField.placeholder = self.txtAddressField.placeholder?.localized
    }
    }
    @IBOutlet weak var txtFacebookField : UITextField!{
        didSet {
        self.txtFacebookField.placeholder = self.txtFacebookField.placeholder?.localized
    }
    }
    @IBOutlet weak var txtNumberField :SkyFloatingLabelTextField!{
        didSet {
        self.txtNumberField.placeholder = self.txtNumberField.placeholder?.localized
      }
    }
    @IBOutlet var termsandcondtionsLabel: UILabel!{
        didSet{
            termsandcondtionsLabel.text = "I agree with Terms and Conditions".localized
        }
    }
    
    let validObj = PayToValidations.init()
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
       // btnMarketSelect.setTitle("Select market name".localized, for: .normal)
        // Initialization code
        phNumValidationsFile = getDataFromJSONFile() ?? []
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.btnMarketSelect.layer.cornerRadius = 10
        self.btnMarketSelect.layer.borderWidth = 2
        self.btnMarketSelect.layer.borderColor = UIColor.gray.cgColor
        
        // Configure the view for the selected state
    }
    
    
    
    
    @IBAction func openMarket  (_ sender : UIButton) {
        
        // here show the contact suggession screen
        self.registerDelegate?.callMarketListShow(index: sender.tag , frame : sender)
    }
    
    
    @IBAction func termsAndConditionAction  (_ sender : UIButton) {
        
        if isTermsAccepted {
            isTermsAccepted = false
            self.btnCheckUncheck.setImage(UIImage(named: "uncheck-black"), for: .normal)
            
        }
        else {
            isTermsAccepted = true
            self.btnCheckUncheck.setImage(UIImage(named: "check-black"), for: .normal)
        }
    }
    
    
    func WrapContentData( formType : Int) {
        if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), lang == "en"{
            self.btnaboutApp.titleLabel?.font =  UIFont(name: appFont, size: 18)
            self.btnaboutApp.setTitle("Where did you hear about our app?".localized, for: .normal)
        } else {
            self.btnaboutApp.titleLabel?.font =  UIFont(name: appFont, size: 18)
            self.btnaboutApp.setTitle("Where did you hear about our app?".localized, for: .normal)
        }
        self.btnRegister.setTitle("REGISTER".localized, for: .normal)
        self.txtAddressField.placeholder = "Enter Address *".localized
        self.txtFacebookField.placeholder = "Enter Your Facebook Id".localized
        self.txtNumberField.placeholder = self.txtNumberField.placeholder?.localized
     //   btnMarketSelect.setTitle("Select market name".localized, for: .normal)
        self.btnaboutApp.layer.cornerRadius = 8.0
        self.btnaboutApp.layer.masksToBounds = true
        self.btnaboutApp.layer.borderWidth = 1.0
        self.btnaboutApp.layer.borderColor = UIColor.white.cgColor
        
        
        switch formType {
        
        case 0:
            self.merketView.isHidden = true
            isMarket = false
            self.facebookView.isHidden = false
            self.addresstView.isHidden = false
            self.numberView.isHidden = false
            self.gradViewHeight.constant = (52*3)+10
        
        case 1:
            self.merketView.isHidden = false
            isMarket = true
            self.facebookView.isHidden = true
            self.addresstView.isHidden = false
            self.numberView.isHidden = false
            self.gradViewHeight.constant = (52*3)+10
        case 2:
            self.merketView.isHidden = true
            isMarket = false
            self.facebookView.isHidden = false
            self.addresstView.isHidden = false
            self.numberView.isHidden = false
            self.gradViewHeight.constant = (52*3)+10
        default:
            self.merketView.isHidden = true
            isMarket = false
            self.facebookView.isHidden = true
            self.addresstView.isHidden = false
            self.numberView.isHidden = false
            self.gradViewHeight.constant = (52*2)+10
        }
    }

    @IBAction func registerAVendor(sender : UIButton) {
    
        if txtAddressField.text == "" || txtAddressField.text == " " || (txtAddressField.text?.count ?? 0) == 0 {
            self.registerDelegate?.showAlert(message: "Please enter address.".localized)
            return
        }
        else if txtNumberField.text == "09" {
            self.registerDelegate?.showAlert(message: "Please enter correct mobile no.".localized)
            return
        }
        else if (marketid == -10) && isMarket {
            self.registerDelegate?.showAlert(message: "Please select Market.".localized)
            return
        }
        else {
            if marketid == -10 {
                marketid  = 0
            }
            self.registerDelegate?.vendorRegister(address: txtAddressField.text ?? "", mobileNumber: txtNumberField.text ?? "", marketid: marketid , facebookID: txtFacebookField.text ?? "")
        }
    }
    
    
}

extension DetailsFormTableViewCell : UITextFieldDelegate {
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtNumberField{
             if let text = textField.text, text.count < 3 { 
                    textField.text = "09"
                }
        }
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtNumberField {
            
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return false }
            if (range.location == 0 && range.length == 0) || (range.location == 1 && range.length == 0)
             {
                 return false
             }
             
             if range.location == 0 && range.length > 1 {
                 textField.text = "09"
              //   self.clearButton.isHidden = true
                 return false
             }
             
             let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
             let textCount  = text.count
             if textCount > 2 {
                // self.clearButton.isHidden = false
             } else {
                // self.clearButton.isHidden = true
             }
             
             let object = validObj.getNumberRangeValidation(text)
             // let validCount = object.min
             
             let  char = string.cString(using: String.Encoding.utf8)!
             let isBackSpace = strcmp(char, "\\b")
             
             if (isBackSpace == -92) {
                 if textField.text == "09" {
                     textField.text = "09"
                  //   self.clearButton.isHidden = true
                     return false
                 }
             }
             
             if object.isRejected == true {
                 textField.text = "09"
               //  self.clearButton.isHidden = true
                 textField.resignFirstResponder()
                 let text = "Invalid Mobile Number".localized
                self.registerDelegate?.showAlert(message: text)
                
                 return false
             }
             if textCount < object.min {
               //  self.loginButton.isEnabled = false
               //  self.loginButton.backgroundColor = .lightGray
             } else if (textCount >= object.min && textCount < object.max) {
                // self.loginButton.isEnabled = true
               //  loginButton.setTitleColor(.white, for: .normal)
               //  self.loginButton.backgroundColor = .orange
             } else if (textCount == object.max) {
                 textField.text = text
               //  loginButton.setTitleColor(.white, for: .normal)
               //  self.loginButton.backgroundColor = .orange
               //  self.loginButton.sendActions(for: .touchUpInside)
               //  self.loginButton.isEnabled = true
                 self.txtNumberField.resignFirstResponder()
                 return false
             } else if textCount > object.max {
                 return false
             }
            
//            if range.location == 0, string == " " {
//                return false
//            }
//
//            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
//            let rangeCheck = PayToValidations().getNumberRangeValidation(text)
//            let textCount  = text.count
//
//            if rangeCheck.isRejected {
//                textField.text = "09"
//                return false
//            }
//
//            if let char = string.cString(using: String.Encoding.utf8) {
//                let isBackSpace = strcmp(char, "\\b")
//                if (isBackSpace == -92) {
//                    textField.text = " 09"
//                   // self.clearButton.isHidden = true
//                    textField.resignFirstResponder()
//                    return false
//                }
//            }
//
//                let chars = textField.text! + string;
//                let number = checkValidNumber(number: chars)
//                if number.isRejected {
//                    println_debug("Rejected number")
//                    return false
//                }else if number.max == textCount {
//                    if textField.text!.count <= number.max  {
//                        textField.text = text
//                    }
//                    return false
//                }else if textCount > number.max {
//                    return false
//                }
//            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return false }
//            let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptedChars).inverted
//            let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
//            if string != filteredSet { return false }
//            if text.count >= rangeCheck.min {
//
//            }
//            if text.count == rangeCheck.max {
//
//                textField.text = text
//                return false
//
//            } else if text.count > rangeCheck.max {
//
//                return false
//            }
       }
        if textField == txtAddressField {
            if range.location == 0 && string == " " {
                 return false
            }
        }
        return true
    }
    
}


class SignInCell: UITableViewCell {

    @IBOutlet weak var signInButton : UIButton!{
        didSet{
            signInButton.setTitle("Already having account? Sign In".localized, for: .normal)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
       // signInButton.titleLabel?.numberOfLines = 0
       // signInButton.titleLabel?.lineBreakMode = .byWordWrapping
        // Initialization code
        
        if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), lang == "en"{
            self.signInButton.titleLabel?.font =  UIFont(name: appFont, size: 18)
            self.signInButton.setTitle("Already having account? Sign In".localized, for: .normal)
        } else {
            self.signInButton.titleLabel?.font =  UIFont(name: appFont, size: 18)
            self.signInButton.setTitle("Already having account? Sign In".localized, for: .normal)
        }
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
