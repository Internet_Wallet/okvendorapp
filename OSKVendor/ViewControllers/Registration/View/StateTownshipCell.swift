//
//  StateTownshipCell.swift
//  OSKVendor
//
//  Created by OK$ on 21/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class StateTownshipCell: UITableViewCell {
    
    @IBOutlet weak var imageState : UIImageView!
    @IBOutlet weak var stateTownshipViewHeight : NSLayoutConstraint!
    @IBOutlet weak var stateTownshipButton : UIButton!{
        didSet{
            stateTownshipButton.setTitle("Select Division/State and Township".localized, for: .normal)
        }
    }
    
    @IBOutlet weak var lblStateTitle : UILabel!{
        didSet{
            lblStateTitle.text = "State / Division".localized
        }
    }
    @IBOutlet weak var lblTownshipTitle : UILabel!{
        didSet{
            lblTownshipTitle.text = "Township".localized
        }
    }
    @IBOutlet weak var lblStateValue : UILabel!
    @IBOutlet weak var lblTownshipValue : UILabel!
    
    @IBOutlet weak var divisionView : UIView!
    @IBOutlet weak var townshipView : UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        let imageName = (try? UIImage(gifName: "AddressA")) ?? UIImage()
        self.imageState.setGifImage(imageName)
        
        self.lblStateTitle.text = "State / Division".localized
        self.lblTownshipTitle.text = "Township".localized
        self.stateTownshipButton.setTitle("Select Division/State and Township".localized, for: .normal)
        // Configure the view for the selected state
    }
    
    
}
