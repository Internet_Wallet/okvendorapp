//
//  ShopOpenViewController.swift
//  OSKVendor
//
//  Created by Sam' MacBook on 28/10/20.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import AVKit
import UIKit

class ShopOpenViewController: UIViewController {
    
    
    @IBOutlet weak var lblShopName:UILabel!
    @IBOutlet weak var ribbinCuttingImageView : UIImageView!
    
    var END_TIME = 2.0
    private var audioPlayer: AVAudioPlayer?
    
    var shopName : String?
    var timer : Timer?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.lblShopName.text = self.shopName
        let selectedLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String
        if selectedLanguage == "en" {
            let imageName = (try? UIImage(gifName: "OPEN_english")) ?? UIImage()
            self.ribbinCuttingImageView.setGifImage(imageName)
        }
        else {
            let imageName = (try? UIImage(gifName: "OPEN_english")) ?? UIImage()
            self.ribbinCuttingImageView.setGifImage(imageName)
        }
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(checkTime), userInfo: nil, repeats: true)
        let alertSound = URL(fileURLWithPath: Bundle.main.path(forResource: "clapping", ofType: "mp3")!)
                print(alertSound)
                try! AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
                try! AVAudioSession.sharedInstance().setActive(true)

                try! audioPlayer = AVAudioPlayer(contentsOf: alertSound)
                audioPlayer!.prepareToPlay()
                audioPlayer!.play()
        }
    
    @objc func checkTime() {
        if self.audioPlayer?.currentTime ?? 0 >= END_TIME {
            self.audioPlayer?.stop()
            timer?.invalidate()
            let delayTime = DispatchTime.now() + 0.3
            DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
              let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
              let viewController = mainStoryboard.instantiateViewController(withIdentifier: "KYDrawerController_id") as! KYDrawerController
              UIApplication.shared.keyWindow?.rootViewController = viewController
              self.performSegue(withIdentifier: "registrationComplete", sender: self)
            })
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

}
