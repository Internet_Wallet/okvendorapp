//
//  MobileNumberTableViewCell.swift
//  Covid Zay Vendor
//
//  Created by iMac on 27/02/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class MobileNumberTableViewCell: UITableViewCell,PhValidationProtocol {
    
    @IBOutlet var mobilenumTF: UITextField!
    private let mobileNumberAcceptedChars = "0123456789"
    
    @IBOutlet weak var mobilenumView: UIView!
    
    let validObj = PayToValidations.init()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mobilenumTF.delegate = self
        phNumValidationsFile = getDataFromJSONFile() ?? []
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension MobileNumberTableViewCell : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == mobilenumTF{
             if let text = textField.text, text.count < 3 {
                    textField.text = "09"
                }
        }
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      //  self.loginbutton.isUserInteractionEnabled = true
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return false
        }
       
            if range.location <= 1 {
                let newPosition = textField.endOfDocument
                textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                return false
            }
       
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return false }
      
            if (range.location == 0 && range.length == 0) || (range.location == 1 && range.length == 0)
            {
                return false
            }
            
            if range.location == 0 && range.length > 1 {
                textField.text = "09"
               // self.clearButton.isHidden = true
                return false
            }
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
            if textCount > 2 {
               // self.clearButton.isHidden = false
            } else {
              //  self.clearButton.isHidden = true
            }
            
            let object = validObj.getNumberRangeValidation(text)
            // let validCount = object.min
            print("@@@@",object)
            
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            if (isBackSpace == -92) {
                if textField.text == "09" {
                    textField.text = "09"
                   // self.clearButton.isHidden = true
                    return false
                }
            }
            
            if object.isRejected == true {
                textField.text = "09"
             //   self.clearButton.isHidden = true
                textField.resignFirstResponder()
                
                let text = "Invalid Mobile Number".localized
                
        
                return false
            }
            if textCount < object.min {
//                self.loginbutton.isEnabled = false
//                self.loginbutton.backgroundColor = .lightGray
            } else if (textCount >= object.min && textCount < object.max) {
               // self.loginbutton.isEnabled = true
               // loginbutton.setTitleColor(.white, for: .normal)
              //  self.loginbutton.backgroundColor = themeColor
            } else if (textCount == object.max) {
                textField.text = text
//                loginbutton.setTitleColor(.white, for: .normal)
//                self.loginbutton.backgroundColor = themeColor
//               // self.loginbutton.sendActions(for: .touchUpInside)
//                self.loginbutton.isEnabled = true
                self.mobilenumTF.resignFirstResponder()
                return false
            } else if textCount > object.max {
                return false
            }
     
        return true
        
    }
    
}
