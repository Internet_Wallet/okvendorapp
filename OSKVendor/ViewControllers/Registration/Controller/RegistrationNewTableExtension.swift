//
//  RegistrationNewTableExtension.swift
//  OSKVendor
//
//  Created by OK$ on 21/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit


//MARK:- Table View Delegate
extension RegistrationNewVC : UITableViewDataSource, UITableViewDelegate {
    
    
    
    
    @objc func callaction(_ sender:UIButton){
        
        if let marketdetails = self.marketList[sender.tag] as? [String : Any] {
         if let cell = tblList.cellForRow(at: IndexPath(row: 1, section: 1)) as? DetailsFormTableViewCell {
             cell.btnMarketSelect.setTitle( marketdetails["BusinessName"] as? String, for: .normal)
             cell.marketid = marketdetails["BusinessId"] as? Int ?? -10
         }
         self.MarketID = marketdetails["BusinessId"] as? Int ?? -10
         isMarketListHidden = false
         marketPlaceView.removeFromSuperview()
         
        }
        tblList.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblMarketList {
            return 1
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblMarketList {
            return self.marketList.count
        }
        else {
            if section == 2 {
                return 1
            }
            return 2
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblMarketList {
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoryListCell", for: indexPath) as! CategoryListCell
            if let catData = self.marketList[indexPath.row] as? [String : Any]{
                //cell.lblTitle.text = catData["BusinessName"] as? String
               // cell.lblTitle.backgroundColor = .red
              //  cell.backgroundColor = .red
                cell.marketSelectButton.tag = indexPath.row
                cell.marketSelectButton.addTarget(self, action: #selector(callaction(_:)), for: .touchUpInside)
            }
            cell.selectionStyle = .none
            return cell
        }
        else {
            if indexPath.section == 0 {
                if indexPath.row == 0 {
                    let cellShopName = tableView.dequeueReusableCell(withIdentifier: "ShopNameCell", for: indexPath) as! ShopNameCell
                    cellShopName.shopDelegate = self
                    cellShopName.btnInfo.addTarget(self, action: #selector(btnInfoAction), for: .touchUpInside)
                    cellShopName.wrapContentData(shopName: self.shopName)
                    return cellShopName
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "StateTownshipCell", for: indexPath) as! StateTownshipCell
                    cell.stateTownshipButton.addTarget(self, action: #selector(navigateToStateTownshipScreen), for: .touchUpInside)
                    
                        cell.imageState.isUserInteractionEnabled = true

                        let tap = UITapGestureRecognizer(target: self, action: #selector(navigateToStateTownshipScreen))
                        cell.imageState.addGestureRecognizer(tap)
                    
                    
                    let tap1 = UITapGestureRecognizer(target: self, action: #selector(navigateToStateTownshipScreen))
                    cell.divisionView.addGestureRecognizer(tap1)
                    
                    let tap2 = UITapGestureRecognizer(target: self, action: #selector(navigateToStateTownshipScreen))
                    cell.townshipView.addGestureRecognizer(tap2)
                    
                    
                    
                    if self.townshipObject.name != "" {
                        cell.lblStateValue.text = self.selectedState.name
                        cell.lblTownshipValue.text = self.townshipObject.name
                        cell.divisionView.isHidden = false
                        cell.townshipView.isHidden = false
                        cell.stateTownshipViewHeight.constant = 90
                    }
                    else {
                        cell.lblStateValue.text = ""
                        cell.lblTownshipValue.text = ""
                        cell.divisionView.isHidden = true
                        cell.townshipView.isHidden = true
                        cell.stateTownshipViewHeight.constant = 0
                    }
                    return cell
                }
            }
            else if indexPath.section == 1 {
                if indexPath.row == 0 {
                    let categoryCell = tableView.dequeueReusableCell(withIdentifier: "categoryTableViewCell", for: indexPath) as! CategoryTableViewCell
                    categoryCell.selectedArray = self.selctionArray
                    categoryCell.delegateMarket = self
                    categoryCell.delegateInfo = self
                    categoryCell.collectionView.reloadData()
                    return categoryCell
                }
                else {
                    let detailFormCell = tableView.dequeueReusableCell(withIdentifier: "detailsFormTableViewCell", for: indexPath) as! DetailsFormTableViewCell
                    if marketLabel == "Select market name".localized{
                        detailFormCell.downImage.image = UIImage(named: "downarrowblack")
                    }else{
                        detailFormCell.downImage.image = UIImage(named: "uparrow")
                    }
                    
                    detailFormCell.btnMarketSelect.setTitle(marketLabel, for: .normal)
                    detailFormCell.btnAddressSelect.addTarget(self, action: #selector(navigateToMapScreen), for: .touchUpInside)
                    detailFormCell.btnMarketSelect.tag = indexPath.row
                    detailFormCell.btnaboutApp.addTarget(self, action: #selector(navigateTodetailScreen), for: .touchUpInside)
                    detailFormCell.registerDelegate = self
                    detailFormCell.marketList = self.marketList
                    detailFormCell.WrapContentData(formType: self.FortmType)
                    return detailFormCell
                }
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SignInCell", for: indexPath) as! SignInCell
                cell.signInButton.addTarget(self, action: #selector(navigateToLoginScreen), for: .touchUpInside)
                cell.signInButton.setTitle("Already having account? Sign In".localized, for: .normal)
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblMarketList {
            return 55
        }
        else {
            if indexPath.section == 0 {
               if indexPath.row == 0 {
                    return 200
                }
                else {
                    return UITableView.automaticDimension
                }
            }
            else if indexPath.section == 1 {
                if indexPath.row == 0 {
                    if self.selectedState.1 != "" {
                        return 900
                    }
                    else {
                        return 0
                    }
                }
                else {
                    if self.isMarket {
                        return UITableView.automaticDimension
                    }
                    else {
                        return 0
                    }
                }
            }
            else {
                return 65
            }
        }
     }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        if tableView == tblMarketList {
            
           if let marketdetails = self.marketList[indexPath.row] as? [String : Any] {
            
            if let cell = tblList.cellForRow(at: IndexPath(row: 1, section: 1)) as? DetailsFormTableViewCell {
                cell.btnMarketSelect.setTitle( marketdetails["BusinessName"] as? String, for: .normal)
                cell.marketid = marketdetails["BusinessId"] as? Int ?? -10
            }
            self.MarketID = marketdetails["BusinessId"] as? Int ?? -10
            isMarketListHidden = false
            marketPlaceView.removeFromSuperview()
            
           }
        }
        else {
            isMarketListHidden = false
            marketPlaceView.isHidden = true
            marketPlaceView.removeFromSuperview()
            
            if indexPath.section == 0 && indexPath.row == 1 {
                self.navigateToStateTownshipScreen()
            }
        }
      }

    @objc func navigateToLoginScreen(){
        boolLang = false
        viewLangOption.isHidden = true
        let storyBorad = UIStoryboard(name: "Login", bundle: nil)
        if #available(iOS 13.0, *) {
            if let obj = storyBorad.instantiateViewController(identifier: "LoginViewController" ) as? LoginViewController{
                obj.delegate = self
                self.navigationController?.pushViewController(obj, animated: true)
            }
        } else {
            if let ViewController = storyBorad.instantiateViewController(withIdentifier: "LoginViewController" ) as? LoginViewController {
                ViewController.delegate = self
                self.navigationController?.pushViewController(ViewController, animated: true)
            }
        }
    }
    
    @objc func navigateToMapScreen(){
        showMapScreen()
    }
    
    @objc func btnInfoAction(){
        self.showAlert(message: "Enter Your Shop Name Or Your Name.".localized)
        boolLang = false
        viewLangOption.isHidden = true
    }
    
    
    @objc func navigateTodetailScreen(){
        boolLang = false
        viewLangOption.isHidden = true
        let storyBorad = UIStoryboard(name: "Login", bundle: nil)
        if #available(iOS 13.0, *) {
            if let obj = storyBorad.instantiateViewController(identifier: "AboutAppViewController" ) as? AboutAppViewController{

             self.navigationController?.pushViewController(obj, animated: true)
            }
        } else {
            if let ViewController = storyBorad.instantiateViewController(withIdentifier: "AboutAppViewController" ) as? AboutAppViewController {
                self.navigationController?.pushViewController(ViewController, animated: true)
            }
        }
    }
    
    
    @objc func navigateToStateTownshipScreen(){
        boolLang = false
        viewLangOption.isHidden = true
        if self.shopName == "" {
            DispatchQueue.main.async {
                let alertVC = SAlertController()
                alertVC.ShowSAlert(title: "Information".localized, withDescription: "Please Enter Shop Name with minimum character 3.".localized, onController: self)
                let cancelAction = SAlertAction()
                cancelAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    self.dismiss(animated: true, completion: nil)
                })
                alertVC.addAction(action: [cancelAction])
            }
        }
        else if (self.shopName.count < 3) {
            DispatchQueue.main.async {
                let alertVC = SAlertController()
                alertVC.ShowSAlert(title: "Information".localized, withDescription: "Please Enter Shop Name with minimum character 3.".localized, onController: self)
                let cancelAction = SAlertAction()
                cancelAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    self.dismiss(animated: true, completion: nil)
                })
                alertVC.addAction(action: [cancelAction])
            }
        }
        else {
            let storyBorad = UIStoryboard(name: "Login", bundle: nil)
            if #available(iOS 13.0, *) {
                if let obj = storyBorad.instantiateViewController(identifier: "StateAndTownshipVC" ) as? StateAndTownshipVC{
                 obj.countryObject = self.countryObject
                 obj.stateDivisonVCDelegate = self
                 self.navigationController?.pushViewController(obj, animated: true)
                }
            } else {
                if let ViewController = storyBorad.instantiateViewController(withIdentifier: "StateAndTownshipVC" ) as? StateAndTownshipVC {
                   ViewController.countryObject = self.countryObject
                   ViewController.stateDivisonVCDelegate = self
                    self.navigationController?.pushViewController(ViewController, animated: true)
                }
            }
         }
       }
    
    func showMapScreen() {
        boolLang = false
        viewLangOption.isHidden = true
          DispatchQueue.main.async {
              let sb = UIStoryboard(name: "Login", bundle: nil)
              let mapViewController  = sb.instantiateViewController(withIdentifier: "MapAlertViewController") as! MapAlertViewController
            mapViewController.strHeader = "Select Location".localized
            mapViewController.strDescription = "Make sure you are at Shop Location/Select your shop location.".localized
            mapViewController.strBtn1Title = "Cancel".localized
            mapViewController.strBtn2Title = "OK".localized
            mapViewController.modalPresentationStyle = .overCurrentContext
            mapViewController.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
          
            mapViewController.delegate = self
              self.present(mapViewController, animated: false, completion: nil)
          }
      }
}


extension RegistrationNewVC : shopNameDelegate , marketTypeDelegate, AddressDelegate , MapAlertDelegate , RegisterVendor , StateDivisionVCDelegate ,InfoSelectionDelegate,LoginDelegate {
    func textediting() {
        boolLang = false
        viewLangOption.isHidden = true
    }
    
    
    func langResetFromLogin( reset : Bool){
        marketPlaceView.isHidden  = true
        isMarket  = false
        self.isWholeSaleMarketSelected = false
        selectedState = (code: "", name: "")
        townshipObject = (code: "", name: "")
        shopName = ""
        
        DispatchQueue.main.async {
            self.updateGUI()
        }
    }

    func setStateAndTownship(townshipName: String, townshipId: String, stateObject: (String, String)) {
        self.selectedState.name = stateObject.1
        self.selectedState.code = stateObject.0
        self.townshipObject.name = townshipName
        self.townshipObject.code = townshipId
        self.getMarketListBasedOnTownship(townshipId: Int(townshipId) ?? 60)
        self.tblList.reloadData()
        boolLang = false
        viewLangOption.isHidden = true
    }
    
  
    func selectedOption(otpNumber: String) {
        if otpNumber == "OK" {
            DispatchQueue.main.async {
                if #available(iOS 13.0, *) {
                    let sb = UIStoryboard(name: "Login", bundle: nil)
                    if let baseObj = sb.instantiateViewController(identifier: "NewGeofenceController") as? NewGeofenceController {
                        baseObj.modalPresentationStyle = .fullScreen
                        baseObj.addressDelegate = self
                        self.present(baseObj, animated: true, completion: nil)
                        //self.navigationController?.pushViewController(baseObj, animated: true)
                    }
                } else {
                    let  homeVC = UIStoryboard(name: "Login", bundle: nil)
                    if let dashbord = homeVC.instantiateViewController(withIdentifier: "NewGeofenceController") as? NewGeofenceController {
                    dashbord.modalPresentationStyle = .fullScreen
                    dashbord.addressDelegate = self
                    self.present(dashbord, animated: true, completion: nil)
                    }
                    //self.navigationController?.pushViewController(dashbord!, animated: true)
                }
                
            }
        }
        //Play with In & Out Market
        
    }

    func setMarketType(isWholeSaleMarket: Bool, marketType: Int, isMarketSelected: Bool) {
        self.isMarket = isMarketSelected
        self.marketTypeId = marketType
        self.isWholeSaleMarketSelected = isWholeSaleMarket
        boolLang = false
        viewLangOption.isHidden = true
    }
    
    func setInfoSelection(message : String) {
        self.showAlert(message: message.localized)
        boolLang = false
        viewLangOption.isHidden = true
    }

    func setTheFormType(form: Int) {
        self.FortmType = form
        self.tblList.reloadRows(at: [IndexPath(row: 1, section: 1)], with: .automatic)
    }
    
    func setTheshopName(shopName: String) {
        self.shopName = shopName
        boolLang = false
        viewLangOption.isHidden = true
    }
    func setAddressWithLatLong( adrress : String, lat : String, long : String) {
        println_debug(adrress)
        let cell = tblList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? DetailsFormTableViewCell
        cell?.txtAddressField.text = ""
        if let adress = adrress as? String {
            cell?.txtAddressField.text = adress
        }
        self.locationLat = lat
        self.locationLong = long
    }
    
    func vendorRegister( address:String , mobileNumber : String , marketid : Int , facebookID : String) {
        self.address = address
        self.mobileNumber = mobileNumber
        self.MarketID = marketid
        self.facebookID = facebookID
      //  self.regsiterVendor()
        self.checkUserLogin()
       
    }
    
    
    func callMarketListShow(index: Int , frame : UIButton) {
       
        boolLang = false
        viewLangOption.isHidden = true
        
//        if !isMarketListHidden{
//            isMarketListHidden = true
//            let frameBTN = frame.superview?.convert(frame.frame, to: self.view)
//
//                if let frame = frameBTN{
//                    var height: CGFloat = CGFloat(Float(marketList.count) * 50.0)
//                    if height >= 200.0{
//                        height = 200.0
//                    }
//                    self.showContactSuggessionView(withFrame: CGRect(x: frame.origin.x, y: frame.origin.y - height, width: frame.size.width, height: height), andContacts: marketList )
//                }
//
//
//        }else{
//            isMarketListHidden = false
//            self.marketPlaceView.removeFromSuperview()
//        }
        
        let indexPath = IndexPath(row: 1, section: 1)
        guard let cell = self.tblList.cellForRow(at: indexPath) as? DetailsFormTableViewCell else{
            return
        }
        dropDown.anchorView = cell.btnMarketSelect
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        var businessName = [String]()
        businessName.removeAll()
        for i in 0..<marketList.count{
            businessName.append(marketList[i]["BusinessName"] as? String ?? "")
        }
        
        dropDown.dataSource = businessName
        
        
        if dropDown.isHidden{
            dropDown.show()
        }else{
            dropDown.hide()
        }
      
        
        
        
    }
    
    func showAlert(message: String) {
        DispatchQueue.main.async {
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "Information".localized, withDescription: message, onController: self)
            let cancelAction = SAlertAction()
            cancelAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                self.dismiss(animated: true, completion: nil)
            })
            alertVC.addAction(action: [cancelAction])
        }
    }
}
