//
//  RegistrationViewController.swift
//  OSKVendor
//
//  Created by vamsi on 6/29/20.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON

protocol AddressDelegate: class {
    func setTheAddress( adrress : String)
    func setAddressWithLatLong( adrress : String, lat : String, long : String)
}

extension AddressDelegate {
    func setTheAddress( adrress : String){
    }
    func setAddressWithLatLong( adrress : String, lat : String, long : String){
    }
}

class RegistrationViewController: OSKBaseViewController, PhValidationProtocol {
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var titleLabel: UILabel!{
        didSet{
            titleLabel.text = "Registration".localized
        }
    }
    @IBOutlet var backButton: UIButton!
    
    @IBOutlet var pwdShowButton: UIButton!
    @IBOutlet var repeatPWDShowButton: UIButton!
    
    @IBOutlet var registerScroll: UIScrollView!
    
    @IBOutlet var selectgenderLabel: UILabel!{
        didSet{
            selectgenderLabel.text = "Select Gender".localized
        }
    }
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var firstNameTF: SkyFloatingLabelTextField!{
        didSet {
            self.firstNameTF.font = UIFont(name: appFont, size: 15.0)
            self.firstNameTF.text = self.firstNameTF.text?.localized
            self.firstNameTF.placeholder = self.firstNameTF.placeholder?.localized
        }
    }
    
    @IBOutlet var mobilenumTF: SkyFloatingLabelTextField!{
        didSet {
            self.mobilenumTF.font = UIFont(name: appFont, size: 15.0)
            self.mobilenumTF.text = self.mobilenumTF.text?.localized
            self.mobilenumTF.placeholder = self.mobilenumTF.placeholder?.localized
        }
    }
    
    @IBOutlet var emailTF: SkyFloatingLabelTextField!{
        didSet {
            self.emailTF.font = UIFont(name: appFont, size: 15.0)
            self.emailTF.text = self.emailTF.text?.localized
            self.emailTF.placeholder = self.emailTF.placeholder?.localized
        }
    }
    
    @IBOutlet var addressLine1TF: SkyFloatingLabelTextField!{
        didSet {
            self.addressLine1TF.font = UIFont(name: appFont, size: 15.0)
            self.addressLine1TF.text = self.addressLine1TF.text?.localized
            
             self.addressLine1TF.placeholder = self.addressLine1TF.placeholder?.localized
        }
    }
    
    @IBOutlet var addressLine2TF: SkyFloatingLabelTextField!{
        didSet {
            self.addressLine2TF.font = UIFont(name: appFont, size: 15.0)
            self.addressLine2TF.text = self.addressLine2TF.text?.localized
            self.addressLine2TF.placeholder = self.addressLine2TF.placeholder?.localized
        }
    }
    
    @IBOutlet var stateTF: SkyFloatingLabelTextField!{
        didSet {
            self.stateTF.font = UIFont(name: appFont, size: 15.0)
            self.stateTF.text = self.stateTF.text?.localized
             self.stateTF.placeholder = self.stateTF.placeholder?.localized
        }
    }
    
    @IBOutlet var townshipTF: SkyFloatingLabelTextField!{
        didSet {
            self.townshipTF.font = UIFont(name: appFont, size: 15.0)
            self.townshipTF.text = self.townshipTF.text?.localized
             self.townshipTF.placeholder = self.townshipTF.placeholder?.localized
        }
    }
    
    @IBOutlet var houseTF: SkyFloatingLabelTextField!{
        didSet {
            self.houseTF.font = UIFont(name: appFont, size: 15.0)
            self.houseTF.text = self.houseTF.text?.localized
             self.houseTF.placeholder = self.houseTF.placeholder?.localized
        }
    }
    
    @IBOutlet var floorTF: SkyFloatingLabelTextField!{
        didSet {
            self.floorTF.font = UIFont(name: appFont, size: 15.0)
            self.floorTF.text = self.floorTF.text?.localized
             self.floorTF.placeholder = self.floorTF.placeholder?.localized
        }
    }
    
    @IBOutlet var roomTF: SkyFloatingLabelTextField!{
        didSet {
            self.roomTF.font = UIFont(name: appFont, size: 15.0)
            self.roomTF.text = self.roomTF.text?.localized
             self.roomTF.placeholder = self.roomTF.placeholder?.localized
        }
    }
    
    @IBOutlet var shopnameTF: SkyFloatingLabelTextField!{
        didSet {
            self.shopnameTF.font = UIFont(name: appFont, size: 15.0)
            self.shopnameTF.text = self.shopnameTF.text?.localized
             self.shopnameTF.placeholder = self.shopnameTF.placeholder?.localized
        }
    }
    
    @IBOutlet var passwordTF: SkyFloatingLabelTextField!{
        didSet {
            self.passwordTF.font = UIFont(name: appFont, size: 15.0)
            self.passwordTF.text = self.passwordTF.text?.localized
            self.passwordTF.placeholder = self.passwordTF.placeholder?.localized
        }
    }
    
    @IBOutlet var confirmPasswordTF: SkyFloatingLabelTextField!{
        didSet {
            self.confirmPasswordTF.font = UIFont(name: appFont, size: 15.0)
            self.confirmPasswordTF.text = self.confirmPasswordTF.text?.localized
            self.confirmPasswordTF.placeholder = self.confirmPasswordTF.placeholder?.localized
        }
    }

    @IBOutlet var maleButton: UIButton! {
        didSet {
            maleButton.setImage(#imageLiteral(resourceName: "checkRed"), for: .normal)
            maleButton.setTitle("Male".localized, for: .normal)
        }
    }
    @IBOutlet var femaleButton: UIButton! {
        didSet {
            femaleButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            femaleButton.setTitle("Female".localized, for: .normal)
        }
    }

    var statusMaleFemale : Bool = true
    
    @IBOutlet var registerButton: UIButton!{
        didSet{
          registerButton.setTitle("Register".localized, for: .normal)
        }
    }

    
    @IBOutlet var mapButton: UIButton!
    
    @IBOutlet var showAndHidePaasKeyButton:UIButton!
    @IBOutlet var showAndHideRepeatPaasKeyButton:UIButton!
    
    var apiManager = APIManager()
    let validObj = PayToValidations.init()
       var country       : Country?
       public var currentSelectedCountry : countrySelected = .myanmarCountry
          enum countrySelected {
              case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
          }
    
    var StatesArray = [[String : Any]]()
    var TownshipArray = [[String : Any]]()
    var pickerTextView: UITextView?
    var stateId = String()
    var cityId = String()
    var countriesAndStatesRespArr = [Any]()
    var stateDict = [String:Any]()
    var townshipDict = [String : Any]()

    var statePicker: UIPickerView!
    var TownshipPicker: UIPickerView!
    var datePicker : UIDatePicker!
    
    var township : String?
    var devision : String?
    var googleAddress : String?
    var locationLat : String?
    var locationLong : String?
    var DateOfBirth : String?
    
    var isPwdVisible : Bool?
    var isRepeatPwdVisible : Bool?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.UpdateUI()
        // Do any additional setup after loading the view.
        self.location()
        
        if stateTF.text == "" || townshipTF.text == ""{
            if AppUtility.isConnectedToNetwork(){
            apiManager.getDevisionInfoForCountry(countryID: "60", onSuccess: { (devisionDictionary) in
                if let devisionData = devisionDictionary["Data"] as? [[String:Any]] , devisionData.count > 0 {
                    self.StatesArray = devisionData
                    if let devisionD = self.StatesArray[0] as? [String : Any]  {
                        let devisionID = devisionD["id"] as? Int
                        self.stateId = "\(devisionID!)"
                        self.stateTF.text = devisionD["name"] as? String
                        self.apiManager.getTownshipInfoForDevision(devisionID: "\(devisionID!)", onSuccess: { (townshipInfo) in
                            if let townshipData = townshipInfo["Data"] as? [[String:Any]] , townshipData.count > 0 {
                                self.TownshipArray = townshipData
                                self.cityId = "\(String(describing: self.TownshipArray[0]["id"] as! Int))"
                                self.townshipTF.text = self.TownshipArray[0]["name"] as! String
                            }
                        }) { (errorX) in
                            DispatchQueue.main.async {
                                             //      self?.showAlert(alertTitle: "Login", description: "Please Check Internet Connection")
                            }
                        }
                    }
                }
            }) { (error) in
                DispatchQueue.main.async {
                    AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
                }
            }
            } else {
                AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        isPwdVisible = false
        isRepeatPwdVisible = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func UpdateUI() {
        
        statePicker = UIPickerView()
        statePicker.delegate = self
        statePicker.dataSource = self
        
        TownshipPicker = UIPickerView()
        TownshipPicker.delegate = self
        TownshipPicker.dataSource = self
        
        let imageView = UIImageView(image: UIImage(named: "downarrow"))
        imageView.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
        let paddingView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        paddingView.addSubview(imageView)
        
        
        let imageView1 = UIImageView(image: UIImage(named: "downarrow"))
        imageView1.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
        let paddingView1: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        paddingView1.addSubview(imageView1)
        
        let imgDown = UIImageView(image: UIImage(named: "downarrow"))
        imgDown.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
        let genderRightView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        genderRightView.addSubview(imgDown)
        
        //Marker
        let buttonMap = UIButton()
        buttonMap.setImage(UIImage(named: "Marker"), for: .normal)
        buttonMap.titleLabel?.textColor = UIColor.white
        buttonMap.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        buttonMap.addTarget(self, action: #selector(self.mapButtonClick(_:)), for: .touchUpInside)
        
        let paddingViewMap: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        paddingViewMap.addSubview(buttonMap)
        
        addressLine1TF.rightView = paddingViewMap
        addressLine1TF.rightViewMode = .always
        
        stateTF.rightView = paddingView
        stateTF.rightViewMode = .always
        
        townshipTF.rightView = paddingView1//UIImageView(image: UIImage(named: "downarrow"))
        townshipTF.rightViewMode = .always
        
        passwordTF.isSecureTextEntry = true
        confirmPasswordTF.isSecureTextEntry = true
        
    }
    
    func location(){
          let isPermission =  VMGeoLocationManager.shared.setUpLocationManager()
          if isPermission {
              VMGeoLocationManager.shared.startUpdateLocation()
              VMGeoLocationManager.shared.getAddressFrom(lattitude:VMGeoLocationManager.shared.currentLatitude, longitude: VMGeoLocationManager.shared.currentLongitude, language: "en")
              { (status, data) in
                  if let addressDic = data as? Dictionary<String, String> {
                      DispatchQueue.main.async {
                          print(addressDic)
                          if let street = addressDic["street"]  {
                              self.township = street
                          }
                          if let region = addressDic["region"]  {
                              self.devision = region
                          }
                          self.googleAddress = "\(addressDic)"
                          
                          self.locationLat = VMGeoLocationManager.shared.currentLatitude
                          self.locationLong = VMGeoLocationManager.shared.currentLongitude
                      }
                  }
              }
          }
          else{
              showLocationDisabledpopUp()
          }
      }
    func showLocationDisabledpopUp() {
          DispatchQueue.main.async {
              let alertVC = SAlertController()
              alertVC.ShowSAlert(title: "Location Services are disabled", withDescription: "Please enable Location Services in your Settings", onController: self)
              
              let cancelAction = SAlertAction()
              cancelAction.action(name: "Cancel", AlertType: .defualt, withComplition: {
                  self.navigationController?.dismiss(animated: true , completion: nil)
              })
              let openAction = SAlertAction()
              openAction.action(name: "Open Setting", AlertType: .defualt, withComplition: {
                  guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                      return
                  }
                  if UIApplication.shared.canOpenURL(settingsUrl) {
                      UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                          print("Settings opened: \(success)") // Prints true
                      })
                  }
              })
              
              alertVC.addAction(action: [cancelAction,openAction])
          }
      }
    
    @IBAction func genderClick(_ sender: Any) {
        if statusMaleFemale {
            statusMaleFemale = false
            maleButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            femaleButton.setImage(#imageLiteral(resourceName: "checkRed"), for: .normal)
        } else {
            statusMaleFemale = true
            maleButton.setImage(#imageLiteral(resourceName: "checkRed"), for: .normal)
            femaleButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }
    }
    
    @IBAction func backClick(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func registerClick(_ sender: Any) {
         let validate = Validation()
        
        if firstNameTF.text == ""{
            AppUtility.alert(message: "Please Enter Name".localized, title: "", controller: self)
        }
        else if shopnameTF.text == ""{
            AppUtility.alert(message: "Please Enter Shop Name".localized, title: "", controller: self)
        }
        else if mobilenumTF.text == "09" || mobilenumTF.text == "" {
            AppUtility.alert(message: "Please Enter Mobile Number".localized, title: "", controller: self)
        }
        else if mobilenumTF.text?.count ?? 0 > 2 && mobilenumTF.text?.count ?? 0 < 9  {
            AppUtility.alert(message: "Please Enter Valid Mobile Number".localized, title: "", controller: self)
        }
//        else if emailTF.text == ""{
//             AppUtility.alert(message: "Please Enter Email", title: "", controller: self)
//        }
        
        else if emailTF.text?.count ?? 0 > 0  && !validate.isEmailString(emailTF.text) {
            AppUtility.alert(message: "Enter Valid Email".localized, title: "", controller: self)
        }
        else if addressLine1TF.text == ""{
            AppUtility.alert(message: "Please Enter AddressLine1".localized, title: "", controller: self)
        }
        else if stateTF.text == ""{
            AppUtility.alert(message: "Please Select State/Division".localized, title: "", controller: self)
        }
        else if townshipTF.text == ""{
            AppUtility.alert(message: "Please Select Township".localized, title: "", controller: self)
        }
        else if passwordTF.text == ""{
            AppUtility.alert(message: "Please Enter Password".localized, title: "", controller: self)
        }
        else if passwordTF.text?.count ?? 0 < 7 {
            AppUtility.alert(message: "Please Enter Password With Minimum 8 Character".localized, title: "", controller: self)
        }
        else if confirmPasswordTF.text == ""{
            AppUtility.alert(message: "Please Enter Confirm Password".localized, title: "", controller: self)
        }
        else if passwordTF.text != confirmPasswordTF.text{
            AppUtility.alert(message: "Password and Confirm Password does not match".localized, title: "", controller: self)
        }
        else{
            //   if NetWorkCode.checkifSimAvailableInPhone(){
            if AppUtility.isConnectedToNetwork(){
//                var mobileNo = mobilenumTF.text ?? ""
//                if mobileNo.hasPrefix("09"){
//                  mobileNo = String(mobileNo.dropFirst())
//                }
           
                let GenerType = statusMaleFemale ? "M" : "F"
//            if self.GenderTF.text == "Male" {GenerType = "M"}
//            if self.GenderTF.text == "Female" {GenerType = "F"}
                
             
           let registrationRequestData:Dictionary<String,Any> = [ "Firstname": firstNameTF.text ??  "",
                                                                  "Lastname": "Vendor",
                                                                  "Email":emailTF.text ?? "",
                                                                  "Username":mobilenumTF.text ?? "",
                                                                  "Mode":"Register",
                                                                  "Gender" : GenerType,
                                                                  "DateOfBirthDay":"22",
                                                                  "DateofBirthMonth":"02",
                                                                  "DateOfBirthYear":"1991",
                                                                  "Address1":addressLine1TF.text ?? "",
                                                                  "Address2":addressLine2TF.text ?? "",
                                                                  "CityId":self.cityId,
                                                                  "StateId":self.stateId,
                                                                  "Country": "Myanmar",
                                                                  "OtherEmail":emailTF.text ?? " ",
                                                                  "MobileNumber":mobilenumTF.text ?? "",
                                                                  "Password":confirmPasswordTF.text ?? "",
                                                                  "HouseNo":houseTF.text ?? "",
                                                                  "FloorNo":floorTF.text ?? "",
                                                                  "RoomNo":roomTF.text ?? "",
                                                                  "ShopName":shopnameTF.text ?? "",
                                                                  "Latitude":locationLat ?? "0.0",
                                                                  "Longitude":locationLong ?? "0.0",
                                                                  "Token":" ",
                                                                  "DeviceID":apiManager.deviceId,
                                                                  "VersionCode":1,
                                                                  "Simid": ""
            ]
                self.callRegisterApi(addressDict: (registrationRequestData))
            }
            else{
                AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
            }
            //                }else{
            //                    AppUtility.alert(message: "Sim Not Available", title: "", controller: self)
            //                }
        }
        
    }
    
     private func callRegisterApi(addressDict: Dictionary<String, Any>?) {
         AppUtility.showLoading(self.view)
        apiManager.RegisterUser(addressDict! as [String : AnyObject], onSuccess: { [weak self] response in
                    
                    DispatchQueue.main.async{
                        if let viewLoc = self?.view {
                            AppUtility.hideLoading(viewLoc)
                        }
                        let json = JSON(response)
                        println_debug("Register Response: \(json)")
                        if let code = json["StatusCode"].int, code == 200 {
                            if let registerInfoDic  = json["Data"].dictionaryObject {
                                let vmTemoModel = VMTempModel()
                                vmTemoModel.wrapDataModel(JSON: registerInfoDic)
                                print(registerInfoDic)
                                DispatchQueue.main.async {
                                        UserDefaults.standard.set(true, forKey: AppConstants.userDefault.loginStatus)
                                        UserDefaults.standard.set(true, forKey: AppConstants.userDefault.enabledLogout)
                                        UserDefaults.standard.synchronize()
                                        let dataDict:[String: String] = ["token": appDelegate.FCMToken]
                                        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
                                        self?.registrationCompleteAction()
                                }
                            }
                            //self?.handleLogin()
                        }else {
                            let message = json["ErrorList"][0].string
                            let alert = UIAlertController(title: "OSK Vendor".localized, message: message, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: nil))
                            self?.present(alert, animated: true, completion: nil)
                        }
                    }
                    }, onError: { [weak self] message in
                        DispatchQueue.main.async {
                        if let viewLoc = self?.view {
                                AppUtility.hideLoading(viewLoc)
                            self?.showAlert(alertTitle: "Register", description: "Please try again later!".localized)
                              
                            }
                        }
                })
        
    }
    
    func registrationCompleteAction() -> Void {
        
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "KYDrawerController_id") as! KYDrawerController
                UIApplication.shared.keyWindow?.rootViewController = viewController
                self.performSegue(withIdentifier: "registrationComplete", sender: self)
    }
    
    @IBAction func signinClick(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
        
        //   self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func mapButtonClick(_ sender: Any) {
        
        DispatchQueue.main.async {
              if #available(iOS 13.0, *) {
                  let sb = UIStoryboard(name: "Login", bundle: nil)
                  if let baseObj = sb.instantiateViewController(identifier: "NewGeofenceController") as? NewGeofenceController {
                      baseObj.modalPresentationStyle = .fullScreen
                      baseObj.addressDelegate = self
                     // self.navigationController?.present(baseObj, animated: true, completion: nil)
                      self.navigationController?.pushViewController(baseObj, animated: true)
                  }
              } else {
                     let  homeVC = UIStoryboard(name: "Login", bundle: nil)
                     let dashbord = homeVC.instantiateViewController(withIdentifier: "NewGeofenceController") as? NewGeofenceController
                      dashbord?.modalPresentationStyle = .fullScreen
                      dashbord?.addressDelegate = self
                   //   self.navigationController?.present(objNav, animated: true)
                    self.navigationController?.pushViewController(dashbord!, animated: true)
                  }
                  
              }
          }
    
   // MARK:- show hide the password
    
    @IBAction func pwdvisableClick(_ sender: Any) {
        
        if isPwdVisible! {
            
                   isPwdVisible = false
                   passwordTF.isSecureTextEntry = true
                   pwdShowButton.setImage(UIImage(named: "Eyeclose"), for: .normal)
                   
                }
               else {
                  
                   isPwdVisible = true
                   passwordTF.isSecureTextEntry = false
                   pwdShowButton.setImage(UIImage(named: "Eyeopen"), for: .normal)
             }
       }
    
    @IBAction func repeatPWDvisableClick(_ sender: Any) {

        if isRepeatPwdVisible! {
            isRepeatPwdVisible = false
            confirmPasswordTF.isSecureTextEntry = true
            repeatPWDShowButton.setImage(UIImage(named: "Eyeclose"), for: .normal)
            
        }
        else {
            isRepeatPwdVisible = true
            confirmPasswordTF.isSecureTextEntry = false
            repeatPWDShowButton.setImage(UIImage(named: "Eyeopen"), for: .normal)
        }
     }
    }
    

extension RegistrationViewController :  AddressDelegate {
//    func setTheAddress( adrress:String) {
//        self.addressLine1TF.text = ""
//        if let adress = adrress as? String {
//            self.addressLine1TF.text = adress
//        }
//    }
    func setAddressWithLatLong( adrress : String, lat : String, long : String) {
        self.addressLine1TF.text = ""
        if let adress = adrress as? String {
            self.addressLine1TF.text = adress
        }
        self.locationLat = lat
        self.locationLong = long
    }
}
