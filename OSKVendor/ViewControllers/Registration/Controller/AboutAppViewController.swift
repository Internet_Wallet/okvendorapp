//
//  AboutAppViewController.swift
//  Covid Zay Vendor
//
//  Created by iMac on 22/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class AboutAppViewController: UIViewController, UITextFieldDelegate,PhValidationProtocol  {
    
    
    @IBOutlet var navigationView: UIView!
    
    @IBOutlet var navigationTitleLabel: UILabel!{
        didSet{
            navigationTitleLabel.text = "Registration".localized
        }
    }
    
    @IBOutlet var headingLabel: UILabel!{
    didSet{
        headingLabel.text = "How did you hear about our App?".localized
    }
}
    
    @IBOutlet var submitButton: UIButton!{
        didSet{
            submitButton.setTitle("SUBMIT".localized, for: .normal)
        }
    }
    @IBOutlet var lisTableView: UITableView!
    @IBOutlet var buttonBottomConstraint: NSLayoutConstraint!
    @IBOutlet var tableviewTopConstraint: NSLayoutConstraint!
    @IBOutlet var contentView : UIView!
    
    var apiManager = APIManager()
    var dataList : [[String : Any]] = [[String : Any]]()
    var marketStatus = false
    var inStatusRow = -1
    var intCheckStatus = -1
    var mobileNumber = "09"
    
    var value = ""
    var Id = 0
    
    var apiManagerClient : APIManagerClient?
    var alertVC : SAlertController?
    
    let validObj = PayToValidations.init()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true

        // Do any additional setup after loading the view.
        
     
        
//        let indexPath = IndexPath(row: 0, section: 5)
//        let cell = self.lisTableView.cellForRow(at: indexPath) as? MobileNumberTableViewCell
//
//
//
//
        submitButton.bindToKeyboard()
     //   cell?.mobilenumTF.bindToKeyboard()
        lisTableView.bindToKeyboard()
     
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardDidShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.getaboutapplist()
    }
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.popViewController(animated: false)
    }
    
    
    
    @objc func keyboardWillShow(notification: NSNotification) {

//        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//              if view.frame.origin.y == 0{
//                  let height = keyboardSize.height
//
//                  self.view.frame.origin.y += height
//
//              }
//
//          }
        tableviewTopConstraint.constant = -40
        

      }

    @objc func keyboardWillHide(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//              if view.frame.origin.y != 0 {
//                  let height = keyboardSize.height
//                  self.view.frame.origin.y -= height
//              }
//
//          }
        tableviewTopConstraint.constant = 20
        
      }
    
    @IBAction func submitButtonClick(_ sender: Any) {
        if value.localized == "marketing person".localized{
            if mobileNumber == "09" || mobileNumber == ""{
                AppUtility.alert(message: "Please enter mobile number".localized, title: "", controller: self)
            }
            else{
                registerAboutApp(id: Id, mobilenumber: mobileNumber)
            }
        }
        else if value == ""{
            AppUtility.alert(message: "Please Select any one Options".localized, title: "", controller: self)
        }
        else{
           // mobileNumber = ""
            registerAboutApp(id: Id, mobilenumber: mobileNumber)
        }
        
    }
    
    func registerAboutApp(id : Int, mobilenumber: String) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/inserthearaboutus/\(id)/\(mobilenumber)", APIManagerClient.sharedInstance.base_url) as String
           // print("Get response :\(urlStr)\n params\(params)")
            self.apiManager.AboutApppostAPIParams(id: Id, mobilenumber: mobileNumber, url: urlStr, productDetails: ["":""], onSuccess: { (dic) in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                                AppUtility.hideLoading(viewLoc)
                        }
//                    let alertVC : SAlertController?
//

                    
                    AppUtility.alert(message: "Thank You".localized, title: "", controller: self)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                        self?.navigationController?.navigationBar.isHidden = false
                        self?.navigationController?.popViewController(animated: false)
                        
                    }
                   
                    
                }
            },  onError: { (error) in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                            AppUtility.hideLoading(viewLoc)
                    }
                    AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
                }
            })
        }else {
            AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
        }
            
    }
    

    func getaboutapplist(){
        if AppUtility.isConnectedToNetwork(){
            AppUtility.showLoading(self.view)
            apiManager.getAboutAPPList { (marketDictionary) in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                                AppUtility.hideLoading(viewLoc)
                        }
                    if let marketData = marketDictionary["Data"] as? [[String:Any]] , marketData.count > 0 {
                        self.dataList = marketData
                        print(self.dataList)
                        self.lisTableView.reloadData()
                    }
                }
            } onError: { (error) in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                            AppUtility.hideLoading(viewLoc)
                    }
                    AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
                }
            }
        } else {
         AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
     }
  }
    
    
    
    
    
}

extension AboutAppViewController : UITableViewDelegate,UITableViewDataSource{
    
    @objc func BtnHeaderCLick(_ sender: UIButton) {
      
        value = dataList[sender.tag]["HearFromName"] as? String ?? ""
        if value.localized == "Marketing Person".localized{
            marketStatus = true
            inStatusRow = sender.tag
        } else {
            marketStatus = false
            inStatusRow = -1
        }
        Id = dataList[sender.tag]["Id"] as? Int ?? 0
        print(Id)
        intCheckStatus = sender.tag
        lisTableView.reloadData()
    }
    
    @objc func BtnCheckCLick(_ sender: UIButton) {
        
        value = dataList[sender.tag]["HearFromName"] as? String ?? ""
        if value.localized == "Marketing Person".localized{
            marketStatus = true
            inStatusRow = sender.tag
        } else {
            marketStatus = false
            inStatusRow = -1
        }
        Id = dataList[sender.tag]["Id"] as?  Int ?? 0
       intCheckStatus = sender.tag
        lisTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      
        let identifier = "Cell"
        let cell: optionsListTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? optionsListTableViewCell
        cell.checkButton.tag = section
        cell.dataButton.tag = section
      //  cell.dataLabel.text = dataList[section]["HearFromName"] as? String
        let value = dataList[section]["HearFromName"] as? String
        
        cell.dataButton.setTitle(value?.localized, for: .normal)
        cell.checkButton.setImage(UIImage(named: "whiteradiobutton"), for: .normal)
        if intCheckStatus == section {
            cell.checkButton.setImage(UIImage(named: "whiteradioselected"), for: .normal)
        } else {
            cell.checkButton.setImage(UIImage(named: "whiteradiobutton"), for: .normal)
        }
//        cell.btnHeader.tag = section
        cell.dataButton.addTarget(self, action: #selector(BtnHeaderCLick(_:)), for: .touchUpInside)
        cell.checkButton.addTarget(self, action: #selector(BtnCheckCLick(_:)), for: .touchUpInside)
//        cell.selectionStyle = .none
//        cell.Delegate = self
        return cell
        
        
       // return UIView()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataList.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if inStatusRow == section {
            return 1
        } else {
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! optionsListTableViewCell
        
     //   cell.dataLabel.text = dataList[indexPath.row]["HearFromName"] as? String
    //    cell.selectionStyle = .none
        
        let cellTF = tableView.dequeueReusableCell(withIdentifier: "TFCell", for: indexPath) as! MobileNumberTableViewCell
        cellTF.mobilenumTF.delegate = self
        cellTF.mobilenumTF.text = mobileNumber
        cellTF.mobilenumTF.placeholder = "Enter Mobile Number".localized
        cellTF.mobilenumTF.becomeFirstResponder()
        
        cellTF.mobilenumView.layer.cornerRadius = 20.0
        cellTF.mobilenumView.layer.masksToBounds = true
//        cellTF.mobilenumTF.addTarget(self, action: #selector(textFieldDidChange(_:)),
//                                              for: UIControl.Event.editingChanged)
        cellTF.mobilenumTF.addTarget(self, action: #selector(textField(_:shouldChangeCharactersIn:replacementString:)),
                                     for: UIControl.Event.allEditingEvents)
  
        cellTF.selectionStyle = .none
        return cellTF
    }
    func scrollToLastRow() {
        let indexPath = NSIndexPath(row: dataList.count - 1, section: 0)
        self.lisTableView.scrollToRow(at: indexPath as IndexPath, at: .middle, animated: true)
    }
    
    func scrollToFirstRow() {
        let indexPath = NSIndexPath(row: 0, section: 5)
        self.lisTableView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
    }
    
    
    @objc internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return false
        }
       
            if range.location <= 1 {
                let newPosition = textField.endOfDocument
                textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                return false
            }
       
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return false }
      
            if (range.location == 0 && range.length == 0) || (range.location == 1 && range.length == 0)
            {
                return false
            }
            
            if range.location == 0 && range.length > 1 {
                textField.text = "09"
               // self.clearButton.isHidden = true
                return false
            }
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
            if textCount > 2 {
               // self.clearButton.isHidden = false
            } else {
              //  self.clearButton.isHidden = true
            }
            
            let object = validObj.getNumberRangeValidation(text)
            // let validCount = object.min
            print("@@@@",object)
            
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            if (isBackSpace == -92) {
                if textField.text == "09" {
                    textField.text = "09"
                   // self.clearButton.isHidden = true
                    return false
                }
            }
            
            if object.isRejected == true {
                textField.text = "09"
             //   self.clearButton.isHidden = true
                textField.resignFirstResponder()
                
                let text = "Invalid Mobile Number".localized
                let alertVC = SAlertController()
                alertVC.ShowSAlert(title: "Information".localized, withDescription: text, onController: self)
                let cancelAction = SAlertAction()
                cancelAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    self.dismiss(animated: true, completion: nil)
                })
                alertVC.addAction(action: [cancelAction])
        
                return false
            }
            if textCount < object.min {
//                self.loginbutton.isEnabled = false
//                self.loginbutton.backgroundColor = .lightGray
            } else if (textCount >= object.min && textCount < object.max) {
               // self.loginbutton.isEnabled = true
               // loginbutton.setTitleColor(.white, for: .normal)
              //  self.loginbutton.backgroundColor = themeColor
            } else if (textCount == object.max) {
                textField.text = text
//                loginbutton.setTitleColor(.white, for: .normal)
//                self.loginbutton.backgroundColor = themeColor
//               // self.loginbutton.sendActions(for: .touchUpInside)
//                self.loginbutton.isEnabled = true
                textField.resignFirstResponder()
                return false
            } else if textCount > object.max {
                return false
            }
     
        return true
    }
    
    //TextFieldDelegate
    @objc private func textFieldDidChange(_ textField: UITextField) {
        //fixed text field with two prefix 09
       
        if let text = textField.text, text.count < 3 {
               textField.text = "09"
           }
         mobileNumber = textField.text ?? ""
        let cell = lisTableView.cellForRow(at: IndexPath.init(row: 0, section: inStatusRow)) as? MobileNumberTableViewCell
        cell?.mobilenumTF.becomeFirstResponder()
        
        //let number = PayToValidations().getNumberRangeValidation(mobileNumber)
      //  let validObj  = PayToValidations()
      //  let object = validObj.getNumberRangeValidation(mobileNumber)
        var string = ""
        string = textField.text ?? ""
        
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return  }
      
        
        

        guard validObj.getNumberRangeValidation(textField.text!).isRejected == false else {
            
            textField.text = "09"
            
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "Information".localized, withDescription: "Incorrect Mobile Number. Please try again".localized, onController: self)
            let cancelAction = SAlertAction()
            cancelAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                self.dismiss(animated: true, completion: nil)
            })
            alertVC.addAction(action: [cancelAction])
            return
        }
        
        let maxLength = validObj.getNumberRangeValidation(textField.text!).max
        if (textField.text?.count)! > maxLength {
            textField.text = String(textField.text!.dropLast())
            return
        }
        
    }
    
}


class optionsListTableViewCell: UITableViewCell{
    
    @IBOutlet var checkButton: UIButton!
   // @IBOutlet var dataLabel: UILabel!
    @IBOutlet var dataButton: UIButton!
    
}

//class MobilenumTableViewCell: UITableViewCell{
//
//    @IBOutlet var mobilenumTF: UITextField!
//    private let mobileNumberAcceptedChars = "0123456789"
//
//    @IBOutlet weak var mobilenumView: UIView!
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        mobilenumTF.delegate = self
//    }
//
//
//}

//extension MobilenumTableViewCell : UITextFieldDelegate {
//
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//
//    }
//
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if textField == mobilenumTF{
//             if let text = textField.text, text.count < 3 {
//                    textField.text = "09"
//                }
//        }
//        return true
//    }
//
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//      //  self.loginbutton.isUserInteractionEnabled = true
//        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
//            let endOfDocument = textField.endOfDocument
//            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
//            return false
//        }
//
//            if range.location <= 1 {
//                let newPosition = textField.endOfDocument
//                textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
//                return false
//            }
//
//        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return false }
//
//            if (range.location == 0 && range.length == 0) || (range.location == 1 && range.length == 0)
//            {
//                return false
//            }
//
//            if range.location == 0 && range.length > 1 {
//                textField.text = "09"
//               // self.clearButton.isHidden = true
//                return false
//            }
//
//            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
//            let textCount  = text.count
//            if textCount > 2 {
//               // self.clearButton.isHidden = false
//            } else {
//              //  self.clearButton.isHidden = true
//            }
//
//            let object = PayToValidations().getNumberRangeValidation(text)
//            // let validCount = object.min
//            print("@@@@",object)
//
//            let  char = string.cString(using: String.Encoding.utf8)!
//            let isBackSpace = strcmp(char, "\\b")
//
//            if (isBackSpace == -92) {
//                if textField.text == "09" {
//                    textField.text = "09"
//                   // self.clearButton.isHidden = true
//                    return false
//                }
//            }
//
//            if object.isRejected == true {
//                textField.text = "09"
//             //   self.clearButton.isHidden = true
//                textField.resignFirstResponder()
//
//                let text = "Invalid Mobile Number".localized
//
//            // AppUtility.alert(message: text, title: "", controller: self)
//                //  self.showAlert(text, image: nil, simChanges: false)
//                return false
//            }
//            if textCount < object.min {
////                self.loginbutton.isEnabled = false
////                self.loginbutton.backgroundColor = .lightGray
//            } else if (textCount >= object.min && textCount < object.max) {
//               // self.loginbutton.isEnabled = true
//               // loginbutton.setTitleColor(.white, for: .normal)
//              //  self.loginbutton.backgroundColor = themeColor
//            } else if (textCount == object.max) {
//                textField.text = text
////                loginbutton.setTitleColor(.white, for: .normal)
////                self.loginbutton.backgroundColor = themeColor
////               // self.loginbutton.sendActions(for: .touchUpInside)
////                self.loginbutton.isEnabled = true
//                self.mobilenumTF.resignFirstResponder()
//                return false
//            } else if textCount > object.max {
//                return false
//            }
//
//        return true
//
//    }
//
//}

extension UIView{
    func bindToKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }

    @objc func keyboardWillChange(_ notification: NSNotification) {
        let duration = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
        let begginingFrame = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let endFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        var deltaY = endFrame.origin.y - begginingFrame.origin.y
        
        if deltaY == -345{
            deltaY += 40
           
        }else{
            deltaY -= 40
        }

        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIView.KeyframeAnimationOptions(rawValue: curve), animations: {
            self.frame.origin.y += deltaY
        }, completion: nil)
    }
}
