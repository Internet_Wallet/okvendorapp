//
//  Registration_TextFiled.swift
//  OSKVendor
//
//  Created by vamsi on 7/1/20.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Foundation
import UIKit



extension RegistrationViewController:UITextFieldDelegate{
    
     func textFieldDidBeginEditing(_ textField: UITextField)
        {
            if textField == stateTF {
                if StatesArray.count > 0{
                    if let stateData = StatesArray[0] as? [String : Any] {
                        stateTF.text = stateData["name"] as? String
                        stateTF.inputView = statePicker
                        stateTF.tintColor = UIColor.clear
                    }
                }
            }
            if textField == townshipTF
            {
                if TownshipArray.count > 0 {
                    if let townshipData = TownshipArray[0] as? [String : Any] {
                        townshipTF.text = townshipData["name"] as? String
                        townshipTF.inputView = TownshipPicker
                        townshipTF.tintColor = UIColor.clear
                    }
                }
            }
        }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == firstNameTF{
            if let text = textField.text{
                textField.text = text
            }
        }
       else if textField == mobilenumTF{
            if  currentSelectedCountry == .myanmarCountry  {
                       
                       if let text = textField.text, text.count < 3 {
                           textField.text = "09"
                         //  self.clearButton.isHidden = true
                         //  self.loginButton.backgroundColor = .lightGray
                        //   self.loginButton.isEnabled = false
                        //   loginButton.setTitleColor(.white, for: .normal)
                       }
                   }
        }
        else if textField == emailTF{
            if let text = textField.text{
                    textField.text = text
            }
        }
        else if textField == addressLine1TF{
            if let text = textField.text{
                    textField.text = text
            }
        }
        else if textField == addressLine2TF{
            if let text = textField.text{
                    textField.text = text
            }
        }
        else if textField == stateTF{
                if let text = textField.text{
                    textField.text = text
            }
        }
        else if textField == townshipTF{
            if let text = textField.text{
                    textField.text = text
            }
        }
        else if textField == houseTF{
            if let text = textField.text{
                    textField.text = text
            }
        }
        else if textField == floorTF{
                if let text = textField.text{
                        textField.text = text
          }
        }
        else if textField == roomTF{
            if let text = textField.text{
                textField.text = text
            }
        }
        else if textField == shopnameTF{
            if let text = textField.text{
                textField.text = text
            }
        }
       else if textField == passwordTF{
        if let text = textField.text{
            textField.text = text
            
        }
       }
        else if textField == confirmPasswordTF{
            if let text = textField.text{
                textField.text = text
            }
        }
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if self.country?.dialCode == "+95" {
            textField.text = "09"
        } else {
            textField.text = ""
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       // self.loginButton.isUserInteractionEnabled = true
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return false
        }
        if textField == mobilenumTF{
        if currentSelectedCountry == .myanmarCountry {
//            if range.location <= 1 {
//                let newPosition = textField.endOfDocument
//                textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
//                return false
//            }
            
            
        }
        
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return false }
        switch currentSelectedCountry {
        case .indiaCountry:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
           // clearButtonShowHide(count: textCount)
            if textField.text!.count >= 10 && range.length == 0 {
                return false
            }
            if textCount >= 10 {
                 textField.text = text
              //  loginButton.backgroundColor = .orange
               // loginButton.setTitleColor(.white, for: .normal)
              //  self.loginButton.sendActions(for: .touchUpInside)
              //  self.loginButton.isEnabled = true
                 self.mobilenumTF.resignFirstResponder()
                return false
            } else {
              //  loginButton.setTitleColor(.white, for: .normal)
              //  loginButton.backgroundColor = UIColor.lightGray
             //   self.loginButton.isEnabled = false
                return true
            }
        case .chinaCountry:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
           // clearButtonShowHide(count: textCount)
            if textField.text!.count >= 11 && range.length == 0 {
                return false
            }
            
            if textCount >= 11  {
                 textField.text = text
              //  loginButton.setTitleColor(.white, for: .normal)
              //  loginButton.backgroundColor = .orange
              //   self.loginButton.sendActions(for: .touchUpInside)
              //   self.loginButton.isEnabled = true
                 self.mobilenumTF.resignFirstResponder()
                return false
            } else {
               // loginButton.setTitleColor(.white, for: .normal)
              //  loginButton.backgroundColor = UIColor.lightGray
              //   self.loginButton.isEnabled = false
                return true
            }
        case .myanmarCountry:
            
            if (range.location == 0 && range.length == 0) || (range.location == 1 && range.length == 0)
            {
                return false
            }
            
            if range.location == 0 && range.length > 1 {
                textField.text = "09"
             //   self.clearButton.isHidden = true
                return false
            }
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
            if textCount > 2 {
               // self.clearButton.isHidden = false
            } else {
               // self.clearButton.isHidden = true
            }
            
            let object = validObj.getNumberRangeValidation(text)
            // let validCount = object.min
            
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            if (isBackSpace == -92) {
                if textField.text == "09" {
                    textField.text = "09"
                 //   self.clearButton.isHidden = true
                    return false
                }
            }
            
            if object.isRejected == true {
                textField.text = "09"
              //  self.clearButton.isHidden = true
                textField.resignFirstResponder()
                
                let text = "Invalid Mobile Number"
                
                AppUtility.alert(message: text, title: "", controller: self)
                //  self.showAlert(text, image: nil, simChanges: false)
                return false
            }
            if textCount < object.min {
              //  self.loginButton.isEnabled = false
              //  self.loginButton.backgroundColor = .lightGray
            } else if (textCount >= object.min && textCount < object.max) {
               // self.loginButton.isEnabled = true
              //  loginButton.setTitleColor(.white, for: .normal)
              //  self.loginButton.backgroundColor = .orange
            } else if (textCount == object.max) {
                textField.text = text
              //  loginButton.setTitleColor(.white, for: .normal)
              //  self.loginButton.backgroundColor = .orange
              //  self.loginButton.sendActions(for: .touchUpInside)
              //  self.loginButton.isEnabled = true
                self.mobilenumTF.resignFirstResponder()
                return false
            } else if textCount > object.max {
                return false
            }
          
         
        case .thaiCountry:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
          //  clearButtonShowHide(count: textCount)
            if textField.text!.count >= 9 && range.length == 0 {
                return false
            }
            
            if textCount >= 9  {
                textField.text = text
               // loginButton.setTitleColor(.white, for: .normal)
              //  loginButton.backgroundColor = .orange
              //   self.loginButton.sendActions(for: .touchUpInside)
               //  self.loginButton.isEnabled = true
                 self.mobilenumTF.resignFirstResponder()
                return false
            }  else {
              //  loginButton.setTitleColor(.white, for: .normal)
             //   loginButton.backgroundColor = .lightGray
              //   self.loginButton.isEnabled = false
                return true
            }
            
        case .other:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
          //  clearButtonShowHide(count: textCount)
            
            if textField.text!.count >= 13 && range.length == 0 {
                return false
            }
            
            
            if textCount >= 4 {
                textField.text = text
              //  loginButton.setTitleColor(.white, for: .normal)
              //  loginButton.backgroundColor = .orange
              //  self.loginButton.isEnabled = true
              if textCount == 13 {
                textField.text = text
            //     self.loginButton.sendActions(for: .touchUpInside)
                 self.mobilenumTF.resignFirstResponder()
              }
                return false
            }
            
            if textCount > 13 {
                textField.text = text
              //  loginButton.setTitleColor(.white, for: .normal)
              //  loginButton.backgroundColor = .orange
              //   self.loginButton.isEnabled = true
              //   self.loginButton.sendActions(for: .touchUpInside)
                 self.mobilenumTF.resignFirstResponder()
                return false
            }
            else {
               // loginButton.setTitleColor(.white, for: .normal)
               // loginButton.backgroundColor = .lightGray
              //   self.loginButton.isEnabled = false
                return true
            }
            
//            if textCount > 13 {
//                loginButton.setTitleColor(.white, for: .normal)
//                loginButton.backgroundColor = .orange
//                //self.registerButton.sendActions(for: .touchUpInside)
//                return false
//            }
//
//            if textCount > 3  {
//                self.mobileNumTF.text = text
//                self.registerButton.isEnabled = true
//                loginButton.setTitleColor(.white, for: .normal)
//                self.loginButton.backgroundColor = .orange
//                //self.submitBtn.sendActions(for: .touchUpInside)
//                return false
//            }
//            else {
//                self.loginButton.isEnabled = false
//                self.loginButton.backgroundColor = .lightGray
//                return true
//            }
      }
    }
    
    if textField == emailTF {
        if string == " " {
           return false
        }
    }
     if textField == passwordTF {
        if string == " " {
          return false
       }
     }
     if textField == confirmPasswordTF {
        if string == " " {
            return false
         }
      }
    if textField == houseTF {
       if string == " " {
          return false
          }
      }
     if textField == floorTF {
         if string == " " {
              return false
          }
       }
     if textField == roomTF {
         if string == " " {
             return false
          }
        }
    else {
         // password TF
    }
        return true
        
    }
}

extension RegistrationViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    // MARK: - PickerView  Delegate Methods
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            var rows: Int = 0
            if pickerView == statePicker {
                rows = StatesArray.count
            }
            if pickerView == TownshipPicker {
                rows = TownshipArray.count
            }
           
            return rows
        }
        
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            var returnString = String()
            if pickerView == statePicker
            {
                returnString = StatesArray[row] as! String
            }
            if pickerView == TownshipPicker
            {
                stateDict = StatesArray[row] as! [String:AnyObject]
                returnString = stateDict["name"] as! String
                stateId = stateDict["id"] as! String
                returnString = TownshipArray[row] as! String
            }
           
            return returnString
        }
        
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            if pickerView == statePicker {
                stateDict = StatesArray[row] as [String:AnyObject]
                 stateTF.text = stateDict["name"] as? String
                 stateId = String(stateDict["id"] as! Int)
            }
            if pickerView == TownshipPicker
            {
                townshipDict = TownshipArray[row] as [String:AnyObject]
                townshipTF.text = townshipDict["name"] as? String
                cityId = String(townshipDict["id"] as! Int)
            }
           
        }
        
        func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
            var pickerLabel: UILabel? = (view as? UILabel)
            if pickerLabel == nil {
                pickerLabel = UILabel()
                pickerLabel?.font = UIFont(name: "Helvetica Neue", size: 15)
                pickerLabel?.textAlignment = .center
            }
            if pickerView == statePicker {
                stateDict = StatesArray[row] as [String:AnyObject]
                pickerLabel?.text = stateDict["name"] as? String
            }
            if pickerView == TownshipPicker {
                townshipDict = TownshipArray[row] as [String:AnyObject]
                pickerLabel?.text = townshipDict["name"] as? String
                
            }
           
            return pickerLabel!
        }
    
    func keyboardWasShown(_ notification: Notification) {
        
       }
       
       func keyboardWillBeHidden(_ notification: Notification)
       {
//           for i in 0..<self.countriesAndStatesRespArr.count
//           {
//               let sampleDict = self.countriesAndStatesRespArr[i] as! [String:Any]
//               if countryTF.text == sampleDict["CName"] as? String
//               {
//                   countryId = sampleDict["CId"] as! String
//                   StatesArray = sampleDict["StateList"] as! [Any]
//                   break
//               }
//           }

           statePicker.removeFromSuperview()
           TownshipPicker.removeFromSuperview()
           pickerTextView?.removeFromSuperview()
       }
    
}
