//
//  MapAlertViewController.swift
//  OSKVendor
//
//  Created by OK$ on 30/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

protocol MapAlertDelegate: class {
    func selectedOption(otpNumber: String)
}

class MapAlertViewController: OSKBaseViewController {

    var delegate: MapAlertDelegate?

    @IBOutlet var headerLabel: UILabel!{
        didSet{
            self.headerLabel.font = UIFont(name: appFont, size: 16.0)
            self.headerLabel.text = self.headerLabel.text?.localized
        }
    }
    @IBOutlet var descriptionLabel: UILabel!{
        didSet{
            self.descriptionLabel.font = UIFont(name: appFont, size: 16.0)
            self.descriptionLabel.text = self.descriptionLabel.text?.localized
        }
    }
    
    @IBOutlet var btnCancel: UIButton!{
        didSet{
            self.btnCancel.setTitle("Cancel", for: .normal)
        }
    }
    @IBOutlet var btnOK: UIButton!{
        didSet{
            self.btnOK.setTitle("OK", for: .normal)
        }
    }
    
    var strHeader = ""
    var strDescription = ""
    var strBtn1Title = ""
    var strBtn2Title = ""
    
    @IBAction func tapGesture(_ sender: Any) {
         self.view.removeFromSuperview()
         self.dismiss(animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        headerLabel.text = strHeader
        descriptionLabel.text = strDescription
        self.btnCancel.setTitle(strBtn1Title.localized, for: .normal)
        self.btnOK.setTitle(strBtn2Title.localized, for: .normal)
        
        btnCancel.titleLabel?.numberOfLines = 0
        btnCancel.titleLabel?.lineBreakMode = .byCharWrapping
        
        btnOK.titleLabel?.numberOfLines = 0
        btnOK.titleLabel?.lineBreakMode = .byCharWrapping
        
        
    }
    
    @IBAction func btn1Action(_ sender: Any) {
        delegate?.selectedOption(otpNumber: strBtn1Title)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btn2Action(_ sender: Any) {
        delegate?.selectedOption(otpNumber: strBtn2Title)
        self.dismiss(animated: false, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
