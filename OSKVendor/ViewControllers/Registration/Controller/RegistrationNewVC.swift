//
//  RegistrationNewVC.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 12/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON
import AVKit
import DropDown


protocol LoginDelegate: class {
    func langResetFromLogin( reset : Bool)
}


class RegistrationNewVC: OSKBaseViewController,PhValidationProtocol {

    @IBOutlet weak var viewLangOption: UIView!
    @IBOutlet weak var btnLangSelected: UIButton!
    
    @IBOutlet weak var tblList: UITableView!
    
    @IBOutlet weak var category: UIView!
    
    @IBOutlet var navigationTitleLabel: UILabel!{
        didSet{
            navigationTitleLabel.text = "Registration".localized
        }
    }
    
    var marketLabel = "Select market name".localized
    
    var isMarketListHidden = false
    @IBOutlet var marketPlaceView: UIView!
    @IBOutlet var tblMarketList: UITableView!
    
    var shopName : String = ""
    var state: String = ""
    var township : String = ""
    var countryObject = (code: "60", name: "Myanmar")
    var selectedState = (code: "", name: "")
    var townshipObject = (code: "", name: "")
    var marketList : [[String : Any]] = [[String : Any]]()
    
    var isWholeSaleMarketSelected:Bool = false
    var marketTypeId = 3
    var isMarket : Bool = false
    var FortmType : Int = 0
    
    var mobileNumber : String?
    var facebookID : String?
    var address : String?
    var latitudeVal : String?
    var longitudeVal : String?
    var MarketID = 0
    
    var locationLat : String?
    var locationLong : String?
    
    var selctionArray : [Bool] = Array(repeating: false, count: 6)
    
    
    var isDevision : Bool = false
    var typeMarket : Int = 0

    var boolLang = false
    var apiManager = APIManager()
    let dropDown = DropDown()
    
    var phNumValidationsFile: [PhNumValidationList] = []
    
    var timer          = Timer()
    var seconds        = 50
    var smsOtpTxt = ""
    var randomOTP = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phNumValidationsFile = getDataFromJSONFile() ?? []
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
   
            
            for i in 0..<marketList.count{
                if marketList[i]["BusinessName"] as? String ?? "" == item{
                 
                    
                    marketLabel = marketList[i]["BusinessName"] as? String ?? ""
                    
                    
//                    if let cell = tblList.cellForRow(at: IndexPath(row: 1, section: 1)) as? DetailsFormTableViewCell {
//                       cell.btnMarketSelect.setTitle( marketList[i]["BusinessName"] as? String, for: .normal)
//                        cell.marketid = marketList[i]["Id"] as? Int ?? -10
//                    }
                    self.MarketID = marketList[i]["Id"] as? Int ?? -10
                    
                    let indexPosition = IndexPath(row: 1, section: 1)
                    tblList.reloadRows(at: [indexPosition], with: .none)
                   
                    break
                }
            }
            
           
           
        }
        
        
        // Do any additional setup after loading the view.
        self.tblList.register(UINib(nibName: "CategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "categoryTableViewCell")
        viewLangOption.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        DispatchQueue.main.asyncAfter (deadline: .now() + 0.5, execute: {
            MediaPlayer.play(fileName: "enter_shop_name", type: "m4a")
        })
        
        self.tblList.delegate = self
        self.tblList.dataSource = self
        tblMarketList.delegate = self
        tblMarketList.dataSource = self
        self.marketPlaceView.addSubview(tblMarketList)
        self.view.addSubview(marketPlaceView)
        marketPlaceView.isHidden = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), lang == "en"{
            btnLangSelected.setBackgroundImage(#imageLiteral(resourceName: "myanmar"), for: .normal)
        } else {
            btnLangSelected.setBackgroundImage(#imageLiteral(resourceName: "ukf"), for: .normal)
        }
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @objc func DismissKeyboard(){
       //Causes the view to resign from the status of first responder.
       view.endEditing(true)
        marketPlaceView.isHidden = true
        isMarketListHidden = false
        self.marketPlaceView.removeFromSuperview()
        boolLang = false
        viewLangOption.isHidden = true
        dropDown.hide()
    }
    
    func updateGUI() {
        appDelegate.setInitialCurrentLang()
        navigationTitleLabel.text = "Registration".localized
        tblList.reloadData()
    }
    
    
    @IBAction func onClickLangChangeBtn(_ sender: Any) {
          if boolLang {
              boolLang = false
              viewLangOption.isHidden = true
          } else {
              boolLang = true
              viewLangOption.isHidden = false
          }
      }
      
      @IBAction func selectedLanguage(_ sender: Any) {
        
        //Reset All Cells
        marketPlaceView.isHidden  = true
        isMarket  = false
        self.isWholeSaleMarketSelected = false
        selectedState = (code: "", name: "")
        townshipObject = (code: "", name: "")
        shopName = ""
        
        //self.overRideWebView()
          viewLangOption.isHidden = true
          let intSelection = (sender as AnyObject).tag
          boolLang = false
          viewLangOption.isHidden = true
        if intSelection == 1 {
            btnLangSelected.setBackgroundImage(#imageLiteral(resourceName: "ukf"), for: .normal)
            UserDefaults.standard.set("uni", forKey: "currentLanguage")
            UserDefaults.standard.set("Myanmar3", forKey: "appFont")
            UserDefaults.standard.synchronize()
            self.changeAppLanguageApiCall(index: 3)
          } else {
            btnLangSelected.setBackgroundImage(#imageLiteral(resourceName: "myanmar"), for: .normal)
            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
            UserDefaults.standard.set("en", forKey: "currentLanguage")
            UserDefaults.standard.synchronize()
            self.changeAppLanguageApiCall(index: 1)
          }
        DispatchQueue.main.async {
            self.updateGUI()
        }
      }
    
    func showContactSuggessionView(withFrame frame: CGRect, andContacts contactList: [Dictionary<String, Any>]) {
           marketPlaceView.isHidden  = false
           tblMarketList.frame = frame
           marketPlaceView.frame = frame
           marketPlaceView.layoutIfNeeded()
          self.view.addSubview(marketPlaceView)
           self.view.bringSubviewToFront(marketPlaceView)
        tblMarketList.delegate = self
        tblMarketList.dataSource = self
        tblMarketList.reloadData()
        }
    
    
    func regsiterVendor () {
        if AppUtility.isConnectedToNetwork(){
            let inputString = mobileNumber ?? ""
            let outputString = String(inputString.dropFirst())
            let finalmobilestring = "0095" + outputString
            
            let registrationRequestData:Dictionary<String,Any> = [ "Firstname": shopName ,
                                                              "Lastname": "Vendor",
                                                              "Email": (mobileNumber ?? "dummy") + "vendor" + "@gmail.com",
                                                              "Username":finalmobilestring,
                                                              "Mode":"Register",
                                                              "Gender" : "non",
                                                              "DateOfBirthDay":"22",
                                                              "DateofBirthMonth":"02",
                                                              "DateOfBirthYear":"1991",
                                                              "Address1":address ?? "",
                                                              "Address2":"",
                                                              "CityId":self.townshipObject.code,
                                                              "StateId":self.selectedState.code,
                                                              "Country": "Myanmar",
                                                              "OtherEmail":(mobileNumber ?? "dummy") + "vendor" + "@gmail.com",
                                                              "MobileNumber":finalmobilestring,
                                                              "Password":"1234567890",
                                                              "HouseNo":"non",
                                                              "FloorNo":"non",
                                                              "RoomNo":"non",
                                                              "ShopName":shopName ,
                                                              "MarketId": MarketID,
                                                              "Latitude":latitudeVal ?? "0.0",
                                                              "Longitude":longitudeVal ?? "0.0",
                                                              "FacebookId":facebookID ?? "",
                                                              "IsWholeSaleMarket":self.isWholeSaleMarketSelected,
                                                              "ShopLocationType":self.marketTypeId ,
                                                              "Token":" ",
                                                              "DeviceID":apiManager.deviceId,
                                                              "VersionCode":1,
                                                              "RegPreferenceCategoryId":6370,
                                                              "Simid": ""
          ]
            self.callRegisterApi(addressDict: (registrationRequestData))
        }
        else{
            AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
        }
    }
    
    //MARK:- Change Language APi
    
    func changeAppLanguageApiCall(index: Int) {
        
        let type = index //(index == 0) ? 1 : 2
        let urlString = String(format: "%@/SetLanguage/%d", APIManagerClient.sharedInstance.base_url, type)
        guard let url = URL(string: urlString) else { return }
        let params = Dictionary<String, Any>()
        AppUtility.showLoading(self.view)
        apiManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, _) in
            if success {
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["StatusCode"] as? Int, code == 200 {
                        DispatchQueue.main.async {
                            if let viewLoc = self?.view {
                                    AppUtility.hideLoading(viewLoc)
                            }
                        }
                        println_debug("Language updated")
                    } else {
                        DispatchQueue.main.async {
                            if let viewLoc = self?.view {
                                    AppUtility.hideLoading(viewLoc)
                            }
                            AppUtility.showToastlocal(message: "Failure", view: self!.view)
                        }
                        
                    }
                }
            }
        }
    }
    
    //MARK:- Register APi
    private func callRegisterApi(addressDict: Dictionary<String, Any>?) {
        AppUtility.showLoading(self.view)
       apiManager.RegisterUser(addressDict! as [String : AnyObject], onSuccess: { [weak self] response in
                   
                   DispatchQueue.main.async{
                       if let viewLoc = self?.view {
                           AppUtility.hideLoading(viewLoc)
                       }
                       let json = JSON(response)
                       println_debug("Register Response: \(json)")
                       if let code = json["StatusCode"].int, code == 200 {
                           if let registerInfoDic  = json["Data"].dictionaryObject {
                               let vmTemoModel = VMTempModel()
                               vmTemoModel.wrapDataModel(JSON: registerInfoDic)
                               print(registerInfoDic)
                               DispatchQueue.main.async {
                                       UserDefaults.standard.set(true, forKey: AppConstants.userDefault.loginStatus)
                                       UserDefaults.standard.set(true, forKey: AppConstants.userDefault.enabledLogout)
                                       UserDefaults.standard.synchronize()
                                        appDelegate.FCMToken = registerInfoDic["Token"] as! String
                                       let dataDict:[String: String] = ["token": appDelegate.FCMToken]
                                       NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
                                       self?.registrationCompleteAction()
                               }
                           }
                           //self?.handleLogin()
                       }else {
                           let message = json["ErrorList"][0].string
                           let alert = UIAlertController(title: "OSK Vendor".localized, message: message, preferredStyle: UIAlertController.Style.alert)
                           alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: nil))
                           self?.present(alert, animated: true, completion: nil)
                       }
                   }
                   }, onError: { [weak self] message in
                       if let viewLoc = self?.view {
                           DispatchQueue.main.async {
                               AppUtility.hideLoading(viewLoc)
                         //      self?.showAlert(alertTitle: "Login", description: "Please Check Internet Connection")
                             
                           }
                       }
               })
       
   }
    
    func registrationCompleteAction() -> Void {
        
        let loginStoryboard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let viewController = loginStoryboard.instantiateViewController(withIdentifier: "ShopOpenViewController") as! ShopOpenViewController
        viewController.shopName = self.shopName
        viewController.navigationController?.navigationBar.isHidden = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    func getMarketListBasedOnTownship( townshipId : Int){
        if AppUtility.isConnectedToNetwork(){
            AppUtility.showLoading(self.view)
            apiManager.getMarketInfoForTownship(TownshipID: "\(townshipId)") { (marketDictionary) in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                                AppUtility.hideLoading(viewLoc)
                        }
                    if let marketData = marketDictionary["Data"] as? [[String:Any]] , marketData.count > 0 {
                        self.marketList = marketData
                    }
                }
            } onError: { (error) in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                            AppUtility.hideLoading(viewLoc)
                    }
                    AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
                }
            }
        } else {
         AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
     }
  }
    
}


extension RegistrationNewVC: UIScrollViewDelegate {
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        marketPlaceView.isHidden = true
//        isMarketListHidden = false
//        self.marketPlaceView.removeFromSuperview()
//    }
//
//    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
//        view.endEditing(true)
//         marketPlaceView.isHidden = true
//         isMarketListHidden = false
//         self.marketPlaceView.removeFromSuperview()
//         dropDown.hide()
//    }
//
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        marketPlaceView.isHidden = true
//        isMarketListHidden = false
//        self.marketPlaceView.removeFromSuperview()
//        dropDown.hide()
//    }
    
}
extension RegistrationNewVC : OTPDelegate {
    
    func checkUserLogin() {
          self.view.endEditing(true)
          let urlString = String(format: "%@", APIManagerClient.sharedInstance.base_url_sendSMS)
          guard let url = URL(string: urlString) else { return }
          let params = AppUtility.JSONStringFromAnyObject(value: self.getSendSMSParams() as AnyObject)
          println_debug(params)
          AppUtility.showLoading(self.view)
          apiManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, _) in
              DispatchQueue.main.async {
                  if let viewLoc = self?.view {
                      AppUtility.hideLoading(viewLoc)
                  }
              }
              if success {
                  if let json = response as? Dictionary<String,Any> {
                      println_debug(json)
                      if let code = json["StatusCode"] as? Int, code == 200 {
                          //RegistrationModel.share.MobileNumber = json["DestinationNumber"] as? String ?? ""
                          self?.randomOTP = json["OTP"] as? String ?? ""
                        if json["IsUserRegistered"] as? Int ?? 0 == 1 {
                          self?.showOtpScreen()
                        } else {
                            DispatchQueue.main.async {

                            self?.showAlert(alertTitle: "Login".localized, description: "Please Register!".localized)
                            }
                        }
                       // self?.showOtpScreen()
                       //   self?.timer.invalidate()
                      } else {
                          DispatchQueue.main.async {
                              
                              self?.showAlert(alertTitle: "Login".localized, description: "Please Check Internet Connection".localized)
                          }
                      }
                  }
              }else {
                  DispatchQueue.main.async {
                    self?.showAlert(alertTitle: "Login".localized, description: response as! String)
                  }
                  
              }
          }
      }
      
      func getSendSMSParams() -> [String:Any] {
          var paramDic            = [String:Any]()
          paramDic["Application"]  = "1 Stop Mart"
          var mobileNo = mobileNumber
        if ((mobileNo?.hasPrefix("09")) != nil) {
            mobileNo = "0095" + (mobileNo?.dropFirst())! 
          }
          paramDic["DestinationNumber"]  = mobileNo
          paramDic["Operator"]  = appDelegate.getNetworkCode().1
          paramDic["AppVersionName"] = "1.0.00"
          paramDic["DeviceTypeId"] =  APIManagerClient.sharedInstance.DeviceTypeId
          paramDic["BuildType"] = "debug"
          return paramDic
      }
    
    
    
    
    func selectedOTPNumber(otpNumber: String , vc:OTPViewController) {
          //self.verifyBtn.setTitle("Verify".localized, for: .normal)
          DispatchQueue.main.async {
              self.view.endEditing(true)
          }
        if randomOTP == otpNumber {
            DispatchQueue.main.async {
               // self.registerClick()
                self.regsiterVendor()
            }
            
        } else {
              //otp doesn't match\
              let alertVC = SAlertController()
              alertVC.ShowSAlert(title: "Warning!".localized, withDescription: "Please enter valid OTP".localized, onController: self)
              let ok = SAlertAction()
              ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                  self.showOtpScreen()
              })
              alertVC.addAction(action: [ok])
          }
      }
      
     func showOtpScreen() {
          DispatchQueue.main.async {
              let sb = UIStoryboard(name: "Login", bundle: nil)
              let oTPViewController  = sb.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
              oTPViewController.modalPresentationStyle = .overCurrentContext
              oTPViewController.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
              oTPViewController.delegate = self
              self.present(oTPViewController, animated: false, completion: nil)
              self.smsOtpTxt = ""
          }
      }
}
