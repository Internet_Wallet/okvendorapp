//
//  StateAndTownshipVC.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 23/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import ObjectMapper

protocol StateDivisionVCDelegate : class {
    
    func setStateAndTownship(townshipName : String , townshipId : String , stateObject : (String , String))
}

class StateAndTownshipVC: OSKBaseViewController, UITextFieldDelegate {
    
      @IBOutlet weak var listTableView: UITableView!
    
      weak var stateDivisonVCDelegate : StateDivisionVCDelegate?
    
    
      var array = [[String:Any]]()
      var array_cityTownship :[[String : Any]] = [[String : Any]]()
    
      var isSearch = false
      var isTownship = false
    
      var tfSearch = UITextField()
      var CHARSET = ""
    
      var aPIManager = APIManager()
      var ok_default_language : String?
    
      var countryObject: (String, String)?
      var stateObject : (String, String)? = ("","")
      
    
    
      @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var btnSearch: UIButton!
      @IBOutlet var btnSearchBar: UIBarButtonItem!
    

    override func viewDidLoad() {
        super.viewDidLoad()
            btnSearch.tag = 1
            loadInitialize()
            
            self.setMarqueLabelInNavigation()
    }
    
    
    func loadInitialize() {
        lblNoRecord.text = "No Record Found".localized
        lblNoRecord.isHidden = true
        listTableView.tableFooterView = UIView(frame: CGRect.zero)
        ok_default_language = UserDefaults.standard.value(forKey: "currentLanguage") as? String
        if ok_default_language == "en"{
            CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
        }else {
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
        }
        self.getState()
        
    }
    
    
    
    private func getState() {
            if AppUtility.isConnectedToNetwork(){
                AppUtility.showLoading(self.view)
                aPIManager.getDevisionInfoForCountry(countryID: countryObject?.0 ?? "60" , onSuccess: { (devisionDictionary) in
                    DispatchQueue.main.async {
                        if let viewLoc = self.view {
                                    AppUtility.hideLoading(viewLoc)
                            }
                        if let devisionData = devisionDictionary["Data"] as? [[String:Any]] , devisionData.count > 0 {
                            self.array_cityTownship = devisionData
                            self.listTableView.reloadData()
                        }
                    }
            }) { (error) in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                            AppUtility.hideLoading(viewLoc)
                    }
                    AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
                }
            }
            } else {
             AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
         }
    }
    
    private func getTownship( stateId : Int) {
           if AppUtility.isConnectedToNetwork(){
            AppUtility.showLoading(self.view)
            self.aPIManager.getTownshipInfoForDevision(devisionID: "\(stateId)", onSuccess: { (townshipInfo) in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                                AppUtility.hideLoading(viewLoc)
                        }
                    if let townshipData = townshipInfo["Data"] as? [[String:Any]] , townshipData.count > 0 {
                        self.isSearch = false
                        self.tfSearch.text = ""
                        self.array_cityTownship = townshipData
                        self.listTableView.reloadData()
                    }
                }
            }) { (errorX) in
                          DispatchQueue.main.async {
                            if let viewLoc = self.view {
                                                   AppUtility.hideLoading(viewLoc)
                                           }
                         }
                    }
           } else {
                  AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
            self.navigationItem.titleView = nil
            self.setMarqueLabelInNavigation()
            setMarqueLabelInNavigation()
            lblNoRecord.isHidden = true
            isSearch = false
            tfSearch.text = ""
            listTableView.reloadData()
        }
       
        private func setMarqueLabelInNavigation() {
            self.navigationItem.titleView = nil
            let lblMarque = MarqueeLabel()
            lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
            lblMarque.font =  UIFont(name: "Myanmar3", size: 18.0)
            if isTownship {
                lblMarque.text = "Select Township / City".localized
            }
            else {
                lblMarque.text = "Select State / Division".localized
            }
            lblMarque.textAlignment = .center
            lblMarque.textColor = UIColor.white
            btnSearch.setTitle("", for: .normal)
            btnSearch.setImage(#imageLiteral(resourceName: "search"), for: .normal)
            self.navigationItem.titleView = lblMarque
            self.navigationController?.navigationBar.barTintColor = UIColor.colorWithRedValue(redValue: 10, greenValue: 177, blueValue: 75, alpha: 1)
        }
        
        @IBAction func backAction(_ sender: Any) {
            if isTownship {
                isTownship = false
                getState()
                 setMarqueLabelInNavigation()
            }
            else {
                 self.navigationController?.popViewController(animated: false)
              }
        }
        
        @IBAction func searchAction(_ sender: Any) {
            if btnSearch.tag == 1 {
                self.navigationItem.titleView = nil
                self.navigationItem.title = ""
                tfSearch.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
                self.navigationItem.titleView = tfSearch
                tfSearch.backgroundColor = UIColor.white
                tfSearch.delegate = self
                tfSearch.layer.cornerRadius = 5.0
                tfSearch.textColor = UIColor.black
                tfSearch.font = UIFont(name: "Myanmar3", size: 17)
                btnSearch.setTitleColor(UIColor.white, for: .normal)
                tfSearch.placeholder = "Search".localized
                //tfSearch.tintColor = UIColor.darkGray
                tfSearch.tintColor = UIColor.init(red: 0/255, green: 144/255, blue: 81/255, alpha: 1)
                tfSearch.delegate = self
                tfSearch.clearButtonMode = .whileEditing
                
                let tfserarcView = UIView()
                tfserarcView.frame = CGRect(x: 10, y: 0, width: 30, height: 30)
                let tfserarchIcon = UIImageView()
                tfserarchIcon.frame = CGRect(x: 8, y: 8, width: 14, height: 14)
                tfserarcView.addSubview(tfserarchIcon)
                tfserarchIcon.image = #imageLiteral(resourceName: "search")
                tfSearch.leftView = tfserarcView
                tfSearch.leftViewMode = UITextField.ViewMode.always
                
                btnSearch.setImage(nil, for: .normal)
                btnSearch.titleLabel?.font = UIFont(name: appFont, size: 13.0) ?? UIFont.systemFont(ofSize: 13)
                btnSearch.setTitle("Cancel".localized, for: .normal)
                //btnSearch.font = UIFont(name: "Myanmar3", size: 17)!
                tfSearch.becomeFirstResponder()
                btnSearch.tag = 2
                
            } else if btnSearch.tag == 2{
                btnSearch.tag = 1
                self.navigationController?.view.endEditing(false)
                lblNoRecord.isHidden = true
                isSearch = false
                tfSearch.text = ""
                self.navigationItem.titleView = nil
                self.setMarqueLabelInNavigation()
                listTableView.reloadData()
            } else if btnSearch.tag == 3 {
                btnSearch.tag = 2
                self.navigationController?.view.endEditing(true)
                view.endEditing(true)
                self.navigationController?.view.endEditing(false)
                lblNoRecord.isHidden = true
                isSearch = false
                tfSearch.text = ""
                listTableView.reloadData()
            }
        }
        
        public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
            tfSearch.resignFirstResponder()
            return true
        }
        
        public func textFieldShouldClear(_ textField: UITextField) -> Bool {
            lblNoRecord.isHidden = true
            tfSearch.text = ""
            btnSearch.tag = 2
            isSearch = false
            listTableView.reloadData()
            println_debug("textFieldShouldClear")
            return true
        }

        func setTownshipCityName(cityDic : TownShipDetailForAddress){
            
//            guard(self.stateDivisonVCDelegate?.setTownshipCityNameFromDivison(Dic: cityDic) != nil) else {
//                return
//            }
//              self.navigationController?.popViewController(animated: false)
        }
        
        public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
            if range.location == 0 && string == " " {
                return false
            }
          if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
            
            if let text = textField.text, let textRange = Range(range, in: text) {
                let updatedText = text.replacingCharacters(in: textRange, with: string)
                if updatedText == "" {
                    isSearch = false
                    listTableView.reloadData()
                     lblNoRecord.isHidden = true
                }
                else{
                    getSearchArrayContains(updatedText)
                }
            }
            return restrictMultipleSpaces(str: string, textField: textField)
        }

    
        func getSearchArrayContains(_ text : String) {
            
//            if ok_default_language == "en"{
//            array =  self.arr_city.filter({$0.stateOrDivitionNameEn.lowercased().contains(find: text.lowercased())})
//            }else {
//            array =  self.arr_city.filter({$0.stateOrDivitionNameUni.lowercased().contains(find: text.lowercased())})
//            }

           // array = self.array_cityTownship?.filter({$0.contains(where: text)}) as! [[String : Any]]
            
            array.removeAll()
            
            for element in array_cityTownship {
                let strTown = element["name"] as? String ?? ""
                
                if strTown.contains(find: text) {
                    array.append(element)
                }
            }
            
            
            
            if array.count == 0 {
                self.btnSearch.tag = 2
                 lblNoRecord.isHidden = false
            }else{
                lblNoRecord.isHidden = true
            }
            isSearch = true
            listTableView.reloadData()
        }
        
    
    func restrictMultipleSpaces(str : String, textField: UITextField) -> Bool {
              if str == " " && textField.text?.last == " "{
                  return false
              } else {
                  if characterBeforeCursor(tf: textField) == " " && str == " " {
                      return false
                  } else {
                      if characterAfterCursor(tf: textField) == " " && str == " " {
                          return false
                      } else {
                          return true
                      }
                  }
              }
           }

           func characterBeforeCursor(tf : UITextField) -> String? {
             if let cursorRange = tf.selectedTextRange {
                 if let newPosition = tf.position(from: cursorRange.start, offset: -1) {
                     let range = tf.textRange(from: newPosition, to: cursorRange.start)
                     return tf.text(in: range!)
                 }
             }
             return nil
           }

           func characterAfterCursor(tf : UITextField) -> String? {
             if let cursorRange = tf.selectedTextRange {
                 if let newPosition = tf.position(from: cursorRange.start, offset: 1) {
                     let range = tf.textRange(from: newPosition, to: cursorRange.start)
                     return tf.text(in: range!)
                 }
             }
             return nil
           }
    }

       


    extension StateAndTownshipVC : UITableViewDelegate,UITableViewDataSource {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if isSearch {
                return array.count
            }else {
                return array_cityTownship.count 
            }
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let divisionCell = tableView.dequeueReusableCell(withIdentifier: "DivisionStateCell", for: indexPath) as! DivisionStateCell
            if isSearch {
                let dic = array[indexPath.row]
                divisionCell.wrapData(title:dic["name"] as! String)
            }else {
                let dic = array_cityTownship[indexPath.row]
                divisionCell.wrapData(title:dic["name"] as! String)
              }
            return divisionCell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
            tableView.deselectRow(at: indexPath, animated: true)
            btnSearch.tag = 1
            if isTownship {
                if isSearch {
                    let dictA = self.array[indexPath.row]
                    let name = (dictA["name"] as? String) ?? "A"
                    let idValue = (dictA["id"] as? Int) ?? 0
                    if let del = self.stateDivisonVCDelegate {
                        del.setStateAndTownship(townshipName: name , townshipId: "\(idValue)", stateObject: self.stateObject ?? ("",""))
                    }
                }else {
                    if let del = self.stateDivisonVCDelegate {
                        let dictA = self.array_cityTownship[indexPath.row]
                        let name = (dictA["name"] as? String) ?? "A"
                        let idValue = (dictA["id"] as? Int) ?? 0
                        del.setStateAndTownship(townshipName: name, townshipId: "\(idValue)", stateObject: self.stateObject ?? ("",""))
                    }
                }
                DispatchQueue.main.async {
                     self.navigationController?.popViewController(animated: false)
                }
            }
            else {
              self.isTownship = true
              if isSearch {
                    let dictA = self.array[indexPath.row]
                    self.getTownship(stateId: dictA["id"] as! Int)
                    self.stateObject?.0 = String((dictA["id"] as? Int ?? 0))
                    self.stateObject?.1 = dictA["name"] as? String ?? "A"
                }
                   else {
                    let dictA = self.array_cityTownship[indexPath.row]
                    self.getTownship(stateId: dictA["id"] as! Int)
                    self.stateObject?.0 = String((dictA["id"] as? Int ?? 0))
                    self.stateObject?.1 = dictA["name"] as? String ?? "A"
                  }
                self.listTableView.reloadData()
                self.listTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
            }
            self.setMarqueLabelInNavigation()
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 60
        }
    }



class DivisionStateCell: UITableViewCell {

    @IBOutlet weak var divisionStateNamelbl: UILabel!
    @IBOutlet weak var townshipCountLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        divisionStateNamelbl.font = UIFont.init(name: appFont, size: 18)
//        townshipCountLbl.font = UIFont.init(name: appFont, size: 13)
//        townshipCountLbl.layer.cornerRadius = 15
//        townshipCountLbl.layer.masksToBounds = true
//        townshipCountLbl.layer.borderWidth = 1.0
//        townshipCountLbl.layer.borderColor = UIColor.white.cgColor
//        townshipCountLbl.backgroundColor = UIColor.red
//        townshipCountLbl.textColor = UIColor.white
//        self.townshipCountLbl.center.x += 30
//        self.townshipCountLbl.isHidden = true
//        UIView.animate(withDuration: 0.5, delay: 0.5,
//                       usingSpringWithDamping: 0.3,
//                       initialSpringVelocity: 0.5,
//                       options: [], animations: {
//                        self.townshipCountLbl.center.x = UIScreen.main.bounds.size.width - 30
//        }, completion:{ _ in })

    }
    
    func wrapData(title: String ) {
            self.divisionStateNamelbl.text = title
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
