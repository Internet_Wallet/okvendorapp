//
//  ChangePasswordVC.swift
//  OSKVendor
//
//  Created by OK$ on 28/09/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import LTMorphingLabel
import SwiftyJSON

class ChangePasswordVC: OSKBaseViewController {
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var titleLabel: UILabel!{
        didSet{
            titleLabel.text = "Change Password".localized
        }
    }
    
    @IBOutlet var headingLabel: LTMorphingLabel!{
        didSet{
            headingLabel.text = "Update your password.".localized
        }
    }
    
    @IBOutlet var oldPwdNumTF: SkyFloatingLabelTextField!{
        didSet {
            self.oldPwdNumTF.font = UIFont(name: appFont, size: 15.0)
            self.oldPwdNumTF.placeholder = self.oldPwdNumTF.placeholder?.localized
        }
    }
    @IBOutlet var newPwdNumTF: SkyFloatingLabelTextField!{
        didSet {
            self.newPwdNumTF.font = UIFont(name: appFont, size: 15.0)
            self.newPwdNumTF.placeholder = self.newPwdNumTF.placeholder?.localized
        }
    }
    @IBOutlet var cfmPwdNumTF: SkyFloatingLabelTextField!{
        didSet {
            self.cfmPwdNumTF.font = UIFont(name: appFont, size: 15.0)
            self.cfmPwdNumTF.placeholder = self.cfmPwdNumTF.placeholder?.localized
        }
    }
    @IBOutlet var submitButton: UIButton!{
        didSet{
            self.submitButton.layer.cornerRadius = 4.0
            submitButton.setTitle("Submit".localized, for: .normal)
        }
    }
    
    var apiManager = APIManager()
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        cfmPwdNumTF.isSecureTextEntry = true
        newPwdNumTF.isSecureTextEntry = true
        oldPwdNumTF.isSecureTextEntry = true
    }
    
    
    private func callChangePwdApi(addressDict: Dictionary<String, String>?) {
            
            AppUtility.showLoading(self.view)
            apiManager.ChangePassword(addressDict! as [String : String], onSuccess: { [weak self] response in
                
                DispatchQueue.main.async{
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    let json = JSON(response)
                    println_debug("Change Password Response: \(json)")
                    if let code = json["StatusCode"].int, code == 200 {
                        if let loginInfoDic  = json.dictionaryObject {
                          
                            print(loginInfoDic)
                            let alert = UIAlertController(title: "OSK Vendor".localized, message: json["SuccessMessage"].string ?? "", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK".localized, style: .default) { action -> Void in
                           
                                self?.navigationController?.popViewController(animated: true)
                            })
                            
                            self?.present(alert, animated: true, completion: nil)
                        }
                    }else {
                        let message = json["ErrorList"][0].string
                        let alert = UIAlertController(title: "OSK Vendor".localized, message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: nil))
                        self?.present(alert, animated: true, completion: nil)
                    }
                }
                
                }, onError: { [weak self] message in
                    DispatchQueue.main.async {
                    if let viewLoc = self?.view {
                            AppUtility.hideLoading(viewLoc)
                        }
                    }
            })
            
        }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitClick(_ sender: Any) {
        if oldPwdNumTF.text == ""{
            AppUtility.alert(message: "Please Enter Old Password".localized, title: "", controller: self)
        }
        else if newPwdNumTF.text == ""{
            AppUtility.alert(message: "Please Enter New Password".localized, title: "", controller: self)
        }
        else if newPwdNumTF.text?.count ?? 0 < 7 {
            AppUtility.alert(message: "Please Enter Password With Minimum 8 Character".localized, title: "", controller: self)
        }
        else if cfmPwdNumTF.text == ""{
            AppUtility.alert(message: "Please Enter Confirm Password".localized, title: "", controller: self)
        }
        else if newPwdNumTF.text != cfmPwdNumTF.text{
            AppUtility.alert(message: "Password and Confirm Password does not match".localized, title: "", controller: self)
        }
        else{
            if AppUtility.isConnectedToNetwork(){
                var paramDic  = [String: Any]()
                paramDic["OldPassword"] = oldPwdNumTF.text
                paramDic["NewPassword"] = newPwdNumTF.text
                paramDic["ConfirmNewPassword"] = cfmPwdNumTF.text
                self.callChangePwdApi(addressDict: (paramDic as! Dictionary<String, String>))
            }
            else{
                AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
            }
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ChangePasswordVC : UITextFieldDelegate {
    
    
}
