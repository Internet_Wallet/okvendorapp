//
//  FilterListCollectionViewCell.swift
//  Covid Zay Vendor
//
//  Created by iMac on 20/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class FilterListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var filternameLabel: MarqueeLabel!
    @IBOutlet var checkButton: UIButton!
    
}
