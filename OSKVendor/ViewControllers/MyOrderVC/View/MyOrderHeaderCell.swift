//
//  MyOrderHeaderCell.swift
//  OSKVendor
//
//  Created by OK$ on 23/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class MyOrderHeaderCell: UITableViewCell {

    @IBOutlet var lblCustomer: UILabel!{
        didSet {
            lblCustomer.text = "Customer".localized
        }
    }
    @IBOutlet var lblQTY: UILabel!{
        didSet {
            lblQTY.text = "Qty".localized
        }
    }
    @IBOutlet var lblAmount: UILabel!{
        didSet {
            lblAmount.text = "Amount".localized
        }
    }
    @IBOutlet var lblStatus: UILabel!{
        didSet {
            lblStatus.text = "Status".localized
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
