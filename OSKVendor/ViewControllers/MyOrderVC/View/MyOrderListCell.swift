//
//  MyOrderListCell.swift
//  OSKVendor
//
//  Created by OK$ on 23/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class MyOrderListCell: UITableViewCell {

    @IBOutlet var lblCustomer: MarqueeLabel!
    @IBOutlet var lblQTY: MarqueeLabel!
    @IBOutlet var lblAmount: MarqueeLabel!
    @IBOutlet var lblStatus: MarqueeLabel!{
        didSet {
            lblStatus.layer.cornerRadius = 14//5
            lblStatus.layer.masksToBounds = true
            lblStatus.textColor = .white
        }
    }
    @IBOutlet var btnMore: UIButton!{
           didSet {
            btnMore.setTitle("More Details".localized, for: .normal)
            btnMore.layer.cornerRadius = 14
            btnMore.layer.masksToBounds = true
            
           }
       }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func wrapData(order: NewDatum) {
        lblCustomer.text = order.customerFullName  ?? ""
        lblQTY.text = ""
        lblAmount.text = ""
        lblStatus.text = ""
    
        lblAmount.text = "\(order.orderTotal ?? "0.0 MMK")"
        btnMore.backgroundColor = UIColor.colorWithRedValue(redValue: 0, greenValue: 124, blueValue: 51, alpha: 1)
        
        if let items = order.items {
            var totalQty = 0
            for  item in items {
                
                totalQty += item.quantity ?? 0
                if let qty = item.quantity, qty >= 1 {
                    lblQTY.text = "\(totalQty)"
                    self.lblQTY.isHidden = false
                } else {
                    self.lblQTY.isHidden = true
                }
            }
        }
        
        let orderStatus = order.orderStatus ?? ""
        
        if orderStatus == "Complete".localized {
            lblStatus.text = orderStatus
            lblStatus.backgroundColor = UIColor.colorWithRedValue(redValue: 0, greenValue: 124, blueValue: 51, alpha: 1)
        } else if orderStatus == "Pending".localized {
            lblStatus.text = orderStatus
            lblStatus.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 159, blueValue: 59, alpha: 1)
        } else if orderStatus == "Processing".localized {
            lblStatus.text = orderStatus
            lblStatus.backgroundColor = UIColor.colorWithRedValue(redValue: 86, greenValue: 190, blueValue: 234, alpha: 1)
        } else if orderStatus == "Cancelled".localized {
            lblStatus.text = orderStatus
            lblStatus.backgroundColor = UIColor.colorWithRedValue(redValue: 204, greenValue: 86, blueValue: 66, alpha: 1)
        } else{
            lblStatus.backgroundColor = UIColor.colorWithRedValue(redValue: 0, greenValue: 124, blueValue: 51, alpha: 1)
            lblStatus.text = orderStatus
        }
    }
    
}
