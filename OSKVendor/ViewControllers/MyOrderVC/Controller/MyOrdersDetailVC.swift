//
//  MyOrdersDetailVC.swift
//  OSKVendor
//
//  Created by OK$ on 15/09/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import WebKit

class MyOrdersDetailVC: OSKBaseViewController {

    var selectedProductId = Int()
    var dictOrderDetails: NewDatum?
    @IBOutlet weak var myOrderDetailTbl: UITableView!
    //@IBOutlet var cartBarButton: UIBarButtonItem!
    var itemsArray : [NewItem]?
    var noteDictionary: [Dictionary<String,Any>] = []
    var PaymentMethodStatus : String = ""
    var aPIManager = APIManager()
    var alertVC : SAlertController?
    
    var remarks = ""
   
    @IBOutlet weak var btnComplete: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var constraintBottomView: NSLayoutConstraint!
    @IBOutlet weak var viewFooter: CardView!
    
    //MARK:- View Life  Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Order Details".localized
        self.alertVC = SAlertController()

        itemsArray = dictOrderDetails?.items
        remarks = dictOrderDetails?.remarks ?? ""
        setupUI()

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        println_debug(dictOrderDetails)
        myOrderDetailTbl.estimatedRowHeight = 50
        myOrderDetailTbl.rowHeight = UITableView.automaticDimension
        myOrderDetailTbl.tableFooterView = UIView()
    }
 
    func setupUI() {
        self.btnCancel.layer.cornerRadius = 10
        self.btnComplete.layer.cornerRadius = 10

        let intOrderStatus =  dictOrderDetails?.orderStatusID
        switch intOrderStatus {
        case 10:
            self.btnComplete.setTitle("Accept Order".localized, for: .normal)
            self.btnComplete.isUserInteractionEnabled = true
            self.btnCancel.isHidden = false
            constraintBottomView.constant = 75
            self.viewFooter.isHidden = false
        case 20:
            self.btnComplete.setTitle("Complete".localized, for: .normal)
            self.btnComplete.isUserInteractionEnabled = true
            self.btnCancel.isHidden = false
            constraintBottomView.constant = 75
            self.viewFooter.isHidden = false
        case 30:
            self.btnComplete.setTitle("Completed".localized, for: .normal)
            self.btnComplete.isUserInteractionEnabled = false
            self.btnCancel.isHidden = true
            constraintBottomView.constant = 0
            self.viewFooter.isHidden = true
        case 40:
            self.btnComplete.setTitle("Order Canceled".localized, for: .normal)
            self.btnComplete.isUserInteractionEnabled = false
            self.btnCancel.isHidden = true
            constraintBottomView.constant = 0
            self.viewFooter.isHidden = true
        default:
            self.btnComplete.setTitle("Accept Order".localized, for: .normal)
            self.btnCancel.isHidden = false
        }
    }
    
    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
      }
    
    @IBAction func cartAction(_ sender: UIBarButtonItem) {
        println_debug("Cart Action")
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    static func returnPlainString(getString: String) -> String{
            return getString.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        }

}

extension MyOrdersDetailVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
            return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            if section == 1 {
                return itemsArray?.count ?? 0
            }
            return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderAddressCell", for: indexPath) as! MyOrderDetailTableCell
            cell.wrapAddressData(responseModel: dictOrderDetails!)
            return cell
        } else if indexPath.section == 1 {//Order data
            let cell = tableView.dequeueReusableCell(withIdentifier: "myOrderItemCell", for: indexPath) as! MyOrderDetailTableCell
            cell.wrapData(responseModel: (itemsArray?[indexPath.row])!)
            return cell
        }
        else if indexPath.section == 2 {//Total Amount
            let cell = tableView.dequeueReusableCell(withIdentifier: "TotalAmountCell", for: indexPath) as! MyOrderDetailTableCell
            cell.lblTotalPayable.text = dictOrderDetails?.orderTotal ?? "0.0 MMK"
            
            return cell
        }
        else if indexPath.section == 3 {//Order Placed
            let cell = tableView.dequeueReusableCell(withIdentifier: "shipStatusCell", for: indexPath) as! MyOrderDetailTableCell
            cell.approveStatusDateLabel.text = AppUtility.dateAndTimeOrderPlaced(timeStr:  dictOrderDetails?.createdOn ?? "")
            let orderStatus = dictOrderDetails?.orderStatus ?? ""
            cell.orderPlacedStatusLabel.text = orderStatus
            
            if orderStatus == "Complete" {
                cell.orderPlacedStatusLabel.backgroundColor = UIColor.colorWithRedValue(redValue: 74, greenValue: 164, blueValue: 98, alpha: 1)
            } else if orderStatus == "Pending" {
                cell.orderPlacedStatusLabel.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 159, blueValue: 59, alpha: 1)
            } else if orderStatus == "Processing" {
                cell.orderPlacedStatusLabel.backgroundColor = UIColor.colorWithRedValue(redValue: 86, greenValue: 190, blueValue: 234, alpha: 1)
            } else if orderStatus == "Cancelled" {
                cell.orderPlacedStatusLabel.backgroundColor = UIColor.colorWithRedValue(redValue: 204, greenValue: 86, blueValue: 66, alpha: 1)
            } else{
                cell.orderPlacedStatusLabel.backgroundColor = UIColor.colorWithRedValue(redValue: 231, greenValue: 160, blueValue: 59, alpha: 1)
            }
            return cell
        }
        else if indexPath.section == 4 {//Last Updated
            let cell = tableView.dequeueReusableCell(withIdentifier: "LastUpdatedCell", for: indexPath) as! MyOrderDetailTableCell
            cell.lastUpdatedDateLabel.text = AppUtility.dateAndTimeOrderPlaced(timeStr:  dictOrderDetails?.updatedOn ?? "")
            return cell
        }
        else if indexPath.section == 5 {//Remarks
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderRemarks", for: indexPath) as! MyOrderDetailTableCell
            cell.txtRemarks.isUserInteractionEnabled = true
            
            cell.txtRemarks.addTarget(self, action: #selector(textFieldDidChange(_:)),
                                                  for: UIControl.Event.editingChanged)
            
            cell.txtRemarks.text = remarks
            let intOrderStatus =  dictOrderDetails?.orderStatusID
            if intOrderStatus == 30 || intOrderStatus == 40 {
                cell.txtRemarks.isUserInteractionEnabled = false
            }
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderPaymentCell", for: indexPath) as! MyOrderDetailTableCell
            cell.wrapPaymentData(orderdetails: dictOrderDetails!)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
            if indexPath.section == 0 {
                return 250
            }else if indexPath.section == 1 {
                    return 151
            }else if indexPath.section == 2 {
                return  61
            }else if indexPath.section == 3 {
                return  91
            }else if indexPath.section == 4 {
                return  91
            }
            else {
                let intOrderStatus =  dictOrderDetails?.orderStatusID
                if intOrderStatus == 30 || intOrderStatus == 40 {
                    if dictOrderDetails?.remarks?.count ?? 0 > 0 {
                        return  85
                    }
                    return 0
                }
                return  85
            }
    }
    
    /*
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 5 {
            let intOrderStatus =  dictOrderDetails?.orderStatusID
            switch intOrderStatus {
            case 30:
                return 0
            case 40:
                return 0
            default:
                return 85
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            
        if section == 5 {
            let cell: OrderNoteCell! = tableView.dequeueReusableCell(withIdentifier: "OrderNoteCell") as? OrderNoteCell
            cell.btnCancel.layer.cornerRadius = 10
            //cell.btnCancel.layer.borderWidth = 1
            //cell.btnCancel.layer.borderColor = UIColor(red: 183/255, green: 28/255, blue: 28/255, alpha: 1.0).cgColor
            //cell.lblTotalPayable.text = dictOrderDetails?.orderTotal ?? "0.0 MMK"
            cell.btnCancel.addTarget(self, action:#selector(self.btnOrderCancelAction(sender:)), for: .touchUpInside)
            cell.btnPaymentStatus.addTarget(self, action:#selector(self.btnOrderPaymentAction(sender:)), for: .touchUpInside)
            
            let intOrderStatus =  dictOrderDetails?.orderStatusID
            switch intOrderStatus {
            case 10:
                cell.btnPaymentStatus.setTitle("Accept Order".localized, for: .normal)
                cell.btnPaymentStatus.isUserInteractionEnabled = true
                cell.btnCancel.isHidden = false
            case 20:
                cell.btnPaymentStatus.setTitle("Complete".localized, for: .normal)
                cell.btnPaymentStatus.isUserInteractionEnabled = true
                cell.btnCancel.isHidden = false
            case 30:
                cell.btnPaymentStatus.setTitle("Completed".localized, for: .normal)
                cell.btnPaymentStatus.isUserInteractionEnabled = false
                cell.btnCancel.isHidden = true
            case 40:
                cell.btnPaymentStatus.setTitle("Order Canceled".localized, for: .normal)
                cell.btnPaymentStatus.isUserInteractionEnabled = false
                cell.btnCancel.isHidden = true
            default:
                cell.btnPaymentStatus.setTitle("Accept Order".localized, for: .normal)
                cell.btnCancel.isHidden = false
            }
            
            return cell
        } else {
            let view = UIView(frame: CGRect(x: 0, y: 0, width:200, height: 1))
            view.backgroundColor = .clear
            return view
        }
    }
    */
    
    //Mark:- Custom Methods
    //TextFieldDelegate
    @objc private func textFieldDidChange(_ textField: UITextField) {
        //fixed text field with two prefix 09
        remarks = textField.text ?? ""
        let cell = myOrderDetailTbl.cellForRow(at: IndexPath.init(row: 0, section: 5)) as? MyOrderDetailTableCell
        cell?.txtRemarks.becomeFirstResponder()
    }
    
    @IBAction func btnOrderCancelAction(sender: UIButton!) {
        println_debug("btnOrderCancelAction")
            self.alertVC?.ShowSAlert(title: "Order Cancel".localized, withDescription: "Are you sure you want to cancel this order?".localized, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "YES".localized, AlertType: .defualt, withComplition: {
                self.updateOrderDetails(orderType:"Cancel")
            })
            let cancelAction = SAlertAction()
            cancelAction.action(name: "NO".localized, AlertType: .defualt, withComplition: {
            })
            self.alertVC?.addAction(action: [YesAction,cancelAction])
    }
    
    @IBAction func btnOrderPaymentAction(sender: UIButton!) {
           println_debug("btnOrderPaymentAction")
        self.updateOrderDetails(orderType:"UpdateOrder")
    }
    
    private func updateOrderDetails(orderType: String) {

        //Get Remarks text
        
        var intOrderStatusId = dictOrderDetails?.orderStatusID ?? 0
        var urlString = String(format: "%@/vendor/v1/changeorderstatus", APIManagerClient.sharedInstance.base_url,"\(dictOrderDetails?.id ?? 0)","\(intOrderStatusId)")
        var params = ["OrderId": "\(dictOrderDetails?.id ?? 0)","OrderStatusId": "\(intOrderStatusId)","Remarks": remarks]

        if orderType == "Cancel" {
            urlString = String(format: "%@/vendor/v1/changeorderstatus", APIManagerClient.sharedInstance.base_url,"\(dictOrderDetails?.id ?? 0)")
            params = ["OrderId": "\(dictOrderDetails?.id ?? 0)","OrderStatusId": "40","Remarks": remarks]
        } else {
            intOrderStatusId = intOrderStatusId + 10
            urlString = String(format: "%@/vendor/v1/changeorderstatus", APIManagerClient.sharedInstance.base_url,"\(dictOrderDetails?.id ?? 0)","\(intOrderStatusId)")
            params = ["OrderId": "\(dictOrderDetails?.id ?? 0)","OrderStatusId": "\(intOrderStatusId)","Remarks": remarks]
        }
       
           guard let url = URL(string: urlString) else { return }
        
           AppUtility.showLoading(self.view)
           aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self] (response, success, data) in
               DispatchQueue.main.async {
                   if let viewLoc = self?.view {
                       AppUtility.hideLoading(viewLoc)
                   }
               }
               if success {
                   do {
                       let decoder = JSONDecoder()
                       guard let dataVal = data else { return }
                       let model = try decoder.decode(MyOrder.self, from: dataVal)
                       if let statusCode = model.statusCode, statusCode == 200 {
//                        DispatchQueue.main.async {
//                        self?.showToast(message: "Your Order Updated Successfully!")
//                        self?.navigationController?.popViewController(animated: true)
//                        }
                        DispatchQueue.main.async {
                             let alertOrder = SAlertController()
                            alertOrder.ShowSAlert(title: "Order Status", withDescription: "Your Order Updated Successfully!".localized, onController: self!)
                            let YesAction = SAlertAction()
                            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                self?.navigationController?.popViewController(animated: true)
                            })
                            alertOrder.addAction(action: [YesAction])
                        }
                       } else {
                           println_debug("Error")
                        self?.showAlert(alertTitle: "Please try again!", description: "")
                       }
                   } catch _ {
                       println_debug("Parse error")
                       DispatchQueue.main.async {
                           if let viewLoc = self?.view {
                               AppUtility.hideLoading(viewLoc)
                           }
                       }
                   }
               } else {
                   DispatchQueue.main.async {
                       
                   }
               }
           }
       }
}

//Mark:- Custom Cell
class MyOrderDetailTableCell: UITableViewCell {
    
    //Order Item List
    @IBOutlet weak var nameLabel: UILabel!{
         didSet {
              self.nameLabel.font = UIFont(name: appFont, size: 15.0)
             self.nameLabel.text = self.nameLabel.text?.localized
         }
     }
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet var webHeightconstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblAttributeInfo: UILabel!{
        didSet {
            self.lblAttributeInfo.font = UIFont(name: appFont, size: 15.0)
           self.lblAttributeInfo.text = "Attribute not available".localized
       }
    }
    @IBOutlet weak var lblLTotal: UILabel!
    @IBOutlet weak var lblSellerName: UILabel!
    @IBOutlet weak var lblPriceWithTax: UILabel!
    @IBOutlet weak var lblAttribute: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var qtyLabel: UILabel!{
           didSet {
               self.qtyLabel.font = UIFont(name: appFont, size: 15.0)
               self.qtyLabel.text = self.qtyLabel.text?.localized
           }
       }
    //Order Placed
    @IBOutlet weak var orderPlacedLabel: UILabel!{
        didSet {
            self.orderPlacedLabel.font = UIFont(name: appFont, size: 15.0)
            self.orderPlacedLabel.text = "Order Placed".localized
        }
    }
    @IBOutlet weak var orderPlacedStatusLabel: UILabel!{
        didSet{
            self.orderPlacedStatusLabel.layer.cornerRadius = 10
            self.orderPlacedStatusLabel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var approveStatusDateLabel: UILabel!{
          didSet {
              self.approveStatusDateLabel.font = UIFont(name: appFont, size: 15.0)
          }
      }
    //16,Jan2021-Gauri
    @IBOutlet weak var lastUpdatedDateTitleLabel: UILabel!{
          didSet {
              self.lastUpdatedDateTitleLabel.font = UIFont(name: appFont, size: 15.0)
            self.lastUpdatedDateTitleLabel.text = "Last Updated".localized
          }
      }
    @IBOutlet weak var lastUpdatedDateLabel: UILabel!{
          didSet {
              self.lastUpdatedDateLabel.font = UIFont(name: appFont, size: 15.0)
          }
      }
    @IBOutlet weak var lblTotalPayable: UILabel!
    @IBOutlet weak var lblTotalPayableTitle: UILabel!{
        didSet {
            self.lblTotalPayableTitle.font = UIFont(name: appFont, size: 15.0)
            self.lblTotalPayableTitle.text = "Total Amount".localized
        }
    }
    @IBOutlet weak var txtRemarks: SkyFloatingLabelTextField!

    //Delivery Address
    @IBOutlet weak var addDetailLbl: UILabel!{
           didSet {
               self.addDetailLbl.font = UIFont(name: appFont, size: 20.0)
               self.addDetailLbl.text = "Delivery Address".localized
           }
       }
    @IBOutlet weak var addressNameLabel: MarqueeLabel!{
           didSet {
               self.addressNameLabel.font = UIFont(name: appFont, size: 18.0)
           }
       }
    @IBOutlet weak var addressLabel: UILabel!{
        didSet {
            self.addressLabel.font = UIFont(name: appFont, size: 17.0)
            self.addressLabel.text = self.addressLabel.text?.localized
            
        }
    }
    @IBOutlet weak var addressPhoneButton: UIButton!
    
    @IBOutlet weak var isLiftOptionLabel: UILabel!{
        didSet {
            self.isLiftOptionLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var addressPhoneLabel: UILabel!{
           didSet {
                self.addressPhoneLabel.font = UIFont(name: appFont, size: 17.0)
           }
       }
    //PaymentDetails
    @IBOutlet weak var paymntDetailLbl: UILabel!{
           didSet {
               self.paymntDetailLbl.font = UIFont(name: appFont, size: 15.0)
               self.paymntDetailLbl.text = "Payment Details ".localized
           }
       }
    @IBOutlet weak var paymentStatusLbl: UILabel!
    
    @IBOutlet weak var orderNumLable: UILabel!{
           didSet {
               self.orderNumLable.font = UIFont(name: appFont, size: 15.0)
               self.orderNumLable.text = "Order No".localized
           }
       }
       @IBOutlet weak var orderNumValueLable: UILabel!{
           didSet {
               self.orderNumValueLable.font = UIFont(name: appFont, size: 15.0)
           }
       }
    @IBOutlet weak var totalQuantityLabel: UILabel!{
        didSet {
            self.totalQuantityLabel.font = UIFont(name: appFont, size: 15.0)
            self.totalQuantityLabel.text = "Total Quantity".localized
        }
    }
    @IBOutlet weak var totalQuantityValueLabel: UILabel!
    @IBOutlet weak var productAmtLabel: UILabel!{
        didSet {
            self.productAmtLabel.font = UIFont(name: appFont, size: 15.0)
            self.productAmtLabel.text = self.productAmtLabel.text?.localized
        }
    }
    @IBOutlet weak var productAmtValueLabel: UILabel!
    @IBOutlet weak var shippingStatusLbl: UILabel!{
          didSet {
              self.shippingStatusLbl.font = UIFont(name: appFont, size: 15.0)
              self.shippingStatusLbl.text = self.shippingStatusLbl.text?.localized
          }
      }
    @IBOutlet weak var shippingStatusValueLabel: UILabel!
    @IBOutlet weak var totalAmtLabel: UILabel!{
           didSet {
               self.totalAmtLabel.font = UIFont(name: appFont, size: 15.0)
               self.totalAmtLabel.text = self.totalAmtLabel.text?.localized
           }
       }
    @IBOutlet weak var totalAmtValueLabel: UILabel!{
        didSet {
            self.totalAmtLabel.font = UIFont(name: appFont, size: 15.0)
            self.totalAmtLabel.text = self.totalAmtLabel.text?.localized
        }
    }
   
    @IBOutlet weak var paymentModeLabel: UILabel!{
        didSet {
            self.paymentModeLabel.font = UIFont(name: appFont, size: 15.0)
            self.paymentModeLabel.text = self.paymentModeLabel.text?.localized
        }
    }
    @IBOutlet weak var paymentModeValueLabel: UILabel!

    func wrapData(responseModel: NewItem) {
        if responseModel.attributeInfo?.count ?? 0 > 0 {
            webHeightconstraint.constant = 42.0
            webView.isHidden = false
            webView.loadHTMLString((responseModel.attributeInfo?.count ?? 0 > 0) ? responseModel.attributeInfo ?? "" : "", baseURL: nil)
        }
        else{
            webHeightconstraint.constant = 0.0
            webView.isHidden = true
        }
     
        //lblAttributeInfo.text = (responseModel.attributeInfo?.count ?? 0 > 0) ? MyOrdersDetailVC.returnPlainString(getString:responseModel.attributeInfo ?? "") : "Attribute not available"//"Consult Specialities: Consultant Physician<br />ColorSquaresRgb: #FFFFFF"//
        nameLabel.text = responseModel.productName ?? ""
        lblPriceWithTax.text = "Price: \(responseModel.unitPriceInclTax ?? "")"
        //lblSellerName.text = "Seller: \(responseModel.vendorName ?? "")"

        
        if let productImageURL = responseModel.pictureThumbnailURL , !productImageURL.isEmpty {
                 itemImageView.sd_setImage(with: URL(string: productImageURL))
             }
             else {
                 itemImageView.image = UIImage(named: "no-image")
             }
        
        //itemImageView.setImageUsingUrl(responseModel.pictureThumbnailURL ?? "")
        itemImageView?.layer.cornerRadius = (self.itemImageView?.frame.width ?? 0) / 2
        itemImageView?.layer.masksToBounds = true
        
        if let qty = responseModel.quantity { //, qty > 1 {
            self.qtyLabel.text = "\("Qty".localized): \(qty)"
            let intTotal = qty * (responseModel.unitPriceInclTaxValue ?? 0)
            lblLTotal.text = "Total: \(intTotal)" + " MMK"
        } else {
            self.qtyLabel.isHidden = true
        }
    }
    
    func wrapPaymentData(orderdetails: NewDatum) {
        if let items = orderdetails.items {
                   var totalQty = 0
                   for  item in items {
                       totalQty += item.quantity ?? 0
                       self.totalQuantityValueLabel.text = "\(totalQty)"
                   }
               }
        
        self.orderNumValueLable.text = orderdetails.customOrderNumber
        self.productAmtValueLabel.text  = orderdetails.orderTotal
        self.shippingStatusValueLabel.text = orderdetails.shippingStatus
        self.totalAmtValueLabel.text = orderdetails.orderTotal
        self.paymentModeValueLabel.text = "COD"
        
        self.paymentStatusLbl.textColor = UIColor.colorWithRedValue(redValue: 231, greenValue: 160, blueValue: 59, alpha: 1)
        self.paymentStatusLbl.text = orderdetails.paymentStatus
    }
    
    @objc func btnDeliveryNumberAction(sender: UIButton) {
        println_debug("btnDeliveryNumberAction")
        guard let number = URL(string: "tel://" + sender.accessibilityHint!) else { return }
        UIApplication.shared.open(number)
    }
       
    func wrapAddressData(responseModel: NewDatum) {
        self.addressNameLabel.text = responseModel.customerFullName
        //self.customerEmailIdButton.setTitle(responseModel.customerEmail, for: .normal)
        self.addressPhoneButton.setTitle(responseModel.customerPhoneNumber, for: .normal)
        if responseModel.customerPhoneNumber?.count ?? 0 > 0 {
            self.addressPhoneButton.setTitle(responseModel.customerPhoneNumber?.replacingOccurrences(of: "0095", with: "(+95)0"), for: .normal)
            self.addressPhoneButton.addTarget(self, action:#selector(self.btnDeliveryNumberAction(sender:)), for: .touchUpInside)
            self.addressPhoneButton.accessibilityHint = responseModel.customerPhoneNumber
        }
        
        if responseModel.shippingAddress?.isLiftOption == true {
            self.isLiftOptionLabel.text = "Lift Available"
            if let ropeColor = responseModel.shippingAddress?.ropeColor, ropeColor.count > 0 {
                self.isLiftOptionLabel.text = "Lift Not Available | Rope Color : \(ropeColor)"
            }
        } else {
            self.isLiftOptionLabel.text = "Lift Not Available"
            if let ropeColor = responseModel.shippingAddress?.ropeColor, ropeColor.count > 0 {
                self.isLiftOptionLabel.text = "Lift Not Available | Rope Color : \(ropeColor)"
            }
        }
        
        let houseNo = responseModel.shippingAddress?.houseNo ?? ""
        let floorNo = responseModel.shippingAddress?.floorNo ?? ""
        let roomNo = responseModel.shippingAddress?.roomNo ?? ""
        let address1 = responseModel.shippingAddress?.address1 ?? ""
        let address2 = responseModel.shippingAddress?.address2 ?? ""
        let city = responseModel.shippingAddress?.city ?? ""
        let state = responseModel.shippingAddress?.stateProvinceName ?? ""
        let country = responseModel.shippingAddress?.countryName ?? ""
            
            var fullAddres = ""
            var house = ""
            var floor = ""
            var room = ""
            
            if houseNo == "House No.".localized {
                house = ""
                //  fullAddres += house
            }
            else if houseNo.count > 0 && houseNo != "non"
            {
                house = "House No " + houseNo + ", "
                fullAddres += house
            }
            
            if floorNo == "Floor No.".localized {
                floor = ""
            }
            else if floorNo.count > 0  && floorNo != "non" {
                floor = "Floor No " + floorNo + ", "
                fullAddres += floor
            }
            
            if roomNo == "Room No.".localized {
                room = ""
            }
            else if roomNo.count > 0  && roomNo != "non"
            {
                room = "Room No " + roomNo +  ", "
                fullAddres += room
            }
            if address1 != "" {
                fullAddres += address1 + ", "
            }
            if address2 != "" {
                fullAddres += address2 + ", "
            }
            if city != "" {
                fullAddres += city + ", "
            }
            if state != "" {
                fullAddres += state + ", "
            }
            if country != "" {
                fullAddres += country
            }
            
            self.addressLabel.text = fullAddres
    }
}

class OrderNoteCell: UITableViewCell {
    
    @IBOutlet weak var btnPaymentStatus: UIButton!
    @IBOutlet weak var btnCancel: UIButton!{
        didSet {
            btnCancel.setTitle("Cancel".localized, for: .normal)
        }
    }
    @IBOutlet weak var lblTotalPayable: UILabel!
    @IBOutlet weak var lblTitleTotalPayable: UILabel!{
        didSet {
            self.lblTitleTotalPayable.font = UIFont(name: appFont, size: 15.0)
            self.lblTitleTotalPayable.text = "Total Payable".localized
        }
    }
}
