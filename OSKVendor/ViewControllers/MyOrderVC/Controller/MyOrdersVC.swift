//
//  MyOrdersVC.swift
//  OSKVendor
//
//  Created by OK$ on 14/09/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class MyOrdersVC: OSKBaseViewController {
    
    @IBOutlet var MyOrderTableView: UITableView!
    @IBOutlet weak var noRecordFoundlbl: UILabel!{
        didSet {
            self.noRecordFoundlbl.font = UIFont(name: appFont, size: 15.0)
            self.noRecordFoundlbl.text = self.noRecordFoundlbl.text?.localized
        }
    }
    @IBOutlet weak var noRecordView: UIView! {
        didSet {
           // self.noRecordView.isHidden = false
        }
    }
    @IBOutlet var noimageview: UIImageView!
    
    @IBOutlet var cartBarButton: UIBarButtonItem!
    @IBOutlet var sortBarButton: UIBarButtonItem!

    var ordersArray = [NewDatum]()
    var filtersearchArray = [NewDatum]()
    var aPIManager = APIManager()
    var selectedProduct = -1
    
    @IBOutlet var filterView: UIView!
    
    @IBOutlet var filterHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var filterorderLabel: UILabel!{
        didSet{
            filterorderLabel.text = "Filter Order".localized
        }
    }
    
    @IBOutlet var cancelButton: UIButton!{
        didSet{
            cancelButton.setTitle("CANCEL".localized, for: .normal)
        }
    }
    @IBOutlet var applyButton: UIButton!{
        didSet{
            applyButton.setTitle("APPLY".localized, for: .normal)
        }
    }
    
    @IBOutlet var filterCollectionView: UICollectionView!
    
    var filterArray = [String]()
    var selectedPaths=Set<IndexPath>()
    var rowsWhichAreChecked = [IndexPath]()
    
    var rowSelected = 0
    
    var filterselectedstring = ""
    var filterStatus = ""
    
    //MARK:- View Life  Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getOrderDetails()
        self.title = "MY ORDERS".localized
        self.cartBarButton = UIBarButtonItem.init(badge: (UserDefaults.standard.string(forKey: "badge") ?? ""), title: "", target: self, action: #selector(self.cartAction(_:)))
        
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "sort"), for: .normal)
        button.addTarget(self, action: #selector(searchClick), for: .touchUpInside)
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItems = [self.cartBarButton,item]
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "MY ORDERS".localized
        noRecordFoundlbl.isHidden = true
        noimageview.isHidden = true
        
        filterView.isHidden = true
        filterHeightConstraint.constant = 0
        
        filterArray = ["All".localized,"Pending".localized,"Processing".localized,"Complete".localized,"Cancelled".localized]
        
        filterView.layer.borderWidth = 1.0
        filterView.layer.borderColor = UIColor.lightGray.cgColor
        filterView.clipsToBounds = true
        filterView.layer.cornerRadius = 20
        filterView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        filterView.layer.shadowColor = UIColor.black.cgColor
        filterView.layer.shadowOpacity = 1
        filterView.layer.shadowOffset = CGSize(width: -0.5, height: -3.0)
        /* since we dont want shadow at bottom */
        filterView.layer.shadowRadius = 2
        filterView.layer.shadowPath = UIBezierPath(roundedRect: filterView.bounds,
        cornerRadius: 10).cgPath
        
        cancelButton.layer.cornerRadius = 8.0
        cancelButton.layer.masksToBounds = true
        cancelButton.layer.borderWidth = 1.0
        cancelButton.layer.borderColor = UIColor.systemGreen.cgColor
        
        applyButton.layer.cornerRadius = 8.0
        applyButton.layer.masksToBounds = true
    
    }
    
    @IBAction func searchClick(_ sender: UIBarButtonItem) {
        println_debug("searchClick")
        filterView.isHidden = false
        filterHeightConstraint.constant = 200
    }
    
    @IBAction func cartAction(_ sender: UIBarButtonItem) {
        println_debug("Cart Action")
    }
    
    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func cancelButtonClick(_ sender: Any) {
        
        //MyOrderTableView.reloadData()
        
        filterView.isHidden = true
        filterHeightConstraint.constant = 0
      //  rowSelected = 0
        //filterselectedstring = ""
        //filterCollectionView.reloadData()
    }
    
    @IBAction func applyButtonClick(_ sender: Any) {
        
        filtersearchArray.removeAll()
        
        if filterselectedstring == "All".localized {
            filterselectedstring = ""
        } else {
            for element in ordersArray {
                
                if element.orderStatus?.localized == filterselectedstring.localized {
                    filtersearchArray.append(element)
                }
            }
        }
        print(filtersearchArray)
        MyOrderTableView.reloadData()
       
        filterView.isHidden = true
        filterHeightConstraint.constant = 0
       // rowSelected = 0
        filterCollectionView.reloadData()
        //filterselectedstring = ""
    }
    
    
}

//MARK:- Web API Delegate
extension MyOrdersVC {
    
    private func parseItems(model: LocationData) {
        DispatchQueue.main.async {
            if let orderArray = model.pickUpOrderListModel?.data, orderArray.count > 0 {
                
                if  model.pickUpOrderListModel?.total == 0 {
                    UserDefaults.standard.set("", forKey: "badge")
                    //self.lblOrderCount.isHidden = true
                }else{
                    //self.lblOrderCount.isHidden = false
                    //self.lblOrderCount.text = "\(model.pickUpOrderListModel?.total ?? 0)"
                   UserDefaults.standard.set("\(model.pickUpOrderListModel?.total ?? 0)", forKey: "badge")
                }
                UserDefaults.standard.synchronize()
                self.cartBarButton = UIBarButtonItem.init(badge: (UserDefaults.standard.string(forKey: "badge") ?? ""), title: "", target: self, action: #selector(self.cartAction(_:)))
               // self.navigationItem.rightBarButtonItems = [self.cartBarButton]
                let button = UIButton()
                button.setImage(#imageLiteral(resourceName: "sort"), for: .normal)
                button.addTarget(self, action: #selector(self.searchClick), for: .touchUpInside)
                //        button.imageEdgeInsets.left = -35
                let item = UIBarButtonItem(customView: button)
                self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
                //self.navigationItem.rightBarButtonItem = item
                
                self.navigationItem.rightBarButtonItems = [self.cartBarButton,item]
                
                self.ordersArray = orderArray
                self.filtersearchArray = orderArray
                self.MyOrderTableView.reloadData()
            } else {
               // self.noRecordView.isHidden = false
                self.noRecordFoundlbl.isHidden = false
                self.noimageview.isHidden = false
                self.MyOrderTableView.isHidden = true
            }
        }
    }
    
    private func getOrderDetails() {
        let urlString = String(format: "%@/vendor/orders?pageNumber=1&pageSize=50", APIManagerClient.sharedInstance.base_url)
        guard let url = URL(string: urlString) else { return }
        let params = Dictionary<String, Any>()
        AppUtility.showLoading(self.view)
        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", header: true) { [weak self] (response, success, data) in
            DispatchQueue.main.async {
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
            }
            if success {
                do {
                    let decoder = JSONDecoder()
                    guard let dataVal = data else { return }
                     let json = response as? Dictionary<String,Any>
                    println_debug(json)
                    guard let jsonData = data as? Data else {
                        println_debug("Data error")
                        return
                    }
                   
                        // make sure this JSON is in the format we expect
                        if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? NSDictionary {
                            let newObj = LocationData(response: json)
                            
                            if newObj.statusCode == 200 {
                                self?.parseItems(model:newObj)
                            } else {
                                println_debug("Error")
                            }
                        }
                    
                    
                    ///Old model
//                    let model = try decoder.decode(MyOrder.self, from: dataVal)
//                    if let statusCode = model.statusCode, statusCode == 200 {
//                        //self?.parseItems(model: model)
//                    } else {
//                        println_debug("Error")
//                    }
                } catch _ {
                    println_debug("Parse error")
                    DispatchQueue.main.async {
                        if let viewLoc = self?.view {
                            AppUtility.hideLoading(viewLoc)
                        }
                    }
                }
            } else {
                DispatchQueue.main.async {
                   // self?.noRecordView.isHidden = false
                    self?.noRecordFoundlbl.isHidden = true
                    self?.noimageview.isHidden = true
                    self?.MyOrderTableView.isHidden = true
                }
            }
        }
    }
}

//MARK:- Table View Delegate
extension MyOrdersVC : UITableViewDataSource, UITableViewDelegate {
    
    // Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      
        let identifier = "MyOrderHeaderCell"
        var cellView: MyOrderHeaderCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? MyOrderHeaderCell
        if cellView == nil {
          tableView.register (UINib(nibName: "MyOrderHeaderCell", bundle: nil), forCellReuseIdentifier: identifier)
          cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? MyOrderHeaderCell
        }
        return cellView
      }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if filterselectedstring != "" {
            if self.filtersearchArray.count == 0{
              //  noRecordView.isHidden = false
                noRecordFoundlbl.isHidden = false
                noimageview.isHidden = false
                MyOrderTableView.isHidden = true
            }else{
                // noRecordView.isHidden = true
                noRecordFoundlbl.isHidden = true
                noimageview.isHidden = true
                MyOrderTableView.isHidden = false
                tableView.backgroundView = nil
            }
        }
        else{
            if self.ordersArray.count == 0{
              //  noRecordView.isHidden = false
                noRecordFoundlbl.isHidden = false
                noimageview.isHidden = false
                MyOrderTableView.isHidden = true
            }else{
                // noRecordView.isHidden = true
                noRecordFoundlbl.isHidden = true
                noimageview.isHidden = true
                MyOrderTableView.isHidden = false
                tableView.backgroundView = nil
            }
        }
      
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if filterselectedstring != "" {
            return filtersearchArray.count
        }
        return self.ordersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrderListCell", for: indexPath) as! MyOrderListCell
        cell.btnMore.tag = indexPath.row
        cell.btnMore.addTarget(self, action: #selector(viewMoreDetailsAction), for: .touchUpInside)
        
        if filterselectedstring != "" {
            let dict = filtersearchArray[indexPath.row]
            cell.wrapData(order: dict)
        } else {
            let arrItem = self.ordersArray[indexPath.row]
            cell.wrapData(order: arrItem)
        }
       
        return cell
    }
    
    @objc func viewMoreDetailsAction(sender:UIButton){
        selectedProduct = sender.tag
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let myOrderDetailVC = mainStoryBoard.instantiateViewController(withIdentifier: "MyOrdersDetailVC") as! MyOrdersDetailVC
        if filterselectedstring != "" {
            myOrderDetailVC.dictOrderDetails = self.filtersearchArray[sender.tag]
        }
        else{
            myOrderDetailVC.dictOrderDetails = self.ordersArray[sender.tag]
        }
       
        myOrderDetailVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(myOrderDetailVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedProduct = indexPath.row
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let myOrderDetailVC = mainStoryBoard.instantiateViewController(withIdentifier: "MyOrdersDetailVC") as! MyOrdersDetailVC
        
        if filterselectedstring != "" {
            myOrderDetailVC.dictOrderDetails = self.filtersearchArray[indexPath.row]
        }
        else{
            myOrderDetailVC.dictOrderDetails = self.ordersArray[indexPath.row]
        }
        myOrderDetailVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(myOrderDetailVC, animated: true)
    }
    
    private func tableView(_ tableView: UITableView, heightForHeaderInSection indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60//UITableView.automaticDimension
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? MyOrdersDetailVC {
            vc.selectedProductId = ordersArray[selectedProduct].id ?? 0
        }
    }
}

//MARK:- MyOrderCustom Cell
class MyOrderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblAndMoreConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblStatus: MarqueeLabel!
    @IBOutlet weak var lblAndMore: MarqueeLabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemName: MarqueeLabel!{
        didSet {
            self.itemName.font = UIFont(name: appFont, size: 15.0)
            self.itemName.text = self.itemName.text?.localized
        }
    }
 
    @IBOutlet weak var itemPrice: MarqueeLabel!{
        didSet {
            self.itemPrice.font = UIFont(name: appFont, size: 15.0)
            self.itemPrice.text = self.itemPrice.text?.localized
        }
    }
    @IBOutlet weak var qtyLabel: MarqueeLabel!{
        didSet {
            self.qtyLabel.font = UIFont(name: appFont, size: 15.0)
            self.qtyLabel.text = self.qtyLabel.text?.localized
        }
    }
    
    @IBOutlet weak var orderNumberLabel: MarqueeLabel!{
        didSet {
            self.orderNumberLabel.font = UIFont(name: appFont, size: 15.0)
            self.orderNumberLabel.text = self.orderNumberLabel.text?.localized
        }
    }
    
    let imageCache = NSCache<AnyObject, AnyObject>()
    var images = [UIImage]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func wrapData(order: Datum) {
        
        var nameTxt = ""
        var imageArray = [String]()
        self.itemImageView?.layer.cornerRadius = (self.itemImageView?.frame.width ?? 0) / 2
        self.itemImageView?.layer.masksToBounds = true
        self.lblStatus.layer.cornerRadius = 10.0
        self.lblStatus?.layer.masksToBounds = true

        itemImageView.animationImages = []
        self.images = []
        if let items = order.items {
            if items.count > 3 {
                for (index, item) in items.enumerated() {
                    nameTxt = "\(nameTxt), \(item.productName ?? "")"
                    imageArray.append(item.pictureThumbnailURL ?? "")
                    if index == 2 {
                        break
                    }
                }
            } else {
                for item in items {
                    nameTxt = "\(nameTxt), \(item.productName ?? "")"
                    imageArray.append(item.pictureThumbnailURL ?? "")
                }
            }
        }
        
        let _ = nameTxt.remove(at: nameTxt.startIndex)
        itemName.text = String(nameTxt)
        itemPrice.text = "Total: \(order.orderTotal ?? "0.0 MMK")"
        itemPrice.amountAttributedString()
        self.orderNumberLabel.text = "Ord No: \(order.customOrderNumber ?? "1 Stop Kitchen")"
        
        if order.items?.count ?? 0 > 1 {
            self.lblAndMore.isHidden = false
            self.lblAndMoreConstraint.constant = 77
        } else {
            self.lblAndMore.isHidden = true
            self.lblAndMoreConstraint.constant = 0
        }
        
        if let items = order.items {
            var totalQty = 0
            for  item in items {
                
                totalQty += item.quantity ?? 0
                if let qty = item.quantity, qty >= 1 {
                    qtyLabel.text = "Quantity: \(totalQty)"
                    self.qtyLabel.isHidden = false
                } else {
                    self.qtyLabel.isHidden = true
                }
            }
        }
        
        if imageArray.count > 0 {
            for (_, imageUrl) in imageArray.enumerated() {
                cacheImage(urlString: imageUrl) { (image) in
                    if let img = image {
                        self.images.append(img)
                    }
                    if imageArray.count == 1 {
                        self.itemImageView.image = self.images.first
                    } else {
                        self.itemImageView.animationImages = self.images
                        self.itemImageView.animationDuration = 3.0
                        self.itemImageView.startAnimating()
                    }
                }
            }
        }
        if UIDevice().screenType == .iPhones_4_4S || UIDevice().screenType == .iPhones_5_5s_5c_SE {
            itemName.font = UIFont(name: appFont, size: 16.0)!
        }
        
        let orderStatus = order.orderStatus ?? ""
        
        if orderStatus == "Complete".localized {
            lblStatus.text = " Status: ".localized + orderStatus
            lblStatus.backgroundColor = UIColor.colorWithRedValue(redValue: 74, greenValue: 164, blueValue: 98, alpha: 1)
        } else if orderStatus == "Pending".localized {
            lblStatus.text = " Status: ".localized + orderStatus
            lblStatus.backgroundColor = UIColor.colorWithRedValue(redValue: 231, greenValue: 160, blueValue: 59, alpha: 1)
        } else if orderStatus == "Processing".localized {
            lblStatus.text = " Status: ".localized + orderStatus
            lblStatus.backgroundColor = UIColor.colorWithRedValue(redValue: 86, greenValue: 190, blueValue: 234, alpha: 1)
        } else if orderStatus == "Cancelled".localized {
            lblStatus.text = " Status: ".localized + orderStatus
                   lblStatus.backgroundColor = UIColor.colorWithRedValue(redValue: 204, greenValue: 86, blueValue: 66, alpha: 1)
        } else{
            lblStatus.backgroundColor = UIColor.colorWithRedValue(redValue: 231, greenValue: 160, blueValue: 59, alpha: 1)
            lblStatus.text = " Status: ".localized + orderStatus
        }
    }
    
    func cacheImage(urlString: String, _ completion: @escaping(_ image: UIImage?) -> Void) {
        guard let url = URL(string: urlString) else { return }
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            completion(imageFromCache)
        } else {
            URLSession.shared.dataTask(with: url) {
                data, response, error in
                if let dataVal = data {
                    DispatchQueue.main.async {
                        if let imageToCache = UIImage(data: dataVal) {
                            self.imageCache.setObject(imageToCache, forKey: urlString as AnyObject)
                            completion(imageToCache)
                        }
                    }
                }
            }.resume()
        }
    }
}

extension MyOrdersVC : UICollectionViewDelegate, UICollectionViewDataSource{
   
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! FilterListCollectionViewCell
        
        cell.filternameLabel.text = filterArray[indexPath.row]
        
        if rowSelected == indexPath.row {
            cell.checkButton.setImage(UIImage(named: "orangecircle"), for: .normal)
        }
        else{
            cell.checkButton.setImage(UIImage(named: "greencircle"), for: .normal)
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        rowSelected = indexPath.row
        filterselectedstring = filterArray[indexPath.row]
        filterCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
      //  if let obj = filterArray{
            let label = UILabel(frame: CGRect.zero)
            label.text = filterArray[indexPath.row]
            label.sizeToFit()
            if label.frame.size.width<80{
                return CGSize(width: 100 , height: 40)
            }else{
                return CGSize(width:  label.frame.size.width + 30, height: 40)
            }
//        }else{
//            return CGSize(width: 100 , height: 40)
//        }
//
        
        
        
     //   return CGSize(width: UIScreen.main.bounds.width, height: 50.0)
    }

    // item spacing = vertical spacing in horizontal flow
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return (UIScreen.main.bounds.width)
    }

    // line spacing = horizontal spacing in horizontal flow
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return (UIScreen.main.bounds.width)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: (UIScreen.main.bounds.width / 2.0), bottom: 0, right: (UIScreen.main.bounds.width / 2.0))
    }
    
    
}
