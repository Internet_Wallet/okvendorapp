// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   var locationData = try? newJSONDecoder().decode(LocationData.self, from: jsonData)

import Foundation

// MARK: - LocationData

class LocationData {
    var pickUpOrderListModel: NewPickUpOrderListModel?
    var successMessage: String?
    var statusCode: Int?

    init(response: NSDictionary?){
        
        if let hasValue = response{
            if hasValue.count>0{
                self.statusCode = hasValue["StatusCode"] as? Int
                self.successMessage = hasValue["SuccessMessage"] as? String
                self.pickUpOrderListModel = NewPickUpOrderListModel(response: hasValue["PickUpOrderListModel"] as? NSDictionary)
            }
        }
    }
}

// MARK: - PickUpOrderListModel
class NewPickUpOrderListModel {
    var data: [NewDatum]?
    var total: Int?

    init(response: NSDictionary?) {
        
        if let hasValue = response {
            if hasValue.count > 0 {
                if let hasCustomValues = hasValue["Data"] as? Array<Any>{
                    if hasCustomValues.count>0{
                        self.data =  hasCustomValues.compactMap({NewDatum(response: $0 as? NSDictionary)})
                    }
                }
                self.total = hasValue["Total"] as? Int ?? 0
            }
        }
    }
}

// MARK: - CustomProperties
class NewCustomProperties {

    init() {
    }
}

// MARK: - Datum
class NewDatum {
    var isLoggedInAsVendor: Bool?
    var id: Int?
    var orderGUID, customOrderNumber, storeName: String?
    var customerID: Int?
    var customerInfo: String?
    var customerEmail, customerFullName, customerIP, customerPhoneNumber: String?
    var affiliateID: Int?
    var affiliateName: String?
    var allowCustomersToSelectTaxDisplayType: Bool?
    var taxDisplayType: Int?
    var orderSubtotalInclTax, orderSubtotalExclTax, orderSubTotalDiscountInclTax, orderSubTotalDiscountExclTax: Any?
    var orderShippingInclTax, orderShippingExclTax, paymentMethodAdditionalFeeInclTax, paymentMethodAdditionalFeeExclTax: Any?
    var tax: Any?
    var displayTax, displayTaxRates: Bool?
    var orderTotalDiscount: Any?
    var redeemedRewardPoints: Int?
    var redeemedRewardPointsAmount: Any?
    var orderTotal: String?
    var refundedAmount, profit: Any?
    var orderSubtotalInclTaxValue, orderSubtotalExclTaxValue, orderSubTotalDiscountInclTaxValue, orderSubTotalDiscountExclTaxValue: Int?
    var orderShippingInclTaxValue, orderShippingExclTaxValue, paymentMethodAdditionalFeeInclTaxValue, paymentMethodAdditionalFeeExclTaxValue: Int?
    var taxValue: Int?
    var taxRatesValue: Any?
    var orderTotalDiscountValue, orderTotalValue, recurringPaymentID: Int?
    var orderStatus: String?
    var orderStatusID: Int?
    var paymentStatus: String?
    var paymentStatusID: Int?
    var paymentMethod: String?
    var allowStoringCreditCardNumber: Bool?
    var cardType, cardName, cardNumber, cardCvv2: Any?
    var cardExpirationMonth, cardExpirationYear, authorizationTransactionID, captureTransactionID: Any?
    var subscriptionTransactionID: Any?
    var isShippable, pickUpInStore: Bool?
    var pickupAddress, pickupAddressGoogleMapsURL: Any?
    var shippingStatus: String?
    var shippingStatusID: Int?
    var shippingAddress: NewShippingAddress?
    var shippingMethod, shippingAddressGoogleMapsURL: Any?
    var canAddNewShipments: Bool?
    var billingAddress, vatNumber: Any?
    var hasDownloadableProducts: Bool?
    var items: [NewItem]?
    var createdOn, updatedOn: String?
    var remarks: String?
    var checkoutAttributeInfo: Any?
    var addOrderNoteDisplayToCustomer: Bool?
    var addOrderNoteMessage: Any?
    var addOrderNoteHasDownload: Bool?
    var addOrderNoteDownloadID, amountToRefund, maxAmountToRefund: Int?
    var primaryStoreCurrencyCode: Any?
    var canCancelOrder, canCapture, canMarkOrderAsPaid, canRefund: Bool?
    var canRefundOffline, canPartiallyRefund, canPartiallyRefundOffline, canVoid: Bool?
    var canVoidOffline: Bool?
    var deliveryBoyID, deliveryBoy, deliveryBoyList, deviceID: Any?
    var form: Any?

    init(response: NSDictionary?) {
        
        if let hasValue = response{
            if hasValue.count>0{
                self.isLoggedInAsVendor = hasValue["IsLoggedInAsVendor"] as? Bool ?? false
                self.id = hasValue["Id"] as? Int ?? 0
                self.orderGUID = hasValue["OrderGuid"] as? String ?? ""
                self.customOrderNumber = hasValue["CustomOrderNumber"] as? String ?? ""
                self.storeName = hasValue["StoreName"] as? String ?? ""
                self.customerID = hasValue["CustomerId"] as? Int ?? 0
                self.customerInfo = hasValue["CustomerInfo"] as? String ?? ""
                self.customerEmail = hasValue["CustomerEmail"] as? String ?? ""
                self.customerFullName = hasValue["CustomerFullName"] as? String ?? ""
                self.customerIP = hasValue["CustomerIP"] as? String ?? ""
                self.customerPhoneNumber = hasValue["CustomerPhoneNumber"] as? String ?? ""
                self.affiliateID = hasValue["AffiliateId"] as? Int ?? 0
                self.affiliateName = hasValue["AffiliateName"] as? String ?? ""
                self.allowCustomersToSelectTaxDisplayType = hasValue["AllowCustomersToSelectTaxDisplayType"] as? Bool ?? false
                self.taxDisplayType =  hasValue["TaxDisplayType"] as? Int ?? 0
                self.orderSubtotalInclTax = hasValue["OrderSubtotalInclTax"]
                self.orderSubtotalExclTax = hasValue["OrderSubtotalExclTax"]
                self.orderSubTotalDiscountInclTax = hasValue["OrderSubTotalDiscountInclTax"]
                self.orderSubTotalDiscountExclTax = hasValue["OrderSubTotalDiscountExclTax"]
                self.orderShippingInclTax =  hasValue["OrderShippingInclTax"]
                self.orderShippingExclTax =  hasValue["OrderShippingExclTax"]
                self.paymentMethodAdditionalFeeInclTax = hasValue["paymentMethodAdditionalFeeInclTax"]
                self.paymentMethodAdditionalFeeExclTax = hasValue["PaymentMethodAdditionalFeeExclTax"]
                self.tax = hasValue["Tax"]
                self.displayTax = hasValue["DisplayTax"] as? Bool ?? false
                self.displayTaxRates = hasValue["DisplayTaxRates"] as? Bool ?? false
                self.orderTotalDiscount = hasValue["OrderTotalDiscount"]
                self.redeemedRewardPoints = hasValue["RedeemedRewardPoints"] as? Int ?? 0
                self.redeemedRewardPointsAmount = hasValue["RedeemedRewardPointsAmount"]
                self.orderTotal = hasValue["OrderTotal"] as? String ?? ""
                self.refundedAmount = hasValue["RefundedAmount"]
                self.profit = hasValue["Profit"]
                self.orderSubtotalInclTaxValue = hasValue["OrderSubtotalInclTaxValue"] as? Int ?? 0
                self.orderSubtotalExclTaxValue = hasValue["OrderSubtotalExclTaxValue"] as? Int ?? 0
                self.orderSubTotalDiscountInclTaxValue = hasValue["OrderSubTotalDiscountInclTaxValue"] as? Int ?? 0
                self.orderSubTotalDiscountExclTaxValue = hasValue["OrderSubTotalDiscountExclTaxValue"] as? Int ?? 0
                self.orderShippingInclTaxValue = hasValue["OrderShippingInclTaxValue"] as? Int ?? 0
                self.orderShippingExclTaxValue = hasValue["OrderShippingExclTaxValue"] as? Int ?? 0
                self.paymentMethodAdditionalFeeInclTaxValue = hasValue["PaymentMethodAdditionalFeeInclTaxValue"] as? Int ?? 0
                self.paymentMethodAdditionalFeeExclTaxValue = hasValue["PaymentMethodAdditionalFeeExclTaxValue"] as? Int ?? 0
                self.taxValue = hasValue["TaxValue"] as? Int ?? 0
                self.taxRatesValue = hasValue["TaxRatesValue"]
                self.orderTotalDiscountValue = hasValue["OrderTotalDiscountValue"] as? Int ?? 0
                self.orderTotalValue = hasValue["OrderTotalValue"]  as? Int ?? 0
                self.recurringPaymentID = hasValue["RecurringPaymentId"] as? Int ?? 0
                self.orderStatus = hasValue["OrderStatus"] as? String ?? ""
                self.orderStatusID = hasValue["OrderStatusId"] as? Int ?? 0
                self.paymentStatus = hasValue["PaymentStatus"] as? String ?? ""
                self.paymentStatusID = hasValue["PaymentStatusId"] as? Int ?? 0
                self.paymentMethod = hasValue["paymentMethod"] as? String ?? ""
                self.allowStoringCreditCardNumber = hasValue["AllowStoringCreditCardNumber"] as? Bool ?? false
                self.cardType = hasValue["CardType"]
                self.cardName = hasValue["CardName"]
                self.cardNumber = hasValue["CardNumber"]
                self.cardCvv2 = hasValue["CardCvv2"]
                self.cardExpirationMonth = hasValue["CardExpirationMonth"]
                self.cardExpirationYear = hasValue["CardExpirationYear"]
                self.authorizationTransactionID = hasValue["AuthorizationTransactionId"]
                self.captureTransactionID = hasValue["CaptureTransactionId"]
                self.subscriptionTransactionID = hasValue["SubscriptionTransactionId"]
                self.isShippable = hasValue["IsShippable"] as? Bool ?? false
                self.pickUpInStore = hasValue["PickUpInStore"] as? Bool ?? false
                self.pickupAddress = hasValue["PickupAddress"]
                self.pickupAddressGoogleMapsURL = hasValue["PickupAddressGoogleMapsURL"]
                self.shippingStatus = hasValue["ShippingStatus"] as? String ?? ""
                self.shippingStatusID = hasValue["ShippingStatusId"] as? Int ?? 0
                //self.shippingAddress = hasValue["ShippingAddress"] as? NewShippingAddress
                
                self.shippingAddress = NewShippingAddress(response: hasValue["ShippingAddress"] as? NSDictionary)
                
                self.shippingMethod = hasValue["ShippingMethod"]
                self.shippingAddressGoogleMapsURL = hasValue["ShippingAddressGoogleMapsURL"]
                self.canAddNewShipments = hasValue["CanAddNewShipments"]  as? Bool ?? false
                self.billingAddress = hasValue["BillingAddress"]
                self.vatNumber = hasValue["VatNumber"]
                self.hasDownloadableProducts = hasValue["HasDownloadableProducts"]  as? Bool ?? false
                //self.items = hasValue["Items"] as? [NewItem]
                if let hasCustomValues = hasValue["Items"] as? Array<Any>{
                    if hasCustomValues.count>0{
                        self.items =  hasCustomValues.compactMap({NewItem(response: $0 as? NSDictionary)})
                    }
                }
                self.createdOn = hasValue["CreatedOn"] as? String ?? ""
                self.updatedOn = hasValue["UpdatedOn"] as? String ?? ""
                self.remarks = hasValue["Remarks"] as? String ?? ""
                self.checkoutAttributeInfo = hasValue["CheckoutAttributeInfo"]
                self.addOrderNoteDisplayToCustomer = hasValue["AddOrderNoteDisplayToCustomer"] as? Bool ?? false
                self.addOrderNoteMessage = hasValue["AddOrderNoteMessage"]
                self.addOrderNoteHasDownload = hasValue["AddOrderNoteHasDownload"] as? Bool ?? false
                self.addOrderNoteDownloadID = hasValue["AddOrderNoteDownloadId"] as? Int ?? 0
                self.amountToRefund = hasValue["AmountToRefund"] as? Int ?? 0
                self.maxAmountToRefund = hasValue["MaxAmountToRefund"] as? Int ?? 0
                self.primaryStoreCurrencyCode = hasValue["PrimaryStoreCurrencyCode"]
                self.canCancelOrder = hasValue["CanCancelOrder"] as? Bool ?? false
                self.canCapture = hasValue["CanCapture"] as? Bool ?? false
                self.canMarkOrderAsPaid = hasValue["CanMarkOrderAsPaid"] as? Bool ?? false
                self.canRefund = hasValue["CanRefund"] as? Bool ?? false
                self.canRefundOffline = hasValue["CanRefundOffline"] as? Bool ?? false
                self.canPartiallyRefund = hasValue["CanPartiallyRefund"] as? Bool ?? false
                self.canPartiallyRefundOffline = hasValue["CanPartiallyRefundOffline"] as? Bool ?? false
                self.canVoid = hasValue["CanVoid"] as? Bool ?? false
                self.canVoidOffline = hasValue["CanVoidOffline"] as? Bool ?? false
                self.deliveryBoyID = hasValue["deliveryBoyId"]
                self.deliveryBoy = hasValue["DeliveryBoy"]
                self.deliveryBoyList = hasValue["DeliveryBoyList"]
                self.deviceID = hasValue["DeviceId"]
                self.form = hasValue["Form"]
            }
        }
                
    }
    
}

// MARK: - Item
class NewItem {
    var productID: Int?
    var productName, vendorName: String?
    var sku: Any?
    var pictureThumbnailURL: String?
    var unitPriceInclTax, unitPriceExclTax: String?
    var unitPriceInclTaxValue, unitPriceExclTaxValue, quantity: Int?
    var discountInclTax, discountExclTax: String?
    var discountInclTaxValue, discountExclTaxValue: Int?
    var subTotalInclTax, subTotalExclTax: String?
    var subTotalInclTaxValue, subTotalExclTaxValue: Int?
    var attributeInfo: String?
    var recurringInfo, rentalInfo: Any?
    //var returnRequests, purchasedGiftCardIDS: [Any?]
    var isDownload: Bool?
    var downloadCount, downloadActivationType: Int?
    var isDownloadActivated: Bool?
    var licenseDownloadGUID: String?
    var cgmItemID, barcode: Any?
    var id: Int?
    var form: Any?

    init(response: NSDictionary?) {
        
        if let hasValue = response{
            if hasValue.count>0{
                self.productID = hasValue["ProductId"] as? Int ?? 0
                self.productName = hasValue["ProductName"] as? String ?? ""
                self.vendorName = hasValue["VendorName"] as? String ?? ""
                self.sku = hasValue["sku"]
                self.pictureThumbnailURL = hasValue["PictureThumbnailUrl"]  as? String ?? ""
                self.unitPriceInclTax = hasValue["UnitPriceInclTax"] as? String ?? ""
                self.unitPriceExclTax = hasValue["UnitPriceExclTax"] as? String ?? ""
                self.unitPriceInclTaxValue = hasValue["UnitPriceInclTaxValue"] as? Int ?? 0
                self.unitPriceExclTaxValue = hasValue["unitPriceExclTaxValue"] as? Int ?? 0
                self.quantity = hasValue["Quantity"] as? Int ?? 0
                self.discountInclTax = hasValue["DiscountInclTax"] as? String ?? ""
                self.discountExclTax = hasValue["DiscountExclTax"] as? String ?? ""
                self.discountInclTaxValue = hasValue["DiscountInclTaxValue"] as? Int ?? 0
                self.discountExclTaxValue = hasValue["DiscountExclTaxValue"] as? Int ?? 0
                self.subTotalInclTax = hasValue["SubTotalInclTax"] as? String ?? ""
                self.subTotalExclTax = hasValue["SubTotalExclTax"] as? String ?? ""
                self.subTotalInclTaxValue = hasValue["SubTotalInclTaxValue"] as? Int ?? 0
                self.subTotalExclTaxValue = hasValue["SubTotalExclTaxValue"] as? Int ?? 0
                self.attributeInfo = hasValue["AttributeInfo"] as? String ?? ""
                self.recurringInfo = hasValue["RecurringInfo"] as? Int ?? 0
                self.rentalInfo = hasValue["RentalInfo"] as? Int ?? 0
                //self.returnRequests = hasValue["ReturnRequests"]
                //self.purchasedGiftCardIDS = hasValue["PurchasedGiftCardIDS"]
                self.isDownload = hasValue["IsDownload"] as? Bool ?? false
                self.downloadCount = hasValue["DownloadCount"] as? Int ?? 0
                self.downloadActivationType = hasValue["DownloadActivationType"] as? Int ?? 0
                self.isDownloadActivated = hasValue["IsDownloadActivated"] as? Bool ?? false
                self.licenseDownloadGUID = hasValue["LicenseDownloadGUId"] as? String ?? ""
                self.cgmItemID = hasValue["CgmItemId"] as? Int ?? 0
                self.barcode = hasValue["Barcode"] as? Int ?? 0
                self.id = hasValue["Id"] as? Int ?? 0
                self.form = hasValue["Form"] as? Int ?? 0
            }
        }
    }
}

// MARK: - ShippingAddress
class NewShippingAddress {
    var firstName, lastName, email: String?
    var companyEnabled, companyRequired: Bool?
    var company: Any?
    var countryEnabled: Bool?
    var countryID: Int?
    var countryName: String?
    var stateProvinceEnabled: Bool?
    var stateProvinceID: Int?
    var stateProvinceName: String?
    var cityEnabled, cityRequired: Bool?
    var city: String?
    var cityID: Int?
    var streetAddressEnabled, streetAddressRequired: Bool?
    var address1: String?
    var streetAddress2Enabled, streetAddress2Required: Bool?
    var address2: String?
    var zipPostalCodeEnabled, zipPostalCodeRequired: Bool?
    var zipPostalCode: Any?
    var phoneEnabled, phoneRequired: Bool?
    var phoneNumber: String?
    var faxEnabled, faxRequired: Bool?
    var faxNumber: Any?
    //var availableCountries, availableStates, availableCities: [Any?]
    var formattedCustomAddressAttributes: String?
    //var customAddressAttributes: [Any?]
    var isDeliveryAllowed: Bool?
    var houseNo, floorNo, roomNo: String?
    var isLiftOption: Bool?
    var ropeColor: String?
    var id: Int?
    var form: Any?

    init(response: NSDictionary?) {
        
        if let hasValue = response{
            if hasValue.count>0 {
                self.firstName = hasValue["FirstName"] as? String ?? ""
                self.lastName = hasValue["LastName"] as? String ?? ""
                self.email = hasValue["Email"] as? String ?? ""
                self.companyEnabled = hasValue["CompanyEnabled"] as? Bool ?? false
                self.companyRequired = hasValue["CompanyRequired"] as? Bool ?? false
                self.company = hasValue["Company"] as? Bool ?? false
                self.countryEnabled = hasValue["CountryEnabled"] as? Bool ?? false
                self.countryID = hasValue["CountryId"] as? Int ?? 0
                self.countryName = hasValue["CountryName"] as? String ?? ""
                self.stateProvinceEnabled = hasValue["StateProvinceEnabled"] as? Bool ?? false
                self.stateProvinceID = hasValue["StateProvinceId"] as? Int ?? 0
                self.stateProvinceName = hasValue["StateProvinceName"] as? String ?? ""
                self.cityEnabled = hasValue["CityEnabled"] as? Bool ?? false
                self.cityRequired = hasValue["CityRequired"] as? Bool ?? false
                self.city = hasValue["City"] as? String ?? ""
                self.cityID = hasValue["CityId"] as? Int ?? 0
                self.streetAddressEnabled = hasValue["StreetAddressEnabled"] as? Bool ?? false
                self.streetAddressRequired = hasValue["StreetAddressRequired"] as? Bool ?? false
                self.address1 = hasValue["Address1"] as? String ?? ""
                self.streetAddress2Enabled = hasValue["StreetAddress2Enabled"] as? Bool ?? false
                self.streetAddress2Required = hasValue["StreetAddress2Required"] as? Bool ?? false
                self.address2 = hasValue["Address2"] as? String ?? ""
                self.zipPostalCodeEnabled = hasValue["ZipPostalCodeEnabled"] as? Bool ?? false
                self.zipPostalCodeRequired = hasValue["ZipPostalCodeRequired"] as? Bool ?? false
                self.zipPostalCode = hasValue["ZipPostalCode"] as? Bool ?? false
                self.phoneEnabled = hasValue["PhoneEnabled"] as? Bool ?? false
                self.phoneRequired = hasValue["PhoneRequired"] as? Bool ?? false
                self.phoneNumber = hasValue["PhoneNumber"] as? String ?? ""
                self.faxEnabled = hasValue["FaxEnabled"] as? Bool ?? false
                self.faxRequired = hasValue["FaxRequired"] as? Bool ?? false
                self.faxNumber = hasValue["FaxNumber"] as? Bool ?? false
                //self.availableCountries = hasValue["AvailableCountries"]
                //self.availableStates = hasValue["AvailableStates"]
                //self.availableCities = hasValue["AvailableCities"] as? Bool ?? false
                self.formattedCustomAddressAttributes = hasValue["FormattedCustomAddressAttributes"] as? String ?? ""
                //self.customAddressAttributes = hasValue["CustomAddressAttributes"] as? Bool ?? false
                self.isDeliveryAllowed = hasValue["IsDeliveryAllowed"] as? Bool ?? false
                self.houseNo = hasValue["HouseNo"] as? String ?? ""
                self.floorNo = hasValue["FloorNo"] as? String ?? ""
                self.roomNo = hasValue["RoomNo"] as? String ?? ""
                self.isLiftOption = hasValue["IsLiftOption"] as? Bool ?? false
                self.ropeColor = hasValue["RopeColor"] as? String ?? ""
                self.id = hasValue["Id"] as? Int ?? 0
                self.form = hasValue["Form"] as? Bool ?? false
            }
        }
    }
}

// MARK: - PagingFilteringContext
class NewPagingFilteringContext {
    var priceRangeFilter: NewPriceRangeFilter?
    //var specificationFilter: NewSpecificationFilter?
    var allowProductSorting: Bool?
    //var availableSortOptions: [Any?]
    var allowProductViewModeChanging: Bool?
    //var availableViewModes: [Any?]
    var allowCustomersToSelectPageSize: Bool?
    //var pageSizeOptions: [Any?]
    var orderBy: Int?
    var viewMode: Any?
    var pageIndex, pageNumber, pageSize, totalItems: Int?
    var totalPages, firstItem, lastItem: Int?
    var hasPreviousPage, hasNextPage: Bool?
    var form: Any?

    init(response: NSDictionary?) {
        
        if let hasValue = response{
            if hasValue.count>0{
//                self.priceRangeFilter =  hasValue["PriceRangeFilter"]
//                self.specificationFilter = hasValue["SpecificationFilter"]
                self.allowProductSorting = hasValue["AllowProductSorting"] as? Bool ?? false
                //self.availableSortOptions = hasValue["AvailableSortOptions"]
                self.allowProductViewModeChanging = hasValue["AllowProductSorting"] as? Bool ?? false
                //self.availableViewModes = hasValue["AvailableViewModes"] as? Bool ?? false
                self.allowCustomersToSelectPageSize = hasValue["AllowCustomersToSelectPageSize"] as? Bool ?? false
                //self.pageSizeOptions = hasValue["PageSizeOptions"] as? Bool ?? false
                self.orderBy = hasValue["OrderBy"] as? Int ?? 0
                self.viewMode = hasValue["ViewMode"] as? Bool ?? false
                self.pageIndex = hasValue["PageIndex"] as? Int ?? 0
                self.pageNumber = hasValue["PageNumber"] as? Int ?? 0
                self.pageSize = hasValue["PageSize"] as? Int ?? 0
                self.totalItems = hasValue["TotalItems"] as? Int ?? 0
                self.totalPages = hasValue["TotalPages"] as? Int ?? 0
                self.firstItem = hasValue["FirstItem"] as? Int ?? 0
                self.lastItem = hasValue["LastItem"] as? Int ?? 0
                self.hasPreviousPage = hasValue["HasPreviousPage"] as? Bool ?? false
                self.hasNextPage = hasValue["HasNextPage"] as? Bool ?? false
                self.form = hasValue["Form"] as? Bool ?? false
            }
        }
    }
    
}

// MARK: - PriceRangeFilter
class NewPriceRangeFilter {
    var enabled: Bool?
    //var items: [Any?]?
    var removeFilterURL: Any?
    
    init(response: NSDictionary?) {
        
        if let hasValue = response{
            if hasValue.count>0{
                self.enabled = hasValue["Enabled"] as? Bool ?? false
                //self.items = hasValue["Items"]
                self.removeFilterURL = hasValue["RemoveFilterURL"]
            }
        }
    }
}

// MARK: - SpecificationFilter
class NewSpecificationFilter {
    var enabled: Bool
    var alreadyFilteredItems, notFilteredItems, filterItems: [Any?]
    var removeFilterURL, form: NSNull
    var customProperties: NewCustomProperties

    init(enabled: Bool, alreadyFilteredItems: [Any?], notFilteredItems: [Any?], filterItems: [Any?], removeFilterURL: NSNull, form: NSNull, customProperties: NewCustomProperties) {
        self.enabled = enabled
        self.alreadyFilteredItems = alreadyFilteredItems
        self.notFilteredItems = notFilteredItems
        self.filterItems = filterItems
        self.removeFilterURL = removeFilterURL
        self.form = form
        self.customProperties = customProperties
    }
}
