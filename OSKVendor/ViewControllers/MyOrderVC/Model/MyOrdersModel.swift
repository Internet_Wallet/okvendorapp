// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let myOrdersModel = try? newJSONDecoder().decode(MyOrdersModel.self, from: jsonData)

import Foundation

// MARK: - MyOrdersModel
struct MyOrder: Codable {
    let pickUpOrderListModel: PickUpOrderListModel?
    let successMessage: String?
    let statusCode: Int?
    let errorList: [String]?

    enum CodingKeys: String, CodingKey {
        case pickUpOrderListModel = "PickUpOrderListModel"
        case successMessage = "SuccessMessage"
        case statusCode = "StatusCode"
        case errorList = "ErrorList"
    }
}

// MARK: - PickUpOrderListModel
struct PickUpOrderListModel: Codable {
    let pagingFilteringContext: PagingFilteringContext?
    let data: [Datum]?
    let total: Int?
    let extraData, errors, form: String?
    let customProperties: CustomProperties?

    enum CodingKeys: String, CodingKey {
        case pagingFilteringContext = "PagingFilteringContext"
        case data = "Data"
        case total = "Total"
        case extraData = "ExtraData"
        case errors = "Errors"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}

// MARK: - Datum
struct Datum: Codable {
    let isLoggedInAsVendor: Bool?
    let id: Int?
    let orderGUID, customOrderNumber, storeName: String?
    let customerID: Int?
    let customerInfo: String?
    let customerEmail, customerFullName, customerIP, customerPhoneNumber: String?
    let affiliateID: Int?
    let affiliateName: String?
    let allowCustomersToSelectTaxDisplayType: Bool?
    let taxDisplayType: Int?
    let orderSubtotalInclTax, orderSubtotalExclTax, orderSubTotalDiscountInclTax, orderSubTotalDiscountExclTax: Int?
    let orderShippingInclTax, orderShippingExclTax, paymentMethodAdditionalFeeInclTax, paymentMethodAdditionalFeeExclTax: Int?
    let tax: JSONNull?
    let displayTax, displayTaxRates: Bool?
    let orderTotalDiscount: Int?
    let redeemedRewardPoints: Int
    let redeemedRewardPointsAmount: Int?
    let orderTotal: String?
    let refundedAmount, profit: Int?
    let orderSubtotalInclTaxValue, orderSubtotalExclTaxValue, orderSubTotalDiscountInclTaxValue, orderSubTotalDiscountExclTaxValue: Int?
    let orderShippingInclTaxValue, orderShippingExclTaxValue, paymentMethodAdditionalFeeInclTaxValue, paymentMethodAdditionalFeeExclTaxValue: Int?
    let taxValue: Int?
    let taxRatesValue: JSONNull?
    let orderTotalDiscountValue, orderTotalValue, recurringPaymentID: Int?
    let orderStatus: String?
    let orderStatusID: Int?
    let paymentStatus: String?
    let paymentStatusID: Int?
    let paymentMethod: JSONNull?
    let allowStoringCreditCardNumber: Bool?
    let cardType, cardName, cardNumber, cardCvv2: JSONNull?
    let cardExpirationMonth, cardExpirationYear, authorizationTransactionID, captureTransactionID: JSONNull?
    let subscriptionTransactionID: JSONNull?
    let isShippable, pickUpInStore: Bool?
    let pickupAddress, pickupAddressGoogleMapsURL: JSONNull?
    let shippingStatus: String?
    let shippingStatusID: Int?
    let shippingAddress: ShippingAddress?
    let shippingMethod, shippingAddressGoogleMapsURL: JSONNull?
    let canAddNewShipments: Bool?
    let billingAddress, vatNumber: JSONNull?
    let hasDownloadableProducts: Bool?
    let items: [Item]?
    let createdOn: String?
    let UpdatedOn: String?
    let Remarks: String?
    let checkoutAttributeInfo: String?
    let addOrderNoteDisplayToCustomer: Bool?
    let addOrderNoteMessage: String?
    let addOrderNoteHasDownload: Bool?
    let addOrderNoteDownloadID, amountToRefund, maxAmountToRefund: Int?
    let primaryStoreCurrencyCode: JSONNull?
    let canCancelOrder, canCapture, canMarkOrderAsPaid, canRefund: Bool?
    let canRefundOffline, canPartiallyRefund, canPartiallyRefundOffline, canVoid: Bool?
    let canVoidOffline: Bool?
    let deliveryBoyID, deliveryBoy, deliveryBoyList, deviceID: JSONNull?
    let form: JSONNull?
    let customProperties: CustomProperties?

    enum CodingKeys: String, CodingKey {
        case Remarks = "Remarks"
        case UpdatedOn = "UpdatedOn"
        case isLoggedInAsVendor = "IsLoggedInAsVendor"
        case id = "Id"
        case orderGUID = "OrderGuid"
        case customOrderNumber = "CustomOrderNumber"
        case storeName = "StoreName"
        case customerID = "CustomerId"
        case customerInfo = "CustomerInfo"
        case customerEmail = "CustomerEmail"
        case customerFullName = "CustomerFullName"
        case customerIP = "CustomerIp"
        case customerPhoneNumber = "CustomerPhoneNumber"
        case affiliateID = "AffiliateId"
        case affiliateName = "AffiliateName"
        case allowCustomersToSelectTaxDisplayType = "AllowCustomersToSelectTaxDisplayType"
        case taxDisplayType = "TaxDisplayType"
        case orderSubtotalInclTax = "OrderSubtotalInclTax"
        case orderSubtotalExclTax = "OrderSubtotalExclTax"
        case orderSubTotalDiscountInclTax = "OrderSubTotalDiscountInclTax"
        case orderSubTotalDiscountExclTax = "OrderSubTotalDiscountExclTax"
        case orderShippingInclTax = "OrderShippingInclTax"
        case orderShippingExclTax = "OrderShippingExclTax"
        case paymentMethodAdditionalFeeInclTax = "PaymentMethodAdditionalFeeInclTax"
        case paymentMethodAdditionalFeeExclTax = "PaymentMethodAdditionalFeeExclTax"
        case tax = "Tax"
        case displayTax = "DisplayTax"
        case displayTaxRates = "DisplayTaxRates"
        case orderTotalDiscount = "OrderTotalDiscount"
        case redeemedRewardPoints = "RedeemedRewardPoints"
        case redeemedRewardPointsAmount = "RedeemedRewardPointsAmount"
        case orderTotal = "OrderTotal"
        case refundedAmount = "RefundedAmount"
        case profit = "Profit"
        case orderSubtotalInclTaxValue = "OrderSubtotalInclTaxValue"
        case orderSubtotalExclTaxValue = "OrderSubtotalExclTaxValue"
        case orderSubTotalDiscountInclTaxValue = "OrderSubTotalDiscountInclTaxValue"
        case orderSubTotalDiscountExclTaxValue = "OrderSubTotalDiscountExclTaxValue"
        case orderShippingInclTaxValue = "OrderShippingInclTaxValue"
        case orderShippingExclTaxValue = "OrderShippingExclTaxValue"
        case paymentMethodAdditionalFeeInclTaxValue = "PaymentMethodAdditionalFeeInclTaxValue"
        case paymentMethodAdditionalFeeExclTaxValue = "PaymentMethodAdditionalFeeExclTaxValue"
        case taxValue = "TaxValue"
        case taxRatesValue = "TaxRatesValue"
        case orderTotalDiscountValue = "OrderTotalDiscountValue"
        case orderTotalValue = "OrderTotalValue"
        case recurringPaymentID = "RecurringPaymentId"
        case orderStatus = "OrderStatus"
        case orderStatusID = "OrderStatusId"
        case paymentStatus = "PaymentStatus"
        case paymentStatusID = "PaymentStatusId"
        case paymentMethod = "PaymentMethod"
        case allowStoringCreditCardNumber = "AllowStoringCreditCardNumber"
        case cardType = "CardType"
        case cardName = "CardName"
        case cardNumber = "CardNumber"
        case cardCvv2 = "CardCvv2"
        case cardExpirationMonth = "CardExpirationMonth"
        case cardExpirationYear = "CardExpirationYear"
        case authorizationTransactionID = "AuthorizationTransactionId"
        case captureTransactionID = "CaptureTransactionId"
        case subscriptionTransactionID = "SubscriptionTransactionId"
        case isShippable = "IsShippable"
        case pickUpInStore = "PickUpInStore"
        case pickupAddress = "PickupAddress"
        case pickupAddressGoogleMapsURL = "PickupAddressGoogleMapsUrl"
        case shippingStatus = "ShippingStatus"
        case shippingStatusID = "ShippingStatusId"
        case shippingAddress = "ShippingAddress"
        case shippingMethod = "ShippingMethod"
        case shippingAddressGoogleMapsURL = "ShippingAddressGoogleMapsUrl"
        case canAddNewShipments = "CanAddNewShipments"
        case billingAddress = "BillingAddress"
        case vatNumber = "VatNumber"
        case hasDownloadableProducts = "HasDownloadableProducts"
        case items = "Items"
        case createdOn = "CreatedOn"
        case checkoutAttributeInfo = "CheckoutAttributeInfo"
        case addOrderNoteDisplayToCustomer = "AddOrderNoteDisplayToCustomer"
        case addOrderNoteMessage = "AddOrderNoteMessage"
        case addOrderNoteHasDownload = "AddOrderNoteHasDownload"
        case addOrderNoteDownloadID = "AddOrderNoteDownloadId"
        case amountToRefund = "AmountToRefund"
        case maxAmountToRefund = "MaxAmountToRefund"
        case primaryStoreCurrencyCode = "PrimaryStoreCurrencyCode"
        case canCancelOrder = "CanCancelOrder"
        case canCapture = "CanCapture"
        case canMarkOrderAsPaid = "CanMarkOrderAsPaid"
        case canRefund = "CanRefund"
        case canRefundOffline = "CanRefundOffline"
        case canPartiallyRefund = "CanPartiallyRefund"
        case canPartiallyRefundOffline = "CanPartiallyRefundOffline"
        case canVoid = "CanVoid"
        case canVoidOffline = "CanVoidOffline"
        case deliveryBoyID = "DeliveryBoyId"
        case deliveryBoy = "DeliveryBoy"
        case deliveryBoyList = "DeliveryBoyList"
        case deviceID = "DeviceId"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}

// MARK: - Item
struct Item: Codable {
    let productID: Int?
    let productName, vendorName: String?
    let sku: Int?
    let pictureThumbnailURL: String?
    let unitPriceInclTax, unitPriceExclTax: String?
    let unitPriceInclTaxValue, unitPriceExclTaxValue, quantity: Int?
    let discountInclTax, discountExclTax: String?
    let discountInclTaxValue, discountExclTaxValue: Int?
    let subTotalInclTax, subTotalExclTax: String?
    let subTotalInclTaxValue, subTotalExclTaxValue: Int?
    let attributeInfo: String?
    let recurringInfo, rentalInfo: String?
    let returnRequests, purchasedGiftCardIDS: [String]?
    let isDownload: Bool?
    let downloadCount, downloadActivationType: Int?
    let isDownloadActivated: Bool?
    let licenseDownloadGUID: String?
    let cgmItemID: Int?
    let barcode: String?
    let id: Int?
    let form: String?
    let customProperties: CustomProperties?

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case productName = "ProductName"
        case vendorName = "VendorName"
        case sku = "Sku"
        case pictureThumbnailURL = "PictureThumbnailUrl"
        case unitPriceInclTax = "UnitPriceInclTax"
        case unitPriceExclTax = "UnitPriceExclTax"
        case unitPriceInclTaxValue = "UnitPriceInclTaxValue"
        case unitPriceExclTaxValue = "UnitPriceExclTaxValue"
        case quantity = "Quantity"
        case discountInclTax = "DiscountInclTax"
        case discountExclTax = "DiscountExclTax"
        case discountInclTaxValue = "DiscountInclTaxValue"
        case discountExclTaxValue = "DiscountExclTaxValue"
        case subTotalInclTax = "SubTotalInclTax"
        case subTotalExclTax = "SubTotalExclTax"
        case subTotalInclTaxValue = "SubTotalInclTaxValue"
        case subTotalExclTaxValue = "SubTotalExclTaxValue"
        case attributeInfo = "AttributeInfo"
        case recurringInfo = "RecurringInfo"
        case rentalInfo = "RentalInfo"
        case returnRequests = "ReturnRequests"
        case purchasedGiftCardIDS = "PurchasedGiftCardIds"
        case isDownload = "IsDownload"
        case downloadCount = "DownloadCount"
        case downloadActivationType = "DownloadActivationType"
        case isDownloadActivated = "IsDownloadActivated"
        case licenseDownloadGUID = "LicenseDownloadGuid"
        case cgmItemID = "CGMItemID"
        case barcode = "Barcode"
        case id = "Id"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}

// MARK: - ShippingAddress
struct ShippingAddress: Codable {
    let firstName, lastName, email: String?
    let companyEnabled, companyRequired: Bool?
    let company: String?
    let countryEnabled: Bool?
    let countryID: Int?
    let countryName: String?
    let stateProvinceEnabled: Bool?
    let stateProvinceID: Int?
    let stateProvinceName: String?
    let cityEnabled, cityRequired: Bool?
    let city: String?
    let cityID: Int?
    let streetAddressEnabled, streetAddressRequired: Bool?
    let address1: String?
    let streetAddress2Enabled, streetAddress2Required: Bool?
    let address2: String?
    let zipPostalCodeEnabled, zipPostalCodeRequired: Bool?
    let zipPostalCode: Int?
    let phoneEnabled, phoneRequired: Bool?
    let phoneNumber: String?
    let faxEnabled, faxRequired: Bool?
    let faxNumber: Int?
    let availableCountries, availableStates, availableCities: [String]?
    let formattedCustomAddressAttributes: String?
    let customAddressAttributes: [String]?
    let isDeliveryAllowed: Bool?
    let houseNo, floorNo, roomNo: String?
    let isLiftOption: Bool?
    let ropeColor: String?
    let id: Int?
    let form: String?
    let customProperties: CustomProperties?

    enum CodingKeys: String, CodingKey {
        case firstName = "FirstName"
        case lastName = "LastName"
        case email = "Email"
        case companyEnabled = "CompanyEnabled"
        case companyRequired = "CompanyRequired"
        case company = "Company"
        case countryEnabled = "CountryEnabled"
        case countryID = "CountryId"
        case countryName = "CountryName"
        case stateProvinceEnabled = "StateProvinceEnabled"
        case stateProvinceID = "StateProvinceId"
        case stateProvinceName = "StateProvinceName"
        case cityEnabled = "CityEnabled"
        case cityRequired = "CityRequired"
        case city = "City"
        case cityID = "CityId"
        case streetAddressEnabled = "StreetAddressEnabled"
        case streetAddressRequired = "StreetAddressRequired"
        case address1 = "Address1"
        case streetAddress2Enabled = "StreetAddress2Enabled"
        case streetAddress2Required = "StreetAddress2Required"
        case address2 = "Address2"
        case zipPostalCodeEnabled = "ZipPostalCodeEnabled"
        case zipPostalCodeRequired = "ZipPostalCodeRequired"
        case zipPostalCode = "ZipPostalCode"
        case phoneEnabled = "PhoneEnabled"
        case phoneRequired = "PhoneRequired"
        case phoneNumber = "PhoneNumber"
        case faxEnabled = "FaxEnabled"
        case faxRequired = "FaxRequired"
        case faxNumber = "FaxNumber"
        case availableCountries = "AvailableCountries"
        case availableStates = "AvailableStates"
        case availableCities = "AvailableCities"
        case formattedCustomAddressAttributes = "FormattedCustomAddressAttributes"
        case customAddressAttributes = "CustomAddressAttributes"
        case isDeliveryAllowed = "IsDeliveryAllowed"
        case houseNo = "HouseNo"
        case floorNo = "FloorNo"
        case roomNo = "RoomNo"
        case isLiftOption = "IsLiftOption"
        case ropeColor = "RopeColor"
        case id = "Id"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}

// MARK: - PagingFilteringContext
struct PagingFilteringContext: Codable {
    let priceRangeFilter: PriceRangeFilter?
    let specificationFilter: SpecificationFilter?
    let allowProductSorting: Bool?
    let availableSortOptions: [JSONAny]?
    let allowProductViewModeChanging: Bool?
    let availableViewModes: [JSONAny]?
    let allowCustomersToSelectPageSize: Bool?
    let pageSizeOptions: [JSONAny]?
    let orderBy: Int?
    let viewMode: JSONNull?
    let pageIndex, pageNumber, pageSize, totalItems: Int?
    let totalPages, firstItem, lastItem: Int?
    let hasPreviousPage, hasNextPage: Bool?
    let form: JSONNull?
    let customProperties: CustomProperties?

    enum CodingKeys: String, CodingKey {
        case priceRangeFilter = "PriceRangeFilter"
        case specificationFilter = "SpecificationFilter"
        case allowProductSorting = "AllowProductSorting"
        case availableSortOptions = "AvailableSortOptions"
        case allowProductViewModeChanging = "AllowProductViewModeChanging"
        case availableViewModes = "AvailableViewModes"
        case allowCustomersToSelectPageSize = "AllowCustomersToSelectPageSize"
        case pageSizeOptions = "PageSizeOptions"
        case orderBy = "OrderBy"
        case viewMode = "ViewMode"
        case pageIndex = "PageIndex"
        case pageNumber = "PageNumber"
        case pageSize = "PageSize"
        case totalItems = "TotalItems"
        case totalPages = "TotalPages"
        case firstItem = "FirstItem"
        case lastItem = "LastItem"
        case hasPreviousPage = "HasPreviousPage"
        case hasNextPage = "HasNextPage"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}

// MARK: - PriceRangeFilter
struct PriceRangeFilter: Codable {
    let enabled: Bool?
    let items: [JSONAny]?
    let removeFilterURL, form: JSONNull?
    let customProperties: CustomProperties?

    enum CodingKeys: String, CodingKey {
        case enabled = "Enabled"
        case items = "Items"
        case removeFilterURL = "RemoveFilterUrl"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}

// MARK: - SpecificationFilter
struct SpecificationFilter: Codable {
    let enabled: Bool
    let alreadyFilteredItems, notFilteredItems, filterItems: [JSONAny]
    let removeFilterURL, form: JSONNull?
    let customProperties: CustomProperties

    enum CodingKeys: String, CodingKey {
        case enabled = "Enabled"
        case alreadyFilteredItems = "AlreadyFilteredItems"
        case notFilteredItems = "NotFilteredItems"
        case filterItems = "FilterItems"
        case removeFilterURL = "RemoveFilterUrl"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}
