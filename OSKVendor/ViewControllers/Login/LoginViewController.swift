//
//  LoginViewController.swift
//  OSKVendor
//
//  Created by vamsi on 6/26/20.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON
import TextFieldEffects
import MessageUI

class LoginViewController: OSKBaseViewController,PhValidationProtocol {

    var delegate : LoginDelegate?
    
    var timer          = Timer()
    var seconds        = 50
    var smsOtpTxt = ""
    var randomOTP = ""
    let themeColor = UIColor.colorWithRedValue(redValue: 10, greenValue: 177, blueValue: 75, alpha: 1)

    @IBOutlet var mobileNumberTF: SkyFloatingLabelTextField!{
        didSet {
        self.mobileNumberTF.placeholder = self.mobileNumberTF.placeholder?.localized
    }
    }
    
    @IBOutlet var passwordTF: SkyFloatingLabelTextField!{
        didSet {
            self.passwordTF.placeholder = self.passwordTF.placeholder?.localized
           }
    }
    @IBOutlet var loginbutton: UIButton!{
        didSet{
            self.loginbutton.layer.cornerRadius = 4.0
            loginbutton.setTitle("Verify".localized, for: .normal)
        }
    }
    @IBOutlet var forgotPasswordbutton: UIButton!{
        didSet{
            forgotPasswordbutton.setTitle("Forgot Password?".localized, for: .normal)
        }
    }
    
    @IBOutlet var languageView: UIView!
    @IBOutlet var loginView: UIView!
    
    @IBOutlet var languageLabel: UILabel!
    
    @IBOutlet var englishLanguageButton: UIButton!
    
    @IBOutlet var unicodeLanguageButton: UIButton!
    
    @IBOutlet var showpasswordButton: UIButton!
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var vendorlogoView: UIView!
    @IBOutlet var titleLabel: UILabel!{
        didSet{
            titleLabel.text = "Login".localized
        }
    }
    
    var apiManager = APIManager()
    var countryView   : PaytoViews?
    var country       : Country?
    let validObj = PayToValidations.init()
    
    public var currentSelectedCountry : countrySelected = .myanmarCountry
    enum countrySelected {
        case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
    }
   // var phNumValidationsFile: [PhNumValidationList] = []
    
    var buttonstr = ""
    override func viewDidLoad() {
        super.viewDidLoad()
       

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        
        vendorlogoView.layer.shadowColor = UIColor.gray.cgColor
        vendorlogoView.layer.shadowOpacity = 0.3
        vendorlogoView.layer.shadowOffset = CGSize.zero
        vendorlogoView.layer.shadowRadius = 6
        vendorlogoView.layer.cornerRadius = 8.0
        vendorlogoView.layer.masksToBounds = true
        
       // passwordTF.isSecureTextEntry = true
       // showpasswordButton.setImage(UIImage(named: "Eyeclose"), for: .normal)
        
        //languageView.layer.cornerRadius = 10.0
        //languageView.layer.borderWidth = 1.0
        //languageView.layer.borderColor = UIColor.lightGray.cgColor
        //languageView.layer.masksToBounds = true
        
        if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), lang == "en"{
            englishLanguageButton.backgroundColor = themeColor
            englishLanguageButton.setTitleColor(UIColor.white, for: .normal)
            
            if #available(iOS 13.0, *) {
                unicodeLanguageButton.backgroundColor = UIColor.systemGroupedBackground
            } else {
                // Fallback on earlier versions
                unicodeLanguageButton.backgroundColor = UIColor.white
            }
            unicodeLanguageButton.setTitleColor(themeColor, for: .normal)
        } else {
            unicodeLanguageButton.backgroundColor = themeColor
            unicodeLanguageButton.setTitleColor(UIColor.white, for: .normal)
            if #available(iOS 13.0, *) {
                englishLanguageButton.backgroundColor = UIColor.systemGroupedBackground
            } else {
                // Fallback on earlier versions
                englishLanguageButton.backgroundColor = UIColor.white
            }
            englishLanguageButton.setTitleColor(themeColor, for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        buttonstr = "0"
        self.updateGUI()
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func callLoginApi(addressDict: Dictionary<String, String>?) {
        
        AppUtility.showLoading(self.view)
        apiManager.LoginInUser(addressDict! as [String : AnyObject], onSuccess: { [weak self] response in
            DispatchQueue.main.async{
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
                let json = JSON(response)
                println_debug("Login Response: \(json)")
                if let code = json["StatusCode"].int, code == 200 {
                    if let loginInfoDic  = json["Data"].dictionaryObject {
                        let vmTemoModel = VMTempModel()
                        vmTemoModel.wrapDataModel(JSON: loginInfoDic)
                        print(loginInfoDic)
                        DispatchQueue.main.async {
                                UserDefaults.standard.set(true, forKey: AppConstants.userDefault.loginStatus)
                                UserDefaults.standard.set(true, forKey: AppConstants.userDefault.enabledLogout)
                                UserDefaults.standard.synchronize()
                                appDelegate.FCMToken = loginInfoDic["Token"] as! String
                                let dataDict:[String: String] = ["token": appDelegate.FCMToken]
                                NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
                                self?.loginCompleteAction()
                        }
                    }
                   // self?.handleLogin()
                }else {
                    let message = json["ErrorList"][0].string
                    let alert = UIAlertController(title: "OSK Vendor".localized, message: message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                }

            }
            
            }, onError: { [weak self] message in
                DispatchQueue.main.async {
                if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                  //      self?.showAlert(alertTitle: "Login", description: "Please Check Internet Connection")
                      
                    }
                }
        })
        
    }
    
    func changeAppLanguageApiCall(index: Int) {
        let type = index //(index == 0) ? 1 : 2
        let urlString = String(format: "%@/SetLanguage/%d", APIManagerClient.sharedInstance.base_url, type)
        guard let url = URL(string: urlString) else { return }
        let params = Dictionary<String, Any>()
        apiManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, _) in
            if success {
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["StatusCode"] as? Int, code == 200 {
                       
                        println_debug("Language updated")
                    } else {
                        AppUtility.showToastlocal(message: "Failure", view: self!.view)
                    }
                }
            }
        }
    }
    
    func loginCompleteAction() -> Void {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "KYDrawerController_id") as! KYDrawerController
        UIApplication.shared.keyWindow?.rootViewController = viewController
        self.performSegue(withIdentifier: "loginComplete", sender: self)
    }
  
//    @IBAction func pwdvisableClick(_ sender: Any) {
//
//        if buttonstr == "0"{
//            buttonstr = "1"
//           // passwordTF.isSecureTextEntry = false
//            showpasswordButton.setImage(UIImage(named: "Eyeopen"), for: .normal)
//        }
//        else if buttonstr == "1"{
//            buttonstr = "0"
//          //  passwordTF.isSecureTextEntry = true
//            showpasswordButton.setImage(UIImage(named: "Eyeclose"), for: .normal)
//        }
//    }
    
    @IBAction func englishButtonClick(_ sender: Any) {
        UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
        UserDefaults.standard.set("en", forKey: "currentLanguage")
        englishLanguageButton.backgroundColor = themeColor
        englishLanguageButton.setTitleColor(UIColor.white, for: .normal)
        
        if #available(iOS 13.0, *) {
            unicodeLanguageButton.backgroundColor = UIColor.systemGroupedBackground
        } else {
            // Fallback on earlier versions
            unicodeLanguageButton.backgroundColor = UIColor.white
        }
        unicodeLanguageButton.setTitleColor(themeColor, for: .normal)
        updateGUI()
        self.changeAppLanguageApiCall(index: 1)
        delegate?.langResetFromLogin(reset: true)
    }
    
    @IBAction func unicodeButtonClick(_ sender: Any) {
        
        UserDefaults.standard.set("uni", forKey: "currentLanguage")
        UserDefaults.standard.set("Myanmar3", forKey: "appFont")
        UserDefaults.standard.synchronize()
        
        unicodeLanguageButton.backgroundColor = themeColor
        unicodeLanguageButton.setTitleColor(UIColor.white, for: .normal)
        
        if #available(iOS 13.0, *) {
            englishLanguageButton.backgroundColor = UIColor.systemGroupedBackground
        } else {
            // Fallback on earlier versions
            englishLanguageButton.backgroundColor = UIColor.white
        }
        englishLanguageButton.setTitleColor(themeColor, for: .normal)
        updateGUI()
        self.changeAppLanguageApiCall(index: 3)
        delegate?.langResetFromLogin(reset: true)
    }
    
    func updateGUI() {
        
        phNumValidationsFile = getDataFromJSONFile() ?? []
        appDelegate.setInitialCurrentLang()
        titleLabel.text = "Login".localized
        mobileNumberTF.text =  (self.country?.dialCode == "+95") ? "09" : ""
        loginbutton.setTitle("Verify".localized, for: .normal)
      //  loginbutton.backgroundColor = UIColor.lightGray
        //forgotPasswordbutton.setTitle("Forgot Password?".localized, for: .normal)
//        languageView.layer.cornerRadius = 16.0
//        languageView.layer.masksToBounds = true
//
//        loginView.layer.cornerRadius = 16.0
//        loginView.layer.masksToBounds = true
        
        languageView.layer.cornerRadius = 12
        languageView.layer.shadowColor = UIColor.lightGray.cgColor
        languageView.layer.shadowOffset = CGSize.zero
        languageView.layer.shadowOpacity = 1.0
        languageView.layer.shadowRadius = 5.0
        languageView.layer.masksToBounds =  false
        
        loginView.layer.cornerRadius = 12
        loginView.layer.shadowColor = UIColor.lightGray.cgColor
        loginView.layer.shadowOffset = CGSize.zero
        loginView.layer.shadowOpacity = 1.0
        loginView.layer.shadowRadius = 5.0
        loginView.layer.masksToBounds =  false
        
        
        
         mobileNumberTF.placeholder = "Mobile Number".localized
        mobileNumberTF.becomeFirstResponder()
       // passwordTF.placeholder = "Password".localized
    }
    
    @IBAction func loginButtonClick(_ sender: Any) {
        
        //self.loginCompleteAction()
       // var validate = Validation()
        if mobileNumberTF.text == "09" || mobileNumberTF.text == "" {
            AppUtility.alert(message: "Please Enter Mobile Number".localized, title: "", controller: self)
        }
//        else {
//            let count = self.mobileNumberTF.text?.count
//            switch currentSelectedCountry {
//
//            case .indiaCountry:
//                if count! < 10 {
//                    AppUtility.alert(message: "Please enter your valid mobile number".localized, title: "", controller: self)
//                   // loginbutton.isEnabled = false
//                    return
//                }
//
//            case .myanmarCountry:
//                if count! < 9 {
//                    AppUtility.alert(message: "Please enter your valid mobile number".localized, title: "", controller: self)
//                   // loginbutton.isEnabled = false
//                    return
//                }
//            case .chinaCountry:
//                if count! < 11 {
//                    AppUtility.alert(message: "Please enter your valid mobile number".localized, title: "", controller: self)
//                   // loginbutton.isEnabled = false
//                    return
//                }
//
//            case .thaiCountry:
//                if count! < 9 {
//                    AppUtility.alert(message: "Please enter your valid mobile number".localized, title: "", controller: self)
//                   // loginbutton.isEnabled = false
//                    return
//                }
//
//            case .other:
//                if (count! < 4) || (count! > 13) {
//                    AppUtility.alert(message: "Please enter your valid mobile number".localized, title: "", controller: self)
//                   // loginbutton.isEnabled = false
//                    return
//                }
//            }
//
//            mobileNumberTF.resignFirstResponder()
//            //passwordTF.resignFirstResponder()
//
//             //   if NetWorkCode.checkifSimAvailableInPhone(){
//                    if AppUtility.isConnectedToNetwork(){
//                        var paramDic  = [String: Any]()
//                        let mobileNo = mobileNumberTF.text ?? ""
////                        if mobileNo.hasPrefix("09"){
////                            mobileNo = String(mobileNo.dropFirst())
////                        }
//                        //let password = passwordTF.text ?? ""
//                        //paramDic["Password"] = password
//                        paramDic["Username"] = mobileNo
//                        self.callLoginApi(addressDict: (paramDic as! Dictionary<String, String>))
//                    }
//                    else{
//                        AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
//                    }
////                }else{
////                    AppUtility.alert(message: "Sim Not Available", title: "", controller: self)
////                }
//
//        }
       

        else{
            mobileNumberTF.resignFirstResponder()
            //passwordTF.resignFirstResponder()

             //   if NetWorkCode.checkifSimAvailableInPhone(){
                    if AppUtility.isConnectedToNetwork(){
                        var paramDic  = [String: Any]()
                        let mobileNo = mobileNumberTF.text ?? ""
//                        if mobileNo.hasPrefix("09"){
//                            mobileNo = String(mobileNo.dropFirst())
//                        }
                        //let password = passwordTF.text ?? ""
                        //paramDic["Password"] = password
                        paramDic["Username"] = mobileNo
                        self.callLoginApi(addressDict: (paramDic as! Dictionary<String, String>))
                    }
                    else{
                        AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
                    }
//                }else{
//                    AppUtility.alert(message: "Sim Not Available", title: "", controller: self)
//                }
            }
        }

    @IBAction func backButtonClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func forgotButtonClick(_ sender: Any) {
     
       // self.navigatetoForgotPassword()
    }
    
//    func navigatetoRegister() {
//        DispatchQueue.main.async {
//            [unowned self] in
//              if #available(iOS 13.0, *) {
//                  let sb = UIStoryboard(name: "Login", bundle: nil)
//                  if let baseObj = sb.instantiateViewController(identifier: "RegistrationViewController") as? RegistrationViewController {
//                      baseObj.modalPresentationStyle = .fullScreen
//                  //  self.navigationController?.present(baseObj, animated: true, completion: nil)
//                    
//                    self.navigationController?.pushViewController(baseObj, animated: true)
//                  }
//              } else {
//                  let  homeVC = UIStoryboard(name: "Login", bundle: nil)
//                  let dashbord = homeVC.instantiateViewController(withIdentifier: "RegistrationViewController") as? RegistrationViewController
//                  
//                  var objNav: UINavigationController? = nil
//                  if let dashbord = dashbord {
//                      objNav = UINavigationController(rootViewController: dashbord)
//                  }
//                  if let objNav = objNav {
//                      dashbord?.modalPresentationStyle = .fullScreen
//                   //  self.navigationController?.present(objNav, animated: true)
//                     self.navigationController?.pushViewController(objNav, animated: true)
//                  }
//                  
//              }
//           }
//      }
    
//    func navigatetoForgotPassword() {
//
//              if #available(iOS 13.0, *) {
//                DispatchQueue.main.async {
//                [unowned self] in
//                  let sb = UIStoryboard(name: "Login", bundle: nil)
//                  if let baseObj = sb.instantiateViewController(identifier: "ForgotPasswordViewController") as? ForgotPasswordViewController {
//                      baseObj.modalPresentationStyle = .fullScreen
//                      self.navigationController?.pushViewController(baseObj, animated: true)
//                   }
//                }
//              } else {
//                  let  homeVC = UIStoryboard(name: "Login", bundle: nil)
//                  let dashbord = homeVC.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as? ForgotPasswordViewController
//
//                  var objNav: UINavigationController? = nil
//                  if let dashbord = dashbord {
//                      objNav = UINavigationController(rootViewController: dashbord)
//                  }
//                  if let objNav = objNav {
//                      dashbord?.modalPresentationStyle = .fullScreen
//                      self.navigationController?.pushViewController(objNav, animated: true)
//                  }
//
//              }
//      }
    
    
}

//MARK: - New Login Flow

extension LoginViewController : OTPDelegate {
    
      func registerClick() {
            
                if AppUtility.isConnectedToNetwork(){
                    
                    let inputString = mobileNumberTF.text ?? ""
                    let outputString = String(inputString.dropFirst())
                    let finalmobilestring = "0095" + outputString
  
               let registrationRequestData:Dictionary<String,Any> = [ "Firstname": "SuperMart",
                                                                      "Lastname": "Vendor",
                                                                      "Email": "",
                                                                      "Username": finalmobilestring,
                                                                      "Mode":"Login",
                                                                      "Gender" : "non",
                                                                      "DateOfBirthDay":"11",
                                                                      "DateofBirthMonth":"01",
                                                                      "DateOfBirthYear":"2020",
                                                                      "Address1": "non",
                                                                      "Address2": "non",
                                                                      "CityId": 5690,
                                                                      "StateId": 83,
                                                                      "Country": "Myanmar",
                                                                      "OtherEmail": "",
                                                                      "MobileNumber": finalmobilestring,
                                                                      "Password": "123456",
                                                                      "HouseNo": "non",
                                                                      "FloorNo": "non",
                                                                      "RoomNo":"non",
                                                                      "ShopName":"SuperMart",
                                                                      "Latitude": "0.0",
                                                                      "Longitude": "0.0",
                                                                      "MarketId":2,
                                                                      "FacebookId":"",
                                                                      "ShopLocationType":1,
                                                                      "IsWholeSaleMarket":false,
                                                                      "Token":" ",
                                                                      "DeviceID":apiManager.deviceId,
                                                                      "VersionCode":1,
                                                                      "Simid": apiManager.deviceId
                ]
                    self.callRegisterApi(addressDict: (registrationRequestData))
                }
                else{
                    AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
                }
              
        }
        
         private func callRegisterApi(addressDict: Dictionary<String, Any>?) {
             AppUtility.showLoading(self.view)
            apiManager.RegisterUser(addressDict! as [String : AnyObject], onSuccess: { [weak self] response in
                        
                        DispatchQueue.main.async{
                            if let viewLoc = self?.view {
                                AppUtility.hideLoading(viewLoc)
                            }
                            let json = JSON(response)
                            println_debug("Register Response: \(json)")
                            if let code = json["StatusCode"].int, code == 200 {
                                if let registerInfoDic  = json["Data"].dictionaryObject {
                                    let vmTemoModel = VMTempModel()
                                    vmTemoModel.wrapDataModel(JSON: registerInfoDic)
                                    print(registerInfoDic)
                                    DispatchQueue.main.async {
                                            UserDefaults.standard.set(true, forKey: AppConstants.userDefault.loginStatus)
                                            UserDefaults.standard.set(true, forKey: AppConstants.userDefault.enabledLogout)
                                            UserDefaults.standard.synchronize()
                                        appDelegate.FCMToken = registerInfoDic["Token"] as! String
                                            let dataDict:[String: String] = ["token": appDelegate.FCMToken]
                                            NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
                                            self?.loginCompleteAction()
                                    }
                                }
                                //self?.handleLogin()
                            }else {
                                let message = json["ErrorList"][0].string
                                let alert = UIAlertController(title: "OSK Vendor".localized, message: message, preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: nil))
                                self?.present(alert, animated: true, completion: nil)
                            }
                        }
                        }, onError: { [weak self] message in
                            DispatchQueue.main.async {
                            if let viewLoc = self?.view {
                                    AppUtility.hideLoading(viewLoc)
                                self?.showAlert(alertTitle: "Login".localized, description: "Please try again later!".localized)
                                  
                                }
                            }
                    })
            
        }
        
    
    //MARK:- Button actions
       @IBAction func verifyButtonAction(_ sender: UIButton) {
        if self.mobileNumberTF.text!.count > 2 {
            DispatchQueue.main.async {
                //self.openSMS()
                self.checkUserLogin()
            }
        }else{
            AppUtility.showToastlocal(message: "Please enter valid mobile number".localized, view: self.view)
           }
       }
       
    
    private func checkUserLogin() {
          self.view.endEditing(true)
          let urlString = String(format: "%@", APIManagerClient.sharedInstance.base_url_sendSMS)
          guard let url = URL(string: urlString) else { return }
          let params = AppUtility.JSONStringFromAnyObject(value: self.getSendSMSParams() as AnyObject)
          println_debug(params)
          AppUtility.showLoading(self.view)
          apiManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, _) in
              DispatchQueue.main.async {
                  if let viewLoc = self?.view {
                      AppUtility.hideLoading(viewLoc)
                  }
              }
              if success {
                  if let json = response as? Dictionary<String,Any> {
                      println_debug(json)
                      if let code = json["StatusCode"] as? Int, code == 200 {
                          //RegistrationModel.share.MobileNumber = json["DestinationNumber"] as? String ?? ""
                          self?.randomOTP = json["OTP"] as? String ?? ""
                        if json["IsUserRegistered"] as? Int ?? 0 == 1 {
                          self?.showOtpScreen()
                        } else {
                            DispatchQueue.main.async {

                            self?.showAlert(alertTitle: "Login".localized, description: "Please Register!".localized)
                            }
                        }
                       // self?.showOtpScreen()
                          self?.timer.invalidate()
                      } else {
                          DispatchQueue.main.async {
                              
                              self?.showAlert(alertTitle: "Login".localized, description: "Please Check Internet Connection".localized)
                          }
                      }
                  }
              }else {
                  DispatchQueue.main.async {
                    self?.showAlert(alertTitle: "Login".localized, description: response as! String)
                  }
                  
              }
          }
      }
      
      func getSendSMSParams() -> [String:Any] {
          var paramDic            = [String:Any]()
          paramDic["Application"]  = "1 Stop Mart"
          var mobileNo = mobileNumberTF.text ?? ""
          if mobileNo.hasPrefix("09") {
              mobileNo = "0095" + mobileNo.dropFirst()
          }
          paramDic["DestinationNumber"]  = mobileNo
          paramDic["Operator"]  = appDelegate.getNetworkCode().1
          paramDic["AppVersionName"] = "1.0.00"
          paramDic["DeviceTypeId"] =  APIManagerClient.sharedInstance.DeviceTypeId
          paramDic["BuildType"] = "debug"
          return paramDic
      }
      
    func selectedOTPNumber(otpNumber: String , vc:OTPViewController) {
          //self.verifyBtn.setTitle("Verify".localized, for: .normal)
          DispatchQueue.main.async {
              self.view.endEditing(true)
          }
        if randomOTP == otpNumber {
            DispatchQueue.main.async {
                self.registerClick()
            }
            
        } else {
              //otp doesn't match\
              let alertVC = SAlertController()
              alertVC.ShowSAlert(title: "Warning!".localized, withDescription: "Please enter valid OTP".localized, onController: self)
              let ok = SAlertAction()
              ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                  self.showOtpScreen()
              })
              alertVC.addAction(action: [ok])
          }
      }
      
    private func showOtpScreen() {
          DispatchQueue.main.async {
              let sb = UIStoryboard(name: "Login", bundle: nil)
              let oTPViewController  = sb.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
              oTPViewController.modalPresentationStyle = .overCurrentContext
              oTPViewController.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
              oTPViewController.delegate = self
              self.present(oTPViewController, animated: false, completion: nil)
              self.smsOtpTxt = ""
          }
      }
      
}

extension LoginViewController: MFMessageComposeViewControllerDelegate {
    func openSMS() {
        seconds = 50
        var defaultNumber = [String]()
        defaultNumber.append("+959252077892")
        let manager  =  UUID2
        let msg = String.init(format: "1 Stop Mart-#%@", manager)
        if (MFMessageComposeViewController.canSendText()) {
            let controller        = MFMessageComposeViewController()
            controller.body       = msg
            controller.recipients = defaultNumber
            controller.messageComposeDelegate = self
            controller.modalPresentationStyle = .fullScreen
            
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult){
        
        switch result {
        case .sent:
            controller.dismiss(animated: true, completion: nil)
            let time : DispatchTime = DispatchTime.now() + 30.0
            //PTLoader.shared.show()
            AppUtility.showLoading(self.view)
            DispatchQueue.main.async {
                self.timer.fire()
                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
            }
            DispatchQueue.main.asyncAfter(deadline: time, execute: {
                AppUtility.hideLoading(self.view)
                self.checkUserLogin()
            })
            break
        case .cancelled:
            controller.dismiss(animated: true, completion: nil)
            println_debug("remove window")
            break
        case .failed:
            controller.dismiss(animated: true, completion: nil)
            println_debug("popup remove, dismiss")
            break
        @unknown default:
            println_debug("default")
        }
    }
    
    @objc func updateTimer() {
        seconds -= 1
//        let string = "Verifying".localized + " " + String(seconds) +  " Sec"
//        verifyBtn.setTitle(string, for: .normal)
        if seconds == 0 {
            self.timer.invalidate()
        }
    }
}

