//
//  OTPViewController.swift
//  OK
//
//  Created by Uma Rajendran on 8/14/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

protocol OTPDelegate: class {
    func selectedOTPNumber(otpNumber: String , vc:OTPViewController)
}

protocol MyTextFieldDelegate {
    func textFieldDidDelete()
}
class OTPViewController: UIViewController,MyTextFieldDelegate {
    
    
    @IBOutlet weak var imgOTP: UIImageView!
    
    @IBOutlet var otpHeaderLabel: UILabel!{
        didSet{
            self.otpHeaderLabel.font = UIFont(name: appFont, size: 15.0)
            self.otpHeaderLabel.text = self.otpHeaderLabel.text?.localized
        }
    }
    @IBOutlet var otpDescriptionLabel: UILabel!{
        didSet{
            self.otpDescriptionLabel.font = UIFont(name: appFont, size: 15.0)
            self.otpDescriptionLabel.text = self.otpDescriptionLabel.text?.localized
        }
    }
    
    @IBOutlet var otpContentView: UIView!
    
    @IBOutlet var firstNumber: MyTextField!
    
    @IBOutlet var secondNumber: MyTextField!
    
    @IBOutlet var thirdNumber: MyTextField!
    
    @IBOutlet var fourthNumber: MyTextField!
    
    @IBOutlet var fifthNumber: MyTextField!
    
    @IBOutlet var sixthNumber: MyTextField!
    @IBOutlet weak var lblOTP: UILabel! {
        didSet {
            self.lblOTP.font = UIFont(name: appFont, size: 15.0)
            self.lblOTP.text = self.lblOTP.text?.localized
            
        }
    }
    
    var activeField: UITextField!
    var delegate: OTPDelegate?
    var fullOTPNumber = ""
    let gifManager = SwiftyGifManager(memoryLimit:6)
    override func viewDidLoad() {
        super.viewDidLoad()
        firstNumber.myDelegate = self
        firstNumber.delegate = self
        
        secondNumber.myDelegate = self
        secondNumber.delegate = self
        
        thirdNumber.myDelegate = self
        thirdNumber.delegate = self
        
        fourthNumber.myDelegate = self
        fourthNumber.delegate = self
        
        fifthNumber.myDelegate = self
        fifthNumber.delegate = self
        
        sixthNumber.myDelegate = self
        sixthNumber.delegate = self
        
        otpContentView.layer.cornerRadius = 10.0
        otpContentView.layer.masksToBounds = true
        lblOTP.text = "Enter the 6 digit code you received via SMS on your mobile number.".localized
        updateLocalizations()
        //  self.view.backgroundColor = kGradientGreyColor
        // loadFaceGIF()
    }
    
    func loadFaceGIF() {
        self.imgOTP.gifImage = nil
        //let gif = UIImage(gifName: "otp_mobile_anime")
        //self.imgOTP.setGifImage(gif, manager: gifManager)
    }
    
    func textFieldDidDelete() {
        
        if secondNumber.text?.count == 0 {
            firstNumber.text = ""
            firstNumber.becomeFirstResponder()
        }else if thirdNumber.text?.count == 0 {
            secondNumber.text = ""
            secondNumber.becomeFirstResponder()
        }else if fourthNumber.text?.count == 0 {
            thirdNumber.text = ""
            thirdNumber.becomeFirstResponder()
        }else if fifthNumber.text?.count == 0 {
            fourthNumber.text = ""
            fourthNumber.becomeFirstResponder()
        }else if sixthNumber.text?.count == 0 {
            fifthNumber.text = ""
            fifthNumber.becomeFirstResponder()
        }
    }
    
    
    @IBAction func doneBtnAction(_ sender: UIButton) {
        if !validateAllFields() {
            return
        }
        
        
        fullOTPNumber = "\(firstNumber.text!)" + secondNumber.text! + thirdNumber.text! + fourthNumber.text! + fifthNumber.text!
        fullOTPNumber += sixthNumber.text!
        println_debug("full otp number :: \(fullOTPNumber)")
        activeField.resignFirstResponder()
        delegate?.selectedOTPNumber(otpNumber: fullOTPNumber, vc:self)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func tapGesture(_ sender: Any) {
         self.view.removeFromSuperview()
         self.dismiss(animated: false, completion: nil)
    }
    
    func validateAllFields() -> Bool {
        if ((firstNumber.text?.count)! > 0) && ((secondNumber.text?.count)! > 0) && ((thirdNumber.text?.count)! > 0) && ((fourthNumber.text?.count)! > 0) && ((fifthNumber.text?.count)! > 0) && ((sixthNumber.text?.count)! > 0) {
            return true
        }
        return false
    }
    func updateLocalizations() {
        // doneBtn.titleLabel?.font = UIFont.init(setLabelFont: 18)
        // doneBtn.titleLabel?.text = self.setLocStr(str: "Done")
        // doneBtn.backgroundColor = kYellowColor
        // otpHeaderLabel.font = UIFont.init(setLabelFont: 20)
        // otpDescriptionLabel.font = UIFont.init(setLabelFont: 16)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        firstNumber.text = ""
        secondNumber.text = ""
        thirdNumber.text = ""
        fourthNumber.text = ""
        fifthNumber.text = ""
        sixthNumber.text = ""
        firstNumber.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        activeField.resignFirstResponder()
    }
}


extension OTPViewController: UITextFieldDelegate {
    //MARK: - TextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        println_debug("textFieldDidBeginEditing")
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        println_debug("textFieldDidEndEditing")
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField
        println_debug("textFieldShouldBeginEditing")
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        println_debug("textFieldShouldClear")
        textField.text = ""
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        println_debug("textFieldShouldEndEditing")
        if textField == sixthNumber {
            let btn = UIButton()
            self.doneBtnAction(btn)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        println_debug("shouldChangeCharactersIn")
        
        let text = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        
        println_debug("typing text :::: \(text)")
        
        // On inputing value to textfield
        if (textField.text!.count < 1  && string.count > 0){
            let nextTag = textField.tag + 1
            
            // get next responder
            var nextResponder = textField.superview?.viewWithTag(nextTag);
            
            if (nextResponder == nil){
                nextResponder = textField.superview?.viewWithTag(1)
            }
            textField.text = string
            println_debug("added numbers are ::: \(string)")
            nextResponder?.becomeFirstResponder()
            return false;
        }
        else if (textField.text!.count >= 1  && string.count == 0){
            // on deleting value from Textfield
            let previousTag = textField.tag - 1
            
            // get next responder
            var previousResponder = textField.superview?.viewWithTag(previousTag)
            
            if (previousResponder == nil){
                previousResponder = textField.superview?.viewWithTag(1)
            }
            textField.text = ""
            println_debug("removed numbers are ::: \(string)")
            previousResponder?.becomeFirstResponder()
            return false
        }else if (textField.text!.count >= 1  && string.count > 0) {
            
            return false
        }
        if textField == sixthNumber {
            let btn = UIButton()
            self.doneBtnAction(btn)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        println_debug("textFieldShouldReturn")
        
        textField.resignFirstResponder()
        return true
    }
    
}

class MyTextField: UITextField {
    var myDelegate: MyTextFieldDelegate?
    override func deleteBackward() {
        super.deleteBackward()
        myDelegate?.textFieldDidDelete()
    }
    
}
