//
//  LoginVC_TextFiled.swift
//  OSKVendor
//
//  Created by vamsi on 6/30/20.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Foundation
import UIKit

var maxstr = ""
//MARK:- TextField Delegate & Functions
extension LoginViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        textField.text =  (self.country?.dialCode == "+95") ? "09" : ""
//         self.clearButton.isHidden = true
//         self.loginButton.backgroundColor = .lightGray
        
        if  currentSelectedCountry == .myanmarCountry  {
            
            if let text = textField.text, text.count < 3 {
                textField.text = "09"
               // self.clearButton.isHidden = true
             //   self.loginbutton.backgroundColor = .lightGray
             //   self.loginbutton.isEnabled = false
               
            }
        }
        
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if self.country?.dialCode == "+95" {
            textField.text = "09"
        } else {
            textField.text = ""
        }
        return false
    }
      
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      //  self.loginbutton.isUserInteractionEnabled = true
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return false
        }
        if currentSelectedCountry == .myanmarCountry {
            if range.location <= 1 {
                let newPosition = textField.endOfDocument
                textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                return false
            }
        }
        
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return false }
        switch currentSelectedCountry {
        case .indiaCountry:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
           // clearButtonShowHide(count: textCount)
            if textField.text!.count >= 10 && range.length == 0 {
                return false
            }
            if textCount >= 10 {
                 textField.text = text
//                loginbutton.backgroundColor = themeColor
//                self.loginbutton.isEnabled = true
                 self.mobileNumberTF.resignFirstResponder()
                return false
            } else {
              //  loginbutton.setTitleColor(.white, for: .normal)
//                loginbutton.backgroundColor = UIColor.lightGray
//                self.loginbutton.isEnabled = false
                return true
            }
        case .chinaCountry:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
          //  clearButtonShowHide(count: textCount)
            if textField.text!.count >= 11 && range.length == 0 {
                return false
            }
            
            if textCount >= 11  {
                 textField.text = text
              
//                loginbutton.backgroundColor = themeColor
//                 self.loginbutton.isEnabled = true
                 self.mobileNumberTF.resignFirstResponder()
                return false
            } else {
               // loginbutton.setTitleColor(.white, for: .normal)
//                loginbutton.backgroundColor = UIColor.lightGray
//                 self.loginbutton.isEnabled = false
                return true
            }
        case .myanmarCountry:
            if (range.location == 0 && range.length == 0) || (range.location == 1 && range.length == 0)
            {
                return false
            }
            
            if range.location == 0 && range.length > 1 {
                textField.text = "09"
               // self.clearButton.isHidden = true
                return false
            }
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
            if textCount > 2 {
               // self.clearButton.isHidden = false
            } else {
              //  self.clearButton.isHidden = true
            }
            
            let object = validObj.getNumberRangeValidation(text)
            // let validCount = object.min
            print("@@@@",object)
            
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            if (isBackSpace == -92) {
                if textField.text == "09" {
                    textField.text = "09"
                   // self.clearButton.isHidden = true
                    return false
                }
            }
            
            if object.isRejected == true {
                textField.text = "09"
             //   self.clearButton.isHidden = true
                textField.resignFirstResponder()
                
                let text = "Invalid Mobile Number".localized
                
                AppUtility.alert(message: text, title: "", controller: self)
                //  self.showAlert(text, image: nil, simChanges: false)
                return false
            }
            if textCount < object.min {
//                self.loginbutton.isEnabled = false
//                self.loginbutton.backgroundColor = .lightGray
            } else if (textCount >= object.min && textCount < object.max) {
               // self.loginbutton.isEnabled = true
               // loginbutton.setTitleColor(.white, for: .normal)
              //  self.loginbutton.backgroundColor = themeColor
            } else if (textCount == object.max) {
                textField.text = text
//                loginbutton.setTitleColor(.white, for: .normal)
//                self.loginbutton.backgroundColor = themeColor
//               // self.loginbutton.sendActions(for: .touchUpInside)
//                self.loginbutton.isEnabled = true
                self.mobileNumberTF.resignFirstResponder()
                return false
            } else if textCount > object.max {
                return false
            }
        case .thaiCountry:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
          //  clearButtonShowHide(count: textCount)
            if textField.text!.count >= 9 && range.length == 0 {
                return false
            }
            
            if textCount >= 9  {
                textField.text = text
//                loginbutton.setTitleColor(.white, for: .normal)
//                loginbutton.backgroundColor = themeColor
//                // self.loginButton.sendActions(for: .touchUpInside)
//                 self.loginbutton.isEnabled = true
                 self.mobileNumberTF.resignFirstResponder()
                return false
            }  else {
                //loginbutton.setTitleColor(.white, for: .normal)
//                loginbutton.backgroundColor = .lightGray
//                 self.loginbutton.isEnabled = false
                return true
            }
            
        case .other:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
         //   clearButtonShowHide(count: textCount)
            
            if textField.text!.count >= 13 && range.length == 0 {
                return false
            }
            
            
            if textCount >= 4 {
                textField.text = text
               // loginbutton.setTitleColor(.white, for: .normal)
//                loginbutton.backgroundColor = themeColor
//                self.loginbutton.isEnabled = true
              if textCount == 13 {
                textField.text = text
                // self.loginbutton.sendActions(for: .touchUpInside)
                self.mobileNumberTF.resignFirstResponder()
              }
                return false
            }
            
            if textCount > 13 {
                textField.text = text
               // loginbutton.setTitleColor(.white, for: .normal)
//                loginbutton.backgroundColor = themeColor
//                 self.loginbutton.isEnabled = true
                // self.loginbutton.sendActions(for: .touchUpInside)
                 self.mobileNumberTF.resignFirstResponder()
                return false
            }
            else {
                //logiloginbuttonnButton.setTitleColor(.white, for: .normal)
//                loginbutton.backgroundColor = .lightGray
//                 self.loginbutton.isEnabled = false
                return true
            }
            
//            if textCount > 13 {
//                loginButton.setTitleColor(.white, for: .normal)
//                loginButton.backgroundColor = .orange
//                //self.registerButton.sendActions(for: .touchUpInside)
//                return false
//            }
//
//            if textCount > 3  {
//                self.mobileNumTF.text = text
//                self.registerButton.isEnabled = true
//                loginButton.setTitleColor(.white, for: .normal)
//                self.loginButton.backgroundColor = .orange
//                //self.submitBtn.sendActions(for: .touchUpInside)
//                return false
//            }
//            else {
//                self.loginButton.isEnabled = false
//                self.loginButton.backgroundColor = .lightGray
//                return true
//            }
      }
        
        return true
        
    }
    
    
    
    
}
