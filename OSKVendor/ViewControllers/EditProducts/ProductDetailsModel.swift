//
//  ProductDetailsModel.swift
//  OSKVendor
//
//  Created by Sam' MacBook on 16/09/20.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit



// MARK: - GetPrdouctDetailsModel
struct GetPrdouctDetailsModel: Codable {
    var data: DataClass?
    var successMessage: JSONNull?
    var statusCode: Int?
    var errorList: [JSONAny]?

    enum CodingKeys: String, CodingKey {
        case data = "Data"
        case successMessage = "SuccessMessage"
        case statusCode = "StatusCode"
        case errorList = "ErrorList"
    }
}

// MARK: - DataClass
struct DataClass: Codable {
    var defaultPictureZoomEnabled: Bool?
    var defaultPictureModel: PictureModel?
    var pictureModels: [PictureModelElement]?
    var videoModels: [VideoModel]?
    var name, shortDescription, fullDescription: String?
    var showManufacturerPartNumber: Bool?
    var manufacturerPartNumber: JSONNull?
    var barcode: String?
    var showVendor: Bool?
    var vendorModel: VendorModel?
    var hasSampleDownload: Bool?
    var giftCard: GiftCard?
    var isShipEnabled, isFreeShipping, freeShippingNotificationEnabled: Bool?
    var deliveryDate: String?
    var url: String?
    var isRental: Bool?
    var rentalStartDate, rentalEndDate: JSONNull?
    var stockAvailability: String?
    var displayBackInStockSubscription, emailAFriendEnabled, compareProductsEnabled: Bool?
    var productPrice: ProductPrice?
    var quantity: Quantity?
    var addToCart: AddToCart?
    var breadcrumb: Breadcrumb?
    var productTags: [JSONAny]?
    var productAttributes: [ProductAttribute]?
    var productSpecifications, productManufacturers: [JSONAny]?
    var productReviewOverview: ProductReviewOverview?
    var tierPrices, associatedProducts: [JSONAny]?
    var availableStartDateTimeUTC, availableEndDateTimeUTC: JSONNull?
    var markAsNew: Bool?
    var cgmItemID: JSONNull?
    var published: Bool?
    var nextProduct, previousProduct: Int?
    var productCategoryName: String?
    var otherLanguageDataUnicode: String?
    var id: Int?
    var customProperties: CustomProperties?
    var UOM: String?
    var SizeGuideImagePath : String?
    var BarCodeImageUrl : JSONNull?
    
    enum CodingKeys: String, CodingKey {
        case defaultPictureZoomEnabled = "DefaultPictureZoomEnabled"
        case defaultPictureModel = "DefaultPictureModel"
        case pictureModels = "PictureModels"
        case videoModels = "VideoModels"
        case name = "Name"
        case shortDescription = "ShortDescription"
        case fullDescription = "FullDescription"
        case showManufacturerPartNumber = "ShowManufacturerPartNumber"
        case manufacturerPartNumber = "ManufacturerPartNumber"
        case barcode = "Barcode"
        case showVendor = "ShowVendor"
        case vendorModel = "VendorModel"
        case hasSampleDownload = "HasSampleDownload"
        case giftCard = "GiftCard"
        case isShipEnabled = "IsShipEnabled"
        case isFreeShipping = "IsFreeShipping"
        case freeShippingNotificationEnabled = "FreeShippingNotificationEnabled"
        case deliveryDate = "DeliveryDate"
        case url = "Url"
        case isRental = "IsRental"
        case rentalStartDate = "RentalStartDate"
        case rentalEndDate = "RentalEndDate"
        case stockAvailability = "StockAvailability"
        case displayBackInStockSubscription = "DisplayBackInStockSubscription"
        case emailAFriendEnabled = "EmailAFriendEnabled"
        case compareProductsEnabled = "CompareProductsEnabled"
        case productPrice = "ProductPrice"
        case quantity = "Quantity"
        case addToCart = "AddToCart"
        case breadcrumb = "Breadcrumb"
        case productTags = "ProductTags"
        case productAttributes = "ProductAttributes"
        case productSpecifications = "ProductSpecifications"
        case productManufacturers = "ProductManufacturers"
        case productReviewOverview = "ProductReviewOverview"
        case tierPrices = "TierPrices"
        case associatedProducts = "AssociatedProducts"
        case availableStartDateTimeUTC = "AvailableStartDateTimeUtc"
        case availableEndDateTimeUTC = "AvailableEndDateTimeUtc"
        case markAsNew = "MarkAsNew"
        case cgmItemID = "CGMItemID"
        case published = "Published"
        case nextProduct = "NextProduct"
        case previousProduct = "PreviousProduct"
        case productCategoryName = "ProductCategoryName"
        case otherLanguageDataUnicode = "OtherLanguageDataUnicode"
        case id = "Id"
        case UOM = "UOM"
        case customProperties = "CustomProperties"
        case SizeGuideImagePath = "SizeGuideImagePath"
        case BarCodeImageUrl = "BarCodeImageUrl"
        
    }
}

// MARK: - AddToCart
struct AddToCart: Codable {
    var productID, enteredQuantity: Int?
    var customerEntersPrice: Bool?
    var customerEnteredPrice: Int?
    var customerEnteredPriceRange: JSONNull?
    var disableBuyButton, disableWishlistButton: Bool?
    var allowedQuantities: [JSONAny]?
    var isRental, availableForPreOrder: Bool?
    var preOrderAvailabilityStartDateTimeUTC: JSONNull?
    var updatedShoppingCartItemID: Int?
    var isWishList: Bool?

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case enteredQuantity = "EnteredQuantity"
        case customerEntersPrice = "CustomerEntersPrice"
        case customerEnteredPrice = "CustomerEnteredPrice"
        case customerEnteredPriceRange = "CustomerEnteredPriceRange"
        case disableBuyButton = "DisableBuyButton"
        case disableWishlistButton = "DisableWishlistButton"
        case allowedQuantities = "AllowedQuantities"
        case isRental = "IsRental"
        case availableForPreOrder = "AvailableForPreOrder"
        case preOrderAvailabilityStartDateTimeUTC = "PreOrderAvailabilityStartDateTimeUtc"
        case updatedShoppingCartItemID = "UpdatedShoppingCartItemId"
        case isWishList = "IsWishList"
    }
}

// MARK: - Breadcrumb
struct Breadcrumb: Codable {
    var enabled: Bool?
    var productID: Int?
    var productName, productSEName: String?
    var categoryBreadcrumb: [CategoryBreadcrumb]?
    var customProperties: CustomProperties?

    enum CodingKeys: String, CodingKey {
        case enabled = "Enabled"
        case productID = "ProductId"
        case productName = "ProductName"
        case productSEName = "ProductSeName"
        case categoryBreadcrumb = "CategoryBreadcrumb"
        case customProperties = "CustomProperties"
    }
}

// MARK: - CategoryBreadcrumb
struct CategoryBreadcrumb: Codable {
    var name: String?
    var seName, numberOfProducts: JSONNull?
    var includeInTopMenu: Bool?
    var subCategories: [JSONAny]?
    var id: Int?
    var form: JSONNull?
    var customProperties: CustomProperties?

    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case seName = "SeName"
        case numberOfProducts = "NumberOfProducts"
        case includeInTopMenu = "IncludeInTopMenu"
        case subCategories = "SubCategories"
        case id = "Id"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}

// MARK: - CustomProperties
struct CustomProperties: Codable {
}

// MARK: - PictureModel
struct PictureModel: Codable {
    var id: Int?
    var imageURL, fullSizeImageURL: String?
    var title: String?
    var alternateText, form: JSONNull?
    var customProperties: CustomProperties?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case imageURL = "ImageUrl"
        case fullSizeImageURL = "FullSizeImageUrl"
        case title = "Title"
        case alternateText = "AlternateText"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}

// MARK: - GiftCard
struct GiftCard: Codable {
    var isGiftCard: Bool?
    var recipientName, recipientEmail, senderName, senderEmail: JSONNull?
    var message: JSONNull?
    var giftCardType: Int?
    var customProperties: CustomProperties?

    enum CodingKeys: String, CodingKey {
        case isGiftCard = "IsGiftCard"
        case recipientName = "RecipientName"
        case recipientEmail = "RecipientEmail"
        case senderName = "SenderName"
        case senderEmail = "SenderEmail"
        case message = "Message"
        case giftCardType = "GiftCardType"
        case customProperties = "CustomProperties"
    }
}

// MARK: - PictureModelElement
struct PictureModelElement: Codable {
    var productID, pictureID: Int?
    var pictureURL: String?
    var displayOrder: Int?
    var overrideAltAttribute, overrideTitleAttribute, file: JSONNull?
    var id: Int?
    var customProperties: CustomProperties?

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case pictureID = "PictureId"
        case pictureURL = "PictureUrl"
        case displayOrder = "DisplayOrder"
        case overrideAltAttribute = "OverrideAltAttribute"
        case overrideTitleAttribute = "OverrideTitleAttribute"
        case file = "File"
        case id = "Id"
        case customProperties = "CustomProperties"
    }
}

// MARK: - ProductAttribute
struct ProductAttribute: Codable {
    var productID, productAttributeID: Int?
    var name: String?
    var productAttributeDescription: String?
    var textPrompt: String?
    var isRequired: Bool?
    var defaultValue, selectedDay, selectedMonth, selectedYear: JSONNull?
    var allowedFileExtensions: [JSONAny]?
    var attributeControlType: Int?
    var values: [Value]?
    var id: Int?
    var customProperties: CustomProperties?

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case productAttributeID = "ProductAttributeId"
        case name = "Name"
        case productAttributeDescription = "Description"
        case textPrompt = "TextPrompt"
        case isRequired = "IsRequired"
        case defaultValue = "DefaultValue"
        case selectedDay = "SelectedDay"
        case selectedMonth = "SelectedMonth"
        case selectedYear = "SelectedYear"
        case allowedFileExtensions = "AllowedFileExtensions"
        case attributeControlType = "AttributeControlType"
        case values = "Values"
        case id = "Id"
        case customProperties = "CustomProperties"
    }
}

// MARK: - Value
struct Value: Codable {
    var name: String?
    var colorSquaresRGB: String?
    var priceAdjustment: JSONNull?
    var priceAdjustmentValue: Int?
    var isPreSelected: Bool?
    var size: String?
    var pictureModel: PictureModel?
    var id: Int?
    var disable: Bool?
    var customProperties: CustomProperties?

    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case colorSquaresRGB = "ColorSquaresRgb"
        case priceAdjustment = "PriceAdjustment"
        case priceAdjustmentValue = "PriceAdjustmentValue"
        case isPreSelected = "IsPreSelected"
        case size = "Size"
        case pictureModel = "PictureModel"
        case id = "Id"
        case disable = "Disabled"
        case customProperties = "CustomProperties"
    }
}

// MARK: - ProductPrice
struct ProductPrice: Codable {
    var currencyCode: String?
    var oldPrice: JSONNull?
    var price: String?
    var priceWithDiscount: JSONNull?
    var priceValue, priceWithDiscountValue: Int?
    var customerEntersPrice, callForPrice: Bool?
    var productID: Int?
    var hidePrices, isRental: Bool?
    var rentalPrice: JSONNull?
    var displayTaxShippingInfo: Bool?
    var basePricePAngV: JSONNull?
    var discountPercentage: Int?
    var discountOfferName: String?
    var customProperties: CustomProperties?
    
    enum CodingKeys: String, CodingKey {
        case currencyCode = "CurrencyCode"
        case oldPrice = "OldPrice"
        case price = "Price"
        case priceWithDiscount = "PriceWithDiscount"
        case priceValue = "PriceValue"
        case priceWithDiscountValue = "PriceWithDiscountValue"
        case customerEntersPrice = "CustomerEntersPrice"
        case callForPrice = "CallForPrice"
        case productID = "ProductId"
        case hidePrices = "HidePrices"
        case isRental = "IsRental"
        case rentalPrice = "RentalPrice"
        case displayTaxShippingInfo = "DisplayTaxShippingInfo"
        case basePricePAngV = "BasePricePAngV"
        case discountPercentage = "DiscountPercentage"
        case discountOfferName = "DiscountOfferName"
        case customProperties = "CustomProperties"
    }
}

// MARK: - ProductReviewOverview
struct ProductReviewOverview: Codable {
    var productID, ratingSum: Int?
    var allowCustomerReviews: Bool?
    var totalReviews: Int?
    var ratingURLFromGSMArena: JSONNull?

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case ratingSum = "RatingSum"
        case allowCustomerReviews = "AllowCustomerReviews"
        case totalReviews = "TotalReviews"
        case ratingURLFromGSMArena = "RatingUrlFromGSMArena"
    }
}

// MARK: - Quantity
struct Quantity: Codable {
    var orderMinimumQuantity, orderMaximumQuantity, stockQuantity: Int?

    enum CodingKeys: String, CodingKey {
        case orderMinimumQuantity = "OrderMinimumQuantity"
        case orderMaximumQuantity = "OrderMaximumQuantity"
        case stockQuantity = "StockQuantity"
    }
}

// MARK: - VendorModel
struct VendorModel: Codable {
    var name, seName: String?
    var id: Int?
    var customProperties: CustomProperties?

    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case seName = "SeName"
        case id = "Id"
        case customProperties = "CustomProperties"
    }
}

// MARK: - VideoModel
struct VideoModel: Codable {
    var productID, videoID: Int?
    var videoURL: String?
    var displayOrder: Int?
    var name: String?
    var file: JSONNull?
    var id: Int?
    var customProperties: CustomProperties?

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case videoID = "VideoId"
        case videoURL = "VideoUrl"
        case displayOrder = "DisplayOrder"
        case name = "Name"
        case file = "File"
        case id = "Id"
        case customProperties = "CustomProperties"
    }
}
