//
//  ProductBarScanner.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 16/09/2020.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit
import AVFoundation

protocol ProductBarScannerDelegate:class {
    func returnBarCode(barCade: String)
}

class ProductBarScanner: UIViewController {
    var captureSession: AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    let scanSize = CGSize(width: 200.0, height: 200.0)
    var contentW: CGFloat = 0.0
    var contentH: CGFloat = 0.0
    var DynView = UIView()
    var scanImg = UIImageView()
    var delegate: ProductBarScannerDelegate?
    var isPresented : Bool = false
    
    let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                              AVMetadataObject.ObjectType.code39,
                              AVMetadataObject.ObjectType.ean13,
                              AVMetadataObject.ObjectType.qr]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let screenSize = UIScreen.main.bounds.size
        self.navigationItem.title = "Scan Barcode".localized
        contentW = screenSize.width
        contentH = screenSize.height
    
        // let captureMetadataOutput = AVCaptureMetadataOutput()
            let metadataOutput = AVCaptureMetadataOutput()

        if ((captureSession?.canAddOutput(metadataOutput)) != nil) {
            captureSession?.addOutput(metadataOutput)
                metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                metadataOutput.metadataObjectTypes = [.ean8, .ean13, .pdf417, .upce]
            }

        self.checkCameraAccessAndOpen()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.global(qos: .background).async {
            self.captureSession?.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        DispatchQueue.main.async {
            if (self.captureSession?.isRunning) != nil {
                self.captureSession?.stopRunning()
            }
        }
    }

}

extension ProductBarScanner {
    
    func setUpQRMask() {
        let centerRect = CGRect(x: (contentW-scanSize.width)/2.0, y: (contentH-scanSize.height)/2.0, width: scanSize.width, height: scanSize.height)
        let path = UIBezierPath(rect: view.bounds)
        let centerPath = UIBezierPath(rect: centerRect)
        path.append(centerPath)
        path.usesEvenOddFillRule = true
        let fillLayer = CAShapeLayer()
        fillLayer.path = path.cgPath
        fillLayer.fillRule = CAShapeLayerFillRule.evenOdd
        fillLayer.fillColor = UIColor.black.withAlphaComponent(0.3).cgColor
        
        scanImg = UIImageView(frame: CGRect(x: (contentW-scanSize.width - 2)/2.0, y: (contentH-scanSize.height - 4)/2.0, width: scanSize.width + 2 , height: scanSize.height + 5 ))
        //        DynView = UIView(frame: CGRect(x: (contentW-scanSize.width + 20)/2.0, y: scanImg.frame.minY + 20, width: self.scanSize.width - 20, height: 3))
        DynView = UIView(frame: CGRect(x: (contentW-scanSize.width + 20)/2.0, y: scanImg.frame.minY + 100, width: self.scanSize.width - 20, height: 1))
        DynView.backgroundColor = UIColor.red
        scanImg.image =  UIImage(named: "scan_bg")
        view.addSubview(scanImg)
        view.addSubview(DynView)
        view.layer.addSublayer(fillLayer)
               self.moveTopToBottom()
    }
    
    private func moveTopToBottom() {
        DynView.frame = CGRect(x: (self.contentW - self.scanSize.width + 20)/2.0, y: scanImg.frame.minY + 20, width: self.scanSize.width - 20, height: 3)
        let mediumInterval: TimeInterval = 4.0
        UIView.animate(withDuration: mediumInterval, animations: {
            self.DynView.frame = CGRect(x: (self.contentW - self.scanSize.width + 20)/2.0 , y: self.scanImg.frame.maxY - 20, width: self.scanSize.width - 20, height: 3)
        }, completion: { (_) in
            self.moveBottomToTop()
        })
    }
    
    private func moveBottomToTop() {
        DynView.frame =  CGRect(x: (self.contentW - self.scanSize.width + 20)/2.0 , y: scanImg.frame.maxY - 20, width: self.scanSize.width - 20 , height: 3)
        let mediumInterval: TimeInterval = 4.0
        UIView.animate(withDuration: mediumInterval, animations: {
            self.DynView.frame = CGRect(x: (self.contentW - self.scanSize.width + 20)/2.0 , y: self.scanImg.frame.minY + 20, width: self.scanSize.width - 20 , height: 3)
        }, completion: { (_) in
            self.moveTopToBottom()
        } )
    }
    
    
    
    
    //MARK: - Methods
    func checkCameraAccessAndOpen() {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            self.addCameraLayer()
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    self.addCameraLayer()
                } else {
                    DispatchQueue.main.async {
                        self.showAlert()
                    }
                }
            })
        }
    }
    
    private func showAlert() {
        let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
        let alertVC = SAlertController()
        alertVC.ShowSAlert(title: "Camera".localized, withDescription: "Camera access required to scan Bar Code".localized, onController: self)
        let cancel = SAlertAction()
        cancel.action(name: "Cancel".localized, AlertType: .defualt, withComplition: {
            // go for dashboard
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)
            }
        })
        let allow = SAlertAction()
        allow.action(name: "Allow Camera".localized, AlertType: .defualt, withComplition: {
            UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
        })
        alertVC.addAction(action: [cancel,allow])
    }
    
    func addCameraLayer() {
        DispatchQueue.main.async  {
            //AVCaptureDevice allows us to reference a physical capture device (video in our case)
            let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
            if let captureDevice = captureDevice {
                do {
                    AppUtility.showLoading(self.view)
                    // CaptureSession needs an input to capture Data from
                    let input = try AVCaptureDeviceInput(device: captureDevice)
                    self.captureSession = AVCaptureSession()
                    self.captureSession?.addInput(input)
                    // CaptureSession needs and output to transfer Data to
                    let captureMetadataOutput = AVCaptureMetadataOutput()
                    self.captureSession?.addOutput(captureMetadataOutput)
                    //We tell our Output the expected Meta-data type
                    captureMetadataOutput.setMetadataObjectsDelegate(self , queue: DispatchQueue.main)
                    captureMetadataOutput.metadataObjectTypes = [.code128, .qr,.ean13, .ean8, .code39, .upce, .aztec, .pdf417] //AVMetadataObject.ObjectType
                    
                    DispatchQueue.main.async {
                        self.captureSession?.startRunning()
                    }
                    //The videoPreviewLayer displays video in conjunction with the captureSession
                    self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession!)
                    self.videoPreviewLayer?.videoGravity = .resizeAspectFill
                    AppUtility.hideLoading(self.view)
                    DispatchQueue.main.async {
                        self.videoPreviewLayer?.frame = self.view.layer.bounds
                        if let vpLayer = self.videoPreviewLayer {
                            self.view.layer.addSublayer(vpLayer)
                            self.setUpQRMask()
                        }
                    }
                } catch {
                    println_debug(error)
                }
            }
            DispatchQueue.main.async {
                self.captureSession?.startRunning()
            }
        }
    }
    
}

//MARK: - AVCaptureMetadataOutputObjectsDelegate
extension ProductBarScanner: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession?.stopRunning()
        self.DynView.isHidden = true
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard var stringValue = readableObject.stringValue else { return }
            if readableObject.type == AVMetadataObject.ObjectType.ean13 && stringValue.hasPrefix("0"){
                let index = stringValue.index(stringValue.startIndex, offsetBy: 1)
                stringValue = String(stringValue[index...])
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                found(code: stringValue)
            }else{
                
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                found(code: stringValue)
            }
            
        }
    }
    func found(code: String) {
        if let del = delegate {
            del.returnBarCode(barCade: code)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

