//
//  GetAllCategories.swift
//  OSKVendor
//
//  Created by Sam' MacBook on 18/09/20.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Foundation
import UIKit


protocol GetAllCategoriesVCDelegate {
    func selectedCategory(category: AllCategoriesData,viewController: UIViewController)
}

class GetAllCategoriesVC: OSKBaseViewController {
    var delegate : GetAllCategoriesVCDelegate?
    let aPIManager = APIManager()
    var apiManagerClient : APIManagerClient?
    var allCategoryModel : GetAllCategoriesModel?

    @IBOutlet var tvCategories: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tvCategories.tableFooterView = UIView()
        self.getAllCategories()
    }
    
    
    func getAllCategories() {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/allcategories", APIManagerClient.sharedInstance.base_url) as String
            print("Get allcategories :\(urlStr) ")
            self.aPIManager.getGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        return
                    }
                    let decode = JSONDecoder()
                    self?.allCategoryModel = try? decode.decode(GetAllCategoriesModel.self, from: jsonData)
                    print("data \(jsonData)")
                    self?.tvCategories.reloadData()
                }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
            })
        }
    }
}


extension GetAllCategoriesVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allCategoryModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GetAllCategoryCell", for: indexPath) as! GetAllCategoryCell
        cell.selectionStyle = .none
        let model = allCategoryModel?.data?[indexPath.row]
        cell.lblCategoryName.text = model?.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let model = allCategoryModel?.data?[indexPath.row] else { return  }
        if let del = delegate {
            del.selectedCategory(category: model, viewController: self)
        }
    }
}


class GetAllCategoryCell: UITableViewCell {
    @IBOutlet var lblCategoryName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
