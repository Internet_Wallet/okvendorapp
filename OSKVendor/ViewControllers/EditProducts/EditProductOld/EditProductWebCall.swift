//
//  EditProductWebCall.swift
//  OSKVendor
//
//  Created by Sam' MacBook on 18/09/20.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Foundation

extension EditProducts {
    
    func deleteCategoryIamge(pictureID: String, handler:@escaping (_ succes: Bool) -> Void) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/deleteproductpicture/%@", APIManagerClient.sharedInstance.base_url,pictureID) as String
            print("Get allcategories :\(urlStr) ")
            self.aPIManager.postGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        return
                    }
                    print(jsonData)
                    handler(true)
                }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    handler(false)
            })
        }
    }
    
    func deleteCategoryVideo(pictureID: String, handler:@escaping (_ succes: Bool) -> Void) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/deleteproductvideo/%@", APIManagerClient.sharedInstance.base_url,pictureID) as String
            print("Get allcategories :\(urlStr) ")
            self.aPIManager.postGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        return
                    }
                    print(jsonData)
                    handler(true)
                }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    handler(false)
            })
        }
    }
    
    
    
    func getProductDetialsByID(productID: String, reloadFor: String) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/getproductbyid/%@", APIManagerClient.sharedInstance.base_url,productID.trimmingCharacters(in: .whitespaces)) as String
            print("Get Product by ID :\(urlStr) ")
            self.aPIManager.getGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        println_debug("Data error")
                        return
                    }
                    do {
                        let decode = JSONDecoder()
                        let model = try? decode.decode(GetPrdouctDetailsModel.self, from: jsonData)
                        self?.productDetailsModel = model
                        println_debug(String(data: jsonData, encoding: .utf8))
               
                        if model?.statusCode == 200 {
                            if reloadFor == "" {
                                if let price =  model?.data?.productPrice?.price, !price.isEmpty {
                                    self?.productPrice = price.replacingOccurrences(of: "MMK", with: "").replacingOccurrences(of: " ", with: "")
                                }else {
                                    self?.productPrice = (model?.data?.productPrice?.price ?? "").replacingOccurrences(of: " ", with: "")
                                }
                                if model?.data?.published ?? false {
                                    self?.btnPublish.setTitle("Hide".localized, for: .normal)
                                }else {
                                    self?.btnPublish.setTitle("Show".localized, for: .normal)
                                }
                                let other = model?.data?.otherLanguageDataUnicode?.replacingOccurrences(of: "\'", with: "\"")
                                let dic = self?.convertStringToArrayOfDictionary(text: other ?? "")
                                self?.productName = model?.data?.name ?? ""
                            
                                self?.productBarCode = model?.data?.barcode ?? ""
                                
                                self?.productShortDescription = model?.data?.shortDescription ?? ""
                              
                                self?.productFullDescription = model?.data?.fullDescription ?? ""
                                if let values = dic as? Dictionary<String,String> {
                                    self?.productNameUni = values["Name"] ?? ""
                                    self?.productShortDescriptionUni = values["ShortDescription"] ?? ""
                                    self?.productFullDescriptionUni = values["FullDescription"] ?? ""
                                }
                                var categoryString = ""
                                if let catArray = model?.data?.breadcrumb?.categoryBreadcrumb {
                                    for (item, element) in catArray.enumerated() {
                                        
                                        if let name = element.name {
                                            categoryString =  categoryString + name
                                        }
                                        
                                        if item < catArray.count - 1 {
                                            categoryString = categoryString + " >> "
                                        }else {
                                            self?.categoryLastName = element.name ?? ""
                                        }
                                        
                                    }
                                    
                                    self?.categoryLastName = catArray.last?.name ?? ""
                                    
                                }
                                
                                self?.productClothing = "\(categoryString)"
                                self?.deliveryDateId.1 = model?.data?.deliveryDate ?? ""
                                if self?.deliveryDateId.1 == "" {
                                    self?.deliveryDateId.0 = "1"
                                }
                                
                             
                                self?.deliveryTimeWebCall(handler: { (isSuccess) in
                                    if isSuccess {
                                        self?.lblLineSeparator.isHidden = false
                                        self?.stackOption.isHidden = false
                                        self?.numberOfRow = 11
                                        self?.tvProduct.delegate = self
                                        self?.tvProduct.dataSource = self
                                        self?.tvProduct.reloadData()
                                        println_debug("Delivery time API Success")
                                        if let deliveryModel = self?.deliverytimeModel?.data {
                                            for item in deliveryModel {
                                                if self?.deliveryDateId.1 == item.name {
                                                    self?.deliveryDateId.0 = item.deliveryDateID ?? ""
                                                }
                                            }
                                        }
                                    }else {
                                        println_debug("Delivery time API failed")
                                    }
                                })
                            }else if reloadFor == "Image" {
                                self?.tvProduct.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                            }else if reloadFor == "ColorAttribute" {
                                
                            }else if reloadFor == "Publish_UnPublish" {
                                if model?.data?.published ?? false {
                                    self?.btnPublish.setTitle("Hide".localized, for: .normal)
                                }else {
                                    self?.btnPublish.setTitle("Show".localized, for: .normal)
                                }
                            }
                        }else {
                            println_debug("Product Attribute Value Not Found")
                        }
                    }
                }
            }, onError: {  message in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                    self.numberOfRow = 0
                    self.lblLineSeparator.isHidden = true
                    self.stackOption.isHidden = true
                    self.tvProduct.reloadData()
                    self.alertVC = SAlertController()
                    self.alertVC?.ShowSAlert(title: "Error", withDescription: "Found error in get product details. Please try again", onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        self.navigationController?.popViewController(animated: true)
                    })
                    self.alertVC?.addAction(action: [YesAction])
                }
            })
        }
    }
    
      func convertStringToArrayOfDictionary(text: String) -> Any? {
            if let data = text.data(using: .utf8) {
                do {
                    return try JSONSerialization.jsonObject(with: data, options: []) as? Any
                } catch {
                    print(error.localizedDescription)
                }
            }
            return nil
        }
    
    func updateProductDetails(params: [String:Any], handler:@escaping (_ succes: Bool) -> Void) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/updateproductfromvendor", APIManagerClient.sharedInstance.base_url) as String
            print("Get allcategories :\(urlStr)\n params\(params)")
            
            self.aPIManager.postAPIParams(url: urlStr,productDetails: params, successBlock: { (dic,isSuccess) in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                    if isSuccess {
                        handler(true)
                    }else {
                        handler(false)
                    }
                }
            }, errorBlock: { (error) in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                }
                handler(false)
            })
        }
    }
    
    func deliveryTimeWebCall(handler: @escaping (_ success: Bool) -> Void) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/GetDeliveryDateList", APIManagerClient.sharedInstance.base_url) as String
            print("Get Deliverytime :\(urlStr) ")
            self.aPIManager.getGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        println_debug("Data error")
                        return
                    }
                    do {
                        let decode = JSONDecoder()
                        let model = try? decode.decode(DeliveryTimeModel.self, from: jsonData)
                        println_debug(String(data: jsonData, encoding: .utf8))
                        if model?.statusCode == 200 {
                            self?.deliverytimeModel = model
                            handler(true)
                        }else {
                            handler(false)
                            println_debug("Product Attribute Value Not Found")
                        }
                    }
                }
            }, onError: {  message in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                    self.alertVC = SAlertController()
                    self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Found error in get delivery time. Please try again".localized, onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                       handler(false)
                    })
                    self.alertVC?.addAction(action: [YesAction])
                }
            })
        }
        
    }
    
    
    func publishAndUnPublishWebCall(productID:String) {
        var isPublish = ""
        if productDetailsModel?.data?.published ?? false {
            isPublish = "false"
        }else {
            isPublish = "true"
        }
        
        
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/UpdateProductStatus/%@/%@", APIManagerClient.sharedInstance.base_url,productID,isPublish) as String
            println_debug("Publish API : \(urlStr)")
            
            self.aPIManager.postGenericWebCall(urlString: urlStr, onSuccess: { data in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        return
                    }
                    do {
                        let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                        println_debug(json)
                        if let dic = json as? Dictionary<String,Any> {
                            if let statusCode = dic["StatusCode"] as? Int, statusCode == 200 {
                                var message = ""
                                if isPublish == "false" {
                                    message = "Your product has been successfully unpublished".localized
                                }else {
                                    message = "Your product has been successfully published".localized
                                }
                                self.alertVC = SAlertController()
                                self.alertVC?.ShowSAlert(title: "", withDescription: message, onController: self)
                                let YesAction = SAlertAction()
                                YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                    self.getProductDetialsByID(productID: productID, reloadFor: "Publish_UnPublish")
                                })
                                self.alertVC?.addAction(action: [YesAction])
                            }else {
                                self.alertVC = SAlertController()
                                self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Found error while Publish / Un Publish. Please try again".localized, onController: self)
                                let YesAction = SAlertAction()
                                YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                    
                                })
                                self.alertVC?.addAction(action: [YesAction])
                            }
                        }
                        
                    } catch {
                        
                    }
                }
            }, onError: { message in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                }
                self.alertVC = SAlertController()
                self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Found error while Publish / Un Publish. Please try again".localized, onController: self)
                let YesAction = SAlertAction()
                YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    
                })
                self.alertVC?.addAction(action: [YesAction])
            })
        }
        
        
    }
    
    func showAlertInClass(title: String,subTitle: String) {
        self.alertVC = SAlertController()
        self.alertVC?.ShowSAlert(title: title, withDescription: subTitle, onController: self)
        let YesAction = SAlertAction()
        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
        })
        self.alertVC?.addAction(action: [YesAction])
    }

    
    func getAllCategories() {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/allcategories", APIManagerClient.sharedInstance.base_url) as String
            self.aPIManager.getGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        return
                    }
                    let decode = JSONDecoder()
                    self?.allCategoryModel = try? decode.decode(GetAllCategoriesModel.self, from: jsonData)
                }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
            })
        }
    }

}


