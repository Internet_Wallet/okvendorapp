//
//  EditProductCell.swift
//  OSKVendor
//
//  Created by Sam' MacBook on 15/09/20.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit

protocol EditProductImagesCellDelegate {
    func reloadProductDetailPage(id: Int)
    func palyVideo(url : URL)
}

class EditProductImagesCell : UITableViewCell {
    var delegate : EditProductImagesCellDelegate?
    @IBOutlet var viewContainerData: UIView!
    @IBOutlet var pageControl: UIPageControl!
    var model : GetPrdouctDetailsModel?
    var scrollImageContainer =  UIScrollView()
    var currentPage = 1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        pageControl.currentPage  = 0
        self.viewContainerData.layer.cornerRadius = 5.0
        self.viewContainerData.layer.masksToBounds = true
        self.viewContainerData.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewContainerData.layer.borderWidth = 1.0
        self.viewContainerData.backgroundColor = .white
    }
    
    func loadImage(imageURL: [PictureModelElement], videoURL : [VideoModel] , model : GetPrdouctDetailsModel?) {
        
        println_debug(self.viewContainerData.frame)
        for (_ , elementX) in scrollImageContainer.subviews.enumerated() {
            elementX.removeFromSuperview()
        }
        
        self.model = model
        pageControl.numberOfPages = (imageURL.count ) + (videoURL.count)
        scrollImageContainer.frame = CGRect(x: 0, y: 0, width: self.viewContainerData.frame.size.width, height: 215)
        scrollImageContainer.delegate = self
        scrollImageContainer.isPagingEnabled = true
        
        var xAxis : CGFloat = 0
        if imageURL.count > 0 {
            for (item, element) in imageURL.enumerated() {
                let viewImageContainer = UIView()
                let imageProduct = UIImageView()
                let btnDeleteProductImage = UIButton()
                println_debug(xAxis)
                viewImageContainer.frame = CGRect(x: xAxis, y: 0, width: self.self.viewContainerData.frame.size.width , height: 215)
                scrollImageContainer.addSubview(viewImageContainer)
                
                imageProduct.frame = CGRect(x: 30, y: 30, width: self.self.viewContainerData.frame.size.width - 60, height: 140)
                imageProduct.contentMode = .scaleAspectFit
                imageProduct.tag = item
                imageProduct.sd_setImage(with: URL(string: (element.pictureURL ?? "") as String))
                
                
                btnDeleteProductImage.frame = CGRect(x: self.viewContainerData.frame.size.width - 40, y: 5, width: 20, height: 20)
                btnDeleteProductImage.setImage(UIImage(named: "Delete"), for: .normal)
                btnDeleteProductImage.tag = item
                btnDeleteProductImage.addTarget(self, action: #selector(onClickDeleteAction(_:)), for: .touchUpInside)
                btnDeleteProductImage.bringSubviewToFront(viewImageContainer)
                viewImageContainer.addSubview(btnDeleteProductImage)
                viewImageContainer.addSubview(imageProduct)
                
                xAxis += self.viewContainerData.frame.size.width
            }
        }

        if videoURL.count > 0 {
            for (item, element) in videoURL.enumerated() {
                
                let viewImageContainer = UIView()
                viewImageContainer.frame = CGRect(x: xAxis, y: 0, width: self.viewContainerData.frame.size.width , height: 215)
                
                let videoProductView = UIView()
                videoProductView.frame = CGRect(x: 30, y: 30, width: self.viewContainerData.frame.size.width - 60, height: 140)
                videoProductView.tag = 100 + item
                videoProductView.backgroundColor = .darkGray
                let url = NSURL(fileURLWithPath: NSString(string: (element.videoURL ?? "") as String) as String)
                videoProductView.layer.setValue(url, forKey: "videoURL")
                
                let btnDeleteProductImage = UIButton()
                btnDeleteProductImage.frame = CGRect(x: self.viewContainerData.frame.size.width - 40, y: 5, width: 20, height: 20)
                btnDeleteProductImage.setImage(UIImage(named: "Delete"), for: .normal)
                btnDeleteProductImage.tag = 100 + item
                btnDeleteProductImage.bringSubviewToFront(viewImageContainer)
                btnDeleteProductImage.addTarget(self, action: #selector(onClickDeleteAction(_:)), for: .touchUpInside)
                viewImageContainer.addSubview(btnDeleteProductImage)
               
                let playButton = UIButton()
                playButton.frame = CGRect.init(x: (videoProductView.frame.size.width / 2  - 25) , y: (videoProductView.frame.size.height / 2  - 25), width: 50, height: 50)
                playButton.setImage(UIImage(named: "Play"), for: .normal)
                playButton.tag = 100 + item
                playButton.addTarget(self, action: #selector(onClickPlayVideoAction(_:)), for: .touchUpInside)
                videoProductView.addSubview(playButton)
                
                viewImageContainer.addSubview(videoProductView)
                scrollImageContainer.addSubview(viewImageContainer)
                
                xAxis += self.viewContainerData.frame.size.width
            }
        }
        
        scrollImageContainer.contentSize = CGSize(width: (self.viewContainerData.frame.size.width * CGFloat((imageURL.count ) + (videoURL.count))), height: 215)
        let tempFrame = CGRect.init(x: self.viewContainerData.frame.origin.x , y: self.viewContainerData.frame.origin.y , width: self.viewContainerData.frame.size.width, height: 158)
        self.viewContainerData.frame = tempFrame
        self.viewContainerData.addSubview(scrollImageContainer)

    }
    
    @objc func onClickDeleteAction(_ button: UIButton) {
        if let del = delegate {
            del.reloadProductDetailPage(id: button.tag)
        }
    }
    
    @objc func onClickPlayVideoAction(_ button: UIButton) {
        if let urlView = button.superview  {
            if let url = urlView.layer.value(forKey: "videoURL") as? URL{
                if let del = delegate {
                    del.palyVideo(url: url)
                }
            }
        }
    }
    
}

class DevliveryTimeAndClothingCell: UITableViewCell {
    @IBOutlet var viewContainerData: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnTitle: UIButton!
    @IBOutlet var imgRight: UIImageView!
    @IBOutlet var lblRight: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContainerData.layer.cornerRadius = 5.0
        self.viewContainerData.layer.masksToBounds = true
        self.viewContainerData.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewContainerData.layer.borderWidth = 1.0
        self.viewContainerData.backgroundColor = .white
    }
}

extension EditProductImagesCell: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }

}



class EditFieldsCell :
    UITableViewCell {
    @IBOutlet var topConstantTitle: NSLayoutConstraint!
    @IBOutlet var viewContainerData: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var tvValue: UITextView!
    @IBOutlet var btnOption:UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContainerData.layer.cornerRadius = 5.0
        self.viewContainerData.layer.masksToBounds = true
        self.viewContainerData.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewContainerData.layer.borderWidth = 1.0
        self.viewContainerData.backgroundColor = .white
    }
    
}
class EditFieldsCell2 : UITableViewCell {
    @IBOutlet var viewContainerData: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var tfValue: UITextField!
    @IBOutlet var btnOption:UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContainerData.layer.cornerRadius = 5.0
        self.viewContainerData.layer.masksToBounds = true
        self.viewContainerData.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewContainerData.layer.borderWidth = 1.0
        self.viewContainerData.backgroundColor = .white
        
        tfValue.attributedPlaceholder =
            NSAttributedString(string: "placeholder text", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(hex: "9D261D")])
    }
    
}



protocol PopoverViewControllerDelegate {
    func selectedDeliveryTime(id: String,name: String)
}
class PopoverViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var delegate : PopoverViewControllerDelegate?
    var tvDeliveryTime = UITableView()
    var model: DeliveryTimeModel?
    override func viewDidLoad() {
        self.view.backgroundColor = .white
        self.preferredContentSize = CGSize(width: 120, height: 250)
        tvDeliveryTime.frame = self.view.bounds
        self.view.addSubview(tvDeliveryTime)
        tvDeliveryTime.delegate = self
        tvDeliveryTime.dataSource = self
        tvDeliveryTime.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: "Cell")
        tvDeliveryTime.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        cell.selectionStyle = .none
        cell.textLabel?.text = model?.data?[indexPath.row].name
        cell.textLabel?.textAlignment = .left
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let del = delegate {
            del.selectedDeliveryTime(id: model?.data?[indexPath.row].deliveryDateID ?? "",name : model?.data?[indexPath.row].name ?? "")
        }
        self.dismiss(animated: true, completion: nil)
    }
}



