//
//  EditProductDelegate.swift
//  OSKVendor
//
//  Created by Sam' MacBook on 15/09/20.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit
import AVKit

extension EditProducts: UITextFieldDelegate,ProductBarScannerDelegate,GetAllCategoriesVCDelegate,EditProductImagesCellDelegate,EditProductAttributsDelegate,UIPopoverPresentationControllerDelegate,PopoverViewControllerDelegate,UITextViewDelegate {
    
    func palyVideo(url: URL) {
        let player = AVPlayer(url: url)
        let vc = AVPlayerViewController()
        vc.player = player
        self.present(vc, animated: true) { vc.player?.play() }
    }
    
//    // MARK: UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        if textView.tag == 10 {
            productName = textView.text ?? ""
            manageTextViewTitle(cell: getCellTextView(row: 1))
        }else if textView.tag == 11 {
            productNameUni = textView.text ?? ""
            manageTextViewTitle(cell: getCellTextView(row: 2))
        }else if textView.tag == 50 {
            productShortDescription = textView.text ?? ""
            manageTextViewTitle(cell: getCellTextView(row: 6))
        }else if textView.tag == 51 {
            productShortDescriptionUni = textView.text ?? ""
            manageTextViewTitle(cell: getCellTextView(row: 7))
        }else if textView.tag == 60 {
            productFullDescription = textView.text ?? ""
            manageTextViewTitle(cell: getCellTextView(row: 8))
        }else if textView.tag == 61 {
            productFullDescriptionUni = textView.text ?? ""
            manageTextViewTitle(cell: getCellTextView(row: 9))
        }
  
        let startHeight = textView.frame.size.height
        let calcHeight = textView.sizeThatFits(textView.frame.size).height
        if startHeight != calcHeight {
            UIView.setAnimationsEnabled(false)
            self.tvProduct.beginUpdates()
            self.tvProduct.endUpdates()
       }
    }
 
    func textViewDidBeginEditing(_ textView: UITextView) {
    }
    
    func setPlacehodlerText(textView: UITextView) -> String {
      var placeHodlerText = ""
        if textView.tag == 10 {
            placeHodlerText = "Product Name*".localized
        }else if textView.tag == 11 {
            placeHodlerText = "Product Name in unicode*".localized
        }else if textView.tag == 50 {
            placeHodlerText = "Description*".localized
        }else if textView.tag == 51 {
            placeHodlerText = "Description in unicode*".localized
        }else if textView.tag == 60 {
            placeHodlerText = "Full Description".localized
        }else if textView.tag == 61 {
            placeHodlerText = "Full Description in unicode".localized
        }
        return placeHodlerText
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {

    }
    
    @objc func didEditChanged(_ textField: UITextField) {
        if textField.tag == 30 {
            productPrice = textField.text ?? ""
            if productPrice.count > 9 {
                productPrice.removeLast()
            }
            textField.text = getDigitDisplay(productPrice)
            manageTextFieldTitle(cell: getCellTextField(row: 3))
        }else if textField.tag == 20 {
            productBarCode = textField.text ?? ""
            productDetailsModel?.data?.barcode = productBarCode
            manageTextFieldTitle(cell: getCellTextField(row: 10))
        }
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if range.location == 0 && text == " " {
            return false
        }
        if textView.tag == 10 || textView.tag == 50 || textView.tag == 60 {
            if !(text == text.components(separatedBy: NSCharacterSet(charactersIn: NAME_CHAR_SET_En).inverted).joined(separator: "")) { return false }
        }else if textView.tag == 11 || textView.tag == 51 || textView.tag == 61 {
            if !(text == text.components(separatedBy: NSCharacterSet(charactersIn: NAME_CHAR_SET_Uni).inverted).joined(separator: "")) { return false }
        }
        
        return true

    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 30 {
            textField.keyboardType = .numberPad
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
         if textField.tag == 30 {
            productPrice = textField.text ?? ""
            productDetailsModel?.data?.productPrice?.price = productPrice
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let _ = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
         if textField.tag == 30  {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")) { return false }
            return true
        }
        return true
    }
    
    func manageTextViewTitle(cell: EditFieldsCell) {
        if cell.tvValue.text?.count == 0 {
            cell.topConstantTitle.constant = 30
            cell.tvValue.tintColor = .clear
        }else {
            cell.topConstantTitle.constant = 0
            cell.tvValue.tintColor = .blue
        }
    }
    
    func manageTextFieldTitle(cell: EditFieldsCell2) {
        if cell.tfValue.text?.count == 0 {
            cell.lblTitle.isHidden = true
        }else {
            cell.lblTitle.isHidden = false
        }
    }
    
    func returnBarCode(barCade: String) {
        productDetailsModel?.data?.barcode = barCade
        let cell = getCellTextField(row: 10)
        cell.tfValue.text = barCade
        manageTextFieldTitle(cell: cell)
    }
    
    func selectedCategory(category: AllCategoriesData, viewController: UIViewController) {
        if let cell = tvProduct.cellForRow(at: IndexPath(row: 4, section: 0)) as? DevliveryTimeAndClothingCell {
            cell.btnTitle.setTitle(category.name, for: .normal)
            productClothing = category.name ?? ""
        }
        viewController.view.removeFromSuperview()
    }
    
    func reloadProductDetailPage(id: Int) {
        if id < 100 {
            var imageArray = self.productDetailsModel?.data?.pictureModels
            self.deleteCategoryIamge(pictureID: String(imageArray![id].id ?? 0), handler: { isSuccess in
                if isSuccess {
                    imageArray?.remove(at: id)
                    self.productDetailsModel?.data?.pictureModels = imageArray!
                    self.tvProduct.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                    }else {
                }
            })
        }else {
            var imageArray = self.productDetailsModel?.data?.videoModels
            self.deleteCategoryVideo(pictureID: String(imageArray![id - 100].id ?? 0), handler: { isSuccess in
                if isSuccess {
                    imageArray?.remove(at: id - 100)
                    self.productDetailsModel?.data?.videoModels = imageArray!
                    self.tvProduct.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                    }else {
                }
            })
        }
   
    }
    
    func reloadImageProductCell() {
        DispatchQueue.main.async {
            if let proID = self.productID, !proID.isEmpty {
                self.getProductDetialsByID(productID: proID, reloadFor: "Image")
            }
        }
    }
    
    func reloadAPIForGetNewColor() {
        if let proID = self.productID, !proID.isEmpty {
            self.getProductDetialsByID(productID: proID, reloadFor: "ColorAttribute")
        }
    }
    
    
    
    public func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
           return .none
       }
    
    func selectedDeliveryTime(id: String,name: String) {
        println_debug("ID : \(id) name : \(name)")
        deliveryDateId.0 = id
        deliveryDateId.1 = name
        tvProduct.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .automatic)
    }
    
    private func getDigitDisplay(_ digit: String) -> String {
          var FormateStr: String
          let mystring = digit.replacingOccurrences(of: ",", with: "")
          let number = NSDecimalNumber(string: mystring)
          
          let formatter = NumberFormatter()
          formatter.groupingSeparator = ","
          formatter.groupingSize = 3
          formatter.usesGroupingSeparator = true
          FormateStr = formatter.string(from: number)!
          if FormateStr == "NaN"{
              FormateStr = ""
          }
          
          return FormateStr
      }
    
}




extension EditProducts {
    
    
    func uploadPhotos() {
        let memeType = "image/jpeg"
        self.imageUpload(productID: productID ?? "", imgName: imageName[uploadedImagesCount - 1], type: memeType, fileData: imageData[uploadedImagesCount - 1], handler: { isSuccess in
            if isSuccess {
                self.uploadedImagesCount = self.uploadedImagesCount - 1
                if self.uploadedImagesCount == 0 {
                    self.imageName.removeAll()
                    self.imageData.removeAll()
                    self.imageAndVideoArray.removeAll()
                    self.reloadImageProductCell()
                }else {
                    self.uploadPhotos()
                }
            }
        })
    }
    
    func uploadVideos() {
        self.videoUpload(productID: self.productID ?? "", videoName: self.videoName[0], fileData: self.videoData[0], handler: {(isSuccess) in
            if isSuccess {
                    self.videoName.removeAll()
                    self.videoData.removeAll()
                    self.imageAndVideoArray.removeAll()
                    self.reloadImageProductCell()
            }
        })
    }
        
    func imageUpload(productID: String, imgName: String,type: String ,fileData: Data,handler: @escaping (_ isSuccess: Bool) -> Void) {
        
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlString = String(format: "%@/vendor/addproductpicture", APIManagerClient.sharedInstance.base_url) as String
            println_debug(urlString)
            let params = ["ProductId": productID.trimmingCharacters(in: .whitespaces),"DisplayOrder": "1","OverrideAltAttribute": "test","OverrideTitleAttribute": "test"]
            println_debug(urlString)
            let request = NSMutableURLRequest(url:URL(string: urlString)!)
            request.httpMethod = "POST";
            let boundary = "Boundary-\(UUID2)"
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.httpBody = createBodyWithParameters(parameters: params, KeyName: "File", type: type ,filePathKey: imgName, imageDataKey: fileData as Data, boundary: boundary) as Data
            request.setValue(Vendor_loginmodel.shared.token, forHTTPHeaderField: "Token")
            request.setValue(aPIManager.nstKEY, forHTTPHeaderField: "NST")
            request.setValue(aPIManager.deviceId, forHTTPHeaderField: "DeviceId")
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                (data, response, error) in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    if error != nil {
                        handler(false)
                        return
                    }
                    if let jsonData = data {
                        println_debug(String(data: jsonData, encoding: .utf8))
                    }
                  
                    println_debug(response)
                    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                       handler(true)
                    }else {
                        handler(false)
                    }
                }
            }
            task.resume()
        }
    }
    
    func videoUpload(productID: String, videoName : String, fileData: Data,handler: @escaping (_ isSuccess: Bool) -> Void) {
        
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            DispatchQueue.main.async {
                AppUtility.showLoading(self.view)
                
               
                
                
                let urlString = String(format: "%@/vendor/addproductvideo", APIManagerClient.sharedInstance.base_url) as String
                println_debug(urlString)
                let params = ["ProductId": productID.trimmingCharacters(in: .whitespaces),"DisplayOrder": "1","OverrideAltAttribute": "test","OverrideTitleAttribute": "test"]
                let request = NSMutableURLRequest(url:URL(string: urlString)!)
                request.httpMethod = "POST";
                let boundary = "Boundary-\(UUID2)"
                request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                
                request.httpBody = self.createBodyWithParametersForVideo(parameters: params, filePathKey: videoName, imageDataKey: fileData as Data, boundary: boundary) as Data
                
                request.setValue(Vendor_loginmodel.shared.token, forHTTPHeaderField: "Token")
                request.setValue(self.aPIManager.nstKEY, forHTTPHeaderField: "NST")
                request.setValue(self.aPIManager.deviceId, forHTTPHeaderField: "DeviceId")
                let task = URLSession.shared.dataTask(with: request as URLRequest) {
                    (data, response, error) in
                    DispatchQueue.main.async {
                        if let viewLoc = self.view {
                            AppUtility.hideLoading(viewLoc)
                        }
                        do {
                            let responseX =  try JSONSerialization.jsonObject(with: data ?? Data(), options: []) as? [String: Any]
                            println_debug(responseX)
                        
                        } catch { print(error.localizedDescription)}
                        if error != nil {
                            handler(false)
                            return
                        }
                        println_debug(response)
                        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                           handler(true)
                        }else {
                            handler(false)
                        }
                    }
                }
                task.resume()
            }
        }
    }
    
    
    func createBodyWithParameters(parameters: [String: String]?, KeyName: String,type: String ,filePathKey: String?, imageDataKey: Data, boundary: String) -> NSData {
        var body = Data()
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(Data("--\(boundary)\r\n".utf8))
                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data("\(value)\r\n".utf8))
            }
        }
        var filename = ""
        if filePathKey != "" {
            let fileArray = filePathKey?.components(separatedBy: "/")
            filename = (fileArray?.last)!
        }
        let mimetype = type
        body.append(Data("--\(boundary)\r\n".utf8))
        body.append(Data("Content-Disposition: form-data; name=\"\(KeyName)\"; filename=\"\(filename)\"\r\n".utf8))
        body.append(Data("Content-Type: \(mimetype)\r\n\r\n".utf8))
        body.append(imageDataKey as Data)
        body.append(Data("\r\n".utf8))
        body.append(Data("--\(boundary)--\r\n".utf8))
        return body as NSData
    }
    
    func createBodyWithParametersForVideo(parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> NSData {
        var body = Data()
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(Data("--\(boundary)\r\n".utf8))
                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data("\(value)\r\n".utf8))
            }
        }
        var filename = ""
        if filePathKey != "" {
            let fileArray = filePathKey?.components(separatedBy: "/")
            filename = (fileArray?.last)!
        }
        let KeyName = "File"
        let mimetype = "video/mp4"
        body.append(Data("--\(boundary)\r\n".utf8))
        body.append(Data("Content-Disposition: form-data; name=\"\(KeyName)\"; filename=\"\(filename)\"\r\n".utf8))
        body.append(Data("Content-Type: \(mimetype)\r\n\r\n".utf8))
        body.append(imageDataKey as Data)
        body.append(Data("\r\n".utf8))
        body.append(Data("--\(boundary)--\r\n".utf8))
        return body as NSData
     }

    
    
}


extension UITextView {
    func adjustUITextViewHeight() {
        self.translatesAutoresizingMaskIntoConstraints = true
        self.sizeToFit()
        self.isScrollEnabled = false
    }
}
