//
//  EditProducts.swift
//  OSKVendor
//
//  Created by Sam' MacBook on 14/09/20.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit

import Photos
import AVKit
import DKImagePickerController

class EditProducts: OSKBaseViewController {
    
    @IBOutlet var tvProduct: UITableView!
    @IBOutlet var btnAddAttributs: UIButton!{
        didSet{
          btnAddAttributs.setTitle("More Details".localized, for: .normal)
        }
    }
    @IBOutlet var btnUdate: UIButton!{
        didSet{
            btnUdate.setTitle("Update".localized, for: .normal)
        }
    }
    @IBOutlet var btnPublish: UIButton!{
        didSet{
           btnPublish.setTitle("Show".localized, for: .normal)
        }
    }
    
    var comingFrom: String?
    var alertVC : SAlertController?
    let aPIManager = APIManager()
    var apiManagerClient : APIManagerClient?
    var productDetailsModel: GetPrdouctDetailsModel?
    var deliverytimeModel : DeliveryTimeModel?
    var allCategoryModel : GetAllCategoriesModel?
    var categoryLastName = ""
    var productID : String?
    var deliveryDateId = ("1","")
    var productName = ""
    var productNameUni = ""
    var productPrice = ""
    var productShortDescription = ""
    var productShortDescriptionUni = ""
    var productFullDescription = ""
    var productFullDescriptionUni = ""
    var productBarCode = ""
    var productClothing = ""
    @IBOutlet var stackOption: UIStackView!
    var numberOfRow = 0
    @IBOutlet var lblLineSeparator: UILabel!
    var imageData = [Data]()
    var imageName = [String]()
    var videoData = [Data]()
    var videoName = [String]()
    var imageAndVideoArray : [DKAsset] = [DKAsset]()
    var imageUploadCount = 0
    var uploadedImagesCount = 0
    var viewHeight : CGFloat = 80.0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Edit Details".localized
        self.lblLineSeparator.isHidden = true
        self.stackOption.isHidden = true
        
        btnPublish.layer.cornerRadius = 7.0
        btnAddAttributs.layer.cornerRadius = 7.0
        btnUdate.layer.cornerRadius = 7.0
        
        if let proID = productID, !proID.isEmpty {
            self.getProductDetialsByID(productID: proID, reloadFor: "")
            self.getAllCategories()
        }
      
    }

    
    @IBAction func onClickPublishAction(_: UIButton) {
        self.publishAndUnPublishWebCall(productID: self.productID ?? "")
    }
   
    @IBAction func onClickBackAction(_: UIButton) {
        if comingFrom == "AddProduct" {
            self.navigationController?.popToRootViewController(animated: true)
        }else {
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewDidLayoutSubviews() {
        tvProduct.reloadData()
    }
    
    @IBAction func onClickAddAttributsAction(_: UIButton) {
        var path = ""
        if let model = allCategoryModel?.data {
            for item in model {
                if item.name == categoryLastName {
                    path = item.iconPath ?? ""
                }
            }
        }
        
        
        let sb = UIStoryboard(name: "EditProduct", bundle: nil)
            let vcAttributs = sb.instantiateViewController(withIdentifier: "EditProductAttributs")  as! EditProductAttributs
            vcAttributs.view.frame = CGRect.init(x: 0, y: 0, width:self.view.frame.width, height: self.view.frame.height)
            vcAttributs.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
            addChild(vcAttributs)
         //   vcAttributs.model = productDetailsModel
            vcAttributs.iconImageURL = path
            vcAttributs.didMove(toParent: self)
            vcAttributs.delegate = self
            self.view.addSubview((vcAttributs.view)!)
    }
    
    @IBAction func onClickUpdateAction(_: UIButton) {
        guard productID != nil else {
            return
        }
        
        if productDetailsModel?.data?.pictureModels?.count == 0 {
            self.showAlertInClass(title: "Error".localized, subTitle: "Please add at least one image".localized)
            return
        }
        
        if productName.isEmpty {
 
            self.showAlertInClass(title: "Error".localized, subTitle: "Please fill product name".localized)
           return
        }
        
        if productNameUni.isEmpty {
            self.showAlertInClass(title: "Error".localized, subTitle: "Please fill product unicode name".localized)
            return
        }
        
        if productPrice.isEmpty {
            self.showAlertInClass(title: "Error".localized, subTitle: "Please fill product price".localized)
            return
        }else {
            productPrice = productPrice.replacingOccurrences(of: ",", with: "")
        }
        
        if productShortDescription.isEmpty {
            self.showAlertInClass(title: "Error".localized, subTitle: "Please fill description".localized)
            return
        }
        
        if productShortDescriptionUni.isEmpty {
            self.showAlertInClass(title: "Error".localized, subTitle: "Please fill unicode description".localized)
            return
        }
        
        if deliveryDateId.1 == "" {
            deliveryDateId.0 = "1"
        }
        
        let uniCodeParams = ["Name": productNameUni, "ShortDescription": productShortDescriptionUni, "FullDescription": productFullDescriptionUni]
        
        let jsonData = try! JSONSerialization.data(withJSONObject: uniCodeParams, options: [])
        let jsonString = String(data: jsonData, encoding: .utf8)!
        
        let params = ["Id": productID ?? "", "DeliveryDateId": deliveryDateId.0, "Name": productName, "Price": productPrice, "ShortDescription":productShortDescription,"FullDescription": productFullDescription, "OtherLanguageDataUnicode": jsonString] as [String:Any]
        
        self.updateProductDetails(params: params, handler: { isSuccess in
            if isSuccess {
                println_debug("Product updated")
                self.alertVC = SAlertController()
                self.alertVC?.ShowSAlert(title: "", withDescription: "Product successfully edited", onController: self)
                let YesAction = SAlertAction()
                YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                self.navigationController?.popToRootViewController(animated: true)
                })
                self.alertVC?.addAction(action: [YesAction])
            }else {
                println_debug("Product updated failed")
                self.showAlertInClass(title: "Error".localized, subTitle: "Product editing is failed".localized)
            }
        })
    }
    
    @objc func onClickCameraAction(_ button: UIButton) {
        self.view.endEditing(true)
        if button.tag == 10 {
            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

            let firstAction: UIAlertAction = UIAlertAction(title: "Photo".localized, style: .default) { action -> Void in
                DispatchQueue.main.async {
                    let pickerController = DKImagePickerController()
                    let photoVideoCount = (self.productDetailsModel?.data?.pictureModels?.count ?? 0) + (self.productDetailsModel?.data?.videoModels?.count ?? 0)
                    self.imageUploadCount = 5 - ((self.productDetailsModel?.data?.pictureModels?.count ?? 0) + (self.productDetailsModel?.data?.videoModels?.count ?? 0))
                    if  photoVideoCount >= 5 {
                        self.alertVC = SAlertController()
                        self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Please remove at least one image or video", onController: self)
                        let YesAction = SAlertAction()
                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        })
                        self.alertVC?.addAction(action: [YesAction])
                       return
                    }
                    pickerController.maxSelectableCount = self.imageUploadCount
                    pickerController.assetType = .allPhotos
                    DKImageExtensionController.registerExtension(extensionClass: CustomCameraExtensionA.self, for: .camera)
                    pickerController.didSelectAssets = { (assets: [DKAsset]) in
                    self.imageAndVideoArray.append(contentsOf: assets)
                    DispatchQueue.main.async {
                             self.loadImageAndVideoData()
                        }
                    }
                    self.present(pickerController, animated: true) {}
                }
            }
            
            let secondAction: UIAlertAction = UIAlertAction(title: "Video", style: .default) { action -> Void in
             let pickerController = DKImagePickerController()
             pickerController.allowMultipleTypes = true
                let photoVideoCount = (self.productDetailsModel?.data?.pictureModels?.count ?? 0) + (self.productDetailsModel?.data?.videoModels?.count ?? 0)
                
                if photoVideoCount < 5 {
                    if (self.productDetailsModel?.data?.videoModels?.count ?? 0) == 0  {
                        pickerController.maxSelectableCount = 1
                    }else {
                        self.alertVC = SAlertController()
                        self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Please remove at least one image or video", onController: self)
                        let YesAction = SAlertAction()
                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        })
                        self.alertVC?.addAction(action: [YesAction])
                       return
                    }
                }else {
                    if photoVideoCount >= 5  {
                        self.alertVC = SAlertController()
                        self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Please remove at least one image or video", onController: self)
                        let YesAction = SAlertAction()
                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        })
                        self.alertVC?.addAction(action: [YesAction])
                       return
                    }else {
                        pickerController.maxSelectableCount = 1
                    }
                }
             pickerController.assetType = .allVideos
             DKImageExtensionController.registerExtension(extensionClass: CustomCameraExtensionB.self, for: .camera)
             pickerController.didSelectAssets = { (assets: [DKAsset]) in
                 print(assets)
                 self.imageAndVideoArray.append(contentsOf: assets)
                 DispatchQueue.main.async {
                     self.loadImageAndVideoData()

                 }
             }
             self.present(pickerController, animated: true) {}
            }
            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
            actionSheetController.addAction(firstAction)
            actionSheetController.addAction(secondAction)
            actionSheetController.addAction(cancelAction)
            
            actionSheetController.popoverPresentationController?.sourceView = self.view
            present(actionSheetController, animated: true) {
                print("option menu presented")
            }
            
        }else if button.tag == 20 {
            DispatchQueue.main.async {
                if #available(iOS 13.0, *) {
                    let vc = self.storyboard?.instantiateViewController(identifier: "ProductBarScanner") as! ProductBarScanner
                    vc.delegate = self
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    // Fallback on earlier versions
                }
            }
        }else if button.tag == 41 {
                    let popoverVC = PopoverViewController()
                    popoverVC.modalPresentationStyle = .popover
                    popoverVC.popoverPresentationController?.sourceView = button
                    popoverVC.popoverPresentationController?.permittedArrowDirections = .left
                    popoverVC.popoverPresentationController?.delegate = self
                    popoverVC.delegate = self
                    popoverVC.model = self.deliverytimeModel
                    self.present(popoverVC, animated: true, completion: nil)
          
        }else {
            let sb = UIStoryboard(name: "EditProduct", bundle: nil)
            let allCategoryVC = sb.instantiateViewController(withIdentifier: "GetAllCategoriesVC")  as! GetAllCategoriesVC
            allCategoryVC.view.frame = CGRect.init(x: 10, y: 64, width:self.view.frame.width - 20, height: self.view.frame.height - 64)
            addChild(allCategoryVC)
            allCategoryVC.delegate = self
            allCategoryVC.didMove(toParent: self)
            self.view.addSubview((allCategoryVC.view)!)
        }
    }
    
    func getCellTextView(row: Int) -> EditFieldsCell {
        return tvProduct.cellForRow(at: IndexPath(row: row, section: 0)) as! EditFieldsCell
    }
    
    func getCellTextField(row: Int) -> EditFieldsCell2 {
        return tvProduct.cellForRow(at: IndexPath(row: row, section: 0)) as! EditFieldsCell2
    }
    
    
    
    func loadImageAndVideoData () {
        
        imageData.removeAll()
        imageName.removeAll()
        videoData.removeAll()
        videoName.removeAll()
        if imageAndVideoArray.count == 0 {
            return
        }
        
        for (_ ,object )in self.imageAndVideoArray.enumerated() {
            if object.type == .photo {
                let assetImage : PHAsset = object.originalAsset ?? PHAsset()
                var img: UIImage?
                let manager = PHImageManager.default()
                let options = PHImageRequestOptions()
                options.version = .original
                options.isSynchronous = true
                manager.requestImageData(for: assetImage, options: options) { data, _, _, _ in
                    if let data = data {
                        img = UIImage(data: data)
                        let data:Data = img?.jpegData(compressionQuality: 0.5) ?? Data()
                        self.imageData.append(data)
                        let name = String(Date().toMillis())
                        self.imageName.append("\(name).jpeg")
                    }
                }
                self.uploadedImagesCount = self.imageData.count
                if self.imageData.count == imageAndVideoArray.count {
                    self.imageAndVideoArray.removeAll()
                    self.uploadPhotos()
                }
            }else if object.type == .video {
                if object.duration > 15 {
                    self.alertVC = SAlertController()
                    self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Selected video time is more than 15 seconds. Please select another", onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    })
                    self.alertVC?.addAction(action: [YesAction])
                    return
                }
                guard let assetData = object.originalAsset else { return }
                self.convertVideo(phAsset: assetData, handler:{(name,data) in
                     self.videoName.append(name)
                     self.videoData.append(data)
                    if self.videoData.count == self.imageAndVideoArray.count {
                        self.imageAndVideoArray.removeAll()
                        self.uploadVideos()
                    }
                })
            }
        }
        println_debug("Photo Count\(imageData)")
        
    }
    
    
    
    func convertVideo(phAsset : PHAsset,handler: @escaping (_ name: String,_ imageData: Data) -> Void) {
        PHImageManager.default().requestAVAsset(forVideo: phAsset, options: PHVideoRequestOptions(), resultHandler: { (asset, audioMix, info) -> Void in
            if let asset = asset as? AVURLAsset {
                do {
                    let videoData = try  Data.init(contentsOf: asset.url)
                    print(asset.url)
                    print("File size before compression: \(Double(videoData.count / 1048576)) mb")
                    let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".MP4")
                    print(compressedURL)
                    self.compressVideo(inputURL: asset.url , outputURL: compressedURL) { (exportSession) in
                        guard let session = exportSession else {
                            return
                        }
                        switch session.status {
                        case .unknown:
                            print("unknown")
                            break
                        case .waiting:
                            print("waiting")
                            break
                        case .exporting:
                            print("exporting")
                            break
                        case .completed:
                            do {
                                let compressedData = try  Data.init(contentsOf: compressedURL)
                                let name = String(Date().toMillis())
                                handler("\(name).mp4", compressedData as Data)
                            }
                            catch{
                                print(error)
                            }
                            
                        case .failed:
                            print("failed")
                            break
                        case .cancelled:
                            print("cancelled")
                            break
                        @unknown default:
                            break
                        }
                    }
                } catch {
                    print(error)
                    //return
                }
            }
        })
        
        
    }
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            return
        }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    

}

extension EditProducts: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRow
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProductImagesCell", for: indexPath) as! EditProductImagesCell
            cell.selectionStyle = .none
            let imageURL = productDetailsModel?.data?.pictureModels
            let videoURL = productDetailsModel?.data?.videoModels
            cell.delegate = self
            if let imgURl = imageURL {
                cell.loadImage(imageURL: imgURl , videoURL: videoURL ?? [], model: productDetailsModel)
            }
           return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditFieldsCell", for: indexPath) as! EditFieldsCell
            cell.selectionStyle = .none
            cell.lblTitle.text = "Product Name*".localized
           // cell.btnOption.setImage(#imageLiteral(resourceName: "Cam"), for: .normal)
            cell.tvValue.text = productName
            cell.tvValue.delegate = self
            cell.tvValue.tag = 10
            cell.btnOption.tag = 10
            cell.tvValue.keyboardType = .default
            cell.btnOption.isHidden = false
            cell.btnOption.addTarget(self, action: #selector(onClickCameraAction(_:)), for: .touchUpInside)
            self.manageTextViewTitle(cell: cell)
            return cell
        }else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditFieldsCell", for: indexPath) as! EditFieldsCell
            cell.selectionStyle = .none
            cell.lblTitle.text = "Product Name in unicode*".localized
           // cell.btnOption.setImage(#imageLiteral(resourceName: "Cam"), for: .normal)
            cell.tvValue.text = productNameUni
            cell.tvValue.delegate = self
            cell.tvValue.tag = 11
            cell.btnOption.isHidden = true
            cell.tvValue.keyboardType = .default
            self.manageTextViewTitle(cell: cell)
            return cell
        }else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditFieldsCell2", for: indexPath) as! EditFieldsCell2
            cell.selectionStyle = .none
            cell.lblTitle.text = "Product Price*".localized
            cell.tfValue.placeholder = "Product Price*".localized
            cell.tfValue.text = productPrice
            cell.btnOption.isHidden = true
            cell.tfValue.delegate = self
            cell.tfValue.tag = 30
            cell.tfValue.keyboardType = .numberPad
            cell.tfValue.addTarget(self, action: #selector(didEditChanged(_:)), for: .editingChanged)
            self.manageTextFieldTitle(cell: cell)
            return cell
        }else if indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditFieldsCell2", for: indexPath) as! EditFieldsCell2
            cell.selectionStyle = .none
            cell.lblTitle.text = "Select Category*".localized
           // cell.btnOption.setImage(#imageLiteral(resourceName: "Cam"), for: .normal)
            cell.tfValue.text = productClothing
            cell.tfValue.delegate = self
            cell.tfValue.tag = 12
            cell.btnOption.isHidden = true
            cell.tfValue.isUserInteractionEnabled = false
            self.manageTextFieldTitle(cell: cell)
            return cell
        }else if indexPath.row == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DevliveryTimeAndClothingCell", for: indexPath) as! DevliveryTimeAndClothingCell
            cell.selectionStyle = .none
            
            cell.btnTitle.setTitle("Delivery Time".localized, for: .normal)
            cell.lblRight.text = deliveryDateId.1
            if deliveryDateId.1 != "" {
                cell.lblRight.isHidden = false
            }else {
                cell.lblRight.isHidden = true
            }
            cell.imgRight.isHidden = false
            cell.imgRight.image = #imageLiteral(resourceName: "downarrow")
            cell.btnTitle.tag = 41
            cell.lblTitle.isHidden = true
            cell.btnTitle.addTarget(self, action: #selector(onClickCameraAction(_:)), for: .touchUpInside)
            return cell
        }else if indexPath.row == 6 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditFieldsCell", for: indexPath) as! EditFieldsCell
            cell.selectionStyle = .none
            cell.lblTitle.text = "Description*".localized
            cell.tvValue.text = productShortDescription
            cell.btnOption.isHidden = true
            cell.tvValue.delegate = self
            cell.tvValue.tag = 50
            cell.tvValue.keyboardType = .default
            return cell
        }else if indexPath.row == 7 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditFieldsCell", for: indexPath) as! EditFieldsCell
            cell.selectionStyle = .none
            cell.lblTitle.text = "Description in unicode*".localized
            cell.tvValue.text = productShortDescriptionUni
            cell.btnOption.isHidden = true
            cell.tvValue.delegate = self
            cell.tvValue.tag = 51
            cell.tvValue.keyboardType = .default
            return cell
        }else if indexPath.row == 8 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditFieldsCell", for: indexPath) as! EditFieldsCell
            cell.selectionStyle = .none
            cell.lblTitle.text = "Full Description".localized
           // cell.tvValue.placeholder = "Full Description".localized
            cell.tvValue.text = productFullDescription
            cell.btnOption.isHidden = true
            cell.tvValue.delegate = self
            cell.tvValue.tag = 60
            cell.tvValue.keyboardType = .default
            self.manageTextViewTitle(cell: cell)
            return cell
        }else if indexPath.row == 9 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditFieldsCell", for: indexPath) as! EditFieldsCell
            cell.selectionStyle = .none
            cell.lblTitle.text = "Full Description in unicode".localized
            cell.tvValue.text = productFullDescriptionUni
            cell.btnOption.isHidden = true
            cell.tvValue.delegate = self
            cell.tvValue.tag = 61
            cell.tvValue.keyboardType = .default
            self.manageTextViewTitle(cell: cell)
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditFieldsCell2", for: indexPath) as! EditFieldsCell2
            cell.selectionStyle = .none
            cell.lblTitle.text = "Product Bar Code".localized
            cell.tfValue.placeholder = "Enter Bar Code".localized
            cell.btnOption.setImage(#imageLiteral(resourceName: "barcode"), for: .normal)
            cell.tfValue.text = productBarCode
            cell.tfValue.delegate = self
            cell.tfValue.tag = 20
            cell.btnOption.tag = 20
            cell.tfValue.keyboardType = .default
            cell.btnOption.isHidden = false
            cell.tfValue.addTarget(self, action: #selector(didEditChanged(_:)), for: .editingChanged)
            cell.btnOption.addTarget(self, action: #selector(onClickCameraAction(_:)), for: .touchUpInside)
             self.manageTextFieldTitle(cell: cell)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            if self.productDetailsModel?.data?.pictureModels?.count ?? 0 > 0 || ((self.productDetailsModel?.data?.videoModels?.count ?? 0) > 0) {
                return 220
            }else {
                return 0
            }
        }else {
            if indexPath.row == 1 || indexPath.row == 2 {
                return UITableView.automaticDimension
            }else {
                return 80
            }
       
        }
    }
}
