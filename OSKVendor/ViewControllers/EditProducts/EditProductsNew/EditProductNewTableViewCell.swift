//
//  EditProductNewTableViewCell.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 04/11/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

protocol EditProductTitleLabelDel {
    func manageHeight(index: Int,hasText: Bool)
}

protocol callCameraEdit {
    func callCameraEdit()
}

class EditProductNewTableViewCell: UITableViewCell {
    
    @IBOutlet var addProductTextView: UITextView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var titleLabelHeight: NSLayoutConstraint!
    @IBOutlet var addProductTextVieHeight: NSLayoutConstraint!
    @IBOutlet var camBtn: UIButton!
    @IBOutlet var imgCell: UIImageView!
    
    var titleDelegate: AddProductTitleLabelDel?
    var cameraDelegate : CallCamera?
    var productParamDel: AddProductParamProtocol?
   
    override func awakeFromNib() {
        super.awakeFromNib()
       // addProductTextView.isUserInteractionEnabled = false
        addProductTextView.delegate = self
        //addProductTextView.text = "Product Name".localized
       // addProductTextView.delegate = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onClickCam(_ sender: UIButton) {
        cameraDelegate?.callCamera()
    }
    
    
    func setDataOnCell(tag: Int,titleValue: String){
        let value = titleValue
        if tag == 1{
            if value != ""{
               
               addProductTextView.text = value
               titleLabelHeight.constant = 18.0
               titleLabel.isHidden = false
               titleLabel.text = "Product Name".localized
            }else{
               addProductTextView.text = "Product Name".localized
               titleLabelHeight.constant = 0.0
               titleLabel.isHidden = true
               titleLabel.text = ""
            }
            
            camBtn.setImage(UIImage(named: "cam"), for: .normal)
            camBtn.isHidden = false
            
            
        }else if tag == 2{
            
            if value != ""{
                addProductTextView.text = value
                titleLabelHeight.constant = 18.0
                titleLabel.isHidden = false
                titleLabel.text = "Product Name In Unicode*".localized
            }else{
                addProductTextView.text = "Product Name In Unicode*".localized
                titleLabelHeight.constant = 0.0
                titleLabel.isHidden = true
                titleLabel.text = ""
            }
            
            camBtn.isHidden = true
            
        }else if tag == 5{
            if value != ""{
                self.addProductTextView.font = UIFont(name: appFont, size: 13.0)
                addProductTextView.text = value
                titleLabelHeight.constant = 18.0
                titleLabel.isHidden = false
                titleLabel.text = "Select Category *".localized
            }else{
                addProductTextView.text = "Select Category *".localized
                titleLabelHeight.constant = 0.0
                titleLabel.isHidden = true
                titleLabel.text = ""
            }
          imgCell.image = UIImage(named: "interface")
          camBtn.isHidden = true
            
        }
        else if tag == 6 {
            if value != ""{
                addProductTextView.text = value
                titleLabelHeight.constant = 18.0
                titleLabel.isHidden = false
                titleLabel.text = "Description".localized
            }else{
                addProductTextView.text = "Description".localized
                titleLabelHeight.constant = 0.0
                titleLabel.isHidden = true
                titleLabel.text = ""
            }
          imgCell.image = UIImage(named: "product-description")
          camBtn.isHidden = true
            
        }else{
            if value != ""{
                addProductTextView.text = value
                titleLabelHeight.constant = 18.0
                titleLabel.isHidden = false
                titleLabel.text = "Description in Unicode".localized
            }else{
                addProductTextView.text = "Description in Unicode".localized
                titleLabelHeight.constant = 0.0
                titleLabel.isHidden = true
                titleLabel.text = ""
            }
          imgCell.image = UIImage(named: "product-description")
          camBtn.isHidden = true
            
        }
    }
}

extension EditProductNewTableViewCell: UITextViewDelegate{
    
    internal func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let updatedString = (textView.text as NSString?)?.replacingCharacters(in: range, with: text)
        if (updatedString?.count ?? 0) > 40 {
            return false
        } else {
            if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), lang == "en"{
                let  char = text.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if isBackSpace != -92 {
                    let strTxt : Character = Character(text)
                    if textView.text?.last == strTxt {
                        if textView.text?.count ?? 0 >= 2 {
                            if let index1 = textView.text?.index((textView.text?.startIndex)!, offsetBy: range.location - 2)
                            {//will call succ 2 times
                                if let lastChar: Character = (textView.text?[index1]) //now we can index!
                                {
                                    if lastChar == strTxt
                                    {
                                        return false
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.tag == 1{
            if textView.text == "Product Name".localized{
                textView.text = ""
            }
            titleLabelHeight.constant = 18.0
            titleLabel.isHidden = false
            titleLabel.text = "Product Name".localized
            imgCell.image = UIImage(named: "product_name")
        }else if textView.tag == 2{
            if textView.text == "Product Name In Unicode *".localized{
                textView.text = ""
            }
            titleLabelHeight.constant = 18.0
            titleLabel.isHidden = false
            titleLabel.text = "Product Name In Unicode *".localized
            imgCell.image = UIImage(named: "product_name")
        }else if textView.tag == 4{
            if textView.text == "Select Category *".localized{
                textView.text = ""
            }
            titleLabelHeight.constant = 18.0
            titleLabel.isHidden = false
            titleLabel.text = "Select Category *".localized
            imgCell.image = UIImage(named: "")
        }else if textView.tag == 6{
            if textView.text == "Description".localized{
                textView.text = ""
            }
            titleLabelHeight.constant = 18.0
            titleLabel.isHidden = false
            titleLabel.text = "product-description".localized
            imgCell.image = UIImage(named: "description")
        }else if textView.tag == 7{
            if textView.text == "Description in Unicode".localized{
                textView.text = ""
            }
            titleLabelHeight.constant = 18.0
            titleLabel.isHidden = false
            titleLabel.text = "Description in Unicode".localized
            imgCell.image = UIImage(named: "product-description")
        }
        
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.tag == 1{
            
            productParamDel?.param(name: CategoryConstant.AddProductKey.ProductNameEnglish.rawValue, value: textView.text ?? "")
            
            if textView.text == ""{
                titleLabelHeight.constant = 0.0
                titleLabel.isHidden = true
                textView.text = "Product Name".localized
            }
        }else if textView.tag == 2{
            productParamDel?.param(name: CategoryConstant.AddProductKey.ProductNameUnicode.rawValue, value: textView.text ?? "")
            if textView.text == ""{
                titleLabelHeight.constant = 0.0
                titleLabel.isHidden = true
                textView.text = "Product Name In Unicode *".localized
            }
        }else if textView.tag == 4{
            productParamDel?.param(name: CategoryConstant.AddProductKey.ProductCategory.rawValue, value: textView.text ?? "")
            if textView.text == ""{
                titleLabelHeight.constant = 0.0
                titleLabel.isHidden = true
                textView.text = "Select Category *".localized
            }
        }else if textView.tag == 6{
            productParamDel?.param(name: CategoryConstant.AddProductKey.ProductDescription.rawValue, value: textView.text ?? "")
            if textView.text == ""{
                titleLabelHeight.constant = 0.0
                titleLabel.isHidden = true
                textView.text = "Description".localized
            }
        }else if textView.tag == 7{
            productParamDel?.param(name: CategoryConstant.AddProductKey.ProductDescriptionInUnicode.rawValue, value: textView.text ?? "")
            if textView.text == ""{
                titleLabelHeight.constant = 0.0
                titleLabel.isHidden = true
                textView.text = "Description in Unicode".localized
            }
        }
    }
    
    
}

protocol EditProductImagesNewCellDelegate {
    func reloadProductDetailPage(id: Int)
    func palyVideo(url : URL)
}

class EditProductImagesNewCell : UITableViewCell {
    var delegate : EditProductImagesNewCellDelegate?
    @IBOutlet var viewContainerData: UIView!
    @IBOutlet var pageControl: UIPageControl!
    var model : GetProductDetailsModel?
    var scrollImageContainer =  UIScrollView()
    var currentPage = 1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        pageControl.currentPage  = 0
        self.viewContainerData.layer.cornerRadius = 5.0
        self.viewContainerData.layer.masksToBounds = true
        self.viewContainerData.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewContainerData.layer.borderWidth = 1.0
        self.viewContainerData.backgroundColor = .white
    }
    
    func loadImage(imageURL: [Tushar_PictureModelElement], videoURL : [Tushar_VideoModelElement] , model : GetProductDetailsModel?) {
        
        println_debug(self.viewContainerData.frame)
        for (_ , elementX) in scrollImageContainer.subviews.enumerated() {
            elementX.removeFromSuperview()
        }
        
        self.model = model
        pageControl.numberOfPages = (imageURL.count ) + (videoURL.count)
        scrollImageContainer.frame = CGRect(x: 0, y: 0, width: self.viewContainerData.frame.size.width, height: 215)
        //scrollImageContainer.delegate = self
        scrollImageContainer.isPagingEnabled = true
        
        var xAxis : CGFloat = 0
        if imageURL.count > 0 {
            for (item, element) in imageURL.enumerated() {
                let viewImageContainer = UIView()
                let imageProduct = UIImageView()
                let btnDeleteProductImage = UIButton()
                println_debug(xAxis)
                viewImageContainer.frame = CGRect(x: xAxis, y: 0, width: self.self.viewContainerData.frame.size.width , height: 215)
                scrollImageContainer.addSubview(viewImageContainer)
                
                imageProduct.frame = CGRect(x: 30, y: 30, width: self.self.viewContainerData.frame.size.width - 60, height: 140)
                imageProduct.contentMode = .scaleAspectFit
                imageProduct.tag = item
                imageProduct.sd_setImage(with: URL(string: (element.pictureURL ?? "") as String))
                
                
                btnDeleteProductImage.frame = CGRect(x: self.viewContainerData.frame.size.width - 40, y: 5, width: 20, height: 20)
                btnDeleteProductImage.setImage(UIImage(named: "Delete"), for: .normal)
                btnDeleteProductImage.tag = item
                btnDeleteProductImage.addTarget(self, action: #selector(onClickDeleteAction(_:)), for: .touchUpInside)
                btnDeleteProductImage.bringSubviewToFront(viewImageContainer)
                viewImageContainer.addSubview(btnDeleteProductImage)
                viewImageContainer.addSubview(imageProduct)
                
                xAxis += self.viewContainerData.frame.size.width
            }
        }

        if videoURL.count > 0 {
            for (item, element) in videoURL.enumerated() {
                
                let viewImageContainer = UIView()
                viewImageContainer.frame = CGRect(x: xAxis, y: 0, width: self.viewContainerData.frame.size.width , height: 215)
                
                let videoProductView = UIView()
                videoProductView.frame = CGRect(x: 30, y: 30, width: self.viewContainerData.frame.size.width - 60, height: 140)
                videoProductView.tag = 100 + item
                videoProductView.backgroundColor = .darkGray
                let url = NSURL(fileURLWithPath: NSString(string: (element.videoURL ?? "") as String) as String)
                videoProductView.layer.setValue(url, forKey: "videoURL")
                
                let btnDeleteProductImage = UIButton()
                btnDeleteProductImage.frame = CGRect(x: self.viewContainerData.frame.size.width - 40, y: 5, width: 20, height: 20)
                btnDeleteProductImage.setImage(UIImage(named: "Delete"), for: .normal)
                btnDeleteProductImage.tag = 100 + item
                btnDeleteProductImage.bringSubviewToFront(viewImageContainer)
                btnDeleteProductImage.addTarget(self, action: #selector(onClickDeleteAction(_:)), for: .touchUpInside)
                viewImageContainer.addSubview(btnDeleteProductImage)
               
                let playButton = UIButton()
                playButton.frame = CGRect.init(x: (videoProductView.frame.size.width / 2  - 25) , y: (videoProductView.frame.size.height / 2  - 25), width: 50, height: 50)
                playButton.setImage(UIImage(named: "Play"), for: .normal)
                playButton.tag = 100 + item
                playButton.addTarget(self, action: #selector(onClickPlayVideoAction(_:)), for: .touchUpInside)
                videoProductView.addSubview(playButton)
                
                viewImageContainer.addSubview(videoProductView)
                scrollImageContainer.addSubview(viewImageContainer)
                
                xAxis += self.viewContainerData.frame.size.width
            }
        }
        
        scrollImageContainer.contentSize = CGSize(width: (self.viewContainerData.frame.size.width * CGFloat((imageURL.count ) + (videoURL.count))), height: 215)
        let tempFrame = CGRect.init(x: self.viewContainerData.frame.origin.x , y: self.viewContainerData.frame.origin.y , width: self.viewContainerData.frame.size.width, height: 158)
        self.viewContainerData.frame = tempFrame
        self.viewContainerData.addSubview(scrollImageContainer)

    }
    
    @objc func onClickDeleteAction(_ button: UIButton) {
        if let del = delegate {
            del.reloadProductDetailPage(id: button.tag)
        }
    }
    
    @objc func onClickPlayVideoAction(_ button: UIButton) {
        if let urlView = button.superview  {
            if let url = urlView.layer.value(forKey: "videoURL") as? URL{
                if let del = delegate {
                    del.palyVideo(url: url)
                }
            }
        }
    }
}


class PriceUOMCell: UITableViewCell {

    @IBOutlet var priceLabelHeight: NSLayoutConstraint!
    @IBOutlet var priceValueTextField: UITextField!
    @IBOutlet var priceLabel: UILabel!
  
    var productParamDel: AddProductParamProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        priceValueTextField.delegate = self
        priceValueTextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        // Initialization code
        //priceValueTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setPrice(price: String){
        let valuePrice = price
        if valuePrice != ""{
            priceLabel.isHidden = false
            priceLabel.text = "Price*".localized
            priceValueTextField.text = valuePrice
        }else{
            priceLabel.isHidden = true
            priceValueTextField.text = "Price*".localized
        }
    }

}

extension PriceUOMCell: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == priceValueTextField{
            
            if textField.text == "Price*".localized{
                textField.keyboardType = .numberPad
                textField.text = ""
                priceLabelHeight.constant = 20.0
                priceLabel.isHidden = false
                priceLabel.text = "Price*".localized
            }
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textField == priceValueTextField{
            if textField.text == ""{
                priceLabelHeight.constant = 20.0
                priceLabel.isHidden = true
                textField.text = "Price".localized
            }else{
                productParamDel?.param(name: CategoryConstant.AddProductKey.ProducPrice.rawValue,value: textField.text ?? "")
            }
        }
        
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == priceValueTextField{
            if let count  = (textField.text?.count ?? 0 + string.count ) as? Int {
                if string == ""{
                    return true
                }else{
                    if count > 8{
                        return false
                    }else{
                        return true
                    }
                }
            }
        }
        return true
    }
    
   @objc func textFieldDidChange(textField: UITextField) {
            if textField == self.priceValueTextField {
                // Some locales use different punctuations.
                
                var textFormatted = textField.text?.replacingOccurrences(of: ",", with: "")
                textFormatted = textFormatted?.replacingOccurrences(of: ".", with: "")

                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                if let text = textFormatted, let textAsInt = Int(text) {
                    textField.text = numberFormatter.string(from: NSNumber(value: textAsInt))
                }
            }
        }
    
}



class PriceUOMCellOne: UITableViewCell {

    @IBOutlet var umoValueTextField: UITextField!
    @IBOutlet var umoTitleLabel: UILabel!
    var productParamDel: AddProductParamProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        umoValueTextField.delegate = self
        // Initialization code
        //priceValueTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUOM(UOM: String){
        let valueUOM = UOM
        umoTitleLabel.text = "UOM *".localized
        if valueUOM != ""{
            umoValueTextField.text  = valueUOM
        }else{
            umoValueTextField.text  = "Ex i.e 1-kg".localized
        }
       
    }

}

extension PriceUOMCellOne: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == "Ex i.e 1-kg".localized{
                textField.keyboardType = .asciiCapable
                textField.text = ""
         }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
            if textField.text != ""{
                productParamDel?.param(name: CategoryConstant.AddProductKey.ProductUom.rawValue,value: textField.text ?? "")
            }
       }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
}





class AddMoreDetails: UITableViewCell {

    @IBOutlet var titleLbl: UILabel! {
        didSet {
            self.titleLbl.font = UIFont(name: appFont, size: 17.0)
            self.titleLbl.text = self.titleLbl.text?.localized
        }
    }
    
    @IBOutlet var arrowButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
