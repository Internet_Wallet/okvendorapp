//
//  EditProductsNewVC.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 03/11/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import DropDown
import Photos
import AVKit
import DKImagePickerController


class EditProductsNewVC: OSKBaseViewController {
    
    @IBOutlet var tblProductDetails: UITableView!
    @IBOutlet var btnUdate: UIButton!{
        didSet{
            btnUdate.setTitle("Update".localized, for: .normal)
        }
    }
    @IBOutlet var btnPublish: UIButton!{
        didSet{
           btnPublish.setTitle("Show".localized, for: .normal)
        }
    }
    
    var comingFrom: String?
    var alertVC : SAlertController?
    let aPIManager = APIManager()
    var apiManagerClient : APIManagerClient?
    
  //  var productDetailsModel: GetPrdouctDetailsModel?
    var productDetailsModel: GetProductDetailsModel?
    var categoryLastName = ""
    var productID : String?
    var productName = ""
    var productNameUni = ""
    var productPrice = ""
    var productUOM = ""
    var productCategory = ""
    var productCategoryID = 0
    var productDescription = ""
    var productDescriptionInUnicode = ""
    
    @IBOutlet var stackOption: UIStackView!
    
    @IBOutlet var lblLineSeparator: UILabel!
    
    var imageData = [Data]()
    var imageName = [String]()
    var videoData = [Data]()
    var videoName = [String]()
    
    var imageAndVideoArray : [DKAsset] = [DKAsset]()
    var allCategoryModel : GetAllCategoriesModel?
    
    var imageUploadCount = 0
    var uploadedImagesCount = 0
    var viewHeight : CGFloat = 80.0
    
    let dropDown = DropDown()
    var categoryDetails : AllCategory?
    var SUbCatList : [Category]? = []
    var subCategoryObj : [Category]? = []
    var childSubCategoryObj : [Category]? = []
    var tag: Int?
    var editValue: [Category]?
    var clickedCount = 0
    var selectedIndex: Int?
    var finalItem = ""
    
    var categoryId = Int()
   

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Edit Details".localized
        self.lblLineSeparator.isHidden = true
        self.stackOption.isHidden = true
        
        btnPublish.layer.cornerRadius = 7.0
        btnUdate.layer.cornerRadius = 7.0
        //self.getAllCategories()
        self.getAllCategoriesAPICall()
    }
    
    
    func getAllCategoriesAPICall(){
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            self.aPIManager.getAllCategories(onSuccess: {
                self.categoryDetails = self.apiManagerClient?.allCategories
                self.SUbCatList = self.categoryDetails?.listCategory
                if let viewLoc = self.view {
                       AppUtility.hideLoading(viewLoc)
                   }
                DispatchQueue.main.async {
                  //  self.categoryCollectionView.reloadData()
                   // self.categoryTableView.reloadData()
                }
            }, onError: { [weak self] message in
                 if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                }
            })
        }
 }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        callProductDetails()
    }
    
    func callProductDetails(){
        if let proID = productID, !proID.isEmpty {
            self.getProductDetialsByID(productID: proID, reloadFor: "")
            self.stackOption.isHidden = false
            self.lblLineSeparator.isHidden = false
        }
    }
    
    
    @IBAction func onClickPublishAction(_: UIButton) {
        self.publishAndUnPublishWebCall(productID: self.productID ?? "")
    }
   
    @IBAction func onClickBackAction(_: UIButton) {
        if comingFrom == "AddProduct" {
            self.navigationController?.popToRootViewController(animated: true)
        }else {
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewDidLayoutSubviews() {
        tblProductDetails.reloadData()
    }
    
    @IBAction func onClickAddAttributsAction(_: UIButton) {
        var path = ""
        if let model = allCategoryModel?.data {
            for item in model {
                if item.name == categoryLastName {
                    path = item.iconPath ?? ""
                }
            }
        }
        
        
//        let sb = UIStoryboard(name: "EditProduct", bundle: nil)
//            let vcAttributs = sb.instantiateViewController(withIdentifier: "EditProductAttributs")  as! EditProductAttributs
//            vcAttributs.view.frame = CGRect.init(x: 0, y: 0, width:self.view.frame.width, height: self.view.frame.height)
//            vcAttributs.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
//            addChild(vcAttributs)
//            vcAttributs.model = productDetailsModel
//            vcAttributs.iconImageURL = path
//            vcAttributs.didMove(toParent: self)
//            vcAttributs.delegate = self
//            self.view.addSubview((vcAttributs.view)!)
    }
    
    @IBAction func onClickUpdateAction(_: UIButton) {
        guard productID != nil else {
            return
        }
        
        if productDetailsModel?.data?.pictureModels?.count == 0 {
            self.showAlertInClass(title: "Error".localized, subTitle: "Please add at least one image".localized)
            return
        }
        
        if productName.isEmpty {
 
            self.showAlertInClass(title: "Error".localized, subTitle: "Please fill product name".localized)
           return
        }
        
        if productNameUni.isEmpty {
            self.showAlertInClass(title: "Error".localized, subTitle: "Please fill product unicode name".localized)
            return
        }
        
        if productPrice.isEmpty {
            self.showAlertInClass(title: "Error".localized, subTitle: "Please fill product price".localized)
            return
        }else {
            productPrice = productPrice.replacingOccurrences(of: ",", with: "")
        }
                
        let uniCodeParams = ["Name": productNameUni]
        
        let jsonData = try! JSONSerialization.data(withJSONObject: uniCodeParams, options: [])
        let jsonString = String(data: jsonData, encoding: .utf8)!
        
        let params = ["Id": productID ?? "","CategoryId":"\(self.productCategoryID)","Name": productName, "Price": productPrice, "OtherLanguageDataUnicode": "\(jsonString)" , "UOM":self.productUOM ,"DeliveryDateId":"1" ,"Published":"true"] as [String:Any]
        
        self.updateProductDetails(params: params, handler: { isSuccess in
            if let viewLoc = self.view {
                   AppUtility.hideLoading(viewLoc)
               }
            DispatchQueue.main.async {
                if isSuccess {
                    println_debug("Product updated")
                    self.alertVC = SAlertController()
                    self.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "Product successfully edited", onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    self.navigationController?.popViewController(animated: true)
                    })
                    self.alertVC?.addAction(action: [YesAction])
                }else {
                    println_debug("Product updated failed")
                    self.showAlertInClass(title: "Error".localized, subTitle: "Product editing is failed".localized)
                }
            }
           
        })
    }
    
    @objc func onClickCameraAction(_ button: UIButton) {
        self.view.endEditing(true)
        if button.tag == 10 {
            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

            let firstAction: UIAlertAction = UIAlertAction(title: "Photo".localized, style: .default) { action -> Void in
                DispatchQueue.main.async {
                    let pickerController = DKImagePickerController()
                    let photoVideoCount = (self.productDetailsModel?.data?.pictureModels?.count ?? 0) + (self.productDetailsModel?.data?.videoModels?.count ?? 0)
                    self.imageUploadCount = 5 - ((self.productDetailsModel?.data?.pictureModels?.count ?? 0) + (self.productDetailsModel?.data?.videoModels?.count ?? 0))
                    if  photoVideoCount >= 5 {
                        self.alertVC = SAlertController()
                        self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Please remove at least one image or video", onController: self)
                        let YesAction = SAlertAction()
                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        })
                        self.alertVC?.addAction(action: [YesAction])
                       return
                    }
                    pickerController.maxSelectableCount = self.imageUploadCount
                    pickerController.assetType = .allPhotos
                    DKImageExtensionController.registerExtension(extensionClass: CustomCameraExtensionA.self, for: .camera)
                    pickerController.didSelectAssets = { (assets: [DKAsset]) in
                    self.imageAndVideoArray.append(contentsOf: assets)
                    DispatchQueue.main.async {
                             self.loadImageAndVideoData()
                        }
                    }
                    self.present(pickerController, animated: true) {}
                }
            }
            
            let secondAction: UIAlertAction = UIAlertAction(title: "Video", style: .default) { action -> Void in
             let pickerController = DKImagePickerController()
             pickerController.allowMultipleTypes = true
                let photoVideoCount = (self.productDetailsModel?.data?.pictureModels?.count ?? 0) + (self.productDetailsModel?.data?.videoModels?.count ?? 0)
                
                if photoVideoCount < 5 {
                    if (self.productDetailsModel?.data?.videoModels?.count ?? 0) == 0  {
                        pickerController.maxSelectableCount = 1
                    }else {
                        self.alertVC = SAlertController()
                        self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Please remove at least one image or video", onController: self)
                        let YesAction = SAlertAction()
                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        })
                        self.alertVC?.addAction(action: [YesAction])
                       return
                    }
                }else {
                    if photoVideoCount >= 5  {
                        self.alertVC = SAlertController()
                        self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Please remove at least one image or video", onController: self)
                        let YesAction = SAlertAction()
                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        })
                        self.alertVC?.addAction(action: [YesAction])
                       return
                    }else {
                        pickerController.maxSelectableCount = 1
                    }
                }
             pickerController.assetType = .allVideos
             DKImageExtensionController.registerExtension(extensionClass: CustomCameraExtensionB.self, for: .camera)
             pickerController.didSelectAssets = { (assets: [DKAsset]) in
                 print(assets)
                 self.imageAndVideoArray.append(contentsOf: assets)
                 DispatchQueue.main.async {
                     self.loadImageAndVideoData()

                 }
             }
             self.present(pickerController, animated: true) {}
            }
            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
            actionSheetController.addAction(firstAction)
            actionSheetController.addAction(secondAction)
            actionSheetController.addAction(cancelAction)
            
            actionSheetController.popoverPresentationController?.sourceView = self.view
            present(actionSheetController, animated: true) {
                print("option menu presented")
            }
            
        }else if button.tag == 20 {
            DispatchQueue.main.async {
                if #available(iOS 13.0, *) {
                    let vc = self.storyboard?.instantiateViewController(identifier: "ProductBarScanner") as! ProductBarScanner
                    vc.delegate = self
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    // Fallback on earlier versions
                }
            }
        }else if button.tag == 41 {

              
          
        }else {
            let sb = UIStoryboard(name: "EditProduct", bundle: nil)
            let allCategoryVC = sb.instantiateViewController(withIdentifier: "GetAllCategoriesVC")  as! GetAllCategoriesVC
            allCategoryVC.view.frame = CGRect.init(x: 10, y: 64, width:self.view.frame.width - 20, height: self.view.frame.height - 64)
            addChild(allCategoryVC)
            allCategoryVC.delegate = self
            allCategoryVC.didMove(toParent: self)
            self.view.addSubview((allCategoryVC.view)!)
        }
    }
    
    func getCellTextView(row: Int) -> EditFieldsCell {
        return tblProductDetails.cellForRow(at: IndexPath(row: row, section: 0)) as! EditFieldsCell
    }
    
    func getCellTextField(row: Int) -> EditFieldsCell2 {
        return tblProductDetails.cellForRow(at: IndexPath(row: row, section: 0)) as! EditFieldsCell2
    }
    
    
    
    func loadImageAndVideoData () {
        
        imageData.removeAll()
        imageName.removeAll()
        videoData.removeAll()
        videoName.removeAll()
        if imageAndVideoArray.count == 0 {
            return
        }
        
        for (_ ,object )in self.imageAndVideoArray.enumerated() {
            if object.type == .photo {
                let assetImage : PHAsset = object.originalAsset ?? PHAsset()
                var img: UIImage?
                let manager = PHImageManager.default()
                let options = PHImageRequestOptions()
                options.version = .original
                options.isSynchronous = true
                manager.requestImageData(for: assetImage, options: options) { data, _, _, _ in
                    if let data = data {
                        img = UIImage(data: data)
                        let data:Data = img?.jpegData(compressionQuality: 0.5) ?? Data()
                        self.imageData.append(data)
                        let name = String(Date().toMillis())
                        self.imageName.append("\(name).jpeg")
                    }
                }
                self.uploadedImagesCount = self.imageData.count
                if self.imageData.count == imageAndVideoArray.count {
                    self.imageAndVideoArray.removeAll()
                    self.uploadPhotos()
                }
            }else if object.type == .video {
                if object.duration > 15 {
                    self.alertVC = SAlertController()
                    self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Selected video time is more than 15 seconds. Please select another", onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    })
                    self.alertVC?.addAction(action: [YesAction])
                    return
                }
                guard let assetData = object.originalAsset else { return }
                self.convertVideo(phAsset: assetData, handler:{(name,data) in
                     self.videoName.append(name)
                     self.videoData.append(data)
                    if self.videoData.count == self.imageAndVideoArray.count {
                        self.imageAndVideoArray.removeAll()
                        self.uploadVideos()
                    }
                })
            }
        }
        println_debug("Photo Count\(imageData)")
        
    }
    
    
    
    func convertVideo(phAsset : PHAsset,handler: @escaping (_ name: String,_ imageData: Data) -> Void) {
        PHImageManager.default().requestAVAsset(forVideo: phAsset, options: PHVideoRequestOptions(), resultHandler: { (asset, audioMix, info) -> Void in
            if let asset = asset as? AVURLAsset {
                do {
                    let videoData = try  Data.init(contentsOf: asset.url)
                    print(asset.url)
                    print("File size before compression: \(Double(videoData.count / 1048576)) mb")
                    let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".MP4")
                    print(compressedURL)
                    self.compressVideo(inputURL: asset.url , outputURL: compressedURL) { (exportSession) in
                        guard let session = exportSession else {
                            return
                        }
                        switch session.status {
                        case .unknown:
                            print("unknown")
                            break
                        case .waiting:
                            print("waiting")
                            break
                        case .exporting:
                            print("exporting")
                            break
                        case .completed:
                            do {
                                let compressedData = try  Data.init(contentsOf: compressedURL)
                                let name = String(Date().toMillis())
                                handler("\(name).mp4", compressedData as Data)
                            }
                            catch{
                                print(error)
                            }
                            
                        case .failed:
                            print("failed")
                            break
                        case .cancelled:
                            print("cancelled")
                            break
                        @unknown default:
                            break
                        }
                    }
                } catch {
                    print(error)
                    //return
                }
            }
        })
        
        
    }
    
   @objc func addMoreDetailsScreen() {
        
    var path = ""
    if let model = allCategoryModel?.data {
        for item in model {
            if item.name == categoryLastName {
                path = item.iconPath ?? ""
            }
        }
    }
    
    
    let sb = UIStoryboard(name: "EditProduct", bundle: nil)
        let vcAttributs = sb.instantiateViewController(withIdentifier: "EditProductAttributs")  as! EditProductAttributs
        vcAttributs.view.frame = CGRect.init(x: 0, y: 0, width:self.view.frame.width, height: self.view.frame.height)
        vcAttributs.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
        addChild(vcAttributs)
      //  vcAttributs.model = productDetailsModel
        vcAttributs.iconImageURL = path
        vcAttributs.didMove(toParent: self)
        vcAttributs.delegate = self
        self.view.addSubview((vcAttributs.view)!)
    }
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            return
        }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    

}

extension EditProductsNewVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //this check will work when coming from my product and edit
                    switch  indexPath.row {
                    case 0:
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EditProductImagesNewCell", for: indexPath) as? EditProductImagesNewCell else{
                            return UITableViewCell()
                        }
                        cell.selectionStyle = .none
                        let imageURL = productDetailsModel?.data?.pictureModels
                        let videoURL = productDetailsModel?.data?.videoModels
                        cell.delegate = self
                        if let imgURl = imageURL {
                            cell.loadImage(imageURL: imgURl , videoURL: videoURL ?? [], model: productDetailsModel)
                        }
                        return cell
                    
                    case 1,2,5,6,7:
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EditProductNewTableViewCell") as? EditProductNewTableViewCell  else {
                            return UITableViewCell()
                        }
                        if indexPath.row == 1{
                            cell.addProductTextView.tag = 1
                            cell.titleLabel.tag = 1
                             let value =  self.productName
                             cell.setDataOnCell(tag: 1,titleValue: value)
                        }else if (indexPath.row == 2){
                            cell.addProductTextView.tag = 2
                            cell.titleLabel.tag = 2
                            let value =  self.productNameUni
                            cell.setDataOnCell(tag: 2,titleValue: value)
                        }
                        else if (indexPath.row == 5){
                            cell.addProductTextView.isUserInteractionEnabled = false
                            cell.addProductTextView.tag = 5
                            cell.titleLabel.tag = 5
                            let value =  self.productCategory
                            cell.setDataOnCell(tag: 5,titleValue: value)
                        }
                       
                        else if (indexPath.row == 6){
                            cell.addProductTextView.isUserInteractionEnabled = true
                            cell.addProductTextView.tag = 6
                            cell.titleLabel.tag = 6
                            let value =  self.productDescription
                            cell.setDataOnCell(tag: 6,titleValue: value)
                        }
                     
                        else if (indexPath.row == 7){
                            cell.addProductTextView.isUserInteractionEnabled = true
                            cell.addProductTextView.tag = 7
                            cell.titleLabel.tag = 7
                            let value =  self.productDescriptionInUnicode
                            cell.setDataOnCell(tag: 7,titleValue: value)
                        }
                     
                        cell.productParamDel = self
                        cell.titleDelegate = self
                        cell.cameraDelegate = self
                        return cell
                    case 3:
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PriceUOMCell") as? PriceUOMCell  else {
                            return UITableViewCell()
                        }
                        let valuePrice =  self.productPrice
                        cell.setPrice(price: valuePrice)
                        cell.productParamDel = self
                        return cell
                    case 4:
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PriceUOMCellOne") as? PriceUOMCellOne  else {
                            return UITableViewCell()
                        }
                        let valueUOM =  self.productUOM
                        cell.setUOM(UOM: valueUOM)
                        cell.productParamDel = self
                        return cell
                        
                    case 8:
                          guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddMoreDetails") as? AddMoreDetails  else {
                            return UITableViewCell()
                        }
                        cell.titleLbl.text = "Add More Details (Optional)".localized
                        cell.arrowButton.addTarget(self, action: #selector(addMoreDetailsScreen), for: .touchUpInside)
                        return cell
                    
                                            
                    default:
                        break
                }
                
        return UITableViewCell()
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            switch  indexPath.row {
            case 0:
                if (productDetailsModel?.data?.pictureModels?.count ?? 0) > 0 ||  (productDetailsModel?.data?.videoModels?.count ?? 0) > 0  {
                    return 220
                }
            case 8:
                if (productDetailsModel?.data?.productAttributes?.count ?? 0) > 0 {
                    return 80
                }
                return 0
            case 1,2,3,4:
                return 80
            case 5,6,7:
                return 80 //UITableView.automaticDimension
            default:
                break
            }
         return 0
     }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.row == 5 {
            editValue =  self.SUbCatList
            
            let indexPath = IndexPath(row: indexPath.row, section: 0)
            guard let cell = self.tblProductDetails.cellForRow(at: indexPath) as? EditProductNewTableViewCell else{
                return
            }
            dropDown.anchorView = cell.addProductTextView
            dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
            dropDown.dataSource = (editValue?.compactMap({$0.name ?? ""}))!
            dropDown.direction = .top
            dropDown.show()
            
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                selectedIndex = index
                
                if finalItem == ""{
                    finalItem = finalItem + " " + item
                }
                else{
                    finalItem = finalItem + " >> " + item
                }
                
                
                self.productCategory  = finalItem
                
                var dataavilable = "0"
                
                print(productCategory)
                if let SUbCat = self.SUbCatList?[index] {
                    
                    if SUbCat.Children?.count ?? 0 > 0 {
                        self.SUbCatList?.removeAll()
                        self.SUbCatList = SUbCat.Children
                       
                        self.productCategoryID = SUbCat.Id ?? 0
                        dataavilable = "1"
                        
                    }
                    else{
                       
                        self.productCategoryID = SUbCat.Id ?? 0
                        dataavilable = "0"
                        dropDown.hide()
                    }
                    
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                    
                    
                    if dataavilable == "1"{
                        self?.dropDown.reloadAllComponents()
                        self?.tblProductDetails.reloadData()
                        let indexPath = IndexPath(row: 5, section: 0)
                        tblProductDetails.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                        if clickedCount == 0{
                            
                            tblProductDetails.delegate?.tableView!(tblProductDetails, didSelectRowAt: indexPath)
                        }else if clickedCount == 1{
                            
                            tblProductDetails.delegate?.tableView!(tblProductDetails, didSelectRowAt: indexPath)
                        }  else{
                            // self?.getAllCategoriesAPICall()
                            
                        }
                        clickedCount = clickedCount + 1
                    }
                    else{
                        self?.getAllCategoriesAPICall()
                        self?.tblProductDetails.reloadData()
                        finalItem = ""
                        dataavilable = "0"
                        clickedCount = 0
                    }
                    
                }
                
                
            }
            
        }
        if indexPath.row == 8 {
            
            var path = ""
            if let model = allCategoryModel?.data {
                for item in model {
                    if item.name == categoryLastName {
                        path = item.iconPath ?? ""
                    }
                }
            }
            
            DispatchQueue.main.async {
                var vcObj: AttributeViewController?
                vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.kAttributeViewController) as? AttributeViewController
                if let validObj = vcObj {
                    validObj.productDetailsModel = self.productDetailsModel
                    self.navigationController?.pushViewController(validObj, animated: true)
                }
            }
            
//            let sb = UIStoryboard(name: "EditProduct", bundle: nil)
//                let vcAttributs = sb.instantiateViewController(withIdentifier: "EditProductAttributs")  as! EditProductAttributs
//                vcAttributs.view.frame = CGRect.init(x: 0, y: 0, width:self.view.frame.width, height: self.view.frame.height)
//                vcAttributs.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
//                addChild(vcAttributs)
//              //  vcAttributs.model = productDetailsModel
//                vcAttributs.iconImageURL = path
//                vcAttributs.didMove(toParent: self)
//                vcAttributs.delegate = self
//                self.view.addSubview((vcAttributs.view)!)
            }
      }
       
}
