//
//  EditProductsNewDelegate.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 03/11/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Foundation
import AVKit
import DKImagePickerController



extension EditProductsNewVC : UITextFieldDelegate,UITextViewDelegate,ProductBarScannerDelegate,GetAllCategoriesVCDelegate,EditProductImagesNewCellDelegate ,EditProductAttributsDelegate , AddProductParamProtocol, AddProductTitleLabelDel, CallCamera , CameraVideoBarcodeDelegate {
    
    func param(name: String, value: String) {
        
        if name ==  CategoryConstant.AddProductKey.ProductNameEnglish.rawValue {
            productName = value
        }
        if name == CategoryConstant.AddProductKey.ProductNameUnicode.rawValue {
            productNameUni = value
        }
        if name == CategoryConstant.AddProductKey.ProductCategory.rawValue {
            productCategory = value
        }
        if name == CategoryConstant.AddProductKey.ProductUom.rawValue {
            productUOM = value
        }
        if name == CategoryConstant.AddProductKey.ProducPrice.rawValue {
            productPrice = value
        }
        
        if name == CategoryConstant.AddProductKey.ProductDescription.rawValue {
            productDescription = value
        }
        
        if name == CategoryConstant.AddProductKey.ProductDescriptionInUnicode.rawValue {
            productDescriptionInUnicode = value
        }
        
    }
    
    func manageHeight(index: Int, hasText: Bool) {
        
        
    }
    
    //0 for camera
    //1 for video
    //2 for barcode
    func cameraVideoBarcodeSelected(index: Int){
        if index == 0{
            let pickerController = DKImagePickerController()
            let photoVideoCount = (self.productDetailsModel?.data?.pictureModels?.count ?? 0) + (self.productDetailsModel?.data?.videoModels?.count ?? 0)
            self.imageUploadCount = 5 - ((self.productDetailsModel?.data?.pictureModels?.count ?? 0) + (self.productDetailsModel?.data?.videoModels?.count ?? 0))
            if  photoVideoCount >= 5 {
                self.alertVC = SAlertController()
                self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Please remove at least one image or video", onController: self)
                let YesAction = SAlertAction()
                YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                })
                self.alertVC?.addAction(action: [YesAction])
               return
            }
            pickerController.maxSelectableCount = self.imageUploadCount
            pickerController.assetType = .allPhotos
            DKImageExtensionController.registerExtension(extensionClass: CustomCameraExtensionA.self, for: .camera)
            pickerController.didSelectAssets = { (assets: [DKAsset]) in
            self.imageAndVideoArray.append(contentsOf: assets)
            DispatchQueue.main.async {
                     self.loadImageAndVideoData()
                }
            }
            self.present(pickerController, animated: true) {}
        }else if index == 1{
            let pickerController = DKImagePickerController()
            pickerController.allowMultipleTypes = true
               let photoVideoCount = (self.productDetailsModel?.data?.pictureModels?.count ?? 0) + (self.productDetailsModel?.data?.videoModels?.count ?? 0)
               
               if photoVideoCount < 5 {
                   if (self.productDetailsModel?.data?.videoModels?.count ?? 0) == 0  {
                       pickerController.maxSelectableCount = 1
                   }else {
                       self.alertVC = SAlertController()
                       self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Please remove at least one image or video", onController: self)
                       let YesAction = SAlertAction()
                       YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                       })
                       self.alertVC?.addAction(action: [YesAction])
                      return
                   }
               }else {
                   if photoVideoCount >= 5  {
                       self.alertVC = SAlertController()
                       self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Please remove at least one image or video", onController: self)
                       let YesAction = SAlertAction()
                       YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                       })
                       self.alertVC?.addAction(action: [YesAction])
                      return
                   }else {
                       pickerController.maxSelectableCount = 1
                   }
               }
            pickerController.assetType = .allVideos
            DKImageExtensionController.registerExtension(extensionClass: CustomCameraExtensionB.self, for: .camera)
            pickerController.didSelectAssets = { (assets: [DKAsset]) in
                print(assets)
                self.imageAndVideoArray.append(contentsOf: assets)
                DispatchQueue.main.async {
                    self.loadImageAndVideoData()

                }
            }
            self.present(pickerController, animated: true) {}
        }else{
            DispatchQueue.main.async {
                 if #available(iOS 13.0, *) {
                     let storyBoard = UIStoryboard(name: "EditProduct", bundle: nil)
                    if let validObj = storyBoard.instantiateViewController(identifier: "ProductBarScanner") as? ProductBarScanner{
                        validObj.delegate = self
                        self.navigationController?.pushViewController(validObj, animated: true)
                    }
                 } else {
                    let storyBoard = UIStoryboard(name: "EditProduct", bundle: nil)
                    if let validObj = storyBoard.instantiateViewController(withIdentifier: "ProductBarScanner") as? ProductBarScanner{
                        validObj.delegate = self
                        self.navigationController?.pushViewController(validObj, animated: true)
                    }
                    
                 }
             }
        }
    }
    
    func callCamera(){
           
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
          if let presentedViewController = storyBoard.instantiateViewController(withIdentifier: CategoryConstant.kCameraVideoBarcodeVC) as? CameraVideoBarcodeVC {
                presentedViewController.providesPresentationContextTransitionStyle = true
                presentedViewController.definesPresentationContext = true
               presentedViewController.delegate = self
                presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
            presentedViewController.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.7)
                self.present(presentedViewController, animated: true, completion: nil)
         }
    }
    
    
    
    func returnBarCode(barCade: String) {
        
        
    }
    
    func selectedCategory(category: AllCategoriesData, viewController: UIViewController) {
    
        
    }
    
    
    func reloadProductDetailPage(id: Int) {
        
            if id < 100 {
                var imageArray = self.productDetailsModel?.data?.pictureModels
                self.deleteCategoryIamge(pictureID: String(imageArray![id].id ?? 0), handler: { isSuccess in
                    if isSuccess {
                        imageArray?.remove(at: id)
                        self.productDetailsModel?.data?.pictureModels = imageArray!
                        self.tblProductDetails.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                        }else {
                    }
                })
            }else {
                var imageArray = self.productDetailsModel?.data?.videoModels
                self.deleteCategoryVideo(pictureID: String(imageArray![id - 100].id ?? 0), handler: { isSuccess in
                    if isSuccess {
                        imageArray?.remove(at: id - 100)
                        self.productDetailsModel?.data?.videoModels = imageArray!
                        self.tblProductDetails.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                        }else {
                    }
                })
            }
      }
    
    func palyVideo(url: URL) {
        let player = AVPlayer(url: url)
        let vc = AVPlayerViewController()
        vc.player = player
        self.present(vc, animated: true) { vc.player?.play() }
    }
    
    //    // MARK: UITextViewDelegate
        func textViewDidChange(_ textView: UITextView) {
            if textView.tag == 1 {
                productName = textView.text ?? ""
            }else if textView.tag == 2 {
                productNameUni = textView.text ?? ""
            }
      
            let startHeight = textView.frame.size.height
            let calcHeight = textView.sizeThatFits(textView.frame.size).height
            if startHeight != calcHeight {
                UIView.setAnimationsEnabled(false)
                self.tblProductDetails.beginUpdates()
                self.tblProductDetails.endUpdates()
           }
        }
     
        func textViewDidBeginEditing(_ textView: UITextView) {
            
            
        }
        
        func setPlacehodlerText(textView: UITextView) -> String {
          var placeHodlerText = ""
            if textView.tag == 1 {
                placeHodlerText = "Product Name*".localized
            }else if textView.tag == 2 {
                placeHodlerText = "Product Name in unicode*".localized
            }
            return placeHodlerText
        }
        
        func textViewDidEndEditing(_ textView: UITextView) {
            if textView.tag == 1 {
                productName = textView.text ?? ""
            }else if textView.tag == 2 {
                productNameUni = textView.text ?? ""
            }
        }
    
        func reloadImageProductCell() {
            DispatchQueue.main.async {
                if let proID = self.productID, !proID.isEmpty {
                    self.getProductDetialsByID(productID: proID, reloadFor: "Image")
                }
            }
        }
        
        func reloadAPIForGetNewColor() {
            if let proID = self.productID, !proID.isEmpty {
                self.getProductDetialsByID(productID: proID, reloadFor: "ColorAttribute")
            }
        }
    
    private func getDigitDisplay(_ digit: String) -> String {
          var FormateStr: String
          let mystring = digit.replacingOccurrences(of: ",", with: "")
          let number = NSDecimalNumber(string: mystring)
          
          let formatter = NumberFormatter()
          formatter.groupingSeparator = ","
          formatter.groupingSize = 3
          formatter.usesGroupingSeparator = true
          FormateStr = formatter.string(from: number)!
          if FormateStr == "NaN"{
              FormateStr = ""
          }
          
          return FormateStr
      }
    
    
    
    public func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
           return .none
       }
}


extension EditProductsNewVC {
    
    func uploadPhotos() {
        let memeType = "image/jpeg"
        self.imageUpload(productID: productID ?? "", imgName: imageName[uploadedImagesCount - 1], type: memeType, fileData: imageData[uploadedImagesCount - 1], handler: { isSuccess in
            if isSuccess {
                
                self.callProductDetails()
                
//                self.uploadedImagesCount = self.uploadedImagesCount - 1
//                if self.uploadedImagesCount == 0 {
//                    self.imageName.removeAll()
//                    self.imageData.removeAll()
//                    self.imageAndVideoArray.removeAll()
//                    self.reloadImageProductCell()
//                }else {
//                    self.uploadPhotos()
//                }
            }
        })
    }
    
    func uploadVideos() {
        self.videoUpload(productID: self.productID ?? "", videoName: self.videoName[0], fileData: self.videoData[0], handler: {(isSuccess) in
            if isSuccess {
//                    self.videoName.removeAll()
//                    self.videoData.removeAll()
//                    self.imageAndVideoArray.removeAll()
//                    self.reloadImageProductCell()
                
                self.callProductDetails()
            }
        })
    }
        
    func imageUpload(productID: String, imgName: String,type: String ,fileData: Data,handler: @escaping (_ isSuccess: Bool) -> Void) {
        
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlString = String(format: "%@/vendor/addproductpicture", APIManagerClient.sharedInstance.base_url) as String
            println_debug(urlString)
            let params = ["ProductId": productID.trimmingCharacters(in: .whitespaces),"DisplayOrder": "1","OverrideAltAttribute": "test","OverrideTitleAttribute": "test"]
            println_debug(urlString)
            let request = NSMutableURLRequest(url:URL(string: urlString)!)
            request.httpMethod = "POST";
            let boundary = "Boundary-\(UUID2)"
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.httpBody = createBodyWithParameters(parameters: params, KeyName: "File", type: type ,filePathKey: imgName, imageDataKey: fileData as Data, boundary: boundary) as Data
            request.setValue(Vendor_loginmodel.shared.token, forHTTPHeaderField: "Token")
            request.setValue(aPIManager.nstKEY, forHTTPHeaderField: "NST")
            request.setValue(aPIManager.deviceId, forHTTPHeaderField: "DeviceId")
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                (data, response, error) in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    if error != nil {
                        handler(false)
                        return
                    }
                    if let jsonData = data {
                        println_debug(String(data: jsonData, encoding: .utf8))
                    }
                  
                    println_debug(response)
                    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                       handler(true)
                    }else {
                        handler(false)
                    }
                }
            }
            task.resume()
        }
    }
    
    func videoUpload(productID: String, videoName : String, fileData: Data,handler: @escaping (_ isSuccess: Bool) -> Void) {
        
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            DispatchQueue.main.async {
                AppUtility.showLoading(self.view)
                
               
                
                
                let urlString = String(format: "%@/vendor/addproductvideo", APIManagerClient.sharedInstance.base_url) as String
                println_debug(urlString)
                let params = ["ProductId": productID.trimmingCharacters(in: .whitespaces),"DisplayOrder": "1","OverrideAltAttribute": "test","OverrideTitleAttribute": "test"]
                let request = NSMutableURLRequest(url:URL(string: urlString)!)
                request.httpMethod = "POST";
                let boundary = "Boundary-\(UUID2)"
                request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                
                request.httpBody = self.createBodyWithParametersForVideo(parameters: params, filePathKey: videoName, imageDataKey: fileData as Data, boundary: boundary) as Data
                
                request.setValue(Vendor_loginmodel.shared.token, forHTTPHeaderField: "Token")
                request.setValue(self.aPIManager.nstKEY, forHTTPHeaderField: "NST")
                request.setValue(self.aPIManager.deviceId, forHTTPHeaderField: "DeviceId")
                let task = URLSession.shared.dataTask(with: request as URLRequest) {
                    (data, response, error) in
                    DispatchQueue.main.async {
                        if let viewLoc = self.view {
                            AppUtility.hideLoading(viewLoc)
                        }
                        do {
                            let responseX =  try JSONSerialization.jsonObject(with: data ?? Data(), options: []) as? [String: Any]
                            println_debug(responseX)
                        
                        } catch { print(error.localizedDescription)}
                        if error != nil {
                            handler(false)
                            return
                        }
                        println_debug(response)
                        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                           handler(true)
                        }else {
                            handler(false)
                        }
                    }
                }
                task.resume()
            }
        }
    }
    
    
    func createBodyWithParameters(parameters: [String: String]?, KeyName: String,type: String ,filePathKey: String?, imageDataKey: Data, boundary: String) -> NSData {
        var body = Data()
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(Data("--\(boundary)\r\n".utf8))
                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data("\(value)\r\n".utf8))
            }
        }
        var filename = ""
        if filePathKey != "" {
            let fileArray = filePathKey?.components(separatedBy: "/")
            filename = (fileArray?.last)!
        }
        let mimetype = type
        body.append(Data("--\(boundary)\r\n".utf8))
        body.append(Data("Content-Disposition: form-data; name=\"\(KeyName)\"; filename=\"\(filename)\"\r\n".utf8))
        body.append(Data("Content-Type: \(mimetype)\r\n\r\n".utf8))
        body.append(imageDataKey as Data)
        body.append(Data("\r\n".utf8))
        body.append(Data("--\(boundary)--\r\n".utf8))
        return body as NSData
    }
    
    func createBodyWithParametersForVideo(parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> NSData {
        var body = Data()
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(Data("--\(boundary)\r\n".utf8))
                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data("\(value)\r\n".utf8))
            }
        }
        var filename = ""
        if filePathKey != "" {
            let fileArray = filePathKey?.components(separatedBy: "/")
            filename = (fileArray?.last)!
        }
        let KeyName = "File"
        let mimetype = "video/mp4"
        body.append(Data("--\(boundary)\r\n".utf8))
        body.append(Data("Content-Disposition: form-data; name=\"\(KeyName)\"; filename=\"\(filename)\"\r\n".utf8))
        body.append(Data("Content-Type: \(mimetype)\r\n\r\n".utf8))
        body.append(imageDataKey as Data)
        body.append(Data("\r\n".utf8))
        body.append(Data("--\(boundary)--\r\n".utf8))
        return body as NSData
     }
    
}
