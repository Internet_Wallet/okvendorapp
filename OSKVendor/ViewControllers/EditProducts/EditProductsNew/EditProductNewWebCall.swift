//
//  EditProductNewWebCall.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 04/11/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Foundation


extension EditProductsNewVC {
    
    func deleteCategoryIamge(pictureID: String, handler:@escaping (_ succes: Bool) -> Void) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/deleteproductpicture/%@", APIManagerClient.sharedInstance.base_url,pictureID) as String
            print("Get allcategories :\(urlStr) ")
            self.aPIManager.postGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        return
                    }
                    print(jsonData)
                    handler(true)
                }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    handler(false)
            })
        }
    }
    
    func deleteCategoryVideo(pictureID: String, handler:@escaping (_ succes: Bool) -> Void) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/deleteproductvideo/%@", APIManagerClient.sharedInstance.base_url,pictureID) as String
            print("Get allcategories :\(urlStr) ")
            self.aPIManager.postGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        return
                    }
                    print(jsonData)
                    handler(true)
                }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    handler(false)
            })
        }
    }
    
    
    
    func getProductDetialsByID(productID: String, reloadFor: String) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/getproductbyid/%@", APIManagerClient.sharedInstance.base_url,productID.trimmingCharacters(in: .whitespaces)) as String
            print("Get Product by ID :\(urlStr) ")
            self.aPIManager.getGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        println_debug("Data error")
                        return
                    }
                    do {
                        if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? NSDictionary {
                            print(json)
                            let model = GetProductDetailsModel(response: json)
                            
                            self?.productDetailsModel = model
                            
                            if model.statusCode == 200{
                                if reloadFor == "" {
                                    if let price =  model.data?.productPrice?.price, !price.isEmpty {
                                        self?.productPrice = price.replacingOccurrences(of: "MMK", with: "").replacingOccurrences(of: " ", with: "")
                                    }else {
                                        self?.productPrice = (model.data?.productPrice?.price ?? "").replacingOccurrences(of: " ", with: "")
                                    }
                                    if model.data?.published ?? false {
                                        self?.btnPublish.setTitle("Hide".localized, for: .normal)
                                    }else {
                                        self?.btnPublish.setTitle("Show".localized, for: .normal)
                                    }
                                    let other = model.data?.otherLanguageDataUnicode?.replacingOccurrences(of: "\'", with: "\"")
                                    let dic = self?.convertStringToArrayOfDictionary(text: other ?? "")
                                    self?.productName = model.data?.name ?? ""

                                    self?.productUOM  = model.data?.uom ?? ""
                                   
                                    
                                    if let values = dic as? Dictionary<String,String> {
                                        self?.productNameUni = values["Name"] ?? ""
                                        self?.productDescription = values["ShortDescription"] ?? ""
                                        self?.productDescriptionInUnicode = values["FullDescription"] ?? ""
                                    }
                                    var categoryString = ""
                                    if let catArray = model.data?.breadcrumb?.categoryBreadcrumb {
                                        for (item, element) in catArray.enumerated() {
                                            
                                            if let name = element.name {
                                                categoryString =  categoryString + name
                                            }
                                            
                                            if item < catArray.count - 1 {
                                                categoryString = categoryString + " >> "
                                            }else {
                                                self?.categoryLastName = element.name ?? ""
                                            }
                                            
                                        }
                                        self?.categoryLastName = catArray.last?.name ?? ""
                                        self?.productCategoryID = catArray.last?.id ?? 0
                                        if categoryString != "" {
                                            self?.productCategory =  categoryString
                                        }
                                        else {
                                            self?.productCategory =  catArray.last?.name ?? ""
                                        }
                                        
                                    }
                                    
                                }
                                DispatchQueue.main.async {
                                    self?.tblProductDetails.reloadData()
                                }
                            }else {
                                println_debug("Product Attribute Value Not Found")
                            }
                        
 
                    }
                }catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
                    
                }
            }, onError: {  message in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                    self.lblLineSeparator.isHidden = true
                    self.stackOption.isHidden = true
                    self.tblProductDetails.reloadData()
                    self.alertVC = SAlertController()
                    self.alertVC?.ShowSAlert(title: "Error", withDescription: "Found error in get product details. Please try again", onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        self.navigationController?.popViewController(animated: true)
                    })
                    self.alertVC?.addAction(action: [YesAction])
                }
            })
        }
    }
    
      func convertStringToArrayOfDictionary(text: String) -> Any? {
            if let data = text.data(using: .utf8) {
                do {
                    return try JSONSerialization.jsonObject(with: data, options: []) as? Any
                } catch {
                    print(error.localizedDescription)
                }
            }
            return nil
        }
    
    func updateProductDetails(params: [String:Any], handler:@escaping (_ succes: Bool) -> Void) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/updateproductfromvendor", APIManagerClient.sharedInstance.base_url) as String
            print("Get allcategories :\(urlStr)\n params\(params)")
            
            self.aPIManager.postAPIParams(url: urlStr,productDetails: params, successBlock: { (dic,isSuccess) in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                    if isSuccess {
                        handler(true)
                    }else {
                        handler(false)
                    }
                }
            }, errorBlock: { (error) in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                }
                handler(false)
            })
        }
    }
    
    func publishAndUnPublishWebCall(productID:String) {
        var isPublish = ""
        if productDetailsModel?.data?.published ?? false {
            isPublish = "false"
        }else {
            isPublish = "true"
        }
        
        
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/UpdateProductStatus/%@/%@", APIManagerClient.sharedInstance.base_url,productID,isPublish) as String
            println_debug("Publish API : \(urlStr)")
            
            self.aPIManager.postGenericWebCall(urlString: urlStr, onSuccess: { data in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        return
                    }
                    do {
                        let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                        println_debug(json)
                        if let dic = json as? Dictionary<String,Any> {
                            DispatchQueue.main.async {
                              
                            if let statusCode = dic["StatusCode"] as? Int, statusCode == 200 {
                                var message = ""
                                if isPublish == "false" {
                                    message = "Your product has been successfully unpublished".localized
                                    self.btnPublish.setTitle("Show".localized, for: .normal)
                                }else {
                                    message = "Your product has been successfully published".localized
                                    self.btnPublish.setTitle("Hide".localized, for: .normal)
                                }
                                
                                
                                self.alertVC = SAlertController()
                                self.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: message, onController: self)
                                let YesAction = SAlertAction()
                                YesAction.action(name: "OK", AlertType: .defualt, withComplition: {
                                    self.getProductDetialsByID(productID: productID, reloadFor: "Publish_UnPublish")
                                })
                                self.alertVC?.addAction(action: [YesAction])
                            }else {
                                self.alertVC = SAlertController()
                                self.alertVC?.ShowSAlert(title: "Error", withDescription: "Found error while Publish / Un Publish. Please try again", onController: self)
                                let YesAction = SAlertAction()
                                YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                    
                                })
                                self.alertVC?.addAction(action: [YesAction])
                            }
                        }
                        }
                        
                    } catch {
                        
                    }
                }
            }, onError: { message in
                DispatchQueue.main.async {

                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                }
                self.alertVC = SAlertController()
                self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Found error while Publish / Un Publish. Please try again".localized, onController: self)
                let YesAction = SAlertAction()
                YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    
                })
                self.alertVC?.addAction(action: [YesAction])
                }
            })
        }
        
        
    }
    
    func showAlertInClass(title: String,subTitle: String) {
        self.alertVC = SAlertController()
        self.alertVC?.ShowSAlert(title: title, withDescription: subTitle, onController: self)
        let YesAction = SAlertAction()
        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
        })
        self.alertVC?.addAction(action: [YesAction])
    }

    
    func getAllCategories() {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/allcategories", APIManagerClient.sharedInstance.base_url) as String
            self.aPIManager.getGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        return
                    }
                    let decode = JSONDecoder()
                    self?.allCategoryModel = try? decode.decode(GetAllCategoriesModel.self, from: jsonData)
                }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
            })
        }
    }

}
