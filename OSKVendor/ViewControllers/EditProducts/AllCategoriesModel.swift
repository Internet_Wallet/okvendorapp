//
//  AllCategoriesModel.swift
//  OSKVendor
//
//  Created by Sam' MacBook on 18/09/20.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit

// MARK: - GetAllCategories
struct GetAllCategoriesModel: Codable {
    var data: [AllCategoriesData]?
    var count: Int?
    var successMessage: JSONNull?
    var statusCode: Int?
    var errorList: [JSONAny]?

    enum CodingKeys: String, CodingKey {
        case data = "Data"
        case count = "Count"
        case successMessage = "SuccessMessage"
        case statusCode = "StatusCode"
        case errorList = "ErrorList"
    }
}

// MARK: - Datum
struct AllCategoriesData: Codable {
    var parentCategoryID, displayOrder: Int?
    var iconPath: String?
    var datumExtension: JSONNull?
    var children: [JSONAny]?
    var id: Int?
    var name: String?
    var productCount: Int?

    enum CodingKeys: String, CodingKey {
        case parentCategoryID = "ParentCategoryId"
        case displayOrder = "DisplayOrder"
        case iconPath = "IconPath"
        case datumExtension = "Extension"
        case children = "Children"
        case id = "Id"
        case name = "Name"
        case productCount = "ProductCount"
    }
}



// MARK: - AddColorModel
struct AddColorAttributes: Codable {
    var productAttributeMappingID, attributeValueTypeID: Int
    var attributeValueTypeName: String?
    var associatedProductID: Int
    var associatedProductName: String?
    var name, colorSquaresRGB: String
    var displayColorSquaresRGB: Bool
    var imageSquaresPictureID: Int
    var displayImageSquaresPicture: Bool
    var priceAdjustment: Int
    var priceAdjustmentStr: String?
    var priceAdjustmentUsePercentage: Bool
    var weightAdjustment: Int
    var weightAdjustmentStr: String?
    var cost: Int
    var customerEntersQty: Bool
    var quantity: Int
    var isPreSelected: Bool
    var measurementData: String?
    var displayOrder, pictureID: Int
    var pictureThumbnailURL: String?
    var productPictureModels: [String]
    var locales: [Locale]
    var id: Int
    var customProperties: CustomPropertiesColor

    enum CodingKeys: String, CodingKey {
        case productAttributeMappingID = "ProductAttributeMappingId"
        case attributeValueTypeID = "AttributeValueTypeId"
        case attributeValueTypeName = "AttributeValueTypeName"
        case associatedProductID = "AssociatedProductId"
        case associatedProductName = "AssociatedProductName"
        case name = "Name"
        case colorSquaresRGB = "ColorSquaresRgb"
        case displayColorSquaresRGB = "DisplayColorSquaresRgb"
        case imageSquaresPictureID = "ImageSquaresPictureId"
        case displayImageSquaresPicture = "DisplayImageSquaresPicture"
        case priceAdjustment = "PriceAdjustment"
        case priceAdjustmentStr = "PriceAdjustmentStr"
        case priceAdjustmentUsePercentage = "PriceAdjustmentUsePercentage"
        case weightAdjustment = "WeightAdjustment"
        case weightAdjustmentStr = "WeightAdjustmentStr"
        case cost = "Cost"
        case customerEntersQty = "CustomerEntersQty"
        case quantity = "Quantity"
        case isPreSelected = "IsPreSelected"
        case measurementData = "MeasurementData"
        case displayOrder = "DisplayOrder"
        case pictureID = "PictureId"
        case pictureThumbnailURL = "PictureThumbnailUrl"
        case productPictureModels = "ProductPictureModels"
        case locales = "Locales"
        case id = "Id"
        case customProperties = "CustomProperties"
    }
}

// MARK: - CustomProperties
struct CustomPropertiesColor: Codable {
}

// MARK: - Locale
struct Locale: Codable {
    var languageID: Int
    var name: String?

    enum CodingKeys: String, CodingKey {
        case languageID = "LanguageId"
        case name = "Name"
    }
}

// MARK: - DeliveryTimeModel
struct DeliveryTimeModel: Codable {
    var data: [DeliveryTimeData]?
    var successMessage: JSONNull?
    var statusCode: Int?
    var errorList: [JSONAny]?

    enum CodingKeys: String, CodingKey {
        case data = "Data"
        case successMessage = "SuccessMessage"
        case statusCode = "StatusCode"
        case errorList = "ErrorList"
    }
}

// MARK: - Datum
struct DeliveryTimeData: Codable {
    var deliveryDateID, name: String?

    enum CodingKeys: String, CodingKey {
        case deliveryDateID = "DeliveryDateId"
        case name = "Name"
    }
}
