//
//  EditProductAttributs.swift
//  OSKVendor
//
//  Created by Sam' MacBook on 19/09/20.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Foundation
import UIKit
import EFColorPicker

protocol EditProductAttributsDelegate {
    func reloadAPIForGetNewColor()
}

class EditProductAttributs: UIViewController {
    var delegate: EditProductAttributsDelegate?
    var alertVC : SAlertController?
   // var model: GetPrdouctDetailsModel?
    var model: GetProductDetailsModel?
    let aPIManager = APIManager()
    var apiManagerClient : APIManagerClient?
    @IBOutlet var tvAttributes: UITableView!
    var viewColorPicker = UIView()
    var btnSelectColorPcker = UIButton()
    var btnSelectColorPckerCancel = UIButton()
    var colorPicker = EFColorSelectionViewController()
    var selectedcolor = UIColor()
    var colorList = [UIColor]()
    var isShowColorSelection = true
    var isShowSizeImage = true
    var sizeList = [String]()
    var iconImageURL: String?
    var sizeTtile = [String]()
    var sizeValue = [false,false,false,false,false,false,false]
    var isComingFromAppProduct = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedcolor = UIColor.white
        tvAttributes.transform = CGAffineTransform(scaleX: 1, y: -1)
        tvAttributes.tableFooterView = UIView()
        tvAttributes.bounces = false
        tvAttributes.showsHorizontalScrollIndicator = false
        tvAttributes.showsVerticalScrollIndicator = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if isComingFromAppProduct{
            fetchDataFromAddProduct(reloadFor: "First")
        }else{
            getProductDetialsByID(productID: String(model?.data?.id ?? 0) , reloadFor: "First")
        }
    }
    
    
    
    fileprivate func fetchDataFromAddProduct(reloadFor: String){
        if reloadFor == "First" {
            
                if let mode = model?.data?.productAttributes {
                    for item in mode {
                        let nameS = item.name?.split(separator: "-")
                        if nameS?.count ?? 0 > 1 {
                            if nameS?[1] == "Size" {
                                for size in item.values! {
                                    let nameSize = size.name?.split(separator: "-")
                                    if nameSize?.count ?? 0 > 1 {
                                        if nameSize![1] == "S" {
                                            self.sizeValue[0] = size.disabled ?? false
                                        }else if nameSize![1] == "M" {
                                            self.sizeValue[1] = size.disabled ?? false
                                        }else if nameSize![1] == "L" {
                                            self.sizeValue[2] = size.disabled ?? false
                                        }else if nameSize![1] == "XS" {
                                            self.sizeValue[3] = size.disabled ?? false
                                        }else if nameSize![1] == "XL" {
                                            self.sizeValue[4] = size.disabled ?? false
                                        }else if nameSize![1] ==  "XXL" {
                                            self.sizeValue[5] = size.disabled ?? false
                                        }else if nameSize![1] == "XXXL" {
                                            self.sizeValue[6] = size.disabled ?? false
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            self.tvAttributes.reloadData()
            
        }else if reloadFor == "Color" {
            if self.model?.data?.productAttributes?[0].name == "Color" {
                self.tvAttributes.reloadSections([1], with: .none)
            }
        }else if reloadFor == "Size" {
            
            if let mode = model?.data?.productAttributes {
                for item in mode {
                    let nameS = item.name?.split(separator: "-")
                    if nameS?.count ?? 0 > 1 {
                        if nameS?[1] == "Size" {
                            for size in item.values! {
                                let nameSize = size.name?.split(separator: "-")
                                if nameSize?.count ?? 0 > 1 {
                                    if nameSize![1] == "S" {
                                        self.sizeValue[0] = size.disabled ?? false
                                    }else if nameSize![1] == "M" {
                                        self.sizeValue[1] = size.disabled ?? false
                                    }else if nameSize![1] == "L" {
                                        self.sizeValue[2] = size.disabled ?? false
                                    }else if nameSize![1] == "XS" {
                                        self.sizeValue[3] = size.disabled ?? false
                                    }else if nameSize![1] == "XL" {
                                        self.sizeValue[4] = size.disabled ?? false
                                    }else if nameSize![1] ==  "XXL" {
                                        self.sizeValue[5] = size.disabled ?? false
                                    }else if nameSize![1] == "XXXL" {
                                        self.sizeValue[6] = size.disabled ?? false
                                    }
                                }
                            }
                        }
                    }
                }
            }
            self.tvAttributes.reloadRows(at: [IndexPath(row: 1, section: 2)], with: .none)
        }

        
    }
    
    
    @objc func onClickHeaderActon(_ button: UIButton) {
        if button.tag == 10 {
        }else  if button.tag == 20 {
        }else if button.tag == 30 {
        }else if button.tag == 40 {
            self.view.removeFromSuperview()
        }
    }
    
    
    @objc func onClickColorPickerAciton(_ button: UIButton) {
        if button.tag == 10 {
            if model?.data?.productAttributes?[0].name == "Color" {
                let color =  self.selectedcolor.toHexString()
                let id = model?.data?.productAttributes?[0].id ?? 0
                self.addNewColorAPI(haxColor: "#\(color)", colorID: id, handler: {(isSuccress) in
                    if isSuccress {
                        self.getProductDetialsByID(productID: String(self.model?.data?.id ?? 0) , reloadFor: "Color")
                    }
                })
            }
        }else {
            // cancel pressed
        }
        
        if #available(iOS 11.0, *) {
            if self.viewColorPicker.contains(self.viewColorPicker) {
                self.colorPicker.view.removeFromSuperview()
                self.viewColorPicker.removeFromSuperview()
            }
        } else {
            // Fallback on earlier versions
        }
    }

    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    @objc func onClickColorAction(_ button: UIButton) {
 
        self.viewColorPicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:self.view.frame.size.height)
        viewColorPicker.backgroundColor = .white
        
        colorPicker.delegate = self
        colorPicker.view.frame = CGRect(x: 10, y: 70, width: self.viewColorPicker.frame.size.width - 20, height:self.viewColorPicker.frame.size.height - 80)
        colorPicker.view.backgroundColor = .white
        colorPicker.setMode(mode: .hsb)
        self.addChild(colorPicker)
        colorPicker.didMove(toParent: self)
        self.viewColorPicker.addSubview(colorPicker.view)
        
        btnSelectColorPcker.frame = CGRect(x: 10, y: colorPicker.view.frame.origin.x + colorPicker.view.frame.size.height, width: ((self.viewColorPicker.frame.size.width - 20 ) / 2), height: 50)
        btnSelectColorPcker.tag = 10
        btnSelectColorPcker.backgroundColor = #colorLiteral(red: 0, green: 0.7320501804, blue: 0.365727663, alpha: 1)
        btnSelectColorPcker.layer.cornerRadius = 5
        btnSelectColorPcker.layer.masksToBounds = true
        btnSelectColorPcker.setTitle("Done".localized, for: .normal)
        btnSelectColorPcker.setTitleColor(.white, for: .normal)
        btnSelectColorPcker.addTarget(self, action: #selector(onClickColorPickerAciton(_:)), for: .touchUpInside)
        self.viewColorPicker.addSubview(btnSelectColorPcker)
        
        btnSelectColorPckerCancel.frame = CGRect(x: btnSelectColorPcker.frame.origin.x + btnSelectColorPcker.frame.size.width + 5, y: colorPicker.view.frame.origin.x + colorPicker.view.frame.size.height, width: ((self.viewColorPicker.frame.size.width - 25 ) / 2), height: 50)
        btnSelectColorPckerCancel.tag = 20
        btnSelectColorPckerCancel.backgroundColor = #colorLiteral(red: 0, green: 0.7320501804, blue: 0.365727663, alpha: 1)
        btnSelectColorPckerCancel.layer.cornerRadius = 5
        btnSelectColorPckerCancel.layer.masksToBounds = true
        btnSelectColorPckerCancel.setTitle("Cancel".localized, for: .normal)
        btnSelectColorPckerCancel.setTitleColor(.white, for: .normal)
        btnSelectColorPckerCancel.addTarget(self, action: #selector(onClickColorPickerAciton(_:)), for: .touchUpInside)
        self.viewColorPicker.addSubview(btnSelectColorPckerCancel)
        self.view.addSubview(self.viewColorPicker)
    }
    
    @objc func onClickSizeAction(_ button: UIButton) {
        button.isSelected = !button.isSelected
        if button.isSelected {
            isShowSizeImage = false
        }else {
            isShowSizeImage = true
        }
        tvAttributes.reloadRows(at: [IndexPath(row: 1, section: 3)], with: .none)
    }
    
    @objc func onClicksizeOption(_ button: UIButton) {
        button.isSelected = !button.isSelected
        var isSelected = false
        if button.isSelected {
            isSelected = true
        }
        var title = ""
        if button.tag == 10 {
            title = "S"
        }else if button.tag == 20 {
            title = "M"
        }else if button.tag == 30 {
            title = "L"
        }else if button.tag == 40 {
            title = "XS"
        }else if button.tag == 50 {
            title = "XL"
        }else if button.tag == 60 {
            title = "XXL"
        }else if button.tag == 70 {
            title = "XXXL"
        }
        if let attributes = model?.data?.productAttributes {
            for item in attributes {
                let nameS = item.name?.split(separator: "-")
                if nameS?.count ?? 0 > 1 {
                    if nameS?[1] == "Size" {
                        for size in item.values! {
                            let nameSize = size.name?.split(separator: "-")
                            if nameSize?.count ?? 0 > 1 {
                                if nameSize![1] == title {
                                    self.updateSizeAPI(dic: size, disable: isSelected, handler: { isSuccess in
                                        if isSuccess {
                                            self.getProductDetialsByID(productID: String(self.model?.data?.id ?? 0), reloadFor: "Size")
                                        }else {
                                            button.isSelected = !button.isSelected
                                        }
                                    })
                                }
                            }
                    
                        }
                    }
                }
            }
        }

    }
    
    func addNewColorAPI(haxColor: String,colorID: Int, handler: @escaping (_ success: Bool) -> Void) {
        
        let dic1 = ["LanguageId": 1,"Name": ""] as [String : Any]
        let dic2 = ["LanguageId": 1,"Name": ""] as [String : Any]
        let dic3 = ["LanguageId": 1,"Name": ""] as [String : Any]
        let local =  [dic1,dic2,dic3]
        let picModel = [Any]()
        let customPro = Dictionary<String,Any>()
        let dic = [
            "ProductAttributeMappingId": colorID,
            "AttributeValueTypeId": 0,
            "AttributeValueTypeName": "",
            "AssociatedProductId": 0,
            "AssociatedProductName": "",
            "Name": "ANyNameValue",
            "ColorSquaresRgb": haxColor,
            "DisplayColorSquaresRgb": false,
            "ImageSquaresPictureId": 0,
            "DisplayImageSquaresPicture": false,
            "PriceAdjustment": 0.0000,
            "PriceAdjustmentStr": "",
            "PriceAdjustmentUsePercentage": false,
            "WeightAdjustment": 0.0000,
            "WeightAdjustmentStr": "",
            "Cost": 0.0000,
            "CustomerEntersQty": false,
            "Quantity": 1,
            "IsPreSelected": false,
            "MeasurementData": "",
            "DisplayOrder": 0,
            "PictureId": 0,
            "PictureThumbnailUrl": "",
            "ProductPictureModels": picModel,
            "Locales":local,
            "Id": 0,
            "CustomProperties": customPro
        ] as [String : Any]
        
        let prodID = String(model?.data?.id ?? 0)
        //addvaluetoproductattributevalues
        let urlStr = String(format: "%@/vendor/addvaluetoproductattribute/%@", APIManagerClient.sharedInstance.base_url, prodID) as String
        print("Get Product by ID :\(urlStr) ")
        
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            self.aPIManager.webCallWithbodyParmas(urlStr: urlStr, params: dic, onSuccess: { data in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                        AppUtility.hideLoading(viewLoc)
                        self.alertVC = SAlertController()
                        self.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "Product color added successfully", onController: self)
                        let YesAction = SAlertAction()
                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                            handler(true)
                        })
                        self.alertVC?.addAction(action: [YesAction])
                    }
                }
            }, onError:  {error in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                        AppUtility.hideLoading(viewLoc)
                        self.alertVC = SAlertController()
                        self.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "Adding color is failed. Please try agian", onController: self)
                        let YesAction = SAlertAction()
                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                            handler(false)
                        })
                        self.alertVC?.addAction(action: [YesAction])
                    }
                }
            })
        }
    }
    
    
    func updateSizeAPI(dic: Tushar_Values, disable:Bool , handler: @escaping (_ success: Bool) -> Void) {
        
        let dic1 = ["LanguageId": 1,"Name": ""] as [String : Any]
        let dic2 = ["LanguageId": 1,"Name": ""] as [String : Any]
        let dic3 = ["LanguageId": 1,"Name": ""] as [String : Any]
        let local =  [dic1,dic2,dic3]
        let picModel = [Any]()
        let attributID = String(dic.id ?? 0)
        let customPro = Dictionary<String,Any>()
        let params = [
            "ProductAttributeMappingId": dic.id ?? "",
            "AttributeValueTypeId": 0,
            "AttributeValueTypeName": "",
            "AssociatedProductId": 0,
            "AssociatedProductName": "",
            "Name": dic.name ?? "",
            "ColorSquaresRgb": "",
            "DisplayColorSquaresRgb": false,
            "ImageSquaresPictureId": 0,
            "DisplayImageSquaresPicture": false,
            "PriceAdjustment": 0.0000,
            "PriceAdjustmentStr": "",
            "PriceAdjustmentUsePercentage": false,
            "WeightAdjustment": 0.0000,
            "WeightAdjustmentStr": "",
            "Cost": 0.0000,
            "CustomerEntersQty": false,
            "Quantity": 1,
            "IsPreSelected": false,
            "Disabled": disable,
            "MeasurementData": "",
            "DisplayOrder": 0,
            "PictureId": 0,
            "PictureThumbnailUrl": "",
            "ProductPictureModels": picModel,
            "Locales":local,
            "Id": 0,
            "CustomProperties": customPro
        ] as [String : Any]
        
        let urlStr = String(format: "%@/vendor/editproductattributevalue/%@", APIManagerClient.sharedInstance.base_url, attributID) as String
        print("Get Product by ID :\(urlStr) ")
        
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            self.aPIManager.webCallWithbodyParmas(urlStr: urlStr, params: params, onSuccess: { data in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                        AppUtility.hideLoading(viewLoc)
                        self.alertVC = SAlertController()
                        self.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "Product Size Updated successfully".localized, onController: self)
                        let YesAction = SAlertAction()
                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                            handler(true)
                        })
                        self.alertVC?.addAction(action: [YesAction])
                    }
                }
            }, onError:  {error in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                        AppUtility.hideLoading(viewLoc)
                        self.alertVC = SAlertController()
                        self.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "Product Size Updation is failed. Please try agian".localized, onController: self)
                        let YesAction = SAlertAction()
                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                            handler(false)
                        })
                        self.alertVC?.addAction(action: [YesAction])
                    }
                }
            })
        }
    }
    
    
    func updatColorAPI(dic: Value, disable:Bool , handler: @escaping (_ success: Bool) -> Void) {
        
        let dic1 = ["LanguageId": 1,"Name": ""] as [String : Any]
        let dic2 = ["LanguageId": 1,"Name": ""] as [String : Any]
        let dic3 = ["LanguageId": 1,"Name": ""] as [String : Any]
        let local =  [dic1,dic2,dic3]
        let picModel = [Any]()
        let attributID = String(dic.id ?? 0)
        let customPro = Dictionary<String,Any>()
        let params = [
            "ProductAttributeMappingId": dic.id ?? "",
            "AttributeValueTypeId": 0,
            "AttributeValueTypeName": "",
            "AssociatedProductId": 0,
            "AssociatedProductName": "",
            "Name": dic.name ?? "",
            "ColorSquaresRgb": dic.colorSquaresRGB ?? "#fffff",
            "DisplayColorSquaresRgb": false,
            "ImageSquaresPictureId": 0,
            "DisplayImageSquaresPicture": false,
            "PriceAdjustment": 0.0000,
            "PriceAdjustmentStr": "",
            "PriceAdjustmentUsePercentage": false,
            "WeightAdjustment": 0.0000,
            "WeightAdjustmentStr": "",
            "Cost": 0.0000,
            "CustomerEntersQty": false,
            "Quantity": 1,
            "IsPreSelected": false,
            "Disabled": disable,
            "MeasurementData": "",
            "DisplayOrder": 0,
            "PictureId": 0,
            "PictureThumbnailUrl": "",
            "ProductPictureModels": picModel,
            "Locales":local,
            "Id": 0,
            "CustomProperties": customPro
        ] as [String : Any]
        
        let urlStr = String(format: "%@/vendor/editproductattributevalue/%@", APIManagerClient.sharedInstance.base_url, attributID) as String
        print("Get Product by ID :\(urlStr) ")
        
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            self.aPIManager.webCallWithbodyParmas(urlStr: urlStr, params: params, onSuccess: { data in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                        AppUtility.hideLoading(viewLoc)
                        self.alertVC = SAlertController()
                        self.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "Product color Updated successfully".localized, onController: self)
                        let YesAction = SAlertAction()
                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                            handler(true)
                        })
                        self.alertVC?.addAction(action: [YesAction])
                    }
                }
            }, onError:  {error in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                        AppUtility.hideLoading(viewLoc)
                        self.alertVC = SAlertController()
                        self.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "Product color Updation is failed. Please try agian".localized, onController: self)
                        let YesAction = SAlertAction()
                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                            handler(false)
                        })
                        self.alertVC?.addAction(action: [YesAction])
                    }
                }
            })
        }
    }
    
    func getProductDetialsByID(productID: String, reloadFor: String) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/getproductbyid/%@", APIManagerClient.sharedInstance.base_url,productID.trimmingCharacters(in: .whitespaces)) as String
            print("Get Product by ID :\(urlStr) ")
            self.aPIManager.getGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        println_debug("Data error")
                        return
                    }
                    do {
                        let decode = JSONDecoder()
                        let model = try? decode.decode(GetPrdouctDetailsModel.self, from: jsonData)
                        if model?.statusCode == 200 {
                            println_debug("Product Attribute Value success")
                            self?.model = nil
                         //   self?.model = model
                            if reloadFor == "First" {
                                
                                    if let mode = model?.data?.productAttributes {
                                        for item in mode {
                                            let nameS = item.name?.split(separator: "-")
                                            if nameS?.count ?? 0 > 1 {
                                                if nameS?[1] == "Size" {
                                                    for size in item.values! {
                                                        let nameSize = size.name?.split(separator: "-")
                                                        if nameSize?.count ?? 0 > 1 {
                                                            if nameSize![1] == "S" {
                                                                self?.sizeValue[0] = size.disable ?? false
                                                            }else if nameSize![1] == "M" {
                                                                self?.sizeValue[1] = size.disable ?? false
                                                            }else if nameSize![1] == "L" {
                                                                self?.sizeValue[2] = size.disable ?? false
                                                            }else if nameSize![1] == "XS" {
                                                                self?.sizeValue[3] = size.disable ?? false
                                                            }else if nameSize![1] == "XL" {
                                                                self?.sizeValue[4] = size.disable ?? false
                                                            }else if nameSize![1] ==  "XXL" {
                                                                self?.sizeValue[5] = size.disable ?? false
                                                            }else if nameSize![1] == "XXXL" {
                                                                self?.sizeValue[6] = size.disable ?? false
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                self?.tvAttributes.reloadData()
                                
                            }else if reloadFor == "Color" {
                                if self?.model?.data?.productAttributes?[0].name == "Color" {
                                    self?.tvAttributes.reloadSections([1], with: .none)
                                }
                            }else if reloadFor == "Size" {
                                
                                if let mode = model?.data?.productAttributes {
                                    for item in mode {
                                        let nameS = item.name?.split(separator: "-")
                                        if nameS?.count ?? 0 > 1 {
                                            if nameS?[1] == "Size" {
                                                for size in item.values! {
                                                    let nameSize = size.name?.split(separator: "-")
                                                    if nameSize?.count ?? 0 > 1 {
                                                        if nameSize![1] == "S" {
                                                            self?.sizeValue[0] = size.disable ?? false
                                                        }else if nameSize![1] == "M" {
                                                            self?.sizeValue[1] = size.disable ?? false
                                                        }else if nameSize![1] == "L" {
                                                            self?.sizeValue[2] = size.disable ?? false
                                                        }else if nameSize![1] == "XS" {
                                                            self?.sizeValue[3] = size.disable ?? false
                                                        }else if nameSize![1] == "XL" {
                                                            self?.sizeValue[4] = size.disable ?? false
                                                        }else if nameSize![1] ==  "XXL" {
                                                            self?.sizeValue[5] = size.disable ?? false
                                                        }else if nameSize![1] == "XXXL" {
                                                            self?.sizeValue[6] = size.disable ?? false
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                self?.tvAttributes.reloadRows(at: [IndexPath(row: 1, section: 2)], with: .none)
                            }
                            }else {
                            println_debug("Product Attribute Value Not Found")
                        }
                    }
                }
            }, onError: {  message in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                    self.alertVC = SAlertController()
                    self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Found error in get product details. Please try again".localized, onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        
                    })
                    self.alertVC?.addAction(action: [YesAction])
                }
            })
        }
    }
    
    
}



extension EditProductAttributs : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return sizeList.count
        }else if section == 1 {
            return 1
        }else if section == 2 {
            return 2
        }else if section == 3 {
            return 2
        }else if section == 4 {
            return 1
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProductAttributesSizeDetailsCell", for: indexPath) as! EditProductAttributesSizeDetailsCell
            cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
            cell.lblOne.text = sizeList[indexPath.row]
            cell.lblTwo.text = sizeList[indexPath.row]
            cell.lblThree.text = sizeList[indexPath.row]
            cell.lblFour.text = sizeList[indexPath.row]
            let title = sizeList[indexPath.row]
            if title == "XS" {
                cell.btnOne.setTitle("92 cm", for: .normal)
                cell.btnTwo.setTitle("43.4 cm", for: .normal)
                cell.btnThree.setTitle("83 cm", for: .normal)
                cell.btnFour.setTitle("76 cm", for: .normal)
            }else if title == "S" {
                cell.btnOne.setTitle("96 cm", for: .normal)
                cell.btnTwo.setTitle("44.6 cm", for: .normal)
                cell.btnThree.setTitle("85 cm", for: .normal)
                cell.btnFour.setTitle("80 cm", for: .normal)
            }else if title == "M" {
                cell.btnOne.setTitle("100 cm", for: .normal)
                cell.btnTwo.setTitle("45.8 cm", for: .normal)
                cell.btnThree.setTitle("87 cm", for: .normal)
                cell.btnFour.setTitle("84 cm", for: .normal)
            }else if title == "L" {
                cell.btnOne.setTitle("104 cm", for: .normal)
                cell.btnTwo.setTitle("47 cm", for: .normal)
                cell.btnThree.setTitle("89 cm", for: .normal)
                cell.btnFour.setTitle("88 cm", for: .normal)
            }else if title == "XL" {
                cell.btnOne.setTitle("108 cm", for: .normal)
                cell.btnTwo.setTitle("48.2 cm", for: .normal)
                cell.btnThree.setTitle("91 cm", for: .normal)
                cell.btnFour.setTitle("92 cm", for: .normal)
            }else if title == "XXL" {
                cell.btnOne.setTitle("112 cm", for: .normal)
                cell.btnTwo.setTitle("49.4 cm", for: .normal)
                cell.btnThree.setTitle("93 cm", for: .normal)
                cell.btnFour.setTitle("96 cm", for: .normal)
            }else if title == "XXXL" {
                cell.btnOne.setTitle("116 cm", for: .normal)
                cell.btnTwo.setTitle("50.6 cm", for: .normal)
                cell.btnThree.setTitle("94 cm", for: .normal)
                cell.btnFour.setTitle("100 cm", for: .normal)
            }
            
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProductAttributesColorCell", for: indexPath) as! EditProductAttributesColorCell
            cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
            cell.delegate = self
            cell.selectionStyle = .none
         //   cell.loadImage(model: model ?? GetPrdouctDetailsModel())
            return cell
        }else if indexPath.section == 2 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EditProductAttributesSizeOptionCell", for: indexPath) as! EditProductAttributesSizeOptionCell
                cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
                cell.selectionStyle = .none
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EditProductAttributesSizeCell", for: indexPath) as! EditProductAttributesSizeCell
                cell.btnS.addTarget(self, action: #selector(onClicksizeOption(_:)), for: .touchUpInside)
                cell.btnM.addTarget(self, action: #selector(onClicksizeOption(_:)), for: .touchUpInside)
                cell.btnL.addTarget(self, action: #selector(onClicksizeOption(_:)), for: .touchUpInside)
                cell.btnXS.addTarget(self, action: #selector(onClicksizeOption(_:)), for: .touchUpInside)
                cell.btnXL.addTarget(self, action: #selector(onClicksizeOption(_:)), for: .touchUpInside)
                cell.btnXXL.addTarget(self, action: #selector(onClicksizeOption(_:)), for: .touchUpInside)
                cell.btnXXXL.addTarget(self, action: #selector(onClicksizeOption(_:)), for: .touchUpInside)
                cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
                cell.selectionStyle = .none
                cell.wrapData(model: sizeValue)
                return cell}
        }else if indexPath.section == 3 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EditProductAttributesColorSizeOptionCell", for: indexPath) as! EditProductAttributesColorSizeOptionCell
                cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
                cell.btnColor.addTarget(self, action: #selector(onClickColorAction(_:)), for: .touchUpInside)
                cell.btnSize.addTarget(self, action: #selector(onClickSizeAction(_:)), for: .touchUpInside)
                cell.selectionStyle = .none
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EditProductAttributesImagesCell", for: indexPath) as! EditProductAttributesImagesCell
                cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
                
                if let str = iconImageURL {
                    cell.loadImage(images: [str])
                }
                cell.selectionStyle = .none
                return cell
            }
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProductAttributesHeader", for: indexPath) as! EditProductAttributesHeader
            cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
            cell.selectionStyle = .none
            cell.btnAttriutes.addTarget(self, action: #selector(onClickHeaderActon(_:)), for: .touchUpInside)
            cell.btnAddColor.addTarget(self, action: #selector(onClickHeaderActon(_:)), for: .touchUpInside)
            cell.btnSizeGuide.addTarget(self, action: #selector(onClickHeaderActon(_:)), for: .touchUpInside)
            cell.btnClose.addTarget(self, action: #selector(onClickHeaderActon(_:)), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if sizeList.count == 0 {
                return 0
            }else {
                return 55
            }
        }else if indexPath.section == 1 {
            if let colorList = model?.data?.productAttributes?[0] {
                if colorList.values?.count == 0 {
                    return 0
                }else {
                    return 110
                }
            }else {
                return 0
            }
        }else if indexPath.section == 2 {
            if indexPath.row == 0 {
                if sizeList.count == 0 {
                    return 0
                }else {
                    return 50
                }
            }else {
                return 190
            }
        }else if indexPath.section == 3 {
            if indexPath.row == 0 {
                return 60
            }else {
                if isShowSizeImage {
                    return 0
                }else {
                    return 0
                }
            }
        }else if indexPath.section == 4 {
            return 60
        }else {
            return 0
        }
       // return 10
    }
    
}

extension EditProductAttributs: EFColorSelectionViewControllerDelegate,EditProductAttributesColorCellDelegate {
    func applySelectedColor(dic: Value, isDisable: Bool) {
        self.updatColorAPI(dic: dic, disable: isDisable, handler: {isSuccess in
            if isSuccess {
                self.getProductDetialsByID(productID: String(self.model?.data?.id ?? 0), reloadFor: "Color")
            }
        })
    }
    
    func colorViewController(_ colorViewCntroller: EFColorSelectionViewController, didChangeColor color: UIColor) {
        self.selectedcolor = color
    }

}




class EditProductAttributesHeader: UITableViewCell {
    @IBOutlet var btnAttriutes: UIButton!{
        didSet{
            btnAttriutes.setTitle("Attributes".localized, for: .normal)
        }
    }
    @IBOutlet var btnAddColor: UIButton!{
        didSet{
            btnAddColor.setTitle("Add Color".localized, for: .normal)
        }
    }
    @IBOutlet var btnSizeGuide: UIButton!{
        didSet{
            btnSizeGuide.setTitle("Size Guide".localized, for: .normal)
        }
    }
    @IBOutlet var btnClose: UIButton!{
        didSet{
            btnClose.setTitle("Close".localized, for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnAttriutes.titleLabel?.textColor = .darkGray
        btnAddColor.titleLabel?.textColor = #colorLiteral(red: 1, green: 0.8769344687, blue: 0.4786319137, alpha: 1)
        btnSizeGuide.titleLabel?.textColor = #colorLiteral(red: 1, green: 0.8769344687, blue: 0.4786319137, alpha: 1)
        btnClose.titleLabel?.textColor = #colorLiteral(red: 1, green: 0.8769344687, blue: 0.4786319137, alpha: 1)
    }
    
}

class EditProductAttributesImagesCell: UITableViewCell {
    @IBOutlet var viewContainer: UIView!
    var scrollImageContainer =  UIScrollView()
    var currentPage = 1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewContainer.layer.cornerRadius = 5.0
        viewContainer.layer.masksToBounds = true
        viewContainer.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        viewContainer.layer.borderWidth = 1.0
        viewContainer.backgroundColor = .white
    }
    
    func loadImage(images: [String]) {
        scrollImageContainer.frame = CGRect(x: 0, y: 0, width: viewContainer.frame.size.width, height: 215)
        // scrollImageContainer.delegate = self
        scrollImageContainer.isPagingEnabled = true
        scrollImageContainer.showsHorizontalScrollIndicator = false
        scrollImageContainer.contentSize = CGSize(width: (viewContainer.frame.size.width * CGFloat(images.count)), height: 215)
        viewContainer.addSubview(scrollImageContainer)
        var xAxis : CGFloat = 0
        
        for (item, element) in images.enumerated() {
            let viewImageContainer = UIView()
            let imageProduct = UIImageView()
            xAxis = CGFloat(item) * viewContainer.frame.size.width
            viewImageContainer.frame = CGRect(x: xAxis, y: 0, width: viewContainer.frame.size.width , height: 215)
            scrollImageContainer.addSubview(viewImageContainer)
            
            imageProduct.frame = CGRect(x: 0, y: 0, width: scrollImageContainer.frame.size.width, height:  scrollImageContainer.frame.size.height)
            imageProduct.sd_setImage(with: URL(string: element), completed: nil)
            imageProduct.contentMode = .scaleAspectFit
            viewImageContainer.addSubview(imageProduct)
        }
    }
    
}

class EditProductAttributesColorSizeOptionCell: UITableViewCell {
    @IBOutlet var btnColor: UIButton!{
        didSet{
            btnColor.setTitle("Color".localized, for: .normal)
        }
    }
    @IBOutlet var btnSize: UIButton!{
        didSet{
            btnSize.setTitle("Size Guide".localized, for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnColor.layer.cornerRadius = 5.0
        btnColor.layer.masksToBounds = true
        
        btnSize.layer.cornerRadius = 5.0
        btnSize.layer.masksToBounds = true
        
    }
    
}


class EditProductAttributesColorCell: UITableViewCell,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var cvColor: UICollectionView!
    var delegate: EditProductAttributesColorCellDelegate?
    var colorList = [Value]()
    override func awakeFromNib() {
        super.awakeFromNib()
        cvColor.delegate = self
        cvColor.dataSource = self
    }
    
    func loadImage(model: GetPrdouctDetailsModel) {
        
//        if let color = model.data?.productAttributes?[0] {
//            colorList = color.values!
//        }
//        cvColor.reloadData()
    }
    
    
    @objc func onClickColorAction(_ button: UIButton) {
        button.isSelected = !button.isSelected
        if let del = delegate {
            del.applySelectedColor(dic: colorList[button.tag], isDisable: button.isSelected)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colorList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "attributColorCollectionCell", for: indexPath) as! attributColorCollectionCell
        let data = colorList[indexPath.row]
        cell.lblColor.backgroundColor = UIColor.init(hex: data.colorSquaresRGB ?? "")
        cell.lblColorName.text = data.name
        cell.btnSelectedColor.isSelected = data.disable ?? false
        cell.btnSelectedColor.tag = indexPath.row
        cell.btnSelectedColor.addTarget(self, action: #selector(onClickColorAction(_:)), for: .touchUpInside)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if colorList.count > 0 {
            return CGSize(width: 80, height: 60)
        }else {
            return CGSize(width: 0, height: 0)
        }
      
    }
    
}

class attributColorCollectionCell: UICollectionViewCell {
    
    @IBOutlet var btnSelectedColor : UIButton!
    @IBOutlet var lblColorName : UILabel!
    @IBOutlet var lblColor : UILabel!
    
    override func awakeFromNib() {
        
    }
}

protocol EditProductAttributesColorCellDelegate {
    func applySelectedColor(dic: Value, isDisable: Bool)
}

class EditProductAttributesSizeCell: UITableViewCell {
    @IBOutlet var btnS: UIButton!
    @IBOutlet var btnM: UIButton!
    @IBOutlet var btnL: UIButton!
    @IBOutlet var btnXS: UIButton!
    @IBOutlet var btnXL: UIButton!
    @IBOutlet var btnXXL: UIButton!
    @IBOutlet var btnXXXL: UIButton!
    
    @IBOutlet var lblS: UILabel!
    @IBOutlet var lblM: UILabel!
    @IBOutlet var lblL: UILabel!
    @IBOutlet var lblXS: UILabel!
    @IBOutlet var lblXL: UILabel!
    @IBOutlet var lblXXL: UILabel!
    @IBOutlet var lblXXXL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnS.tag = 10
        btnM.tag = 20
        btnL.tag = 30
        btnXS.tag = 40
        btnXL.tag = 50
        btnXXL.tag = 60
        btnXXXL.tag = 70
        
        btnS.setImage(UIImage(named: "check"), for: .selected)
        btnS.setImage(UIImage(named: "uncheck"), for: .normal)
        
        btnM.setImage(UIImage(named: "check"), for: .selected)
        btnM.setImage(UIImage(named: "uncheck"), for: .normal)
        
        btnL.setImage(UIImage(named: "check"), for: .selected)
        btnL.setImage(UIImage(named: "uncheck"), for: .normal)
        
        btnXS.setImage(UIImage(named: "check"), for: .selected)
        btnXS.setImage(UIImage(named: "uncheck"), for: .normal)
        
        btnXL.setImage(UIImage(named: "check"), for: .selected)
        btnXL.setImage(UIImage(named: "uncheck"), for: .normal)
        
        btnXXL.setImage(UIImage(named: "check"), for: .selected)
        btnXXL.setImage(UIImage(named: "uncheck"), for: .normal)
        
        btnXXXL.setImage(UIImage(named: "check"), for: .selected)
        btnXXXL.setImage(UIImage(named: "uncheck"), for: .normal)
        
    }
    
    func wrapData(model: [Bool]) {
        self.btnS.isSelected = model[0]
        self.btnM.isSelected = model[1]
        self.btnL.isSelected = model[2]
        self.btnXS.isSelected = model[3]
        self.btnXL.isSelected = model[4]
        self.btnXXL.isSelected = model[5]
        self.btnXXXL.isSelected = model[6]
    }
}


class EditProductAttributesSizeOptionCell: UITableViewCell {
    @IBOutlet var btnBust: UIButton!{
        didSet{
            btnBust.setTitle("Bust".localized, for: .normal)
        }
    }
    @IBOutlet var btnShoulder: UIButton!{
        didSet{
            btnShoulder.setTitle("Shoulder".localized, for: .normal)
        }
    }
    @IBOutlet var btnSleevs: UIButton!{
        didSet{
            btnSleevs.setTitle("Sleeve".localized, for: .normal)
        }
    }
    @IBOutlet var btnWaist: UIButton!{
        didSet{
            btnWaist.setTitle("Waist".localized, for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnBust.layer.cornerRadius = 5.0
        btnBust.layer.masksToBounds = true
        
        btnShoulder.layer.cornerRadius = 5.0
        btnShoulder.layer.masksToBounds = true
        
        btnSleevs.layer.cornerRadius = 5.0
        btnSleevs.layer.masksToBounds = true
        
        btnWaist.layer.cornerRadius = 5.0
        btnWaist.layer.masksToBounds = true
        
    }
}

class EditProductAttributesSizeDetailsCell: UITableViewCell {
    @IBOutlet var lblOne: UILabel!
    @IBOutlet var lblTwo: UILabel!
    @IBOutlet var lblThree: UILabel!
    @IBOutlet var lblFour: UILabel!
    
    @IBOutlet var btnOne: UIButton!
    @IBOutlet var btnTwo: UIButton!
    @IBOutlet var btnThree: UIButton!
    @IBOutlet var btnFour: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnOne.layer.cornerRadius = 5.0
        btnOne.layer.borderColor = #colorLiteral(red: 0, green: 0.7320501804, blue: 0.365727663, alpha: 1)
        btnOne.layer.borderWidth = 1.0
        
        btnTwo.layer.cornerRadius = 5.0
        btnTwo.layer.borderColor = #colorLiteral(red: 0, green: 0.7320501804, blue: 0.365727663, alpha: 1)
        btnTwo.layer.borderWidth = 1.0
        
        btnThree.layer.cornerRadius = 5.0
        btnThree.layer.borderColor = #colorLiteral(red: 0, green: 0.7320501804, blue: 0.365727663, alpha: 1)
        btnThree.layer.borderWidth = 1.0
        
        btnFour.layer.cornerRadius = 5.0
        btnFour.layer.borderColor = #colorLiteral(red: 0, green: 0.7320501804, blue: 0.365727663, alpha: 1)
        btnFour.layer.borderWidth = 1.0
    }
}


extension UIColor {
    
    public convenience init?(rgbaString : String){
        self.init(ciColor: CIColor(string: rgbaString))
    }
    //Convert UIColor to RGBA String
    func toRGBAString()-> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        return "\(r) \(g) \(b) \(a)"
        
    }
    //return UIColor from Hexadecimal Color string
    public convenience init?(hexaDecimalString: String) {
        
        let r, g, b, a: CGFloat
        
        if hexaDecimalString.hasPrefix("#") {
            let start = hexaDecimalString.index(hexaDecimalString.startIndex, offsetBy: 1)
            let hexColor = hexaDecimalString.substring(from: start)
            
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
    // Convert UIColor to Hexadecimal String
    func toHexString() -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        return String(
            format: "%02X%02X%02X",
            Int(r * 0xff),
            Int(g * 0xff),
            Int(b * 0xff)
        )
    }
}
