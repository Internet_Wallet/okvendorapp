//
//  SideMenuViewController.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 29/06/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class SideMenuViewController: OSKBaseViewController {

    
     var ListArray: [String]?
     var aPIManager = APIManager()
     var apiManagerClient: APIManagerClient!
      
     
     fileprivate let cellOne = "UserViewCell"
     fileprivate let cellTwo = "CategoaryViewCell"
     fileprivate let cellThree = "SettingViewCell"
     fileprivate let cellFour = "SegmentViewCell"
     fileprivate let cellFive = "VersionCell"
     
     
     @IBOutlet weak var CategoaryTbl: UITableView!
     @IBOutlet weak var versionTitle : UILabel!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
     override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           //versionTitle.text = "Version: 1.0".localized
        
           let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
           statusBarView.backgroundColor = UIColor.colorWithRedValue(redValue: 10, greenValue: 177, blueValue: 75, alpha: 1)
           view.addSubview(statusBarView)
           CategoaryTbl.estimatedRowHeight = 50
           CategoaryTbl.rowHeight = UITableView.automaticDimension
           self.apiManagerClient = APIManagerClient.sharedInstance
           self.loadInitCategoary()
           //NotificationCenter.default.addObserver(self, selector: #selector(reloadTableView), name: NSNotification.Name("CategoryReload"), object: nil)
           
           if let DrawerViewController = navigationController?.parent as? HomeViewController{
               DrawerViewController.KYDCDelegate = self
               print(DrawerViewController)
           }
       }
       
       @objc func reloadTableView() {
           loadInitCategoary()
       }
       
       override func viewWillDisappear(_ animated: Bool) {
           super.viewWillDisappear(animated)
           self.CategoaryTbl.setContentOffset(CGPoint.zero, animated: true)
       }
       
       func animate () {
           self.CategoaryTbl.reloadData()
           let cells = self.CategoaryTbl.visibleCells
           let tableHeight: CGFloat = self.CategoaryTbl.bounds.size.height
           for i in cells {
               let cell: UITableViewCell = i as UITableViewCell
               cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
           }
           var index = 0
           for a in cells {
               let cell: UITableViewCell = a as UITableViewCell
               UIView.animate(withDuration: 1.3, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                   cell.transform = CGAffineTransform(translationX: 0, y: 0);
               }, completion: nil)
               index += 1
           }
       }
       
       func loadInitCategoary() {
           
//           if UserDefaults.standard.bool(forKey: "EnabledLogOut") {
//               ListArray = ["My Account","My Cart","My WishList","My Order","Help & Support", "Log Out"]
//           }else if UserDefaults.standard.bool(forKey: "LoginStatus") {
//               ListArray = ["My Account","My Cart","My WishList","My Order","Help & Support", "Log Out"]
//           }else {
//               ListArray =  ["My Account","My Cart","My WishList","My Order","Help & Support"]
//           }
        
//           self.headerCategoryArray = []
//           for category in self.apiManagerClient.leftMenuCategoriesArray {
//               if category.isLowestItem == false {
//                   println_debug(category.name)
//                   self.headerCategoryArray.append(category)
//               }
//           }

        ListArray = ["Choose Language".localized,"Dashboard".localized, "Profile".localized,"Change Password".localized,"Help & Support".localized]
           DispatchQueue.main.async {
               self.CategoaryTbl.delegate = self
               self.CategoaryTbl.dataSource = self
               self.CategoaryTbl.reloadData()
           }
       }
       
       override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "MenuToHomeUnwindSegue" {
               if let vc = segue.destination as? HomeViewController {
                   vc.isLogoutTrue = true
               }
           }
       }
       
       private func showAlert(message: String) {
           if message == "Success" {
               DispatchQueue.main.async {
                let alert  = UIAlertController(title: message, message: "Updated successfully".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { (alert) in
                       self.dismiss(animated: true, completion: nil)
                   }))
                   self.present(alert, animated: true, completion: nil)
               }
           } else {
               DispatchQueue.main.async {
                let alert  = UIAlertController(title: "Warning".localized, message: "Try again".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
                   self.present(alert, animated: true, completion: nil)
               }
           }
       }
       deinit {
//           NotificationCenter.default.removeObserver(self, name: NSNotification.Name("CategoryReload"), object: nil)
       }
       
       //MARK: UIGuesture Method
       @objc func tappedOnIconImage() {
           if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
               DrawerTableViewController.setDrawerState(.closed, animated: true)
           }
           performSegue(withIdentifier: "MenuToHomeUnwindSegue", sender: self)
       }
   }

   extension SideMenuViewController: KYDrawerCustomDelegate{
       func resetAllData() {
           self.loadInitCategoary()
       }
   }


   //MARK: - UITableViewDataSource, UITableViewDelegate
   extension SideMenuViewController: UITableViewDataSource, UITableViewDelegate {
       func numberOfSections(in tableView: UITableView) -> Int {
           return 8
       }
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
               return 1
       }
       
       func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
               return 0
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           var cell: CategoaryViewCell!
           switch indexPath.section {
           case 0:
               cell = tableView.dequeueReusableCell(withIdentifier: cellOne, for: indexPath) as? CategoaryViewCell
               let tap = UITapGestureRecognizer(target: self, action: #selector(SideMenuViewController.tappedOnIconImage))
               cell.imgPersonIcon.addGestureRecognizer(tap)
               cell.imgPersonIcon.isUserInteractionEnabled = true
              // let firstName = VMLoginModel.shared.firstName
               cell.lblheader.font = UIFont(name: appFont, size: 22.0)
               if UserDefaults.standard.bool(forKey: "LoginStatus") {
                let userName = "\(Vendor_loginmodel.shared.firstName ?? "")"
                cell.lblheader.text = userName.count > 0 ? userName : Vendor_loginmodel.shared.shopName
               }else {
                cell.lblheader.text = "OSK Vendor".localized
               }
               //cell.contentView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 183, greenValue: 28, blueValue: 28, alpha: 1), UIColor.init(red: 185.0/255.0, green: 30.0/255.0, blue: 30.0/255.0, alpha: 1.0)])
               
            case 1:
                cell = tableView.dequeueReusableCell(withIdentifier: cellTwo, for: indexPath) as? CategoaryViewCell
//                if appLang == "English" {
//                      let name = "Choose Language".localized + " (" + appLang.localized + ")"
//                        //"ဘာသာစကား ေရြးပါ" + " (" + "အဂၤလိပ္" + ")"+ "\n" + "Choose Language" + "/ (" + appLang + ")"
//                    cell.wrapCategoary(string: name)
//                } else {
//
//
//                    // + "\n" + "Choose Language" + " / (" + appLang + ")"
//
//                }
                let name = "Choose Language /\n ဘာသာစကား ရွေးချယ်ပါ"
                cell.wrapCategoary(string: name)
                cell.borderView.isHidden = true
                cell.selectionStyle = .none
                
           case 2:
                cell = tableView.dequeueReusableCell(withIdentifier: cellTwo, for: indexPath) as? CategoaryViewCell
                let name = "Dashboard".localized
                cell.borderView.isHidden = true
            if #available(iOS 13.0, *) {
                cell.backgroundColor = .systemGray5
            } else {
                // Fallback on earlier versions
                cell.backgroundColor = .lightText
            }
                cell.wrapCategoary(string: name)
            case 3:
                       cell = tableView.dequeueReusableCell(withIdentifier: cellTwo, for: indexPath) as? CategoaryViewCell
                       let name = "My Products List".localized
                       cell.borderView.isHidden = true
                       cell.wrapCategoary(string: name)
            case 4:
                       cell = tableView.dequeueReusableCell(withIdentifier: cellTwo, for: indexPath) as? CategoaryViewCell
                       let name = "My Orders List".localized
                       cell.borderView.isHidden = true
                       cell.wrapCategoary(string: name)
            case 5:
            cell = tableView.dequeueReusableCell(withIdentifier: cellTwo, for: indexPath) as? CategoaryViewCell
            let name = "Profile".localized
            cell.borderView.isHidden = true
            cell.wrapCategoary(string: name)
           case 6:
               cell = tableView.dequeueReusableCell(withIdentifier: cellTwo, for: indexPath) as? CategoaryViewCell
              // let name = "Logout".localized
               let name = "Help & Support".localized
               cell.borderView.isHidden = true
               cell.wrapCategoary(string: name)
            
           case 7:
               cell = tableView.dequeueReusableCell(withIdentifier: cellTwo, for: indexPath) as? CategoaryViewCell
              // let name = "Logout".localized
               let name = "Version 1.0".localized
               cell.borderView.isHidden = true
               cell.selectionStyle = .none
               cell.wrapCategoary(string: name)
            
           default: break
//               cell = tableView.dequeueReusableCell(withIdentifier: cellFive, for: indexPath) as? CategoaryViewCell
//               //cell.viewBGVersion.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
//               cell.viewBGVersion.backgroundColor = UIColor.colorWithRedValue(redValue: 10, greenValue: 177, blueValue: 75, alpha: 1)
//               cell.lblVersion.font = UIFont(name: appFont, size: 15.0)
           }
           return cell
       }
       
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           switch indexPath.section {
           case 0:
               return 180
           case 1:
               return 60//UITableView.automaticDimension
           case 2:
               return 60//UITableView.automaticDimension
           case 3:
               return 60//UITableView.automaticDimension
           case 4:
               return 60//UITableView.automaticDimension
            case 5:
                return 60//UITableView.automaticDimension
            case 6:
                return 60//UITableView.automaticDimension
            case 7:
               return 60//UITableView.automaticDimension
           default:
               return 80
           }
       }
       
       func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           let headerView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 2))
           headerView.backgroundColor = UIColor.colorWithRedValue(redValue: 220, greenValue: 220, blueValue: 220, alpha: 1)
           return headerView
       }
       
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
        switch indexPath.section {
        case 0://User Icon
            performSegue(withIdentifier: "MenuToHomeUnwindSegue", sender: self)
        case 1:
            
            if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
                DrawerTableViewController.setDrawerState(.closed, animated: true)
            }
            performSegue(withIdentifier: "MenuToLanguage", sender: self)
        case 2:
            if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
                DrawerTableViewController.setDrawerState(.closed, animated: true)
            }
            performSegue(withIdentifier: "MenuToHomeUnwindSegue", sender: self)
        case 3://My Products
            if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
                DrawerTableViewController.setDrawerState(.closed, animated: true)
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TapOnCategory"), object: nil, userInfo: ["controller":"ProductListViewController"])
            
        case 4://My Orders
            if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
                DrawerTableViewController.setDrawerState(.closed, animated: true)
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TapOnCategory"), object: nil, userInfo: ["controller":"MyOrdersVC"])
        case 5:
            if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
                DrawerTableViewController.setDrawerState(.closed, animated: true)
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TapOnCategory"), object: nil, userInfo: ["controller":"EditProfileVC"])
        case 6:
//            if UserDefaults.standard.bool(forKey: "LoginStatus") {
//                if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
//                    UserDefaults.standard.set(true, forKey: "isLogin")
//                    UserDefaults.standard.synchronize()
//                    DrawerTableViewController.setDrawerState(.closed, animated: true)
//                }else if let DrawerTableViewController = self.parent as? KYDrawerController{
//                    DrawerTableViewController.setDrawerState(.closed, animated: true)
//                }
//                UserDefaults.standard.synchronize()
//                performSegue(withIdentifier: "MenuToHomeUnwindSegue", sender: self)
//            }
            if let DrawerTableViewController = navigationController?.parent as? KYDrawerController{
                DrawerTableViewController.setDrawerState(.closed, animated: true)
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TapOnCategory"), object: nil, userInfo: ["controller":"HelpSupportVC"])
            
            
            
        default:
            break
        }
       }
    
    func getTopMostViewController() -> UIViewController? {
          var topMostViewController = UIApplication.shared.keyWindow?.rootViewController
          
          while let presentedViewController = topMostViewController?.presentedViewController {
              topMostViewController = presentedViewController
          }
          
          return topMostViewController
      }
      
   }



   /*
    
    protocol ChangeLanguageProtocol: class {
    func changeAppLanguage(index: Int)
    }
    */

   //MARK: - PromotionalViewCell
   class CategoaryViewCell: UITableViewCell {
       
       @IBOutlet var lblheader: UILabel!{
           didSet {
             //  self.lblheader.font = UIFont(name: appFont, size: 15.0)
               self.lblheader.text = self.lblheader.text
               
           }
       }
       
       //MARK: - Outlet
       @IBOutlet var lblCatName: UILabel!{
           didSet {
               self.lblCatName.font = UIFont(name: appFont, size: 15.0)
               self.lblCatName.text = self.lblCatName.text
               
           }
       }
       @IBOutlet var borderView: UIView!
       
       @IBOutlet var lblCompanyName: UILabel!{
           didSet {
               self.lblCompanyName.font = UIFont(name: appFont, size: 15.0)
               self.lblCompanyName.text = self.lblCompanyName.text
               
           }
       }
       @IBOutlet var lblDesc: UILabel!{
           didSet {
               self.lblDesc.font = UIFont(name: appFont, size: 15.0)
               self.lblDesc.text = self.lblDesc.text
               
           }
       }
       @IBOutlet var lblDate: UILabel!{
           didSet {
               self.lblDate.font = UIFont(name: appFont, size: 15.0)
               self.lblDate.text = self.lblDate.text
               
           }
       }
       @IBOutlet var imgPersonIcon: UIImageView!
       @IBOutlet weak var englishLangView: UIView! {
           didSet {
               englishLangView.roundCorners([.topRight, .bottomRight], radius: 20)
           }
       }
       
       @IBOutlet weak var stackLanguage: UIStackView!
       
       
       @IBOutlet weak var burmeseLangView: UIView! {
           didSet {
               burmeseLangView.roundCorners([.topLeft, .bottomLeft], radius: 20)
           }
       }
       
       @IBOutlet weak var lblEnlishLanguage: UILabel!{
           didSet {
               self.lblEnlishLanguage.font = UIFont(name: appFont, size: 15.0)
               self.lblEnlishLanguage.text = self.lblEnlishLanguage.text
               
           }
       }
       @IBOutlet weak var lblBurmeseLanguage: UILabel!{
           didSet {
               self.lblBurmeseLanguage.font = UIFont(name: appFont, size: 15.0)
               self.lblBurmeseLanguage.text = self.lblBurmeseLanguage.text
               
           }
       }
       
       @IBOutlet weak var viewBGVersion: UIView!
       @IBOutlet weak var lblVersion: UILabel!{
           didSet {
               self.lblVersion.font = UIFont(name: appFont, size: 15.0)
               self.lblVersion.text = self.lblVersion.text
               
           }
       }
       
       weak var delegate: ChangeLanguageProtocol?
       
       //MARK: - Views Life Cycle
       override func awakeFromNib() {
           super.awakeFromNib()
           //        self.imgPersonIcon?.layer.cornerRadius = (self.imgPersonIcon?.frame.width ?? 0) / 2
       }
       
       override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)
       }
       
    func wrapCategoary(string: String) {
      //  lblCatName.font = UIFont(name: appFont, size: 15.0)
        //println_debug("item list : \(string)")
        if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), lang == "en"{
            self.lblCatName.font = UIFont.systemFont(ofSize: 15)
           
        } else {
            self.lblCatName.font =  UIFont(name: appFont, size: 15)
            
        }
        
        
        lblCatName.text = string.localized
    }
   }

//   extension SideMenuViewController : RegistrationDelegate {
//
//       func backToMainPage() {
//           self.dismiss(animated: true, completion: nil)
//       }
//   }

   extension UIView {
       
       func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
           let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
           let mask = CAShapeLayer()
           mask.path = path.cgPath
           self.layer.mask = mask
       }
       
   }
