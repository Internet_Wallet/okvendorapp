//
//  VMHelpAndSupport.swift
//  Covid Zay Vendor
//
//  Created by iMac on 15/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import MessageUI

class VMHelpAndSupport: OSKBaseViewController {
    
    @IBOutlet weak var tbHelpSupport: UITableView!{
        didSet {
            tbHelpSupport.tableFooterView = UIView(frame: .zero)
        }
    }
    var alertVC : SAlertController?
    
    var list : HelpSupportData?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Help & Support".localized
//        self.navigationController?.isNavigationBarHidden = false
//        self.title = "Help & Support".localized
        //        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        HelpSupportDataClass.generateDataList(handler: { (isBool,model)  in
            if isBool {
                self.list = model
                DispatchQueue.main.async {
                    self.tbHelpSupport.delegate = self
                    self.tbHelpSupport.dataSource = self
                    self.tbHelpSupport.reloadData()
                }
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        
       // self.dismiss(animated: true, completion: nil)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func onClickBackAction(_: UIButton) {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNavToAudioVideo(_ sender: UIButton) {
        //        if let viewController = UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "AuthController") as? LoginTableViewController{
        //
        //            self.present(viewController, animated: true, completion: nil)
        //        }
        
        
        
        if let viewController = UIStoryboard(name: "Authorization", bundle: nil).instantiateViewController(withIdentifier: "AuthController") as? UINavigationController{
            self.present(viewController, animated: true, completion: nil)
        }
        
        
        
        
        
        //        let storyBoard : UIStoryboard = UIStoryboard(name: "Auth", bundle:nil)
        //
        //        let newViewController2 = storyBoard.instantiateViewController(withIdentifier: "AuthController")
        //
        //        let newViewController = storyBoard.instantiateViewController(withIdentifier: "LoginTableViewController") as? LoginTableViewController
        //        let navigationController = UINavigationController(rootViewController: newViewController)
        //        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        //        appdelegate.window!.rootViewController = navigationController
        
        
    }
}
extension VMHelpAndSupport : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list?.data.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = list?.data[indexPath.row]
        if indexPath.row == 0 {
            let cell = tbHelpSupport.dequeueReusableCell(withIdentifier: "HeadOffice") as! helpSupportCell
            cell.selectionStyle = .none
            cell.lblHeadOffice.text = item?.value//"No.(265), Bogyoke Aung San Rd, Kyauktada Township, Yangon, Myanmar"//
            cell.imgHeadOffice.image = UIImage(named: "buildimage")
            return cell
        }else {
            let cell = tbHelpSupport.dequeueReusableCell(withIdentifier: "CallCell") as! helpSupportCell
            return wrapCell(index: indexPath, cell: cell)
        }
    }
    
    func wrapCell(index: IndexPath, cell: helpSupportCell) -> helpSupportCell {
        cell.selectionStyle = .none
        let item = list?.data[index.row]
        cell.btnCall.tag = index.row
        switch item?.title {
        case "CALL US":
            cell.lblCall.text = item?.value
            cell.btnCall.setTitle("CALL US", for: .normal)
            cell.btnCall.addTarget(self, action: #selector(callOnNumber(_:)), for: .touchUpInside)
            cell.imgCall.image = UIImage(named: "callimage")
            break
        case "SMS US":
            cell.lblCall.text = item?.value
            cell.btnCall.setTitle("SMS US", for: .normal)
            cell.btnCall.addTarget(self, action: #selector(smsOnNumber(_:)), for: .touchUpInside)
            cell.imgCall.image = UIImage(named: "smsimage")
            break
        case "VIBER":
            cell.lblCall.text = item?.value
            cell.btnCall.setTitle("VIBER", for: .normal)
            cell.btnCall.addTarget(self, action: #selector(openViber(_:)), for: .touchUpInside)
            cell.imgCall.image = UIImage(named: "viberimage")
            break
        case "WhatsApp":
            cell.lblCall.text = item?.value
            cell.btnCall.setTitle("WhatsApp", for: .normal)
            cell.btnCall.addTarget(self, action: #selector(openWhatsApp(_:)), for: .touchUpInside)
            cell.imgCall.image = UIImage(named: "whatsappimage")
            break
        case "EMAIL US":
            cell.lblCall.text = item?.value
            cell.btnCall.setTitle("EMAIL US", for: .normal)
            cell.btnCall.addTarget(self, action: #selector(openMail(_:)), for: .touchUpInside)
            cell.imgCall.image = UIImage(named: "mailimage")
            break
        default:
            break
        }
        
        return cell
    }
    
    @objc func onClickCall(_ sender: UIButton) {
        
    }
    
    @objc func callOnNumber(_ sender: UIButton) {
        let item = list?.data[sender.tag]
        let phoneNumber = item?.value
        let cleanNumber = phoneNumber!.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
        guard let number = URL(string: "telprompt://" + cleanNumber) else { return }
        UIApplication.shared.open(number, options: [:], completionHandler: nil)
    }
    
    @objc  func smsOnNumber(_ sender: UIButton) {
        let item = list?.data[sender.tag]
        if !MFMessageComposeViewController.canSendText() {
        }else{
            let composeVC = MFMessageComposeViewController()
            composeVC.messageComposeDelegate = self
            composeVC.recipients = [item?.value] as? [String]
            composeVC.body = "Welcome To 24 7 Sine!"
            self.present(composeVC, animated: true, completion: nil)
        }
    }
    
    @objc  func openViber(_ sender: UIButton) {
        let item = list?.data[sender.tag]
        let phoneNumber = item?.value
        let cleanNumber = phoneNumber!.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
        let urlWhats = "viber://chat:<\(cleanNumber)>"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(whatsappURL, options: [:], completionHandler: nil)
                    }else {
                        UIApplication.shared.openURL(whatsappURL)
                    }
                }else {
                    let alert = UIAlertController(title: "Warning!", message: "Viber app is not install in your Phone, Please install and try agian", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
        }
    }
    
    @objc  func openWhatsApp(_ sender: UIButton) {
        let item = list?.data[sender.tag]
        let phoneNumber = item?.value
        let cleanNumber = phoneNumber!.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
        let urlWhats = "whatsapp://send?text=\(cleanNumber)"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(whatsappURL, options: [:], completionHandler: nil)
                    }else {
                        UIApplication.shared.openURL(whatsappURL)
                    }
                }else {
                    let alert = UIAlertController(title: "Warning!", message: "Whats app is not install in your Phone, Please install and try agian", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    @objc  func openMail(_ sender: UIButton) {
        let item = list?.data[sender.tag]
        let email = item?.value
        if let url = URL(string: "mailto:\(email ?? "INFO@OKZAY.COM")") {
            UIApplication.shared.open(url)
        }else{
            let alert = UIAlertController(title: "Warning!", message: "Mail is not configured", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
}


extension VMHelpAndSupport : MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    
}


class helpSupportCell : UITableViewCell {
    
    @IBOutlet weak var imgHeadOffice: UIImageView!
    @IBOutlet weak var lblHeadOffice: UILabel!{
        didSet {
            self.lblHeadOffice.font = UIFont(name: appFont, size: 15.0)
            self.lblHeadOffice.text = self.lblHeadOffice.text?.localized
            
        }
    }
    @IBOutlet weak var imgCall: UIImageView!
    @IBOutlet weak var lblCall: UILabel!{
        didSet {
            self.lblCall.font = UIFont(name: appFont, size: 15.0)
            self.lblCall.text = self.lblCall.text?.localized
            
        }
    }
    @IBOutlet weak var btnCall: UIButton!{
        didSet {
            btnCall.setTitle((btnCall.titleLabel?.text ?? "").localized, for: .normal)
            btnCall.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
        
    }
    
}


