//
//  HelpSupportDataClass.swift
//  Covid Zay Vendor
//
//  Created by iMac on 15/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import SwiftyJSON
class HelpSupportDataClass: NSObject {
    
    class func generateDataList(handler: @escaping (_ success: Bool,_ model: HelpSupportData?)-> Void) {
        
        let apiManager = APIManager()
        let urlParameters = String(format: "%@/%@", APIManagerClient.sharedInstance.base_url,"customer/helpandsupport") as String
        let urlString =  URL(string: urlParameters)!
        let paramDic = Dictionary<String,AnyObject>()
        
        apiManager.genericClass(url: urlString, param: paramDic as AnyObject, httpMethod: "GET", header: true, handle: {(json, isSuccess, data)in
            if isSuccess {
                if let model = try? JSONDecoder().decode(HelpSupportData.self, from: data ?? Data()) {
                    handler(true, model)
                }
            }else {
                print("API Failed")
                handler(false, nil)
            }
        })
    }
    
}

struct HelpSupportData: Codable {
    let data: [HelpSupportDictionary]
    
    enum CodingKeys: String, CodingKey {
        case data = "Data"
    }
}

struct HelpSupportDictionary: Codable {
    let title, value: String
    
    enum CodingKeys: String, CodingKey {
        case title = "Title"
        case value = "Value"
    }
}
