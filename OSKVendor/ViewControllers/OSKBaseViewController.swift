//
//  ViewController.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 26/06/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class OSKBaseViewController: UIViewController {
    
    let sAlertController = SAlertController()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
              self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 19.0)  ?? UIFont.systemFont(ofSize: 19), NSAttributedString.Key.foregroundColor: UIColor.white ]
               //        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
              //self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 0 , greenValue: 119, blueValue: 182, alpha: 1), UIColor.init(red: 5.0/255.0, green: 124.0/255.0, blue: 187.0/255.0, alpha: 1.0)])
             self.navigationController?.navigationBar.backgroundColor = UIColor.colorWithRedValue(redValue: 10, greenValue: 177, blueValue: 75, alpha: 1)
             self.navigationController?.setStatusBar(backgroundColor: UIColor.colorWithRedValue(redValue: 10, greenValue: 177, blueValue: 75, alpha: 1))//(13, 177, 75)
             let barButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
             barButton.tintColor = .white
             navigationItem.backBarButtonItem = barButton
             if #available(iOS 13.0, *) {
                overrideUserInterfaceStyle = .light
             }
         }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.backgroundColor = UIColor.colorWithRedValue(redValue: 10, greenValue: 177, blueValue: 75, alpha: 1)
        self.navigationController?.setStatusBar(backgroundColor: UIColor.colorWithRedValue(redValue: 10, greenValue: 177, blueValue: 75, alpha: 1))//(13, 177, 75)
        let barButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        barButton.tintColor = .white
        navigationItem.backBarButtonItem = barButton
        if #available(iOS 13.0, *) {
           overrideUserInterfaceStyle = .light
        }
    }
    
    func showAlert(alertTitle: String, description: String) {
        sAlertController.ShowSAlert(title: alertTitle, withDescription: description, onController: self)
        let okAlertAction = SAlertAction()
        okAlertAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
            
        })
        sAlertController.addAction(action: [okAlertAction])
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
    
}



extension UINavigationController {

    func setStatusBar(backgroundColor: UIColor) {
        let statusBarFrame: CGRect
        if #available(iOS 13.0, *) {
            statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        } else {
            statusBarFrame = UIApplication.shared.statusBarFrame
        }
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = backgroundColor
        view.addSubview(statusBarView)
    }

}

