//
//  AddProductCell.swift
//  Cell
//
//  Created by Tushar Lama on 29/10/2020.
//

import UIKit


protocol AddProductTitleLabelDel {
    func manageHeight(index: Int,hasText: Bool)
}

protocol CallCamera {
    func callCamera()
}

class AddProductCell: UITableViewCell {

    @IBOutlet var addProductTextView: UITextView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var titleLabelHeight: NSLayoutConstraint!
    @IBOutlet var addProductTextVieHeight: NSLayoutConstraint!
    @IBOutlet var camBtn: UIButton!
    var titleDelegate: AddProductTitleLabelDel?
    var cameraDelegate : CallCamera?
    var productParamDel: AddProductParamProtocol?
   
    override func awakeFromNib() {
        super.awakeFromNib()
        //addProductTextView.text = "Product Name".localized
        addProductTextView.delegate = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onClickCam(_ sender: Any) {
        cameraDelegate?.callCamera()
    }
    
    
    func setDataOnCell(tag: Int,titleValue: String){
        let value = titleValue
        if tag == 1{
            if value != ""{
               addProductTextView.text = value
               titleLabelHeight.constant = 18.0
               titleLabel.isHidden = false
               titleLabel.text = "Product Name".localized
            }else{
               addProductTextView.text = "Product Name".localized
               titleLabelHeight.constant = 0.0
               titleLabel.isHidden = true
               titleLabel.text = ""
            }
            camBtn.isHidden = false
            
        }else if tag == 2{
            
            if value != ""{
                addProductTextView.text = value
                titleLabelHeight.constant = 18.0
                titleLabel.isHidden = false
                titleLabel.textColor = .black
                titleLabel.text = "Product Name In Unicode*".localized
            }else{
                addProductTextView.text = "Product Name In Unicode*".localized
                titleLabelHeight.constant = 0.0
                titleLabel.isHidden = true
                titleLabel.text = ""
            }
            
            camBtn.isHidden = true
            
        }else if tag == 3{
            if value != ""{
                addProductTextView.text = value
                titleLabelHeight.constant = 18.0
                titleLabel.isHidden = false
                titleLabel.text = "Select Category*".localized
            }else{
                addProductTextView.text = "Select Category*".localized
                titleLabelHeight.constant = 0.0
                titleLabel.isHidden = true
                titleLabel.text = ""
            }
            
        camBtn.isHidden = true
            
        }
        
        
    }
    

}

extension AddProductCell: UITextViewDelegate{
    
    internal func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let updatedString = (textView.text as NSString?)?.replacingCharacters(in: range, with: text)
       
        if (updatedString?.count ?? 0) > 40 {
            return false
        } else {
            if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), lang == "en"{
                let  char = text.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if isBackSpace != -92 {
                    let strTxt : Character = Character(text)
                    if textView.text?.last == strTxt {
                        if textView.text?.count ?? 0 >= 2 {
                            if let index1 = textView.text?.index((textView.text?.startIndex)!, offsetBy: range.location - 2)
                            {//will call succ 2 times
                                if let lastChar: Character = (textView.text?[index1]) //now we can index!
                                {
                                    if lastChar == strTxt
                                    {
                                        return false
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.tag == 1{
            if textView.text == "Product Name".localized{
                textView.text = ""
            }
            titleLabelHeight.constant = 18.0
            titleLabel.isHidden = false
            titleLabel.text = "Product Name".localized
        }else if textView.tag == 2{
            if textView.text == "Product Name In Unicode*".localized{
                textView.text = ""
            }
            titleLabelHeight.constant = 18.0
            titleLabel.isHidden = false
            titleLabel.textColor = .black
            titleLabel.text = "Product Name In Unicode*".localized
        }else if textView.tag == 3{
            if textView.text == "Select Category*".localized{
                textView.text = ""
            }
            titleLabelHeight.constant = 18.0
            titleLabel.isHidden = false
            titleLabel.text = "Select Category *".localized
        }
        
        
        
        
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.tag == 1{
            
            productParamDel?.param(name: CategoryConstant.AddProductKey.ProductNameEnglish.rawValue, value: textView.text ?? "")
            
            if textView.text == ""{
                titleLabelHeight.constant = 0.0
                titleLabel.isHidden = true
                textView.text = "Product Name".localized
            }
        }else if textView.tag == 2{
            productParamDel?.param(name: CategoryConstant.AddProductKey.ProductNameUnicode.rawValue, value: textView.text ?? "")
            if textView.text == ""{
                titleLabelHeight.constant = 0.0
                titleLabel.isHidden = true
                textView.text = "Product Name In Unicode*".localized
            }
        }else if textView.tag == 3{
            productParamDel?.param(name: CategoryConstant.AddProductKey.ProductCategory.rawValue, value: textView.text ?? "")
            if textView.text == ""{
                titleLabelHeight.constant = 0.0
                titleLabel.isHidden = true
                textView.text = "Select Category*".localized
            }
        }
    }
    
    
}



