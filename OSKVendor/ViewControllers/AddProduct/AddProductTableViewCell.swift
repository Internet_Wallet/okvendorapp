//
//  AddProductTableViewCell.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 16/09/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import DKImagePickerController


protocol imageDelete {
    func deleteImageContentAt(index : Int)
    func palyVideo(index : Int)
}

protocol addProductDelgate {
    func onClickCameraAction( sender : UIButton)
    func onClickDeliveryTimeAction( sender : UIButton)
    func didEditChanged( sender : UITextField)
    
}

class AddProductTableViewCell: UITableViewCell {
    
        @IBOutlet var viewContainer: UIView!
        @IBOutlet var pageControl: UIPageControl!
        
        var delegateX: imageDelete?
    
        var scrollImageContainer =  UIScrollView()
        var currentPage = 1
        var btnDeleteProductImage = UIButton()
        var buttonPlay = UIButton()
    
    
        override func awakeFromNib() {
            super.awakeFromNib()
        }

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
        }
    
    func wrapData ( cell : AddProductTableViewCell , ImageData : [DKAsset] ) {
        
        for (_ , elementX) in scrollImageContainer.subviews.enumerated() {
            elementX.removeFromSuperview()
        }
       pageControl.currentPage  = 0
       pageControl.numberOfPages = ImageData.count
       viewContainer.layer.cornerRadius = 5.0
       viewContainer.layer.masksToBounds = true
       viewContainer.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
       viewContainer.layer.borderWidth = 1.0
       viewContainer.backgroundColor = .white
       
       scrollImageContainer.frame = CGRect(x: 0, y: 0, width: viewContainer.frame.size.width, height: 215)
       scrollImageContainer.delegate = self
       scrollImageContainer.isPagingEnabled = true
      // scrollImageContainer.bounces = false
       
       scrollImageContainer.contentSize = CGSize(width: (viewContainer.frame.size.width * CGFloat(ImageData.count)), height: 215)
       viewContainer.addSubview(scrollImageContainer)
       var xAxis : CGFloat = 0
       
       for (item, _) in ImageData.enumerated() {
           
           let asset = ImageData[item]
           let viewImageContainer = UIView()
           xAxis = CGFloat(item) * viewContainer.frame.size.width
           viewImageContainer.frame = CGRect(x: xAxis, y: 0, width: viewContainer.frame.size.width , height: 215)
        
           let BaseBackground = UIView()
           BaseBackground.frame = CGRect(x: 15, y: 5 , width: viewContainer.frame.size.width - 30, height: viewContainer.frame.size.height - 10)
        
           let imageProduct = UIImageView()
           imageProduct.frame = CGRect(x: 0, y: 0 , width: BaseBackground.frame.size.width, height: 158)
           imageProduct.contentMode = .scaleAspectFit
           imageProduct.tag = item
           if asset.type == .video {
                   asset.fetchImage(with: CGSize(width: 512.0, height: 750.0).toPixel(), completeBlock: { image, info in
                       if imageProduct.tag == item {
                              imageProduct.image = image
                              self.buttonPlay = UIButton()
                              self.buttonPlay.frame = CGRect(x: 0.0, y: 0.0, width: imageProduct.frame.size.width, height: imageProduct.frame.size.height)
                              self.buttonPlay.setImage(UIImage(named: "Play"), for: .normal)
                              self.buttonPlay.tag = item
                              self.buttonPlay.addTarget(self, action: #selector(self.playVideo(sender:)), for: .touchUpInside)
                              BaseBackground.addSubview(self.buttonPlay)
                          }
                      })
            
               } else {
                  asset.fetchImage(with: CGSize(width: 512.0, height: 750.0).toPixel(), completeBlock: { image, info in
                      if imageProduct.tag == item {
                             imageProduct.image = image
                         }
                    })
               }
       
           btnDeleteProductImage = UIButton()
           btnDeleteProductImage.frame = CGRect(x: viewContainer.frame.size.width - 40, y: 10, width: 20, height: 20)
           btnDeleteProductImage.setImage(UIImage(named: "Delete"), for: .normal)
           btnDeleteProductImage.tag = item
           btnDeleteProductImage.addTarget(self, action: #selector(deleteImage(sender:)), for: .touchUpInside)
           
           BaseBackground.addSubview(imageProduct)
           viewImageContainer.addSubview(BaseBackground)
           viewImageContainer.addSubview(btnDeleteProductImage)
           scrollImageContainer.addSubview(viewImageContainer)
       }
    }
    
    @objc func deleteImage ( sender : UIButton ) {
         print("Delete Image No : \(sender.tag) call")
         delegateX?.deleteImageContentAt(index: sender.tag)
    }
    
    @objc func playVideo( sender : UIButton ) {
         print("Play Video at Index No : \(sender.tag) call")
         delegateX?.palyVideo(index: sender.tag)
    }
    
}



extension AddProductTableViewCell : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }

}

class AddProductsFieldsCell : UITableViewCell {
    
    var delegateA : addProductDelgate?
    
    @IBOutlet var viewContainer: UIView!
    @IBOutlet var lblTitle: UILabel!{
        didSet {
            self.lblTitle.font = UIFont(name: appFont, size: 15.0)
            self.lblTitle.text = self.lblTitle.text?.localized
        }
    }
    @IBOutlet var tfValue: UITextField! {
        didSet{
            self.tfValue.font = UIFont(name: appFont, size: 15.0)
            self.tfValue.placeholder = self.tfValue.placeholder?.localized
        }
    }
    @IBOutlet var txtView: UITextView! {
        didSet{
            self.txtView.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var lblPlaceholderText: UILabel!{
        didSet {
            self.lblPlaceholderText.font = UIFont(name: appFont, size: 15.0)
            self.lblPlaceholderText.text = self.lblPlaceholderText.text?.localized
        }
    }
    
    @IBOutlet var btnOption:UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewContainer.layer.cornerRadius = 5.0
        viewContainer.layer.masksToBounds = true
        viewContainer.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        viewContainer.layer.borderWidth = 1.0
        viewContainer.backgroundColor = .white
    }
    
    func  WrapCellData( lblTitle : String , placeHolderText : String? , indexPath : IndexPath , inputData : String) {
        
        self.selectionStyle = .none
        self.lblTitle.text = lblTitle.localized
        self.tfValue.placeholder = placeHolderText?.localized
        self.tfValue.keyboardType = .default
        if indexPath.row == 2 {
            self.tfValue.keyboardType = .numberPad
        }
        self.tfValue.tag = 10+10*indexPath.row
        self.btnOption.tag = 10+10*indexPath.row
        self.btnOption.isHidden = true
        self.tfValue.autocorrectionType = .no
        self.tfValue.addTarget(self, action: #selector(didEditChanged(sender:)), for: .editingChanged)

        if inputData != "" {
            self.tfValue.text = inputData
        }
        else {
            self.tfValue.text = ""
        }
        if indexPath.row == 0 {
              self.btnOption.setImage(#imageLiteral(resourceName: "Cam"), for: .normal)
            self.btnOption.isHidden = false
        }
//        else if (indexPath.row == 3){
//            self.btnOption.setImage(#imageLiteral(resourceName: "downarrow"), for: .normal)
//            self.btnOption.isHidden = false
//        }
        else if (indexPath.row == 9) {
            self.btnOption.setImage(#imageLiteral(resourceName: "barcode"), for: .normal)
            self.btnOption.isHidden = false
        }
    }
    
    @IBAction func ButtonClicked ( sender : UIButton) {
        self.delegateA?.onClickCameraAction(sender: sender)
    }

    
     @objc func didEditChanged( sender : UITextField) {
        self.delegateA?.didEditChanged(sender: sender)
    }
    
    
    
}

class CategoryListCell : UITableViewCell {
    
    @IBOutlet var lblTitle: UILabel! {
        didSet{
            self.lblTitle.font = UIFont(name: appFont, size: 15.0)
            self.lblTitle.text = self.lblTitle.text?.localized
        }
    }
    
    @IBOutlet weak var marketSelectButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}


class DevliveryTimeAddProductCell: UITableViewCell {
    
    @IBOutlet var viewContainerData: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnTitle: UIButton!
    @IBOutlet var imgRight: UIImageView!
    @IBOutlet var lblRight: UILabel!
    
    var delegateB : addProductDelgate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContainerData.layer.cornerRadius = 5.0
        self.viewContainerData.layer.masksToBounds = true
        self.viewContainerData.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewContainerData.layer.borderWidth = 1.0
        self.viewContainerData.backgroundColor = .white
    }
    
    
    func  WrapCellData( lblTitle : String , indexPath : IndexPath , ProductDetails : [String : Any] , deliveryData : (String , String)) {
        
        self.selectionStyle = .none
        self.btnTitle.setTitle("Delivery Time".localized, for: .normal)
        self.lblRight.text = deliveryData.1
        if deliveryData.1 != "" {
            self.lblRight.isHidden = false
        }else {
            self.lblRight.isHidden = true
        }
        self.imgRight.isHidden = false
        self.imgRight.image = #imageLiteral(resourceName: "downarrow")
        self.btnTitle.tag = 10+10*indexPath.row
        self.lblTitle.isHidden = true
    }
    
    @IBAction func onClickDeliveryTimeAction(sender : UIButton) {
        self.delegateB?.onClickDeliveryTimeAction(sender: sender)
    }
    
}


class DescriptionCell: UITableViewCell {
    
    
    var delegateA : addProductDelgate?
    
    
    @IBOutlet var viewContainer: UIView!
    @IBOutlet var lblTitle: UILabel!{
        didSet {
            self.lblTitle.font = UIFont(name: appFont, size: 15.0)
            self.lblTitle.text = self.lblTitle.text?.localized
        }
    }
    @IBOutlet var textView: UITextView! {
        didSet{
            self.textView.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var lblPlaceholderText: UILabel!{
        didSet {
            self.lblPlaceholderText.font = UIFont(name: appFont, size: 15.0)
            self.lblPlaceholderText.text = self.lblPlaceholderText.text?.localized
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContainer.layer.cornerRadius = 5.0
        self.viewContainer.layer.masksToBounds = true
        self.viewContainer.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewContainer.layer.borderWidth = 1.0
        self.viewContainer.backgroundColor = .white
    }
    
    
    func  WrapCellData( lblTitle : String , placeHolderText : String? , indexPath : IndexPath , inputData : String ) {
        
        self.selectionStyle = .none
        self.lblTitle.text = lblTitle.localized
        self.textView.keyboardType = .default
        self.textView.tag = 10+10*indexPath.row
        self.lblPlaceholderText.tag = 10+10*indexPath.row
        self.lblPlaceholderText.text = placeHolderText?.localized
        self.textView.autocorrectionType = .no
        
        if inputData != "" {
            self.textView.text = inputData
        }
        else {
            self.textView.text = ""
        }
        
    }
    
}
