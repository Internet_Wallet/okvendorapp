//
//  AddProductsViewController.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 06/07/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import Photos
import AVKit
import DKImagePickerController

class AddProductsViewController: OSKBaseViewController, ProductBarScannerDelegate, imageDelete , addProductDelgate {
    
    @IBOutlet var AddProductTblView:ContentSizedTableView!
    @IBOutlet var gestureTapView: UIView!
    @IBOutlet var saveButtonView: UIView!
    @IBOutlet var btnSaveProduct: UIButton!{
        didSet{
           btnSaveProduct.setTitle("Save".localized, for: .normal)
        }
    }
    
    @IBOutlet var btnBackSubCat: UIButton!
    @IBOutlet var titileLbl : UILabel! {
        didSet{
            self.titileLbl.font = UIFont(name: appFont, size: 18.0)
            self.titileLbl.text = self.titileLbl.text?.localized
        }
    }
    
    @IBOutlet var categoryTblBaseViewHeight : NSLayoutConstraint!
    @IBOutlet var CategoryView : UIView!
    @IBOutlet var categoryTableView: UITableView!
    
    
    let descriptionIdentifier = "DescriptionCell"
    let imageCellIdentifier = "addProductTableViewCell"
    let addProductIdentifier = "addProductsFieldsCell"
    let deliveryIdentifier = "DevliveryTimeAddProductCell"
    
    var cellTitle : [String]!
    var cellSubTitle : [String]!
    
    var deliverytimeModel : DeliveryTimeModel?
    var deliveryDateId = ("1"," 2 hours")
    var alertVC : SAlertController?
    
    var imageAndVideoArray : [DKAsset] = [DKAsset]()
    var inputData : [String] = Array.init(repeating: "", count: 10)
    var categoryInputArray : [String] = [String]()
    var categoryInputSubCatArray : [[Category]] = [[Category]]()
    
    let aPIManager = APIManager()
    var apiManagerClient : APIManagerClient?
    
    var categoryDetails : AllCategory?
    var SUbCatList : [Category]?
    
    var categoryID : Int = 0
    var ProductID : Int = -1999
    
    var otherLanguageData : [String : Any]  = [String : Any]()
    var ProductDetails : [String : Any]  = [String : Any]()
    
    var imageData = [Data]()
    var imageName = [String]()
    var videoData = [Data]()
    var videoName = [String]()
    
    var isVideoIncluded : Bool = false
    
    var uploadedImagesCount = 0
    var uploadedVideosCount = 0
    
    var frameOrigionY : CGFloat = 0
    
    
        override func viewDidLoad() {
            super.viewDidLoad()
            
            self.categoryTableView.register(UINib(nibName: CategoryConstant.kScanBarcodeCell, bundle: nil), forCellReuseIdentifier: CategoryConstant.kScanBarcodeTableCell)
            
            self.categoryTableView.register(UINib(nibName: CategoryConstant.kDescriptionCell, bundle: nil), forCellReuseIdentifier: CategoryConstant.kDescriptionCell)
            
            self.categoryTableView.register(UINib(nibName: CategoryConstant.kAddProductTableViewCell, bundle: nil), forCellReuseIdentifier: CategoryConstant.kAddProductTableViewCell)
            
            self.categoryTableView.register(UINib(nibName: CategoryConstant.kAddProductsFieldsCell, bundle: nil), forCellReuseIdentifier: CategoryConstant.kAddProductsFieldsCell)
            
//
//            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
//            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
            self.title = "Add Products".localized
            self.cellTitle = ["Product Name","Product Name In Unicode","Product Price","Category","Delivery Time","Description","Description in Unicode","Full Description","Full Description in Unicode","Product Bar Code"]
            self.cellSubTitle = ["Enter Name","Enter Unicode Name","Enter Price","Select Category","","Enter Description","Enter Description in Unicode","Enter Full Description","Enter Full Description in Unicode","Enter Bar Code"]
        }
    
    override func viewWillDisappear(_ animated: Bool) {
      //  NotificationCenter.default.removeObserver(UIResponder.keyboardWillShowNotification)
       // NotificationCenter.default.removeObserver(UIResponder.keyboardWillHideNotification)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      //  self.getAllCategoriesAPICall()
    }
    
    override func viewDidLayoutSubviews() {
//        if inputData[0] == "" {
//            if let cell = self.getCell(row: 0) as? AddProductsFieldsCell {
//                if let txtFiled = cell.tfValue {
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { // Change `2.0` to the desired number of seconds.
//                        txtFiled.becomeFirstResponder()
//                    }
//                }
//            }
//        }
    }
       
       @IBAction func onClickSaveAction( sender : UIButton) {
        self.view.endEditing(true)
        sender.isUserInteractionEnabled = false
        if self.validateTheUserInputs() {
            if self.otherLanguageData.count > 0 {
                self.ProductDetails.updateValue(self.otherLanguageData, forKey: "OtherLanguageDataUnicode")
            }
            self.addProductAPICall()
        }
        else {
            sender.isUserInteractionEnabled = true
         }
       }
    
    func validateTheUserInputs() -> Bool {
        
        var Productname , Productprice , Productcategory , Productshortdescription , ProductNameUnicode, ProductShortUnicode : String!
        
        Productname = self.ProductDetails["Name"] as? String ?? ""
        Productprice = self.ProductDetails["Price"] as? String ?? ""
        Productcategory = self.ProductDetails["CategoryId"] as? String ?? ""
        Productshortdescription = self.ProductDetails["ShortDescription"] as? String ?? ""
        ProductNameUnicode = self.otherLanguageData["Name"] as? String ?? ""
        ProductShortUnicode = self.otherLanguageData["ShortDescription"] as? String ?? ""
        
        if imageAndVideoArray.count < 1 {
            self.alertVC = SAlertController()
            self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErroMessage.errProductImage, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
            })
            self.alertVC?.addAction(action: [YesAction])
            return false
        }
        if Productname == "" {
            self.alertVC = SAlertController()
            self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErroMessage.errProductName, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
            })
            self.alertVC?.addAction(action: [YesAction])
            return false
        }else if ProductNameUnicode == "" {
            self.alertVC = SAlertController()
            self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErroMessage.errProductNameUnicode, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
            })
            self.alertVC?.addAction(action: [YesAction])
            return false
        }else if Productprice == "" {
            self.alertVC = SAlertController()
            self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErroMessage.errProductPrice, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
            })
            self.alertVC?.addAction(action: [YesAction])
            return false
        }else if Productcategory == "" {
            self.alertVC = SAlertController()
            self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErroMessage.errProductCategory, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
            })
            self.alertVC?.addAction(action: [YesAction])
            return false
        }else if Productshortdescription == "" {
            self.alertVC = SAlertController()
            self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErroMessage.errProductDescription, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
            })
            self.alertVC?.addAction(action: [YesAction])
            return false
        }else if ProductShortUnicode == "" {
            self.alertVC = SAlertController()
            self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErroMessage.errProductShortDisCriptionUnicode, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
            })
            self.alertVC?.addAction(action: [YesAction])
            return false
        }
        if deliveryDateId.1 == "" {
            deliveryDateId.0 = "1"
        }
        self.ProductDetails.updateValue(deliveryDateId.0, forKey: "DeliveryDateId")
        self.ProductDetails.updateValue("true", forKey: "Published")
        return true
    }
        
    func onClickCameraAction(sender : UIButton) {
        self.view.endEditing(true)
           if sender.tag == 10 {
               let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
               let firstAction: UIAlertAction = UIAlertAction(title: "Photo", style: .default) { action -> Void in
                   print("First Action pressed")
                let pickerController = DKImagePickerController()
                pickerController.allowMultipleTypes = true
                let imageCount = 5 - self.imageAndVideoArray.count
                pickerController.maxSelectableCount = imageCount
                pickerController.assetType = .allPhotos
                DKImageExtensionController.registerExtension(extensionClass: CustomCameraExtensionA.self, for: .camera)
                pickerController.didSelectAssets = { (assets: [DKAsset]) in
                    print("didSelectAssetsPhotos")
                    print(assets)
                    self.imageAndVideoArray.append(contentsOf: assets)
                    DispatchQueue.main.async {
                        self.loadImageAndVideoData()
                        self.AddProductTblView.reloadData()
                    }
                }
                self.present(pickerController, animated: true) {}
               }
               let secondAction: UIAlertAction = UIAlertAction(title: "Video", style: .default) { action -> Void in
                   print("Second Action pressed")
                if self.isVideoIncluded {
                    self.alertVC = SAlertController()
                    self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErrorUploading.videoLimitReachError, onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    })
                    self.alertVC?.addAction(action: [YesAction])
                }
                else {
                    let pickerController = DKImagePickerController()
                    pickerController.allowMultipleTypes = true
                    pickerController.maxSelectableCount = 1
                    pickerController.assetType = .allVideos
                    DKImageExtensionController.registerExtension(extensionClass: CustomCameraExtensionB.self, for: .camera)
                    pickerController.didSelectAssets = { (assets: [DKAsset]) in
                        print("didSelectAssetsVideos")
                        print(assets)
                        DispatchQueue.main.async {
                            if assets.count != 0 {
                                self.imageAndVideoArray.append(contentsOf: assets)
                                if let assetVideo = assets.last as? DKAsset {
                                    if assetVideo.type == .video {
                                        self.isVideoIncluded = true
                                    }
                                }
                            }
                            self.loadImageAndVideoData()
                            self.AddProductTblView.reloadData()
                        }
                    }
                    self.present(pickerController, animated: true) {}
                  }
               }
               let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
               actionSheetController.addAction(firstAction)
               actionSheetController.addAction(secondAction)
               actionSheetController.addAction(cancelAction)
               
               actionSheetController.popoverPresentationController?.sourceView = self.view
               present(actionSheetController, animated: true) {
                   print("option menu presented")
               }
           }
           else if sender.tag == 100 {
                DispatchQueue.main.async {
                     if #available(iOS 13.0, *) {
                         let storyBoard = UIStoryboard(name: "EditProduct", bundle: nil)
                         let vc = storyBoard.instantiateViewController(identifier: "ProductBarScanner") as! ProductBarScanner
                         vc.delegate = self
                         self.navigationController?.pushViewController(vc, animated: true)
                     } else {
                         // Fallback on earlier versions
                     }
                 }
             }
             else {
                   self.categoryInputSubCatArray.removeAll()
                   self.SUbCatList = self.categoryDetails?.listCategory
                   self.categoryInputSubCatArray.append(self.categoryDetails?.listCategory ?? [])
                   self.upDateTheUI()
                   self.categoryTableView.reloadData()
                 }
           }
    
    
    @objc func didEditChanged(sender : UITextField) {
       if sender.tag == 30 {
           sender.text = AppUtility.getDigitDisplay(sender.text ?? "")
       }
    }
    
    func upDateTheUI() {
        
        self.CategoryView.frame = self.view.frame
        self.view.addSubview(self.CategoryView)
        self.btnBackSubCat.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        
        let gestureR = UITapGestureRecognizer.init(target: self, action: #selector(close))
        gestureR.numberOfTapsRequired = 1
        gestureR.delegate = self
        self.gestureTapView.addGestureRecognizer(gestureR)
        
        UIView.animate(withDuration: 0.2) {
        let height = (self.SUbCatList?.count ?? 1) * 55 + 50
         if height < 400 {
             self.categoryTblBaseViewHeight.constant = CGFloat(height)
         }
         else {
                self.categoryTblBaseViewHeight.constant = 420
             }
          }
    }
    
    @objc func closeAction () {
        
        if categoryInputArray.count == 0 {
            UIView.animate(withDuration: 0.2) {
                let frame = CGRect.init(x: 20, y: self.view.frame.size.height / 2 + 10 , width: self.view.frame.size.width - 40 , height: 0)
                self.CategoryView.frame = frame
                self.CategoryView.removeFromSuperview()
            }
        }
        else {
            self.categoryInputArray.removeLast()
            self.categoryInputSubCatArray.removeLast()
            self.SUbCatList = self.categoryInputSubCatArray.last
            self.upDateTheUI()
            self.categoryTableView.reloadData()
        }
    }
    
    @objc func close () {
        UIView.animate(withDuration: 0.2) {
            let frame = CGRect.init(x: 20, y: self.view.frame.size.height / 2 + 10 , width: self.view.frame.size.width - 40 , height: 0)
            self.CategoryView.frame = frame
            self.CategoryView.removeFromSuperview()
        }
    }
    
    func loadImageAndVideoData () {
        
        imageData.removeAll()
        imageName.removeAll()
        videoData.removeAll()
        videoName.removeAll()
        
        for (item,obj) in imageAndVideoArray.enumerated() {
                
            let asset = imageAndVideoArray[item]
            if obj.type == .photo{
                asset.fetchOriginalImage(completeBlock: { image, info in
                    let data:Data = image?.jpegData(compressionQuality: 0.5) ?? Data()
                    self.imageData.append(data)
                    let name = String(Date().toMillis())
                    self.imageName.append("\(name).jpeg")
                })

            }else{
                guard let assetData = obj.originalAsset else { return }
                        self.convertVideo(phAsset: assetData)
                
            }
             
        }
        
//        for (_ ,object )in self.imageAndVideoArray.enumerated() {
//                   if object.type == .photo {
//                    let assetImage : PHAsset = object.originalAsset ?? PHAsset()
//                    var img: UIImage?
//                    let manager = PHImageManager.default()
//                    let options = PHImageRequestOptions()
//                    options.version = .original
//                    options.isSynchronous = true
//                    manager.requestImageData(for: assetImage, options: options) { data, _, _, _ in
//                        if let data = data {
//                            img = UIImage(data: data)
//                            let data:Data = img?.jpegData(compressionQuality: 0.5) ?? Data()
//                            self.imageData.append(data)
//                            let name = String(Date().toMillis())
//                            self.imageName.append("\(name).jpeg")
//                        }
//                      }
//                   }
//                   if object.type == .video {
//                    guard let assetData = object.originalAsset else { return }
//                    self.convertVideo(phAsset: assetData)
//                    // notify the main thread when all task are completed
//                 }
//            }
        }
    
    
    func convertVideo(phAsset : PHAsset){

       PHImageManager.default().requestAVAsset(forVideo: phAsset, options: PHVideoRequestOptions(), resultHandler: { (asset, audioMix, info) -> Void in
           if let asset = asset as? AVURLAsset {
               do {
                   let videoData = try  Data.init(contentsOf: asset.url)
                   print(asset.url)
                   print("File size before compression: \(Double(videoData.count / 1048576)) mb")
                   let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".MP4")
                   print(compressedURL)
                   self.compressVideo(inputURL: asset.url , outputURL: compressedURL) { (exportSession) in
                       guard let session = exportSession else {
                           return
                       }
                       switch session.status {
                       case .unknown:
                           print("unknown")
                           break
                       case .waiting:
                           print("waiting")
                           break
                       case .exporting:
                           print("exporting")
                           break
                       case .completed:
                           do {
                           let compressedData = try  Data.init(contentsOf: compressedURL)
                                print(compressedData)
                                print("File size AFTER compression: \(Double(compressedData.count / 1048576)) mb")
                                self.videoData.append(compressedData as Data)
                                let name = String(Date().toMillis())
                                self.videoName.append("\(name).mp4")
                           }
                           catch{
                              print(error)
                           }

                       case .failed:
                           print("failed")
                           break
                       case .cancelled:
                           print("cancelled")
                           break
                       }
                   }
               } catch {
                   print(error)
                   //return
               }
           }
       })


   }

   func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
       let urlAsset = AVURLAsset(url: inputURL, options: nil)
       guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
           handler(nil)
           return
       }
       exportSession.outputURL = outputURL
       exportSession.outputFileType = AVFileType.mp4
       exportSession.shouldOptimizeForNetworkUse = true
       exportSession.exportAsynchronously { () -> Void in
           handler(exportSession)
       }
   }

       func getCell(row: Int) -> AddProductsFieldsCell {
        if let cell = AddProductTblView.cellForRow(at: IndexPath(row: row, section: 0)) as? AddProductsFieldsCell {
            return cell
        }
        return AddProductsFieldsCell()
       }
    
      func deleteImageContentAt(index: Int) {
        if let object = self.imageAndVideoArray[index] as? DKAsset {
            if object.type == .video {
                self.isVideoIncluded = false
            }
        }
        self.imageAndVideoArray.remove(at: index)
           DispatchQueue.main.async {
             self.AddProductTblView.reloadData()
           }
        }
    
    func palyVideo(index : Int) {
        let asset = self.imageAndVideoArray[index]
               asset.fetchAVAsset { (avAsset, info) in
                   DispatchQueue.main.async(execute: { () in
                       self.playVideo(avAsset!)
              })
         }
    }
    
    func playVideo(_ asset: AVAsset) {
            let avPlayerItem = AVPlayerItem(asset: asset)
            let avPlayer = AVPlayer(playerItem: avPlayerItem)
            let player = AVPlayerViewController()
            player.player = avPlayer
            avPlayer.play()
            self.present(player, animated: true, completion: nil)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }

}


extension AddProductsViewController {
  
    //MARK:- GetAllCategOryAPI
        func getAllCategoriesAPICall(){
            self.apiManagerClient = APIManagerClient.sharedInstance
            if AppUtility.isConnectedToNetwork() {
                AppUtility.showLoading(self.view)
                self.aPIManager.getAllCategories(onSuccess: {
                    self.categoryDetails = self.apiManagerClient?.allCategories
                    self.SUbCatList = self.categoryDetails?.listCategory
                    if let viewLoc = self.view {
                           AppUtility.hideLoading(viewLoc)
                       }
                }, onError: { [weak self] message in
                     if let viewLoc = self?.view {
                            AppUtility.hideLoading(viewLoc)
                    }
                })
            }
        }
    
    //MARK:- ADDProductAPI
    func addProductAPICall(){
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            self.aPIManager.createProduct(productDetails: self.ProductDetails , onSuccess: { product in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                }
                let productIDServer = product["ProductId"] as? Int
                self.ProductID = productIDServer ?? 0
                DispatchQueue.main.async {
                    if self.imageAndVideoArray.count > 0 {
                        self.uploadedImagesCount = self.imageData.count
                        self.uploadedVideosCount = self.videoData.count
                        if self.isVideoIncluded {
                            self.uploadVideoToserver(productID: self.ProductID)
                        }
                        else {
                            self.uploadImageToserver(productID: self.ProductID)
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            let sb = UIStoryboard(name: "EditProduct", bundle: nil)
                            if #available(iOS 13.0, *) {
                                let editVC = sb.instantiateViewController(identifier: "EditProducts") as! EditProducts
                                editVC.productID = String(self.ProductID)
                                editVC.comingFrom = "AddProduct"
                                self.navigationController?.pushViewController(editVC, animated: true)
                                return
                            }else {

                            }
                        }
                    }
                   }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                        self?.btnSaveProduct.isUserInteractionEnabled = true
                }
           })
        }
    }
    

    //MARK:- ImageUpload
    func uploadImageToserver(productID : Int)  {
        if imageData.count > 0 {
            self.imageUpload(productID: String(productID) , imgName: imageName[uploadedImagesCount - 1], fileData: imageData[uploadedImagesCount - 1], handler: { isSuccess in
                if isSuccess {
                    self.uploadedImagesCount = self.uploadedImagesCount - 1
                    if self.uploadedImagesCount == 0 {
                        DispatchQueue.main.async {
                            let sb = UIStoryboard(name: "EditProduct", bundle: nil)
                            if #available(iOS 13.0, *) {
                                let editVC = sb.instantiateViewController(identifier: "EditProducts") as! EditProducts
                                editVC.productID = String(self.ProductID)
                                editVC.comingFrom = "AddProduct"
                                self.navigationController?.pushViewController(editVC, animated: true)
                                return
                            }else {

                            }
                        }
                    }else {
                        self.uploadImageToserver(productID: productID)
                    }
                }else {
                    self.alertVC = SAlertController()
                    self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErrorUploading.messageImage, onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    })
                    self.alertVC?.addAction(action: [YesAction])
                    return
                }
            })
        }
     }
    
    //MARK:- Create Body for the Image Uploading
        func createBodyWithParametersForImage(parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> NSData {
            var body = Data()
            if parameters != nil {
                for (key, value) in parameters! {
                    body.append(Data("--\(boundary)\r\n".utf8))
                    body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                    body.append(Data("\(value)\r\n".utf8))
                }
            }
            var filename = ""
            if filePathKey != "" {
                let fileArray = filePathKey?.components(separatedBy: "/")
                filename = (fileArray?.last)!
            }
            let KeyName = "File"
            let mimetype = "image/jpeg"
            body.append(Data("--\(boundary)\r\n".utf8))
            body.append(Data("Content-Disposition: form-data; name=\"\(KeyName)\"; filename=\"\(filename)\"\r\n".utf8))
            body.append(Data("Content-Type: \(mimetype)\r\n\r\n".utf8))
            body.append(imageDataKey as Data)
            body.append(Data("\r\n".utf8))
            body.append(Data("--\(boundary)--\r\n".utf8))
            return body as NSData
        }
    
    //MARK:- VideoUpload
    func uploadVideoToserver(productID : Int)  {
        if videoData.count > 0 {
            self.videoUpload(productID: String(productID) , videoName: imageName[uploadedVideosCount - 1] , fileData: imageData[uploadedVideosCount - 1] , handler: { (isSuccess) in
                if isSuccess {
                       self.uploadImageToserver(productID: productID)
                   // self.uploadedVideosCount = self.uploadedVideosCount - 1
//                    if self.uploadedImagesCount == 0 {
//                        DispatchQueue.main.async {
//
//                        }
//                    }else {
//                        self.uploadImageToserver(productID: productID)
//                    }
                }else {
                    self.alertVC = SAlertController()
                    self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErrorUploading.messageImage, onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    })
                    self.alertVC?.addAction(action: [YesAction])
                    return
                }
            })
        }
     }
    
    
  }



extension AddProductsViewController {
    
        //MARK:- UPLOAD IMAGE
        func imageUpload(productID: String, imgName: String , fileData: Data ,handler: @escaping (_ isSuccess: Bool) -> Void) {
            
            self.apiManagerClient = APIManagerClient.sharedInstance
            if AppUtility.isConnectedToNetwork() {
                AppUtility.showLoading(self.view)
                let urlString = String(format: "%@/vendor/addproductpicture", APIManagerClient.sharedInstance.base_url) as String
                println_debug(urlString)
                let params = ["ProductId": productID.trimmingCharacters(in: .whitespaces),"DisplayOrder": "1","OverrideAltAttribute": "test","OverrideTitleAttribute": "test"]
                println_debug(urlString)
                let request = NSMutableURLRequest(url:URL(string: urlString)!)
                request.httpMethod = "POST";
                let boundary = "Boundary-\(UUID2)"
                request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                request.httpBody = createBodyWithParametersForImage(parameters: params, filePathKey: imgName, imageDataKey: fileData as Data, boundary: boundary) as Data
                request.setValue(Vendor_loginmodel.shared.token, forHTTPHeaderField: "Token")
                request.setValue(aPIManager.nstKEY, forHTTPHeaderField: "NST")
                request.setValue(aPIManager.deviceId, forHTTPHeaderField: "DeviceId")
                let task = URLSession.shared.dataTask(with: request as URLRequest) {
                    (data, response, error) in
                    DispatchQueue.main.async {
                        do {
                            let responseX =  try JSONSerialization.jsonObject(with: data ?? Data(), options: []) as? [String: Any]
                            println_debug(responseX)
                        } catch { print(error.localizedDescription)}
                        if let viewLoc = self.view {
                            AppUtility.hideLoading(viewLoc)
                        }
                        if error != nil {
                            handler(false)
                            return
                        }
                        println_debug(response)
                        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                           handler(true)
                        }else {
                            handler(false)
                        }
                    }
                }
                task.resume()
            }
        }
    
    
    //MARK:- UPLOAD VIDEO
    func videoUpload(productID: String, videoName : String , fileData: Data ,handler: @escaping (_ isSuccess: Bool) -> Void) {
        
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlString = String(format: "%@/vendor/addproductvideo", APIManagerClient.sharedInstance.base_url) as String
            println_debug(urlString)
            let params = ["ProductId": productID.trimmingCharacters(in: .whitespaces),"DisplayOrder": "1","OverrideAltAttribute": "test","OverrideTitleAttribute": "test"]
            println_debug(urlString)
            let request = NSMutableURLRequest(url:URL(string: urlString)!)
            request.httpMethod = "POST";
            let boundary = "Boundary-\(UUID2)"
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.httpBody = createBodyWithParametersForVideo(parameters: params, filePathKey: videoName , imageDataKey: fileData as Data, boundary: boundary) as Data
            request.setValue(Vendor_loginmodel.shared.token, forHTTPHeaderField: "Token")
            request.setValue(aPIManager.nstKEY, forHTTPHeaderField: "NST")
            request.setValue(aPIManager.deviceId, forHTTPHeaderField: "DeviceId")
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                (data, response, error) in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    do {
                        let responseX =  try JSONSerialization.jsonObject(with: data ?? Data(), options: []) as? [String: Any]
                        println_debug(responseX)
                    } catch { print(error.localizedDescription)}
                    if error != nil {
                        handler(false)
                        return
                    }
                    println_debug(response)
                    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                       handler(true)
                    }else {
                        handler(false)
                    }
                }
            }
            task.resume()
        }
    }
        
    
    
    //MARK:- Create Body for the Video Uploading
    func createBodyWithParametersForVideo(parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> NSData {
        var body = Data()
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(Data("--\(boundary)\r\n".utf8))
                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data("\(value)\r\n".utf8))
            }
        }
        var filename = ""
        if filePathKey != "" {
            let fileArray = filePathKey?.components(separatedBy: "/")
            filename = (fileArray?.last)!
        }
        let KeyName = "File"
        let mimetype = "video/mp4"
        body.append(Data("--\(boundary)\r\n".utf8))
        body.append(Data("Content-Disposition: form-data; name=\"\(KeyName)\"; filename=\"\(filename)\"\r\n".utf8))
        body.append(Data("Content-Type: \(mimetype)\r\n\r\n".utf8))
        body.append(imageDataKey as Data)
        body.append(Data("\r\n".utf8))
        body.append(Data("--\(boundary)--\r\n".utf8))
        return body as NSData
     }
  } 


extension AddProductsViewController : UITableViewDelegate,UITableViewDataSource {
    
    //MARK:- TableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == categoryTableView {
            return self.SUbCatList?.count ?? 1
        }else {
            if imageAndVideoArray.count == 0 {
                return 10
            }
            return 11
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == categoryTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoryListCell", for: indexPath) as! CategoryListCell
            if let catData = self.SUbCatList?[indexPath.row] {
                cell.lblTitle.text = catData.name ?? "Select Category"
            }
            cell.selectionStyle = .none
            return cell
        }else {
            if imageAndVideoArray.count > 0 {
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: imageCellIdentifier , for: indexPath) as! AddProductTableViewCell
                    //cell.wrapData(cell: cell, ImageData: self.imageAndVideoArray)
                    cell.delegateX = self
                    cell.selectionStyle = .none
                    return cell
                }
                else {
                    let indexPathNew = NSIndexPath(row: (indexPath.row - 1), section: indexPath.section)
                    if indexPathNew.row == 4 {
                        let cell = tableView.dequeueReusableCell(withIdentifier: deliveryIdentifier, for: indexPath) as! DevliveryTimeAddProductCell
                        cell.WrapCellData(lblTitle: self.cellTitle[indexPath.row - 1], indexPath: indexPathNew as IndexPath, ProductDetails: self.ProductDetails , deliveryData: self.deliveryDateId)
                        cell.delegateB = self
                        return cell
                    }
                    else if (indexPathNew.row > 4) && (indexPathNew.row < 9) {
                        let cell = tableView.dequeueReusableCell(withIdentifier: descriptionIdentifier, for: indexPath) as! DescriptionCell
                        cell.WrapCellData(lblTitle: self.cellTitle[indexPath.row - 1], placeHolderText: self.cellSubTitle[indexPath.row - 1], indexPath: indexPathNew as IndexPath , inputData: self.inputData[indexPathNew.row])
                        cell.delegateA = self
                        return cell
                    }
                    else {
                        let cell = tableView.dequeueReusableCell(withIdentifier: addProductIdentifier, for: indexPath) as! AddProductsFieldsCell
                        cell.WrapCellData(lblTitle: self.cellTitle[indexPath.row - 1], placeHolderText: self.cellSubTitle[indexPath.row - 1], indexPath: indexPathNew as IndexPath , inputData: self.inputData[indexPathNew.row])
                        cell.delegateA = self
                        return cell
                    }
                }
            }else {
                if indexPath.row == 4 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: deliveryIdentifier , for: indexPath) as! DevliveryTimeAddProductCell
                    cell.WrapCellData(lblTitle: self.cellTitle[indexPath.row], indexPath: indexPath as IndexPath, ProductDetails: self.ProductDetails, deliveryData: self.deliveryDateId)
                    cell.delegateB = self
                    return cell
                }
                else if (indexPath.row > 4) && (indexPath.row < 9) {
                    let cell = tableView.dequeueReusableCell(withIdentifier: descriptionIdentifier, for: indexPath) as! DescriptionCell
                    cell.WrapCellData(lblTitle: self.cellTitle[indexPath.row], placeHolderText: self.cellSubTitle[indexPath.row], indexPath: indexPath , inputData: self.inputData[indexPath.row])
                    cell.delegateA = self
                    return cell
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: addProductIdentifier, for: indexPath) as! AddProductsFieldsCell
                    cell.WrapCellData(lblTitle: self.cellTitle[indexPath.row], placeHolderText: self.cellSubTitle[indexPath.row], indexPath: indexPath , inputData: self.inputData[indexPath.row])
                    cell.delegateA = self
                    return cell
                }
            }
        }
    }
    
    //MARK:- TableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var isNextSubCategory = false
        
        if tableView == categoryTableView {
            if let SUbCat = self.SUbCatList?[indexPath.row] {
                if let SubCatData = SUbCat.Children , SubCatData.count > 0 {
                    self.categoryInputArray.append(SUbCat.name ?? "")
                    self.categoryInputSubCatArray.append(SUbCat.Children ?? [])
                    self.SUbCatList = SUbCat.Children
                    isNextSubCategory = true
                }
            }
            if self.SUbCatList!.count > 0 && isNextSubCategory {
                self.upDateTheUI()
                self.categoryTableView.reloadData()
            }else {
                var catName = ""
                if let catData = self.SUbCatList?[indexPath.row] {
                    self.categoryID = catData.Id ?? 0
                    self.ProductDetails.updateValue("\(self.categoryID)",forKey: "CategoryId")
                    for (_ ,name) in self.categoryInputArray.enumerated() {
                        catName.append(name)
                        catName.append(">>")
                    }
                    catName.append(catData.name ?? "")
                    self.insertElementAtIndex(element: catName , index: 3)
                }
                if imageAndVideoArray.count > 0 {
                    let cellY = self.getCell(row: 4)
                    cellY.tfValue.text = ""
                    cellY.tfValue.text = catName
                }else {
                        let cellY = self.getCell(row: 3)
                        cellY.tfValue.text = ""
                        cellY.tfValue.text = catName
                   }
                UIView.animate(withDuration: 0.2) {
                    let frame = CGRect.init(x: 20, y: self.view.frame.size.height / 2 + 10 , width: self.view.frame.size.width - 40 , height: 0)
                    self.CategoryView.frame = frame
                    self.categoryInputArray.removeAll()
                    self.CategoryView.removeFromSuperview()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == categoryTableView {
            return 55
        }else {
            if imageAndVideoArray.count > 0 {
                if indexPath.row == 0 {
                    return 220
                }else{
                    return UITableView.automaticDimension
                }
            }else {
                return UITableView.automaticDimension
            }
        }
    }
}



