//
//  AddProductNewViewController.swift
//  OSKVendor
//
//  Created by Tushar Lama on 29/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import Photos
import AVKit
import DKImagePickerController
import Alamofire
import SwiftyJSON


protocol AddProductParamProtocol {
    func param(name: String,value: String)
}

class AddProductNewViewController: OSKBaseViewController, ScanBarcodeDelegate {

    var sizeAttributeModel = [AttributeModel]()
    var categoryName = ""
    @IBOutlet var productTableView: UITableView!
    var titleLableHeightDic = NSMutableDictionary()
    var alertVC : SAlertController?
    var imageAndVideoArray : [DKAsset] = [DKAsset]()
    var imageData = [Data]()
    var imageName = [String]()
    var imageDataFromServer = [Data]()
    var imageNameFromServer = [String]()
    var videoData = [Data]()
    var videoName = [String]()
    var isVideoIncluded : Bool = false
    var addProductParam = NSMutableDictionary()
    var selectedCategory: Int?
    let aPIManager = APIManager()
    var apiManagerClient : APIManagerClient?
    var ProductID : Int = -1999
    var barCodeNumber = ""

    var otherLanguageData : [String : Any]  = [String : Any]()
    var ProductDetails : [String : Any]  = [String : Any]()
    var uploadedImagesCount = 0
    var uploadedVideosCount = 0
    var allCategoryModel : GetAllCategoriesModel?
    var productDetailsModel: GetPrdouctDetailsModel?
    var modelObj: GetProductDetailsModel?
    
    var isHavingAttributes = false
    var callApi = false
    var descriptionTextFieldHeight = CGFloat()
    var descriptionUnicodeTextFieldHeight = CGFloat()
    var categoryTextFieldHeight = CGFloat()
    var increaseCount = 0
    var isComingFromEdit = false
    var isComingFromBarCode = false

    
    @IBOutlet var addMoreDetailsButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
        self.title = "Add Product".localized
        productTableView.register(UINib(nibName: "ImageVideoCell", bundle: nil), forCellReuseIdentifier: "ImageVideoCell")

        productTableView.register(UINib(nibName: CategoryConstant.kScanBarcodeCell, bundle: nil), forCellReuseIdentifier: CategoryConstant.kScanBarcodeTableCell)
        
        productTableView.register(UINib(nibName: CategoryConstant.kAddProductCell, bundle: nil), forCellReuseIdentifier: CategoryConstant.kAddProductCell)
        
        productTableView.register(UINib(nibName: CategoryConstant.kPriceUMOTableViewXIB, bundle: nil), forCellReuseIdentifier: CategoryConstant.kPriceUMOTableViewCell)

        productTableView.register(UINib(nibName: CategoryConstant.kAddProductTableViewCell, bundle: nil), forCellReuseIdentifier: CategoryConstant.kAddProductTableViewCell)
        
        productTableView.register(UINib(nibName: CategoryConstant.kEditProductImagesCell, bundle: nil), forCellReuseIdentifier: CategoryConstant.kEditProductImagesCell)
        
        productTableView.register(UINib(nibName: CategoryConstant.kAddUOMCell, bundle: nil), forCellReuseIdentifier: CategoryConstant.kAddUOMCell)
        
        productTableView.register(UINib(nibName: CategoryConstant.kProductDescriptionCell, bundle: nil), forCellReuseIdentifier: CategoryConstant.kProductDescriptionCell)
        
        productTableView.register(UINib(nibName: CategoryConstant.kProductDescriptionUnicodeCell, bundle: nil), forCellReuseIdentifier: CategoryConstant.kProductDescriptionUnicodeCell)
        
        productTableView.register(UINib(nibName: CategoryConstant.kAddMoreDetails, bundle: nil), forCellReuseIdentifier: "AddMoreAttributeCell")
        
        productTableView.register(UINib(nibName: CategoryConstant.kSelectProductCategoryCell, bundle: nil), forCellReuseIdentifier: "SelectProductCategoryCell")
        
        
        productTableView.register(UINib(nibName: "EditProductImageCell", bundle: nil), forCellReuseIdentifier: "EditProductImagesNewCell")
        
        if isHavingAttributes{
            addMoreDetailsButton.setTitle("Add More Details".localized, for: .normal)
        }else{
            addMoreDetailsButton.setTitle("Save".localized, for: .normal)
        }

        productTableView.separatorColor = .clear
        if isComingFromEdit || isComingFromBarCode {
            self.getAllCategories()
        }else{
            productTableView.delegate = self
            productTableView.dataSource = self
            productTableView.reloadData()
        }
    }
    
    func getAllCategories() {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/allcategories", APIManagerClient.sharedInstance.base_url) as String
            self.aPIManager.getGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        return
                    }
                    let decode = JSONDecoder()
                    self?.allCategoryModel = try? decode.decode(GetAllCategoriesModel.self, from: jsonData)
//                    if ((self?.isComingFromEdit) == true) {
//                        self?.getProductDetialsByID(productID:  "\(self?.ProductID ?? 0)" , reloadFor: "")
//                    } else if ((self?.isComingFromBarCode) == true) {
//                        self?.getProductDetialsByBarCode(barCode: self?.barCodeNumber ?? "" , reloadFor: "")
//                    }
                    self?.getProductDetialsByID(productID:  "\(self?.ProductID ?? 0)" , reloadFor: "")

                }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        self?.getProductDetialsByID(productID:  "\(self?.ProductID ?? 0)" , reloadFor: "")
                    AppUtility.hideLoading(viewLoc)
                    }
            })
        }
    }
    
    func setNavigationBar() {

        self.navigationItem.setHidesBackButton(true, animated:false)

        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: -10, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: -15, y: 0, width: 40, height: 40))

        if let imgBackArrow = UIImage(named: "backarrow") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)

        let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMain))
        view.addGestureRecognizer(backTap)

        let leftBarButtonItem = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }

    @objc func backToMain() {
        if ProductID != -1999 {
            if AppUtility.isConnectedToNetwork() {
                let alertView = SAlertController()
                alertView.ShowSAlert(title: "OSK Vendor".localized, withDescription: "Are you sure you want to discard product ?".localized, onController: self)
                let continueAction = SAlertAction()
                continueAction.action(name: "CONTINUE TO SAVE".localized, AlertType: .defualt, withComplition: {
                    println_debug("CONTINUE TO SAVE")
                })
                let discardAction = SAlertAction()
                discardAction.action(name: "DISCARD".localized, AlertType: .defualt, withComplition: {
                    //Delete Record while discard
                    AppUtility.showLoading(self.view)
                    self.aPIManager.deleteProduct(productID: self.ProductID) {
                        DispatchQueue.main.async {
                            if let viewLoc = self.view {
                                AppUtility.hideLoading(viewLoc)
                                DispatchQueue.main.async {
                                    self.navigationController?.popToRootViewController(animated: false)
                                }
                            }
                        }
                    } onError: { [weak self] message in
                        if let viewLoc = self?.view {
                            AppUtility.hideLoading(viewLoc)
                            DispatchQueue.main.async {
                                self?.navigationController?.popToRootViewController(animated: false)
                            }
                        }
                    }
                })
                alertView.addAction(action: [continueAction,discardAction])
                
            }
            else {
                self.showAlertInClass(title: "",subTitle: "Please Make Sure You Have a Valid Internet Connection".localized)
            }
        }else{
            //this means product is not saved by the vendor in this case just pop back
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    func playVideo(_ asset: AVAsset) {
            let avPlayerItem = AVPlayerItem(asset: asset)
            let avPlayer = AVPlayer(playerItem: avPlayerItem)
            let player = AVPlayerViewController()
            player.player = avPlayer
            avPlayer.play()
            self.present(player, animated: true, completion: nil)
    }
    
    func showAlertInClass(title: String,subTitle: String) {
        self.alertVC = SAlertController()
        self.alertVC?.ShowSAlert(title: title, withDescription: subTitle, onController: self)
        let YesAction = SAlertAction()
        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
        })
        self.alertVC?.addAction(action: [YesAction])
    }
    
    func updateProductDetails(params: [String:Any], handler:@escaping (_ succes: Bool) -> Void) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/updateproductfromvendor", APIManagerClient.sharedInstance.base_url) as String
            print("Get allcategories :\(urlStr)\n params\(params)")
            
            self.aPIManager.postAPIParams(url: urlStr,productDetails: params, successBlock: { (dic,isSuccess) in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                    if isSuccess {
                        handler(true)
                    }else {
                        handler(false)
                    }
                }
            }, errorBlock: { (error) in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                }
                handler(false)
            })
        }
    }
    

    @IBAction func onClickNext(_ sender: Any) {
        
        if isComingFromBarCode && !isHavingAttributes{
            if self.validateTheUserInputs(){
                self.apiManagerClient = APIManagerClient.sharedInstance
                self.addProductAPICall()
            }
        }else if isComingFromBarCode && isHavingAttributes{
            
            if callApi &&  addMoreDetailsButton.currentTitle == "Save".localized{
                if self.validateTheUserInputs(){
                    self.apiManagerClient = APIManagerClient.sharedInstance
                    self.addProductAPICall()
                }
            } else{
                //move to  attribute screen
                DispatchQueue.main.async {
                    var vcObj: AttributeViewController?
                    vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.kAttributeViewController) as? AttributeViewController
                    if let validObj = vcObj {
                        validObj.saveDelegate = self
                        validObj.productDetailsModel = self.modelObj
                        self.navigationController?.pushViewController(validObj, animated: true)
                    }
                }
                
            }
        }else{
            
            //this flow will work for add new product
            if self.validateTheUserInputs(){
                if callApi &&  addMoreDetailsButton.currentTitle == "Save".localized{
                    self.apiManagerClient = APIManagerClient.sharedInstance
                    self.updateProductDetails(params: ProductDetails, handler: { isSuccess in
                        if isSuccess {
                            println_debug("Product updated")
                            self.alertVC = SAlertController()
                            self.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "Product successfully Saved".localized, onController: self)
                            let YesAction = SAlertAction()
                            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                            self.navigationController?.popToRootViewController(animated: true)
                            })
                            self.alertVC?.addAction(action: [YesAction])
                        }else {
                            println_debug("Product updated failed")
                            self.showAlertInClass(title: "Error".localized, subTitle: "Product editing is failed".localized)
                        }
                    })
                }else{
                    self.addProductAPICall()
                }
            }
            
        }
    
    }
    
    func addProductAPICall(){
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            self.aPIManager.createProduct(productDetails: self.ProductDetails , onSuccess: { product in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                }
                let productIDServer = product["ProductId"] as? Int
                self.ProductID = productIDServer ?? 0
                                
                    DispatchQueue.main.async {
                            self.uploadedImagesCount = self.imageDataFromServer.count
                            self.uploadedVideosCount = self.videoData.count
                            if self.isVideoIncluded {
                                self.uploadVideoToserver(productID: self.ProductID)
                            }
                            else {
                                self.uploadImageToserver(productID: self.ProductID)
                            }
                       }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                }
           })
        }
    }
    
}


extension AddProductNewViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isComingFromBarCode{
            if modelObj?.data?.pictureModels?.count ?? 0 > 0 || ((modelObj?.data?.videoModels?.count ?? 0) != 0) || imageAndVideoArray.count>0{
                if isHavingAttributes{
                    if callApi && addMoreDetailsButton.currentTitle == "Save".localized{
                        return 9
                    }else{
                        return 8
                    }
                }
                return 8
            }
            else{
                if isHavingAttributes{
                    if callApi && addMoreDetailsButton.currentTitle == "Save".localized{
                        return 9
                    }else{
                        return 8
                    }
                }
                return 8
            }
            
        }else{
            if imageAndVideoArray.count>0{
                if isHavingAttributes{
                    if callApi && addMoreDetailsButton.currentTitle == "Save".localized{
                        return 9
                    }else{
                        return 8
                    }
                    
                }else{
                    return 8
                }
            }else{
                return 7
            }
    }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //this check will work when coming from my product and edit
        
        if isComingFromBarCode{
            
            if modelObj?.data?.pictureModels?.count ?? 0 > 0 || ((modelObj?.data?.videoModels?.count ?? 0) != 0) || imageAndVideoArray.count>0{
                
                switch  indexPath.row {
                    
                case 0:
                    
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ImageVideoCell" , for: indexPath) as? ImageVideoCell else{
                        return UITableViewCell()
                    }
                   
                    cell.selectionStyle = .none
                    cell.delegateX = self
                    cell.wrapDataValue(ImageDataValue : imageDataFromServer)
                    return cell
                
                case 1:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kScanBarcodeTableCell) as? ScanBarcodeTableCell  else {
                        return UITableViewCell()
                    }
                    return cell
                    
                case 2,3:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kAddProductCell) as? AddProductCell  else {
                        return UITableViewCell()
                    }
                    if indexPath.row == 2{
                        cell.addProductTextView.tag = 1
                        cell.titleLabel.tag = 1
                        
                        let value =  addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductNameEnglish.rawValue) as? String ?? ""
                        cell.setDataOnCell(tag: 1,titleValue: value)
                    }else{
                        cell.addProductTextView.tag = 2
                        cell.titleLabel.tag = 2
                        let value =  addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductNameUnicode.rawValue) as? String ?? ""
                        cell.setDataOnCell(tag: 2,titleValue: value)
                    }
                    cell.productParamDel = self
                    cell.titleDelegate = self
                    cell.cameraDelegate = self
                    cell.selectionStyle = .none
                    return cell
                    
                case 4:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kPriceUMOTableViewCell) as? PriceUMOTableViewCell  else {
                        return UITableViewCell()
                    }
                    
                    let valuePrice =  addProductParam.value(forKey: CategoryConstant.AddProductKey.ProducPrice.rawValue) as? String ?? ""
                    cell.setPrice(price: valuePrice)
                    
                    cell.productParamDel = self
                    return cell
                    
                case 5:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kAddUOMCell) as? UOMCell  else {
                        return UITableViewCell()
                    }
                    let valueUOM = addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductUom.rawValue) as? String ?? ""
                    cell.setUOM(UOM: valueUOM)
                    cell.productParamDel = self
                    
                    return cell
              
                
               
                case 6:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kProductDescriptionCell) as? ProductDescriptionCell  else {
                        return UITableViewCell()
                    }
                    cell.delgate = self
                    cell.productParamDel = self
                    let value = addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductDescription.rawValue) as? String ?? ""
                    
                    cell.setValue(value: value)
                    
                    return cell
               
                case 7:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kProductDescriptionUnicodeCell) as? ProductDescriptionUnicodeCell  else {
                        return UITableViewCell()
                    }
                    cell.delgate = self
                    cell.productParamDel = self
                    
                    return cell
                 
                case 8:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddMoreAttributeCell") as? AddMoreAttributeCell  else {
                        return UITableViewCell()
                    }
                    
                    return cell
                
                default:
                    break
                    
                }
                
            }else{
                
                switch  indexPath.row {
                    
                
                case 0:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kScanBarcodeTableCell) as? ScanBarcodeTableCell  else {
                        return UITableViewCell()
                    }
                    return cell
                    
                case 1,2:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kAddProductCell) as? AddProductCell  else {
                        return UITableViewCell()
                    }
                    if indexPath.row == 1{
                        cell.addProductTextView.tag = 1
                        cell.titleLabel.tag = 1
                        
                        let value =  addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductNameEnglish.rawValue) as? String ?? ""
                        cell.setDataOnCell(tag: 1,titleValue: value)
                    }else{
                        cell.addProductTextView.tag = 2
                        cell.titleLabel.tag = 2
                        let value =  addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductNameUnicode.rawValue) as? String ?? ""
                        cell.setDataOnCell(tag: 2,titleValue: value)
                    }
                    cell.productParamDel = self
                    cell.titleDelegate = self
                    cell.cameraDelegate = self
                    cell.selectionStyle = .none

                    return cell
                    
                case 3:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kPriceUMOTableViewCell) as? PriceUMOTableViewCell  else {
                        return UITableViewCell()
                    }
                    
                    let valuePrice =  addProductParam.value(forKey: CategoryConstant.AddProductKey.ProducPrice.rawValue) as? String ?? ""
                    cell.setPrice(price: valuePrice)
                    
                    cell.productParamDel = self
                    return cell
                    
                case 4:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kAddUOMCell) as? UOMCell  else {
                        return UITableViewCell()
                    }
                    let valueUOM = addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductUom.rawValue) as? String ?? ""
                    cell.setUOM(UOM: valueUOM)
                    cell.productParamDel = self
                    
                    return cell
              
               
               
                case 5:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kProductDescriptionCell) as? ProductDescriptionCell  else {
                        return UITableViewCell()
                    }
                    cell.delgate = self
                    cell.productParamDel = self
                    let value = addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductDescription.rawValue) as? String ?? ""
                    
                    cell.setValue(value: value)
                    
                    return cell
               
                case 6:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kProductDescriptionUnicodeCell) as? ProductDescriptionUnicodeCell  else {
                        return UITableViewCell()
                    }
                    cell.delgate = self
                    cell.productParamDel = self
                    
                    return cell
                 
                case 7:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddMoreAttributeCell") as? AddMoreAttributeCell  else {
                        return UITableViewCell()
                    }
                    
                    return cell
                
                default:
                    break
                    
                    
                }
                
                
            }
        }else{
            
            if imageAndVideoArray.count>0 {
                
                    switch  indexPath.row {
                    
                    case 0:
                        
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kAddProductTableViewCell , for: indexPath) as? AddProductTableViewCell else{
                            return UITableViewCell()
                        }
                       cell.wrapData(cell: cell, ImageData: self.imageAndVideoArray)
                        cell.delegateX = self
                        cell.selectionStyle = .none
                        return cell
                    
                    case 1:
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kScanBarcodeTableCell) as? ScanBarcodeTableCell  else {
                            return UITableViewCell()
                        }
                        return cell
                        
                    case 2,3:
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kAddProductCell) as? AddProductCell  else {
                            return UITableViewCell()
                        }
                        if indexPath.row == 2{
                            cell.addProductTextView.tag = 1
                            cell.titleLabel.tag = 1
                            
                            let value =  addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductNameEnglish.rawValue) as? String ?? ""
                            cell.setDataOnCell(tag: 1,titleValue: value)
                        }else{
                            cell.addProductTextView.tag = 2
                            cell.titleLabel.tag = 2
                            let value =  addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductNameUnicode.rawValue) as? String ?? ""
                            cell.setDataOnCell(tag: 2,titleValue: value)
                        }
                        cell.productParamDel = self
                        cell.titleDelegate = self
                        cell.cameraDelegate = self
                        cell.selectionStyle = .none
                        return cell
                        
                    case 4:
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kPriceUMOTableViewCell) as? PriceUMOTableViewCell  else {
                            return UITableViewCell()
                        }
                        
                        let valuePrice =  addProductParam.value(forKey: CategoryConstant.AddProductKey.ProducPrice.rawValue) as? String ?? ""
                        cell.setPrice(price: valuePrice)
                        
                        cell.productParamDel = self
                        
                        return cell
                    case 5:
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kAddUOMCell) as? UOMCell  else {
                            return UITableViewCell()
                        }
                        let valueUOM = addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductUom.rawValue) as? String ?? ""
                        cell.setUOM(UOM: valueUOM)
                        cell.productParamDel = self
                        
                        return cell
                        
                        
                    case 6:
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kProductDescriptionCell) as? ProductDescriptionCell  else {
                            return UITableViewCell()
                        }
                        cell.delgate = self
                        cell.productParamDel = self
                        let value = addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductDescription.rawValue) as? String ?? ""
                        
                        cell.setValue(value: value)
                        
                        return cell
                   
                    case 7:
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kProductDescriptionUnicodeCell) as? ProductDescriptionUnicodeCell  else {
                            return UITableViewCell()
                        }
                        cell.delgate = self
                        cell.productParamDel = self
                        
                        return cell
                     
                    case 8:
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddMoreAttributeCell") as? AddMoreAttributeCell  else {
                            return UITableViewCell()
                        }
                       
                        
                        return cell

                        
                        
                    default:
                        break
                }
                    
                
            }else{
                
                switch  indexPath.row {
                
                case 0:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kScanBarcodeTableCell) as? ScanBarcodeTableCell  else {
                        return UITableViewCell()
                    }
                    cell.scanAlertDelegate = self
                    cell.scanBarcodeButton.addTarget(self, action: #selector(barcodeButtonACTION(_:)), for: .touchUpInside)
                    return cell
                    
                case 1,2:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kAddProductCell) as? AddProductCell  else {
                        return UITableViewCell()
                    }
                    
                    if indexPath.row == 1{
                        cell.addProductTextView.tag = 1
                        cell.titleLabel.tag = 1
                        
                        let value =  addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductNameEnglish.rawValue) as? String ?? ""
                        
                        cell.setDataOnCell(tag: 1, titleValue: value)
                    }else{
                        cell.addProductTextView.tag = 2
                        cell.titleLabel.tag = 2
                        
                        
                        let value =  addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductNameUnicode.rawValue) as? String ?? ""
                        
                        cell.setDataOnCell(tag: 2, titleValue: value)
                    }
                    cell.productParamDel = self
                    cell.titleDelegate = self
                    cell.cameraDelegate = self
                    cell.selectionStyle = .none
                    return cell
                    
                
                case 3:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kPriceUMOTableViewCell) as? PriceUMOTableViewCell  else {
                        return UITableViewCell()
                    }
                   
                    let valuePrice =  addProductParam.value(forKey: CategoryConstant.AddProductKey.ProducPrice.rawValue) as? String ?? ""
                    
                    cell.setPrice(price: valuePrice)
                    cell.productParamDel = self
                    
                    return cell
                case 4:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kAddUOMCell) as? UOMCell  else {
                        return UITableViewCell()
                    }
                    let valueUOM = addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductUom.rawValue) as? String ?? ""
                    cell.setUOM(UOM: valueUOM)
                    cell.productParamDel = self
                    
                    return cell
                    
                case 5:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kProductDescriptionCell) as? ProductDescriptionCell  else {
                        return UITableViewCell()
                    }
                    
                    let value = addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductDescription.rawValue) as? String ?? ""
                    
                    cell.setValue(value: value)
                    cell.delgate = self
                    cell.productParamDel = self
                    
                    
                    return cell
               
                case 6:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryConstant.kProductDescriptionUnicodeCell) as? ProductDescriptionUnicodeCell  else {
                        return UITableViewCell()
                    }
                    
                    
                    cell.delgate = self
                    cell.productParamDel = self
                    
                    return cell
              
                case 7:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddMoreAttributeCell") as? AddMoreAttributeCell  else {
                        return UITableViewCell()
                    }
                   
                    
                    return cell
                    
                default:
                    break
                }
                
            }
            
        }
          
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if isComingFromBarCode{
            
            if modelObj?.data?.pictureModels?.count ?? 0 > 0 || ((modelObj?.data?.videoModels?.count ?? 0) != 0) || imageAndVideoArray.count>0 {
                switch  indexPath.row {
                case 0:
                    return 220
                case 1:
                    return 158
                case 2,3,4,5,6:
                    return 80
                case 7 :
                    if categoryTextFieldHeight == 0.0 || categoryTextFieldHeight == 33.0{
                        return 80.0
                    }else{
                        return 80.0 + categoryTextFieldHeight
                    }
                case 8 :
                    if descriptionTextFieldHeight == 0.0 || descriptionTextFieldHeight == 33.0{
                        return 80.0
                    }else{
                        return 80.0 + descriptionTextFieldHeight
                    }
                case 9 :
                    if descriptionUnicodeTextFieldHeight == 0.0 || descriptionUnicodeTextFieldHeight == 33.0{
                        return 80.0
                    }else{
                        return 80.0 + descriptionUnicodeTextFieldHeight
                    }
                    
                    
                default:
                    break
                }
            }else{
                switch  indexPath.row {
                case 0:
                    return 158
                case 1,2,3,4,5:
                    return 80
                case 6 :
                    if categoryTextFieldHeight == 0.0 || categoryTextFieldHeight == 33.0{
                        return 80.0
                    }else{
                        return 80.0 + categoryTextFieldHeight
                    }
                case 7 :
                    if descriptionTextFieldHeight == 0.0 || descriptionTextFieldHeight == 33.0{
                        return 80.0
                    }else{
                        return 80.0 + descriptionTextFieldHeight
                    }
                case 8 :
                    if descriptionUnicodeTextFieldHeight == 0.0 || descriptionUnicodeTextFieldHeight == 33.0{
                        return 80.0
                    }else{
                        return 80.0 + descriptionUnicodeTextFieldHeight
                    }
                    
                    
                default:
                    break
                }
            }
            
            
        }else{
            
            if imageAndVideoArray.count>0 {
                switch  indexPath.row {
                case 0:
                    return 220
                case 1:
                    return 158
                case 2,3,4,5,6,9:
                    
                    return 80
                case 7 :
                    if descriptionTextFieldHeight == 0.0 || descriptionTextFieldHeight == 33.0{
                        return 80.0
                    }else{
                        return 80.0 + descriptionTextFieldHeight
                    }
                case 8 :
                    if descriptionUnicodeTextFieldHeight == 0.0 || descriptionUnicodeTextFieldHeight == 33.0{
                        return 80.0
                    }else{
                        return 80.0 + descriptionUnicodeTextFieldHeight
                    }
                    
                    
                default:
                    break
                }
            }else{
                switch  indexPath.row {
                case 0:
                    return 158
                case 1,2,3,4,7:
                    return 80
                    
                case 5 :
                    if descriptionTextFieldHeight == 0.0 || descriptionTextFieldHeight == 33.0{
                        return 80.0
                    }else{
                        return 80.0 + descriptionTextFieldHeight
                    }
                case 6 :
                    if descriptionUnicodeTextFieldHeight == 0.0 || descriptionUnicodeTextFieldHeight == 33.0{
                        return 80.0
                    }else{
                        return 80.0 + descriptionUnicodeTextFieldHeight
                    }
                
                default:
                    break
                }
            }
        }
        
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isComingFromBarCode{
            if isHavingAttributes{
                if modelObj?.data?.pictureModels?.count ?? 0 > 0 || ((modelObj?.data?.videoModels?.count ?? 0) != 0) || imageAndVideoArray.count>0{
                    if indexPath.row == 8{
                        DispatchQueue.main.async {
                            var vcObj: AttributeViewController?
                            vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.kAttributeViewController) as? AttributeViewController
                            if let validObj = vcObj {
                                validObj.saveDelegate = self
                                validObj.productDetailsModel = self.modelObj
                                self.navigationController?.pushViewController(validObj, animated: true)
                            }
                        }
                    }
                }else{
                    if indexPath.row == 7{
                        DispatchQueue.main.async {
                            var vcObj: AttributeViewController?
                            vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.kAttributeViewController) as? AttributeViewController
                            if let validObj = vcObj {
                                validObj.saveDelegate = self
                                validObj.productDetailsModel = self.modelObj
                                self.navigationController?.pushViewController(validObj, animated: true)
                            }
                        }
                    }
                }
            }
        }else {
            if isHavingAttributes{
                if modelObj?.data?.pictureModels?.count ?? 0 > 0 || ((modelObj?.data?.videoModels?.count ?? 0) != 0) || imageAndVideoArray.count>0{
                    if indexPath.row == 8{
                        DispatchQueue.main.async {
                            var vcObj: AttributeViewController?
                            vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.kAttributeViewController) as? AttributeViewController
                            if let validObj = vcObj {
                                validObj.saveDelegate = self
                                validObj.productDetailsModel = self.modelObj
                                self.navigationController?.pushViewController(validObj, animated: true)
                            }
                        }
                    }
                }else{
                    if indexPath.row == 7{
                        DispatchQueue.main.async {
                            var vcObj: AttributeViewController?
                            vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.kAttributeViewController) as? AttributeViewController
                            if let validObj = vcObj {
                                validObj.saveDelegate = self
                                validObj.productDetailsModel = self.modelObj
                                self.navigationController?.pushViewController(validObj, animated: true)
                            }
                        }
                    }
                }
            }
            
        }
        
        
//        if indexPath.row == 7 || indexPath.row == 8{
//            DispatchQueue.main.async {
//                var vcObj: AttributeViewController?
//                vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.kAttributeViewController) as? AttributeViewController
//                if let validObj = vcObj {
//                    validObj.saveDelegate = self
//                    validObj.productDetailsModel = modelObj
//                    self.navigationController?.pushViewController(validObj, animated: true)
//                }
//            }
//        }
    }
    
    func showScanAlert() {
        let alertVC1 = SAlertController()
        alertVC1.ShowSAlert(title: "OSK Vendor".localized, withDescription: "If your product available in server , details will auto fill up .".localized, onController: self)
         let YesAction = SAlertAction()
         YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
         })
        alertVC1.addAction(action: [YesAction])
    }

    @objc func barcodeButtonACTION (_ sender: UIButton) {

        DispatchQueue.main.async {
                   if #available(iOS 13.0, *) {
                       let storyBoard = UIStoryboard(name: "EditProduct", bundle: nil)
                       let vc = storyBoard.instantiateViewController(identifier: "ProductBarScanner") as! ProductBarScanner
                       vc.delegate = self
                       self.navigationController?.pushViewController(vc, animated: true)
                   } else {
                       // Fallback on earlier versions
                   }
               }
    }
    
}


extension AddProductNewViewController: AddProductTitleLabelDel {
   
    func manageHeight(index: Int,hasText: Bool){
        titleLableHeightDic.setValue(hasText, forKey: String(index))
        UIView.performWithoutAnimation {
            self.productTableView.reloadRows(at: [IndexPath(item: index, section: 0)], with: .none)
        }
    }
}


extension AddProductNewViewController: CallCamera {
    
    func callCamera(){
        if let presentedViewController = self.storyboard?.instantiateViewController(withIdentifier: CategoryConstant.kCameraVideoBarcodeVC) as? CameraVideoBarcodeVC {
            presentedViewController.providesPresentationContextTransitionStyle = true
            presentedViewController.definesPresentationContext = true
            presentedViewController.delegate = self
            presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
            presentedViewController.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.7)
            self.present(presentedViewController, animated: true, completion: nil)
        }
    }
}

extension AddProductNewViewController: CameraVideoBarcodeDelegate {
    //0 for camera
    //1 for video
    //2 for barcode
    func cameraVideoBarcodeSelected(index: Int){
        if index == 0{
            let pickerController = DKImagePickerController()
            pickerController.allowMultipleTypes = true
            //let allCount = self.modelObj?.data?.pictureModels?.count ?? 0 + self.modelObj?.data?.videoModels?.count ?? 0+self.imageAndVideoArray.count ?? 0
            self.imageAndVideoArray.removeAll()
            
            let imageCount = 5 - imageDataFromServer.count

            //let imageCount = 5-allCount
            pickerController.maxSelectableCount = imageCount
            pickerController.assetType = .allPhotos
            DKImageExtensionController.registerExtension(extensionClass: CustomCameraExtensionA.self, for: .camera)
            pickerController.didSelectAssets = { (assets: [DKAsset]) in
                print("didSelectAssetsPhotos")
                print(assets)
                self.imageAndVideoArray.append(contentsOf: assets)
                self.loadImageAndVideoData()
              //  DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                   // self.productTableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                    self.productTableView.reloadData()
               // })
            }
            self.present(pickerController, animated: true) {}
        }else if index == 1{
            if self.isVideoIncluded {
                self.alertVC = SAlertController()
                self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErrorUploading.videoLimitReachError, onController: self)
                let YesAction = SAlertAction()
                YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                })
                self.alertVC?.addAction(action: [YesAction])
            }
            else {
                let pickerController = DKImagePickerController()
                pickerController.allowMultipleTypes = true
                pickerController.maxSelectableCount = 1
                pickerController.assetType = .allVideos
                DKImageExtensionController.registerExtension(extensionClass: CustomCameraExtensionB.self, for: .camera)
                pickerController.didSelectAssets = { (assets: [DKAsset]) in
                    print("didSelectAssetsVideos")
                    print(assets)
                    DispatchQueue.main.async {
                        if assets.count != 0 {
                            self.imageAndVideoArray.append(contentsOf: assets)
                            if let assetVideo = assets.last {
                                if assetVideo.type == .video {
                                    self.isVideoIncluded = true
                                }
                            }
                        }
                        self.loadImageAndVideoData()
                        self.productTableView.reloadData()
                    }
                }
                self.present(pickerController, animated: true) {}
              }
        }else{
            DispatchQueue.main.async {
                 if #available(iOS 13.0, *) {
                     let storyBoard = UIStoryboard(name: "EditProduct", bundle: nil)
                    if let validObj = storyBoard.instantiateViewController(identifier: "ProductBarScanner") as? ProductBarScanner{
                        validObj.delegate = self
                        self.navigationController?.pushViewController(validObj, animated: true)
                    }
                 } else {
                    let storyBoard = UIStoryboard(name: "EditProduct", bundle: nil)
                    if let validObj = storyBoard.instantiateViewController(withIdentifier: "ProductBarScanner") as? ProductBarScanner{
                        validObj.delegate = self
                        self.navigationController?.pushViewController(validObj, animated: true)
                    }
                    
                 }
             }
        }
    }
}



extension AddProductNewViewController: ProductBarScannerDelegate{
    
    func returnBarCode(barCade: String) {
        println_debug(barCade)
        barCodeNumber = barCade
        isComingFromBarCode = true
        self.getProductDetialsByBarCode(barCode: barCade , reloadFor: "")

        //self.getAllCategories()
    }
    
}


extension AddProductNewViewController: imageVideoDelete {
  
    func deleteImageContentAtNew(index: Int) {
        
        imageDataFromServer.remove(at: index)
        imageNameFromServer.remove(at: index)
        
        DispatchQueue.main.async {
          self.productTableView.reloadData()
        }
    }
    /*
    func deleteImageContentAtNew(index: Int) {
        
        let count = index
        
        
        if let imageUrlCount = modelObj?.data?.pictureModels{
            if imageUrlCount.count>0{
                if count<=imageUrlCount.count{
                    if imageUrlCount.indices.contains(count){
                        print("delete the image url here")
                        self.modelObj?.data?.pictureModels?.remove(at: index)
                        DispatchQueue.main.async {
                          self.productTableView.reloadData()
                        }
                    } else {
                        //if no url image
                        let index = count - (modelObj?.data?.videoModels?.count ?? 0 + imageUrlCount.count )
                        if let object = self.imageAndVideoArray[index] as? DKAsset {
                            if object.type == .video {
                                self.isVideoIncluded = false
                            }
                        }
                        self.imageAndVideoArray.remove(at: index)
                           DispatchQueue.main.async {
                             self.productTableView.reloadData()
                           }
                    }
                }else if count>=imageUrlCount.count{
                    if let videoUrlCount = modelObj?.data?.videoModels{
                        if videoUrlCount.count>0{
                            if imageUrlCount.count + videoUrlCount.count >= count{
                                let index = count - imageUrlCount.count
                                if videoUrlCount.indices.contains(index){
                                    print("delete video")
                                    self.modelObj?.data?.videoModels?.remove(at: index)
                                    DispatchQueue.main.async {
                                      self.productTableView.reloadData()
                                    }
                                    
                                }
                            }else if imageUrlCount.count + videoUrlCount.count + imageAndVideoArray.count >= count{
                                let index = count - (imageUrlCount.count + videoUrlCount.count)
                                if let object = self.imageAndVideoArray[index] as? DKAsset {
                                    if object.type == .video {
                                        self.isVideoIncluded = false
                                    }
                                }
                                self.imageAndVideoArray.remove(at: index)
                                   DispatchQueue.main.async {
                                     self.productTableView.reloadData()
                                   }
                            }
                        }else{
                           print("if no video")
                            let index = count - (imageUrlCount.count + videoUrlCount.count)
                            if let object = self.imageAndVideoArray[index] as? DKAsset {
                                if object.type == .video {
                                    self.isVideoIncluded = false
                                }
                            }
                            self.imageAndVideoArray.remove(at: index)
                               DispatchQueue.main.async {
                                 self.productTableView.reloadData()
                               }
                        }
                        
                    }
                    else {
                        let index = count - (modelObj?.data?.videoModels?.count ?? 0 + imageUrlCount.count )
                        if let object = self.imageAndVideoArray[index] as? DKAsset {
                            if object.type == .video {
                                self.isVideoIncluded = false
                            }
                        }
                        self.imageAndVideoArray.remove(at: index)
                           DispatchQueue.main.async {
                             self.productTableView.reloadData()
                           }
                    }
                } else {
                    let index = count - (modelObj?.data?.videoModels?.count ?? 0 + imageUrlCount.count )
                    if let object = self.imageAndVideoArray[index] as? DKAsset {
                        if object.type == .video {
                            self.isVideoIncluded = false
                        }
                    }
                    self.imageAndVideoArray.remove(at: index)
                       DispatchQueue.main.async {
                         self.productTableView.reloadData()
                       }
                }
            }
        }else{
            
            if let object = self.imageAndVideoArray[index] as? DKAsset {
                if object.type == .video {
                    self.isVideoIncluded = false
                }
            }
            self.imageAndVideoArray.remove(at: index)
               DispatchQueue.main.async {
                 self.productTableView.reloadData()
               }
            
        }
        
       
        
        
      
      }
  */
    
  func palyVideoNew(index : Int) {
      let asset = self.imageAndVideoArray[index]
             asset.fetchAVAsset { (avAsset, info) in
                 DispatchQueue.main.async(execute: { () in
                     self.playVideo(avAsset!)
            })
       }
  }
}


extension AddProductNewViewController: imageDelete {
    func deleteImageContentAt(index: Int) {
      if let object = self.imageAndVideoArray[index] as? DKAsset {
          if object.type == .video {
              self.isVideoIncluded = false
          }
      }
      self.imageAndVideoArray.remove(at: index)
         DispatchQueue.main.async {
           self.productTableView.reloadData()
         }
      }
  
  func palyVideo(index : Int) {
      let asset = self.imageAndVideoArray[index]
             asset.fetchAVAsset { (avAsset, info) in
                 DispatchQueue.main.async(execute: { () in
                     self.playVideo(avAsset!)
            })
       }
  }
}


extension AddProductNewViewController: AddProductParamProtocol {
   
    func param(name: String,value: String){
        addProductParam.setValue(value, forKey: name)
    }
}

extension AddProductNewViewController: HeightForProductTextView {
    func heightOfTextView(height: CGFloat,textfieldName: String){
       //this means to increase the unicode textfield height
        if textfieldName == ""{
            descriptionUnicodeTextFieldHeight = height
        }else if textfieldName == "productDescription"{
            descriptionTextFieldHeight = height
        }else{
            descriptionUnicodeTextFieldHeight = height
        }
        self.productTableView.beginUpdates()
        self.productTableView.endUpdates()

    }

}

extension AddProductNewViewController: EditProductImagesNewCellDelegate{
    
    func reloadProductDetailPage(id: Int){
        
        if id < 100 {
            var imageArray = self.productDetailsModel?.data?.pictureModels
            self.deleteCategoryIamge(pictureID: String(imageArray![id].id ?? 0), handler: { isSuccess in
                if isSuccess {
                    imageArray?.remove(at: id)
                    self.productDetailsModel?.data?.pictureModels = imageArray!
                    self.productTableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                    }else {
                }
            })
        }else {
            var imageArray = self.productDetailsModel?.data?.videoModels
            self.deleteCategoryVideo(pictureID: String(imageArray![id - 100].id ?? 0), handler: { isSuccess in
                if isSuccess {
                    imageArray?.remove(at: id - 100)
                    self.productDetailsModel?.data?.videoModels = imageArray!
                    self.productTableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                    }else {
                }
            })
        }
  }
    
    func palyVideo(url : URL){
            let player = AVPlayer(url: url)
            let vc = AVPlayerViewController()
            vc.player = player
            self.present(vc, animated: true) { vc.player?.play() }
    }
    
}



extension AddProductNewViewController{
    func deleteCategoryIamge(pictureID: String, handler:@escaping (_ succes: Bool) -> Void) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/deleteproductpicture/%@", APIManagerClient.sharedInstance.base_url,pictureID) as String
            print("Get allcategories :\(urlStr) ")
            self.aPIManager.postGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        return
                    }
                    print(jsonData)
                    handler(true)
                }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    handler(false)
            })
        }
    }
    
    
    func deleteCategoryVideo(pictureID: String, handler:@escaping (_ succes: Bool) -> Void) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/deleteproductvideo/%@", APIManagerClient.sharedInstance.base_url,pictureID) as String
            print("Get allcategories :\(urlStr) ")
            self.aPIManager.postGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        return
                    }
                    print(jsonData)
                    handler(true)
                }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    handler(false)
            })
        }
    }
}
