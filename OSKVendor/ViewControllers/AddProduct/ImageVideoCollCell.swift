//
//  ImageVideoCollCell.swift
//  Covid Zay Vendor
//
//  Created by OK$ on 26/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class ImageVideoCollCell: UICollectionViewCell {
    
    @IBOutlet var imgViewPic: UIImageView!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var viewBG: UIView!

}

