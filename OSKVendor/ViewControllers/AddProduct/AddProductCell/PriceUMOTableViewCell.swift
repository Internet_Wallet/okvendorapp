//
//  PriceUMOTableViewCell.swift
//  OSKVendor
//
//  Created by Tushar Lama on 29/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class PriceUMOTableViewCell: UITableViewCell {

    @IBOutlet var priceLabelHeight: NSLayoutConstraint!
    @IBOutlet var priceValueTextField: UITextField!
    @IBOutlet var priceLabel: UILabel!
    
    var productParamDel: AddProductParamProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        priceValueTextField.delegate = self
        priceValueTextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        // Initialization code
        //priceValueTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setPrice(price: String){
        let valuePrice = price
        if valuePrice != ""{
            priceLabel.isHidden = false
            priceLabel.text = "Price (MMK)*".localized
            priceValueTextField.text = valuePrice
        }else{
            priceLabel.isHidden = true
            priceValueTextField.text = "Price (MMK)*".localized
        }
    }

}

extension PriceUMOTableViewCell: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == priceValueTextField{
            
            if textField.text == "Price (MMK)*".localized{
                textField.keyboardType = .numberPad
                textField.text = ""
                priceLabelHeight.constant = 18.0
                priceLabel.isHidden = false
                priceLabel.text = "Price (MMK)*".localized
            }
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textField == priceValueTextField{
            if textField.text == ""{
                priceLabelHeight.constant = 18.0
                priceLabel.isHidden = true
                textField.text = "Price (MMK)*".localized
            }else{
                productParamDel?.param(name: CategoryConstant.AddProductKey.ProducPrice.rawValue,value: textField.text ?? "")
            }
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == priceValueTextField{
            if let count  = (textField.text?.count ?? 0 + string.count) as? Int {
                if string == ""{
                    return true
                }else{
                    if count > 10{
                        return false
                    }else{
                        return true
                    }
                }
            }
        }
        return true
    }
    
   @objc func textFieldDidChange(textField: UITextField) {
            if textField == self.priceValueTextField {
                // Some locales use different punctuations.
                
                var textFormatted = textField.text?.replacingOccurrences(of: ",", with: "")
                textFormatted = textFormatted?.replacingOccurrences(of: ".", with: "")

                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                if let text = textFormatted, let textAsInt = Int(text) {
                    textField.text = numberFormatter.string(from: NSNumber(value: textAsInt))
                }
            }
        }
    
}
