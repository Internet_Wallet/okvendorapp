//
//  AddMoreAttributeCell.swift
//  Covid Zay Vendor
//
//  Created by Tushar Lama on 22/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class AddMoreAttributeCell: UITableViewCell {
    
    @IBOutlet var contentLabel: UILabel!{
        didSet{
            contentLabel.text = "Add More Details (optional)".localized
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
