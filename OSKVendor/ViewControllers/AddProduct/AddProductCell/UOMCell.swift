//
//  UOMCell.swift
//  Covid Zay Vendor
//
//  Created by Avaneesh Awasthi on 26/11/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class UOMCell: UITableViewCell {
    
    @IBOutlet var umoValueTextField: UITextField!
    @IBOutlet var umoTitleLabel: UILabel!
    
    var productParamDel: AddProductParamProtocol?

    override func awakeFromNib() {
        super.awakeFromNib()
        umoValueTextField.delegate = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUOM(UOM: String){
        let valueUOM = UOM
        umoTitleLabel.text = "Unit Of Measurement*".localized
        if valueUOM != ""{
            umoValueTextField.text  = valueUOM
        }else{
            umoValueTextField.text  = "Ex i.e 1-kg".localized
        }
    }

}

extension UOMCell: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == "Ex i.e 1-kg".localized{
            textField.keyboardType = .asciiCapable
            textField.text = ""
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
            if textField.text != ""{
                productParamDel?.param(name: CategoryConstant.AddProductKey.ProductUom.rawValue,value: textField.text ?? "")
            }else{
                textField.text = "Ex i.e 1-kg".localized
            }
      }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
}
