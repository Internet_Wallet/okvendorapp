//
//  ScanBarcodeTableCell.swift
//  OSKVendor
//
//  Created by Tushar Lama on 29/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

protocol ScanBarcodeDelegate {
    func showScanAlert()
}

class ScanBarcodeTableCell: UITableViewCell {
    
    var scanAlertDelegate:ScanBarcodeDelegate?

    @IBOutlet var scanBarcodeButton: UIButton!
    @IBOutlet var autoFillLabelHeight: NSLayoutConstraint!
    @IBOutlet var scanView: CardView!
    @IBOutlet var scanBarcodeInfoLabel: UILabel!
    {
        didSet {
            self.scanBarcodeInfoLabel.font = UIFont(name: appFont, size: 13.0)
            self.scanBarcodeInfoLabel.text = self.scanBarcodeInfoLabel.text?.localized
        }
    }
    @IBOutlet var scanBarcodeLabel: UILabel! {
        didSet {
            self.scanBarcodeLabel.font = UIFont(name: appFont, size: 18.0)
            self.scanBarcodeLabel.text = self.scanBarcodeLabel.text?.localized
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.scanBarcodeInfoLabel.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    @IBAction func onClickInfo(_ sender: Any) {
        self.scanAlertDelegate?.showScanAlert()
    }
}
