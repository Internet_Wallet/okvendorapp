//
//  ProductDescriptionCell.swift
//  Covid Zay Vendor
//
//  Created by Tushar Lama on 22/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class ProductDescriptionCell: UITableViewCell {

    @IBOutlet var labelHeight: NSLayoutConstraint!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var topTitleLabel: UILabel!
    var productParamDel: AddProductParamProtocol?
    var delgate:HeightForProductTextView?
    override func awakeFromNib() {
        super.awakeFromNib()
        descriptionTextView.delegate = self
        descriptionTextView.text = "Description".localized
        topTitleLabel.isHidden = true
        labelHeight.constant = 0.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValue(value: String){
        let valuePrice = value
        if valuePrice != ""{
            descriptionTextView.text = valuePrice
            labelHeight.constant = 18.0
            topTitleLabel.isHidden = false
            topTitleLabel.text = "Description".localized
        }else{
            descriptionTextView.text = "Description".localized
            labelHeight.constant = 10.0
            topTitleLabel.isHidden = true
            topTitleLabel.text = ""
        }
    }
    

}


extension ProductDescriptionCell: UITextViewDelegate{
        
    func textViewDidBeginEditing(_ textView: UITextView){
        if textView.text == "Description".localized{
            textView.text = ""
        }
        topTitleLabel.isHidden = false
        topTitleLabel.text = "Description".localized
        labelHeight.constant = 18.0
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            descriptionTextView.text = "Description".localized
           // topTitleLabel.isHidden = true
            labelHeight.constant = 10.0
            topTitleLabel.text = ""
        }else{
            productParamDel?.param(name: CategoryConstant.AddProductKey.ProductDescription.rawValue,value: descriptionTextView.text ?? "")
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

//        var CHARSET = ""
//       if appDel.currentLanguage == "en" {
//              CHARSET = NAME_CHAR_SET_En
//        }else {
//             CHARSET = NAME_CHAR_SET_Uni
//        }
        let updatedText = (textView.text! as NSString).replacingCharacters(in: range, with: text)
      //  if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: text).inverted).joined(separator: "")) { return false }
      
//        if updatedText == " " || updatedText == "  "{
//            return false
//        }
//        if (updatedText.contains("  ")){
//            return false
//        }
        if updatedText.count > 40 {
            return false
        }

//
//        if remarkTextView.text.count > 0 && remarkTextView.text != ""{
//
//        }
//        else{
////            if remarkTextView.text.count == 1 && remarkTextView.text != ""{
////
////                remarkCloseButton.isHidden = false
////            }else{
//                remarkCloseButton.isHidden = true
//        //    }
//        }
        return true
    }
    
    
    
//    func textViewDidChange(_ textView: UITextView) {
//        let fixedWidth: CGFloat = textView.frame.size.width
//        let newSize: CGSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
//           if let iuDelegate = self.delgate {
//            iuDelegate.heightOfTextView(height: newSize.height, textfieldName: "productDescription")
//
//           }
//       }
}
