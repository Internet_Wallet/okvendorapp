//
//  SelectProductCategoryCell.swift
//  Covid Zay Vendor
//
//  Created by Tushar Lama on 23/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class SelectProductCategoryCell: UITableViewCell {

    @IBOutlet var categoryTextView: UITextView!
    @IBOutlet var selectCategoryLabel: UILabel!
    var delgate:HeightForProductTextView?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setValue(value: String,textViewObj: UITextView){
        let valuePrice = value
        if valuePrice != ""{
            categoryTextView.text = valuePrice
        }else{
            
        }
        
        let fixedWidth: CGFloat = textViewObj.frame.size.width
        let newSize: CGSize = textViewObj.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
           if let iuDelegate = self.delgate {
            iuDelegate.heightOfTextView(height: newSize.height, textfieldName: "category")
           }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
