//
//  ProductDescriptionUnicodeCell.swift
//  Covid Zay Vendor
//
//  Created by Tushar Lama on 22/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import UIKit

protocol HeightForProductTextView {
    func heightOfTextView(height: CGFloat,textfieldName: String)

}

class ProductDescriptionUnicodeCell: UITableViewCell {
    @IBOutlet var topTitleLabel: UILabel!
    @IBOutlet var descriptionUnicodeTextView: UITextView!
    var delgate:HeightForProductTextView?
    var productParamDel: AddProductParamProtocol?
    
    @IBOutlet var titleHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        descriptionUnicodeTextView.text = "Description in Unicode".localized
        // Initialization code
        descriptionUnicodeTextView.delegate = self
        topTitleLabel.isHidden = true
        titleHeight.constant = 0.0
    }
    
    
    func setValue(value: String){
        let valuePrice = value
        if valuePrice != ""{
            
            descriptionUnicodeTextView.text = valuePrice
            
            titleHeight.constant = 18.0
            //  topTitleLabel.isHidden = false
            topTitleLabel.text = "Description in Unicode".localized
            
        }else{
            descriptionUnicodeTextView.text = "Description in Unicode".localized
            //  topTitleLabel.isHidden = true
            
            titleHeight.constant = 10.0
            // descriptionUnicodeTextView.text = valuePrice
            topTitleLabel.text = ""
            
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


extension ProductDescriptionUnicodeCell: UITextViewDelegate{
  
  
    func textViewDidBeginEditing(_ textView: UITextView){
        if textView.text == "Description in Unicode".localized{
            textView.text = ""
        }
            topTitleLabel.text = "Description in Unicode".localized
            topTitleLabel.isHidden = false
            titleHeight.constant = 18.0
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            descriptionUnicodeTextView.text = "Description in Unicode".localized
           // topTitleLabel.isHidden = true
            titleHeight.constant = 10.0
            topTitleLabel.text = ""
        }else{
            productParamDel?.param(name: CategoryConstant.AddProductKey.ProductDescriptionInUnicode.rawValue,value: descriptionUnicodeTextView.text ?? "")
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

//        var CHARSET = ""
//       if appDel.currentLanguage == "en" {
//              CHARSET = NAME_CHAR_SET_En
//        }else {
//             CHARSET = NAME_CHAR_SET_Uni
//        }
        let updatedText = (textView.text! as NSString).replacingCharacters(in: range, with: text)
      //  if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: text).inverted).joined(separator: "")) { return false }
      
//        if updatedText == " " || updatedText == "  "{
//            return false
//        }
//        if (updatedText.contains("  ")){
//            return false
//        }
        if updatedText.count > 40 {
            return false
        }

//
//        if remarkTextView.text.count > 0 && remarkTextView.text != ""{
//
//        }
//        else{
////            if remarkTextView.text.count == 1 && remarkTextView.text != ""{
////
////                remarkCloseButton.isHidden = false
////            }else{
//                remarkCloseButton.isHidden = true
//        //    }
//        }
        return true
    }

        
//    func textViewDidChange(_ textView: UITextView) {
//        let fixedWidth: CGFloat = textView.frame.size.width
//        let newSize: CGSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
//           if let iuDelegate = self.delgate {
//            iuDelegate.heightOfTextView(height: newSize.height, textfieldName: "")
//
//           }
//       }
}
