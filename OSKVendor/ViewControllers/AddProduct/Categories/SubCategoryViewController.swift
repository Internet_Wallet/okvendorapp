//
//  SubCategoryViewController.swift
//  OSKVendor
//
//  Created by Tushar Lama on 28/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class SubCategoryViewController: OSKBaseViewController {
    
    @IBOutlet var subCatCollectionView: UICollectionView!
    var subCategoryObj : [Category]? = []
    var isSubcategorySelected = false
    @IBOutlet var CategoryView : UIView!
    @IBOutlet var categoryTableView: UITableView!
    @IBOutlet var selectSubCaltegoryLabel: UILabel!
    @IBOutlet var categoryTableHeight: NSLayoutConstraint!
    
    @IBOutlet var nxtButtonFrame: NSLayoutConstraint!
    var tableViewHeight: CGFloat = 0
    var childSubCategoryObj : [Category]? = []
    var categoryId: Int?
    var selectedIndexPath: IndexPath?
    var hideShowCategoryView = false
    var categoryname = ""
    var subcategoryname = ""

    
    @IBOutlet var nextButton: UIButton!{
        didSet{
               nextButton.setTitle("Next".localized, for: .normal)
          }
      }
    
    @IBOutlet var selectSubCatView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = categoryname//"Category".localized
        //        UIBarButtonItem.appearance().setTitleTextAttributes([.foregroundColor: UIColor.clear], for: .normal)
        //        self.title = "Category"
        self.navigationController?.navigationBar.tintColor = UIColor.white
        //        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        //  self.navigationItem.backBarButtonItem?.title = ""
        self.selectSubCaltegoryLabel.text = "Select Sub Category".localized
        self.subCatCollectionView.register(UINib(nibName: "CategoriesCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SubCategoryCell")
        subCatCollectionView.delegate = self
        subCatCollectionView.dataSource = self
        subCatCollectionView.reloadData()
        categoryTableView.delegate = self
        categoryTableView.dataSource = self
        categoryTableView.reloadData()
        nextButton.isHidden = true
        nxtButtonFrame.constant = 0
    }
    
    
    
    @IBAction func onClickSelectCategory(_ sender: Any) {
        
        if !hideShowCategoryView{
            hideShowCategoryView = true
            categoryTableView.reloadData()
            self.view.addSubview(self.CategoryView)
           // self.view.bringSubviewToFront(self.CategoryView)
            UIView.animate(withDuration: 0.2) {
                self.CategoryView.frame = CGRect(x: 20, y: Int(self.selectSubCatView.frame.size.height + self.selectSubCatView.frame.origin.y), width: Int(self.view.frame.size.width) - 40, height: Int(self.categoryTableView.contentSize.height) + 80)
            }
        }else{
            hideShowCategoryView = false
            self.CategoryView.removeFromSuperview()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("**********************")
        if self.selectSubCaltegoryLabel.text == "Select Sub Category".localized {
            self.nextButton.isHidden = true
            self.nxtButtonFrame.constant = 0
        }
        else {
            self.nextButton.isHidden = false
            self.nxtButtonFrame.constant = 75
        }
    }
    
    @IBAction func onClickNectButton(_ sender: Any) {
        
        navigate(index: nextButton.tag)
    }
    
    func navigate(index: Int){
        if let hasValue = childSubCategoryObj{
            
            if let isValidIndex = selectedIndexPath{
                guard let cell = self.subCatCollectionView.cellForItem(at: isValidIndex) as? CategoriesCollectionCell else{
                    return
                }
                cell.tickImageView.image = nil
            }
            
            if hasValue.count>0{
                if #available(iOS 13.0, *) {
                    let obj = self.storyboard?.instantiateViewController(identifier: CategoryConstant.kChildCategoryViewController) as? ChildCategoryViewController
                    if let isValid = obj{
                        isValid.childSubCategoryObj =  childSubCategoryObj
                        isValid.subcatgory = subcategoryname
                        self.navigationController?.pushViewController(isValid, animated: true)
                    }
                } else {
                    // Fallback on earlier versions
                    if let obj = self.storyboard?.instantiateViewController(withIdentifier: CategoryConstant.kChildCategoryViewController) as? ChildCategoryViewController{
                        obj.childSubCategoryObj =  childSubCategoryObj
                        obj.subcatgory = subcategoryname
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }
            }else{
                //move to add product screen
                print("11231323213")
                
                if #available(iOS 13.0, *) {
                    let obj = self.storyboard?.instantiateViewController(identifier: CategoryConstant.kAddProductNewViewController) as? AddProductNewViewController
                    if let isValid = obj{
                        if let id = subCategoryObj?[index] {
                            isValid.selectedCategory = id.Id
                            isValid.isHavingAttributes = id.haveInfoAttribute ?? false
                            isValid.categoryName = id.name ?? ""
                        }
                        self.navigationController?.pushViewController(isValid, animated: true)
                    }
                } else {
                    // Fallback on earlier versions
                    if let obj = self.storyboard?.instantiateViewController(withIdentifier: CategoryConstant.kAddProductNewViewController) as? AddProductNewViewController{
                        if let id = subCategoryObj?[index] {
                            obj.selectedCategory = id.Id
                            obj.isHavingAttributes = id.haveInfoAttribute ?? false
                            obj.categoryName = id.name ?? ""
                        }
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }
                
            }
        }else{
            
            
        }
        
    }
    
    
    //this function will store selected sub category when we select from drop down or collection view
    func storeSubCategory(index: Int){
        
        if let SUbCat = self.subCategoryObj?[index] {
            if let _ = childSubCategoryObj{
                childSubCategoryObj = nil
                childSubCategoryObj = SUbCat.Children
            }else{
                childSubCategoryObj = SUbCat.Children
            }
        }
    }
}

extension SubCategoryViewController : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout , UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subCategoryObj?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = subCatCollectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath) as! CategoriesCollectionCell
        if let currentObject = subCategoryObj?[indexPath.row] {
            cell.updateCellWithData(CategoryData: currentObject, index: indexPath.row)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        storeSubCategory(index: indexPath.row)
        
        guard let cell = self.subCatCollectionView.cellForItem(at: indexPath) as? CategoriesCollectionCell else{
            return
        }
        selectedIndexPath = indexPath
        let currentObject = subCategoryObj?[indexPath.row]
        subcategoryname = currentObject?.name ?? "Category"
        
        cell.tickImageView.image = UIImage(named: "tick_green")
        AppUtility.showLoading(self.view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            AppUtility.hideLoading(self.view)
            self.navigate(index: indexPath.row)
        })
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemWidth = HomeConstant.getItemWidth(boundWidth: collectionView.bounds.size.width + 5)
        return CGSize(width: itemWidth, height: itemWidth + 20)
        
    }
    
}



extension SubCategoryViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCategoryObj?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryListCell", for: indexPath) as! CategoryListCell
        if let catData = self.subCategoryObj?[indexPath.row] {
            cell.lblTitle.text = catData.name ?? ""
        }
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    //    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return categoryTableView.estimatedRowHeight
    //    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.tableViewHeight += cell.frame.size.height
        categoryTableHeight.constant = self.tableViewHeight
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        storeSubCategory(index: indexPath.row)
        let catData = self.subCategoryObj?[indexPath.row]
        subcategoryname = catData?.name ?? "Category"

        //  if !isSubcategorySelected{
        nextButton.isHidden = false
        nxtButtonFrame.constant = 75
        nextButton.tag = indexPath.row
        selectedIndexPath = indexPath
        if let catData = self.subCategoryObj?[indexPath.row] {
            selectSubCaltegoryLabel.text = catData.name ?? ""
        }
        //  }else{
        //     isSubcategorySelected = false
        // }
        CategoryView.removeFromSuperview()
    }
    
}
