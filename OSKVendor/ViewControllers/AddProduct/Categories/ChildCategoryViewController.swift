//
//  ChildCategoryViewController.swift
//  OSKVendor
//
//  Created by Tushar Lama on 28/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class ChildCategoryViewController: OSKBaseViewController {
    
    @IBOutlet var subCatCollectionView: UICollectionView!
    var childSubCategoryObj : [Category]? = []
    var isSubcategorySelected = false
    @IBOutlet var CategoryView : UIView!
    @IBOutlet var categoryTableView: UITableView!
    @IBOutlet var selectSubCaltegoryLabel: UILabel!
    @IBOutlet var categoryTableHeight: NSLayoutConstraint!
    var categoryId: Int?
    var tableViewHeight: CGFloat = 0
    var selectedIndexPath: IndexPath?
    var hideShowTable = false
    var subcatgory = ""
    
    @IBOutlet var nextButton: UIButton!{
        didSet{
            nextButton.setTitle("Next".localized, for: .normal)
            
        }
        
    }
    @IBOutlet var selectSubCatView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = subcatgory //"Category".localized
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.subCatCollectionView.register(UINib(nibName: "CategoriesCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SubCategoryCell")
        subCatCollectionView.delegate = self
        subCatCollectionView.dataSource = self
        subCatCollectionView.reloadData()
        categoryTableView.delegate = self
        categoryTableView.dataSource = self
        categoryTableView.reloadData()
        nextButton.isHidden = true
        if let catData = self.childSubCategoryObj?[0] {
            selectSubCaltegoryLabel.text = catData.name
        }
        
    }
    
    @IBAction func onClickSelectCategory(_ sender: Any) {
        
        if !hideShowTable{
            hideShowTable = true
            categoryTableView.reloadData()
            self.view.addSubview(self.CategoryView)
            
            UIView.animate(withDuration: 0.2) {
                self.CategoryView.frame = CGRect(x: 20, y: Int(self.selectSubCatView.frame.size.height + self.selectSubCatView.frame.origin.y), width: Int(self.view.frame.size.width) - 40, height: Int(self.categoryTableView.contentSize.height) + 80)
            }
        }else{
            hideShowTable = false
            self.CategoryView.removeFromSuperview()
        }
        

    }
    
    @IBAction func onClickNectButton(_ sender: Any) {
        
        navigate(index: nextButton.tag)
    }
    
    
    func removeTickMark(){
        if let isValidIndex = selectedIndexPath{
            guard let cell = self.subCatCollectionView.cellForItem(at: isValidIndex) as? CategoriesCollectionCell else{
                return
            }
            cell.tickImageView.image = nil
        }
    }
    
    fileprivate func navigate(index: Int){
        
        if #available(iOS 13.0, *) {
            let obj = self.storyboard?.instantiateViewController(identifier: CategoryConstant.kAddProductNewViewController) as? AddProductNewViewController
            if let isValid = obj{
                if let isValidValue = childSubCategoryObj{
                    if isValidValue.indices.contains(index){
                        if let id = childSubCategoryObj?[index] {
                            isValid.selectedCategory = id.Id
                            isValid.isHavingAttributes = id.haveInfoAttribute ?? false
                            isValid.categoryName = id.name ?? ""
                        }
                        self.removeTickMark()
                        self.navigationController?.pushViewController(isValid, animated: true)
                    }
                }
            }
        } else {
            // Fallback on earlier versions
            if let obj = self.storyboard?.instantiateViewController(withIdentifier: CategoryConstant.kAddProductNewViewController) as? AddProductNewViewController{
                
                if let isValidValue = childSubCategoryObj{
                    if isValidValue.indices.contains(index){
                        if let id = childSubCategoryObj?[index] {
                            obj.selectedCategory = id.Id
                            obj.isHavingAttributes = id.haveInfoAttribute ?? false
                            obj.categoryName = id.name ?? ""
                        }
                        self.removeTickMark()
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }
            }
        }
    }
    
    
    
}


extension ChildCategoryViewController : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout , UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return childSubCategoryObj?.count ?? 0
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           let cell = subCatCollectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath) as! CategoriesCollectionCell
           if let currentObject = childSubCategoryObj?[indexPath.row] {
              cell.updateCellWithData(CategoryData: currentObject, index: indexPath.row)
           }
           return cell
       }
       
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let cell = self.subCatCollectionView.cellForItem(at: indexPath) as? CategoriesCollectionCell else{
            return
        }
        
        selectedIndexPath = indexPath
        
        cell.tickImageView.image = UIImage(named: "tick_green")
        AppUtility.showLoading(self.view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            AppUtility.hideLoading(self.view)
            self.navigate(index: indexPath.row)
        })
        
       }
    
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = HomeConstant.getItemWidth(boundWidth: collectionView.bounds.size.width + 5)
       return CGSize(width: itemWidth, height: itemWidth + 20)
    }
       
}



extension ChildCategoryViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return childSubCategoryObj?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoryListCell", for: indexPath) as! CategoryListCell
            if let catData = self.childSubCategoryObj?[indexPath.row] {
                cell.lblTitle.text = catData.name ?? ""
            }
            cell.selectionStyle = .none
            return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.tableViewHeight += cell.frame.size.height
        categoryTableHeight.constant = self.tableViewHeight
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        selectedIndexPath = indexPath
        if !isSubcategorySelected{
            isSubcategorySelected = true
            nextButton.isHidden = false
            nextButton.tag = indexPath.row
            if let catData = self.childSubCategoryObj?[indexPath.row] {
                selectSubCaltegoryLabel.text = catData.name ?? ""
            }
            CategoryView.removeFromSuperview()
        }else{
            isSubcategorySelected = false
        }
    }
    
}
