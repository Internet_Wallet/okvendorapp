//
//  CategoriesExtension.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 16/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Foundation
import UIKit


extension CategoriesViewController {
      
        //MARK:- GetAllCategOryAPI
            func getAllCategoriesAPICall(){
                self.apiManagerClient = APIManagerClient.sharedInstance
                if AppUtility.isConnectedToNetwork() {
                    AppUtility.showLoading(self.view)
                    self.aPIManager.getAllCategories(onSuccess: {
                        self.categoryDetails = self.apiManagerClient?.allCategories
                        self.SUbCatList = self.categoryDetails?.listCategory
                        if let viewLoc = self.view {
                               AppUtility.hideLoading(viewLoc)
                           }
                        DispatchQueue.main.async {
                            self.categoryCollectionView.reloadData()
                           // self.categoryTableView.reloadData()
                        }
                    }, onError: { [weak self] message in
                         if let viewLoc = self?.view {
                                AppUtility.hideLoading(viewLoc)
                        }
                    })
                }
         }
    
    func getProductDetialsByBarCode(barCode: String, reloadFor: String) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/product/GetProductIdbyBarcode/%@", APIManagerClient.sharedInstance.base_url,barCode.trimmingCharacters(in: .whitespaces)) as String
            print("Get Product by barCode :\(urlStr) ")
            self.aPIManager.getGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        println_debug("Data error")
                        return
                    }
                    do {
                        // make sure this JSON is in the format we expect
                        if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? NSDictionary {
                            if json["Message"] as? String ?? "" == "Success." {
                                
                                DispatchQueue.main.async {
//                                    let sb = UIStoryboard(name: "EditProduct", bundle: nil)
//                                    if #available(iOS 13.0, *) {
//                                        let editVC = sb.instantiateViewController(identifier: "EditProductsNewVC") as! EditProductsNewVC
//                                        editVC.productID = "\(json["ProductId"] ?? 0)"
//                                        editVC.modalPresentationStyle = .overCurrentContext
//                                        self?.navigationController?.pushViewController(editVC, animated: true)
//                                    }
                                    
                                    if let obj = self?.storyboard?.instantiateViewController(withIdentifier: CategoryConstant.kAddProductNewViewController) as? AddProductNewViewController{
                                        obj.isComingFromBarCode = true
                                        obj.barCodeNumber = barCode
                                        obj.categoryName = json["Name"] as? String ?? ""
                                        obj.ProductID = json["ProductId"] as? Int ?? 0
                                        obj.isHavingAttributes = json["HaveAttributeInfo"] as? Bool ?? false
                                        self?.navigationController?.pushViewController(obj, animated: true)
                                    }
                                    
                                }
                            
                            } else {
                                DispatchQueue.main.async {
                                    if let viewNew = self{
                                        viewNew.alertVC = SAlertController()
                                        viewNew.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: json["Message"] as? String ?? "", onController: viewNew)
                                        let YesAction = SAlertAction()
                                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                            viewNew.navigationController?.popToRootViewController(animated: true)
                                        })
                                        viewNew.alertVC?.addAction(action: [YesAction])
                                        
                                    }
                                    
                                }
                                
                            }
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
             }
         }, onError: {  message in
            DispatchQueue.main.async {
             if let viewLoc = self.view {
                 AppUtility.hideLoading(viewLoc)
                   self.alertVC = SAlertController()
                   self.alertVC?.ShowSAlert(title: "Error", withDescription: "Found error in get product details. Please try again", onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        self.navigationController?.popViewController(animated: true)
                    })
                    self.alertVC?.addAction(action: [YesAction])
                }
            }
            })
        }
    }
    
    func getProductDetialsByIDForBarCode(productID: String, reloadFor: String) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/getproduct/%@", APIManagerClient.sharedInstance.base_url,productID.trimmingCharacters(in: .whitespaces)) as String
            print("Get Product by ID :\(urlStr) ")
            self.aPIManager.getGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        println_debug("Data error")
                        return
                    }
                    do {
                        // make sure this JSON is in the format we expect
                        if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? NSDictionary {
                            let newObj = GetProductDetailsModel(response: json)
                            if newObj.statusCode == 200{
                                println_debug(newObj)
                                
                            }else{
                                DispatchQueue.main.async {
                                    if let viewNew = self{
                                        viewNew.alertVC = SAlertController()
                                        viewNew.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "Some Error happened while saving the product.Please Try After some time", onController: viewNew)
                                        let YesAction = SAlertAction()
                                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                            viewNew.navigationController?.popToRootViewController(animated: true)
                                        })
                                        viewNew.alertVC?.addAction(action: [YesAction])
                                    }
                                }
                            }
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                    
                    
//                    do {
//
//
//
//                        do {
//                            // make sure this JSON is in the format we expect
//                            if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? NSDictionary {
//                               let newObj = GetProductDetailsModel(response: json)
//
//                                if newObj.statusCode == 200{
//                                    if self?.isHavingAttributes == true{
//                                        //this means it has extra attributes
//                                        DispatchQueue.main.async {
//                                            var vcObj: AttributeViewController?
//                                            vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.kAttributeViewController) as? AttributeViewController
//                                            if let validObj = vcObj {
//                                                validObj.productDetailsModel = model
//                                                self?.navigationController?.pushViewController(validObj, animated: true)
//                                            }
//                                        }
//                                    }else{
//
//
//
//                                    }
//                                }else{
//
//
//                                }
//
//
//
//                            }
//                        } catch let error as NSError {
//                            print("Failed to load: \(error.localizedDescription)")
//                        }
//
//
//
//
//                        let decode = JSONDecoder()
//                        let model = try? decode.decode(GetPrdouctDetailsModel.self, from: jsonData)
//                       self?.productDetailsModel = model
//                        println_debug(String(data: jsonData, encoding: .utf8))
//
//                        if model?.statusCode == 200 {
//
//                            if self?.isHavingAttributes == true{
//                                //this means it has extra attributes
//                                DispatchQueue.main.async {
//                                    var vcObj: AttributeViewController?
//                                    vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.kAttributeViewController) as? AttributeViewController
//                                    if let validObj = vcObj {
//                                        validObj.productDetailsModel = model
//                                        self?.navigationController?.pushViewController(validObj, animated: true)
//                                    }
//                                }
//                            }else{
//
//
//
//                            }
//
//
////                               if let price =  model?.data?.productPrice?.price, !price.isEmpty {
////                                self?.addProductParam.setValue(price.replacingOccurrences(of: "MMK", with: "").replacingOccurrences(of: " ", with: ""), forKey: CategoryConstant.AddProductKey.ProducPrice.rawValue)
////                               }else {
////                                self?.addProductParam.setValue((model?.data?.productPrice?.price ?? "").replacingOccurrences(of: " ", with: ""), forKey: CategoryConstant.AddProductKey.ProducPrice.rawValue)
////                               }
////
////
////                               let other = model?.data?.otherLanguageDataUnicode?.replacingOccurrences(of: "\'", with: "\"")
////                               let dic = self?.convertStringToArrayOfDictionary(text: other ?? "")
////
////                            self?.addProductParam.setValue(model?.data?.name ?? "", forKey: CategoryConstant.AddProductKey.ProductNameEnglish.rawValue)
////
////                            self?.addProductParam.setValue(model?.data?.barcode ?? "", forKey: CategoryConstant.AddProductKey.ProductBarcode.rawValue)
////
////                            if let values = dic as? Dictionary<String,String> {
////
////                                self?.addProductParam.setValue(values["Name"] ?? "", forKey: CategoryConstant.AddProductKey.ProductNameUnicode.rawValue)
////
////                               }
////
////                            self?.addProductParam.setValue(model?.data?.UOM, forKey: CategoryConstant.AddProductKey.ProductUom.rawValue)
////
////                               var categoryString = ""
////                              var categoryLastName = ""
////
////                               if let catArray = model?.data?.breadcrumb?.categoryBreadcrumb {
////                                   for (item, element) in catArray.enumerated() {
////
////                                       if let name = element.name {
////                                           categoryString =  categoryString + name
////                                       }
////
////                                       if item < catArray.count - 1 {
////                                           categoryString = categoryString + " >> "
////                                       }else {
////                                           categoryLastName = element.name ?? ""
////                                       }
////
////                                   }
////
////                                  categoryLastName = catArray.last?.name ?? ""
////
////                               }
////
////
////                            self?.addProductParam.setValue("\(categoryString)", forKey: CategoryConstant.AddProductKey.ProductCategory.rawValue)
////
////
////                            if self?.productDetailsModel?.data?.productAttributes?.count ?? 0>0{
////                                //navigate to edit screen
////
////
////
////                            }else{
////                                self?.addMoreDetailsButton.setTitle("Save", for: .normal)
////                                self?.alertVC = SAlertController()
////                                self?.alertVC?.ShowSAlert(title: "", withDescription: "More Details is not available for this product", onController: self!)
////                                let YesAction = SAlertAction()
////                                YesAction.action(name: "Save".localized, AlertType: .defualt, withComplition: {
////                                    self?.alertVC?.ShowSAlert(title: "", withDescription: "Product Saved Successfully", onController: self!)
////                                    self?.navigationController?.popToRootViewController(animated: false)
////                                })
////                                let YesActionNew = SAlertAction()
////                                YesActionNew.action(name: "Discard".localized, AlertType: .defualt, withComplition: {
////                                    //call delete product api
////                                    DispatchQueue.main.async {
////                                        self?.callDeleteProductApi()
////                                    }
////                                })
////                                self?.alertVC?.addAction(action: [YesAction,YesActionNew])
////                            }
////
////                            DispatchQueue.main.async {
////                                self?.productTableView.delegate = self
////                                self?.productTableView.dataSource = self
////                                self?.productTableView.reloadData()
////                            }
//
//
//                     }else {
//                           println_debug("Product Attribute Value Not Found")
//                       }
//                 }
             }
         }, onError: {  message in
             if let viewLoc = self.view {
                 AppUtility.hideLoading(viewLoc)
                   self.alertVC = SAlertController()
                   self.alertVC?.ShowSAlert(title: "Error", withDescription: "Found error in get product details. Please try again", onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        self.navigationController?.popViewController(animated: true)
                    })
                    self.alertVC?.addAction(action: [YesAction])
                }
            })
        }
    }
   
 }


extension CategoriesViewController : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout , UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SUbCatList?.count ?? 0
           //        return 10
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
           let cell = categoryCollectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath) as! CategoriesCollectionCell
           if let currentObject = SUbCatList?[indexPath.row] {
              cell.updateCellWithData(CategoryData: currentObject, index: indexPath.row)
            
           }
           return cell
       }
       
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
           selectedIndex = indexPath
          let currentObject = SUbCatList?[indexPath.row]
          categoryname = currentObject?.name ?? "Category"
        
           self.TapSubcategoary(indexPath: indexPath)
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let itemWidth = HomeConstant.getItemWidth(boundWidth: collectionView.bounds.size.width + 5)
           return CGSize(width: itemWidth, height: itemWidth + 20)
       }
    
    
}


extension CategoriesViewController : UITableViewDataSource , UITableViewDelegate {
    
    //MARK:- TableViewDataSource
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
              return self.SUbCatList?.count ?? 1
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              let cell = tableView.dequeueReusableCell(withIdentifier: "categoryListCell", for: indexPath) as! CategoryListCell
              if let catData = self.SUbCatList?[indexPath.row] {
                  cell.lblTitle.text = catData.name ?? "Select Category"
               
              }
              cell.selectionStyle = .none
              return cell
      }
      
      //MARK:- TableViewDelegate
      
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          
        
           selectedIndex = indexPath
         // var isNextSubCategory = false
          let catData = self.SUbCatList?[indexPath.row]
          categoryname = catData?.name ?? "Category"
        
          if tableView == categoryTableView {
            storeSubCategory(index: indexPath.row)
            BtnNext.isHidden = false
            nextViewHeight.constant = 75
            self.CategoryView.removeFromSuperview()
            }
            
            
            
//              if let SUbCat = self.SUbCatList?[indexPath.row] {
//                  if let SubCatData = SUbCat.Children , SubCatData.count > 0 {
//                      self.categoryInputArray.append(SUbCat.name ?? "")
//                      self.categoryInputSubCatArray.append(SUbCat.Children ?? [])
//                      self.SUbCatList = SUbCat.Children
//                      isNextSubCategory = true
//                  }
//              }
//            if self.SUbCatList?.count ?? 1 > 0 && isNextSubCategory {
//                  self.upDateTheUI()
//                  self.categoryTableView.reloadData()
//              }else {
//                  var catName = ""
//                  if let catData = self.SUbCatList?[indexPath.row] {
//                      self.categoryID = catData.Id ?? 0
//                      self.ProductDetails.updateValue("\(self.categoryID)",forKey: "CategoryId")
//                      for (_ ,name) in self.categoryInputArray.enumerated() {
//                          catName.append(name)
//                          catName.append(">>")
//                      }
//                      catName.append(catData.name ?? "")
//                  }
//                  UIView.animate(withDuration: 0.2) {
//                      let frame = CGRect.init(x: 20, y: self.view.frame.size.height / 2 + 10 , width: self.view.frame.size.width - 40 , height: 0)
//                      self.CategoryView.frame = frame
//                      self.categoryInputArray.removeAll()
//                      self.CategoryView.removeFromSuperview()
//                  }
//              }
       //   }
      }
      
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
              return 55
      }
}
