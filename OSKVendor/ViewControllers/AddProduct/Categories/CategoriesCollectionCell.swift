//
//  CategoriesCollectionCell.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 16/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class CategoriesCollectionCell: UICollectionViewCell {
    
    @IBOutlet var tickImageView: UIImageView!
    @IBOutlet var mainBackView: UIView!
    @IBOutlet var subCategoryImageView: UIImageView!
    @IBOutlet var subCategoryNameLbl: UILabel!{
        didSet {
            self.subCategoryNameLbl.font = UIFont(name: appFont, size: 15.0)
            self.subCategoryNameLbl.numberOfLines = 2
            self.subCategoryNameLbl.text = self.subCategoryNameLbl.text?.localized
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainBackView.shadowToColleectionView()
    }
    
    func updateCellWithData(CategoryData: Category , index : Int) {
        subCategoryImageView.sd_setImage(with: URL(string: CategoryData.iconPath ?? "") , completed: nil)
        subCategoryImageView.clipsToBounds = true
        subCategoryNameLbl.text = CategoryData.name
    }

}
