//
//  CategoryConstant.swift
//  OSKVendor
//
//  Created by Tushar Lama on 28/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Foundation
import UIKit


struct CategoryConstant {
    
    static let kSubCategoryViewController = "SubCategoryViewController"
    static let kChildCategoryViewController = "ChildCategoryViewController"
    static let kScanBarcodeTableCell = "ScanBarcodeTableCell"
    static let kScanBarcodeCell = "ScanBarcodeCell"
    static let kAddProductsViewController = "EditProducts"
    static let kDescriptionCell = "DescriptionCell"
    static let kAddProductsFieldsCell = "addProductsFieldsCell"
    static let kAddProductTableViewCell = "addProductTableViewCell"
    static let kPriceUMOTableViewCell = "PriceUMOTableViewCell"
    static let kPriceUMOTableViewXIB = "PriceUMOCell"
    static let kAddProductNewViewController = "AddProductNewViewController"
    static let kAddProductCell = "AddProductCell"
    static let kCameraVideoBarcodeVC = "CameraVideoBarcodeVC"
    static let kEditProductImagesCell = "EditProductImagesCell"
    static let kAddUOMCell = "UOMCell"
    static let kProductDescriptionCell = "ProductDescriptionCell"
    static let kProductDescriptionUnicodeCell = "ProductDescriptionUnicodeCell"
    static let kAddMoreDetails = "AddMoreDetails"
    static let kSelectProductCategoryCell = "SelectProductCategoryCell"
    
    static let VENDORBLUE = UIColor(red: 253.0/255.0, green: 247.0/255.0, blue: 252.0/255.0, alpha: 1.0)
    
    
    enum AddProductKey: String {
        case ProductNameEnglish =  "ProductNameEnglish"
        case ProductNameUnicode = "ProductNameUnicode"
        case ProducPrice = "ProducPrice"
        case ProductUom = "ProductUom"
        case ProductBarcode = "ProductBarcode"
        case ProductCategory = "ProductCategory"
        case ProductDescription = "ProductDescription"
        case ProductDescriptionInUnicode = "ProductDescriptionInUnicode"
        
    }
    
     
    
}



