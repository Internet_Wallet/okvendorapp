//
//  CategoriesViewController.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 15/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class CategoriesViewController: OSKBaseViewController, ProductBarScannerDelegate {
    
    
      @IBOutlet weak var categoryCollectionView : UICollectionView!
      @IBOutlet weak var BarCodeMainView : UIView!
      @IBOutlet weak var BarCodeView : UIView!
    
      @IBOutlet weak var categoryMainView : UIView!
      @IBOutlet weak var categorySelectionView : UIView!
      @IBOutlet weak var categorySelectionTitle: UILabel!{
        didSet{
            self.categorySelectionTitle.font = UIFont(name: appFont, size: 18.0)
            self.categorySelectionTitle.text = self.categorySelectionTitle.text?.localized
        }
    }
    @IBOutlet weak var barcodeTitleLBL: UILabel!{
      didSet{
          self.barcodeTitleLBL.font = UIFont(name: appFont, size: 18.0)
          self.barcodeTitleLBL.text = self.barcodeTitleLBL.text?.localized
      }
    }
    @IBOutlet weak var messageBarcodeLBL: UILabel!{
      didSet{
          self.messageBarcodeLBL.font = UIFont(name: appFont, size: 14.0)
          self.messageBarcodeLBL.text = self.messageBarcodeLBL.text?.localized
      }
    }
     
      @IBOutlet weak var nextViewHeight : NSLayoutConstraint!
      @IBOutlet weak var barcodeViewHeight :NSLayoutConstraint!
      @IBOutlet weak var infoMessageHeight :NSLayoutConstraint!
      
    
      @IBOutlet var gestureTapView: UIView!
    
       @IBOutlet var BtnNext: UIButton!{
           didSet{
              BtnNext.setTitle("Next".localized, for: .normal)
           }
       }
      
       @IBOutlet var CategoryView : UIView!
       @IBOutlet var categoryTableView: UITableView!
       @IBOutlet var categoryBtn: UIButton!
       
       
       let collectionCell = "SubCategoryCell"
       let addProductIdentifier = "addProductsFieldsCell"
       let deliveryIdentifier = "DevliveryTimeAddProductCell"
       
       var cellTitle : [String]!
       var cellSubTitle : [String]!
       
       var alertVC : SAlertController?
       
       var categoryInputArray : [String] = [String]()
       var categoryInputSubCatArray : [[Category]] = [[Category]]()
       
       let aPIManager = APIManager()
       var apiManagerClient : APIManagerClient?
       
       var categoryDetails : AllCategory?
       var SUbCatList : [Category]?
       
       var categoryID : Int = 0
       var ProductID : Int = -1999
       
       var otherLanguageData : [String : Any]  = [String : Any]()
       var ProductDetails : [String : Any]  = [String : Any]()
    
       var frameOrigionY : CGFloat = 0
       var isCategorySelectionViewVisible : Bool = false
       var subCategoryObj : [Category]? = []
       var selectedIndex: IndexPath?
    
       var categoryname = ""
    
    

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = "Category".localized
        
        self.categoryCollectionView.register(UINib(nibName: "CategoriesCollectionCell", bundle: nil), forCellWithReuseIdentifier: collectionCell)
        self.categoryCollectionView.delegate = self
        self.categoryCollectionView.dataSource = self

        // Do any additional setup after loading the view.
        
        self.BarCodeView.shadowToNormalView()
        self.categorySelectionView.shadowToNormalView()
        
        self.BarCodeView.layer.cornerRadius = 20
        self.categorySelectionView.layer.cornerRadius = 20
        
        self.CategoryView.layer.cornerRadius = 3
        self.CategoryView.shadowToNormalView()
        
        let tapGestureRegonizer = UITapGestureRecognizer(target: self, action:
        #selector(CategoriesViewController.viewCellTapped(recognizer:)))
            tapGestureRegonizer.numberOfTapsRequired = 1
          BarCodeMainView.addGestureRecognizer(tapGestureRegonizer)

    
    }
    
    @objc func viewCellTapped (recognizer: UITapGestureRecognizer){
        print("label Tapped")
        isCategorySelectionViewVisible = false
        self.CategoryView.removeFromSuperview()

}
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAllCategoriesAPICall()
        isCategorySelectionViewVisible = false
        self.CategoryView.removeFromSuperview()
        self.messageBarcodeLBL.isHidden = true
        self.infoMessageHeight.constant = 0
        self.categoryCollectionView.reloadData()
        if self.categorySelectionTitle.text == "Select Product Category".localized {
            BtnNext.isHidden = true
            self.nextViewHeight.constant = 0
        }
        else {
            BtnNext.isHidden = false
            self.nextViewHeight.constant = 75
        }
    }
  
    
    @IBAction func barcodeButtonACTION ( sender : UIButton) {
        DispatchQueue.main.async {
             if #available(iOS 13.0, *) {
                 let storyBoard = UIStoryboard(name: "EditProduct", bundle: nil)
                 let vc = storyBoard.instantiateViewController(identifier: "ProductBarScanner") as! ProductBarScanner
                 vc.delegate = self
                 self.navigationController?.pushViewController(vc, animated: true)
             } else {
                 // Fallback on earlier versions
             }
         }
    }
    
    @IBAction func CategoryACTION ( sender : UIButton) {
      //  categoryTableView.isHidden = false
        self.nextViewHeight.constant = 75
        self.upDateTheUI()
        
    }
    
    @IBAction func infoBtnAction ( sender : UIButton) {
        
      //  If your product available in server , details will auto fill up .
        
        isCategorySelectionViewVisible = false
        self.CategoryView.removeFromSuperview()
        
        self.alertVC = SAlertController()
        self.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "If your product available in server , details will auto fill up .".localized, onController: self)
         let YesAction = SAlertAction()
         YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
            // self.navigationController?.popViewController(animated: true)
         })
         self.alertVC?.addAction(action: [YesAction])
        
        
//
//        if sender.isSelected {
//            sender.isSelected = false
//            infoMessageHeight.constant = 0
//            self.messageBarcodeLBL.isHidden = true
//        }
//        else {
//            sender.isSelected = true
//            if let lang = appDelegate.getSelectedLanguage() as? String , lang == "en" {
//                infoMessageHeight.constant = 20
//            }
//            else {
//                infoMessageHeight.constant = 80
//            }
//            self.messageBarcodeLBL.isHidden = false
//        }
        
    }
    
    @IBAction func backAction ( sender : Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func returnBarCode(barCade: String) {
        print(" BarCode Is : \(barCade)")
        self.getProductDetialsByBarCode(barCode: barCade , reloadFor: "")

    }
    
    func upDateTheUI() {
        if !isCategorySelectionViewVisible {
            isCategorySelectionViewVisible = true
            self.CategoryView.frame = CGRect(x: 25, y: (self.categoryMainView.frame.size.height + self.BarCodeMainView.frame.size.height + 84 ) - 10 , width: self.categorySelectionView.frame.size.width  , height: 0)
            self.view.addSubview(self.CategoryView)
            UIView.animate(withDuration: 0.2) {
            let height = (self.SUbCatList?.count ?? 1) * 55 + 50
             if height < 400 {
                self.CategoryView.frame = CGRect(x: 25, y: (self.categoryMainView.frame.size.height + self.BarCodeMainView.frame.size.height + 84 ) - 10 , width: self.categorySelectionView.frame.size.width  , height: CGFloat(height))
             }
             else {
                   self.CategoryView.frame = CGRect(x: 25 , y: (self.categoryMainView.frame.size.height + self.BarCodeMainView.frame.size.height + 84) - 10 , width: self.categorySelectionView.frame.size.width , height: 420)
                 }
             }
            
            self.categoryTableView.reloadData()
        }else{
            isCategorySelectionViewVisible = false
            self.CategoryView.removeFromSuperview()
        }
    }
    
    @objc func closeAction () {
        
        if categoryInputArray.count == 0 {
            UIView.animate(withDuration: 0.2) {
                let frame = CGRect.init(x: 20, y: self.view.frame.size.height / 2 + 10 , width: self.view.frame.size.width - 40 , height: 0)
                self.CategoryView.frame = frame
                self.CategoryView.removeFromSuperview()
            }
        }
        else {
            self.categoryInputArray.removeLast()
            self.categoryInputSubCatArray.removeLast()
            self.SUbCatList = self.categoryInputSubCatArray.last
            self.upDateTheUI()
            self.categoryTableView.reloadData()
        }
    }
    
    @IBAction func onClickSaveAndNextAction( sender : UIButton) {
        navigate()
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
    }
    
    //this function will store selected sub category when we select from drop down or collection view
    func storeSubCategory(index: Int){
        if let SUbCat = self.SUbCatList?[index] {
            categorySelectionTitle.text = SUbCat.name
            if let _ = subCategoryObj{
                subCategoryObj = nil
                subCategoryObj = SUbCat.Children
            }else{
                subCategoryObj = SUbCat.Children
            }
            
        }
    }
    
    
    
    func navigateToAddProduct(index: Int){
        if subCategoryObj?.count ?? 0 == 0{
            if let isValidIndex = selectedIndex{
                guard let cell = self.categoryCollectionView.cellForItem(at: isValidIndex) as? CategoriesCollectionCell else{
                    return
                }
                cell.tickImageView.image = nil
            }
            
            if let SUbCat = self.SUbCatList?[index] {
                if #available(iOS 13.0, *) {
                    let obj = self.storyboard?.instantiateViewController(identifier: CategoryConstant.kAddProductNewViewController) as? AddProductNewViewController
                    if let isValid = obj{
                            isValid.selectedCategory = SUbCat.Id
                        isValid.isHavingAttributes = SUbCat.haveInfoAttribute ?? false
                        isValid.categoryName = SUbCat.name ?? ""
                        self.navigationController?.pushViewController(isValid, animated: true)
                    }
                } else {
                    // Fallback on earlier versions
                    if let obj = self.storyboard?.instantiateViewController(withIdentifier: CategoryConstant.kAddProductNewViewController) as? AddProductNewViewController{
                        obj.selectedCategory = SUbCat.Id
                        obj.categoryName = SUbCat.name ?? ""
                        obj.isHavingAttributes = SUbCat.haveInfoAttribute ?? false
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }
            }
        }
    }
    
    
    
    func navigate(){
        if let hasValue = subCategoryObj{
            if hasValue.count>0{
                if let isValidIndex = selectedIndex{
                    guard let cell = self.categoryCollectionView.cellForItem(at: isValidIndex) as? CategoriesCollectionCell else{
                        return
                    }
                    cell.tickImageView.image = nil
                }

                if #available(iOS 13.0, *) {
                    let obj = self.storyboard?.instantiateViewController(identifier: CategoryConstant.kSubCategoryViewController) as? SubCategoryViewController
                    if let isValid = obj{
                        isValid.subCategoryObj =  subCategoryObj
                        let backBarButtonItem = UIBarButtonItem()
                                backBarButtonItem.title = nil
                        isValid.navigationItem.backBarButtonItem = backBarButtonItem
                        
                        isValid.categoryname = categoryname
                        
                        self.navigationController?.pushViewController(isValid, animated: true)
                    }
                } else {
                    // Fallback on earlier versions
                    if let obj = self.storyboard?.instantiateViewController(withIdentifier: CategoryConstant.kSubCategoryViewController) as? SubCategoryViewController{
                        obj.subCategoryObj =  subCategoryObj
                        obj.categoryname = categoryname
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }
            }
        }
        
    }
    
    
    func TapSubcategoary(indexPath: IndexPath)  {
        storeSubCategory(index: indexPath.row)
        
        guard let cell = self.categoryCollectionView.cellForItem(at: indexPath) as? CategoriesCollectionCell else{
            return
        }
        
        cell.tickImageView.image = UIImage(named: "tick_green")
        AppUtility.showLoading(self.view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            AppUtility.hideLoading(self.view)
            
            if self.subCategoryObj?.count ?? 0 > 0{
                self.navigate()
            }else{
                self.navigateToAddProduct(index: indexPath.row)
            }
        })
    }
    
}
