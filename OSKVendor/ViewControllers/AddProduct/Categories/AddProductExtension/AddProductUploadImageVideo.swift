//
//  AddProductUploadImageVideo.swift
//  OSKVendor
//
//  Created by Tushar Lama on 30/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Photos
import AVKit
import DKImagePickerController



extension AddProductNewViewController{
    
    
    //MARK:- UPLOAD IMAGE
    func imageUpload(productID: String, imgName: String , fileData: Data ,handler: @escaping (_ isSuccess: Bool) -> Void) {
        
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlString = String(format: "%@/vendor/addproductpicture", APIManagerClient.sharedInstance.base_url) as String
            println_debug(urlString)
            let params = ["ProductId": productID.trimmingCharacters(in: .whitespaces),"DisplayOrder": "1","OverrideAltAttribute": "test","OverrideTitleAttribute": "test"]
            println_debug(urlString)
            let request = NSMutableURLRequest(url:URL(string: urlString)!)
            request.httpMethod = "POST";
            let boundary = "Boundary-\(UUID2)"
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.httpBody = createBodyWithParametersForImage(parameters: params, filePathKey: imgName, imageDataKey: fileData as Data, boundary: boundary) as Data
            request.setValue(Vendor_loginmodel.shared.token, forHTTPHeaderField: "Token")
            request.setValue(aPIManager.nstKEY, forHTTPHeaderField: "NST")
            request.setValue(aPIManager.deviceId, forHTTPHeaderField: "DeviceId")
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                (data, response, error) in
                DispatchQueue.main.async {
                    do {
                        let responseX =  try JSONSerialization.jsonObject(with: data ?? Data(), options: []) as? [String: Any]
                        println_debug(responseX)
                    } catch { print(error.localizedDescription)}
                    if let viewLoc = self.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    if error != nil {
                        handler(false)
                        return
                    }
                    println_debug(response)
                    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                       handler(true)
                    }else {
                        handler(false)
                    }
                }
            }
            task.resume()
        }
    }
    
    //MARK:- ImageUpload
    func uploadImageToserver(productID : Int)  {
        if imageData.count > 0 {
            self.imageUpload(productID: String(productID) , imgName: imageNameFromServer[uploadedImagesCount - 1], fileData: imageDataFromServer[uploadedImagesCount - 1], handler: { isSuccess in
                if isSuccess {
                    self.uploadedImagesCount = self.uploadedImagesCount - 1
                    if self.uploadedImagesCount == 0 {
                        DispatchQueue.main.async { [self] in
                            self.getProductDetialsByID(productID:  "\(self.ProductID)" , reloadFor: "")
                        }
                    }else {
                        self.uploadImageToserver(productID: productID)
                    }
                }else {
                    self.alertVC = SAlertController()
                    self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErrorUploading.messageImage, onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    })
                    self.alertVC?.addAction(action: [YesAction])
                    return
                }
            })
        }
     }
    
    //MARK:- UPLOAD VIDEO
    func videoUpload(productID: String, videoName : String , fileData: Data ,handler: @escaping (_ isSuccess: Bool) -> Void) {
        
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlString = String(format: "%@/vendor/addproductvideo", APIManagerClient.sharedInstance.base_url) as String
            println_debug(urlString)
            let params = ["ProductId": productID.trimmingCharacters(in: .whitespaces),"DisplayOrder": "1","OverrideAltAttribute": "test","OverrideTitleAttribute": "test"]
            println_debug(urlString)
            let request = NSMutableURLRequest(url:URL(string: urlString)!)
            request.httpMethod = "POST";
            let boundary = "Boundary-\(UUID2)"
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.httpBody = createBodyWithParametersForVideo(parameters: params, filePathKey: videoName , imageDataKey: fileData as Data, boundary: boundary) as Data
            request.setValue(Vendor_loginmodel.shared.token, forHTTPHeaderField: "Token")
            request.setValue(aPIManager.nstKEY, forHTTPHeaderField: "NST")
            request.setValue(aPIManager.deviceId, forHTTPHeaderField: "DeviceId")
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                (data, response, error) in
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    do {
                        let responseX =  try JSONSerialization.jsonObject(with: data ?? Data(), options: []) as? [String: Any]
                        println_debug(responseX)
                    } catch { print(error.localizedDescription)}
                    if error != nil {
                        handler(false)
                        return
                    }
                    println_debug(response)
                    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                       handler(true)
                    }else {
                        handler(false)
                    }
                }
            }
            task.resume()
        }
    }
    
    //MARK:- VideoUpload
    func uploadVideoToserver(productID : Int)  {
        if videoData.count > 0 {
            self.videoUpload(productID: String(productID) , videoName: imageName[uploadedVideosCount - 1] , fileData: imageData[uploadedVideosCount - 1] , handler: { (isSuccess) in
                if isSuccess {
                       self.uploadImageToserver(productID: productID)
                   // self.uploadedVideosCount = self.uploadedVideosCount - 1
//                    if self.uploadedImagesCount == 0 {
//                        DispatchQueue.main.async {
//
//                        }
//                    }else {
//                        self.uploadImageToserver(productID: productID)
//                    }
                }else {
                    self.alertVC = SAlertController()
                    self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErrorUploading.messageImage, onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    })
                    self.alertVC?.addAction(action: [YesAction])
                    return
                }
            })
        }
     }
    
}

