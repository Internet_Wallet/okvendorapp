//
//  AddProduct.swift
//  OSKVendor
//
//  Created by Tushar Lama on 30/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Photos
import AVKit
import DKImagePickerController


extension AddProductNewViewController{
    
    //MARK:- Create Body for the Image Uploading
        func createBodyWithParametersForImage(parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> NSData {
            var body = Data()
            if parameters != nil {
                for (key, value) in parameters! {
                    body.append(Data("--\(boundary)\r\n".utf8))
                    body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                    body.append(Data("\(value)\r\n".utf8))
                }
            }
            var filename = ""
            if filePathKey != "" {
                let fileArray = filePathKey?.components(separatedBy: "/")
                filename = (fileArray?.last)!
            }
            let KeyName = "File"
            let mimetype = "image/jpeg"
            body.append(Data("--\(boundary)\r\n".utf8))
            body.append(Data("Content-Disposition: form-data; name=\"\(KeyName)\"; filename=\"\(filename)\"\r\n".utf8))
            body.append(Data("Content-Type: \(mimetype)\r\n\r\n".utf8))
            body.append(imageDataKey as Data)
            body.append(Data("\r\n".utf8))
            body.append(Data("--\(boundary)--\r\n".utf8))
            return body as NSData
        }
    
    //MARK:- Create Body for the Video Uploading
    func createBodyWithParametersForVideo(parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> NSData {
        var body = Data()
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(Data("--\(boundary)\r\n".utf8))
                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data("\(value)\r\n".utf8))
            }
        }
        var filename = ""
        if filePathKey != "" {
            let fileArray = filePathKey?.components(separatedBy: "/")
            filename = (fileArray?.last)!
        }
        let KeyName = "File"
        let mimetype = "video/mp4"
        body.append(Data("--\(boundary)\r\n".utf8))
        body.append(Data("Content-Disposition: form-data; name=\"\(KeyName)\"; filename=\"\(filename)\"\r\n".utf8))
        body.append(Data("Content-Type: \(mimetype)\r\n\r\n".utf8))
        body.append(imageDataKey as Data)
        body.append(Data("\r\n".utf8))
        body.append(Data("--\(boundary)--\r\n".utf8))
        return body as NSData
     }
    
    
    func convertVideo(phAsset : PHAsset){

       PHImageManager.default().requestAVAsset(forVideo: phAsset, options: PHVideoRequestOptions(), resultHandler: { (asset, audioMix, info) -> Void in
           if let asset = asset as? AVURLAsset {
               do {
                   let videoData = try  Data.init(contentsOf: asset.url)
                   print(asset.url)
                   print("File size before compression: \(Double(videoData.count / 1048576)) mb")
                   let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".MP4")
                   print(compressedURL)
                   self.compressVideo(inputURL: asset.url , outputURL: compressedURL) { (exportSession) in
                       guard let session = exportSession else {
                           return
                       }
                       switch session.status {
                       case .unknown:
                           print("unknown")
                           break
                       case .waiting:
                           print("waiting")
                           break
                       case .exporting:
                           print("exporting")
                           break
                       case .completed:
                           do {
                           let compressedData = try  Data.init(contentsOf: compressedURL)
                                print(compressedData)
                                print("File size AFTER compression: \(Double(compressedData.count / 1048576)) mb")
                                self.videoData.append(compressedData as Data)
                                let name = String(Date().toMillis())
                                self.videoName.append("\(name).mp4")
                           }
                           catch{
                              print(error)
                           }

                       case .failed:
                           print("failed")
                           break
                       case .cancelled:
                           print("cancelled")
                           break
                       }
                   }
               } catch {
                   print(error)
                   //return
               }
           }
       })
   }
    
    func loadImageAndVideoData () {
        
        imageData.removeAll()
        imageName.removeAll()
        videoData.removeAll()
        videoName.removeAll()
        
        for (item,obj) in imageAndVideoArray.enumerated() {
                
            let asset = imageAndVideoArray[item]
            if obj.type == .photo{
                asset.fetchOriginalImage(completeBlock: { image, info in
                    let data:Data = image?.jpegData(compressionQuality: 0.5) ?? Data()
                    self.imageData.append(data)
                    self.imageDataFromServer.append(data)
                    let name = String(Date().toMillis())
                    self.imageName.append("\(name).jpeg")
                    self.imageNameFromServer.append("\(name).jpeg")
                })

            }else{
                guard let assetData = obj.originalAsset else { return }
                        self.convertVideo(phAsset: assetData)
                
            }
             
        }
            
           
//                let assetImage : PHAsset = item.originalAsset ?? PHAsset()
//             var img: UIImage?
//             let manager = PHImageManager.default()
//             let options = PHImageRequestOptions()
//             options.version = .current
//             options.isSynchronous = false
//             manager.requestImageData(for: assetImage, options: options) { data, _, _, _ in
//                 if let data = data {
//                     img = UIImage(data: data)
//                     let data:Data = img?.jpegData(compressionQuality: 0.5) ?? Data()
//                     self.imageData.append(data)
//                     let name = String(Date().toMillis())
//                     self.imageName.append("\(name).jpeg")
//                 }
            //   }
         //   }

            
        }
        
//        for (_ ,object )in self.imageAndVideoArray.enumerated() {
//                   if object.type == .photo {
//                    let assetImage : PHAsset = object.originalAsset ?? PHAsset()
//                    var img: UIImage?
//                    let manager = PHImageManager.default()
//                    let options = PHImageRequestOptions()
//                    options.version = .current
//                    options.isSynchronous = false
//                    manager.requestImageData(for: assetImage, options: options) { data, _, _, _ in
//                        if let data = data {
//                            img = UIImage(data: data)
//                            let data:Data = img?.jpegData(compressionQuality: 0.5) ?? Data()
//                            self.imageData.append(data)
//                            let name = String(Date().toMillis())
//                            self.imageName.append("\(name).jpeg")
//                        }
//                      }
//                   }
//                   if object.type == .video {
//                    guard let assetData = object.originalAsset else { return }
//                    self.convertVideo(phAsset: assetData)
//                    // notify the main thread when all task are completed
//                 }
//            }
 //       }
    
    
    
}
