//
//  EditModule.swift
//  OSKVendor
//
//  Created by Tushar Lama on 30/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Foundation
import UIKit

extension AddProductNewViewController {
    
    func convertStringToArrayOfDictionary(text: String) -> Any? {
          if let data = text.data(using: .utf8) {
              do {
                  return try JSONSerialization.jsonObject(with: data, options: []) as? Any
              } catch {
                  print(error.localizedDescription)
              }
          }
          return nil
      }
    
    func getProductDetialsByBarCode(barCode: String, reloadFor: String) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/product/GetProductIdbyBarcode/%@", APIManagerClient.sharedInstance.base_url,barCode.trimmingCharacters(in: .whitespaces)) as String
            print("Get Product by barCode :\(urlStr) ")
            self.aPIManager.getGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        println_debug("Data error")
                        return
                    }
                    do {
                        // make sure this JSON is in the format we expect
                        if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? NSDictionary {
                            if json["Message"] as? String ?? "" == "Success." {
                                //self?.getProductDetialsByIDForBarCode(productID:  "\(json["ProductId"] ?? 0)", reloadFor: "")
                                self?.getProductDetialsByID(productID:  "\(json["ProductId"] ?? 0)" , reloadFor: "")

                            } else {
                                DispatchQueue.main.async {
                                    if let viewNew = self{
                                        viewNew.alertVC = SAlertController()
                                        viewNew.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "Product is not available!".localized, onController: viewNew)
                                        let YesAction = SAlertAction()
                                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                            viewNew.navigationController?.popToRootViewController(animated: true)
                                        })
                                        viewNew.alertVC?.addAction(action: [YesAction])
                                        
                                    }
                                    
                                }
                                
                            }
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
             }
         }, onError: {  message in
             if let viewLoc = self.view {
                 AppUtility.hideLoading(viewLoc)
                   self.alertVC = SAlertController()
                   self.alertVC?.ShowSAlert(title: "Error", withDescription: "Found error in get product details. Please try again", onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        self.navigationController?.popViewController(animated: true)
                    })
                    self.alertVC?.addAction(action: [YesAction])
                }
            })
        }
    }
    
    func getProductDetialsByIDForBarCode(productID: String, reloadFor: String) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/getproduct/%@", APIManagerClient.sharedInstance.base_url,productID.trimmingCharacters(in: .whitespaces)) as String
            print("Get Product by ID :\(urlStr) ")
            self.aPIManager.getGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        println_debug("Data error")
                        return
                    }
                    do {
                        // make sure this JSON is in the format we expect
                        if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? NSDictionary {
                            let newObj = GetProductDetailsModel(response: json)
                            if newObj.statusCode == 200{
                                if self?.isHavingAttributes == true{
                                    //this means it has extra attributes
                                    DispatchQueue.main.async {
                                        var vcObj: AttributeViewController?
                                        vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.kAttributeViewController) as? AttributeViewController
                                        if let validObj = vcObj {
                                            validObj.saveDelegate = self
                                            validObj.productDetailsModel = newObj
                                            self?.navigationController?.pushViewController(validObj, animated: true)
                                        }
                                    }
                                    
                                }else{
                                    DispatchQueue.main.async {
                                        if let viewNew = self{
                                            viewNew.alertVC = SAlertController()
                                            viewNew.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "Product successfully Saved", onController: viewNew)
                                            let YesAction = SAlertAction()
                                            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                                viewNew.navigationController?.popToRootViewController(animated: true)
                                            })
                                            viewNew.alertVC?.addAction(action: [YesAction])
                                            
                                        }
                                        
                                    }
                                    
                                }
                            }else{
                                DispatchQueue.main.async {
                                    if let viewNew = self{
                                        viewNew.alertVC = SAlertController()
                                        viewNew.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "Some Error happened while saving the product.Please Try After some time", onController: viewNew)
                                        let YesAction = SAlertAction()
                                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                            viewNew.navigationController?.popToRootViewController(animated: true)
                                        })
                                        viewNew.alertVC?.addAction(action: [YesAction])
                                    }
                                }
                            }
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                    
                    
//                    do {
//
//
//
//                        do {
//                            // make sure this JSON is in the format we expect
//                            if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? NSDictionary {
//                               let newObj = GetProductDetailsModel(response: json)
//
//                                if newObj.statusCode == 200{
//                                    if self?.isHavingAttributes == true{
//                                        //this means it has extra attributes
//                                        DispatchQueue.main.async {
//                                            var vcObj: AttributeViewController?
//                                            vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.kAttributeViewController) as? AttributeViewController
//                                            if let validObj = vcObj {
//                                                validObj.productDetailsModel = model
//                                                self?.navigationController?.pushViewController(validObj, animated: true)
//                                            }
//                                        }
//                                    }else{
//
//
//
//                                    }
//                                }else{
//
//
//                                }
//
//
//
//                            }
//                        } catch let error as NSError {
//                            print("Failed to load: \(error.localizedDescription)")
//                        }
//
//
//
//
//                        let decode = JSONDecoder()
//                        let model = try? decode.decode(GetPrdouctDetailsModel.self, from: jsonData)
//                       self?.productDetailsModel = model
//                        println_debug(String(data: jsonData, encoding: .utf8))
//
//                        if model?.statusCode == 200 {
//
//                            if self?.isHavingAttributes == true{
//                                //this means it has extra attributes
//                                DispatchQueue.main.async {
//                                    var vcObj: AttributeViewController?
//                                    vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.kAttributeViewController) as? AttributeViewController
//                                    if let validObj = vcObj {
//                                        validObj.productDetailsModel = model
//                                        self?.navigationController?.pushViewController(validObj, animated: true)
//                                    }
//                                }
//                            }else{
//
//
//
//                            }
//
//
////                               if let price =  model?.data?.productPrice?.price, !price.isEmpty {
////                                self?.addProductParam.setValue(price.replacingOccurrences(of: "MMK", with: "").replacingOccurrences(of: " ", with: ""), forKey: CategoryConstant.AddProductKey.ProducPrice.rawValue)
////                               }else {
////                                self?.addProductParam.setValue((model?.data?.productPrice?.price ?? "").replacingOccurrences(of: " ", with: ""), forKey: CategoryConstant.AddProductKey.ProducPrice.rawValue)
////                               }
////
////
////                               let other = model?.data?.otherLanguageDataUnicode?.replacingOccurrences(of: "\'", with: "\"")
////                               let dic = self?.convertStringToArrayOfDictionary(text: other ?? "")
////
////                            self?.addProductParam.setValue(model?.data?.name ?? "", forKey: CategoryConstant.AddProductKey.ProductNameEnglish.rawValue)
////
////                            self?.addProductParam.setValue(model?.data?.barcode ?? "", forKey: CategoryConstant.AddProductKey.ProductBarcode.rawValue)
////
////                            if let values = dic as? Dictionary<String,String> {
////
////                                self?.addProductParam.setValue(values["Name"] ?? "", forKey: CategoryConstant.AddProductKey.ProductNameUnicode.rawValue)
////
////                               }
////
////                            self?.addProductParam.setValue(model?.data?.UOM, forKey: CategoryConstant.AddProductKey.ProductUom.rawValue)
////
////                               var categoryString = ""
////                              var categoryLastName = ""
////
////                               if let catArray = model?.data?.breadcrumb?.categoryBreadcrumb {
////                                   for (item, element) in catArray.enumerated() {
////
////                                       if let name = element.name {
////                                           categoryString =  categoryString + name
////                                       }
////
////                                       if item < catArray.count - 1 {
////                                           categoryString = categoryString + " >> "
////                                       }else {
////                                           categoryLastName = element.name ?? ""
////                                       }
////
////                                   }
////
////                                  categoryLastName = catArray.last?.name ?? ""
////
////                               }
////
////
////                            self?.addProductParam.setValue("\(categoryString)", forKey: CategoryConstant.AddProductKey.ProductCategory.rawValue)
////
////
////                            if self?.productDetailsModel?.data?.productAttributes?.count ?? 0>0{
////                                //navigate to edit screen
////
////
////
////                            }else{
////                                self?.addMoreDetailsButton.setTitle("Save", for: .normal)
////                                self?.alertVC = SAlertController()
////                                self?.alertVC?.ShowSAlert(title: "", withDescription: "More Details is not available for this product", onController: self!)
////                                let YesAction = SAlertAction()
////                                YesAction.action(name: "Save".localized, AlertType: .defualt, withComplition: {
////                                    self?.alertVC?.ShowSAlert(title: "", withDescription: "Product Saved Successfully", onController: self!)
////                                    self?.navigationController?.popToRootViewController(animated: false)
////                                })
////                                let YesActionNew = SAlertAction()
////                                YesActionNew.action(name: "Discard".localized, AlertType: .defualt, withComplition: {
////                                    //call delete product api
////                                    DispatchQueue.main.async {
////                                        self?.callDeleteProductApi()
////                                    }
////                                })
////                                self?.alertVC?.addAction(action: [YesAction,YesActionNew])
////                            }
////
////                            DispatchQueue.main.async {
////                                self?.productTableView.delegate = self
////                                self?.productTableView.dataSource = self
////                                self?.productTableView.reloadData()
////                            }
//
//
//                     }else {
//                           println_debug("Product Attribute Value Not Found")
//                       }
//                 }
             }
         }, onError: {  message in
             if let viewLoc = self.view {
                 AppUtility.hideLoading(viewLoc)
                   self.alertVC = SAlertController()
                   self.alertVC?.ShowSAlert(title: "Error", withDescription: "Found error in get product details. Please try again", onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        self.navigationController?.popViewController(animated: true)
                    })
                    self.alertVC?.addAction(action: [YesAction])
                }
            })
        }
    }
    
   
    func getProductDetialsByID(productID: String, reloadFor: String) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            var urlStr = String(format: "%@/vendor/getproductbyid/%@", APIManagerClient.sharedInstance.base_url,productID.trimmingCharacters(in: .whitespaces)) as String
            if self.isComingFromBarCode {
                urlStr = String(format: "%@/vendor/getproduct/%@", APIManagerClient.sharedInstance.base_url,productID.trimmingCharacters(in: .whitespaces)) as String
            } else {
                urlStr = String(format: "%@/vendor/getproductbyid/%@", APIManagerClient.sharedInstance.base_url,productID.trimmingCharacters(in: .whitespaces)) as String

            }
            print("Get Product by ID :\(urlStr) ")
            self.aPIManager.getGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        println_debug("Data error")
                        return
                    }
                    do {
                        // make sure this JSON is in the format we expect
                        if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? NSDictionary {
                            let newObj = GetProductDetailsModel(response: json)
                            self?.modelObj = GetProductDetailsModel(response: json)
                            if newObj.statusCode == 200{
                                
                                if  self?.isComingFromBarCode ?? false && ((self?.callApi) == true) &&  self?.addMoreDetailsButton.currentTitle == "Save".localized{
                                    DispatchQueue.main.async {
                                        if let viewNew = self{
                                            viewNew.alertVC = SAlertController()
                                            viewNew.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "Product successfully Saved", onController: viewNew)
                                            let YesAction = SAlertAction()
                                            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                                viewNew.navigationController?.popToRootViewController(animated: true)
                                            })
                                            viewNew.alertVC?.addAction(action: [YesAction])
                                        }
                                    }
                                    
                                }else  if self?.isComingFromBarCode ?? false{
                                    
                                    self?.imageDataFromServer.removeAll()
                                    self?.imageNameFromServer.removeAll()
                                    if let imageURL = newObj.data?.pictureModels{
                                        if imageURL.count>0{
                                                for i in 0..<imageURL.count{
                                                    println_debug(imageURL[i].pictureURL ?? "")
                                                    var img: UIImage?
                                                    let forecastURL = URL(string: imageURL[i].pictureURL ?? "")
                                                    let testImage = NSData(contentsOf: forecastURL! as URL)
                                                    img = UIImage(data: testImage! as Data)
                                                   let data:Data = img?.jpegData(compressionQuality: 0.5) ?? Data()
                                                    self?.imageDataFromServer.append(data)
                                                   let name = String(Date().toMillis())
                                                    self?.imageNameFromServer.append("\(name).jpeg")
                                                }
                                        }
                                    }
                                    
                                    
                                    
                                    if let price =  newObj.data?.productPrice?.price, !price.isEmpty {
                                        self?.addProductParam.setValue(price.replacingOccurrences(of: "MMK", with: "").replacingOccurrences(of: " ", with: ""), forKey: CategoryConstant.AddProductKey.ProducPrice.rawValue)
                                    }else {
                                        self?.addProductParam.setValue((newObj.data?.productPrice?.price ?? "").replacingOccurrences(of: " ", with: ""), forKey: CategoryConstant.AddProductKey.ProducPrice.rawValue)
                                    }
                                    
                                    let other = newObj.data?.otherLanguageDataUnicode?.replacingOccurrences(of: "\'", with: "\"")
                                    let dic = self?.convertStringToArrayOfDictionary(text: other ?? "")
                                    
                                    self?.addProductParam.setValue(newObj.data?.name ?? "", forKey: CategoryConstant.AddProductKey.ProductNameEnglish.rawValue)
                                    
                                    
                                    self?.addProductParam.setValue(newObj.data?.uom ?? "", forKey: CategoryConstant.AddProductKey.ProductUom.rawValue)
                                    
                                    
                                    
                                    if let values = dic as? Dictionary<String,String> {
                                        
                                        self?.addProductParam.setValue(values["Name"] ?? "", forKey: CategoryConstant.AddProductKey.ProductNameUnicode.rawValue)
                                        
                                        self?.addProductParam.setValue(values["ShortDescription"] ?? "", forKey: CategoryConstant.AddProductKey.ProductDescription.rawValue)
                                        
                                        self?.addProductParam.setValue(values["FullDescription"] ?? "", forKey: CategoryConstant.AddProductKey.ProductDescriptionInUnicode.rawValue)
                                    }
                                    
                                    
                                    if let catArray = newObj.data?.breadcrumb?.categoryBreadcrumb {
                                        self?.selectedCategory = catArray.last?.id ?? 0
                                    }
                                    
                                    DispatchQueue.main.async{
                                        self?.productTableView.delegate = self
                                        self?.productTableView.dataSource = self
                                        self?.productTableView.reloadData()
                                    }
                                } else if !(self?.isComingFromBarCode ?? false) && self?.isHavingAttributes ?? false{
                                    //this means it has extra attributes
                                    DispatchQueue.main.async {
                                        var vcObj: AttributeViewController?
                                        vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.kAttributeViewController) as? AttributeViewController
                                        if let validObj = vcObj {
                                            validObj.saveDelegate = self
                                            validObj.productDetailsModel = newObj
                                            self?.navigationController?.pushViewController(validObj, animated: true)
                                        }
                                    }
                                }else{
                                    DispatchQueue.main.async {
                                        if let viewNew = self{
                                            viewNew.alertVC = SAlertController()
                                            viewNew.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "Product successfully Saved", onController: viewNew)
                                            let YesAction = SAlertAction()
                                            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                                viewNew.navigationController?.popToRootViewController(animated: true)
                                            })
                                            viewNew.alertVC?.addAction(action: [YesAction])
                                            
                                        }
                                    }
                                }
                                
                            }else{
                                DispatchQueue.main.async {
                                    if let viewNew = self{
                                        viewNew.alertVC = SAlertController()
                                        viewNew.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "Some Error happened while saving the product.Please Try After some time", onController: viewNew)
                                        let YesAction = SAlertAction()
                                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                            viewNew.navigationController?.popToRootViewController(animated: true)
                                        })
                                        viewNew.alertVC?.addAction(action: [YesAction])
                                    }
                                }
                            }
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                    
                    
//                    do {
//
//
//
//                        do {
//                            // make sure this JSON is in the format we expect
//                            if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? NSDictionary {
//                               let newObj = GetProductDetailsModel(response: json)
//
//                                if newObj.statusCode == 200{
//                                    if self?.isHavingAttributes == true{
//                                        //this means it has extra attributes
//                                        DispatchQueue.main.async {
//                                            var vcObj: AttributeViewController?
//                                            vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.kAttributeViewController) as? AttributeViewController
//                                            if let validObj = vcObj {
//                                                validObj.productDetailsModel = model
//                                                self?.navigationController?.pushViewController(validObj, animated: true)
//                                            }
//                                        }
//                                    }else{
//
//
//
//                                    }
//                                }else{
//
//
//                                }
//
//
//
//                            }
//                        } catch let error as NSError {
//                            print("Failed to load: \(error.localizedDescription)")
//                        }
//
//
//
//
//                        let decode = JSONDecoder()
//                        let model = try? decode.decode(GetPrdouctDetailsModel.self, from: jsonData)
//                       self?.productDetailsModel = model
//                        println_debug(String(data: jsonData, encoding: .utf8))
//
//                        if model?.statusCode == 200 {
//
//                            if self?.isHavingAttributes == true{
//                                //this means it has extra attributes
//                                DispatchQueue.main.async {
//                                    var vcObj: AttributeViewController?
//                                    vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.kAttributeViewController) as? AttributeViewController
//                                    if let validObj = vcObj {
//                                        validObj.productDetailsModel = model
//                                        self?.navigationController?.pushViewController(validObj, animated: true)
//                                    }
//                                }
//                            }else{
//
//
//
//                            }
//
//
////                               if let price =  model?.data?.productPrice?.price, !price.isEmpty {
////                                self?.addProductParam.setValue(price.replacingOccurrences(of: "MMK", with: "").replacingOccurrences(of: " ", with: ""), forKey: CategoryConstant.AddProductKey.ProducPrice.rawValue)
////                               }else {
////                                self?.addProductParam.setValue((model?.data?.productPrice?.price ?? "").replacingOccurrences(of: " ", with: ""), forKey: CategoryConstant.AddProductKey.ProducPrice.rawValue)
////                               }
////
////
////                               let other = model?.data?.otherLanguageDataUnicode?.replacingOccurrences(of: "\'", with: "\"")
////                               let dic = self?.convertStringToArrayOfDictionary(text: other ?? "")
////
////                            self?.addProductParam.setValue(model?.data?.name ?? "", forKey: CategoryConstant.AddProductKey.ProductNameEnglish.rawValue)
////
////                            self?.addProductParam.setValue(model?.data?.barcode ?? "", forKey: CategoryConstant.AddProductKey.ProductBarcode.rawValue)
////
////                            if let values = dic as? Dictionary<String,String> {
////
////                                self?.addProductParam.setValue(values["Name"] ?? "", forKey: CategoryConstant.AddProductKey.ProductNameUnicode.rawValue)
////
////                               }
////
////                            self?.addProductParam.setValue(model?.data?.UOM, forKey: CategoryConstant.AddProductKey.ProductUom.rawValue)
////
////                               var categoryString = ""
////                              var categoryLastName = ""
////
////                               if let catArray = model?.data?.breadcrumb?.categoryBreadcrumb {
////                                   for (item, element) in catArray.enumerated() {
////
////                                       if let name = element.name {
////                                           categoryString =  categoryString + name
////                                       }
////
////                                       if item < catArray.count - 1 {
////                                           categoryString = categoryString + " >> "
////                                       }else {
////                                           categoryLastName = element.name ?? ""
////                                       }
////
////                                   }
////
////                                  categoryLastName = catArray.last?.name ?? ""
////
////                               }
////
////
////                            self?.addProductParam.setValue("\(categoryString)", forKey: CategoryConstant.AddProductKey.ProductCategory.rawValue)
////
////
////                            if self?.productDetailsModel?.data?.productAttributes?.count ?? 0>0{
////                                //navigate to edit screen
////
////
////
////                            }else{
////                                self?.addMoreDetailsButton.setTitle("Save", for: .normal)
////                                self?.alertVC = SAlertController()
////                                self?.alertVC?.ShowSAlert(title: "", withDescription: "More Details is not available for this product", onController: self!)
////                                let YesAction = SAlertAction()
////                                YesAction.action(name: "Save".localized, AlertType: .defualt, withComplition: {
////                                    self?.alertVC?.ShowSAlert(title: "", withDescription: "Product Saved Successfully", onController: self!)
////                                    self?.navigationController?.popToRootViewController(animated: false)
////                                })
////                                let YesActionNew = SAlertAction()
////                                YesActionNew.action(name: "Discard".localized, AlertType: .defualt, withComplition: {
////                                    //call delete product api
////                                    DispatchQueue.main.async {
////                                        self?.callDeleteProductApi()
////                                    }
////                                })
////                                self?.alertVC?.addAction(action: [YesAction,YesActionNew])
////                            }
////
////                            DispatchQueue.main.async {
////                                self?.productTableView.delegate = self
////                                self?.productTableView.dataSource = self
////                                self?.productTableView.reloadData()
////                            }
//
//
//                     }else {
//                           println_debug("Product Attribute Value Not Found")
//                       }
//                 }
             }
         }, onError: {  message in
             if let viewLoc = self.view {
                 AppUtility.hideLoading(viewLoc)
                   self.alertVC = SAlertController()
                   self.alertVC?.ShowSAlert(title: "Error", withDescription: "Found error in get product details. Please try again", onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        self.navigationController?.popViewController(animated: true)
                    })
                    self.alertVC?.addAction(action: [YesAction])
                }
            })
        }
    }
    
    func callDeleteProductApi(){
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/deleteproductfromvendor/%@", APIManagerClient.sharedInstance.base_url,"\(self.ProductID)") as String
            self.aPIManager.postGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        println_debug("Data error")
                        return
                    }
                    do {
                        let decode = JSONDecoder()
                        let model = try? decode.decode(GetPrdouctDetailsModel.self, from: jsonData)
                        self?.productDetailsModel = model
                        println_debug(String(data: jsonData, encoding: .utf8))
                        
                        if model?.statusCode == 200 {
                            self?.navigationController?.popToRootViewController(animated: false)
                        }else {
                            println_debug("Product Attribute Value Not Found")
                        }
                    }
             }
         }, onError: {  message in
             if let viewLoc = self.view {
                 AppUtility.hideLoading(viewLoc)
                   self.alertVC = SAlertController()
                   self.alertVC?.ShowSAlert(title: "Error", withDescription: "Found error while deleting product . Please try again", onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        self.navigationController?.popViewController(animated: true)
                    })
                    self.alertVC?.addAction(action: [YesAction])
                }
            })
        }
    }
    
}




extension AddProductNewViewController: ChangeSaveButtonState {
    func changeSaveButtonState(callApi: Bool){
        self.callApi = callApi
        addMoreDetailsButton.setTitle("Save".localized, for: .normal)
        productTableView.reloadData()
    }
}
