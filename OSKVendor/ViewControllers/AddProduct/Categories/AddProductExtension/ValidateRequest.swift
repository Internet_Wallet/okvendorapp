//
//  ValidateRequest.swift
//  OSKVendor
//
//  Created by Tushar Lama on 30/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Photos
import AVKit
import DKImagePickerController


extension AddProductNewViewController{
    
    func validateTheUserInputs() -> Bool {
        
        var Productname , Productprice , Productcategory  , ProductNameUnicode , ProductUOM : String!
       
        Productname = addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductNameEnglish.rawValue) as? String ?? ""
        Productprice = addProductParam.value(forKey: CategoryConstant.AddProductKey.ProducPrice.rawValue) as? String ?? ""
      
        if let data = selectedCategory{
            Productcategory = "\(data)"
        }else{
            Productcategory = ""
        }
        ProductNameUnicode = addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductNameUnicode.rawValue) as? String ?? ""
        ProductUOM = addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductUom.rawValue) as? String ?? ""
      
        if isComingFromBarCode{
            
            if imageAndVideoArray.count < 1 && modelObj?.data?.pictureModels?.count ?? 0 < 1{
                self.alertVC = SAlertController()
                self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErroMessage.errProductImage, onController: self)
                let YesAction = SAlertAction()
                YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                })
                self.alertVC?.addAction(action: [YesAction])
                return false
            }
            
            
        }else{
            if imageAndVideoArray.count < 1 {
                self.alertVC = SAlertController()
                self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErroMessage.errProductImage, onController: self)
                let YesAction = SAlertAction()
                YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                })
                self.alertVC?.addAction(action: [YesAction])
                return false
            }
        }
        
       
        if Productname == "" {
            self.alertVC = SAlertController()
            self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErroMessage.errProductName, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
            })
            self.alertVC?.addAction(action: [YesAction])
            return false
        }else if ProductNameUnicode == "" {
            self.alertVC = SAlertController()
            self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErroMessage.errProductNameUnicode, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
            })
            self.alertVC?.addAction(action: [YesAction])
            return false
        }else if Productprice == "" {
            self.alertVC = SAlertController()
            self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErroMessage.errProductPrice, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
            })
            self.alertVC?.addAction(action: [YesAction])
            return false
        }else if ProductUOM == "" {
            self.alertVC = SAlertController()
            self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: AppConstants.ErroMessage.errProductUOM, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
            })
            self.alertVC?.addAction(action: [YesAction])
            return false
        }
        if callApi &&  addMoreDetailsButton.currentTitle == "Save".localized{
            self.ProductDetails.updateValue("\(ProductID)", forKey: "Id")
        }
        
        self.ProductDetails.updateValue(modelObj?.data?.barcode ?? "", forKey: "Barcode")
        self.ProductDetails.updateValue(modelObj?.data?.barCodeImageURL ?? "", forKey: "BarCodeImageUrl")

        self.ProductDetails.updateValue(Productcategory ?? "", forKey: "CategoryId")
        self.ProductDetails.updateValue(Productprice ?? "", forKey: "Price")
        self.ProductDetails.updateValue("1", forKey: "DeliveryDateId")
        self.ProductDetails.updateValue(Productname ?? "", forKey: "Name")
        self.ProductDetails.updateValue("\(addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductDescription.rawValue) as? String ?? "")", forKey: "ShortDescription")
        
        self.otherLanguageData.updateValue(ProductNameUnicode ?? "", forKey: "Name")
        self.otherLanguageData.updateValue("\(addProductParam.value(forKey: CategoryConstant.AddProductKey.ProductDescription.rawValue) as? String ?? "")", forKey: "ShortDescription")
        self.otherLanguageData.updateValue("", forKey: "FullDescription")
        let jsonData = try! JSONSerialization.data(withJSONObject: otherLanguageData, options: [])
        let jsonString = String(data: jsonData, encoding: .utf8)!
        self.ProductDetails.updateValue(jsonString, forKey: "OtherLanguageDataUnicode")
        
        self.ProductDetails.updateValue("true", forKey: "Published")
        self.ProductDetails.updateValue(ProductUOM ?? "" , forKey: "UOM")
         
        
        return true
    }
    
    
}

