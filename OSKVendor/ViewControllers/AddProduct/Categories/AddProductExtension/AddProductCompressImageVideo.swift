//
//  AddProductCompressImageVideo.swift
//  OSKVendor
//
//  Created by Tushar Lama on 30/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Photos
import AVKit
import DKImagePickerController


extension AddProductNewViewController{
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            return
        }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
}




