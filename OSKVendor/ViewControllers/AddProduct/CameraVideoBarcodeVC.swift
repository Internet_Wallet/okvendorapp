//
//  CameraVideoBarcodeVC.swift
//  OSKVendor
//
//  Created by Tushar Lama on 29/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

protocol CameraVideoBarcodeDelegate {
    //0 for camera
    //1 for video
    //2 for barcode
    func cameraVideoBarcodeSelected(index: Int)
}

class CameraVideoBarcodeVC: UIViewController {

    //Take a Photo
    //Take a Video
    //Scan Barcode
    
    @IBOutlet var barcodeLabel: UILabel!{
        didSet{
            self.barcodeLabel.text = "Scan Barcode".localized
        }
    }
    @IBOutlet var barcodeView: CardView!
    @IBOutlet var videoLabel: UILabel!{
        didSet{
            self.videoLabel.text = "Take a Video".localized
        }
    }
    @IBOutlet var videoView: CardView!
    @IBOutlet var cameraLabel: UILabel!{
        didSet{
            self.cameraLabel.text = "Take a Photo".localized
        }
    }
    @IBOutlet var cameraView: CardView!
    var delegate: CameraVideoBarcodeDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapOnView = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tapOnView)
        
        let tapOnCamera = UITapGestureRecognizer(target: self, action: #selector(self.handleTapOnCamera(_:)))
        let tapOnVideo = UITapGestureRecognizer(target: self, action: #selector(self.handleTapOnVideo(_:)))
        let tapOnBarcode = UITapGestureRecognizer(target: self, action: #selector(self.handleTapOnBarcode(_:)))
        cameraView.addGestureRecognizer(tapOnCamera)
        videoView.addGestureRecognizer(tapOnVideo)
        barcodeView.addGestureRecognizer(tapOnBarcode)
        
    }
    

    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func handleTapOnCamera(_ sender: UITapGestureRecognizer? = nil) {
        self.dismiss(animated: true, completion: {
            self.delegate?.cameraVideoBarcodeSelected(index: 0)
        })
        
    }
    
    @objc func handleTapOnVideo(_ sender: UITapGestureRecognizer? = nil) {
        self.dismiss(animated: true, completion: {
            self.delegate?.cameraVideoBarcodeSelected(index: 1)
        })
    }
    
    @objc func handleTapOnBarcode(_ sender: UITapGestureRecognizer? = nil) {
        self.dismiss(animated: true, completion: {
            self.delegate?.cameraVideoBarcodeSelected(index: 2)
        })
    }
    
}
