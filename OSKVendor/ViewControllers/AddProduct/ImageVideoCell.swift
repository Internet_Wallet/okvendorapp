//
//  ImageVideoCell.swift
//  Covid Zay Vendor
//
//  Created by OK$ on 26/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import DKImagePickerController

protocol imageVideoDelete {
    func deleteImageContentAtNew(index : Int)
    func palyVideoNew(index : Int)
}

protocol addProductVideoDelgate {
   
    func onClickCameraAction( sender : UIButton)
    func onClickDeliveryTimeAction( sender : UIButton)
    func didEditChanged( sender : UITextField)
    
}

class ImageVideoCell: UITableViewCell {

    @IBOutlet var collViewPage : UICollectionView!
    var delegate : addProductVideoDelgate?
    var delegateX: imageVideoDelete?
   
    var finalData = [Data]()
 
    @IBOutlet var imagePageController: UIPageControl!

    func wrapDataValue(ImageDataValue : [Data]) {
        finalData.removeAll()
        finalData = ImageDataValue
        imagePageController.currentPage  = 0
        imagePageController.numberOfPages = finalData.count
        imagePageController.pageIndicatorTintColor = UIColor.green
     
        collViewPage.reloadData()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //ImageVideoCell
         
        self.collViewPage.register(UINib(nibName: "ImageVideoCollCell", bundle: nil), forCellWithReuseIdentifier: "ImageVideoCollCell")
        collViewPage.delegate = self
        collViewPage.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ImageVideoCell : UICollectionViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return finalData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageVideoCollCell", for: indexPath) as! ImageVideoCollCell
        
        cell.imgViewPic.image =  UIImage(data: finalData[indexPath.row] as Data)
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(deleteImage(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func deleteImage ( sender : UIButton ) {
         print("Delete Image No : \(sender.tag) call")
         delegateX?.deleteImageContentAtNew(index: sender.tag)
    }
    
    @objc func playVideo( sender : UIButton ) {
         print("Play Video at Index No : \(sender.tag) call")
         delegateX?.palyVideoNew(index: sender.tag)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        imagePageController.currentPage = Int(indexPath.row)
    }
    
//    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if collectionView == collectionView {
//            if imagePageController.currentPage == indexPath.row {
//                guard let visible = collectionView.visibleCells.first else { return }
//                guard let index = collectionView.indexPath(for: visible)?.row else { return }
//                imagePageController.currentPage = index
//            }
//
//        }
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: UIScreen.main.bounds.width, height: 210)
        
    }
}
