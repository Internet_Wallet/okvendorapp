//
//  AddProductExtension.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 16/09/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import Foundation
import DKCamera
import DKImagePickerController
import Photos


extension AddProductsViewController: UITextFieldDelegate , UITextViewDelegate {
    
        func returnBarCode(barCade: String) {
            
                var cell:AddProductsFieldsCell?
                if imageAndVideoArray.count > 0 {
                    cell = getCell(row: 10)
                }
                else {
                    cell = getCell(row: 9)
                }
                cell?.tfValue.text = barCade
                self.insertElementAtIndex(element: barCade, index: 9)
                manageTextFieldTitle(cell: cell!)
        }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField.tag == 40 {
            DispatchQueue.main.async {
                self.view.endEditing(true)
            }
            self.categoryInputSubCatArray.removeAll()
            self.SUbCatList = self.categoryDetails?.listCategory
            self.categoryInputSubCatArray.append(self.categoryDetails?.listCategory ?? [])
            var isViewAvailable = false
            for (_ , element) in self.view.subviews.enumerated() {
                if element.isKind(of: UIView.self) {
                    if element.tag == 1001 {
                        isViewAvailable = true
                    }
                }
            }
            if isViewAvailable {
                self.CategoryView.removeFromSuperview()
            }
            else {
                self.upDateTheUI()
                self.categoryTableView.reloadData()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text: String? = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        println_debug("typing string :: \(String(describing: text))")
        
        if textField.tag == 100 {
            if text?.count ?? 0 == 16 {
                return false
            } else {
                return true
            }
        }
      if textField.tag == 30 {
          if text?.count ?? 0 >= 10  {
              return false
          } else {
              return true
          }
      }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        switch textField.tag
        {
        case 10:
                self.insertElementAtIndex(element: textField.text ?? "null", index: 0)
                self.ProductDetails.updateValue(textField.text ?? "null", forKey: "Name")
        case 20:
                self.insertElementAtIndex(element: textField.text ?? "null", index: 1)
                self.otherLanguageData.updateValue(textField.text ?? "null", forKey: "Name")
        case 30:
                self.insertElementAtIndex(element: textField.text ?? "null", index: 2)
                self.ProductDetails.updateValue(textField.text ?? "null", forKey: "Price")
        case 40:
                self.insertElementAtIndex(element: textField.text ?? "null", index: 3)
                self.ProductDetails.updateValue("\(self.categoryID)",forKey: "CategoryId")
        case 100:
                self.insertElementAtIndex(element: textField.text ?? "0000", index: 9)
                self.ProductDetails.updateValue(textField.text ?? "0000", forKey: "Barcode")
        default:
                print("It is nothing")
        }

    }
    
         func manageTextFieldTitle(cell: AddProductsFieldsCell) {
             if cell.tfValue.text?.count == 0 {
                 cell.lblTitle.isHidden = true
             }else {
                 cell.lblTitle.isHidden = false
             }
         }
    
        func insertElementAtIndex(element: String, index: Int) {
            self.inputData[index] = element
        }
    
    // MARK: UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        
        // Calculate if the text view will change height, then only force
        // the table to update if it does.  Also disable animations to
        // prevent "jankiness".

        let startHeight = textView.frame.size.height
        let calcHeight = textView.sizeThatFits(textView.frame.size).height  //iOS 8+ only

        if startHeight != calcHeight {

            UIView.setAnimationsEnabled(false) // Disable animations
            self.AddProductTblView.beginUpdates()
            self.AddProductTblView.endUpdates()

            // Might need to insert additional stuff here if scrolls
            // table in an unexpected way.  This scrolls to the bottom
            // of the table. (Though you might need something more
            // complicated if editing in the middle.)

            let scrollTo = self.AddProductTblView.contentSize.height - self.AddProductTblView.frame.size.height
            self.AddProductTblView.setContentOffset(CGPoint(x: 0, y: scrollTo), animated: false)

            UIView.setAnimationsEnabled(true)  // Re-enable animations.
       }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        let index = ((textView.tag) / 10) - 1
        if imageAndVideoArray.count > 0 {
            let cell = self.AddProductTblView.cellForRow(at: IndexPath(row: (index + 1), section: 0)) as? DescriptionCell
            cell?.lblPlaceholderText.isHidden = true
        }
        else {
            let cell = self.AddProductTblView.cellForRow(at: IndexPath(row: index, section: 0)) as? DescriptionCell
            cell?.lblPlaceholderText.isHidden = true
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        let index = ((textView.tag) / 10) - 1
        let cell = self.AddProductTblView.cellForRow(at: IndexPath(row: index, section: 0)) as? DescriptionCell
        if cell?.textView.text.count == 0 {
            cell?.lblPlaceholderText.isHidden = false
        }
        switch textView.tag
        {
        case 60:
            self.insertElementAtIndex(element: textView.text ?? "null", index: 5)
            self.ProductDetails.updateValue(textView.text ?? "null", forKey: "ShortDescription");
        case 70:
            self.insertElementAtIndex(element: textView.text ?? "null", index: 6)
            self.otherLanguageData.updateValue(textView.text ?? "null", forKey: "ShortDescription");
        case 80:
            self.insertElementAtIndex(element: textView.text ?? "null", index: 7)
            self.ProductDetails.updateValue(textView.text ?? "null", forKey: "FullDescription");
        case 90:
            self.insertElementAtIndex(element: textView.text ?? "null", index: 8)
            self.otherLanguageData.updateValue(textView.text ?? "null", forKey: "FullDescription");
        default:
            print("It is nothing");
        }
    }

    @objc func keyboardWillShow(notification: NSNotification) {
     
         var extraHeight = 0
         if UIDevice().hasNotch {
            extraHeight = Int(self.saveButtonView.frame.height)
         }
         AddProductTblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: (315 + CGFloat(extraHeight)), right: 0)
         let keyBoardHeight = self.getKeyBoardHeight(notificationKeyBoard: notification)
            if self.saveButtonView.frame.origin.y >= (self.view.frame.size.height - self.saveButtonView.frame.height) {
                self.saveButtonView.frame.origin.y -= (keyBoardHeight + CGFloat(extraHeight))
                self.frameOrigionY = (keyBoardHeight + CGFloat(extraHeight))
                print(keyBoardHeight)
                println_debug("frame origion on keyboard opne : \(self.saveButtonView.frame.origin.y)")
         }
        else {
                self.saveButtonView.frame.origin.y += self.frameOrigionY + CGFloat(extraHeight)
                self.frameOrigionY = keyBoardHeight + CGFloat(extraHeight)
                self.saveButtonView.frame.origin.y -= keyBoardHeight + CGFloat(extraHeight)
                println_debug(self.saveButtonView.frame.origin.y)
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        
        AddProductTblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        if self.saveButtonView.frame.origin.y != (self.view.frame.size.height - self.saveButtonView.frame.height) {
            self.saveButtonView.frame.origin.y += self.frameOrigionY
                println_debug("frame origion on keyboard close : \(self.saveButtonView.frame.origin.y)")
        }
    }
    
    func getKeyBoardHeight(notificationKeyBoard: NSNotification) -> CGFloat {
        
        let kbFrame = (notificationKeyBoard.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        let screenSize = UIScreen.main.bounds
        let intersectRect = kbFrame?.intersection(screenSize) //Calculating actual keyboard displayed size, keyboard frame may be different when hardware keyboard is attached (Bug ID: #469) (Bug ID: #381)

        var kbSize: CGSize
        if intersectRect?.isNull ?? false {
            kbSize = CGSize(width: screenSize.size.width, height: 0)
        } else {
            kbSize = intersectRect?.size as? CGSize ?? CGSize.zero
        }
        
        var rootViewController = UIApplication.shared.keyWindow?.rootViewController
        while (rootViewController != nil) {
            if rootViewController is UINavigationController {
                let navigationController = rootViewController as? UINavigationController
                let nextRootViewController = navigationController?.viewControllers.last
                if let nextRootViewController = nextRootViewController {
                    rootViewController = nextRootViewController
                    continue
                } else {
                    break
                }
            }
            if rootViewController is UITabBarController {
                let tabBarController = rootViewController as? UITabBarController
                let nextRootViewController = tabBarController?.selectedViewController
                if let nextRootViewController = nextRootViewController {
                    rootViewController = nextRootViewController
                    continue
                }
                else {
                    break
                }
            }
            if rootViewController?.presentedViewController == nil {
                break
            }
            rootViewController = rootViewController?.presentedViewController
        }

        let navigationBarAreaHeight = UIApplication.shared.statusBarFrame.size.height + (rootViewController?.navigationController?.navigationBar.frame.size.height ?? 0.0)
        let layoutAreaHeight = rootViewController?.view.layoutMargins.top ?? 0
        let topLayoutGuide = CGFloat(max(navigationBarAreaHeight, layoutAreaHeight))
        let bottomLayoutGuide = rootViewController?.view.layoutMargins.bottom ?? 0
        let magicNumber: CGFloat = 5 //Apple Constant
        
        return kbSize.height - topLayoutGuide - bottomLayoutGuide + magicNumber
    }
    
 }




extension AddProductsViewController : UIPopoverPresentationControllerDelegate,PopoverViewControllerDelegate , UIGestureRecognizerDelegate {
    
    public func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
           return .none
       }
    
    func selectedDeliveryTime(id: String,name: String) {
        
        println_debug("ID : \(id) name : \(name)")
        deliveryDateId.0 = id
        deliveryDateId.1 = name
        if imageAndVideoArray.count > 0 {
            AddProductTblView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .automatic)
        }
        else {
            AddProductTblView.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
        }
        
        
    }
    
   func onClickDeliveryTimeAction(sender: UIButton){
        self.deliveryTimeWebCall(handler: { (isSuccess) in
            if isSuccess {
                let popoverVC = PopoverViewController()
                popoverVC.modalPresentationStyle = .popover
                popoverVC.accessibilityFrame.origin.x = self.view.frame.size.width / 2 + 90
                popoverVC.popoverPresentationController?.sourceView = sender
                popoverVC.popoverPresentationController?.permittedArrowDirections = .left
                popoverVC.popoverPresentationController?.delegate = self
                popoverVC.delegate = self
                popoverVC.model = self.deliverytimeModel
                self.present(popoverVC, animated: true, completion: nil)
            }
        })
    }
    
    func deliveryTimeWebCall(handler: @escaping (_ success: Bool) -> Void) {
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/vendor/GetDeliveryDateList", APIManagerClient.sharedInstance.base_url) as String
            print("Get Deliverytime :\(urlStr) ")
            self.aPIManager.getGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        println_debug("Data error")
                        return
                    }
                    do {
                        let decode = JSONDecoder()
                        let model = try? decode.decode(DeliveryTimeModel.self, from: jsonData)
                        println_debug(String(data: jsonData, encoding: .utf8))
                        if model?.statusCode == 200 {
                            self?.deliverytimeModel = model
                            handler(true)
                        }else {
                            handler(false)
                            println_debug("Product Attribute Value Not Found")
                        }
                    }
                }
            }, onError: {  message in
                if let viewLoc = self.view {
                    AppUtility.hideLoading(viewLoc)
                    self.alertVC = SAlertController()
                    self.alertVC?.ShowSAlert(title: "Error", withDescription: "Found error in get delivery time. Please try again", onController: self)
                    let YesAction = SAlertAction()
                    YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                       handler(false)
                    })
                    self.alertVC?.addAction(action: [YesAction])
                }
            })
        }
        
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == self.gestureTapView;
    }
}
