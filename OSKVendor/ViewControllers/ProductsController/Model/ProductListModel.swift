//
//  ProductListModel.swift
//  OSKVendor
//
//  Created by OK$ on 26/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

struct ReviewOverviewModel: Codable {
    var allowCustomerReviews : String?
    var ratingSum : String?
    var productId : String?
    var totalReviews : String?
    var ratingUrlFromGSMArena : String?

    enum CodingKeys: String, CodingKey {
        case allowCustomerReviews = "AllowCustomerReviews"
        case ratingSum = "RatingSum"
        case productId = "ProductId"
        case totalReviews = "TotalReviews"
        case ratingUrlFromGSMArena = "RatingUrlFromGSMArena"
    }
}

struct DefaultPictureModel: Codable {
    
    var fullSizeImageUrl : String?
    var customProperties : String?
    var form : String?
    var Id : String?
    var imageUrl : String?
    var alternateText : String?
    var title : String?

    enum CodingKeys: String, CodingKey {
        case fullSizeImageUrl = "FullSizeImageUrl"
        case customProperties = "CustomProperties"
        case form = "Form"
        case Id = "Id"
        case imageUrl = "ImageUrl"
        case alternateText = "AlternateText"
        case title = "Title"

    }
    
}

struct ProductListModel: Codable {
    
    var defaultPictureModel: [DefaultPictureModel] = []
    var vendorMobileNumber: String?
    var categoryIconPath: String?
    var reviewOverviewModel: [ReviewOverviewModel] = []
    var shortDescription: String?
    var form: String?
    var productPrice: [ProductPrice] = []
    var customProperties: [CustomProperties] = []
    var ID: String?
    var markAsNew: String?
    var isWishList: String?
    var published: String?
    var disableWishlistButton: String?
    var name: String?
    var categoryName: String?
    var categoryId: String?
    var displayOrder: String?
    var createdOn: String?
    var sku: String?
    var verify: String?
    
    enum CodingKeys: String, CodingKey {
        case defaultPictureModel = "DefaultPictureModel"
        case vendorMobileNumber = "VendorMobileNumber"
        case categoryIconPath = "CategoryIconPath"
        case reviewOverviewModel = "ReviewOverviewModel"
        case shortDescription = "ShortDescription"
        case form = "Form"
        case productPrice = "ProductPrice"
        case customProperties = "CustomProperties"
        case ID = "Id"
        case markAsNew = "MarkAsNew"
        case isWishList = "IsWishList"
        case published = "Published"
        case disableWishlistButton = "DisableWishlistButton"
        case name = "Name"
        case categoryName = "CategoryName"
        case categoryId = "CategoryId"
        case displayOrder = "DisplayOrder"
        case createdOn = "CreatedOn"
        case sku = "Sku"
        case verify = "Verify"
    }
}

