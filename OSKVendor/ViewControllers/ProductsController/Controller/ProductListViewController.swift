//
//  ProductListViewController.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 07/07/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit


struct ProductListSection {
    var sectionID: String
    var sectionName: String
    var sectionURL: String
    var items: [ProductsInfo]
    var collapsed: Bool
    
    init(sectionID:String, sectionName: String, sectionURL: String, items: [ProductsInfo], collapsed: Bool = false) {
        self.sectionID = sectionID
        self.sectionName = sectionName
        self.sectionURL = sectionURL
        self.items = items
        self.collapsed = collapsed
    }
}

class ProductListViewController: OSKBaseViewController  {
    
    @IBOutlet weak var lblHeaderTitle: UILabel!{
              didSet {
                  self.lblHeaderTitle.font = UIFont(name: appFont, size: 15.0)
                  self.lblHeaderTitle.text = "My Products".localized
              }
          }
    let aPIManager = APIManager()
    var apiManagerClient : APIManagerClient?
    var pageNo : Int = 1
    var ProductInfoArray : ProductDetailsInfo? = ProductDetailsInfo()
    var isLoading = false
    var isSectionTapped = false
    var sections = [ProductListSection]()
    
    @IBOutlet weak var productTableView : UITableView!
    @IBOutlet weak var noRecordFoundlbl: UILabel!{
        didSet {
            self.noRecordFoundlbl.font = UIFont(name: appFont, size: 15.0)
            self.noRecordFoundlbl.text = self.noRecordFoundlbl.text?.localized
        }
    }
    @IBOutlet weak var imgAdd : UIImageView!
    
    @IBOutlet weak var noRecordView: UIView! {
        didSet {
            self.noRecordView.isHidden = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "My Products".localized

        self.loadInitialData()
        
        let cell = UINib(nibName: "MyProductCategoryCell", bundle: nil)
        self.productTableView.register(cell, forCellReuseIdentifier: "MyProductCategoryCell")
        
        let cellSubCat = UINib(nibName: "MyProductSubCategoryCell", bundle: nil)
        self.productTableView.register(cellSubCat, forCellReuseIdentifier: "MyProductSubCategoryCell")
        
        self.productTableView.delegate = self
        self.productTableView.dataSource = self
        self.productTableView.reloadData()
        
        //        DispatchQueue.main.async {
        //            if #available(iOS 13.0, *) {
        //                if let viewLoc = self.view {
        //                    AppUtility.hideLoading(viewLoc)
        //                }
        //                let sb = UIStoryboard(name: "EditProduct", bundle: nil)
        //                let vc = sb.instantiateViewController(identifier: "PhotoVideoPicker") as! PhotoVideoPicker
        //                vc.optionImage = false
        //                self.navigationController?.pushViewController(vc, animated: true)
        //            }
        //        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        pageNo = 1
    }
    
    func loadInitialData(){
        self.apiManagerClient = APIManagerClient.sharedInstance
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            
            let page = "\(pageNo)"
            self.aPIManager.getAllProductsInformation(pageNo: page , onSuccess: { [weak self] getHomeProducts in
                
                self?.ProductInfoArray?.productsInfo.removeAll()
                
                self?.ProductInfoArray = getHomeProducts
                
                println_debug(self?.ProductInfoArray?.productsInfo)
                var arrCategory : [String] = []
                
                //Add in section wise Array
                for i in 0..<(self?.ProductInfoArray?.productsInfo.count)! {
                    let dictInfo = self?.ProductInfoArray?.productsInfo[i]
                    //print(dictInfo)
                    let catID = "\(dictInfo?.CategoryId ?? 0)"
                    if arrCategory.contains(catID) {
                        println_debug("Already Exist")
                        for s in 0..<(self?.sections.count)! {
                            let val = self?.sections[s].sectionID
                            if catID == val {
                                self?.sections[s].items.append(dictInfo!)
                                break
                            }
                        }
                    } else {
                        println_debug("New Category found")
                        arrCategory.append(catID)
                        
                        self?.sections.append(ProductListSection(sectionID: catID, sectionName: dictInfo?.CategoryName ?? "",sectionURL:dictInfo?.CategoryIconPath  ?? "", items: [dictInfo!], collapsed: true))
                    }
                }
                
                DispatchQueue.main.async {
                    if (self?.ProductInfoArray?.productsInfo.count ?? 0) > 0{
                        
                        self?.noRecordView.isHidden = true
                        self?.productTableView.delegate = self
                        self?.productTableView.dataSource = self
                        self?.productTableView.reloadData()
                        self?.productTableView.isHidden = false
                        
                    }else {
                        self?.noRecordView.isHidden = false
                        self?.productTableView.isHidden = true
                    }
                    
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                            DispatchQueue.main.async {
                                AppUtility.hideLoading(viewLoc)
                                self?.noRecordView.isHidden = false
                                self?.productTableView.isHidden = true
                            }
                    }
            })
        }
    }
    
    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
       }
    
    @objc func MoveForwardAction (sender : UIButton) {
        
    }
    
    @objc func deleteTheProduct(sender : Any)
      {
        let productID = (sender as? UIButton)?.tag
        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            aPIManager.deleteProduct(productID: productID ?? 0) {
                DispatchQueue.main.async {
                    if let viewLoc = self.view {
                        AppUtility.hideLoading(viewLoc)
                        AppUtility.showToastlocal(message: "Product deleted successfully".localized, view: viewLoc)
                    }
                    self.ProductInfoArray?.productsInfo.removeAll()
                    self.sections.removeAll()
                    self.pageNo = 1
                    self.loadInitialData()
                }
            } onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
            }
        }
        else {
            
        }
     }
  }

extension ProductListViewController : UIScrollViewDelegate {
    func loadMoreData() {
        if !self.isLoading {
            self.isLoading = true
            if self.pageNo <= (self.ProductInfoArray?.TotalPages ?? 1) {
                self.pageNo += 1
                self.apiManagerClient = APIManagerClient.sharedInstance
                if AppUtility.isConnectedToNetwork() {
                    AppUtility.showLoading(self.view)
                    let page = "\(pageNo)"
                    self.aPIManager.getAllProductsInformation(pageNo: page , onSuccess: { [weak self] getHomeProducts in
                        
                        self?.ProductInfoArray?.productsInfo.append(contentsOf: getHomeProducts.productsInfo)
            
                        
                        self?.productTableView.reloadData()
                        
                        if let viewLoc = self?.view {
                            AppUtility.hideLoading(viewLoc)
                        }
                        DispatchQueue.main.async {
                            self?.isLoading = false
                        }
                    }, onError: { [weak self] message in
                        if let viewLoc = self?.view {
                            AppUtility.hideLoading(viewLoc)
                        }
                    })
                }
            }
            else {
                 self.isLoading = false
            }
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee.y = max(targetContentOffset.pointee.y - 1, 1)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
      let location = scrollView.panGestureRecognizer.location(in: productTableView)
      guard let indexPath = productTableView.indexPathForRow(at: location) else {
            print("could not specify an indexpath")
            return
        }
        
        /*
        if indexPath.row != 0{

            if (indexPath.row == transactionRecords.count - 2){
                //calling the API here
                //  let firstIndex  =
                if transactionRecords.count >= 20 {
                    if filterArray.count > 0 {
                        print("Not fatching records from the server")
                    }
                    else {
                        addMore = transactionRecords.count + 20
                                           if appDelegate.checkNetworkAvail(){
                                               //PTLoader.shared.show()
                                               modelReport.getALLTransactionInfo(first: "\(addMore)", last: "\(20)")
                        }
                    }
                }
            }
        }
         */
        
        println_debug("will begin dragging at row \(indexPath.row)")
    }
}

extension ProductListViewController : UITableViewDataSource, UITableViewDelegate {
    
    //MARK:- DataSourceMethods
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 65
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return sections[(indexPath as NSIndexPath).section].collapsed ? 0 : 50
        //return 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.sections.count == 0{
            noRecordView.isHidden = false
            productTableView.isHidden = true
        }else{
            noRecordView.isHidden = true
            productTableView.isHidden = false
            tableView.backgroundView = nil
        }
        return sections.count
    }
    
    @objc func toggleCollapse(sender: UIButton) {
      let section = sender.tag
//      let collapsed = sections[section].collapsed
//
//      // Toggle collapse
//      sections[section].collapsed = !collapsed
//      productTableView.reloadData()
        
      // Reload section
      //productTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
        let collapsed = !sections[section].collapsed
        // Toggle collapse
        self.sections[section].collapsed = collapsed
        // collapsed the opened section
        for i in 0..<self.sections.count {
          if i == section {
            continue
          }
          self.sections[i].collapsed = true
        }
        self.productTableView.reloadData()
        
    }
    
    // Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cellView = self.productTableView.dequeueReusableCell(withIdentifier: "MyProductCategoryCell") as! MyProductCategoryCell
        cellView.toggleButton.tag = section
        cellView.toggleButton.addTarget(self, action: #selector(toggleCollapse), for: .touchUpInside)
        cellView.ArrowBtn.addTarget(self, action: #selector(MoveForwardAction(sender:)), for: .touchUpInside)
        cellView.categoryNameLbl.text = sections[section].sectionName
        cellView.categoryCountLbl.text = "\(sections[section].items.count)"
        let productImageURL = sections[section].sectionURL
        if !productImageURL.isEmpty {
            cellView.categoryImageView.sd_setImage(with: URL(string: productImageURL))
        }
        else {
            cellView.categoryImageView.image = UIImage(named: "no-image")
        }
        
        if sections[section].collapsed {
            cellView.ArrowBtn.rotate(CGFloat(0.0))
        } else {
            cellView.ArrowBtn.rotate(CGFloat(Double.pi))
        }

        
        return cellView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sections[section].collapsed ? 0 : sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.productTableView.dequeueReusableCell(withIdentifier: "MyProductSubCategoryCell") as! MyProductSubCategoryCell
        cell.productNameLbl.text = sections[indexPath.section].items[indexPath.row].Name as String
        cell.productAmountLbl.text = sections[indexPath.section].items[indexPath.row].Price
        cell.deleteProduct.tag = sections[indexPath.section].items[indexPath.row].ProductId
        cell.deleteProduct.addTarget(self, action: #selector(deleteTheProduct(sender:)), for: .touchUpInside)
        return cell
    }
    
    //MARK:- DelegateMethods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            let sb = UIStoryboard(name: "EditProduct", bundle: nil)
            if #available(iOS 13.0, *) {
                let editVC = sb.instantiateViewController(identifier: "EditProductsNewVC") as! EditProductsNewVC
                editVC.productID = "\(self.sections[indexPath.section].items[indexPath.row].ProductId)"//String(object.id)
                editVC.modalPresentationStyle = .overCurrentContext
                self.navigationController?.pushViewController(editVC, animated: true)
            }
            
//            if #available(iOS 13.0, *) {
//                let obj = self.storyboard?.instantiateViewController(identifier: CategoryConstant.kAddProductNewViewController) as? AddProductNewViewController
//                if let isValid = obj{
//                    isValid.ProductID = self.sections[indexPath.section].items[indexPath.row].ProductId
//
//                    self.navigationController?.pushViewController(isValid, animated: true)
//                }
//            } else {
//                // Fallback on earlier versions
//                if let obj = self.storyboard?.instantiateViewController(withIdentifier: CategoryConstant.kAddProductNewViewController) as? AddProductNewViewController{
//                    obj.ProductID = self.sections[indexPath.section].items[indexPath.row].ProductId
//                        self.navigationController?.pushViewController(obj, animated: true)
//                    }
//                }
            }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == ((self.ProductInfoArray?.productsInfo.count ?? 1) - 50)  && !self.isLoading {
            loadMoreData()
        }
    }
}



extension UIView {
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        
        self.layer.add(animation, forKey: nil)
    }
}
