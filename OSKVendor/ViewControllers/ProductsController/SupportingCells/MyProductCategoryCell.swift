//
//  MyProductCategoryCell.swift
//  OSKVendor
//
//  Created by OK$ on 26/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class MyProductCategoryCell: UITableViewCell {

    @IBOutlet var toggleButton: UIButton!
    @IBOutlet var ArrowBtn : UIButton!
    @IBOutlet var categoryImageView: UIImageView!
    @IBOutlet var categoryNameLbl: UILabel!{
          didSet {
              self.categoryNameLbl.font = UIFont(name: appFont, size: 15.0)
          }
      }
   
    @IBOutlet var categoryCountLbl: UILabel!{
        didSet {
            self.categoryCountLbl.font = UIFont(name: appFont, size: 15.0)
            self.categoryCountLbl.layer.cornerRadius = 17.5
            self.categoryCountLbl.layer.masksToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCellWithData(productObject: ProductsInfo) {
        
        if let productImageURL = productObject.CategoryIconPath , !productImageURL.isEmpty {
            categoryImageView.sd_setImage(with: URL(string: productImageURL))
        }
        else {
            categoryImageView.image = UIImage(named: "no-image")
        }
        
        categoryNameLbl.text = productObject.CategoryName
    }
}
