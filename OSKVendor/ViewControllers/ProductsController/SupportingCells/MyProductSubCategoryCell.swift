//
//  MyProductSubCategoryCell.swift
//  OSKVendor
//
//  Created by OK$ on 26/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class MyProductSubCategoryCell: UITableViewCell {

    @IBOutlet var productNameLbl: UILabel!{
        didSet {
            self.productNameLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var productAmountLbl: UILabel!{
        didSet {
            self.productAmountLbl.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var deleteProduct: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCellWithData(productObject: ProductsInfo) {
        productNameLbl.text = "Shorts"
        productAmountLbl.text = "10,000 MMK"
    }
}
