//
//  ListProductTableViewCell.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 07/07/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class ListProductTableViewCell: UITableViewCell {
    
       @IBOutlet var mainBackView: UIView!
       @IBOutlet var productImageView: UIImageView!
       
       @IBOutlet var productNameLbl: UILabel!{
           didSet {
               self.productNameLbl.font = UIFont(name: appFont, size: 15.0)
               self.productNameLbl.text = self.productNameLbl.text?.localized
               self.productNameLbl.textColor = UIColor.red
           }
       }
       @IBOutlet var amountLbl: UILabel!{
           didSet {
               self.amountLbl.font = UIFont(name: appFont, size: 15.0)
               self.amountLbl.text = self.amountLbl.text?.localized
               self.amountLbl.textColor = UIColor.black
               
           }
       }
    @IBOutlet var ArrowBtn : UIButton!
    @IBOutlet var lblStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainBackView.shadowToColleectionView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCellWithData(productObject: ProductsInfo) {
        
        productImageView?.layer.cornerRadius = (self.productImageView?.frame.width ?? 0) / 2
        productImageView?.layer.masksToBounds = true
        productImageView.layer.borderWidth = 1.0
        productImageView.layer.borderColor = UIColor.darkGray.cgColor
        
        if let productImageURL = productObject.ImageUrl as? String , !productImageURL.isEmpty {
            productImageView.sd_setImage(with: URL(string: productImageURL))
        }
        else {
            productImageView.image = UIImage(named: "no-image")
        }
        
        productNameLbl.text = productObject.Name as String
        
        let (Price,_,_) = AppUtility.getPriceOldPriceDiscountPrice(price: productObject.Price ?? "", oldPrice: productObject.OldPrice ?? "", priceWithDiscount: productObject.PriceWithDiscount)
        
        if let productPrice = productObject.Price?.replacingOccurrences(of: "MMK", with: ""){
            
            let attrString = NSMutableAttributedString(string: productPrice + mmkText)
            let nsRange = NSString(string: productPrice + mmkText).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
            //Font change
            attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: UIFont(name: appFont, size: 9) ?? UIFont.systemFont(ofSize: 9), range: nsRange)
            amountLbl.attributedText = attrString
            
        }
        
        if Price == "" {
            amountLbl.isHidden = true
            if productObject.PriceWithDiscount == "" {
                
                if let productPrice2 = productObject.Price?.replacingOccurrences(of: "MMK", with: ""){
                    let attrStr = NSMutableAttributedString(string: productPrice2 + mmkText)
                    let nsRange = NSString(string: productPrice2 + mmkText).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                    attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont ?? UIFont.systemFont(ofSize: 9), range: nsRange)
                }
            } else {
                var productPrice2 = productObject.PriceWithDiscount.replacingOccurrences(of: "MMK", with: "")
                productPrice2 = productPrice2 + mmkText
                let attrStr = NSMutableAttributedString(string: productPrice2)
                let nsRange = NSString(string: productPrice2).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
                attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont ?? UIFont.systemFont(ofSize: 9), range: nsRange)
            }
        }
        else {
            amountLbl.isHidden = false
        }
        
        if productObject.Published {
            lblStatus.text = "Status: " + "Show"
            lblStatus.textColor = UIColor.colorWithRedValue(redValue: 14, greenValue: 159, blueValue: 68, alpha: 1)
        }else {
            lblStatus.text = "Status: " + "Hide"
            lblStatus.textColor = UIColor.red
        }
    }
    
}
