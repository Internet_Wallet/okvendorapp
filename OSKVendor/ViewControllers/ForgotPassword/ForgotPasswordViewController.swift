//
//  ForgotPasswordViewController.swift
//  OSKVendor
//
//  Created by vamsi on 6/27/20.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import LTMorphingLabel
import SwiftyJSON

class ForgotPasswordViewController: OSKBaseViewController,PhValidationProtocol {
    @IBOutlet var navigationView: UIView!
    @IBOutlet var titleLabel: UILabel!{
        didSet{
            titleLabel.text = "Forgot Password".localized
        }
    }
    @IBOutlet var backButton: UIButton!
    
    @IBOutlet var headingLabel: LTMorphingLabel!{
        didSet{
            headingLabel.text = "Enter your mobile number to get a new password.".localized
        }
    }
    
    @IBOutlet var mobileNumTF: SkyFloatingLabelTextField!{
        didSet {
        self.mobileNumTF.placeholder = self.mobileNumTF.placeholder?.localized
       }
    }
    
    @IBOutlet var forgotButton: UIButton!{
        didSet{
            self.forgotButton.layer.cornerRadius = 4.0
            forgotButton.setTitle((forgotButton.titleLabel?.text ?? ""), for: .normal)
            
            forgotButton.setTitle("Forgot Password".localized, for: .normal)
        }
    }
    
    var apiManager = APIManager()
       
       let validObj = PayToValidations.init()
       var country       : Country?
       public var currentSelectedCountry : countrySelected = .myanmarCountry
          enum countrySelected {
              case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
          }
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

    }
    
    private func callForgotApi(addressDict: Dictionary<String, String>?) {
            
            AppUtility.showLoading(self.view)
            apiManager.ForgotUser(addressDict! as [String : String], onSuccess: { [weak self] response in
                
                DispatchQueue.main.async{
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    let json = JSON(response)
                    println_debug("ForgotUser Response: \(json)")
                    if let code = json["StatusCode"].int, code == 200 {
                        if let loginInfoDic  = json.dictionaryObject {
                            //let vmTemoModel = VMTempModel()
                           // vmTemoModel.wrapDataModel(JSON: loginInfoDic)
                            print(loginInfoDic)
                            //AppUtility.alert(message: json["SuccessMessage"].string ?? "", title: "OSK Vendor", controller: self!)
                            let alert = UIAlertController(title: "OSK Vendor".localized, message: json["SuccessMessage"].string ?? "", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK".localized, style: .default) { action -> Void in
                           
                                self?.navigationController?.popViewController(animated: true)
                            })
                            
                            self?.present(alert, animated: true, completion: nil)
                        }
                    }else {
                        let message = json["ErrorList"][0].string
                        let alert = UIAlertController(title: "OSK Vendor".localized, message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: nil))
                        self?.present(alert, animated: true, completion: nil)
                    }
                }
                
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        DispatchQueue.main.async {
                            AppUtility.hideLoading(viewLoc)
                        }
                    }
            })
            
        }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func forgotClick(_ sender: Any) {
        if mobileNumTF.text == "09" || mobileNumTF.text == "" {
            AppUtility.alert(message: "Please Enter Mobile Number".localized, title: "", controller: self)
        }
        else if mobileNumTF.text?.count ?? 0 > 2 && mobileNumTF.text?.count ?? 0 < 9  {
            AppUtility.alert(message: "Please Enter Valid Mobile Number".localized, title: "", controller: self)
        }
        else{
            if AppUtility.isConnectedToNetwork(){
                var paramDic  = [String: Any]()
                let mobileNo = mobileNumTF.text ?? ""
                paramDic["Username"] = mobileNo
                self.callForgotApi(addressDict: (paramDic as! Dictionary<String, String>))
            }
            else{
                AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
            }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
