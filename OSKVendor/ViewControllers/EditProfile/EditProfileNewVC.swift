//
//  EditProfileNewVC.swift
//  OSKVendor
//
//  Created by OK$ on 23/10/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON

class EditProfileNewVC: OSKBaseViewController {
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var titleLabel: UILabel!{
        didSet{
            titleLabel.text = "Profile".localized
        }
    }
    @IBOutlet var backButton: UIButton!
    @IBOutlet var updateButton: UIButton!{
        didSet{
            updateButton.setTitle("Update".localized, for: .normal)
            updateButton.layer.cornerRadius = 5.0
        }
    }
    
    @IBOutlet var editProfileScroll: UIScrollView!
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var shopnameTF: SkyFloatingLabelTextField!{
        didSet {
            self.shopnameTF.font = UIFont(name: appFont, size: 17.0)
            self.shopnameTF.text = self.shopnameTF.text?.localized
            self.shopnameTF.placeholder = self.shopnameTF.placeholder?.localized
        }
    }
    
    @IBOutlet var mobilenumTF: SkyFloatingLabelTextField!{
        didSet {
            self.mobilenumTF.font = UIFont(name: appFont, size: 17.0)
            self.mobilenumTF.text = self.mobilenumTF.text?.localized
            self.mobilenumTF.placeholder = self.mobilenumTF.placeholder?.localized
        }
    }
    
    @IBOutlet var addressLine1TF: SkyFloatingLabelTextField!{
        didSet {
            self.addressLine1TF.font = UIFont(name: appFont, size: 17.0)
            self.addressLine1TF.text = self.addressLine1TF.text?.localized
            self.addressLine1TF.placeholder = self.addressLine1TF.placeholder?.localized
        }
    }
    
    @IBOutlet var facebookidTF: SkyFloatingLabelTextField!{
        didSet{
            self.facebookidTF.font = UIFont(name: appFont, size: 17.0)
            self.facebookidTF.text = self.facebookidTF.text?.localized
            self.facebookidTF.placeholder = self.facebookidTF.placeholder?.localized
        }
    }
    
    @IBOutlet var nameView: UIView!
    @IBOutlet var mobileView: UIView!
    @IBOutlet var addressView: UIView!
    @IBOutlet var fbView: UIView!
    
    var StatesArray = [[String : Any]]()
    var TownshipArray = [[String : Any]]()
    var stateId = String()
    var cityId = Int()//String()
    var countriesAndStatesRespArr = [Any]()
    var stateDict = [String:Any]()
    var townshipDict = [String : Any]()
    
    var apiManager = APIManager()
    var googleAddress : String?
    var locationLat : String?
    var locationLong : String?
    
    var alertVC : SAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile".localized
        
        Vendor_loginmodel.wrapModel()
        
        // Do any additional setup after loading the view.
        self.UpdateUI()
        self.location()
        apiManager.getDevisionInfoForCountry(countryID: "60", onSuccess: { (devisionDictionary) in
            if let devisionData = devisionDictionary["Data"] as? [[String:Any]] , devisionData.count > 0 {
                self.StatesArray = devisionData
                if let devisionD = self.StatesArray[0] as? [String : Any]  {
                    let devisionID = devisionD["id"] as? Int
                    self.stateId = "\(devisionID!)"
                    //  self.stateTF.text = devisionD["name"] as? String
                    self.apiManager.getTownshipInfoForDevision(devisionID: "\(devisionID!)", onSuccess: { (townshipInfo) in
                        if let townshipData = townshipInfo["Data"] as? [[String:Any]] , townshipData.count > 0 {
                            self.TownshipArray = townshipData
                            for cityTemp in self.TownshipArray {
                                if Vendor_loginmodel.shared.city == "\(cityTemp["id"] as? Int64 ?? 0)" {
                                    self.cityId = Int((cityTemp["id"] as? Int64 ?? 0))
                                    //  self.townshipTF.text = cityTemp["name"] as? String
                                    break
                                }
                            }
                        }
                    }) { (errorX) in
                        DispatchQueue.main.async {
                            AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
                        }
                    }
                }
            }
        }) { (error) in
            DispatchQueue.main.async {
                AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
            }
        }
        
    }
    
    
    func UpdateUI() {
        
        
        let mobilestr = Vendor_loginmodel.shared.mobileNumber
        let outputString = String(mobilestr?.dropFirst(4) ?? "")
        let finalmobilestring = "0" + outputString
        
        shopnameTF.text = Vendor_loginmodel.shared.shopName
        mobilenumTF.text = finalmobilestring//Vendor_loginmodel.shared.mobileNumber
        addressLine1TF.text  = Vendor_loginmodel.shared.address1
        facebookidTF.text = Vendor_loginmodel.shared.facebookId
        
        nameView.layer.cornerRadius = 10.0
        nameView.layer.borderWidth = 1.0
        nameView.layer.borderColor = UIColor.lightGray.cgColor
        //   nameView.layer.masksToBounds = false
        nameView.layer.shadowColor = UIColor.lightGray.cgColor
        nameView.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        nameView.layer.shadowOpacity = 0.5
        nameView.layer.shadowRadius = 4.0
        
        mobileView.layer.cornerRadius = 10.0
        mobileView.layer.borderWidth = 1.0
        mobileView.layer.borderColor = UIColor.lightGray.cgColor
        //  mobileView.layer.masksToBounds = false
        mobileView.layer.shadowColor = UIColor.lightGray.cgColor
        mobileView.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        mobileView.layer.shadowOpacity = 0.5
        mobileView.layer.shadowRadius = 4.0
        
        addressView.layer.cornerRadius = 10.0
        addressView.layer.borderWidth = 1.0
        addressView.layer.borderColor = UIColor.lightGray.cgColor
        //   addressView.layer.masksToBounds = false
        addressView.layer.shadowColor = UIColor.lightGray.cgColor
        addressView.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        addressView.layer.shadowOpacity = 0.5
        addressView.layer.shadowRadius = 4.0
        
        fbView.layer.cornerRadius = 10.0
        fbView.layer.borderWidth = 1.0
        fbView.layer.borderColor = UIColor.lightGray.cgColor
        // fbView.layer.masksToBounds = false
        fbView.layer.shadowColor = UIColor.lightGray.cgColor
        fbView.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        fbView.layer.shadowOpacity = 0.5
        fbView.layer.shadowRadius = 4.0
        
        
        //Marker
        let buttonMap = UIButton()
        buttonMap.setImage(UIImage(named: "Marker"), for: .normal)
        buttonMap.titleLabel?.textColor = UIColor.white
        buttonMap.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        buttonMap.addTarget(self, action: #selector(self.mapButtonClick(_:)), for: .touchUpInside)
        
        let paddingViewMap: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        paddingViewMap.addSubview(buttonMap)
        
        addressLine1TF.rightView = paddingViewMap
        addressLine1TF.rightViewMode = .always
        
    }
    
    func location(){
        let isPermission =  VMGeoLocationManager.shared.setUpLocationManager()
        if isPermission {
            VMGeoLocationManager.shared.startUpdateLocation()
            VMGeoLocationManager.shared.getAddressFrom(lattitude:VMGeoLocationManager.shared.currentLatitude, longitude: VMGeoLocationManager.shared.currentLongitude, language: "en")
            { (status, data) in
                if let addressDic = data as? Dictionary<String, String> {
                    DispatchQueue.main.async {
                        print(addressDic)
                        if let street = addressDic["street"]  {
                            // self.township = street
                        }
                        if let region = addressDic["region"]  {
                            // self.devision = region
                        }
                        self.googleAddress = "\(addressDic)"
                        
                        self.locationLat = VMGeoLocationManager.shared.currentLatitude
                        self.locationLong = VMGeoLocationManager.shared.currentLongitude
                    }
                }
            }
        }
        else{
            showLocationDisabledpopUp()
        }
    }
    
    func showLocationDisabledpopUp() {
        DispatchQueue.main.async {
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "Location Services are disabled", withDescription: "Please enable Location Services in your Settings", onController: self)
            
            let cancelAction = SAlertAction()
            cancelAction.action(name: "Cancel", AlertType: .defualt, withComplition: {
                //self.navigationController?.dismiss(animated: true , completion: nil)
            })
            let openAction = SAlertAction()
            openAction.action(name: "Open Setting", AlertType: .defualt, withComplition: {
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            })
            
            alertVC.addAction(action: [cancelAction,openAction])
        }
    }
    
    @IBAction func mapButtonClick(_ sender: Any) {
        
        DispatchQueue.main.async {
            if #available(iOS 13.0, *) {
                let sb = UIStoryboard(name: "Login", bundle: nil)
                if let baseObj = sb.instantiateViewController(identifier: "NewGeofenceController") as? NewGeofenceController {
                    baseObj.modalPresentationStyle = .fullScreen
                    baseObj.addressDelegate = self
                    self.present(baseObj, animated: true, completion: nil)
                    //self.navigationController?.pushViewController(baseObj, animated: true)
                }
            } else {
                let  homeVC = UIStoryboard(name: "Login", bundle: nil)
                if let dashbord = homeVC.instantiateViewController(withIdentifier: "NewGeofenceController") as? NewGeofenceController{
                    dashbord.modalPresentationStyle = .fullScreen
                    dashbord.addressDelegate = self
                    self.present(dashbord, animated: true, completion: nil)
                }
            }
            
        }
    }
    
    
    @IBAction func backClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updateProfileAction(_ sender: Any) { 
        if shopnameTF.text == ""{
            AppUtility.alert(message: "Please Enter Shop Name".localized, title: "", controller: self)
        }
        else if addressLine1TF.text == ""{
            AppUtility.alert(message: "Please Enter AddressLine1".localized, title: "", controller: self)
        }
        else{
            if AppUtility.isConnectedToNetwork(){
                //                var mobileNo = mobilenumTF.text ?? ""
                //                if mobileNo.hasPrefix("09"){
                //                    mobileNo = String(mobileNo.dropFirst())
                //                }
                
                let profileRequestData:Dictionary<String,Any> = [ "Firstname":Vendor_loginmodel.shared.firstName ?? "",
                                                                  "Lastname":"",
                                                                  "email": Vendor_loginmodel.shared.email ?? "",
                                                                  "DateOfBirthDay": Vendor_loginmodel.shared.dateOfBirthDay ?? "01",
                                                                  "DateofBirthMonth": Vendor_loginmodel.shared.dateofBirthMonth ?? "01",
                                                                  "DateOfBirthYear": Vendor_loginmodel.shared.dateOfBirthYear ?? "1990",
                                                                  "Address1":addressLine1TF.text ?? "",
                                                                  "address2":"",
                                                                  "cityid":self.cityId,
                                                                  "stateid":self.stateId,
                                                                  "country": "myanmar",
                                                                  "otheremail": Vendor_loginmodel.shared.otherEmail ?? "",
                                                                  //"MobileNumber":mobilenumTF.text ?? "",
                                                                  "HouseNo":Vendor_loginmodel.shared.houseNo ?? "",
                                                                  "FloorNo":Vendor_loginmodel.shared.floorNo ?? "",
                                                                  "RoomNo":Vendor_loginmodel.shared.roomNo ?? "",
                                                                  "ShopName":shopnameTF.text ?? "",
                                                                  "FacebookId":facebookidTF.text ?? "",
                                                                  "Latitude": self.locationLat ?? "0.0",
                                                                  "Longitude": self.locationLong ?? "0.0",
                                                                  "MarketId":0,
                                                                  "ShopLocationType":0,
                                                                  "IsWholeSaleMarket":false,
                                                                  "RegPreferenceCategoryId": 6370,
                                                                  "ViberNumber": "",
                                                                  "WhatsappNumber": ""
                ]
                self.callUpdateProfileApi(addressDict: (profileRequestData))
            }
            else{
                AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
            }
        }
    }
    
    private func callUpdateProfileApi(addressDict: Dictionary<String, Any>?) {
        AppUtility.showLoading(self.view)
        apiManager.UpdateProfile(addressDict! as [String : AnyObject], onSuccess: { [weak self] response in
            
            DispatchQueue.main.async{
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
                let json = JSON(response)
                println_debug("UpdateProfile Response: \(json)")
                if let code = json["StatusCode"].int, code == 200 {
                    if let updateProfileDic  = json["Data"].dictionaryObject {
                        let vmTemoModel = VMTempModel()
                        vmTemoModel.wrapDataModel(JSON: updateProfileDic)
                        print(updateProfileDic)
                        let alert = UIAlertController(title: "OSK Vendor".localized, message: "Profile Updated Successfully".localized, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default)
                        { action -> Void in
                            self?.navigationController?.popViewController(animated: true)
                        })
                        self?.present(alert, animated: true, completion: nil)
                        
                    }
                }else {
                    let message = json["ErrorList"][0].string
                    let alert = UIAlertController(title: "OSK Vendor".localized, message: message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                }
            }
        }, onError: { [weak self] message in
            if let viewLoc = self?.view {
                DispatchQueue.main.async {
                    AppUtility.hideLoading(viewLoc)
                }
            }
        })
        
    }
    
}

extension EditProfileNewVC :  AddressDelegate {
   
    func setAddressWithLatLong( adrress : String, lat : String, long : String) {
        self.addressLine1TF.text = ""
        if let adress = adrress as? String {
            self.addressLine1TF.text = adress
        }
        self.locationLat = lat
        self.locationLong = long
    }
    
}
