//
//  EditProfileExtensionVC.swift
//  OSKVendor
//
//  Created by OK$ on 21/09/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

extension EditProfileVC: UITextFieldDelegate{
    
     func textFieldDidBeginEditing(_ textField: UITextField)
        {
            if textField == stateTF {
                if StatesArray.count > 0 {
                if let stateData = StatesArray[0] as? [String : Any] {
                    stateTF.text = stateData["name"] as? String
                    stateTF.inputView = statePicker
                    stateTF.tintColor = UIColor.clear
                }
            }
            }
            if textField == townshipTF
            {
                if TownshipArray.count > 0 {
                    if let townshipData = TownshipArray[0] as? [String : Any] {
                        townshipTF.text = townshipData["name"] as? String
                        townshipTF.inputView = TownshipPicker
                        townshipTF.tintColor = UIColor.clear
                    }
                }
            }
        }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == firstNameTF{
            if let text = textField.text{
                textField.text = text
            }
        }
      
       else if textField == mobilenumTF{
            if  currentSelectedCountry == .myanmarCountry  {
                       
                       if let text = textField.text, text.count < 3 {
                           textField.text = "09"
                         
                       }
                   }
        }
        else if textField == emailTF{
            if let text = textField.text{
                    textField.text = text
            }
        }
        else if textField == addressLine1TF{
            if let text = textField.text{
                    textField.text = text
            }
        }
        else if textField == stateTF{
                if let text = textField.text{
                    textField.text = text
            }
        }
        else if textField == townshipTF{
            if let text = textField.text{
                    textField.text = text
            }
        }
       else if textField == shopnameTF{
                if let text = textField.text{
                        textField.text = text
          }
       }
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if self.country?.dialCode == "+95" {
            textField.text = "09"
        } else {
            textField.text = ""
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return false
        }
        
        if textField == emailTF {
            if string == " " {
                return false
            }
        }
        
        return true
        
    }
}

extension EditProfileVC: UIPickerViewDelegate,UIPickerViewDataSource {
    // MARK: - PickerView  Delegate Methods
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            var rows: Int = 0
            if pickerView == statePicker {
                rows = StatesArray.count
            }
            if pickerView == TownshipPicker {
                rows = TownshipArray.count
            }
            return rows
        }
        
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            var returnString = String()
            if pickerView == statePicker
            {
                returnString = StatesArray[row] as! String
            }
            if pickerView == TownshipPicker
            {
                stateDict = StatesArray[row] as! [String:AnyObject]
                returnString = stateDict["name"] as! String
                stateId = stateDict["id"] as! String
                returnString = TownshipArray[row] as! String
            }

            return returnString
        }
        
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            if pickerView == statePicker {
                stateDict = StatesArray[row] as [String:AnyObject]
                 stateTF.text = stateDict["name"] as? String
                 stateId = String(stateDict["id"] as! Int)
            }
            if pickerView == TownshipPicker
            {
                townshipDict = TownshipArray[row] as [String:AnyObject]
                townshipTF.text = townshipDict["name"] as? String
                cityId = String(townshipDict["id"] as! Int)
            }
        
        }
        
        func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
            var pickerLabel: UILabel? = (view as? UILabel)
            if pickerLabel == nil {
                pickerLabel = UILabel()
                pickerLabel?.font = UIFont(name: "Helvetica Neue", size: 15)
                pickerLabel?.textAlignment = .center
            }
            if pickerView == statePicker {
                stateDict = StatesArray[row] as [String:AnyObject]
                pickerLabel?.text = stateDict["name"] as? String
            }
            if pickerView == TownshipPicker {
                townshipDict = TownshipArray[row] as [String:AnyObject]
                pickerLabel?.text = townshipDict["name"] as? String
            }
            return pickerLabel!
        }
    
    func keyboardWasShown(_ notification: Notification) {
        
       }
       
       func keyboardWillBeHidden(_ notification: Notification)
       {
//           for i in 0..<self.countriesAndStatesRespArr.count
//           {
//               let sampleDict = self.countriesAndStatesRespArr[i] as! [String:Any]
//               if countryTF.text == sampleDict["CName"] as? String
//               {
//                   countryId = sampleDict["CId"] as! String
//                   StatesArray = sampleDict["StateList"] as! [Any]
//                   break
//               }
//           }

           statePicker.removeFromSuperview()
           TownshipPicker.removeFromSuperview()
       }
    
}
