//
//  EditProfileVC.swift
//  OSKVendor
//
//  Created by OK$ on 21/09/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON

class EditProfileVC: OSKBaseViewController {
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var titleLabel: UILabel!{
        didSet{
            titleLabel.text = "Profile".localized
        }
    }
    @IBOutlet var backButton: UIButton!
    @IBOutlet var updateButton: UIButton!{
        didSet{
          updateButton.setTitle("Update".localized, for: .normal)
        }
    }
    
    @IBOutlet var editProfileScroll: UIScrollView!
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var firstNameTF: SkyFloatingLabelTextField!{
        didSet {
            self.firstNameTF.font = UIFont(name: appFont, size: 15.0)
            self.firstNameTF.text = self.firstNameTF.text?.localized
             self.firstNameTF.placeholder = self.firstNameTF.placeholder?.localized
        }
    }
    @IBOutlet var shopnameTF: SkyFloatingLabelTextField!{
        didSet {
            self.shopnameTF.font = UIFont(name: appFont, size: 15.0)
            self.shopnameTF.text = self.shopnameTF.text?.localized
            self.shopnameTF.placeholder = self.shopnameTF.placeholder?.localized
        }
    }
    
    @IBOutlet var mobilenumTF: SkyFloatingLabelTextField!{
        didSet {
            self.mobilenumTF.font = UIFont(name: appFont, size: 15.0)
            self.mobilenumTF.text = self.mobilenumTF.text?.localized
             self.mobilenumTF.placeholder = self.mobilenumTF.placeholder?.localized
        }
    }
    
    @IBOutlet var emailTF: SkyFloatingLabelTextField!{
        didSet {
            self.emailTF.font = UIFont(name: appFont, size: 15.0)
            self.emailTF.text = self.emailTF.text?.localized
            self.emailTF.placeholder = self.emailTF.placeholder?.localized
        }
    }
    @IBOutlet var addressLine1TF: SkyFloatingLabelTextField!{
        didSet {
            self.addressLine1TF.font = UIFont(name: appFont, size: 15.0)
            self.addressLine1TF.text = self.addressLine1TF.text?.localized
            self.addressLine1TF.placeholder = self.addressLine1TF.placeholder?.localized
        }
    }
    @IBOutlet var addressLine2TF: SkyFloatingLabelTextField!{
        didSet {
            self.addressLine2TF.font = UIFont(name: appFont, size: 15.0)
            self.addressLine2TF.text = self.addressLine2TF.text?.localized
             self.addressLine2TF.placeholder = self.addressLine2TF.placeholder?.localized
        }
    }
    @IBOutlet var stateTF: SkyFloatingLabelTextField!{
        didSet {
            self.stateTF.font = UIFont(name: appFont, size: 15.0)
            self.stateTF.text = self.stateTF.text?.localized
            self.stateTF.placeholder = self.stateTF.placeholder?.localized
        }
    }
    
    @IBOutlet var townshipTF: SkyFloatingLabelTextField!{
        didSet {
            self.townshipTF.font = UIFont(name: appFont, size: 15.0)
            self.townshipTF.text = self.townshipTF.text?.localized
             self.townshipTF.placeholder = self.townshipTF.placeholder?.localized
        }
    }
    
    var apiManager = APIManager()
    var country       : Country?
    public var currentSelectedCountry : countrySelected = .myanmarCountry
    enum countrySelected {
        case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
    }
    
    var StatesArray = [[String : Any]]()
    var TownshipArray = [[String : Any]]()
    var stateId = String()
    var cityId = String()
    var countriesAndStatesRespArr = [Any]()
    var stateDict = [String:Any]()
    var townshipDict = [String : Any]()
    
    var statePicker: UIPickerView!
    var TownshipPicker: UIPickerView!
    
    var township : String?
    var devision : String?
    var googleAddress : String?
    var locationLat : String?
    var locationLong : String?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile".localized
        Vendor_loginmodel.wrapModel()

        self.UpdateUI()
        // Do any additional setup after loading the view.
        self.location()
        //if stateTF.text == "" || townshipTF.text == ""{
            apiManager.getDevisionInfoForCountry(countryID: "60", onSuccess: { (devisionDictionary) in
                if let devisionData = devisionDictionary["Data"] as? [[String:Any]] , devisionData.count > 0 {
                    self.StatesArray = devisionData
                    if let devisionD = self.StatesArray[0] as? [String : Any]  {
                        let devisionID = devisionD["id"] as? Int
                        self.stateId = "\(devisionID!)"
                        self.stateTF.text = devisionD["name"] as? String
                        self.apiManager.getTownshipInfoForDevision(devisionID: "\(devisionID!)", onSuccess: { (townshipInfo) in
                            if let townshipData = townshipInfo["Data"] as? [[String:Any]] , townshipData.count > 0 {
                                self.TownshipArray = townshipData
                                for cityTemp in self.TownshipArray {
                                    if Vendor_loginmodel.shared.city == "\(cityTemp["id"] as? Int64 ?? 0)" {
                                        self.cityId = "\(cityTemp["id"] as? Int64 ?? 0)"
                                        self.townshipTF.text = cityTemp["name"] as? String
                                        break
                                    }
                                }
                            }
                        }) { (errorX) in
                            DispatchQueue.main.async {
                                AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
                            }
                        }
                    }
                }
            }) { (error) in
                DispatchQueue.main.async {
                    AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
                }
            }
        //}
    }
    
    func UpdateUI() {
        firstNameTF.text = "\(Vendor_loginmodel.shared.firstName ?? "")"
        shopnameTF.text = Vendor_loginmodel.shared.shopName
        mobilenumTF.text = Vendor_loginmodel.shared.mobileNumber
        emailTF.text  = Vendor_loginmodel.shared.email
        addressLine1TF.text  = Vendor_loginmodel.shared.address1
        addressLine2TF.text  = Vendor_loginmodel.shared.address2 ?? ""
//        stateTF.text = Vendor_loginmodel.shared.state
//        townshipTF.text = Vendor_loginmodel.shared.city
        
        statePicker = UIPickerView()
        statePicker.delegate = self
        statePicker.dataSource = self
        
        TownshipPicker = UIPickerView()
        TownshipPicker.delegate = self
        TownshipPicker.dataSource = self
        
        //Marker
        let buttonMap = UIButton()
        buttonMap.setImage(UIImage(named: "Marker"), for: .normal)
        buttonMap.titleLabel?.textColor = UIColor.white
        buttonMap.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        buttonMap.addTarget(self, action: #selector(self.mapButtonClick(_:)), for: .touchUpInside)
    
        let paddingViewMap: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        paddingViewMap.addSubview(buttonMap)
        
        let imageView = UIImageView(image: UIImage(named: "downarrow"))
        imageView.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
        let paddingView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        paddingView.addSubview(imageView)
        
        let imageView1 = UIImageView(image: UIImage(named: "downarrow"))
        imageView1.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
        let paddingView1: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        paddingView1.addSubview(imageView1)
        
        addressLine1TF.rightView = paddingViewMap
        addressLine1TF.rightViewMode = .always
        
        stateTF.rightView = paddingView
        stateTF.rightViewMode = .always
        
        townshipTF.rightView = paddingView1
        townshipTF.rightViewMode = .always
    }
    
    func location(){
        let isPermission =  VMGeoLocationManager.shared.setUpLocationManager()
        if isPermission {
            VMGeoLocationManager.shared.startUpdateLocation()
            VMGeoLocationManager.shared.getAddressFrom(lattitude:VMGeoLocationManager.shared.currentLatitude, longitude: VMGeoLocationManager.shared.currentLongitude, language: "en")
            { (status, data) in
                if let addressDic = data as? Dictionary<String, String> {
                    DispatchQueue.main.async {
                        print(addressDic)
                        if let street = addressDic["street"]  {
                            self.township = street
                        }
                        if let region = addressDic["region"]  {
                            self.devision = region
                        }
                        self.googleAddress = "\(addressDic)"
                        
                        self.locationLat = VMGeoLocationManager.shared.currentLatitude
                        self.locationLong = VMGeoLocationManager.shared.currentLongitude
                    }
                }
            }
        }
        else{
            showLocationDisabledpopUp()
        }
    }
    
    func showLocationDisabledpopUp() {
        DispatchQueue.main.async {
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "Location Services are disabled", withDescription: "Please enable Location Services in your Settings", onController: self)
            
            let cancelAction = SAlertAction()
            cancelAction.action(name: "Cancel", AlertType: .defualt, withComplition: {
                self.navigationController?.dismiss(animated: true , completion: nil)
            })
            let openAction = SAlertAction()
            openAction.action(name: "Open Setting", AlertType: .defualt, withComplition: {
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            })
            
            alertVC.addAction(action: [cancelAction,openAction])
        }
    }
    
    @IBAction func backClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updateProfileAction(_ sender: Any) {
        let validate = Validation()
        
        if firstNameTF.text == ""{
            AppUtility.alert(message: "Please Enter Name".localized, title: "", controller: self)
        }
//        else if !validate.isNameString(firstNameTF.text){
//            AppUtility.alert(message: "Enter Valid First Name", title: "", controller: self)
//        }
        else if shopnameTF.text == ""{
            AppUtility.alert(message: "Please Enter Shop Name".localized, title: "", controller: self)
        }
        else if emailTF.text == ""{
            AppUtility.alert(message: "Please Enter Email".localized, title: "", controller: self)
        }
        else if !validate.isEmailString(emailTF.text){
            AppUtility.alert(message: "Enter Valid Email".localized, title: "", controller: self)
        }
        else if addressLine1TF.text == ""{
            AppUtility.alert(message: "Please Enter AddressLine1".localized, title: "", controller: self)
        }
        else if stateTF.text == ""{
            AppUtility.alert(message: "Please Select State/Division".localized, title: "", controller: self)
        }
        else if townshipTF.text == ""{
            AppUtility.alert(message: "Please Select Township".localized, title: "", controller: self)
        }
        else{
            if AppUtility.isConnectedToNetwork(){
//                var mobileNo = mobilenumTF.text ?? ""
//                if mobileNo.hasPrefix("09"){
//                    mobileNo = String(mobileNo.dropFirst())
//                }
               
                let profileRequestData:Dictionary<String,Any> = [ "Firstname":firstNameTF.text ??  "",
                                                                  "Lastname":"Vendor",
                                                                  "email":emailTF.text ?? "",
                                                                  "DateOfBirthDay": Vendor_loginmodel.shared.dateOfBirthDay ?? "01",
                                                                  "DateofBirthMonth": Vendor_loginmodel.shared.dateofBirthMonth ?? "01",
                                                                  "DateOfBirthYear": Vendor_loginmodel.shared.dateOfBirthYear ?? "1990",
                                                                  "Address1":addressLine1TF.text ?? "",
                                                                  "Address2":addressLine2TF.text ?? "",
                                                                  "CityId":self.cityId,
                                                                  "StateId":self.stateId,
                                                                  "country": "Myanmar",
                                                                  "otheremail":emailTF.text ?? "",
                                                                  "MobileNumber":mobilenumTF.text ?? "",
                                                                  "HouseNo":Vendor_loginmodel.shared.houseNo ?? "",
                                                                  "FloorNo":Vendor_loginmodel.shared.floorNo ?? "",
                                                                  "RoomNo":Vendor_loginmodel.shared.roomNo ?? "",
                                                                  "ShopName":shopnameTF.text ?? "",
                                                                  "Latitude": self.locationLat ?? "0.0",
                                                                  "Longitude": self.locationLong ?? "0.0"
                ]
                self.callUpdateProfileApi(addressDict: (profileRequestData))
            }
            else{
                AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
            }
        }
    }
    
    private func callUpdateProfileApi(addressDict: Dictionary<String, Any>?) {
        AppUtility.showLoading(self.view)
        apiManager.UpdateProfile(addressDict! as [String : AnyObject], onSuccess: { [weak self] response in
            
            DispatchQueue.main.async{
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
                let json = JSON(response)
                println_debug("UpdateProfile Response: \(json)")
                if let code = json["StatusCode"].int, code == 200 {
                    if let updateProfileDic  = json["Data"].dictionaryObject {
                        let vmTemoModel = VMTempModel()
                        vmTemoModel.wrapDataModel(JSON: updateProfileDic)
                        print(updateProfileDic)
                        self?.navigationController?.popViewController(animated: true)
                    }
                }else {
                    let message = json["ErrorList"][0].string
                    let alert = UIAlertController(title: "OSK Vendor".localized, message: message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                }
            }
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    DispatchQueue.main.async {
                        AppUtility.hideLoading(viewLoc)
                    }
                }
        })
        
    }
    
    @IBAction func mapButtonClick(_ sender: Any) {
        
        DispatchQueue.main.async {
            if #available(iOS 13.0, *) {
                let sb = UIStoryboard(name: "Login", bundle: nil)
                if let baseObj = sb.instantiateViewController(identifier: "NewGeofenceController") as? NewGeofenceController {
                    baseObj.modalPresentationStyle = .fullScreen
                    baseObj.addressDelegate = self
                    self.navigationController?.pushViewController(baseObj, animated: true)
                }
            } else {
                let  homeVC = UIStoryboard(name: "Login", bundle: nil)
                let dashbord = homeVC.instantiateViewController(withIdentifier: "NewGeofenceController") as? NewGeofenceController
                dashbord?.modalPresentationStyle = .fullScreen
                dashbord?.addressDelegate = self
                self.navigationController?.pushViewController(dashbord!, animated: true)
            }
            
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension EditProfileVC :  AddressDelegate {
   
    func setAddressWithLatLong( adrress : String, lat : String, long : String) {
        self.addressLine1TF.text = ""
        if let adress = adrress as? String {
            self.addressLine1TF.text = adress
        }
        self.locationLat = lat
        self.locationLong = long
    }
    
}
