//
//  InitialViewController.swift
//  OSKVendor
//
//  Created by vamsi on 6/27/20.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {
    
    let segueIdentifier = "firstScreen"
    let loginScreen = "LoginScreen"
    
    var aPIManager = APIManager()

    override func viewDidLoad() {
        super.viewDidLoad()
            APIManagerClient.sharedInstance.urlEnable(urlTo: APIManagerClient.sharedInstance.serverUrl)
            self.chekLoginStatus()
            NotificationCenter.default.addObserver(self , selector: #selector(InitialViewController.setupUserState), name: NSNotification.Name(rawValue: "FCMToken") , object: nil)
            if let lang = UserDefaults.standard.value(forKey: "currentLanguage") as? String, lang == "en" {
                      changeAppLanguageApiCall(index: 1)
             }
              else {
                      changeAppLanguageApiCall(index: 3)
                  }
        // Do any additional setup after loading the view.
    }
    
    
    @objc func setupUserState() {
        if let vendorData = Vendor_loginmodel.shared  as? Vendor_loginmodel{
            if let email = vendorData.email , email != "" , let fcmToken = appDelegate.FCMToken as? String , fcmToken != nil {
                let aPIManager = APIManager()
                if AppUtility.isConnectedToNetwork(){

                aPIManager.registerDeviceId(appDelegate.FCMToken, EmailID: email, onSuccess: {
                }, onError: { message in
                    DispatchQueue.main.async {
                        AppUtility.alert(message: message ?? "Check your Network Connectivity".localized, title: "", controller: self)
                    }
                })
                }
                else{
                    AppUtility.alert(message: "Check your Network Connectivity".localized, title: "", controller: self)
                }
            }
        }
    }
    
    func chekLoginStatus() {
        
        let status = UserDefaults.standard.value(forKey: AppConstants.userDefault.loginStatus) as? Bool
        if status == true {
            DispatchQueue.main.async {
                [unowned self] in
                Vendor_loginmodel.wrapModel()
                self.performSegue(withIdentifier: self.segueIdentifier, sender: self)
            }
        }
        else {
            DispatchQueue.main.async {
                [unowned self] in
                    self.performSegue(withIdentifier: self.loginScreen, sender: self)
            }
        }
    }
    
    func changeAppLanguageApiCall(index: Int) {
        let type = index//(index == 0) ? 1 : 2
        let urlString = String(format: "%@/SetLanguage/%d", APIManagerClient.sharedInstance.base_url, type)
        guard let url = URL(string: urlString) else { return }
        let params = Dictionary<String, Any>()
        //AppUtility.showLoading(self.view)
        print(aPIManager.getHeaderDic())
        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, _) in
            DispatchQueue.main.async {
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                } }
            if success {
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["StatusCode"] as? Int, code == 200 {
                        if type == 1 {
                            UserDefaults.standard.set("en", forKey: "currentLanguage")
                            appLang = "English"
                            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
                            UserDefaults.standard.synchronize()
                            
                        } else{
                            UserDefaults.standard.set("uni", forKey: "currentLanguage")
                            appLang = "Unicode"
                            UserDefaults.standard.set("Myanmar3", forKey: "appFont")
                            UserDefaults.standard.synchronize()
                        }
                    }
                }
            }
        }
    }
}


