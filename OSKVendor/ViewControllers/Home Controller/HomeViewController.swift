//
//  HomeViewController.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 29/06/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit


protocol KYDrawerCustomDelegate: class {
    
    func resetAllData()
}

class HomeViewController: OSKBaseViewController {

    @IBOutlet var menuBarButton: UIBarButtonItem!
    var apiManager = APIManager()

    @IBOutlet var addProductView : UIView!
    @IBOutlet var editProductVew : UIView!
    @IBOutlet var myOrderView : UIView!
    @IBOutlet var myProductView : UIView!
    
    @IBOutlet var btnAddProduct: UIButton!{
        didSet{
          btnAddProduct.setTitle("ADD PRODUCT TO SELL".localized, for: .normal)
        }
    }
    @IBOutlet var btnEditProduct: UIButton!{
        didSet{
            btnEditProduct.setTitle("CHANGE/ EDIT PRODUCTS".localized, for: .normal)
        }
    }
    @IBOutlet var btnProductList: UIButton!{
        didSet{
            btnProductList.setTitle("MY PRODUCTS LIST".localized, for: .normal)
        }
    }
    @IBOutlet var btnMyOrders: UIButton!{
        didSet{
          btnMyOrders.setTitle("MY ORDERS".localized, for: .normal)
        }
    }

    
     var alertVC : SAlertController?
     var isLogoutTrue : Bool = false
     weak var KYDCDelegate: KYDrawerCustomDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Vendor_loginmodel.wrapModel()
        
        self.title = "OSK Vendor".localized
        getFAQ()
        addProductView.layer.cornerRadius = 10
        addProductView.layer.masksToBounds = true
        addProductView.shadowToNormalView()
        
        editProductVew.layer.cornerRadius = 10
        editProductVew.layer.masksToBounds = true
        editProductVew.shadowToNormalView()
        
        myOrderView.layer.cornerRadius = 10
        myOrderView.layer.masksToBounds = true
        myOrderView.shadowToNormalView()
       
        myProductView.layer.cornerRadius = 10
        myProductView.layer.masksToBounds = true
        myProductView.shadowToNormalView()
        
        btnAddProduct.titleLabel?.numberOfLines = 0
        btnAddProduct.titleLabel?.lineBreakMode = .byCharWrapping
        
        btnEditProduct.titleLabel?.numberOfLines = 0
        btnEditProduct.titleLabel?.lineBreakMode = .byCharWrapping
        
        btnProductList.titleLabel?.numberOfLines = 0
        btnProductList.titleLabel?.lineBreakMode = .byCharWrapping
        
        btnMyOrders.titleLabel?.numberOfLines = 0
        btnMyOrders.titleLabel?.lineBreakMode = .byCharWrapping
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateGUI), name: NSNotification.Name("AppLanguageChange"), object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.alertVC = SAlertController()
        self.updateGUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(tapOnCategory(_:)), name: NSNotification.Name("TapOnCategory"), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(animated)
           if UserDefaults.standard.bool(forKey: "isLogin") {
               if self.isLogoutTrue  {
                   self.logout()
               }
           }
       }
    
    deinit {
           NotificationCenter.default.removeObserver(self, name: NSNotification.Name("TapOnCategory"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("TapOnCategory"), object: nil)
    }
    
    @objc func updateGUI() {
        appDelegate.setInitialCurrentLang()
        DispatchQueue.main.async{
        self.title = "OSK Vendor".localized
        self.btnAddProduct.setTitle("ADD PRODUCT TO SELL".localized, for: .normal)
        self.btnEditProduct.setTitle("CHANGE/ EDIT PRODUCTS".localized, for: .normal)
        self.btnProductList.setTitle("MY PRODUCTS LIST".localized, for: .normal)
        self.btnMyOrders.setTitle("MY ORDERS".localized, for: .normal)
        }
    }
    
    @objc func tapOnCategory(_ notification: NSNotification) {
        let controller = notification.userInfo?["controller"] as? String
        if (controller == "ProductListViewController") {
 
            let payto = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProductListViewController")
                     payto.modalPresentationStyle = .fullScreen
                    DispatchQueue.main.async {
                       self.navigationController?.pushViewController(payto, animated: true)
                  }
             }
        if (controller == "MyOrdersVC") {
            let payto = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyOrdersVC")
                     payto.modalPresentationStyle = .fullScreen
                       DispatchQueue.main.async {
                          self.navigationController?.pushViewController(payto, animated: true)
                      }
             }
        if (controller == "EditProfileVC") {
            let payto = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditProfileNewVC")
                     payto.modalPresentationStyle = .fullScreen
                     DispatchQueue.main.async {
                       self.navigationController?.pushViewController(payto, animated: true)
                   }
             }
        if (controller == "HelpSupportVC") {
            let payto = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VMHelpAndSupport")
                     payto.modalPresentationStyle = .fullScreen
                     DispatchQueue.main.async {
                       self.navigationController?.pushViewController(payto, animated: true)
                   }
             }
    }
    
    @IBAction func showInfoAction( _ sender: UIButton) {
        let tag = sender.tag
        var msgDisplay = ""
        switch tag {
        case 0:
            println_debug("0")
            msgDisplay = "ADD PRODUCT TO SELL"
        case 1:
            println_debug("1")
            msgDisplay = "CHANGE/ EDIT PRODUCTS"
        case 2:
            println_debug("2")
            msgDisplay = "MY PRODUCTS LIST"
        case 3:
            println_debug("3")
            msgDisplay = "MY ORDERS"
        default:
            println_debug("default")

        }
        
     //   AppUtility.alert(message: msgDisplay.localized, title: "", controller: self)
        
     //   AppUtility.showToastlocal(message: msgDisplay.localized, view: self.view)
        
//        let alert = UIAlertController(title: "OSK Vendor".localized, message: msgDisplay.localized, preferredStyle: UIAlertController.Style.alert)
//        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: nil))
//        self.present(alert, animated: true, completion: nil)
//
        self.showAlert(alertTitle: "OSK Vendor".localized, description: msgDisplay.localized)
    }
    
    func getTopMostViewController() -> UIViewController? {
          var topMostViewController = UIApplication.shared.keyWindow?.rootViewController
          
          while let presentedViewController = topMostViewController?.presentedViewController {
              topMostViewController = presentedViewController
          }
          
          return topMostViewController
      }
    
    //MARK:- Side Menu action
    @IBAction func menuAction(_ sender: UIBarButtonItem) {
        
        if let drawerController = self.navigationController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
    
    
    //MARK:- all actions
    
    @IBAction func MenuToHomeUnwindSegue( _ seg: UIStoryboardSegue) {
              
              
    }
    
    func logout(){
        DispatchQueue.main.async {
            self.alertVC?.ShowSAlert(title: "Information".localized, withDescription: "Are you sure you want to logout?".localized, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "Yes".localized, AlertType: .defualt, withComplition: {
                UserDefaults.standard.set(false, forKey: AppConstants.userDefault.loginStatus)
                UserDefaults.standard.set(false, forKey: AppConstants.userDefault.isLogin)
                UserDefaults.standard.set(false, forKey: AppConstants.userDefault.enabledLogout)
                UserDefaults.standard.synchronize()
                let loginStoryboard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                let viewController = loginStoryboard.instantiateInitialViewController()
                UIApplication.shared.keyWindow?.rootViewController = viewController
                self.performSegue(withIdentifier: "logoutComplete", sender: self)
                
            })
            let cancelAction = SAlertAction()
            cancelAction.action(name: "Cancel".localized, AlertType: .defualt, withComplition: {
                UserDefaults.standard.set(false, forKey: "isLogin")
            })
            self.alertVC?.addAction(action: [YesAction,cancelAction])
            self.isLogoutTrue = false
        }
    }
    
    func getFAQ() {
        println_debug("getFAQ")

        if AppUtility.isConnectedToNetwork() {
            AppUtility.showLoading(self.view)
            let urlStr = String(format: "%@/Vendor/GetFAQDetails", APIManagerClient.sharedInstance.base_url) as String
            print("Get GetFAQDetails :\(urlStr) ")
            self.apiManager.getGenericWebCall(urlString: urlStr, onSuccess: { [weak self] data in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    guard let jsonData = data as? Data else {
                        return
                    }
                    let decode = JSONDecoder()
                    
                    print("data \(jsonData)")
                }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
            })
        }
    }
}
