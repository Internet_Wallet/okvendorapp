//
//  SamTextfield.swift
//  AnimatedTextField
//
//  Created by Sam on 02/07/2020.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit



class SamTextfield : NSObject,UITextFieldDelegate {
    // Objects
    var containerView = UIView()
    var textField = UITextField()
    var title = UILabel()
    
    var containerBackgroundColor : UIColor?
    // Title label Properties
    var titleBackgroundColor : UIColor?
    var titlefontColor : UIColor?
    // textField Properties
    var textFieldRadius : CGFloat?
    var textFieldBorderColor : UIColor?
    var textFieldBorderWidth : CGFloat?
    
    
    func animatedTextField(rect: CGRect,placeholder: String,vc: UIViewController) -> UIView  {
        containerView.frame = rect
        containerView.backgroundColor = containerBackgroundColor ?? UIColor.white
        
        textField.frame = CGRect(x: 10, y: 10, width: rect.width - 20, height: rect.height - 20)
        textField.backgroundColor = UIColor.white
        textField.layer.borderColor = textFieldBorderColor?.cgColor ?? UIColor.black.cgColor
        textField.layer.cornerRadius = textFieldRadius ?? 0.0
        textField.layer.borderWidth = textFieldBorderWidth ?? 1.0
        textField.placeholder = placeholder
        textField.delegate = self
        textField.addTarget(self, action: #selector(editChanged(_:)), for: .editingChanged)
        
        containerView.addSubview(textField)
        let width = textWidth(text: placeholder, font: UIFont.systemFont(ofSize: 14))
        title.frame = CGRect(x: rect.origin.x + 20, y: 20, width: width + 10, height: 20)
        title.text = " " + placeholder
        title.textColor = titlefontColor ?? UIColor.blue
        title.font = UIFont.systemFont(ofSize: 14)
        title.textAlignment = .left
        title.backgroundColor = titleBackgroundColor ?? UIColor.white
        title.bringSubviewToFront(containerView)
        title.isHidden = true
        
        containerView.addSubview(title)
        return containerView
    }
    
    func textWidth(text: String, font: UIFont?) -> CGFloat {
        let attributes = font != nil ? [NSAttributedString.Key.font: font] : [:]
        return text.size(withAttributes: attributes as [NSAttributedString.Key : Any]).width
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count == 0 {
            title.isHidden = true
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut],
                           animations: {
                            self.title.center.y -= self.containerView.bounds.height - 50
            }, completion: nil)
        }else {
            title.isHidden = false
        }
        
    }
    
    @objc func editChanged(_ textField: UITextField) {
        if textField.text?.count ?? 0 > 0 {
            if title.isHidden {
                title.isHidden = false
                if !title.isHidden {
                    UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut],
                                   animations: {
                                    self.title.center.y -= self.containerView.bounds.height - 50
                    }, completion: nil)
                }
            }
        }else {
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn],
                           animations: {
                            self.title.frame.origin.y = 20
                            self.title.isHidden = true
            }, completion: nil)
        }
    }
    
}
