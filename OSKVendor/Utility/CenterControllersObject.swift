//
//  CenterControllersObject.swift
//  BereSoon
//
//  Created by Rupak on 11/11/16.
//  Copyright © 2016 bs23. All rights reserved.
//

import UIKit


struct CenterControllersObject {
    
    var centerControllers: [AnyObject] = []
    var checkoutControllers: [AnyObject] = []
    var currentCenterVC: AnyObject?
    
    static var sharedInstance = CenterControllersObject()
    
    init(){
        centerControllers = []
        checkoutControllers = []
    }
    
//    func getParentController() -> AnyObject{
//        if centerControllers.count > 0{
//            return centerControllers[centerControllers.count - 1]
//        }
//        return appDelegate.homeViewC!
//    }
    
//    mutating func pushNewController(controller: AnyObject){
//        if centerControllers.count > 0 {
//            let fromController = centerControllers[centerControllers.count - 1] as! UIViewController
//            let toController = controller as! UIViewController
//            slideInTransitionFromView(view: fromController.view, toView: toController.view, duration: 0.2, completion: { (bool) in
//                centerControllersObject.centerControllers.append(controller)
//                if centerControllersObject.centerControllers.count > 1{
//                    appDelegate.slideContainer.panMode = MFSideMenuPanMode.init(0)
//                }
//                appDelegate.slideContainer.centerViewController = controller
//            })
//        }else {
//            currentCenterVC = controller
//            centerControllers.append(controller)
//            appDelegate.slideContainer.centerViewController = controller
//        }
//        if centerControllers.count > 1{
//            appDelegate.slideContainer.panMode = MFSideMenuPanMode.init(0)
//        }
//    }
    
    mutating func changeCurrentViewController(controller: AnyObject){
//        appDelegate.slideContainer.centerViewController = controller
        currentCenterVC = controller
//        centerControllersObject.centerControllers[0] = controller
    }
    
//    mutating func popTopViewController(){
//        if centerControllersObject.centerControllers.count > 1{
//            let fromController = centerControllersObject.centerControllers[centerControllers.count - 1] as! UIViewController
//            let toController = centerControllersObject.centerControllers[centerControllers.count - 2] as! UIViewController
//            slideOutTransitionFromView(view: fromController.view, toView: toController.view, duration: 0.2, completion: { (bool) in
//                centerControllersObject.centerControllers.removeLast()
//                appDelegate.slideContainer.centerViewController = centerControllersObject.centerControllers[centerControllersObject.centerControllers.count - 1]
//                if centerControllersObject.centerControllers.count == 1 {
//                    appDelegate.slideContainer.panMode = MFSideMenuPanMode.init(1)
//                }
//            })
//        }
 //   }
    
//    mutating func popTopRootViewController(controller: AnyObject){
//            let fromController = controller as! UIViewController
//            let toController = appDelegate.homeViewC!
//            slideOutTransitionFromView(view: fromController.view, toView: toController.view, duration: 0.2, completion: { (bool) in
//                centerControllersObject.centerControllers.removeAll()
//
//                centerControllersObject.currentCenterVC = appDelegate.homeViewC!
//                centerControllersObject.centerControllers.append(appDelegate.homeViewC!)
//                appDelegate.slideContainer.centerViewController = appDelegate.homeViewC
//
//                if centerControllersObject.centerControllers.count == 1 {
//                    appDelegate.slideContainer.panMode = MFSideMenuPanMode.init(1)
//                }
//            })
//    }
    
    func updateAfterChangingShoppingQuantity(){

    }
    
//    func presentViewCntroller(controller: AnyObject){
//        centerControllersObject.checkoutControllers.append(controller)
//    }
    
//    func dismissCheckoutController(){
//        let topController = centerControllersObject.checkoutControllers[0] as! UIViewController
//        topController.dismiss(animated: true, completion: nil)
//        centerControllersObject.checkoutControllers = []
//    }
    
}

extension CenterControllersObject{
    
//    func slideInTransitionFromView(view: UIView, toView: UIView, duration: TimeInterval, completion: ((Bool) -> Void)?) {
//        if let container = view.superview {
//            var outgoingViewEndX = CGFloat(0) ,incomingViewEndX = CGFloat(0), incomingViewStartX = CGFloat(0), outgoingDisplacement = CGFloat(0)
//
//            if appDelegate.langStr.compare("en").rawValue == 0 {
//                outgoingViewEndX = view.frame.origin.x - view.frame.size.width
//                incomingViewEndX = view.frame.origin.x
//                incomingViewStartX = view.frame.origin.x + view.frame.size.width
//                outgoingDisplacement = view.frame.origin.x - outgoingViewEndX
//            }else{
//                outgoingViewEndX = view.frame.size.width
//                incomingViewEndX = view.frame.origin.x
//                incomingViewStartX = view.frame.origin.x - view.frame.size.width
//                outgoingDisplacement = outgoingViewEndX - incomingViewEndX
//            }
//
//            toView.frame.origin = CGPoint(x: incomingViewStartX, y: toView.frame.origin.y)
//                //CGPointMake(incomingViewStartX, toView.frame.origin.y)
//            container.addSubview(toView)
//
//            DispatchQueue.main.async(execute: { ()
//                UIView.animate(withDuration: duration, delay: 0, options: .curveLinear, animations: {
//                    view.frame.origin.x = outgoingViewEndX
//                    toView.frame.origin.x = incomingViewEndX
//                    }, completion: {(complete: Bool) in
//                        //keep sliding until it's offscreen
//                        if appDelegate.langStr.compare("en").rawValue == 0 && outgoingViewEndX + view.frame.size.width > 0 {
//
//                            let finalDuration = duration * Double((outgoingViewEndX + view.frame.size.width)/outgoingDisplacement)
//                            UIView.animate(withDuration: finalDuration, delay: 0, options: .curveLinear, animations: {
//                                view.frame.origin.x = -view.frame.size.width
//                                }, completion: { (complete: Bool) in
//                                    view.frame.origin.x += outgoingDisplacement
//                                    completion?(complete)
//                                    view.removeFromSuperview()
//                            })
//                        }
//                        else {
//                            if appDelegate.langStr.compare("en").rawValue == 0{
//                                view.frame.origin.x += outgoingDisplacement
//                                //toView.frame.origin = CGPointMake(0.0, toView.frame.origin.y)
//                            }else {
//                                view.frame.origin.x -= outgoingDisplacement
//                            }
//                            view.removeFromSuperview()
//                            completion?(complete)
//                        }
//                })
//            })
//        }
//    }
    
//    func slideOutTransitionFromView(view: UIView, toView: UIView, duration: TimeInterval, completion: ((Bool) -> Void)?) {
//        if let container = view.superview {
//            var outgoingViewEndX = CGFloat(0) ,incomingViewEndX = CGFloat(0), incomingViewStartX = CGFloat(0), outgoingDisplacement = CGFloat(0)
//
//            if appDelegate.langStr.compare("en").rawValue == 0 {
//                outgoingViewEndX = view.frame.size.width
//                incomingViewEndX = view.frame.origin.x
//                incomingViewStartX = view.frame.origin.x - view.frame.size.width
//                outgoingDisplacement = view.frame.origin.x + outgoingViewEndX
//            }else{
//                outgoingViewEndX = view.frame.origin.x - view.frame.size.width
//                incomingViewEndX = view.frame.origin.x
//                incomingViewStartX = view.frame.origin.x + view.frame.size.width
//                outgoingDisplacement = view.frame.origin.x - outgoingViewEndX
//            }
//
//            toView.frame.origin = CGPoint(x: incomingViewStartX, y: toView.frame.origin.y)
//                //CGPointMake(incomingViewStartX, toView.frame.origin.y)
//            container.addSubview(toView)
//
//            DispatchQueue.main.async(execute: { ()
//                UIView.animate(withDuration: duration, delay: 0, options: .curveLinear, animations: {
//                    view.frame.origin.x = outgoingViewEndX
//                    toView.frame.origin.x = incomingViewEndX
//                    }, completion: {(complete: Bool) in
//                        //keep sliding until it's offscreen
//                        if appDelegate.langStr.compare("en").rawValue == 0 && outgoingViewEndX - 2.0*view.frame.size.width > 0 {
//
//                            let finalDuration = duration * Double((outgoingViewEndX + view.frame.size.width)/outgoingDisplacement)
//                            UIView.animate(withDuration: finalDuration, delay: 0, options: .curveLinear, animations: {
//                                view.frame.origin.x = view.frame.size.width
//                                }, completion: { (complete: Bool) in
//                                    //toView.frame.origin.x -= outgoingDisplacement
//                                    completion?(complete)
//                                    //view.removeFromSuperview()
//                            })
//                        }
//                        else {
//                            if appDelegate.langStr.compare("en").rawValue == 0{
//                                view.frame.origin.x -= outgoingDisplacement
//                            }else{
//                                view.frame.origin.x += outgoingDisplacement
//                            }
//                            view.removeFromSuperview()
//                            completion?(complete)
//                        }
//                })
//            })
//        }
//    }
}

extension CenterControllersObject{
    
}
