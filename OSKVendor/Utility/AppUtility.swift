//
//  AppUtility.swift
//  BereSoon
//
//  Created by Rupak on 10/25/16.
//  Copyright © 2016 bs23. All rights reserved.
//

import UIKit
import Reachability
import CoreMotion

func println_debug <OSKVendor> (_ object: OSKVendor) {
    #if DEBUG
    print(object)
    #endif
}

extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}

class AppUtility: NSObject {
    
    class func setFrameForContactSuggestion(arrayCount: Int, indexpath: IndexPath,onTextField: UITextField,tableView: UITableView) -> CGRect {
                          
                  var height: CGFloat = CGFloat(Float(arrayCount) * 70.0)
                  if height >= 200.0{
                      height = 200.0
                  }
                  return CGRect(x: onTextField.frame.minX, y: tableView.convert(tableView.rectForRow(at: indexpath), to: tableView).minY - height + onTextField.frame.height + 20, width: onTextField.frame.width, height: height)
           
       }
    
    class func gettransID()->String {
        let tenDigit = Date().toMillis()
        return "\(tenDigit ?? 323232232323232)"
    }
    
    @objc class func hideLoading(_ view: UIView) {
        DispatchQueue.main.async {
            guard let keyWindow = UIApplication.shared.keyWindow else { return }
            if let loaderView   = keyWindow.subviews.first(where: {$0.tag == ZayOKLoader.uniqueTag})  {
                loaderView.removeFromSuperview()
            }
        }
    }
    
    class func showLoading(_ view: UIView) {
        DispatchQueue.main.async {
            guard let loader = ZayOKLoader.updateView() else { return }
            guard let keyWindow = UIApplication.shared.keyWindow else { return }
            keyWindow.addSubview(loader)
            keyWindow.makeKeyAndVisible()
        }
    }
    
    class func showToast(_ message:String?, view: UIView) {
        //        if let rootView = appDelegate.window?.rootViewController?.view {
        //            let globalHud = MBProgressHUD.showAdded(to: rootView, animated: true)
        //            globalHud.mode = MBProgressHUDMode.text
        //            globalHud.detailsLabel.text = message ?? ""
        //            self.perform(#selector(AppUtility.hideLoading(_:)), with: rootView, afterDelay: 2)
        //        }
    }
    
    class func showToastCustomBlackInView(message : String, controller: UIViewController) {
        //        let toastView = UIView(frame: CGRect(x: 0, y: (SCREEN_HEIGHT/2)-30, width: SCREEN_WIDTH-40, height: 40))
        let toastView = UIView()
        toastView.layer.cornerRadius = 8
        toastView.layer.masksToBounds = true
        let toastLabel = UILabel()
        toastLabel.textAlignment = .center
        toastLabel.lineBreakMode = .byWordWrapping
        toastLabel.numberOfLines = 0
        toastLabel.textColor = .white
        toastView.backgroundColor = UIColor.black
        toastView.alpha = 0.6
      //  toastLabel.text = message.localized
        toastLabel.font = UIFont(name: appFont,size: 12.0)
        var heightLabel = AppUtility.heightForView(text: toastLabel.text ?? "", font: UIFont(name: appFont, size: 12)!, width: SCREEN_WIDTH-90)
        heightLabel += 30
        toastLabel.frame = CGRect(x: 15, y: 10, width: SCREEN_WIDTH-70, height: heightLabel)
        toastView.frame = CGRect(x: 30, y: (SCREEN_HEIGHT/2)-heightLabel, width: SCREEN_WIDTH-60, height: heightLabel)
        controller.view.bringSubviewToFront(toastLabel)
        toastView.addSubview(toastLabel)
        controller.view.addSubview(toastView)
        UIView.animate(withDuration: 4, delay: 1, options: .curveEaseOut, animations: {
            toastView.alpha = 0.0
        }, completion: {(isCompleted) in
            toastView.removeFromSuperview()
        })
    }
    
    class func showToastlocal(message : String, view: UIView) {
        DispatchQueue.main.async {
            let toastView = UIView(frame: CGRect(x: 20, y: (view.frame.height/2)-40, width: view.frame.width-40, height: 80))
            toastView.layer.cornerRadius = 20
            toastView.layer.masksToBounds = true
            let toastLabel = UILabel()
            toastLabel.frame = CGRect(x: 10, y: 10, width: toastView.frame.size.width-20, height: 60)
            toastLabel.text = message
            toastLabel.baselineAdjustment = .alignCenters
            toastLabel.lineBreakMode = .byClipping
            toastLabel.numberOfLines = 0
            toastLabel.font = UIFont(name: appFont,size: 15.0)
            toastLabel.textAlignment = .center
            toastLabel.textColor = .white
            toastView.backgroundColor = UIColor.black
            view.bringSubviewToFront(toastLabel)
            toastView.addSubview(toastLabel)
            view.addSubview(toastView)
            UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseOut, animations: {
                toastView.alpha = 0.0
            }, completion: {(isCompleted) in
                toastView.removeFromSuperview()
            })
        }
    }
    
    static func setFrameForMarketSuggestion(arrayCount: Int, indexpath: IndexPath, onView: UIButton,tableView: UITableView) -> CGRect {
                           
                   var height: CGFloat = CGFloat(Float(arrayCount) * 70.0)
                   if height >= 200.0{
                       height = 200.0
                   }
                   return CGRect(x: onView.frame.minX, y: tableView.convert(tableView.rectForRow(at: indexpath), to: tableView).minY - height + onView.frame.height + 20, width: onView.frame.width, height: height)
            
        }
    
    class func isValidEmail(_ testEmail:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testEmail)
    }
    
    class func getTheAmountFromString( amount : String ) -> Double {
        if let amountExtract = amount.replacingOccurrences(of: "MMK", with: " ") as? String {
            if let amountExtract2 = amountExtract.replacingOccurrences(of: ",", with: "") as? String {
                if  let amountExtract3 = amountExtract2.replacingOccurrences(of: " ", with: "") as? String {
                    let doubleValue = Double(amountExtract3) ?? 0.0
                    return Double(doubleValue)
                }
            }
        }
    }
    
    
    class func getPriceOldPriceDiscountPrice(price: String, oldPrice: String, priceWithDiscount: String) -> (String, String, String) {
        let price = price
        let priceStr = price.replacingOccurrences(of: ",", with: "")
        let priceArr = priceStr.split(separator: " ")
        let priceStrNumber = String(priceArr.first ?? "")
        let priceValue = NSString(string: priceStrNumber).floatValue
        
        let oldPrice = oldPrice
        let oldPriceStr = oldPrice.replacingOccurrences(of: ",", with: "")
        let oldPriceArr = oldPriceStr.split(separator: " ")
        let oldPriceStrNumber = String(oldPriceArr.first ?? "")
        let oldPriceValue = NSString(string: oldPriceStrNumber).floatValue
        
        let priceWithDiscount = priceWithDiscount
        let priceWithDiscountStr = priceWithDiscount.replacingOccurrences(of: ",", with: "")
        let priceWithDiscountArr = priceWithDiscountStr.split(separator: " ")
        let priceWithDiscountStrNumber = String(priceWithDiscountArr.first ?? "")
        let priceWithDiscountValue = NSString(string: priceWithDiscountStrNumber).floatValue
        
        var priceFinal = ""
        var oldPriceFinal = ""
        var discountPriceFinal = ""
        
        if oldPriceValue != 0 {
            if (oldPriceValue > priceValue) {
                discountPriceFinal = price
                oldPriceFinal = oldPrice
            }
            else {
                 priceFinal = price
            }
        }
        else if priceWithDiscountValue != 0 {
            if (priceWithDiscountValue < priceValue) {
                discountPriceFinal = priceWithDiscount
                oldPriceFinal = price
            }
            else {
                priceFinal = price
            }
        }
        else {
            priceFinal = price
        }
        
        return (priceFinal, oldPriceFinal, discountPriceFinal)
    }
    
    class func isConnectedToNetwork() -> Bool {
        do {
            let reachability = try Reachability()
            
            switch reachability.connection{
            case .wifi, .cellular:
                println_debug("Connected With wifi")
                return true
            case .none:
                println_debug("Not Connected")
                return false
            default:
                return true
            }
        }
        catch let error as NSError{
            print(error.debugDescription)
            return true
        }
    }
    
//    class func hasConnectivity() -> Bool {
//
//        let status = Reachability.instance.networkConnectionStatus()
//        guard status != NetworkConnectionStatus.notConnection else {
//            showAlertView(message: UsersAlertConstant.checkInternet)
//            return false
//        }
//        return true
//    }
//
//
//    class func showAlertView(message: String?) {
//
//        DispatchQueue.main.async {
//            let alertController = UIAlertController(title: "1 Stop Mart", message: message, preferredStyle: .alert)
//            alertController.addAction(UIAlertAction(title: UsersAlertConstant.okAction, style: .default, handler: nil))
//            UIApplication.topViewController()!.present(alertController, animated: true)
//        }
//
//    }
    
    
//    class func showInternetErrorToast(_ message:String, view: UIView) {
//        //UIColor.messageBGColor
//        ZAlertView.positiveColor            = UIColor.navigationPrimaryColor()
//        //UIColor.color("#ff4665")
//        ZAlertView.negativeColor            = UIColor.color("#ff4665")
//        //UIColor.color("#ff4665")
//        ZAlertView.blurredBackground        = true
//        ZAlertView.showAnimation            = .bounceTop
//        ZAlertView.hideAnimation            = .flyBottom
//        ZAlertView.initialSpringVelocity    = 0.9
//        ZAlertView.duration                 = 2
//        ZAlertView.alertTitleFont = UIFont(name: "IRANSansMobile_Bold", size: 18)
//        ZAlertView.buttonFont = UIFont(name: "IRANSansMobile_Bold", size: 17)
//        ZAlertView.messageFont = UIFont(name: "IRANSansMobile_UltraLight", size: 12)
//        ZAlertView.titleColor = UIColor.black
//        ZAlertView.buttonTitleColor = UIColor.white
//        ZAlertView.messageColor = UIColor.darkGray
//        let dialog = ZAlertView(title: "ERROR".localized,
//                                message:message,
//                                closeButtonText: "DISMISS".localized,
//                                closeButtonHandler: { alertView in
//                                    alertView.dismissAlertView()
//        }
//        )
//        dialog.allowTouchOutsideToDismiss = true
//        dialog.show()
//    }
    //    class func getHeaderDic() -> [String:String] {
    //        
    //        let deviceId = UIDevice.current.identifierForVendor?.uuidString ?? ""
    //        let headers = ["DeviceId": deviceId]
    //        var headerDictionary = headers
    //        if let token = VMLoginModel.shared.token {
    //            headerDictionary = ["Token":token,"DeviceId": deviceId]
    //            print(token)
    //        }
    //        
    //        return headerDictionary
    //        
    //    }
    
    class func userIsLoggedIn() -> Bool {
        let defaults = UserDefaults.standard
        if defaults.string(forKey: "accessToken") != nil {
            return true
        }
        return false
    }
    
    class func userLogOut() {
        
    }
    
    class func reloadInterface() {
        //centerControllersObject.centerControllers = []
      //  APIManagerClient.sharedInstance.homePageProductArray = []
        //        appDelegate.window?.rootViewController?.removeFromParent()
        //        appDelegate.homeViewC = nil
        //        appDelegate.initialSetUpRootViewController()
    }
    
    
    class func backToRootView(_ navC:UINavigationController) {
        self.perform(#selector(AppUtility.performAfterTwoSecond(_:)), with: navC, afterDelay:2)
    }
    
    @objc class func performAfterTwoSecond(_ navC:UINavigationController) {
        let array = (navC.viewControllers) as NSArray
        navC.popToViewController(array.object(at: 0) as! UIViewController, animated: true)
    }
    
    class func getPrimaryCellColor ()-> UIColor {
        return UIColor(red: 252/255.0, green: 113/255.0, blue: 0/255.0, alpha: 1.0)
    }
    
    class func getAttributeString(_ firstString: String, secondString: String, secondStrColor: UIColor) -> NSAttributedString {
        let mutAttStr = NSMutableAttributedString()
        let firstStr = NSAttributedString(string: firstString, attributes: [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 15)])
        let secondStr = NSAttributedString(string: secondString, attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 15),NSAttributedString.Key.foregroundColor:secondStrColor])
        mutAttStr.append(firstStr)
        mutAttStr.append(secondStr)
        return mutAttStr
    }
    
    class func getCurrentLanguageInApp() -> String {
        let defaults = UserDefaults.standard
        if let language = defaults.string(forKey: "appCurrentLanguage"), language.count > 0 {
            return language
        } else {
            UserDefaults.standard.set("English", forKey: "appCurrentLanguage")
            return "English"
        }
    }
    
    class func getDiscount(priceValue:Float,oldDiscountPriceValue:Float) -> String {
        var vagFol =  ((priceValue - oldDiscountPriceValue) * 100)/priceValue
        vagFol = vagFol.rounded()
        if(vagFol>0){
            return "+\(vagFol)%"
        }
        return "\(vagFol)%"
    }
    
    class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    class func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = UIFont(name: appFont, size: 15.0)
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    class func getIPAddress() -> String? {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer {
                    ptr = ptr?.pointee.ifa_next
                }
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    if let ifaName = interface?.ifa_name {
                        if String(cString: ifaName) == "en0" {
                            var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                            getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                            address = String(cString: hostname)
                        }
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address
    }
    
    class  func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    class func JSONStringFromAnyObject(value: AnyObject, prettyPrinted: Bool = true) -> String {
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : nil
        if JSONSerialization.isValidJSONObject(value) {
            
            do {
                let data = try JSONSerialization.data(withJSONObject: value, options: options!)
                if let string = String(data: data, encoding: String.Encoding.utf8) {
                    return string
                }
            } catch {
                println_debug(error)
            }
        }
        return ""
    }
    
    class func resize(_ image: UIImage) -> UIImage {
        var actualHeight = Float(image.size.height)
        var actualWidth = Float(image.size.width)
        let maxHeight: Float = 300.0
        let maxWidth: Float = 400.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.5
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img?.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!) ?? UIImage()
    }
    
    
    class func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    class func NKPlaceholderImage(image:UIImage?, imageView:UIImageView?,imgUrl:String,compate:@escaping (UIImage?) -> Void){
        
        if image != nil && imageView != nil {
            imageView!.image = image!
        }
        
        var urlcatch = imgUrl.replacingOccurrences(of: "/", with: "#")
        let documentpath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        urlcatch = documentpath + "/" + "\(urlcatch)"
        
        let image = UIImage(contentsOfFile:urlcatch)
        if image != nil && imageView != nil
        {
            imageView!.image = image!
            compate(image)
            
        }else{
            
            if let url = URL(string: imgUrl){
                
                DispatchQueue.global(qos: .background).async {
                    () -> Void in
                    let imgdata = NSData(contentsOf: url)
                    DispatchQueue.main.async {
                        () -> Void in
                        imgdata?.write(toFile: urlcatch, atomically: true)
                        let image = UIImage(contentsOfFile:urlcatch)
                        compate(image)
                        if image != nil  {
                            if imageView != nil  {
                                imageView!.image = image!
                            }
                        }
                    }
                }
            }
        }
    }
    
    class func getDigitDisplay(_ digit: String) -> String {
        var FormateStr: String
        let mystring = digit.replacingOccurrences(of: ",", with: "")
        let number = NSDecimalNumber(string: mystring)
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        FormateStr = formatter.string(from: number)!
        if FormateStr == "NaN"{
            FormateStr = ""
        }
        
        return FormateStr
    }
    
    class func removeCommaFromDigit(_ digit: String) -> String {
        
        var formateStr: String
        
        formateStr = digit.replacingOccurrences(of: ",", with: "")
        return formateStr
        
    }
    
    class func getFormattedDate(date: Date, format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: date)
    }
    
    class func dateAndTimeOrderPlaced(timeStr:String)->String {
        
        var date = ""
        let arrTemp = timeStr.split(separator:"T")
        if (arrTemp.count > 0) {
            let secondPart = arrTemp[1]
            let arrSSS = secondPart.split(separator:".")
            if (arrSSS.count > 0) {
                date = String(arrSSS[0])
            }
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss.SSS"
        guard let time = dateFormatter.date(from: timeStr) else { return "" }//timeStr
        dateFormatter.dateFormat = "EEEE MMM d yyyy"//"E, d MMM yyyy hh:mm a"//HH:mm:ss
        return dateFormatter.string(from: time) + " " + date
    }
    
    //Present Alert with title and message
    static func alert(message: String, title: String ,controller: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK".localized, style: .default, handler: nil)
        alertController.addAction(OKAction)
        controller.present(alertController, animated: true, completion:nil)
    }
    
    var phNumValidationsFile: [PhNumValidationList] = []
    
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: String.Encoding.utf8)
            else { return nil}
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html,
                                                                .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            
        } catch let error as NSError {
            println_debug(error.localizedDescription)
            return nil
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func stringByAppendingPathComponent(_ path: String) -> String {
        let nsSt = self as NSString
        return nsSt.appendingPathComponent(path)
    }
    
    var trailingSpacesTrimmed: String {
        var newString = self
        
        while newString.hasSuffix(" ") {
            newString = String(newString.dropLast())
        }
        
        return newString
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

extension NSAttributedString {
    func height(containerWidth: CGFloat) -> CGFloat {
        let rect = self.boundingRect(with: CGSize.init(width: containerWidth, height: CGFloat.greatestFiniteMagnitude), options: [.usesLineFragmentOrigin, .usesDeviceMetrics, .usesFontLeading], context: nil)
        return ceil(rect.size.height)
    }
}

extension String {
    var html: NSAttributedString? {
        do {
            let attr = try  NSAttributedString.init(data: self.data(using: .utf8) ?? Data(), options:[.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            return attr
        } catch {
            println_debug(error)
        }
        return NSAttributedString.init(string: "")
    }
}

extension UIApplication {
    class func topViewController(_ controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(presented)
        }
        return controller
    }
}

extension UISearchBar{
    func applySearchGradient( colors : [UIColor]) {
        var frameAndStatusBar: CGRect = self.bounds
        frameAndStatusBar.size.height += 20 // add 20 to account for the status bar
        setBackgroundImage(UISearchBar.searchgradient(size: frameAndStatusBar.size, colors: colors), for: UIBarPosition.top, barMetrics: .default)
    }
    
    
    /// Creates a gradient image with the given settings
    static func searchgradient(size : CGSize, colors : [UIColor]) -> UIImage? {
        // Turn the colors into CGColors
        let cgcolors = colors.map { $0.cgColor }
        // Begin the graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        // If no context was retrieved, then it failed
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        // From now on, the context gets ended if any return happens
        defer { UIGraphicsEndImageContext() }
        // Create the Coregraphics gradient
        var locations : [CGFloat] = [0.0, 1.0]
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: cgcolors as NSArray as CFArray, locations: &locations) else { return nil }
        // Draw the gradient
        context.drawLinearGradient(gradient, start: CGPoint(x: 0.0, y: 0.0), end: CGPoint(x: size.width, y: 0.0), options: [])
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}

extension UINavigationBar {
    /// Applies a background gradient with the given colors
    func applyNavigationGradient( colors : [UIColor]) {
        var frameAndStatusBar: CGRect = self.bounds
        frameAndStatusBar.size.height += 20 // add 20 to account for the status bar
        setBackgroundImage(UINavigationBar.gradient(size: frameAndStatusBar.size, colors: colors), for: .default)
    }
    
    func applyNavigationGradient1( colors : [UIColor]) {
        var frameAndStatusBar: CGRect = self.bounds
        frameAndStatusBar.size.height += 20 // add 20 to account for the status bar
        setBackgroundImage(UINavigationBar.gradient(size: frameAndStatusBar.size, colors: colors), for: .default)
    }
    
    func applyNavigationGradient2( colors : [UIColor]) {
        var frameAndStatusBar: CGRect = self.bounds
        frameAndStatusBar.size.height += 20 // add 20 to account for the status bar
        setBackgroundImage(UINavigationBar.gradient(size: frameAndStatusBar.size, colors: colors), for: .default)
    }
    /// Creates a gradient image with the given settings
    static func gradient(size : CGSize, colors : [UIColor]) -> UIImage? {
        // Turn the colors into CGColors
        let cgcolors = colors.map { $0.cgColor }
        // Begin the graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        // If no context was retrieved, then it failed
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        // From now on, the context gets ended if any return happens
        defer {
            UIGraphicsEndImageContext()
        }
        // Create the Coregraphics gradient
        var locations : [CGFloat] = [0.0, 1.0]
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: cgcolors as NSArray as CFArray, locations: &locations) else { return nil }
        // Draw the gradient
        context.drawLinearGradient(gradient, start: CGPoint(x: 0.0, y: 0.0), end: CGPoint(x: size.width, y: 0.0), options: [])
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    
    
}


extension UINavigationItem{
    func setMarqueLabelInNavigation(TextStr: String, count: Int) {
        self.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 10, width: SCREEN_WIDTH, height: 25)
        lblMarque.font =  UIFont(name: appFont, size: 18)
       // lblMarque.text = TextStr.localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        if count > 50 {
            lblMarque.speed = .duration(55)
        } else {
            lblMarque.speed = .duration(30)
        }
        self.titleView = lblMarque
    }
}

extension UILabel{
    func setMarqueLabelInLabel(TextStr: String, count: Int) {
        self.text = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 10, width: SCREEN_WIDTH, height: 25)
        lblMarque.font =  UIFont(name: appFont, size: 18)
      //  lblMarque.text = TextStr.localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        if count > 50 {
            lblMarque.speed = .duration(20)
        } else {
            lblMarque.speed = .duration(10)
        }
    }
}

extension UICollectionViewCell {
    func roundCell(_ cell:UIView) {
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
        self.backgroundColor = UIColor.white
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 1.30, height: 1.30)
        self.layer.shadowOpacity = 0.25
        self.layer.shadowRadius = 1.3
        self.layer.masksToBounds = true
        self.clipsToBounds = false
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor(red: 225/255, green: 225/255, blue: 225/255, alpha: 1.0).cgColor
    }
}

extension UIView {
    func shadowToColleectionView () {
        self.layer.cornerRadius = 6
        self.layer.shadowOpacity = 0.25
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 2.5
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.masksToBounds = false
    }
    
    func shadowToNormalView () {
        
        self.layer.cornerRadius = 10
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 3.5
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.masksToBounds = false
    }
    
    func applyViewnGradient( colors : [UIColor]) {
        var frameAndStatusBar: CGRect = self.bounds
        frameAndStatusBar.size.height += 20 // add 20 to account for the status bar
        let imageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: frame.width, height: frame.height))
        imageView.image = UISearchBar.ViewGradient(size: frameAndStatusBar.size, colors: colors)
        self.addSubview(imageView)
        self.sendSubviewToBack(imageView)
    }
    
    /// Creates a gradient image with the given settings
    static func ViewGradient(size : CGSize, colors : [UIColor]) -> UIImage? {
        // Turn the colors into CGColors
        let cgcolors = colors.map { $0.cgColor }
        // Begin the graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        // If no context was retrieved, then it failed
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        // From now on, the context gets ended if any return happens
        defer {
            UIGraphicsEndImageContext()
        }
        // Create the Coregraphics gradient
        var locations : [CGFloat] = [0.0, 1.0]
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: cgcolors as NSArray as CFArray, locations: &locations) else { return nil }
        // Draw the gradient
        context.drawLinearGradient(gradient, start: CGPoint(x: 0.0, y: 0.0), end: CGPoint(x: size.width, y: 0.0), options: [])
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
}



extension UIButton {
    func BtnShadowToView () {
        self.layer.cornerRadius = 6
        self.layer.shadowOpacity = 0.25
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 2.5
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.masksToBounds = false
    }
    
    func applyButtonGradient( colors : [UIColor]) {
        let frameAndStatusBar: CGRect = self.bounds
        //frameAndStatusBar.size.height += 5 // add 20 to account for the status bar
        setBackgroundImage(UIButton.ButtonGradient(size: frameAndStatusBar.size, colors: colors), for: .normal)
    }
    
    /// Creates a gradient image with the given settings
    static func ButtonGradient(size : CGSize, colors : [UIColor]) -> UIImage?{
        // Turn the colors into CGColors
        let cgcolors = colors.map { $0.cgColor }
        // Begin the graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        // If no context was retrieved, then it failed
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        // From now on, the context gets ended if any return happens
        defer { UIGraphicsEndImageContext() }
        // Create the Coregraphics gradient
        var locations : [CGFloat] = [0.0, 1.0]
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: cgcolors as NSArray as CFArray, locations: &locations) else { return nil }
        // Draw the gradient
        context.drawLinearGradient(gradient, start: CGPoint(x: 0.0, y: 0.0), end: CGPoint(x: size.width, y: 0.0), options: [])
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
}

extension UITableView {
    func animate () {
        self.reloadData()
        let cells = self.visibleCells
        let tableHeight: CGFloat = self.bounds.size.height
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        var index = 0
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.1, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
    
    
    
    
    
    // Export pdf from UITableView and save pdf in drectory and return pdf file path
    func exportAsPdfFromTable() -> String {
        
        // let originalBounds = self.bounds
        // self.bounds = CGRect(x:originalBounds.origin.x, y: originalBounds.origin.y, width: self.contentSize.width, height: self.contentSize.height + 300)
        let pdfPageFrame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.contentSize.height + 300)
        
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageFrame, nil)
        UIGraphicsBeginPDFPageWithInfo(pdfPageFrame, nil)
        guard let pdfContext = UIGraphicsGetCurrentContext() else { return "" }
        self.layer.render(in: pdfContext)
        UIGraphicsEndPDFContext()
        self.bounds = pdfPageFrame
        // Save pdf data
        return self.saveTablePdf(data: pdfData)
        
    }
    
    // Save pdf file in document directory
    func saveTablePdf(data: NSMutableData) -> String {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docDirectoryPath = paths[0]
        let pdfPath = docDirectoryPath.appendingPathComponent("tablePdf.pdf")
        if data.write(to: pdfPath, atomically: true) {
            return pdfPath.path
        } else {
            return ""
        }
    }
    
    
    
    
    
    
    
}

extension UICollectionView {
    func animate () {
        self.reloadData()
        let cells = self.visibleCells
        let tableHeight: CGFloat = self.bounds.size.height
        for i in cells {
            let cell: UICollectionViewCell = i as UICollectionViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        var index = 0
        for a in cells {
            let cell: UICollectionViewCell = a as UICollectionViewCell
            UIView.animate(withDuration: 1.1, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
}


public enum ViewState {
    case initial, pick, putDown
}

public enum ParallaxType {
    case basedOnHierarchyInParallaxView(parallaxOffsetMultiplier: CGFloat?)
    case basedOnTag
    case custom(parallaxOffset: CGFloat)
}

private struct AccelerometerMovement {
    let x: Double
    let y: Double
}

open class MPParallaxView: UIView {
    @IBInspectable open var initialParallaxOffset: CGFloat = 5.0
    @IBInspectable open var zoomMultipler: CGFloat = 0.02
    @IBInspectable open var parallaxOffsetDuringPick: CGFloat = 15.0
    @IBInspectable open var multiplerOfIndexInHierarchyToParallaxOffset: CGFloat = 7.0
    @IBInspectable open var initialShadowRadius: CGFloat = 10.0
    @IBInspectable open var finalShadowRadius: CGFloat = 20.0
    
    fileprivate(set) open var state: ViewState = .initial {
        didSet {
            if state != oldValue {
                animateForGivenState(state)
            }
        }
    }
    fileprivate(set) open var contentView: UIView = UIView()
    @IBInspectable open var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            contentView.layer.cornerRadius = cornerRadius
        }
    }
    open var parallaxType: ParallaxType = .basedOnTag
    @IBInspectable open var iconStyle: Bool = true
    var glowEffect: UIImageView = UIImageView()
    //MARK: CoreMotion
    fileprivate lazy var motionManager: CMMotionManager = CMMotionManager()
    fileprivate var accelerometerMovement: AccelerometerMovement?
    @IBInspectable open var accelerometerEnabled: Bool = false {
        didSet {
            if !accelerometerEnabled && motionManager.isAccelerometerActive {
                motionManager.stopAccelerometerUpdates()
            }
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        prepareParallaxLook()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareParallaxLook()
    }
    
    //MARK: Setup layout
    open func prepareParallaxLook() {
        setupLayout()
        addShadowPath()
        setupContentView()
    }
    
    open func prepareForNewViews() {
        if accelerometerEnabled {
            glowEffect.alpha = 0.0
            layer.transform = CATransform3DIdentity
            contentView.subviews.forEach { subview in
                subview.layer.transform = CATransform3DIdentity
            }
        }
        subviews.forEach { $0.removeFromSuperview() }
        contentView.subviews.forEach { $0.removeFromSuperview() }
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        prepareForMotionDetection()
    }
    
    fileprivate func prepareForMotionDetection() {
        if let currentQueue = OperationQueue.current, accelerometerEnabled {
            motionManager.accelerometerUpdateInterval = 0.1
            motionManager.startAccelerometerUpdates(to: currentQueue) {[weak self] data, error in
                guard let strongSelf = self else { return }
                strongSelf.accelerometerMovement = AccelerometerMovement(x: data?.acceleration.x ?? 0.0, y: data?.acceleration.y ?? 0.0)
                UIView.animate(withDuration: 0.1, animations: {
                    strongSelf.applyParallaxEffectOnView(basedOnTouch: nil)
                    strongSelf.applyGlowEffectOnView(basedOnAccelerometerMovement: strongSelf.accelerometerMovement)
                })
            }
        }
    }
    
    fileprivate func setupLayout() {
        layer.shadowRadius = initialShadowRadius
        layer.shadowOpacity = 0.6
        layer.shadowColor = UIColor.black.cgColor
        cornerRadius = iconStyle ? 5.0 : 0.0
        backgroundColor = .clear
    }
    
    fileprivate func setupContentView() {
        contentView.frame = bounds
        contentView.layer.masksToBounds = true
        contentView.backgroundColor = .white
        subviews.forEach { subview in
            subview.translatesAutoresizingMaskIntoConstraints = true
            subview.removeFromSuperview()
            contentView.addSubview(subview)
        }
        resizeSubviewsForParallax()
        if let glowImage = UIImage(named: "gloweffect") {
            glowEffect = UIImageView(image: glowImage)
            glowEffect.alpha = 0.0
            contentView.addSubview(glowEffect)
        }
        addSubview(contentView)
    }
    
    fileprivate func resizeSubviewsForParallax() {
        let offset: CGFloat = initialParallaxOffset
        contentView.subviews.forEach { subview in
            subview.frame.origin = CGPoint(x: -offset, y: -offset)
            subview.frame.size = CGSize(width: subview.frame.size.width + offset * 2.0, height: subview.frame.size.height + offset * 2.0)
        }
    }
    
    fileprivate func addShadowPath() {
        let path = UIBezierPath()
        let xOffset: CGFloat = 4.0
        let yOffset: CGFloat = 20.0
        path.move(to: CGPoint(x: xOffset, y: bounds.height))
        path.addLine(to: CGPoint(x: bounds.width - xOffset, y: bounds.height))
        path.addLine(to: CGPoint(x: bounds.width - xOffset, y: yOffset))
        path.addLine(to: CGPoint(x: xOffset, y: yOffset))
        path.close()
        layer.shadowPath = path.cgPath
    }
    
    //MARK: Animations
    fileprivate func makeZoomInEffect() {
        contentView.subviews.forEach { subview in
            subview.center = CGPoint(x: subview.center.x - widthZoom(subview), y: subview.center.y - heightZoom(subview))
            subview.frame.size = CGSize(width: subview.frame.size.width + widthZoom(subview) * 2.0, height: subview.frame.size.height + heightZoom(subview) * 2.0)
        }
    }
    
    fileprivate func makeZoomOutEffect() {
        UIView.animate(withDuration: 0.3, animations: {
            self.contentView.subviews.forEach { subview in
                subview.center = CGPoint(x: subview.center.x + self.widthZoom(subview), y: subview.center.y + self.heightZoom(subview))
                subview.frame.size = CGSize(width: subview.frame.size.width - self.widthZoom(subview) * 2.0, height: subview.frame.size.height - self.heightZoom(subview) * 2.0)
            }
        })
    }
    
    fileprivate func animateForGivenState(_ state: ViewState) {
        switch state {
        case .pick:
            animatePick()
            makeZoomInEffect()
        case .putDown:
            animateReturn()
            makeZoomOutEffect()
        case .initial:
            break
        }
    }
    
    fileprivate func addShadowGroupAnimation(shadowOffset: CGSize, shadowRadius: CGFloat, duration: TimeInterval, layer: CALayer) {
        if let presentationLayer = layer.presentation() {
            let offsetAnimation = CABasicAnimation(keyPath: "shadowOffset")
            offsetAnimation.fromValue = NSValue(cgSize: presentationLayer.shadowOffset)
            offsetAnimation.toValue = NSValue(cgSize: shadowOffset)
            let radiusAnimation = CABasicAnimation(keyPath: "shadowRadius")
            radiusAnimation.fromValue = presentationLayer.shadowRadius as NSNumber
            radiusAnimation.toValue = shadowRadius
            let animationGroup = CAAnimationGroup()
            animationGroup.duration = duration
            animationGroup.animations = [offsetAnimation, radiusAnimation]
            layer.add(animationGroup, forKey: "shadowAnim")
        }
        layer.shadowRadius = shadowRadius
        layer.shadowOffset = shadowOffset
    }
    
    fileprivate func pickAnimation() {
        let shadowOffset = CGSize(width: 0.0, height: 30.0);
        addShadowGroupAnimation(shadowOffset: shadowOffset, shadowRadius: finalShadowRadius, duration: 0.02, layer: layer)
    }
    
    fileprivate func putDownAnimation() {
        let shadowOffset = CGSize.zero
        addShadowGroupAnimation(shadowOffset: shadowOffset, shadowRadius: initialShadowRadius, duration: 0.4, layer: layer)
    }
    
    fileprivate func animatePick() {
        pickAnimation()
    }
    
    fileprivate func animateReturn() {
        putDownAnimation()
    }
    
    fileprivate func parallaxOffset(forView view: UIView) -> CGFloat {
        switch parallaxType {
        case .basedOnHierarchyInParallaxView(let parallaxOffsetMultiplier):
            if let indexInSuperview = view.superview?.subviews.index(of: view) {
                return CGFloat(indexInSuperview) * (parallaxOffsetMultiplier ?? multiplerOfIndexInHierarchyToParallaxOffset)
            } else {
                return 5.0
            }
        case .custom(let parallaxOffset):
            return parallaxOffset
        case .basedOnTag:
            return CGFloat(view.tag) * 2.0
        }
    }
    
    fileprivate func applyParallaxEffectOnSubviews(xOffset: CGFloat, yOffset: CGFloat) {
        var parallaxOffsetToSet: CGFloat
        for subview in contentView.subviews {
            parallaxOffsetToSet = parallaxOffset(forView: subview)
            let xParallaxOffsetAndSuperviewOffset = xOffset * CGFloat(parallaxOffsetToSet)
            let yParallaxOffsetAndSuperviewOffset = yOffset * CGFloat(parallaxOffsetToSet)
            subview.layer.transform = CATransform3DMakeTranslation(xParallaxOffsetAndSuperviewOffset, yParallaxOffsetAndSuperviewOffset, 0)
        }
    }
    
    fileprivate func applyParallaxEffectOnView(basedOnTouch touch: UITouch?) {
        var parallaxOffset = parallaxOffsetDuringPick
        var offsetX: CGFloat = 0.0, offsetY: CGFloat = 0.0
        if let accelerometerMovement = accelerometerMovement {
            offsetX = CGFloat(accelerometerMovement.x * 0.25)
            offsetY = CGFloat(accelerometerMovement.y * -0.25)
            parallaxOffset *= 2.0
        }
        
        if let touch = touch, let superview = superview, offsetX == 0.0 && offsetY == 0.0 {
            offsetX = (0.5 - touch.location(in: superview).x / superview.bounds.width) * -1
            offsetY = (0.5 - touch.location(in: superview).y / superview.bounds.height) * -1
        }
        var t = CATransform3DMakeScale(1.1, 1.1, 1.1)
        t.m34 = 1.0/(-500)
        let xAngle = (offsetX * parallaxOffset) * CGFloat(Double.pi / 180.0)
        let yAngle = (offsetY * parallaxOffset) * CGFloat(Double.pi / 180.0)
        t = CATransform3DRotate(t, xAngle, 0, -(0.5 - offsetY), 0)
        layer.transform = CATransform3DRotate(t, yAngle, (0.5 - offsetY) * 2, 0, 0)
        applyParallaxEffectOnSubviews(xOffset: offsetX, yOffset: offsetY)
    }
    
    fileprivate func removeParallaxEffectFromView() {
        UIView.animate(withDuration: 0.5, animations: {
            self.glowEffect.alpha = 0.0
            self.layer.transform = CATransform3DIdentity
            self.contentView.subviews.forEach { subview in
                subview.layer.transform = CATransform3DIdentity
            }
        })
    }
    
    //MARK: Glow effect
    fileprivate func applyGlowAlpha(_ glowAlpha: CGFloat) {
        if glowAlpha < 1.0 && glowAlpha > 0.0 {
            glowEffect.alpha = glowAlpha
        }
    }
    
    fileprivate func applyGlowEffectOnView(basedOnTouch touch: UITouch?) {
        let changeAlphaValue: CGFloat = 0.05
        if let touch = touch, touch.location(in: self).y > bounds.height / 2 {
            glowEffect.center = touch.location(in: self)
            applyGlowAlpha(glowEffect.alpha + changeAlphaValue)
        } else {
            applyGlowAlpha(glowEffect.alpha - changeAlphaValue)
        }
    }
    
    fileprivate func applyGlowEffectOnView(basedOnAccelerometerMovement movement: AccelerometerMovement?) {
        guard let movement = movement, let superview = superview else { return }
        let bigMultiplerForAccelerometerToGlowOffset: Double = Double(superview.bounds.size.width * 2.0)
        glowEffect.center = CGPoint(x: center.x + CGFloat(movement.x * bigMultiplerForAccelerometerToGlowOffset),
                                    y: center.y + CGFloat(movement.y *
                                        bigMultiplerForAccelerometerToGlowOffset) * -1.0)
        let changeAlphaValue: CGFloat = 0.05
        if glowEffect.center.y > center.y {
            applyGlowAlpha(glowEffect.alpha + changeAlphaValue)
        } else {
            applyGlowAlpha(glowEffect.alpha - changeAlphaValue)
        }
    }
    
    //MARK: On touch actions
    open override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        state = .pick
        applyParallaxEffectOnView(basedOnTouch: Array(touches).first)
        applyGlowEffectOnView(basedOnTouch: Array(touches).first)
        super.touchesMoved(touches, with: event)
    }
    
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        state = .putDown
        removeParallaxEffectFromView()
        super.touchesEnded(touches, with: event)
    }
}

extension MPParallaxView {
    func heightZoom(_ viewToCalculate: UIView) -> CGFloat {
        return viewToCalculate.bounds.size.height * zoomMultipler
    }
    
    func widthZoom(_ viewToCalculate: UIView) -> CGFloat {
        return viewToCalculate.bounds.size.width * zoomMultipler
    }
}




public extension UILabel {
    public convenience init(badgeText: String, color: UIColor = .white, fontSize: CGFloat = UIFont.smallSystemFontSize) {
        self.init()
        text = badgeText.count > 1 ? " \(badgeText) " : badgeText
        textAlignment = .center
        textColor = .white
        backgroundColor = color
        font = UIFont.systemFont(ofSize: fontSize)
        layer.cornerRadius = fontSize * CGFloat(0.6)
        clipsToBounds = true
        translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: self, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: self, attribute: .height, multiplier: 1, constant: 0))
    }
}


public extension UIBarButtonItem {
    convenience init(badge: String?, title: String, target: AnyObject?, action: Selector) {
        let button = UIButton(type: .custom)
        if let image = UIImage(named: "Cart") {
            button.setImage(image, for: .normal)
        }
        //        button.setTitle(title, for: .normal)
        //        button.titleLabel?.font = UIFont.systemFont(ofSize: UIFont.buttonFontSize)
        button.addTarget(target, action: action, for: .touchUpInside)
        button.sizeToFit()
        
        if let badge = badge {
            let badgeLabel = UILabel(badgeText: badge)
            badgeLabel.textColor = UIColor.white
            badgeLabel.backgroundColor = UIColor.colorWithRedValue(redValue: 231, greenValue: 160, blueValue: 59, alpha: 1)
            button.addSubview(badgeLabel)
            button.addConstraint(NSLayoutConstraint(item: badgeLabel, attribute: .top, relatedBy: .equal, toItem: button, attribute: .top, multiplier: 1, constant: 0))
            button.addConstraint(NSLayoutConstraint(item: badgeLabel, attribute: .centerX, relatedBy: .equal, toItem: button, attribute: .trailing, multiplier: 1, constant: 0))
        }
        self.init(customView: button)
    }
    
    var badgeLabel: UILabel? {
        return customView?.viewWithTag(UIBarButtonItem.badgeTag) as? UILabel
    }
    
    var badgedButton: UIButton? {
        return customView as? UIButton
    }
    
    var badgeString: String? {
        get { return badgeLabel?.text?.trimmingCharacters(in: .whitespaces) }
        set {
            if let badgeLabel = badgeLabel {
                badgeLabel.text = nil == newValue ? nil : " \(newValue!) "
                badgeLabel.sizeToFit()
                badgeLabel.isHidden = nil == newValue
            }
        }
    }
    
    var badgedTitle: String? {
        get { return badgedButton?.title(for: .normal) }
        set { badgedButton?.setTitle(newValue, for: .normal); badgedButton?.sizeToFit() }
    }
    
    private static let badgeTag = 7373
}

extension UIDevice {
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    enum ScreenType: String {
        case iPhones_4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhones_X_XS = "iPhone X or iPhone XS"
        case iPhone_XR = "iPhone XR"
        case iPhone_XSMax = "iPhone XS Max"
        case unknown
    }
    
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhones_4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1792:
            return .iPhone_XR
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhones_X_XS
        case 2688:
            return .iPhone_XSMax
        default:
            return .unknown
        }
    }
    
    var hasNotch: Bool {
        var bottom: CGFloat = 0.0
        if #available(iOS 11.0, *) {
            bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        } else {
            // Fallback on earlier versions
        }
        return bottom > 0
    }
}

extension Dictionary {
    func safeValueForKey(_ key: Key) -> Value? {
        if self.contains(where: { $0.key == key }) {
            if let _ = self[key] as? NSNull {
                return nil
            }
            return self[key]
        } else {
            return nil
        }
    }
}

public extension Optional {
    func safelyWrappingString() -> String {
        switch self {
        case .some(let value):
            return String(describing: value)
        case _:
            return ""
        }
    }
    
    func unwrap<T>(_ any: T) -> Any {
        let mirror = Mirror(reflecting: any)
        guard mirror.displayStyle == .optional, let first = mirror.children.first else {
            return any
        }
        return unwrap(first.value)
    }
}

extension Array {
    func unique<T:Hashable>(map: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(map(value)) {
                set.insert(map(value))
                arrayOrdered.append(value)
            }
        }
        return arrayOrdered
    }
}

extension UIView {
    func fadeIn(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
        self.alpha = 0.0
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.isHidden = false
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in }) {
        self.alpha = 1.0
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }) { (completed) in
            self.isHidden = true
            completion(true)
        }
    }
    
    
    
    
    
    
    
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    
    
}

extension UIView {
    func animateTo(frame: CGRect, withDuration duration: TimeInterval, completion: ((Bool) -> Void)? = nil) {
        guard let _ = superview else {
            return
        }
        let xScale = frame.size.width / self.frame.size.width
        let yScale = frame.size.height / self.frame.size.height
        let x = frame.origin.x + (self.frame.width * xScale) * self.layer.anchorPoint.x
        let y = frame.origin.y + (self.frame.height * yScale) * self.layer.anchorPoint.y
        UIView.animate(withDuration: duration, delay: 0, options: .curveLinear, animations: {
            self.layer.position = CGPoint(x: x, y: y)
            self.transform = self.transform.scaledBy(x: xScale, y: yScale)
        }, completion: completion)
    }
}

class LBXScanLineAnimation: UIImageView {
    var isAnimationing = false
    var animationRect: CGRect = CGRect.zero
    func startAnimatingWithRect(animationRect: CGRect, parentView: UIView, image: UIImage?){
        self.image = image
        self.animationRect = animationRect
        parentView.addSubview(self)
        self.isHidden = false;
        isAnimationing = true;
        if image != nil {
            stepAnimation()
        }
    }
    
    @objc func stepAnimation() {
        if (!isAnimationing) {
            return;
        }
        var frame:CGRect = animationRect;
        let hImg = self.image!.size.height * animationRect.size.width / self.image!.size.width;
        frame.origin.y -= hImg;
        frame.size.height = hImg;
        self.frame = frame;
        self.alpha = 0.0;
        UIView.animate(withDuration: 1.4, animations: { () -> Void in
            self.alpha = 1.0;
            var frame = self.animationRect;
            let hImg = self.image!.size.height * self.animationRect.size.width / self.image!.size.width;
            frame.origin.y += (frame.size.height -  hImg);
            frame.size.height = hImg;
            self.frame = frame;
        }, completion:{ (value: Bool) -> Void in
            self.perform(#selector(LBXScanLineAnimation.stepAnimation), with: nil, afterDelay: 0.3)
        })
    }
    
    func stopStepAnimating() {
        self.isHidden = true;
        isAnimationing = false;
    }
    
    static public func instance()->LBXScanLineAnimation{
        return LBXScanLineAnimation()
    }
    
    deinit {
        //        print("LBXScanLineAnimation deinit")
        stopStepAnimating()
    }
    
}

public enum LBXScanViewAnimationStyle {
    case LineMove   //线条上下移动
    case NetGrid//网格
    case LineStill//线条停止在扫码区域中央
    case None    //无动画
}

public enum LBXScanViewPhotoframeAngleStyle {
    case Inner//内嵌，一般不显示矩形框情况下
    case Outer//外嵌,包围在矩形框的4个角
    case On   //在矩形框的4个角上，覆盖
}

public struct LBXScanViewStyle {
    // MARK: - -中心位置矩形框
    /// 是否需要绘制扫码矩形框，默认YES
    public var isNeedShowRetangle:Bool = true
    /**
     *  默认扫码区域为正方形，如果扫码区域不是正方形，设置宽高比
     */
    public var whRatio:CGFloat = 1.0
    /**
     @brief  矩形框(视频显示透明区)域向上移动偏移量，0表示扫码透明区域在当前视图中心位置，如果负值表示扫码区域下移
     */
    public var centerUpOffset:CGFloat = 44
    /**
     *  矩形框(视频显示透明区)域离界面左边及右边距离，默认60
     */
    public var xScanRetangleOffset:CGFloat = 60
    /**
     @brief  矩形框线条颜色，默认白色
     */
    public var colorRetangleLine = UIColor.white
    //MARK -矩形框(扫码区域)周围4个角
    /**
     @brief  扫码区域的4个角类型
     */
    public var photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle.Outer
    //4个角的颜色
    public var colorAngle = UIColor(red: 0.0, green: 167.0/255.0, blue: 231.0/255.0, alpha: 1.0)
    //扫码区域4个角的宽度和高度
    public var photoframeAngleW:CGFloat = 24.0
    public var photoframeAngleH:CGFloat = 24.0
    /**
     @brief  扫码区域4个角的线条宽度,默认6，建议8到4之间
     */
    public var photoframeLineW:CGFloat = 6
    //MARK: ----动画效果
    /**
     @brief  扫码动画效果:线条或网格
     */
    public var anmiationStyle = LBXScanViewAnimationStyle.LineMove
    /**
     *  动画效果的图像，如线条或网格的图像
     */
    public var animationImage:UIImage?
    // MARK: -非识别区域颜色,默认 RGBA (0,0,0,0.5)，范围（0--1）
    public var color_NotRecoginitonArea:UIColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5);
    
    public init() {
        
    }
}


extension UIColor {
    convenience init(hex:String) {
        let hexString:String = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        let scanner            = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}



extension UILabel{
    
    
    func amountAttributedString(){
        if let textString = self.text {
            let mmkfont:UIFont? = UIFont(name: appFont, size:10)
            let amountfont:UIFont? = UIFont(name: appFont, size:13)
            
            var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string:textString, attributes: [NSAttributedString.Key.font:amountfont ?? UIFont.systemFont(ofSize: 13)])
            myMutableString.addAttribute(NSAttributedString.Key.font,
                                         value: mmkfont ?? UIFont.systemFont(ofSize: 10),range: NSRange(location: myMutableString.length - 3,length: 3))
            self.attributedText = myMutableString
            
        }
    }
}

import UIKit


enum HomeNavigationActions {
    case Search,Barcode,Wishlist,ScanQR,Cart
}


@IBDesignable
class CardView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var VshadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = VshadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
}
extension String {
    var numberValue:NSNumber? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        return formatter.number(from: self)
    }
}

extension UIImageView {
    func ViewRoundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

class LogoAnimationView: UIView {
    
    let logoGifImageView = UIImageView(gifImage: try! UIImage(gifName: "new.gif"), loopCount: 1)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = UIColor(white: 246.0 / 255.0, alpha: 1)
        addSubview(logoGifImageView)
        logoGifImageView.translatesAutoresizingMaskIntoConstraints = false
        logoGifImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        logoGifImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        //        logoGifImageView.widthAnchor.constraint(equalToConstant: 280).isActive = true
        //        logoGifImageView.heightAnchor.constraint(equalToConstant: 108).isActive = true
    }
    
    
}

extension UIView {
    
    func pinEdgesToSuperView() {
        guard let superView = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: superView.topAnchor).isActive = true
        leftAnchor.constraint(equalTo: superView.leftAnchor).isActive = true
        bottomAnchor.constraint(equalTo: superView.bottomAnchor).isActive = true
        rightAnchor.constraint(equalTo: superView.rightAnchor).isActive = true
    }
    
}
//extension String {
//    func aesEncrypt(key: String) throws -> String {
//        var result = ""
//        do {
//            let key: [UInt8] = Array(key.utf8) as [UInt8]
//            // let aes = try! AES(key: key, blockMode: ECB() , padding:.pkcs7) // AES128 .ECB pkcs7
//
//            let daya = try! AES(key: key, blockMode: CBC(iv: Array("1234567890123456".utf8)), padding: .pkcs7)
//            let encrypted = try daya.encrypt(Array(self.utf8))
//            result = encrypted.toBase64()!
//            print("AES Encryption Result: \(result)")
//        } catch {
//            print("Error: \(error)")
//        }
//        return result
//    }
//
//
//}


extension String {
//    func aesEncrypt(key: String) throws -> String {
//        var result = ""
//        do {
//            let key: [UInt8] = Array(key.utf8) as [UInt8]
//           // let aes = try! AES(key: key, blockMode: ECB() , padding:.pkcs7) // AES128 .ECB pkcs7
//            
//            let daya = try! AES(key: key, blockMode: CBC(iv: Array("1234567890123456".utf8)), padding: .pkcs7)
//            let encrypted = try daya.encrypt(Array(self.utf8))
//            result = encrypted.toBase64()!
//            print("AES Encryption Result: \(result)")
//        } catch {
//            print("Error: \(error)")
//        }
//        return result
//    }
    
    
}



extension URL {
    var queryDictionary: [String: String]? {
        guard let query = self.query else { return nil}

        var queryStrings = [String: String]()
        for pair in query.components(separatedBy: "&") {

            let key = pair.components(separatedBy: "=")[0]

            let value = pair
                .components(separatedBy:"=")[1]
                .replacingOccurrences(of: "+", with: " ")
                .removingPercentEncoding ?? ""

            queryStrings[key] = value
        }
        return queryStrings
    }
}
