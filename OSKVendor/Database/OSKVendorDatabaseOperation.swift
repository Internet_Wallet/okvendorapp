//
//  VMDatabaseOperation.swift
//  VMart
//
//  Created by Kethan on 11/13/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import CoreData

class OSKVendorDatabaseOperation {
    
    static let sharedInstance = OSKVendorDatabaseOperation()
    let managedContext  =  appDelegate.persistentContainer.viewContext
    func insertUserProfileDetailsIntoDB(dict: VMTempModel) {
        /*
         */
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "UserProfileDetails")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchReq)
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
        }
        let profileInfo = UserProfileDetails(context: managedContext)
        self.insertIntoUserProfileDB(dataToInsert: dict, insertDataTo: profileInfo)
    }
    
    func insertIntoUserProfileDB(dataToInsert: VMTempModel, insertDataTo: UserProfileDetails) {
        insertDataTo.mobileNo     = dataToInsert.mobileNumber ?? ""
        insertDataTo.firstName         = dataToInsert.firstName ?? ""
        insertDataTo.simID        = dataToInsert.simid ?? ""
        insertDataTo.lastName   = dataToInsert.lastName ?? ""
        insertDataTo.facebook = dataToInsert.facebookId ?? ""
        if let date = dataToInsert.dateOfBirthDay, let month = dataToInsert.dateofBirthMonth, let year = dataToInsert.dateOfBirthYear {
            insertDataTo.dob          = "\(date)/\(month)/\(year)"
        } else {
            insertDataTo.dob          = ""
        }
        insertDataTo.profilePic    = dataToInsert.profilePictureUrl ?? ""
        insertDataTo.emailId = dataToInsert.email
        insertDataTo.country      = dataToInsert.country ?? ""
        insertDataTo.deviceId     = dataToInsert.deviceID
        insertDataTo.address1         = dataToInsert.address1 ?? ""
        insertDataTo.address2         = dataToInsert.address2 ?? ""
        insertDataTo.gender           = dataToInsert.gender ?? ""
        insertDataTo.state            = dataToInsert.state ?? ""
        insertDataTo.city         = dataToInsert.city ?? ""
        insertDataTo.displayAvatar = dataToInsert.displayAvatar ?? false
        insertDataTo.maritialStatus = dataToInsert.maritalStatus ?? Int16(0)
        insertDataTo.mode = dataToInsert.mode ?? ""
        insertDataTo.otherEmail = dataToInsert.otherEmail ?? ""
        insertDataTo.addressAdded = dataToInsert.addressAdded ?? false
        insertDataTo.token = dataToInsert.token ?? ""
        insertDataTo.password = dataToInsert.password ?? ""
        insertDataTo.shopName = dataToInsert.shopName ?? ""
        insertDataTo.houseNo = dataToInsert.houseNo ?? ""
        insertDataTo.floorNo = dataToInsert.floorNo ?? ""
        insertDataTo.roomNo = dataToInsert.roomNo ?? ""
        insertDataTo.versionCode = dataToInsert.versionCode ?? 0
        insertDataTo.customerId = dataToInsert.customerId ?? 0
        
        self.saveContext()
        Vendor_loginmodel.wrapModel()
        UserDefaults.standard.set(true, forKey: "LoginStatus")
        if let (n_Code, n_name) = appDelegate.getNetworkCode() as? (String, String){
            UserDefaults.standard.set(n_Code, forKey: "networkcode")
            UserDefaults.standard.set(n_name, forKey: "networkname")
        }
        
        //        UserModel.wrapFromDatabase()
    }
    
    func saveContext() {
        let managedContext  =  appDelegate.persistentContainer.viewContext
        do {
            try managedContext.save()
        } catch let error as NSError {
            println_debug("could not save :::: \(error)  ,,, \(error.description) ,,, \(error.userInfo)")
        }
    }
    
    //MARK: Fetch User Profile Data
    func fetchUserProfileDetailsRecord() -> UserProfileDetails? {
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "UserProfileDetails")
        do {
            guard let fetchedObjects    = try managedContext.fetch(fetchReq) as? [UserProfileDetails] else { return nil }
            if fetchedObjects.count > 0 {
                println_debug(fetchedObjects[0])
                return fetchedObjects[0]
            }
        }catch {
            //            println_debug("Fetching operation failed", error)
        }
        return nil
    }
}
