//
//  GlobalRightMenu.swift
//  NopCommerce
//
//  Created by BS-125 on 2/9/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit
import DropDown
//import MBProgressHUD


protocol MoreMenuDelegate{
    func reloadShoppingCart()
}

class GlobalRightMenu: NSObject {

    let dropDown = DropDown()
    var dropDownContainerView = UIView()
    var containingViewController = UIViewController()
    
    var delegate:MoreMenuDelegate?

    init (dropDownContainerView : UIView, containingViewController : UIViewController) {
        super.init()
        self.dropDown.selectionBackgroundColor = UIColor.white
        self.dropDownContainerView = dropDownContainerView
        self.containingViewController = containingViewController
        self.dropDown.anchorView = self.dropDownContainerView
        self.dropDown.bottomOffset = CGPoint(x: self.dropDownContainerView.frame.origin.x-225, y:self.dropDownContainerView.frame.origin.y-44)
        
        self.dropDown.selectionAction = {
            [unowned self] (index, item) in
             let defaults = UserDefaults.standard
            if (defaults.string(forKey: "accessToken") != nil) {
                if index == 0 {
                    AppUtility.showToast("You are Logged Out",view: self.containingViewController.view);
                    defaults.removeObject(forKey: "accessToken")
                    
                    if self.containingViewController.isKind(of: OSKBaseViewController.self) {
                        self.delegate?.reloadShoppingCart()
                    }
                    self.perform(#selector(GlobalRightMenu.reloadDropDownMenuList), with: nil, afterDelay:0)
                    self.perform(#selector(GlobalRightMenu.backToRootView), with: nil, afterDelay:2)
                } else if index == 1 {
                   
//                    if !self.containingViewController.isKind(of: MyAccountViewController.self) {
//                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//                        let myAccViewC = mainStoryBoard.instantiateViewController(withIdentifier: "MyAccountViewController") as! MyAccountViewController
//                        let centerNavVC = UINavigationController(rootViewController: myAccViewC)
//                        centerNavVC.navigationBar.isHidden = true
//                        containingViewController.panel!.center(centerNavVC, afterThat: {
//                            print("Executing block after changing center panelVC From Right Menu")
//                        })
////                        centerControllersObject.pushNewController(controller: myAccViewC)
//                    }
                } else if index == 2 {
//                    if !self.containingViewController.isKind(of: AddressViewController.self) {
//
//                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//                        let addressVC = mainStoryBoard.instantiateViewController(withIdentifier: "AddressViewController") as! AddressViewController
//                        let centerNavVC = UINavigationController(rootViewController: addressVC)
//                        centerNavVC.navigationBar.isHidden = true
//                        containingViewController.panel!.center(centerNavVC, afterThat: {
//                            print("Executing block after changing center panelVC From Right Menu")
//                        })
////                        centerControllersObject.pushNewController(controller: addressVC)
//                    }
                } else if index == 3 {
//                    if !self.containingViewController.isKind(of: OrdersViewController.self) {
//                        //let ordersViewC = OrdersViewController(nibName: "OrdersViewController", bundle: nil)
//                        //centerControllersObject.pushNewController(controller: ordersViewC)
//                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//                        let ordersViewC = mainStoryBoard.instantiateViewController(withIdentifier: "OrdersViewController") as! OrdersViewController
//                        let centerNavVC = UINavigationController(rootViewController: ordersViewC)
//                        centerNavVC.navigationBar.isHidden = true
//                        containingViewController.panel!.center(centerNavVC, afterThat: {
//                            print("Executing block after changing center panelVC From Right Menu")
//                        })
////                        centerControllersObject.pushNewController(controller: ordersViewC)
//                    }
                } else if index == 4 {
//                    if !self.containingViewController.isKind(of: WishListViewController.self) {
//                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//                        let wishListViewC = mainStoryBoard.instantiateViewController(withIdentifier: "WishListViewController") as! WishListViewController
//                        let centerNavVC = UINavigationController(rootViewController: wishListViewC)
//                        centerNavVC.navigationBar.isHidden = true
//                        containingViewController.panel!.center(centerNavVC, afterThat: {
//                            print("Executing block after changing center panelVC From Right Menu")
//                        })
//
//                        //self.containingViewController.navigationController?.pushViewController(wishListViewC, animated: true)
////                        centerControllersObject.pushNewController(controller: wishListViewC)
//
//                    }
                } else if index == 5 {
//                    if !self.containingViewController.isKind(of: SettingsViewController.self) {
//                        self.navigateToSettingsPage()
//                    }
                } else if index == 6 {
//                    if !self.containingViewController.isKind(of: ContactViewController.self) {
//                        self.navigateToAboutUsVCPage()
//                    }
                }
                else if index == 7 {
//                    if !self.containingViewController.isKind(of: ContactViewController.self) {
//                        self.navigateToContactUsPage()
//                    }
                }else if index == 8 {
//                    if self.containingViewController.isKind(of: ViewController.self) {
//                        self.navigateToPrivacyVCPage()
//                    }
                }
                    
                else if index == 9 {
                    if self.containingViewController.isKind(of: OSKBaseViewController.self) {
                        self.navigateToBarcodeReaderPage()
                    }else{
                        AppUtility.showToast("This feature is not applicable here.", view: self.containingViewController.view)
                    }
                }
            }
            else {
                if index == 5 {
//                    if !self.containingViewController.isKind(of: SettingsViewController.self) {
//                        self.navigateToSettingsPage()
//                    }
                } else if index == 6 {
//                    if !self.containingViewController.isKind(of: ContactViewController.self) {
//                        self.navigateToAboutUsVCPage()
//                    }
                }else if index == 7 {
//                    if !self.containingViewController.isKind(of: ContactViewController.self) {
//                        self.navigateToContactUsPage()
//                    }
                }
                else if index == 8 {
//                    if self.containingViewController.isKind(of: ViewController.self) {
//                        self.navigateToPrivacyVCPage()
//                    }
                }else if index == 9 {
//                    if self.containingViewController.isKind(of: ViewController.self) {
//                        self.navigateToBarcodeReaderPage()
//                    }else{
//                        AppUtility.showToast("This feature is not applicable here.", view: self.containingViewController.view)
//                    }
                }
                else {
//                    if !self.containingViewController.isKind(of: SignInViewController.self) {
//                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//                        let signInViewC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
//                        let centerNavVC = UINavigationController(rootViewController: signInViewC)
//                        centerNavVC.navigationBar.isHidden = true
//                        containingViewController.panel!.center(centerNavVC, afterThat: {
//                            print("Executing block after changing center panelVC From Right Menu")
//                        })
////                        centerControllersObject.pushNewController(controller: signInViewC)
//
//                    }
                }
            }
        }
    }
    
    func navigateToSettingsPage() {
    
//        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//        let settingsViewC = mainStoryBoard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
//        let centerNavVC = UINavigationController(rootViewController: settingsViewC)
//        centerNavVC.navigationBar.isHidden = true
//        containingViewController.panel!.center(centerNavVC, afterThat: {
//            print("Executing block after changing center panelVC From Right Menu")
//        })
//        centerControllersObject.pushNewController(controller: settingsViewC)
    }
    
    func navigateToBarcodeReaderPage() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "com.notification.BarcodeReader"), object: nil);
    }
    func navigateToAboutUsVCPage() {
//        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//        let aboutUsVC = mainStoryBoard.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
//        let centerNavVC = UINavigationController(rootViewController: aboutUsVC)
//        centerNavVC.navigationBar.isHidden = true
////        centerControllersObject.pushNewController(controller: centerNavVC)
//        containingViewController.panel!.center(centerNavVC, afterThat: {
//            print("Executing block after changing center panelVC From Right Menu")
//        })
    }
    func navigateToPrivacyVCPage() {
//        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//        let aboutUsVC = mainStoryBoard.instantiateViewController(withIdentifier: "PrivacyVC") as! PrivacyVC
//        let centerNavVC = UINavigationController(rootViewController: aboutUsVC)
//        centerNavVC.navigationBar.isHidden = true
////        centerControllersObject.pushNewController(controller: centerNavVC)
//
//        containingViewController.panel!.center(centerNavVC, afterThat: {
//            print("Executing block after changing center panelVC From Right Menu")
//        })
    }
    
    func navigateToContactUsPage() {
//        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//        let contactViewC = mainStoryBoard.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
//        let centerNavVC = UINavigationController(rootViewController: contactViewC)
//        centerNavVC.navigationBar.isHidden = true
//        containingViewController.panel!.center(centerNavVC, afterThat: {
//            print("Executing block after changing center panelVC From Right Menu")
//        })
        
//        centerControllersObject.pushNewController(controller: contactViewC)
    }
    @objc func backToRootView() {
        
//        if centerControllersObject.centerControllers.count > 1{
//         centerControllersObject.popTopRootViewController(controller: containingViewController)
//        }
    }
    @objc func reloadDropDownMenuList() {
//        dropDown.dataSource.removeAll()
//        let defaults = UserDefaults.standard
//        if let token = defaults.string(forKey: "accessToken") {
//            print("Token == \(token)")
//            dropDown.dataSource.append(NSLocalizedString("Sign Out".localized, comment: ""))
//        }
//        else {
//            dropDown.dataSource.append(NSLocalizedString("Sign In".localized, comment: ""))
//        }
//        dropDown.dataSource.append(NSLocalizedString("My Account".localized, comment: ""))
//        dropDown.dataSource.append(NSLocalizedString("Address".localized, comment: ""))
//        dropDown.dataSource.append(NSLocalizedString("My Orders".localized, comment: ""))
//        dropDown.dataSource.append(NSLocalizedString("Wishlist".localized, comment: ""))
//        dropDown.dataSource.append(NSLocalizedString("Settings".localized, comment: ""))
//        dropDown.dataSource.append(NSLocalizedString("About Us".localized, comment: ""))
//        dropDown.dataSource.append(NSLocalizedString("Contact Us".localized, comment: ""))
//        dropDown.dataSource.append(NSLocalizedString("Privacy Policy".localized, comment: ""))
//        dropDown.dataSource.append(NSLocalizedString("Bar Code Read".localized, comment: ""))
    }
}
