//
//  APIManagerClient.swift
//  NopCommerce
//
//  Created by BS-125 on 11/5/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Foundation

class APIManagerClient: NSObject {
    
    var shoppingCartCount               = 0
    var AppStartUp : AppStartUpModel?   = nil
    var isRequestedToReloadOnlyOnce: Bool = false
    var versioncode = 1
    var DeviceTypeId = 5 // FOR IOS 5
    var medicineRequestId = 0 // 0 for create new request
    var allCategories : AllCategory?
    
    // Mohit
    enum UrlTypes {
        case productionUrl, testUrl, stagingUrl
    }
    
    let serverUrl: UrlTypes = .testUrl
    var OKPayment = ""
    var base_url                         = ""
    var base_url_sendSMS                   = ""
    var NST_KEY_Value       = ""
    var NST_SECRET_Value    = ""
    var secretKey = ""
    var productionMode = false
    var merchantID = ""
    var currencyCode = "104"
    
    func urlEnable(urlTo: UrlTypes) {
        switch urlTo {
        case .testUrl:
            base_url                         = "http://69.160.4.149:1009/api"
            base_url_sendSMS                 = "http://69.160.4.149:1009/api/customer/VerifyMobileNumber"
            NST_KEY_Value                    = "bm9wU3RhdGlvblRva2Vu"
            NST_SECRET_Value                 = "bm5xS6V5"
        case .stagingUrl:
            base_url                         = "http://69.160.4.149:1009/api"
            base_url_sendSMS                 = "http://13.250.225.91/api/vendor/VerifyMobileNumber"
       
            NST_KEY_Value                    = "bm9wU3RhdGlvblRva2Vu"
            NST_SECRET_Value                 = "bm5xS6V5"
        case .productionUrl:
            base_url                         = "http://69.160.4.149:1009/api"
            base_url_sendSMS                 = "https://onestop-kitchen.com/api/customer/VerifyMobileNumber"
           
            NST_KEY_Value                    = "bm9wU3RhdGlvblRva2Vu"
            NST_SECRET_Value                 = "bm5xS6V5"
            
        }
    }
    
    var baseURL                         = "http://69.160.4.149:1009/api"
    var defaultBaseUrl                  = "http://69.160.4.149:1009/api"
    
    class var sharedInstance:APIManagerClient {
        return Static.instance
    }
    private struct Static {
        static let instance = Static.createInstance()
        static func createInstance()->APIManagerClient {
            
            let myInstance = APIManagerClient()
            return myInstance
        }
    }
    override init() {
        
    }
    
    
   
  
}

