//
//  APIManager.swift
//  NopCommerce
//
//  Created by BS-125 on 11/4/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON
import ObjectMapper
import JWT

open class APIManager: NSObject {
    
    var baseURL                         = APIManagerClient.sharedInstance.baseURL
    var NST_KEY_Value                   = APIManagerClient.sharedInstance.NST_KEY_Value
    var NST_SECRET_Value                = APIManagerClient.sharedInstance.NST_SECRET_Value
    var baseURL1                        = APIManagerClient.sharedInstance.base_url
    var barcodeResponse :                NSNumber?
    var nstKEY =  JWT.encode(claims: ["NST_KEY": "\(APIManagerClient.sharedInstance.NST_KEY_Value)"], algorithm: .hs512("\(APIManagerClient.sharedInstance.NST_SECRET_Value)".data(using: .utf8)!))
    let deviceId                        = UIDevice.current.identifierForVendor?.uuidString ?? ""
    let headers                         = ["DeviceId": UIDevice.current.identifierForVendor?.uuidString ?? ""]
 
    
    func getHeaderDic() -> [String:String] {
        Alamofire.SessionManager.default.session.configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        println_debug("\nDevice id : \(self.deviceId)\n")
        let manager = APIManagerClient.sharedInstance
        let key = manager.NST_KEY_Value
        let secret = manager.NST_SECRET_Value
        print("APP Key : \(key) And Secret : \(secret)")
        let encodedString = JWT.encode(claims: ["NST_KEY": "\(key)"], algorithm: .hs512("\(secret)".data(using: .utf8)!))
        var headerDictionary = self.headers
        if let token = Vendor_loginmodel.shared.token, token.count > 0 {
            headerDictionary = ["Token":token,"DeviceId": self.deviceId,"NST":encodedString]
            println_debug("Header bb : \(headerDictionary)\n")
        } else {
            headerDictionary = ["DeviceId": self.deviceId,"NST":encodedString]
            println_debug("Without Access Token of Header : \(headerDictionary)\n")
        }
        return headerDictionary
    }
    
    func displayErrorMessage(json: JSON) -> String {
        var errorMessage = ""
        let errorList = json["ErrorList"].arrayObject
        if errorList != nil{
            for error in errorList! {
                errorMessage += "\(error as! String)\n"
            }
        }
        return errorMessage
    }
    
    // MARK: ProductCartCount
    open func getProductCartCount(onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@vendor/productcount",  APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                APIManagerClient.sharedInstance.shoppingCartCount = json["Count"].intValue
                successBlock()
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    // MARK: orderCount
    open func getOrderCount(onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@vendor/ordercount",  APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                APIManagerClient.sharedInstance.shoppingCartCount = json["Count"].intValue
                successBlock()
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    // MARK: Dashboard
    open func getDashboardDetail(onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/vendor/dashboard",  APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                APIManagerClient.sharedInstance.shoppingCartCount = json["Count"].intValue
                successBlock()
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: All Categories
    open func getAllCategories(onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //%@/vendor/allcategories
        let urlParameters = String(format: "%@/v1/categories",  APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                APIManagerClient.sharedInstance.allCategories = Mapper<AllCategory>().map(JSONObject: json.dictionaryObject)
                successBlock()
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: ShoppingCartCount
    open func SaveNotifyRequest(_ productId: String,onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "/product/SaveNotifyRequest/%@",  APIManagerClient.sharedInstance.base_url,productId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {  response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                print(json)
                successBlock()
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    open func ScanBarcodeId(_ productId: Int,  onSuccess successBlock: @escaping ((JSON) -> Void), onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/product/GetProductIdbyBarcode/\(productId)", APIManagerClient.sharedInstance.base_url ) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {   response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                successBlock(json)
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    // MARK: Get languages from server
    func getLanguagesFromServer(onSuccess successBlock: ((GetLanguageModel) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/GetLanguage", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/GetLanguage", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")

                if let languageModelObj  = Mapper<GetLanguageModel>().map(JSONObject: json.dictionaryObject){
                    println_debug("\nPlay")
                    successBlock(languageModelObj)
                }else{
                    errorBlock("Could not print JSON")
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: Set languages In Server
    func setLanguageInServer(languageId : Int, onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!){
        //        let urlParameters = String(format: "%@/SetLanguage/%d",baseURL, languageId) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
        let urlParameters = String(format: "%@/SetLanguage/%d",APIManagerClient.sharedInstance.base_url, languageId) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                }else{
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    func convertToArrDictionary(text: String) -> [AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [AnyObject]
            } catch {
                println_debug(error.localizedDescription)
            }
        }
        return nil
    }
    
    //MARK: RegisterDeviceId
    open  func registerDeviceId(_ deviceId: String , EmailID : String , onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        let urlParameters = String(format: "%@/vendor/AppStart", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: ["DeviceTypeId":"5","SubscriptionId":deviceId ,"EmailAddress":EmailID ,"DeviceId":self.deviceId] , encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                if json["StatusCode"].int == 200 {
                    if let dic = Mapper<AppStartUpModel>().map(JSONObject: json["Data"].dictionaryObject){
                        APIManagerClient.sharedInstance.AppStartUp = dic
                    }
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    //MARK: Create Product
    open  func createProduct( productDetails : [String : Any] , onSuccess successBlock: (([String : Any]) -> Void)!, onError errorBlock: ((String?) -> Void)!) {
        
            let urlParameters = String(format: "%@/vendor/createproductfromvendor", APIManagerClient.sharedInstance.base_url) as String
            println_debug(urlParameters)
            println_debug(productDetails)
            // target dict
            var params = [String: String]()
            for (key, value) in productDetails {
                if let value = value as? String {
                    params[key] = value
                }
            }
            let request = NSMutableURLRequest(url:URL(string: urlParameters)!)
            request.httpMethod = "POST";
            let boundary = "Boundary-\(UUID2)"
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            var body = ""
            for ( key , value ) in params {
                let paramName = key
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                let paramValue = value as! String
                  body += "\r\n\r\n\(paramValue)\r\n"
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)
            request.httpBody = postData
            let headerData = self.getHeaderDic()
            for (key , Value ) in headerData {
                request.setValue(Value, forHTTPHeaderField:key)
            }
            let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                DispatchQueue.main.async {
                    if error != nil {
                        errorBlock("\(String(describing: error?.localizedDescription))")
                        return
                    }
                    println_debug(response)
                    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                      let json = JSON(data)
                        println_debug(json)
                        println_debug("\n")
                        if json["StatusCode"].int == 200 {
                         if let dic = json["Data"].dictionaryObject {
                                successBlock(dic)
                            }
                        } else {
                            errorBlock(self.displayErrorMessage(json: json))
                        }
                    }
                    else {
                        errorBlock(AppConstants.InternetErrorText.messageServerError)
                    }
                }
             }
             task.resume()
       }
    
    //MARK: SignInAVendor
    
    open func webCallWithbodyParmas(urlStr : String, params:[String: Any], onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        
        let url1 =  URL(string: urlStr)!
        println_debug(params)
        println_debug(url1)
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url1)
        request.timeoutInterval = 90
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = self.getHeaderDic()
        request.httpMethod = "POST"
       // request.setValue("application/json", forHTTPHeaderField: "Accept")
     
        let theJSONData  = try? JSONSerialization.data(withJSONObject: params , options: [])
        let jsonString   = String(data: theJSONData!,encoding: .utf8)
        let jsonData =  jsonString!.data(using: .utf8)
        request.httpBody =  jsonData
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            
            if let errorResponse = error {
                println_debug(errorResponse)
            }else {
                if let httpStatus = response as? HTTPURLResponse {
                    if httpStatus.statusCode == 200 {
                        if let json = data {
                            do {
                                let dict = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
                                if let dataDict = dict as? Dictionary<String,AnyObject> {
                                    println_debug(dataDict)
                                    if let success = dataDict["StatusCode"] as? NSNumber{
                                        if success == 200 {
                                            successBlock(dataDict)
                                        }
                                        else{
                                            successBlock(dataDict)
                                        }
                                    }
                                }
                            } catch {}
                        }
                    }else {
                        println_debug(httpStatus)
                        errorBlock("\(error?.localizedDescription ?? "")")
                    }
                }else {
                    println_debug("Fail")
                    errorBlock("\(error?.localizedDescription ?? "")")
                }
            }
        }
        dataTask.resume()
    }
    
    open func LoginInUser(_ params:[String: AnyObject], onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        
        let url1 =  URL(string: "\(APIManagerClient.sharedInstance.base_url)/vendor/v1/login")!
        println_debug(params)
        println_debug(url1)
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url1)
        request.timeoutInterval = 90
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        
        let theJSONData  = try? JSONSerialization.data(withJSONObject: params , options: .prettyPrinted)
        let jsonString   = NSString(data: theJSONData!,encoding: String.Encoding.utf8.rawValue)
        
        request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue)
        request.allHTTPHeaderFields = self.getHeaderDic()
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            
            if let errorResponse = error {
                println_debug(errorResponse)
            }else {
                if let httpStatus = response as? HTTPURLResponse {
                    if httpStatus.statusCode == 200 {
                        if let json = data {
                            do {
                                let dict = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
                                if let dataDict = dict as? Dictionary<String,AnyObject> {
                                    println_debug(dataDict)
                                    if let success = dataDict["StatusCode"] as? NSNumber{
                                        if success == 200 {
                                            successBlock(dataDict)
                                        }
                                        else{
                                            successBlock(dataDict)
                                        }
                                    }
                                }
                            } catch {}
                        }
                    }else {
                        println_debug(httpStatus)
                        errorBlock("\(error?.localizedDescription ?? "")")
                    }
                }else {
                    println_debug("Fail")
                    errorBlock("\(error?.localizedDescription ?? "")")
                }
            }
        }
        dataTask.resume()
    }
    //MARK: getDevisionDetailsForCountryID
    open func getDevisionInfoForCountry( countryID : String, onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/customer/info", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/country/getstatesbycountryid/\(countryID)", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock(json.dictionaryObject! as [String : AnyObject])
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: getTownshipDetailsForCountryID
    open func getTownshipInfoForDevision(devisionID : String , onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/customer/info", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/country/getcitiesbystateid/\(devisionID)", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock(json.dictionaryObject! as [String : AnyObject])
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: getMarketBasedOnTownship
    open func getMarketInfoForTownship(TownshipID : String , onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/customer/info", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/vendor/getcitywisebusinesslocation/\(TownshipID)", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock(json.dictionaryObject! as [String : AnyObject])
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: getAboutAppList
    open func getAboutAPPList(onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/customer/info", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/vendor/getlistheadfrommaster/", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock(json.dictionaryObject! as [String : AnyObject])
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    
    //MARK: RegisterAboutApp
    open func RegisterAboutUser(id: Int , mobilenumber: String , onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
         
         let url1 =  URL(string: "\(APIManagerClient.sharedInstance.base_url)/vendor/inserthearaboutus/\(id)/\(mobilenumber)")!
        // println_debug(params)
         println_debug(url1)
         
         let session             = URLSession.shared
         let request             = NSMutableURLRequest(url:url1)
         request.timeoutInterval = 90
         request.addValue("application/json", forHTTPHeaderField: "Content-Type")
         //request.setValue("application/json", forHTTPHeaderField: "Accept")
         request.httpMethod = "POST"
         
         let theJSONData  = try? JSONSerialization.data(withJSONObject: "" , options: .prettyPrinted)
         let jsonString   = NSString(data: theJSONData!,encoding: String.Encoding.utf8.rawValue)
         
         request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue)
         request.allHTTPHeaderFields = self.getHeaderDic()
         
         let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
             
             if let errorResponse = error {
                 println_debug(errorResponse)
             }else {
                 if let httpStatus = response as? HTTPURLResponse {
                     if httpStatus.statusCode == 200 {
                         if let json = data {
                             do {
                                 let dict = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
                                 if let dataDict = dict as? Dictionary<String,AnyObject> {
                                     println_debug(dataDict)
                                     if let success = dataDict["StatusCode"] as? NSNumber{
                                         if success == 200 {
                                             successBlock(dataDict)
                                         }
                                         else{
                                             successBlock(dataDict)
                                         }
                                     }
                                 }
                             } catch {}
                         }
                     }else {
                         println_debug(httpStatus)
                         errorBlock("\(error?.localizedDescription ?? "")")
                     }
                 }else {
                     println_debug("Fail")
                     errorBlock("\(error?.localizedDescription ?? "")")
                 }
             }
         }
         dataTask.resume()
     }
    
    
    
    open func AboutApppostAPIParams(id: Int , mobilenumber: String, url : String, productDetails : [String : Any] ,onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        
            let urlParameters = String(format: "%@/vendor/inserthearaboutus/\(id)/\(mobilenumber)", APIManagerClient.sharedInstance.base_url) as String
            println_debug(urlParameters)
            println_debug(productDetails)
            // target dict
            var params = [String: String]()
            for (key, value) in productDetails {
                if let value = value as? String {
                    params[key] = value
                }
            }
            let request = NSMutableURLRequest(url:URL(string: urlParameters)!)
            request.httpMethod = "POST";
            let boundary = "Boundary-\(UUID2)"
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            var body = ""
            for ( key , value ) in params {
                let paramName = key
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                let paramValue = value
                  body += "\r\n\r\n\(paramValue)\r\n"
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)
            request.httpBody = postData
            let headerData = self.getHeaderDic()
            for (key , Value ) in headerData {
                request.setValue(Value, forHTTPHeaderField:key)
            }
            let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                DispatchQueue.main.async {
                    if error != nil {
                      //  errorBlock(true)
                        return
                    }
                    println_debug(response)
                    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                      let json = JSON(data)
                        if let json = data {
                            do {
                                let dict = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
                                if let dataDict = dict as? Dictionary<String,AnyObject> {
                                    println_debug(dataDict)
                                    if let success = dataDict["StatusCode"] as? NSNumber{
                                        if success == 200 {
                                            successBlock(dataDict)
                                        }
                                        else{
                                            successBlock(dataDict)
                                        }
                                    }
                                }
                            } catch {}
                        }else {
                      //  errorBlock(true)
                    }
                    }else {
                      //  errorBlock(true)
                    }
                }
             }
             task.resume()
       }
    
    
    
    
    
   //MARK: RegisterAVendor
    open func RegisterUser(_ params:[String: AnyObject], onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        
        let url1 =  URL(string: "\(APIManagerClient.sharedInstance.base_url)/vendor/v1/registervendorapi")!
        println_debug(params)
        println_debug(url1)
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url1)
        request.timeoutInterval = 90
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        
        let theJSONData  = try? JSONSerialization.data(withJSONObject: params , options: .prettyPrinted)
        let jsonString   = NSString(data: theJSONData!,encoding: String.Encoding.utf8.rawValue)
        
        request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue)
        request.allHTTPHeaderFields = self.getHeaderDic()
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            
            if let errorResponse = error {
                println_debug(errorResponse)
            }else {
                if let httpStatus = response as? HTTPURLResponse {
                    if httpStatus.statusCode == 200 {
                        if let json = data {
                            do {
                                let dict = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
                                if let dataDict = dict as? Dictionary<String,AnyObject> {
                                    println_debug(dataDict)
                                    if let success = dataDict["StatusCode"] as? NSNumber{
                                        if success == 200 {
                                            successBlock(dataDict)
                                        }
                                        else{
                                            successBlock(dataDict)
                                        }
                                    }
                                }
                            } catch {}
                        }
                    }else {
                        println_debug(httpStatus)
                        errorBlock("\(error?.localizedDescription ?? "")")
                    }
                }else {
                    println_debug("Fail")
                    errorBlock("\(error?.localizedDescription ?? "")")
                }
            }
        }
        dataTask.resume()
    }
    
    //MARK: UpdateProfile
    open func UpdateProfile(_ params:[String: AnyObject], onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        
        let url1 =  URL(string: "\(APIManagerClient.sharedInstance.base_url)/vendor/editvendorprofile")!
        println_debug(params)
        println_debug(url1)
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url1)
        request.timeoutInterval = 90
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        
        let theJSONData  = try? JSONSerialization.data(withJSONObject: params , options: .prettyPrinted)
        let jsonString   = NSString(data: theJSONData!,encoding: String.Encoding.utf8.rawValue)
        
        request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue)
        request.allHTTPHeaderFields = self.getHeaderDic()
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            
            if let errorResponse = error {
                println_debug(errorResponse)
            }else {
                if let httpStatus = response as? HTTPURLResponse {
                    if httpStatus.statusCode == 200 {
                        if let json = data {
                            do {
                                let dict = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
                                if let dataDict = dict as? Dictionary<String,AnyObject> {
                                    println_debug(dataDict)
                                    if let success = dataDict["StatusCode"] as? NSNumber{
                                        if success == 200 {
                                            successBlock(dataDict)
                                        }
                                        else{
                                            successBlock(dataDict)
                                        }
                                    }
                                }
                            } catch {}
                        }
                    }else {
                        println_debug(httpStatus)
                        errorBlock("\(error?.localizedDescription ?? "")")
                    }
                }else {
                    println_debug("Fail")
                    errorBlock("\(error?.localizedDescription ?? "")")
                }
            }
        }
        dataTask.resume()
    }
    
    //MARK: ChangePasswordAPI
     open func ChangePassword(_ params:[String: String], onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
         
        let url1 =  URL(string: "\(APIManagerClient.sharedInstance.base_url)/vendor/changepassword)")!
         println_debug(params)
         println_debug(url1)
         
         let session             = URLSession.shared
         let request             = NSMutableURLRequest(url:url1)
         request.timeoutInterval = 90
         request.addValue("application/json", forHTTPHeaderField: "Content-Type")
         //request.setValue("application/json", forHTTPHeaderField: "Accept")
         request.httpMethod = "POST"
         
//         let theJSONData  = try? JSONSerialization.data(withJSONObject: params , options: .prettyPrinted)
//         let jsonString   = NSString(data: theJSONData!,encoding: String.Encoding.utf8.rawValue)
         
         //request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue)
         request.allHTTPHeaderFields = self.getHeaderDic()
         
         let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
             
             if let errorResponse = error {
                 println_debug(errorResponse)
             }else {
                 if let httpStatus = response as? HTTPURLResponse {
                     if httpStatus.statusCode == 200 {
                         if let json = data {
                             do {
                                 let dict = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
                                 if let dataDict = dict as? Dictionary<String,AnyObject> {
                                     println_debug(dataDict)
                                     if let success = dataDict["StatusCode"] as? NSNumber{
                                         if success == 200 {
                                             successBlock(dataDict)
                                         }
                                         else{
                                             successBlock(dataDict)
                                         }
                                     }
                                     
                                     
                                 }
                             } catch {}
                         }
                     }else {
                         println_debug(httpStatus)
                         errorBlock("\(error?.localizedDescription ?? "")")
                     }
                 }else {
                     println_debug("Fail")
                     errorBlock("\(error?.localizedDescription ?? "")")
                 }
             }
         }
         dataTask.resume()
     }
    
    
    //MARK: ForgotPasswordAPI
     open func ForgotUser(_ params:[String: String], onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
         
        let url1 =  URL(string: "\(APIManagerClient.sharedInstance.base_url)/vendor/forgotpassword/\(params["Username"] ?? "")")!
         println_debug(params)
         println_debug(url1)
         
         let session             = URLSession.shared
         let request             = NSMutableURLRequest(url:url1)
         request.timeoutInterval = 90
         request.addValue("application/json", forHTTPHeaderField: "Content-Type")
         //request.setValue("application/json", forHTTPHeaderField: "Accept")
         request.httpMethod = "GET"
         
//         let theJSONData  = try? JSONSerialization.data(withJSONObject: params , options: .prettyPrinted)
//         let jsonString   = NSString(data: theJSONData!,encoding: String.Encoding.utf8.rawValue)
         
         //request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue)
         request.allHTTPHeaderFields = self.getHeaderDic()
         
         let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
             
             if let errorResponse = error {
                 println_debug(errorResponse)
             }else {
                 if let httpStatus = response as? HTTPURLResponse {
                     if httpStatus.statusCode == 200 {
                         if let json = data {
                             do {
                                 let dict = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
                                 if let dataDict = dict as? Dictionary<String,AnyObject> {
                                     println_debug(dataDict)
                                     if let success = dataDict["StatusCode"] as? NSNumber{
                                         if success == 200 {
                                             successBlock(dataDict)
                                         }
                                         else{
                                             successBlock(dataDict)
                                         }
                                     }
                                     
                                     
                                 }
                             } catch {}
                         }
                     }else {
                         println_debug(httpStatus)
                         errorBlock("\(error?.localizedDescription ?? "")")
                     }
                 }else {
                     println_debug("Fail")
                     errorBlock("\(error?.localizedDescription ?? "")")
                 }
             }
         }
         dataTask.resume()
     }
    
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    //MARK: Customer Info
    open func loadCustomerInfo(onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        let urlParameters = String(format: "%@/customer/info", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock(json.dictionaryObject! as [String : AnyObject])
                } else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    open func getGenericWebCall(urlString: String ,onSuccess successBlock:((Any) -> Void)!, onError errorBlock:((String?) -> Void)!) {
         let urlParameters = urlString
         Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
             response in
             switch response.result {
             case .success(let data):
                 let json = JSON(data)
                 if json["StatusCode"].int == 200 {
                    successBlock(response.data)
                 } else if json["Message"].string == "Success." {
                   successBlock(response.data)
                }
                 else if json["Message"].string == "Barcode not found" {
                  successBlock(response.data)
               }
                 else {
                     errorBlock(self.displayErrorMessage(json: json))
                 }
             case .failure(let error):
                 errorBlock("\(error.localizedDescription)")
             }
         }
     }
    
    open func postGenericWebCall(urlString: String ,onSuccess successBlock:((Any) -> Void)!, onError errorBlock:((String?) -> Void)!) {
         let urlParameters = urlString
         Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
             response in
             switch response.result {
             case .success(let data):
                 let json = JSON(data)
                 if json["StatusCode"].int == 200 {
                    successBlock(response.data)
                 } else {
                     errorBlock(self.displayErrorMessage(json: json))
                 }
             case .failure(let error):
                 errorBlock("\(error.localizedDescription)")
             }
         }
     }
    
    
    open func postAPIParams(url : String, productDetails : [String : Any] ,successBlock: @escaping ((_ data: Data, _ success: Bool) -> Void), errorBlock: @escaping ((_ fail: Bool) -> Void)) {
        
            let urlParameters = String(format: "%@/vendor/updateproductfromvendor", APIManagerClient.sharedInstance.base_url) as String
            println_debug(urlParameters)
            println_debug(productDetails)
            // target dict
            var params = [String: String]()
            for (key, value) in productDetails {
                if let value = value as? String {
                    params[key] = value
                }
            }
            let request = NSMutableURLRequest(url:URL(string: urlParameters)!)
            request.httpMethod = "POST";
            let boundary = "Boundary-\(UUID2)"
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            var body = ""
            for ( key , value ) in params {
                let paramName = key
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                let paramValue = value
                  body += "\r\n\r\n\(paramValue)\r\n"
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)
            request.httpBody = postData
            let headerData = self.getHeaderDic()
            for (key , Value ) in headerData {
                request.setValue(Value, forHTTPHeaderField:key)
            }
            let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                DispatchQueue.main.async {
                    if error != nil {
                        errorBlock(true)
                        return
                    }
                    println_debug(response)
                    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                      let json = JSON(data)
                    if json["StatusCode"].int == 200 {
                        if let dt = data {
                                successBlock(dt,true)
                        }
                    }else {
                        errorBlock(true)
                    }
                    }else {
                        errorBlock(true)
                    }
                }
             }
             task.resume()
       }
    
    // MARK: Delete Product From Server
    func deleteProduct(productID : Int, onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String?) -> Void)!){
        
        let urlParameters = String(format: "%@/vendor/deleteproductfromvendor/%d",APIManagerClient.sharedInstance.base_url, productID) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                }else{
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK: Customer Information Edit
    open func saveCustomerInfo(_ params:[String: AnyObject], onSuccess successBlock:(() -> Void)!, onError errorBlock:((String?) -> Void)!) {
        //        let urlParameters = String(format: "%@/customer/info", baseURL) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/customer/info", APIManagerClient.sharedInstance.base_url) as String
        Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock()
                }  else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //MARK:  get Product Images
       open func getProductImages(_ params:[String: AnyObject], onSuccess successBlock:(() -> Void)!, onError errorBlock:((String?) -> Void)!) {
           let urlParameters = String(format: "/vendor/getproductpicturelist/%@", APIManagerClient.sharedInstance.base_url) as String
           Alamofire.request(URL(string: urlParameters)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
               response in
               switch response.result {
               case .success(let data):
                   let json = JSON(data)
                   if json["StatusCode"].int == 200 {
                       successBlock()
                   }  else {
                       errorBlock(self.displayErrorMessage(json: json))
                   }
               case .failure(let error):
                   errorBlock("\(error.localizedDescription)")
               }
           }
       }
    
    
    //MARK: Add Attributes
    
    open func addAttributes(params:[String: AnyObject], successBlock:((Any) -> Void)!,errorBlock:((String?) -> Void)!) {
        
        Alamofire.request(URL(string: URLConstant.updateProductAttributes())!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                if json["StatusCode"].int == 200 {
                    successBlock(json)
                }  else {
                    errorBlock(self.displayErrorMessage(json: json))
                }
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }

    //MARK: Get ProductDetails
    open func getAllProductsInformation(pageNo : String, size : String = "50" , onSuccess successBlock:((ProductDetailsInfo) -> Void)!, onError errorBlock:((String?) -> Void)!) {
        let urlSubString = "vendor/productdetails?pageNumber=\(pageNo)&pageSize=\(size)"
        
        //        let urlParameters = String(format: "%@/%@/%d", baseURL, urlSubString, categoryId) as String
        //        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: params, encoding: URLEncoding.default, headers: self.getHeaderDic()).responseJSON { //(response) -> Void in
        let urlParameters = String(format: "%@/%@", APIManagerClient.sharedInstance.base_url, urlSubString) as String
        Alamofire.request(URL(string: urlParameters)!, method: .get, parameters: [:], encoding: URLEncoding.default, headers: self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                println_debug(json)
                println_debug("\n")
                let productdetailsinfo = ProductDetailsInfo ()
                //                categoryDetailsInfo.fromPrice = json["PriceRange"]["From"].double
                //                categoryDetailsInfo.toPrice = json["PriceRange"]["To"].double
                productdetailsinfo.TotalPages = json["TotalPages"].int ?? 1
                
                if let resData = json["Products"].arrayObject {
                    print(resData)
                    for productDictionary in resData {
                        let products = ProductsInfo(JSON: productDictionary as! [String : AnyObject] as AnyObject)
                        productdetailsinfo.productsInfo.append(products)
                    }
                }
                if let resData = json["PriceRange"].dictionaryObject {
                    if let defaultMinValue = UserDefaults.standard.value(forKey: "min") as? String , defaultMinValue != "" {
                        guard let minVal = String(format: "%@", UserDefaults.standard.value(forKey: "min") as! CVarArg).toDouble() as? Double else { return }
                        let minValueNew = json["PriceRange"]["From"].doubleValue
                        if minVal > minValueNew {
                                                    productdetailsinfo.fromPrice = json["PriceRange"]["From"].double
                                                    UserDefaults.standard.set(json["PriceRange"]["From"].stringValue, forKey: "min")
                                                    UserDefaults.standard.synchronize()
                                                    let notFilteredItem = PriceRange(JSON: resData as [String : AnyObject] as AnyObject)
                                                    productdetailsinfo.PriceRange.append(notFilteredItem)
                       }
                        else {
                            productdetailsinfo.fromPrice = minVal
                        }
                    }
                    if let defaultMaxValue = UserDefaults.standard.value(forKey: "max") as? String , defaultMaxValue != "" {
                        guard let maxVal = String(format: "%@", UserDefaults.standard.value(forKey: "max") as! CVarArg).toDouble() as? Double else { return }
                        let maxValueNew = json["PriceRange"]["To"].doubleValue
                                     if maxVal < maxValueNew {
                                                    productdetailsinfo.toPrice = json["PriceRange"]["To"].double
                                                    UserDefaults.standard.set(json["PriceRange"]["To"].stringValue, forKey: "max")
                                                    UserDefaults.standard.synchronize()
                                                    let notFilteredItem = PriceRange(JSON: resData as [String : AnyObject] as AnyObject)
                                                    productdetailsinfo.PriceRange.append(notFilteredItem)
                                            }
                                     else {
                                        productdetailsinfo.toPrice = maxVal
                                      }
                         }
                    else {
                        productdetailsinfo.fromPrice = json["PriceRange"]["From"].double
                        productdetailsinfo.toPrice = json["PriceRange"]["To"].double
                        if productdetailsinfo.toPrice != nil {
                            UserDefaults.standard.set(json["PriceRange"]["From"].stringValue, forKey: "min")
                            UserDefaults.standard.set(json["PriceRange"]["To"].stringValue, forKey: "max")
                            UserDefaults.standard.synchronize()
                            let notFilteredItem = PriceRange(JSON: resData as [String : AnyObject] as AnyObject)
                            productdetailsinfo.PriceRange.append(notFilteredItem)
                        }
                    }
                }
                if let resData = json["NotFilteredItems"].arrayObject {
                    for notFilteredItemDictionary in resData {
                        let notFilteredItem = NotFilteredItems(JSON: notFilteredItemDictionary as! [String : AnyObject] as AnyObject)
                        productdetailsinfo.notFilteredItems.append(notFilteredItem)
                    }
                }
                if let resData = json["AlreadyFilteredItems"].arrayObject {
                    for alreadyFilteredItemDictionary in resData {
                        let alreadyFilteredItem = NotFilteredItems(JSON: alreadyFilteredItemDictionary as! [String : AnyObject] as AnyObject)
                        productdetailsinfo.alreadyFilteredItems.append(alreadyFilteredItem)
                    }
                }
                if let resData = json["AvailableSortOptions"].arrayObject {
                    for availableSortOptionDictionary in resData {
                        let availableSortOptionItem = AvailableSortOptions(JSON: availableSortOptionDictionary as! [String : AnyObject] as AnyObject)
                        productdetailsinfo.availableSortOptions.append(availableSortOptionItem)
                    }
                }
                successBlock(productdetailsinfo)
            case .failure(let error):
                errorBlock("\(error.localizedDescription)")
            }
        }
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    //MARK:- Webapi manager
    public func genericClass(url: URL, param: AnyObject, httpMethod: String, header: Bool? = false, addAddress: String? = "", hbData: Data? = nil, handle :@escaping (_ result: Any, _ success: Bool, _ data: Data?) -> Void) {
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        println_debug(url)
        
        if httpMethod == "POST" {
            if let stringParams = param as? String {
                let postLength = NSString(format:"%lu", stringParams.count) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = stringParams.data(using: String.Encoding.utf8, allowLossyConversion:true)
            } else {
                let theJSONData = try? JSONSerialization.data(
                    withJSONObject: param ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                let jsonString          = NSString(data: theJSONData!,encoding: String.Encoding.ascii.rawValue)
                let postLength = NSString(format:"%lu", jsonString!.length) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:false)
                if let httpBodyData = hbData {
                    request.httpBody = httpBodyData
                }
            }
        }
        if header ?? false {
            request.allHTTPHeaderFields = self.getHeaderDic()
        }
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                handle(error!.localizedDescription, false, nil)
            } else {
                do {
                    guard let dataResponse = data else {
                        handle("error", false, nil)
                        return }
                    let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments)
                    handle(json, true, dataResponse)
                } catch {
                    handle(error.localizedDescription, false, nil)
                }
            }
        }
        dataTask.resume()
    }
}


extension Alamofire.SessionManager{
    @discardableResult
    open func requestWithoutCache(
        _ url: URLConvertible,
        method: HTTPMethod = .post,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = JSONEncoding.default,
        headers: HTTPHeaders? = nil)// also you can add URLRequest.CachePolicy here as parameter
        -> DataRequest
    {
        do {
            var urlRequest = try URLRequest(url: url, method: method, headers: headers)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            let encodedURLRequest = try encoding.encode(urlRequest, with: parameters)
            return request(encodedURLRequest)
        } catch {
            // TODO: find a better way to handle error
            print(error)
            return request(URLRequest(url: URL(string: "http://example.com/wrong_request")!))
        }
    }
}
