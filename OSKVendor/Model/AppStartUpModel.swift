//
//  AppStartUpModel.swift
//  NopCommerce
//
//  Created by BS-125 on 5/20/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit
import ObjectMapper


class AppStartUpModel: Mappable {
    
    
    var iOsAPPUDID: String?
    
    var TextColorofPrimaryButton: String?
    
    var AppNameOnAppleStore: String?
    
    var AppLogoAltText: String?
    
    var ActivatePushNotification: Bool = false
    
    var AndroidAppStatus: String?
    
    var HeaderBackgroundColor: String?
    
    var BannerModel: [Bannermodel]?
    
    var BackgroundColorofPrimaryButton: String?
    
    var DefaultNopFlowSameAs: Bool = false
    
    var AppKey: String?
    
    var GoogleApiProjectNumber: String?
    
    var AppName: String?
    
    var AppNameOnGooglePlayStore: String?
    
    var AppLogo: Int = 0
    
    var DownloadUrl: String?
    
    var EnableBestseller: Bool = false
    
    var HighlightedTextColor: String?
    
    var BackgroundColorofSecondaryButton: String?
    
    var UploudeIOSPEMFile: Int = 0
    
    var EnableFeaturedProducts: Bool = false
    
    var AppUrlonAppleStore: String?
    
    var SecondaryTextColor: String?
    
    var CreatedDate: String?
    
    var AppDescription: String?
    
    var EnableNewProducts: Bool = false
    
    var PrimaryTextColor: String?
    
    var GcmApiKey: String?
    
    var PushNotificationMessage: String?
    
    var AppImage: Int = 0
    
    var MobilWebsiteURL: String?
    
    var Willusedefaultnopcategory: Bool = false
    
    var AppLogoUrl: String?
    
    var LicenceType: String?
    
    var PEMPassword: String?
    
    var AppImgUrl: String?
    
    var HeaderFontandIconColor: String?
    
    var AppUrlOnGooglePlayStore: String?
    
    var SandboxMode: Bool = false
    
    var PushNotificationHeading: String?
    
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        
        iOsAPPUDID   <- map["iOsAPPUDID"]
        
        TextColorofPrimaryButton <- map["TextColorofPrimaryButton"]
        
        AppNameOnAppleStore      <- map["AppNameOnAppleStore"]
        
        AppLogoAltText <- map["AppLogoAltText"]
        
        ActivatePushNotification <- map["ActivatePushNotification"]
        
        AndroidAppStatus <- map["AndroidAppStatus"]
        
        HeaderBackgroundColor <- map["HeaderBackgroundColor"]
        
        BannerModel <- map["BannerModel"]
        
        BackgroundColorofPrimaryButton <- map["BackgroundColorofPrimaryButton"]
        
        DefaultNopFlowSameAs <- map["DefaultNopFlowSameAs"]
        
        AppKey      <- map["AppKey"]
        
        GoogleApiProjectNumber  <- map["GoogleApiProjectNumber"]
        
        AppName  <- map["AppName"]
        
        AppNameOnGooglePlayStore   <- map["AppNameOnGooglePlayStore"]
        
        AppLogo   <- map["AppLogo"]
        
        DownloadUrl  <- map["DownloadUrl"]
        
        EnableBestseller   <- map["EnableBestseller"]
        
        HighlightedTextColor  <- map["HighlightedTextColor"]
        
        BackgroundColorofSecondaryButton  <- map["BackgroundColorofSecondaryButton"]
        
        UploudeIOSPEMFile  <- map["UploudeIOSPEMFile"]
        
        EnableFeaturedProducts  <- map["EnableFeaturedProducts"]
        
        AppUrlonAppleStore <- map["AppUrlonAppleStore"]
        
        SecondaryTextColor  <- map["SecondaryTextColor"]
        
        CreatedDate   <- map["CreatedDate"]
        
        AppDescription <- map["AppDescription"]
        
        EnableNewProducts  <- map["EnableNewProducts"]
        
        PrimaryTextColor  <- map["PrimaryTextColor"]
        
        GcmApiKey   <- map["GcmApiKey"]
        
        PushNotificationMessage  <- map["PushNotificationMessage"]
        
        AppImage   <- map["AppImage"]
        
        MobilWebsiteURL   <- map["MobilWebsiteURL"]
        
        Willusedefaultnopcategory   <- map["Willusedefaultnopcategory"]
        
        AppLogoUrl   <- map["AppLogoUrl"]
        
        LicenceType   <- map["LicenceType"]
        
        PEMPassword   <- map["PEMPassword"]
        
        AppImgUrl   <- map["AppImgUrl"]
        
        HeaderFontandIconColor  <- map["HeaderFontandIconColor"]
        
        AppUrlOnGooglePlayStore   <- map["AppUrlOnGooglePlayStore"]
        
        SandboxMode   <- map["SandboxMode"]
        
        PushNotificationHeading  <- map["PushNotificationHeading"]
        
        
        
    }
}
class Bannermodel: Mappable {
    
    var PictureId: Int = 0
    
    var ProductOrCategoryId: String?
    
    var Text: String?
    
    var Link: String?
    
    var IsProduct: Bool = false
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        PictureId   <- map["PictureId"]
        ProductOrCategoryId <- map["ProductOrCategoryId"]
        Text      <- map["Text"]
        Link <- map["Link"]
        IsProduct <- map["IsProduct"]
    }
}

