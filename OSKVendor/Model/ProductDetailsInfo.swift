//
//  ProductDetailsInfo.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 25/08/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit

open class ProductDetailsInfo: NSObject {
    
    var productsInfo: [ProductsInfo] = []
    var fromPrice: Double?
    var toPrice: Double?
    var PriceRange: [PriceRange] = []
    var availableSortOptions: [AvailableSortOptions] = []
    var notFilteredItems: [NotFilteredItems] = []
    var alreadyFilteredItems : [NotFilteredItems] = []
    var TotalPages = 0
}

open class ProductsInfo: NSObject {
    
    var ProductId :             NSInteger
    var TotalReviews :          NSInteger
    var RatingSum :             NSInteger
    var AllowCustomerReviews :  Bool
    var Published :  Bool
    var id :                    NSInteger
    var CustomProperties :      NSDictionary?
    var iswishList:             Bool?
    var DisableWishlistButton : Bool? = false
    var MarkAsNew : Bool? = false
    var ImageUrl :              NSString
    var Name :                  NSString
    var ShortDescription :      NSString?
    
    var OldPrice :              String?
    var Price :                 String?
    
    var PriceWithDiscountValue : Float
    var PriceValue : Float
    var oldPriceValue : Float
    var DiscountPercentage : Int?
    
    var PriceWithDiscount : String = ""
    var CurrencyCode : String = ""
    
    var DisplayOrder : NSInteger
    var CreatedOn : Date
    var vendorMobileNumber : String?
    
    //New Data
    var CategoryId : Int?
    var CategoryName : String?
    var CategoryIconPath : String?
    
    init (JSON:AnyObject) {
        
        self.ProductId              = JSON.value(forKeyPath: "ReviewOverviewModel.ProductId") as? NSInteger ?? 0
        self.TotalReviews           = JSON.value(forKeyPath: "ReviewOverviewModel.TotalReviews") as? NSInteger ?? 0
        self.RatingSum              = JSON.value(forKeyPath: "ReviewOverviewModel.RatingSum") as? NSInteger ?? 0
        self.AllowCustomerReviews   = JSON.value(forKeyPath: "ReviewOverviewModel.AllowCustomerReviews") as? Bool ?? false
        self.Published              = JSON.value(forKeyPath: "Published") as? Bool ?? false
        self.id                     = JSON.value(forKeyPath: "Id") as? NSInteger ?? 0
        self.CustomProperties       = JSON.value(forKeyPath: "CustomProperties") as? NSDictionary
        self.iswishList             = JSON.value(forKeyPath: "IsWishList") as? Bool
        self.DisableWishlistButton  = JSON.value(forKeyPath: "DisableWishlistButton") as? Bool
        self.MarkAsNew  = JSON.value(forKeyPath: "MarkAsNew") as? Bool
        self.ImageUrl               = JSON.value(forKeyPath: "DefaultPictureModel.ImageUrl") as? NSString ?? ""
        self.CategoryId             = JSON.value(forKeyPath: "CategoryId") as? Int ?? 0
        self.CategoryIconPath       = JSON.value(forKeyPath: "CategoryIconPath") as? String ?? ""
        self.CategoryName           = JSON.value(forKeyPath: "CategoryName") as? String ?? ""
        self.Name                   = JSON.value(forKeyPath: "Name") as? NSString ?? ""
        self.ShortDescription       = JSON.value(forKeyPath: "ShortDescription") as? NSString
        //self.OldPrice               = JSON.value(forKeyPath: "ProductPrice.OldPrice") as? String
        //self.Price                  = JSON.value(forKeyPath: "ProductPrice.Price") as? String
        if let oldpp = JSON.value(forKeyPath: "ProductPrice.OldPrice") as? String{
            self.OldPrice           = oldpp
        }
        self.Price                  = JSON.value(forKeyPath: "ProductPrice.Price") as? String
        self.vendorMobileNumber     = JSON.value(forKeyPath: "VendorMobileNumber") as? String
        if let pp = JSON.value(forKeyPath: "ProductPrice.PriceWithDiscount") as? String{
            self.PriceWithDiscount = pp
        }
        
        self.PriceValue             = JSON.value(forKeyPath: "ProductPrice.PriceValue") as? Float ?? 0.0
        self.PriceWithDiscountValue = JSON.value(forKeyPath: "ProductPrice.PriceWithDiscountValue") as? Float ?? 0.0
        self.oldPriceValue          = JSON.value(forKeyPath: "ProductPrice.OldPriceValue") as? Float ?? 0.0
        self.DiscountPercentage = JSON.value(forKeyPath: "ProductPrice.DiscountPercentage") as? Int ?? 0
        
        self.CurrencyCode           = JSON.value(forKeyPath: "ProductPrice.CurrencyCode") as? String ?? "MMK"
        self.DisplayOrder           = JSON.value(forKeyPath: "DisplayOrder") as? NSInteger ?? 0
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ssZZZ"
        let date = dateFormatter.date(from: JSON.value(forKeyPath: "CreatedOn") as! String)
        self.CreatedOn              =  date ?? Date()
        
    }
}


open class AvailableSortOptions: NSObject {
    
    var Selected    :   Bool?
    var Group       :   NSString?
    var Disabled    :   Bool?
    var Text        :   NSString?
    var Value       :   NSString?
    
    init (JSON:AnyObject) {
        
        self.Selected        = JSON.value(forKeyPath: "Selected") as? Bool
        self.Group           = JSON.value(forKeyPath: "Group") as? NSString
        self.Disabled        = JSON.value(forKeyPath: "Disabled") as? Bool
        self.Text            = JSON.value(forKeyPath: "Text") as? NSString
        self.Value           = JSON.value(forKeyPath: "Value") as? NSString
    }
}

open class PriceRange: NSObject {
    var fromPrice : NSString?
    var toPrice : NSString?
    
    init (JSON:AnyObject) {
        self.fromPrice          = JSON.value(forKeyPath: "From").safelyWrappingString() as NSString
        self.toPrice            = JSON.value(forKeyPath: "To").safelyWrappingString() as NSString
    }
}

open class NotFilteredItems: NSObject {
    var SpecificationAttributeName       : NSString?
    var FilterId                         : NSInteger?
    var SpecificationAttributeOptionName : NSString?
    var ProductId :             NSInteger
    
    init (JSON:AnyObject) {
        self.SpecificationAttributeName          = JSON.value(forKeyPath: "SpecificationAttributeName") as? NSString
        self.FilterId                            = JSON.value(forKeyPath: "FilterId") as? NSInteger
        self.SpecificationAttributeOptionName    = JSON.value(forKeyPath: "SpecificationAttributeOptionName") as? NSString
        self.ProductId                           = JSON.value(forKeyPath: "ProductId") as? NSInteger ?? 0
    }
}
