//
//  VMLoginModel.swift
//  VMart
//
//  Created by Kethan on 11/12/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import Foundation

class Vendor_loginmodel: NSObject {
    static let shared = Vendor_loginmodel()
    var firstName: String?
    var lastName: String?
    var email: String?
    var userName: String?
    var gender: String?
    var dateOfBirthDay: Int?
    var dateofBirthMonth: Int?
    var dateOfBirthYear: Int?
    var facebookId: String?
    var address1: String?
    var address2: String?
    var city: String?
    var state: String?
    var country: String?
    var otherEmail: String?
    var mobileNumber: String?
    var shopName: String?
    var password: String?
    var deviceID: String?
    var simid: String?
    var token: String?
    var mode: String?
    var profilePictureUrl: String?
    var viberNumber: String?
    var maritalStatus: Int16?
    var displayAvatar: Bool?
    var addressAdded: Bool? = false
    var deviceInfo: Any?
    var houseNo: String?
    var floorNo: String?
    var roomNo: String?
    var countryCode: String?
    var versionCode: Int32?
    var customerId: Int32?
    
    class func wrapModel() {
        if let profile = OSKVendorDatabaseOperation.sharedInstance.fetchUserProfileDetailsRecord() {
            self.shared.firstName = profile.firstName.safelyWrappingString()
            self.shared.lastName = profile.lastName.safelyWrappingString()
            self.shared.email = profile.emailId.safelyWrappingString()
            self.shared.userName =  ""
            self.shared.gender = profile.gender.safelyWrappingString()
            self.shared.addressAdded = profile.addressAdded
            self.shared.facebookId = profile.facebook
            if let dob = profile.dob {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy"
                let curFormat = DateFormatter()
                curFormat.dateFormat = "dd-MM-yyyy"
                if let date = formatter.date(from: dob) {
                    let strDate = curFormat.string(from: date)
                    let dateArray = strDate.components(separatedBy: "-")
                    if dateArray.count > 2 {
                        self.shared.dateOfBirthDay = Int(dateArray[0]) ?? 0
                        self.shared.dateofBirthMonth = Int(dateArray[1]) ?? 0
                        self.shared.dateOfBirthYear = Int(dateArray[2]) ?? 0
                    } else {
                        shared.setDate()
                    }
                } else {
                    shared.setDate()
                }
            } else {
                shared.setDate()
            }
            self.shared.address1  = profile.address1.safelyWrappingString()
            self.shared.address2 = profile.address2.safelyWrappingString()
            self.shared.city = profile.city.safelyWrappingString()
            self.shared.state = profile.state.safelyWrappingString()
            self.shared.country = profile.country.safelyWrappingString()
            self.shared.otherEmail = profile.otherEmail.safelyWrappingString()
            self.shared.mobileNumber  = profile.mobileNo.safelyWrappingString()
            self.shared.shopName = profile.shopName.safelyWrappingString()
            self.shared.deviceID  = profile.deviceId.safelyWrappingString()
            self.shared.simid = profile.simID.safelyWrappingString()
            self.shared.token =  profile.token.safelyWrappingString()
            self.shared.mode =  profile.mode.safelyWrappingString()
            self.shared.profilePictureUrl = profile.profilePic.safelyWrappingString()
            self.shared.viberNumber = ""
            self.shared.maritalStatus = profile.maritialStatus
            self.shared.displayAvatar = profile.displayAvatar
            self.shared.deviceInfo = profile.deviceInfo.safelyWrappingString()
            self.shared.password = profile.password.safelyWrappingString()
            self.shared.houseNo = profile.houseNo.safelyWrappingString()
            self.shared.floorNo = profile.floorNo.safelyWrappingString()
            self.shared.roomNo = profile.roomNo.safelyWrappingString()
            self.shared.countryCode = profile.countryCode.safelyWrappingString()
            self.shared.versionCode = profile.versionCode
            self.shared.customerId = profile.customerId
        }
    }
    
    func setDate() {
        self.dateOfBirthDay = 0
        self.dateofBirthMonth = 0
        self.dateOfBirthYear = 0
    }
}

class VMTempModel: NSObject {
    var firstName: String?
    var lastName: String?
    var email: String?
    var userName: String?
    var gender: String?
    var dateOfBirthDay: Int?
    var dateofBirthMonth: Int?
    var dateOfBirthYear: Int?
    var address1: String?
    var address2: String?
    var city: String?
    var state: String?
    var country: String?
    var otherEmail: String?
    var mobileNumber: String?
    var password: String?
    var deviceID: String?
    var simid: String?
    var token: String?
    var mode: String?
    var profilePictureUrl: String?
    var viberNumber: String?
    var maritalStatus: Int16?
    var displayAvatar: Bool?
    var deviceInfo: Any?
    var addressAdded: Bool? = false
    var shopName: String?
    var houseNo: String?
    var floorNo: String?
    var roomNo: String?
    var countryCode: String?
    var versionCode: Int32?
    var customerId: Int32?
    var facebookId: String?
    
    func wrapDataModel(JSON: Dictionary<String, Any>) {
        self.firstName = JSON["FirstName"] as? String ?? ""
        self.lastName = JSON["LastName"] as? String ?? ""
        self.email = JSON["Email"] as? String ?? ""
        self.userName = JSON["UserName"] as? String ?? ""
        self.gender = JSON["Gender"] as? String ?? ""
        self.dateOfBirthDay = JSON["DateOfBirthDay"] as? Int ?? 0
        self.dateofBirthMonth = JSON["DateofBirthMonth"] as? Int ?? 0
        self.dateOfBirthYear = JSON["DateOfBirthYear"] as? Int ?? 0
        self.address1  = JSON["Address1"] as? String ?? ""
        self.address2 = JSON["Address2"] as? String ?? ""
        self.city = JSON["City"] as? String ?? ""
        self.state = JSON["State"] as? String ?? ""
        self.country = JSON["Country"] as? String ?? ""
        self.otherEmail = JSON["OtherEmail"] as? String ?? ""
        self.mobileNumber  = JSON["MobileNumber"] as? String ?? ""
        self.shopName  = JSON["ShopName"] as? String ?? ""
        self.password = JSON["Password"] as? String ?? ""
        self.deviceID  = JSON["DeviceID"] as? String ?? ""
        self.simid = JSON["Simid"] as? String ?? ""
        self.token = JSON["Token"] as? String ?? ""
        self.mode = JSON["Mode"] as? String ?? ""
        self.profilePictureUrl = JSON["ProfilePictureUrl"] as? String ?? ""
        self.viberNumber = JSON["ViberNumber"] as? String ?? ""
        self.maritalStatus = Int16(JSON["MaritalStatus"] as? Int ?? 1)
        self.displayAvatar = JSON["DisplayAvatar"] as? Bool ?? true
        self.deviceInfo = JSON["DeviceInfo"]
        self.facebookId = JSON["FacebookId"] as? String ?? ""
        if let addressAdd = Vendor_loginmodel.shared.addressAdded, addressAdd == true {
            self.addressAdded = true
        } else {
            self.addressAdded = false
        }
        
        self.houseNo = JSON["HouseNo"] as? String ?? ""
        self.floorNo = JSON["FloorNo"] as? String ?? ""
        self.roomNo = JSON["RoomNo"] as? String ?? ""
        self.countryCode = JSON["CountryCode"] as? String ?? ""
        self.versionCode = Int32(JSON["VersionCode"] as? Int ?? 1)
        self.customerId = Int32(JSON["CustomerId"] as? Int ?? 1)
        self.insertToDataBase(model: self)
    }
    
    
    func wrapModel(dic: Dictionary<String,Any>) {
        if let profileDetail = dic["ProfileDetails"] as? Array<Dictionary<String,Any>>, profileDetail.count > 0, let profile = profileDetail.first {
            self.firstName = profile["Name"] as? String ?? ""
            self.lastName = ""
            self.email = profile["EmailId"] as? String ?? ""
            self.userName =  ""
            self.gender = (profile["Gender"] as? Bool ?? true) ? "M" : "F"
            if let dob = profile["DOB"] as? String {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy"
                let curFormat = DateFormatter()
                curFormat.dateFormat = "dd-MMM-yyyy"
                if let date = curFormat.date(from: dob) {
                    let strDate = formatter.string(from: date)
                    let dateArray = strDate.components(separatedBy: "/")
                    if dateArray.count > 2 {
                        self.dateOfBirthDay = Int(dateArray[0]) ?? 0
                        self.dateofBirthMonth = Int(dateArray[1]) ?? 0
                        self.dateOfBirthYear = Int(dateArray[2]) ?? 0
                    } else {
                        self.setDate()
                    }
                } else {
                    self.setDate()
                }
            } else {
                self.setDate()
            }
            self.address1  = profile["Address1"] as? String ?? ""
            self.address2 = profile["Address2"] as? String ?? ""
            self.city = profile["Township"] as? String ?? ""
            self.state = profile["State"] as? String ?? ""
            self.country = profile["Country"] as? String ?? ""
            self.otherEmail = ""
            self.mobileNumber  = profile["Mobilenumber"] as? String ?? ""
            self.shopName  = profile["ShopName"] as? String ?? ""
            self.password = profile["Password"] as? String ?? ""
            self.deviceID  = profile["DeviceId"] as? String ?? ""
            self.simid = profile["SimID"] as? String ?? ""
            self.token =  profile["Token"] as? String ?? ""
            self.mode =  "AutoLogin"
            self.profilePictureUrl = profile["ProfilePic"] as? String ?? ""
            self.viberNumber = ""
            self.maritalStatus = Int16(0)
            self.displayAvatar = false
            self.deviceInfo = nil
            self.facebookId = profile["FacebookId"] as? String ?? ""
            if let addressAdd = Vendor_loginmodel.shared.addressAdded, addressAdd == true {
                self.addressAdded = true
            } else {
                self.addressAdded = false
            }
            self.houseNo = profile["HouseNo"] as? String ?? ""
            self.floorNo = profile["FloorNo"] as? String ?? ""
            self.roomNo = profile["RoomNo"] as? String ?? ""
            self.countryCode = profile["CountryCode"] as? String ?? ""
            self.versionCode = Int32(profile["VersionCode"] as? Int ?? 1)
            self.customerId = Int32(profile["CustomerId"] as? Int ?? 1)
            self.insertToDataBase(model: self)
        }
    }
    
    func insertToDataBase(model: VMTempModel) {
        OSKVendorDatabaseOperation.sharedInstance.insertUserProfileDetailsIntoDB(dict: model)
    }
    
    func setDate() {
        self.dateOfBirthDay = 0
        self.dateofBirthMonth = 0
        self.dateOfBirthYear = 0
    }
}
