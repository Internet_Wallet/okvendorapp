//
//  AllCategory.swift
//  OSKVendor
//
//  Created by Avaneesh Awasthi on 18/09/2020.
//  Copyright © 2020 Avaneesh Awasthi. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

class AllCategory:  Mappable {
    
    var listCategory : [Category]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.listCategory <- map["Data"]
    }
    
}

 class Category: Mappable {
    
    var parentCategoryId : Int? = 0
    var displayOrder : Int? = 0
    var iconPath : String?
    var Extension : String?
    var Children : [Category]? = []
    var Id : Int? = 0
    var name : String?
    var productCount : Int? = 0
    var haveInfoAttribute: Bool?
    
    required  init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        self.parentCategoryId <- map["ParentCategoryId"]
        self.displayOrder     <- map["DisplayOrder"]
        self.iconPath         <- map["IconPath"]
        self.Extension        <- map["Extension"]
        self.Children         <- map["Children"]
        self.Id               <- map["Id"]
        self.name             <- map["Name"]
        self.productCount     <- map["ProductCount"]
        self.haveInfoAttribute <- map["HaveAttributeInfo"]
    }
    
}
