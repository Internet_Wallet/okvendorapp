//
//  AttributesSizeGuideVC.swift
//  Covid Zay Vendor
//
//  Created by Tushar Lama on 21/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import UIKit

protocol GetTheSizeDetails {
    func getSizeDetail(obj: [AttributeModel])
}

class AttributesSizeGuideVC: UIViewController {

    @IBOutlet var savebuttonOutlet: UIButton!{
        didSet{
            savebuttonOutlet.setTitle("Save".localized, for: .normal)
        }
    }
    @IBOutlet var sizeTable: UITableView!
    var values: [Tushar_Values]?
    var attributeParamModel = [AttributeModel]()
    var countDecrease = 2
    var sizeGuideImage = ""
    var increaseCount = 0
    var mainID: Int?
    var newDelegate: GetTheSizeDetails?
    
    
    @IBOutlet var cancelButtonOutlet: UIButton!{
        didSet{
            cancelButtonOutlet.setTitle("Cancel".localized, for: .normal)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
        self.sizeTable.register(UINib(nibName: "AttributeGuideImage", bundle: nil), forCellReuseIdentifier: "AttributeGuideImage")
        self.sizeTable.register(UINib(nibName: "AddMeasurementCell", bundle: nil), forCellReuseIdentifier: "AddMeasurementCell")
        self.navigationController?.navigationBar.tintColor = .white
        
        sizeTable.separatorColor = .clear
        sizeTable.delegate = self
        sizeTable.dataSource = self
        sizeTable.reloadData()
    }
    
    
    func setNavigationBar() {

        self.navigationItem.setHidesBackButton(true, animated:false)

        self.title = "More Details".localized
      
        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: -10, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: -15, y: 0, width: 40, height: 40))

        if let imgBackArrow = UIImage(named: "backarrow") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)

        let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMain))
        view.addGestureRecognizer(backTap)

        let leftBarButtonItem = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    @objc func backToMain() {
        
        self.navigationController?.popViewController(animated: true)
    }


    @IBAction func onClickSaveButton(_ sender: Any) {
        newDelegate?.getSizeDetail(obj: attributeParamModel)
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func onClickCancel(_ sender: Any) {
        newDelegate?.getSizeDetail(obj: attributeParamModel)
        self.navigationController?.popViewController(animated: false)
    }
    
    
}

extension AttributesSizeGuideVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let obj = values{
            return obj.count + increaseCount + countDecrease
        }else{
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            guard let cell = sizeTable.dequeueReusableCell(withIdentifier: "AttributeGuideImage") as? AttributeGuideImage else {
                return UITableViewCell()
            }
            cell.suggestionImageView.setImageUsingUrl(sizeGuideImage)
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == 1{
            guard let cell = sizeTable.dequeueReusableCell(withIdentifier: "AddMeasurementCell") as? AddMeasurementCell else {
                return UITableViewCell()
            }
            
            
            cell.selectionStyle = .none
            cell.nameTextField.backgroundColor = .lightGray
            cell.valueTestField.backgroundColor = .lightGray
            cell.nameTextField.isUserInteractionEnabled = false
            cell.valueTestField.isUserInteractionEnabled = false
            cell.nameTextField.textAlignment = .center
            cell.valueTestField.textAlignment = .center
            cell.nameTextField.text = "Tailoring".localized
            cell.valueTestField.text = "Inches".localized
            
            
            return cell
        }else{
            guard let cell = sizeTable.dequeueReusableCell(withIdentifier: "AddMeasurementCell") as? AddMeasurementCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            let currentIndex = indexPath.row - countDecrease
           // cell.delegate = self
            cell.guideDelegate = self
            cell.valueTestField.keyboardType = .numberPad
            cell.valueTestField.tag = currentIndex
            cell.nameTextField.tag = currentIndex
            cell.nameTextField.textAlignment = .center
            cell.valueTestField.textAlignment = .center
            
            if attributeParamModel.count>0{
                if attributeParamModel.indices.contains(currentIndex){
                    let data = attributeParamModel[currentIndex].name ?? ""
                    let newValue = data.components(separatedBy: "-")
                    if newValue.indices.contains(0){
                        cell.nameTextField.text = newValue[0]
                    }
                    if newValue.indices.contains(1){
                        cell.valueTestField.text = newValue[1]
                    }
                }
            }else{
                if let obj = values{
                    if obj.indices.contains(currentIndex){
                        if obj[currentIndex].disabled ?? false{
                            cell.nameTextField.text = obj[currentIndex].name ?? ""
                        }else{
                            if let hasName = obj[currentIndex].name{
                                let newValue = hasName.components(separatedBy: "-")
                                if newValue.indices.contains(0){
                                    cell.nameTextField.text = newValue[0]
                                }
                                if newValue.indices.contains(1){
                                    cell.valueTestField.text = newValue[1]
                                }
                            }
                        }
                        
                    }
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 201.0
        }else{
            return 47.0
        }
    }
    
   
    
}


extension AttributesSizeGuideVC : CallColorPicker {
    func callColorPicker(product: Int){
        
        
        
    }
}

extension AttributesSizeGuideVC: IncreaseCount {
    func increaseMeasurementCount(){
        increaseCount = increaseCount + 1
        sizeTable.reloadData()
    }
}


extension AttributesSizeGuideVC: GetSizeAttributesDetails {
    func getDetails(index: Int,textFieldName: String){
        
        if let valueMainId = mainID{
            if let hasValueId = values{
             
                if let idx = attributeParamModel.firstIndex(where: { $0.productAttributeValueId == hasValueId[index].id ?? 0 }) {
                    attributeParamModel.remove(at: idx)
                }
                
                    let newValue = textFieldName.components(separatedBy: "-")
                    if (newValue.indices.contains(0) && newValue.indices.contains(1)) || newValue.indices.contains(0){
                        attributeParamModel.append(AttributeModel(productAttributeValueId: hasValueId[index].id ?? 0, productAttributeMappingId: valueMainId, name: textFieldName, color: hasValueId[index].colorSquaresRGB ?? "", priceAdjustment: 0, isPreSelected: false, disable: false))
                    }
                
            }
        }
    }
}
