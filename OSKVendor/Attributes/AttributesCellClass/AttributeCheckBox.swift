//
//  AttributeCheckBox.swift
//  Attributes
//
//  Created by Tushar Lama on 12/01/2021.
//

import UIKit

//"ColorSquaresRgb": "#000002",
//               "PriceAdjustment": 2000,
//               "IsPreSelected": false,
//               "Disabled": false

protocol AttributeCheckBoxData {
    func attributeCheckBoxData(valueId: Int,attributeId: Int,name: String,colorSquare: String,priceAdjustment: Int,isPreSelect: Bool,disable: Bool,isComingFrom: String)
}

class AttributeCheckBox: UITableViewCell {

    @IBOutlet var attributeName: UILabel!
    var values: [Tushar_Values]?
    @IBOutlet var attributeView: UIView!
    @IBOutlet var attributeCollectionView: UICollectionView!
    var delegate: AttributeCheckBoxData?
    var productID: Int?
    var checkMarkDic = NSMutableDictionary()
    var selectedArrayIndex: [Int]?
    var selectedArray = [Bool]()
   
    override func awakeFromNib() {
        super.awakeFromNib()
        self.attributeCollectionView.register(UINib(nibName: "CheckBoxCell", bundle: nil), forCellWithReuseIdentifier: "CheckBoxCell")
        self.attributeCollectionView.delegate = self
        self.attributeCollectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValue(values: [Tushar_Values]){
        for i in 0..<values.count{
            if values[i].disabled ?? false{
                selectedArray.append(false)
            }else{
                selectedArray.append(true)
            }
        }
    }
    

}

extension AttributeCheckBox: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let obj = values{
            return obj.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = attributeCollectionView.dequeueReusableCell(withReuseIdentifier: "CheckBoxCell", for: indexPath) as? CheckBoxCell else{
            return UICollectionViewCell()
        }
        if let obj = values{
            cell.attributeQuantityLabel.text = obj[indexPath.row].name ?? ""
        }
        
        
        if selectedArray[indexPath.row]{
            cell.roundedBorderWithShadow(borderColor: UIColor.init(hex: Constant.kPrimaryThemeGreen) ,shadowColor: .clear,backgroundColor: UIColor.init(hex: Constant.kPrimaryThemeGreen))
        }else{
            cell.roundedBorderWithShadow(borderColor: UIColor.init(hex: Constant.kPrimaryThemeGreen) ,shadowColor: .clear,backgroundColor: .clear)
        }
        

        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        if let obj = values{
            let label = UILabel(frame: CGRect.zero)
            label.text = obj[indexPath.row].name ?? ""
            label.sizeToFit()
            if label.frame.size.width<80{
                return CGSize(width: 80 , height: 40)
            }else{
                return CGSize(width:  label.frame.size.width , height: 40)
            }
            
        }else{
            return CGSize(width: 80 , height: 40)
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                    
                
        if selectedArray[indexPath.row]{
            selectedArray.remove(at: indexPath.row)
            selectedArray.insert(false, at: indexPath.row)
            if let obj = values{
                delegate?.attributeCheckBoxData(valueId: obj[indexPath.row].id ?? 0, attributeId: productID ?? 0, name: obj[indexPath.row].name ?? "",colorSquare: obj[indexPath.row].colorSquaresRGB ?? "" ,priceAdjustment: obj[indexPath.row].priceAdjustmentValue ?? 0,isPreSelect: obj[indexPath.row].isPreSelected ?? false,disable: true,isComingFrom:"checkBox")
            }
        }else{
            selectedArray.remove(at: indexPath.row)
            selectedArray.insert(true, at: indexPath.row)
            if let obj = values{
                delegate?.attributeCheckBoxData(valueId: obj[indexPath.row].id ?? 0, attributeId: productID ?? 0, name: obj[indexPath.row].name ?? "",colorSquare: obj[indexPath.row].colorSquaresRGB ?? "" ,priceAdjustment: obj[indexPath.row].priceAdjustmentValue ?? 0,isPreSelect: obj[indexPath.row].isPreSelected ?? false,disable: false,isComingFrom:"checkBox")
            }
        }
            
        attributeCollectionView.reloadItems(at: [indexPath])
        
    }
}
