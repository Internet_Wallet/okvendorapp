//
//  ColorPickerAttribute.swift
//  Attributes
//
//  Created by Tushar Lama on 14/01/2021.
//

import UIKit

protocol CallColorPicker {
    func callColorPicker(product: Int)
}

class ColorPickerAttribute: UITableViewCell {

    @IBOutlet var colorCollection: UICollectionView!
    @IBOutlet var bottomLabel: UILabel!
    @IBOutlet var attributeView: UIView!
    @IBOutlet var plusbutton: UIButton!
    @IBOutlet var colorTextField: UITextField!
    var delegate: CallColorPicker?
    var productID: Int?
    var mainID: Int?
    var name = ""
    var colorArray: [UIColor]?
    var checkMarkDic = NSMutableDictionary()
    var newDelegate: AttributeCheckBoxData?
    var selectedArrayIndex: [Int]?
    var values: [Tushar_Values]?
    var selectedIndex: [Int]?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.colorCollection.register(UINib(nibName: "ColorBox", bundle: nil), forCellWithReuseIdentifier: "ColorBox")
        self.colorTextField.text = "Select Color".localized
        colorCollection.delegate = self
        colorCollection.dataSource = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func hexStringFromColor(color: UIColor) -> String {
        let components = color.cgColor.components
        let r: CGFloat = components?[0] ?? 0.0
        let g: CGFloat = components?[1] ?? 0.0
        let b: CGFloat = components?[2] ?? 0.0

        let hexString = String.init(format: "#%02lX%02lX%02lX", lroundf(Float(r * 255)), lroundf(Float(g * 255)), lroundf(Float(b * 255)))
        print(hexString)
        return hexString
     }
    
    func getColorList(value: [Tushar_Values]){
         let newValues = value
            if newValues.count>0{
                for j in 0..<newValues.count{
                    self.colorArray?.append(UIColor(hex: newValues[j].colorSquaresRGB ?? ""))
                }
        }
    }

    @IBAction func onClickPlusButton(_ sender: Any) {
        delegate?.callColorPicker(product: mainID ?? 0)
    }
}


extension ColorPickerAttribute: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let obj = colorArray{
            return obj.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = colorCollection.dequeueReusableCell(withReuseIdentifier: "ColorBox", for: indexPath) as? ColorBox else{
            return UICollectionViewCell()
        }
        
        if let obj = colorArray{
            cell.colorBoxButton.backgroundColor = obj[indexPath.row]
        }
        
        cell.colorBoxButton.tag = indexPath.row
        cell.delegate = self
                
        if checkMarkDic.count>0{
            if checkMarkDic.value(forKey: "\(indexPath.row)") != nil{
                if checkMarkDic.value(forKey: "\(indexPath.row)") as! Bool{
                    if cell.layer.borderColor == UIColor.init(hex: Constant.kPrimaryThemeGreen).cgColor{
                        cell.roundedBorderWithShadow(borderColor: .clear ,shadowColor: .clear,backgroundColor: .clear)
                    }else{
                        cell.roundedBorderWithShadow(borderColor: UIColor.init(hex: Constant.kPrimaryThemeGreen) ,shadowColor: .clear,backgroundColor: .clear)
                    }
                }else{
                    cell.roundedBorderWithShadow(borderColor: .clear ,shadowColor: .clear,backgroundColor: .clear)
                }
            }else{
                cell.roundedBorderWithShadow(borderColor: UIColor.init(hex: Constant.kPrimaryThemeGreen) ,shadowColor: .clear,backgroundColor: .clear)
            }
        }else{
            //edit case
            if let obj = values{
                if obj.indices.contains(indexPath.row){
                    if obj[indexPath.row].disabled ?? false{
                        cell.roundedBorderWithShadow(borderColor: .clear ,shadowColor: .clear,backgroundColor: .clear)
                    }else{
                        cell.roundedBorderWithShadow(borderColor: UIColor.init(hex: Constant.kPrimaryThemeGreen) ,shadowColor: .clear,backgroundColor: .clear)
                    }
                }else{
                    cell.roundedBorderWithShadow(borderColor: UIColor.init(hex: Constant.kPrimaryThemeGreen) ,shadowColor: .clear,backgroundColor: .clear)

                }
            }else{
                //add product case
                cell.roundedBorderWithShadow(borderColor: UIColor.init(hex: Constant.kPrimaryThemeGreen) ,shadowColor: .clear,backgroundColor: .clear)
            }
        }
            return cell
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50 , height: 50)
    }
        
}


extension ColorPickerAttribute: GetCellIndex{
  
    func getCellIndex(index: Int){
        
        let indexPath = IndexPath(row: index, section: 0)
        guard let cell = self.colorCollection.cellForItem(at: indexPath) as? ColorBox else{
            return
        }
        
        
        //edit
        if let hasValue = values{
            if let colorNew = colorArray{
                if let indexNew = hasValue.firstIndex(where: { $0.colorSquaresRGB?.lowercased() ==  hexStringFromColor(color: colorNew[index]).lowercased()}) {
                    //that means already selected
                    if cell.layer.borderColor == UIColor.init(hex: Constant.kPrimaryThemeGreen).cgColor{
                        //pass true
                        checkMarkDic.setValue(false, forKey: "\(index)")
                        if hasValue.indices.contains(indexNew){
                            newDelegate?.attributeCheckBoxData(valueId:  hasValue[index].id ?? 0, attributeId: mainID ?? 0, name: hasValue[index].name ?? "", colorSquare: hasValue[index].colorSquaresRGB ?? "", priceAdjustment: hasValue[index].priceAdjustmentValue ?? 0, isPreSelect: hasValue[index].isPreSelected ?? false, disable: true, isComingFrom: "Color")
                        }
                    }else{
                        //pass false
                        checkMarkDic.setValue(true, forKey: "\(index)")
                        if hasValue.indices.contains(indexNew){
                            newDelegate?.attributeCheckBoxData(valueId:  hasValue[index].id ?? 0, attributeId: mainID ?? 0, name: hasValue[index].name ?? "", colorSquare: hasValue[index].colorSquaresRGB ?? "", priceAdjustment: hasValue[index].priceAdjustmentValue ?? 0, isPreSelect: hasValue[index].isPreSelected ?? false, disable: false, isComingFrom: "Color")
                        }
                    }
                }
            }
        }else{
            //this is adding case
            if let colorNew = colorArray{
                if let indexNew = colorNew.firstIndex(where: { hexStringFromColor(color: $0).lowercased() == hexStringFromColor(color: colorNew[index]).lowercased()}) {
                    //that means already selected
                    if cell.layer.borderColor == UIColor.init(hex: Constant.kPrimaryThemeGreen).cgColor{
                        //pass true
                        checkMarkDic.setValue(false, forKey: "\(index)")
                        newDelegate?.attributeCheckBoxData(valueId:  0, attributeId: mainID ?? 0, name: name, colorSquare: colorNew[indexNew].HexString(), priceAdjustment: 0, isPreSelect: false, disable: true, isComingFrom: "Color")
                    }else{
                        //pass false
                        checkMarkDic.setValue(true, forKey: "\(index)")
                        newDelegate?.attributeCheckBoxData(valueId:  0, attributeId: mainID ?? 0, name: name, colorSquare: colorNew[indexNew].HexString(), priceAdjustment: 0, isPreSelect: false, disable: false, isComingFrom: "Color")
                    }
                }
                
            }
            
        }
        
        colorCollection.reloadData()
    }
    
}


extension UIColor {
    func HexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0

        getRed(&r, green: &g, blue: &b, alpha: &a)

        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0

        return NSString(format:"#%06x", rgb) as String
    }

    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")

        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }

}
