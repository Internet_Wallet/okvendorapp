//
//  AttributeRadioBox.swift
//  Attributes
//
//  Created by Tushar Lama on 12/01/2021.
//

import UIKit

protocol GetCellIndex {
    func getCellIndex(index: Int)
}

class AttributeRadioBox: UITableViewCell {
    
    @IBOutlet var radioButtonText: UILabel!
    var values: [Tushar_Values]?
    @IBOutlet var attributeView: UIView!
    var delegate: AttributeCheckBoxData?
    var productID: Int?
    @IBOutlet var radioButtonCollection: UICollectionView!
    var checkMarkDic = NSMutableDictionary()
    var initialIndex: Int?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.radioButtonCollection.register(UINib(nibName: "AttributeRadioButtonCell", bundle: nil), forCellWithReuseIdentifier: "AttributeRadioButtonCell")

        self.radioButtonCollection.delegate = self
        self.radioButtonCollection.dataSource = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

extension AttributeRadioBox: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let obj = values{
            return obj.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = radioButtonCollection.dequeueReusableCell(withReuseIdentifier: "AttributeRadioButtonCell", for: indexPath) as? AttributeRadioButtonCell else{
            return UICollectionViewCell()
        }
        cell.checkMarkButton.tag = indexPath.row
        cell.delegate = self
        
        if let obj = values{
            cell.checkMarkLabelCount.text = obj[indexPath.row].name ?? ""
        }
        
        if checkMarkDic.count>0{
            if checkMarkDic.value(forKey: "\(indexPath.row)") != nil{
                if checkMarkDic.value(forKey: "\(indexPath.row)") as! Bool{
                    if cell.checkMarkButton.backgroundColor == .clear{
                        initialIndex = indexPath.row
                        cell.checkMarkButton.backgroundColor = .black
                    }else{
                        initialIndex = nil
                        cell.checkMarkButton.backgroundColor = .clear
                    }
                }else{
                    cell.checkMarkButton.backgroundColor = .clear
                }
            }else{
                cell.checkMarkButton.backgroundColor = .clear
            }
        }else{
            if let obj = values{
                if obj[indexPath.row].disabled ?? false{
                    cell.checkMarkButton.backgroundColor = .clear
                }else{
                    cell.checkMarkButton.backgroundColor = .black
                    initialIndex = indexPath.row
                }
            }
        }
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let obj = values{
            let label = UILabel(frame: CGRect.zero)
            label.text = obj[indexPath.row].name ?? ""
            label.sizeToFit()
            if label.frame.size.width<80{
                return CGSize(width: 100 , height: 40)
            }else{
                return CGSize(width:  label.frame.size.width + 30, height: 40)
            }
        }else{
            return CGSize(width: 100 , height: 40)
        }
    }
    
    
    
    
    
}

extension AttributeRadioBox: GetCellIndex{
  
    func getCellIndex(index: Int){
        
        checkMarkDic.removeAllObjects()
        checkMarkDic.setValue(true, forKey: "\(index)")
        
        //case Edit
        if let editObj = values{
                 
            if let initialSelectedIndex = initialIndex{
              
                //case if user selected previously selected Data
                if index == initialSelectedIndex{
                    //removing from radio button list
                    delegate?.attributeCheckBoxData(valueId: editObj[index].id ?? 0, attributeId: productID ?? 0, name: editObj[index].name ?? "",colorSquare: editObj[index].colorSquaresRGB ?? "" ,priceAdjustment: editObj[index].priceAdjustmentValue ?? 0,isPreSelect: editObj[index].isPreSelected ?? false,disable: true, isComingFrom:"radio")
                }//this case will be user has not selected the radio button which was already selected
                else if index != initialIndex{
                    //adding list to the radio button check box
                    delegate?.attributeCheckBoxData(valueId: editObj[index].id ?? 0, attributeId: productID ?? 0, name: editObj[index].name ?? "",colorSquare: editObj[index].colorSquaresRGB ?? "" ,priceAdjustment: editObj[index].priceAdjustmentValue ?? 0,isPreSelect: editObj[index].isPreSelected ?? false,disable: false, isComingFrom:"radio")
                    
                    //removing the state of previously added data
                    delegate?.attributeCheckBoxData(valueId: editObj[initialSelectedIndex].id ?? 0, attributeId: productID ?? 0, name: editObj[initialSelectedIndex].name ?? "",colorSquare: editObj[initialSelectedIndex].colorSquaresRGB ?? "" ,priceAdjustment: editObj[initialSelectedIndex].priceAdjustmentValue ?? 0,isPreSelect: editObj[initialSelectedIndex].isPreSelected ?? false,disable: true, isComingFrom:"radio")
                    
                }
            }else{
                
                //this will work when initial index will be nil for the fist time user
                delegate?.attributeCheckBoxData(valueId: editObj[index].id ?? 0, attributeId: productID ?? 0, name: editObj[index].name ?? "",colorSquare: editObj[index].colorSquaresRGB ?? "" ,priceAdjustment: editObj[index].priceAdjustmentValue ?? 0,isPreSelect: editObj[index].isPreSelected ?? false,disable: false, isComingFrom:"radio")
            }
            
               
            
        }
        
//        if let obj = values{
//            if let data = obj[index].disabled{
//                if data{
//                    delegate?.attributeCheckBoxData(valueId: obj[index].id ?? 0, attributeId: productID ?? 0, name: obj[index].name ?? "",colorSquare: obj[index].colorSquaresRGB ?? "" ,priceAdjustment: obj[index].priceAdjustmentValue ?? 0,isPreSelect: obj[index].isPreSelected ?? false,disable: false, isComingFrom:"radio")
//                }else{
//                    delegate?.attributeCheckBoxData(valueId: obj[index].id ?? 0, attributeId: productID ?? 0, name: obj[index].name ?? "",colorSquare: obj[index].colorSquaresRGB ?? "" ,priceAdjustment: obj[index].priceAdjustmentValue ?? 0,isPreSelect: obj[index].isPreSelected ?? false,disable: true, isComingFrom:"radio")
//                }
//            }
//        }
        radioButtonCollection.reloadData()
        
    }
    
}
