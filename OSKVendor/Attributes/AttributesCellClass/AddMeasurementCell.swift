//
//  AddMeasurementCell.swift
//  Covid Zay Vendor
//
//  Created by Tushar Lama on 21/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import UIKit


protocol GetSizeAttributesDetails {
    func getDetails(index: Int,textFieldName: String)
}

class AddMeasurementCell: UITableViewCell {
    

    @IBOutlet var valueTestField: UITextField!
    @IBOutlet var nameTextField: UITextField!
    var delegate: CallColorPicker?
    var guideDelegate: GetSizeAttributesDetails?
    override func awakeFromNib() {
        super.awakeFromNib()
        valueTestField.delegate  = self
        nameTextField.delegate = self
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
 

extension AddMeasurementCell: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let name = nameTextField.text ?? ""
        let namevalue = valueTestField.text ?? ""
        guideDelegate?.getDetails(index: textField.tag, textFieldName: name + "-" + namevalue)
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
       
        if (updatedString?.count ?? 0) > 40 {
            return false
        }else{
            
            return true
        }
    }
    
}
