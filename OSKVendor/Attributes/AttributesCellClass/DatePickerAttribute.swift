//
//  DatePickerAttribute.swift
//  Attributes
//
//  Created by Tushar Lama on 14/01/2021.
//

import UIKit

class DatePickerAttribute: UITableViewCell {
    @IBOutlet weak var titleTopLabel: UILabel!
    
    @IBOutlet var bottomLabel: UILabel!
    @IBOutlet var datePickerTextField: UITextField!
    @IBOutlet var attributeView: UIView!
    var delegate: AttributeCheckBoxData?
    var datePicker = UIDatePicker()
    var productID: Int?
    var mainID: Int?
    var value: [Tushar_Values]?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        datePickerTextField.delegate = self
        datePicker.datePickerMode = .time
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        datePickerTextField.inputView = datePicker
    
        datePicker.addTarget(self, action: #selector(updateDateField(sender:)), for:
        .valueChanged)
        
    }
    
    
    @objc func updateDateField(sender: UIDatePicker) {
        datePickerTextField?.text = formatDateForDisplay(date: sender.date)
    }
    
    // Formats the date chosen with the date picker.
    fileprivate func formatDateForDisplay(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: date)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

extension DatePickerAttribute: UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let hasValue = value{
            if hasValue.indices.contains(0){
                delegate?.attributeCheckBoxData(valueId: hasValue[0].id ?? 0, attributeId: mainID ?? 0, name: datePickerTextField?.text ?? "", colorSquare: "", priceAdjustment: 0, isPreSelect: false, disable: false, isComingFrom: "DatePicker")
            }
        }else{
            delegate?.attributeCheckBoxData(valueId: 0, attributeId: mainID ?? 0, name: datePickerTextField?.text ?? "", colorSquare: "", priceAdjustment: 0, isPreSelect: false, disable: false, isComingFrom: "DatePicker")
        }
        
        
    }
    
    
    
}
