//
//  AttributeTextBox.swift
//  Attributes
//
//  Created by Tushar Lama on 14/01/2021.
//

import UIKit

protocol HeightForTextView {
    func heightOfTextView(height: CGFloat)

}

class AttributeTextBox: UITableViewCell {

    @IBOutlet var seperatorLabel: UILabel!
    @IBOutlet var texBoxLabel: UILabel!
    @IBOutlet var attributeView: UIView!
    @IBOutlet var descriptionTextview: UITextView!
    var productID: Int?
    var mainID: Int?
    var delgate:HeightForTextView?
    var newDelegate: AttributeCheckBoxData?
    var callGuideDelegate: CallColorPicker?
    @IBOutlet var arrowButton: UIButton!
    var name = ""
    var buttonClickedDelegate: CallColorPicker?
    var values: [Tushar_Values]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        descriptionTextview.delegate = self
    }
    
    @IBAction func onClickArrow(_ sender: Any) {
        buttonClickedDelegate?.callColorPicker(product: 0000000000)
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension AttributeTextBox: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if name == "Size By Measurement"{
           // callGuideDelegate?.callColorPicker(product: 0000000000)
        }
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        let fixedWidth: CGFloat = textView.frame.size.width
        let newSize: CGSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
           if let iuDelegate = self.delgate {
            iuDelegate.heightOfTextView(height: newSize.height)
            
            if name == "Size By Measurement"{
                
            }else{
                if let obj = values{
                    if obj.count>0{
                        newDelegate?.attributeCheckBoxData(valueId: obj[0].id ?? 0, attributeId: mainID ?? 0, name: textView.text ?? "", colorSquare: obj[0].colorSquaresRGB ?? "", priceAdjustment: obj[0].priceAdjustmentValue ?? 0, isPreSelect: obj[0].isPreSelected ?? false, disable: false, isComingFrom: "textBox")
                    }
                }else{
                    newDelegate?.attributeCheckBoxData(valueId: 0, attributeId: mainID ?? 0, name: textView.text ?? "", colorSquare: "", priceAdjustment: 0, isPreSelect: false, disable: false, isComingFrom: "textBox")
                }
            }
           }
       }
}
