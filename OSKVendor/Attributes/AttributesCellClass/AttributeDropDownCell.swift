//
//  AttributeDropDownCell.swift
//  Covid Zay Vendor
//
//  Created by Tushar Lama on 19/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class AttributeDropDownCell: UITableViewCell {
    @IBOutlet var dropDownName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
