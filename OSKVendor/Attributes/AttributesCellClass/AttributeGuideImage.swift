//
//  AttributeGuideImage.swift
//  Covid Zay Vendor
//
//  Created by Tushar Lama on 21/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import UIKit

protocol IncreaseCount {
    func increaseMeasurementCount()
}

class AttributeGuideImage: UITableViewCell {

    @IBOutlet var addMoreSizeLabel: UILabel!
    @IBOutlet var suggestionImageView: UIImageView!
    @IBOutlet var addButtonOutlet: UIButton!
    var delegate: IncreaseCount?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addButtonOutlet.isHidden = true
        addMoreSizeLabel.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onClickAddButton(_ sender: Any) {
        delegate?.increaseMeasurementCount()
    }
}
