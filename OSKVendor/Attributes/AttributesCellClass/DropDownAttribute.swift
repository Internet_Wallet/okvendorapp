//
//  DropDownAttribute.swift
//  Attributes
//
//  Created by Tushar Lama on 14/01/2021.
//

import UIKit
import DropDown
import IQKeyboardManagerSwift

protocol CallDropDown {
    func showDropDown(field: UITextField,dropDownvalue: [Tushar_Values],productID : Int)
}

class DropDownAttribute: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var dropDownTestField: UITextField!
    @IBOutlet var attributeView: UIView!
    var delegate: CallDropDown?
    var productID: Int?
    var dropDownvalue: [Tushar_Values]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dropDownTestField.delegate = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension DropDownAttribute: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let hasValue = dropDownvalue{
            delegate?.showDropDown(field: textField, dropDownvalue: hasValue,productID: productID ?? 0)
        }
        return false
    }
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        
//        
//        
//        
//        
//    }
    
}
