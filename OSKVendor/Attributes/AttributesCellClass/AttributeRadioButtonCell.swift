//
//  AttributeRadioButtonCell.swift
//  Attributes
//
//  Created by Tushar Lama on 12/01/2021.
//

import UIKit

class AttributeRadioButtonCell: UICollectionViewCell {
    @IBOutlet var checkMarkButton: UIButton!
    @IBOutlet var checkMarkLabelCount: UILabel!
    var delegate: GetCellIndex?
    
    
    override func awakeFromNib() {
        checkMarkButton.layer.cornerRadius = checkMarkButton.frame.size.width/2
        checkMarkButton.clipsToBounds = true
        self.roundedBorderWithShadow(borderColor: UIColor.init(hex: Constant.kPrimaryThemeGreen) ,shadowColor: .clear,backgroundColor: .clear)
    }
   
    
    @IBAction func onClickRadioButton(_ sender: UIButton) {
        delegate?.getCellIndex(index: checkMarkButton.tag)
        
    }
}
