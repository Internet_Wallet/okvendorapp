//
//  ColorBox.swift
//  Covid Zay Vendor
//
//  Created by Tushar Lama on 20/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import UIKit

class ColorBox: UICollectionViewCell {
    
    @IBOutlet var colorBoxButton: UIButton!
    var delegate: GetCellIndex?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = 4.0
        // Initialization code
    }
    
    
    @IBAction func onClickColorBox(_ sender: Any) {
        delegate?.getCellIndex(index: colorBoxButton.tag)
    }
    
}
