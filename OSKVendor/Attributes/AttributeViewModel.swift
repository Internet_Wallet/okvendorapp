//
//  AttributeViewModel.swift
//  Covid Zay Vendor
//
//  Created by Tushar Lama on 18/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import Foundation

class AttributeViewModel{
    var attributeParamModel = [AttributeModel]()
    var dictArray = Array<Dictionary<String,Any>>()
    
    

    
    func getAttributeParam(productAttributeValueId: Int,productAttributeMappingId: Int,name: String,color: String,priceAdjustment: Int,isPreSelected: Bool,disable: Bool) -> Dictionary<String, Any>{
        var dict = Dictionary<String, Any>()
        dict["ProductAttributeValueId"] = productAttributeValueId
        dict["ProductAttributeMappingId"] = productAttributeMappingId
        dict["Name"] = name
        dict["ColorSquaresRgb"] = color
        dict["PriceAdjustment"] = priceAdjustment
        dict["IsPreSelected"] = isPreSelected
        dict["Disabled"] = disable
        return dict
    }
    
    
    
    
}
