//
//  Constant.swift
//  Covid Zay Vendor
//
//  Created by Tushar Lama on 14/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import Foundation




struct Constant {
    
    static let kAttributeViewController = "AttributeViewController"
    static let kPrimaryThemeGreen = "0AB14B"
}

struct URLConstant{
    
    static func updateProductAttributes() -> String{
        return APIManagerClient.sharedInstance.base_url + "/vendor/addorupdateproductattributevalues"
    }
    
    static func updateProductFromVendor() -> String{
        return APIManagerClient.sharedInstance.base_url + "/vendor/updateproductfromvendor"
    }
}

