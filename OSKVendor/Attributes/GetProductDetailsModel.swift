//
//  GetProductDetailsModel.swift
//  Covid Zay Vendor
//
//  Created by Tushar Lama on 16/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import Foundation


open class GetProductDetailsModel{
    
    public var statusCode : Int?
    public var successMessage : String?
    var data: ProductData?
    
    
    public init(response: NSDictionary?){
        
        if let hasValue = response{
            if hasValue.count>0{
                self.statusCode = hasValue["StatusCode"] as? Int
                self.successMessage = hasValue["SuccessMessage"] as? String
                self.data = ProductData(response: hasValue["Data"] as? NSDictionary)
            }
        }
    }
    
}



open class ProductData{
        var productAttributes: [Tushar_ProductAttributes]?
        var productReviewOverview: Tushar_ProductReviewOverview?
        var barCodeImageURL: String?
        var freeShippingNotificationEnabled: Bool?
        var barcode: String?
        var giftCard: Tushar_GiftCard?
        var shortDescription: Any?
        var stockAvailability: String?
        var rentalStartDate: Any?
        var url: String?
        var compareProductsEnabled: Bool?
        var rentalEndDate: Any?
        var sizeGuideImagePath: String?
        var customProperties: Tushar_CustomProperties?
        var productPrice: Tushar_ProductPrice?
        var productCategoryName, deliveryDate: String?
        var fullDescription: Any?
      //  var productSpecifications: [Any?]
        var isFreeShipping: Bool?
        var defaultPictureModel: Tushar_PictureModel?
        var isRental, published: Bool?
        var otherLanguageDataUnicode: String?
     //   var productManufacturers, associatedProducts: [Any?]
        var name: String?
        var id: Int?
        var addToCart: Tushar_AddToCart?
        var breadcrumb: Tushar_Breadcrumb?
      //  var tierPrices: [Any?]
        var hasSampleDownload, emailAFriendEnabled: Bool?
        var vendorModel: Tushar_VendorModel?
        var manufacturerPartNumber: Any?
        var uom: String?
        var showVendor: Bool?
      //  var productTags: [Any?]
        var availableEndDateTimeUTC: Any?
        var nextProduct: Int?
        var isShipEnabled: Bool?
        var cgmItemID: Any?
        var displayBackInStockSubscription: Bool?
        var pictureModels: [Tushar_PictureModelElement]?
         var videoModels: [Tushar_VideoModelElement]?
        var markAsNew: Bool?
        var availableStartDateTimeUTC: Any?
        var previousProduct: Int?
        var defaultPictureZoomEnabled: Bool?
        var quantity: Tushar_Quantity?
        var showManufacturerPartNumber: Bool?
    
    public init(response: NSDictionary?){
        if let hasValue = response{
            if hasValue.count>0{
                if let hasProductAttribute = hasValue["ProductAttributes"] as? Array<Any>{
                    if hasProductAttribute.count>0{
                        self.productAttributes =  hasProductAttribute.compactMap({Tushar_ProductAttributes(response: $0 as? NSDictionary)})
                    }
                }
                
                self.productReviewOverview = Tushar_ProductReviewOverview(response: hasValue["ProductReviewOverview"] as? NSDictionary)
                self.barCodeImageURL = hasValue["barCodeImageURL"] as? String
                self.freeShippingNotificationEnabled = hasValue["FreeShippingNotificationEnabled"] as? Bool
                self.barcode = hasValue["Barcode"] as? String
                self.giftCard = Tushar_GiftCard(response: hasValue["GiftCard"] as? NSDictionary)
                self.shortDescription = hasValue["ShortDescription"] as Any
                self.stockAvailability = hasValue["StockAvailability"] as? String
                self.rentalStartDate = hasValue["StockAvailability"] as Any
                self.url = hasValue["Url"] as? String
                self.compareProductsEnabled = hasValue["CompareProductsEnabled"] as? Bool
                self.rentalEndDate = hasValue["RentalEndDate"] as Any
                self.sizeGuideImagePath = hasValue["SizeGuideImagePath"] as? String
                if let hasCustomProperties = hasValue["CustomProperties"] as? NSDictionary{
                    self.customProperties = Tushar_CustomProperties(response: hasCustomProperties)
                }
                self.productPrice = Tushar_ProductPrice(response: hasValue["ProductPrice"] as? NSDictionary)
                self.productCategoryName = hasValue["ProductCategoryName"] as? String
                self.deliveryDate = hasValue["DeliveryDate"] as? String
                self.fullDescription = hasValue["FullDescription"] as Any
                self.isFreeShipping = hasValue["IsFreeShipping"] as? Bool
                self.defaultPictureModel = Tushar_PictureModel(response: hasValue["DefaultPictureModel"] as? NSDictionary)
                self.isRental = hasValue["IsRental"] as? Bool
                self.published = hasValue["Published"] as? Bool
                self.otherLanguageDataUnicode = hasValue["OtherLanguageDataUnicode"] as? String
                self.name = hasValue["Name"] as? String
                self.id = hasValue["Id"] as? Int
                self.addToCart = Tushar_AddToCart(response: hasValue["AddToCart"] as? NSDictionary)
                self.breadcrumb = Tushar_Breadcrumb(response: hasValue["Breadcrumb"] as? NSDictionary)
                self.hasSampleDownload = hasValue["HasSampleDownload"] as? Bool
                self.emailAFriendEnabled = hasValue["EmailAFriendEnabled"] as? Bool
                self.vendorModel = Tushar_VendorModel(response: hasValue["VendorModel"] as? NSDictionary)
                self.manufacturerPartNumber = hasValue["ManufacturerPartNumber"] as Any
                self.uom = hasValue["UOM"] as? String
                self.showVendor = hasValue["ShowVendor"] as? Bool
                self.availableEndDateTimeUTC = hasValue["AvailableEndDateTimeUtc"] as Any
                self.nextProduct = hasValue["NextProduct"] as? Int
                self.isShipEnabled = hasValue["IsShipEnabled"] as? Bool
                self.cgmItemID = hasValue["CGMItemID"] as Any
                self.displayBackInStockSubscription = hasValue["DisplayBackInStockSubscription"] as? Bool
                if let hasPictureModel = hasValue["PictureModels"] as? Array<Any>{
                    if hasPictureModel.count>0{
                        self.pictureModels =  hasPictureModel.compactMap({Tushar_PictureModelElement(response: $0 as? NSDictionary)})
                    }
                }
                self.markAsNew = hasValue["MarkAsNew"] as? Bool
                self.availableStartDateTimeUTC = hasValue["AvailableStartDateTimeUtc"] as Any
                self.previousProduct = hasValue["PreviousProduct"] as? Int
                if let hasVideoModel = hasValue["VideoModels"] as? Array<Any>{
                    if hasVideoModel.count>0{
                        self.videoModels =  hasVideoModel.compactMap({Tushar_VideoModelElement(response: $0 as? NSDictionary)})
                    }
                }
                
                self.defaultPictureZoomEnabled = hasValue["DefaultPictureZoomEnabled"] as? Bool
                self.quantity = Tushar_Quantity(response: hasValue["Quantity"] as? NSDictionary)
                self.showManufacturerPartNumber =  hasValue["ShowManufacturerPartNumber"] as? Bool
            }}
        
    }
    
    
    
}

open class Tushar_ProductReviewOverview {
    var allowCustomerReviews: Bool?
    var ratingSum, productID, totalReviews: Int?
    var ratingURLFromGSMArena: Any?

    init(response: NSDictionary?) {
        if let hasValue = response{
            if hasValue.count>0{
                self.allowCustomerReviews = hasValue["AllowCustomerReviews"] as? Bool
                self.ratingSum = hasValue["RatingSum"] as? Int
                self.productID = hasValue["ProductId"] as? Int
                self.totalReviews = hasValue["TotalReviews"] as? Int
                self.ratingURLFromGSMArena = hasValue["RatingUrlFromGSMArena"] as Any
            }
        }
    }
}


open class Tushar_ProductAttributes{
    
       var id: Int?
       var defaultValue: Any?
       var productAttributeID: Int?
       var customProperties: Tushar_CustomProperties?
       var attributeControlType: Int?
       var selectedYear, selectedMonth: Any?
       var allowedFileExtensions: [Any]?
       var isExpandable: Bool?
       var values: [Tushar_Values]?
       var productID: Int?
       var selectedDay: Any?
       var textPrompt: String?
       var isRequired: Bool?
       var name: String?
       var productAttributeDescription: Any?
    
    
    public init(response: NSDictionary?){
        if let hasValue = response{
            if hasValue.count>0{
                self.id = hasValue["Id"] as? Int
                self.defaultValue = hasValue["DefaultValue"] as Any
                self.productAttributeID = hasValue["ProductAttributeId"] as? Int
               
                if let hasCustomProperties = hasValue["CustomProperties"] as? NSDictionary{
                    self.customProperties = Tushar_CustomProperties(response: hasCustomProperties)
                }
                
                if let hasCustomValues = hasValue["Values"] as? Array<Any>{
                    if hasCustomValues.count>0{
                        self.values =  hasCustomValues.compactMap({Tushar_Values(response: $0 as? NSDictionary)})
                    }
                }
                self.attributeControlType = hasValue["AttributeControlType"] as? Int
                self.selectedYear = hasValue["SelectedYear"] as Any
                self.selectedMonth = hasValue["SelectedMonth"] as Any
                self.allowedFileExtensions = hasValue["AllowedFileExtensions"] as? [Any]
                self.isExpandable = hasValue["IsExpandable"] as? Bool
                self.productID = hasValue["ProductId"] as? Int
                self.selectedDay = hasValue["SelectedDay"] as Any
                self.textPrompt = hasValue["TextPrompt"] as? String
                self.isRequired = hasValue["IsRequired"] as? Bool
                self.productAttributeDescription = hasValue["Description"] as Any
                self.name = hasValue["Name"] as? String
            }
        }
    }
    
}


open class Tushar_CustomProperties{
    
    
    public init(response: NSDictionary?){
        
    }
    
}


open class Tushar_Values{
    
     var isPreSelected: Bool?
     var colorSquaresRGB: String?
     var id: Int?
     var priceAdjustment: Any?
     var size: String?
     var disabled: Bool?
     var customProperties: Tushar_CustomProperties?
     var priceAdjustmentValue: Int?
     var pictureModel: Tushar_PictureModel?
     var name: String?
    
    
    public init(response: NSDictionary?){
        if let hasValue = response{
            if hasValue.count>0{
                self.isPreSelected = hasValue["IsPreSelected"] as? Bool
                self.colorSquaresRGB = hasValue["ColorSquaresRgb"] as? String
                self.id = hasValue["Id"] as? Int
                self.priceAdjustment = hasValue["PriceAdjustment"] as Any
                self.size = hasValue["Size"] as? String
                self.disabled = hasValue["Disabled"] as? Bool
                if let hasCustomProperties = hasValue["CustomProperties"] as? NSDictionary{
                    self.customProperties = Tushar_CustomProperties(response: hasCustomProperties)
                }
                if let hasPictureModel = hasValue["PictureModel"] as? NSDictionary{
                    self.pictureModel = Tushar_PictureModel(response: hasPictureModel)
                }
                self.priceAdjustmentValue = hasValue["PriceAdjustmentValue"] as? Int
                 self.name = hasValue["Name"] as? String
            }
        }
    }
    
}


open class Tushar_PictureModel{
    var title,fullSizeImageURL,imageURL: String?
    var form, alternateText: Any?
    var id: Int?
    var customProperties: Tushar_CustomProperties?
    
    public init(response: NSDictionary?){
        if let hasValue = response{
            if hasValue.count>0{
                self.title = hasValue["Title"] as? String
                self.fullSizeImageURL = hasValue["FullSizeImageUrl"] as? String
                self.imageURL = hasValue["ImageUrl"] as? String
                self.form = hasValue["Form"] as Any
                self.alternateText = hasValue["AlternateText"] as Any
                self.id = hasValue["Id"] as? Int
                if let hasCustomProperties = hasValue["CustomProperties"] as? NSDictionary{
                    self.customProperties = Tushar_CustomProperties(response: hasCustomProperties)
                }
            }}
    }
}


open class Tushar_ProductReviewOverView{
       var allowCustomerReviews: Bool?
       var ratingSum, productID, totalReviews: Int?
       var ratingURLFromGSMArena: Any?
    
    public init(response: NSDictionary?){
        if let hasValue = response{
            if hasValue.count>0{
                self.allowCustomerReviews = hasValue["AllowCustomerReviews"] as? Bool
                self.ratingSum = hasValue["RatingSum"] as? Int
                self.productID = hasValue["ProductId"] as? Int
                self.ratingURLFromGSMArena = hasValue["RatingUrlFromGSMArena"] as Any
                self.totalReviews = hasValue["TotalReviews"] as? Int
                
            }
        }
    }
}


open class Tushar_GiftCard{
    var giftCardType: Int?
    var isGiftCard: Bool?
    var recipientName, senderEmail: String?
    var customProperties: Tushar_CustomProperties?
    var message, senderName, recipientEmail: String?
    
    
    public init(response: NSDictionary?){
        if let hasValue = response{
            if hasValue.count>0{
                self.giftCardType = hasValue["GiftCardType"] as? Int
                self.isGiftCard = hasValue["IsGiftCard"] as? Bool
                self.recipientName = hasValue["RecipientName"] as? String
                self.senderEmail = hasValue["SenderEmail"] as? String
                if let hasCustomProperties = hasValue["CustomProperties"] as? NSDictionary{
                    self.customProperties = Tushar_CustomProperties(response: hasCustomProperties)
                }
                self.message = hasValue["Message"] as? String
                self.senderName = hasValue["SenderName"] as? String
                self.recipientEmail = hasValue["RecipientEmail"] as? String
            }
        }
    }
    
}


open class Tushar_DefaultPictureModel{
       var title: String?
       var fullSizeImageURL, imageURL,alternateText: String?
       var form: Any?
       var id: Int?
       var customProperties: Tushar_CustomProperties?
    
    
    public init(response: NSDictionary?){
        if let hasValue = response{
            if hasValue.count>0{
                self.title = hasValue["Title"] as? String
                self.fullSizeImageURL = hasValue["FullSizeImageUrl"] as? String
                self.imageURL = hasValue["ImageUrl"] as? String
                self.form = hasValue["Form"] as Any
                self.alternateText = hasValue["AlternateText"] as? String
                self.id = hasValue["Id"] as? Int
                if let hasCustomProperties = hasValue["CustomProperties"] as? NSDictionary{
                    self.customProperties = Tushar_CustomProperties(response: hasCustomProperties)
                }
            }
        }
    }
    
}


open class Tushar_ProductPrice{
    var priceWithDiscountValue: Int?
    var price: String?
    var displayTaxShippingInfo: Bool?
    var discountOfferName: String?
    var rentalPrice, priceWithDiscount: Any?
    var hidePrices: Bool?
    var discountPercentage: Int?
    var customProperties: Tushar_CustomProperties?
    var currencyCode: String?
    var isRental, callForPrice: Bool?
    var productID: Int?
    var oldPrice: Any?
    var customerEntersPrice: Bool?
    var basePricePAngV: Any?
    var priceValue: Int?
    
    public init(response: NSDictionary?){
        if let hasValue = response{
            if hasValue.count>0{
                self.priceWithDiscountValue = hasValue["PriceWithDiscountValue"] as? Int
                self.price = hasValue["Price"] as? String
                self.displayTaxShippingInfo = hasValue["DisplayTaxShippingInfo"] as? Bool
                self.discountOfferName = hasValue["DiscountOfferName"] as? String
                self.rentalPrice = hasValue["RentalPrice"] as Any
                self.priceWithDiscount = hasValue["PriceWithDiscount"] as Any
                self.hidePrices = hasValue["HidePrices"] as? Bool
                self.discountPercentage = hasValue["DiscountPercentage"] as? Int
                if let hasCustomProperties = hasValue["CustomProperties"] as? NSDictionary{
                    self.customProperties = Tushar_CustomProperties(response: hasCustomProperties)
                }
                self.currencyCode = hasValue["CurrencyCode"] as? String
                self.isRental = hasValue["IsRental"] as? Bool
                self.callForPrice = hasValue["CallForPrice"] as? Bool
                self.productID = hasValue["ProductId"] as? Int
                self.oldPrice = hasValue["OldPrice"] as Any
                self.customerEntersPrice = hasValue["CustomerEntersPrice"] as? Bool
                self.basePricePAngV = hasValue["BasePricePAngV"] as Any
                self.priceValue = hasValue["PriceValue"] as? Int
                
            }
            
        }
    }
    
}


open class Tushar_AddToCart{
    
        var disableBuyButton, availableForPreOrder: Bool?
        var customerEnteredPriceRange: Any?
        var enteredQuantity, customerEnteredPrice: Int?
        var preOrderAvailabilityStartDateTimeUTC: Any?
        var isRental: Bool?
        var updatedShoppingCartItemID, productID: Int?
        var customerEntersPrice, disableWishlistButton, isWishList: Bool?
    
    public init(response: NSDictionary?){
        if let hasValue = response{
            if hasValue.count>0{
                self.disableBuyButton = hasValue["DisableBuyButton"] as? Bool
                self.availableForPreOrder = hasValue["AvailableForPreOrder"] as? Bool
                self.customerEnteredPriceRange = hasValue["CustomerEnteredPriceRange"] as Any
                self.enteredQuantity = hasValue["EnteredQuantity"] as? Int
                self.customerEnteredPrice = hasValue["CustomerEnteredPrice"] as? Int
                self.preOrderAvailabilityStartDateTimeUTC = hasValue["PreOrderAvailabilityStartDateTimeUtc"] as Any
                self.isRental = hasValue["IsRental"] as? Bool
                self.updatedShoppingCartItemID = hasValue["UpdatedShoppingCartItemId"] as? Int
                self.productID = hasValue["ProductId"] as? Int
                self.customerEntersPrice = hasValue["CustomerEntersPrice"] as? Bool
                self.disableWishlistButton = hasValue["DisableWishlistButton"] as? Bool
                self.isWishList = hasValue["IsWishList"] as? Bool
            }
        }
        
    }
}


open class Tushar_Breadcrumb{
    
        var productName: String?
        var enabled: Bool?
        var productID: Int?
        var customProperties: Tushar_CustomProperties?
       var categoryBreadcrumb: [Tushar_CategoryBreadcrumb]?
        var productSEName: String?
    
    public init(response: NSDictionary?){
        if let hasValue = response{
            if hasValue.count>0{
                self.productName = hasValue["ProductName"] as? String
                self.enabled = hasValue["Enabled"] as? Bool
                self.productID = hasValue["ProductId"] as? Int
                if let hasCustomProperties = hasValue["CustomProperties"] as? NSDictionary{
                    self.customProperties = Tushar_CustomProperties(response: hasCustomProperties)
                }
                if let hasCustomValues = hasValue["CategoryBreadcrumb"] as? Array<Any>{
                    if hasCustomValues.count>0{
                        self.categoryBreadcrumb =  hasCustomValues.compactMap({Tushar_CategoryBreadcrumb(response: $0 as? NSDictionary)})
                    }
                }
                self.productSEName = hasValue["ProductSeName"] as? String
            }
        }
        
    }
}

open class Tushar_CategoryBreadcrumb{
        var id: Int?
        var name,seName: String?
        var includeInTopMenu: Bool?
        var form, numberOfProducts: Any?
        var customProperties: Tushar_CustomProperties?
    
    public init(response: NSDictionary?){
        if let hasValue = response{
            if hasValue.count>0{
                self.id = hasValue["Id"] as? Int
                self.name = hasValue["Name"] as? String
                self.includeInTopMenu = hasValue["IncludeInTopMenu"] as? Bool
                self.seName = hasValue["SeName"] as? String
                self.form = hasValue["Form"] as Any
                self.numberOfProducts = hasValue["NumberOfProducts"] as Any
                if let hasCustomProperties = hasValue["CustomProperties"] as? NSDictionary{
                    self.customProperties = Tushar_CustomProperties(response: hasCustomProperties)
                }
            }
        }
    }
    
}

open class Tushar_VendorModel{
       var id: Int?
       var name: String?
       var verify: Bool?
       var customProperties: Tushar_CustomProperties?
       var seName: String?
    
    public init(response: NSDictionary?){
        if let hasValue = response{
            if hasValue.count>0{
                self.id = hasValue["Id"] as? Int
                self.name = hasValue["Name"] as? String
                self.verify = hasValue["Verify"] as? Bool
                self.seName = hasValue["SeName"] as? String
                if let hasCustomProperties = hasValue["CustomProperties"] as? NSDictionary{
                    self.customProperties = Tushar_CustomProperties(response: hasCustomProperties)
                }
            }
        }
    }
    
}




open class Tushar_VideoModelElement{
    
    var productID, videoID: Int?
    var videoURL: String?
    var displayOrder: Int?
    var name: String?
    var id: Int?
    var customProperties: Tushar_CustomProperties?
    
    public init(response: NSDictionary?){
        if let hasValue = response{
            if hasValue.count>0{
                self.productID = hasValue["ProductId"] as? Int
                self.videoID = hasValue["videoID"] as? Int
                self.videoURL = hasValue["VideoUrl"] as? String
                self.displayOrder = hasValue["DisplayOrder"] as? Int
                self.name = hasValue["Name"] as? String
                self.id = hasValue["Id"] as? Int
                if let hasCustomProperties = hasValue["CustomProperties"] as? NSDictionary{
                    self.customProperties = Tushar_CustomProperties(response: hasCustomProperties)
                }
            }
        }
    }
    
}


open class Tushar_PictureModelElement{
    
       var overrideTitleAttribute, file: Any?
       var productID: Int?
       var pictureURL: String?
       var displayOrder, id: Int?
       var customProperties: Tushar_CustomProperties?
       var overrideAltAttribute: Any?
       var pictureID: Int?
    
    public init(response: NSDictionary?){
        if let hasValue = response{
            if hasValue.count>0{
                self.overrideTitleAttribute = hasValue["Id"] as Any
                self.file = hasValue["Id"] as Any
                self.productID = hasValue["ProductId"] as? Int
                self.pictureURL = hasValue["PictureUrl"] as? String
                self.displayOrder = hasValue["DisplayOrder"] as? Int
                self.id = hasValue["Id"] as? Int
                self.overrideAltAttribute = hasValue["Id"] as Any
                self.pictureID = hasValue["PictureId"] as? Int
                if let hasCustomProperties = hasValue["CustomProperties"] as? NSDictionary{
                    self.customProperties = Tushar_CustomProperties(response: hasCustomProperties)
                }
            }
        }
    }
    
}

open class Tushar_Quantity{
    
    var orderMinimumQuantity, orderMaximumQuantity, stockQuantity: Int?
    
    public init(response: NSDictionary?){
        if let hasValue = response{
            if hasValue.count>0{
                self.orderMinimumQuantity = hasValue["OrderMinimumQuantity"] as? Int
                self.orderMaximumQuantity = hasValue["OrderMaximumQuantity"] as? Int
                self.stockQuantity = hasValue["StockQuantity"] as? Int
            }
        }
    }
    
}

