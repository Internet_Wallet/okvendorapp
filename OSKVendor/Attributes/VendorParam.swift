//
//  VendorParam.swift
//  Covid Zay Vendor
//
//  Created by Tushar Lama on 18/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//

import Foundation

//ProductAttributeValueId": 43645,
//                "ProductAttributeMappingId": 17263,
//                "Name": "newbkacakck",
//                "ColorSquaresRgb": "#000002",
//                "PriceAdjustment": 2000,
//                "IsPreSelected": false,
//                "Disabled": false

open class AttributeModel{
    var productAttributeValueId: Int?
    var productAttributeMappingId: Int?
    var name: String?
    var color: String?
    var priceAdjustment: Int?
    var isPreSelected: Bool?
    var disable: Bool?
    
    public init(productAttributeValueId: Int,productAttributeMappingId: Int,name: String,color: String,priceAdjustment: Int,isPreSelected: Bool,disable: Bool){
        self.productAttributeValueId = productAttributeValueId
        self.productAttributeMappingId = productAttributeMappingId
        self.name = name
        self.color = color
        self.priceAdjustment = priceAdjustment
        self.isPreSelected = isPreSelected
        self.disable = disable
    }
    
}
