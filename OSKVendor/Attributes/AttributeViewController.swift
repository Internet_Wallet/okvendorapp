//
//  AttributeViewController.swift
//  Covid Zay Vendor
//
//  Created by Tushar Lama on 14/01/2021.
//  Copyright © 2021 Avaneesh Awasthi. All rights reserved.
//\


//            DispatchQueue.main.async {
//                var vcObj: AttributesSizeGuideVC?
//                vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.kAttributeViewController) as? AttributesSizeGuideVC
//                if let validObj = vcObj {
//                    if let attributeArray = self.productDetailsModel?.data?.productAttributes{
//                        validObj.values = attributeArray[product].values
//                        self.navigationController?.pushViewController(validObj, animated: true)
//                    }
//                }
//            }

import UIKit
import DropDown
import EFColorPicker
import Alamofire
import SwiftyJSON

protocol ChangeSaveButtonState {
    func changeSaveButtonState(callApi: Bool,sizeAttributeModel: [AttributeModel])
}

class AttributeViewController: UIViewController {
    
    var viewColorPicker = UIView()
    var btnSelectColorPcker = UIButton()
    var btnSelectColorPckerCancel = UIButton()
    var colorPicker = EFColorSelectionViewController()
    var selectedcolor: UIColor?
    var colorList = [UIColor]()
    var isShowColorSelection = true
    var saveDelegate: ChangeSaveButtonState?
    var alertVC : SAlertController?
    
    // let dropDown = DropDown()
    @IBOutlet var attributeTable: UITableView!
    var productDetailsModel: GetProductDetailsModel?
    var cellNameArray = [String]()
    var customCellArray = [UITableViewCell]()
    var textViewHeight = CGFloat()
    @IBOutlet var cancelButtonOutlet: UIButton!
    var attributeParamModel = [AttributeModel]()
    var radioBoxData = [AttributeModel]()
    var dropDownData = [AttributeModel]()
    var sizeAttributeModel = [AttributeModel]()
    var viewModel = AttributeViewModel()
    var apiManagerClient : APIManagerClient?
    var aPIManager = APIManager()
    var dictArray = Array<Dictionary<String,Any>>()
    var stringNameArray = [String]()
    var dropDownvalue = [Tushar_Values]()
    var dropDownProductId = 0
    let dropDown = DropDown()
    var tag: Int?
    var colorProductID: Int?
    var sizeGuide: [Tushar_Values]?
    var dropDownName = ""
    var sizeGuideImage = ""
    var sizeChartMainId: Int?
   
    @IBOutlet var saveButton: UIButton!{
        didSet{
            saveButton.setTitle("Save".localized, for: .normal)
        }
    }
    @IBOutlet var cancelButton: UIButton!{
        didSet{
            cancelButton.setTitle("Cancel".localized, for: .normal)
        }
    }
    
    
    @IBOutlet var saveButtonOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
        selectedcolor = UIColor.white
        
        
                
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            dropDownData.removeAll()
            
            if let idx = attributeParamModel.firstIndex(where: { $0.productAttributeMappingId == dropDownProductId}) {
                attributeParamModel.remove(at: idx)
                
                
                attributeParamModel.append(AttributeModel(productAttributeValueId: dropDownvalue[index].id ?? 0, productAttributeMappingId: dropDownProductId, name: dropDownvalue[index].name ?? "", color: dropDownvalue[index].colorSquaresRGB ?? "", priceAdjustment: dropDownvalue[index].priceAdjustmentValue ?? 0, isPreSelected: dropDownvalue[index].isPreSelected ?? false, disable: false))
            }else{
                attributeParamModel.append(AttributeModel(productAttributeValueId: dropDownvalue[index].id ?? 0, productAttributeMappingId: dropDownProductId, name: dropDownvalue[index].name ?? "", color: dropDownvalue[index].colorSquaresRGB ?? "", priceAdjustment: dropDownvalue[index].priceAdjustmentValue ?? 0, isPreSelected: dropDownvalue[index].isPreSelected ?? false, disable: false))
            }
            
            DispatchQueue.main.async {
                if let tagValue = tag{
                    let indexPath = IndexPath(row: tagValue, section: 0)
                    guard let cell = self.attributeTable.cellForRow(at: indexPath) as? DropDownAttribute else{
                        return
                    }
                    dropDownName = dropDownvalue[index].name ?? ""
                    cell.dropDownTestField.text = dropDownvalue[index].name ?? ""
                }
            }
        }
        
      
        if let obj = productDetailsModel{
            if let attribute = obj.data?.productAttributes{
                for i in 0..<attribute.count{
                    if attribute[i].attributeControlType == 1{
                        if let dataaValue = attribute[i].values{
                            for j in 0..<dataaValue.count{
                                if dataaValue[j].disabled ?? false{
                                }else{
                                    attributeParamModel.append(AttributeModel(productAttributeValueId: dataaValue[j].id ?? 0, productAttributeMappingId: attribute[i].id ?? 0, name: dataaValue[j].name ?? "", color: dataaValue[j].colorSquaresRGB ?? "", priceAdjustment: dataaValue[j].priceAdjustmentValue ?? 0, isPreSelected: dataaValue[j].isPreSelected ?? false, disable: false))
                                    dropDownName = dataaValue[j].name ?? ""
                                    break
                                }
                            }
                        }
                        break
                    }
                }
            }
        }
        
        
        
//        //this will work for edit case only because this will have value
        if let obj = productDetailsModel{
            if let attribute = obj.data?.productAttributes{
                for i in 0..<attribute.count{
                    if attribute[i].attributeControlType == 40{
                        if let newValues = attribute[i].values{
                            if newValues.count>0{
                                for j in 0..<newValues.count{
                                    self.colorList.append(UIColor(hex: newValues[j].colorSquaresRGB ?? ""))
                                }
                            }
                        }
                        break
                    }
                }
            }
        }
        
        self.attributeTable.register(UINib(nibName: "AttributeCheckBox", bundle: nil), forCellReuseIdentifier: "AttributeCheckBox")
        self.attributeTable.register(UINib(nibName: "AttributeRadioBox", bundle: nil), forCellReuseIdentifier: "AttributeRadioBox")
        self.attributeTable.register(UINib(nibName: "AttributeTextBox", bundle: nil), forCellReuseIdentifier: "AttributeTextBox")
        self.attributeTable.register(UINib(nibName: "DatePickerAttribute", bundle: nil), forCellReuseIdentifier: "DatePickerAttribute")
        self.attributeTable.register(UINib(nibName: "ColorPickerAttribute", bundle: nil), forCellReuseIdentifier: "ColorPickerAttribute")
        self.attributeTable.register(UINib(nibName: "DropDownAttribute", bundle: nil), forCellReuseIdentifier: "DropDownAttribute")
        
        attributeTable.separatorColor = .clear
        attributeTable.delegate = self
        attributeTable.dataSource = self
        attributeTable.reloadData()
        
    }
    
    
 
    @objc func onClickColorPickerAciton(_ button: UIButton) {
        
        //button tag 10 means user has pressed the done button
        if button.tag == 10 {
            
            if let hasColor = selectedcolor{
                self.colorList.append(hasColor)
                selectedcolor = nil
                attributeParamModel.append(AttributeModel(productAttributeValueId: 0, productAttributeMappingId: colorProductID ?? 0, name: "", color: hasColor.HexString(), priceAdjustment: 0, isPreSelected: false, disable: false))
                self.attributeTable.reloadData()
            }
        }else {
            selectedcolor = nil
        }
        
        if #available(iOS 11.0, *) {
            if self.viewColorPicker.contains(self.viewColorPicker) {
                self.colorPicker.view.removeFromSuperview()
                self.viewColorPicker.removeFromSuperview()
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    fileprivate func callColorPickerNew(){
        
        self.viewColorPicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:self.view.frame.size.height)
        viewColorPicker.backgroundColor = .white
        
        colorPicker.delegate = self
        colorPicker.view.frame = CGRect(x: 10, y: 100, width: self.viewColorPicker.frame.size.width - 20, height:self.viewColorPicker.frame.size.height - 80)
        colorPicker.view.backgroundColor = .white
        colorPicker.setMode(mode: .hsb)
        self.addChild(colorPicker)
        colorPicker.didMove(toParent: self)
        self.viewColorPicker.addSubview(colorPicker.view)
        
        btnSelectColorPcker.frame = CGRect(x: 10, y: colorPicker.view.frame.origin.x + colorPicker.view.frame.size.height, width: ((self.viewColorPicker.frame.size.width - 20 ) / 2), height: 50)
        btnSelectColorPcker.tag = 10
        btnSelectColorPcker.backgroundColor = #colorLiteral(red: 0, green: 0.7320501804, blue: 0.365727663, alpha: 1)
        btnSelectColorPcker.layer.cornerRadius = 5
        btnSelectColorPcker.layer.masksToBounds = true
        btnSelectColorPcker.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        btnSelectColorPcker.setTitle("Done".localized, for: .normal)
        btnSelectColorPcker.setTitleColor(.white, for: .normal)
        btnSelectColorPcker.addTarget(self, action: #selector(onClickColorPickerAciton(_:)), for: .touchUpInside)
        self.viewColorPicker.addSubview(btnSelectColorPcker)
        
        btnSelectColorPckerCancel.frame = CGRect(x: btnSelectColorPcker.frame.origin.x + btnSelectColorPcker.frame.size.width + 5, y: colorPicker.view.frame.origin.x + colorPicker.view.frame.size.height, width: ((self.viewColorPicker.frame.size.width - 25 ) / 2), height: 50)
        btnSelectColorPckerCancel.tag = 20
        btnSelectColorPckerCancel.backgroundColor = #colorLiteral(red: 0, green: 0.7320501804, blue: 0.365727663, alpha: 1)
        btnSelectColorPckerCancel.layer.cornerRadius = 5
        btnSelectColorPckerCancel.layer.masksToBounds = true
        btnSelectColorPckerCancel.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        btnSelectColorPckerCancel.setTitle("Cancel".localized, for: .normal)
        btnSelectColorPckerCancel.setTitleColor(.white, for: .normal)
        btnSelectColorPckerCancel.addTarget(self, action: #selector(onClickColorPickerAciton(_:)), for: .touchUpInside)
        self.viewColorPicker.addSubview(btnSelectColorPckerCancel)
        self.view.addSubview(self.viewColorPicker)
    }

    @IBAction func onClickSave(_ sender: Any) {
        dictArray.removeAll()
        for i in 0..<attributeParamModel.count{
            let data = attributeParamModel[i]
            dictArray.append(viewModel.getAttributeParam(productAttributeValueId: data.productAttributeValueId ?? 0, productAttributeMappingId: data.productAttributeMappingId ?? 0, name: data.name ?? "", color: data.color ?? "", priceAdjustment: data.priceAdjustment ?? 0, isPreSelected: data.isPreSelected ?? false, disable: data.disable ?? false))
        }
        
        for j in 0..<sizeAttributeModel.count{
            let data = sizeAttributeModel[j]
            dictArray.append(viewModel.getAttributeParam(productAttributeValueId: data.productAttributeValueId ?? 0, productAttributeMappingId: data.productAttributeMappingId ?? 0, name: data.name ?? "", color: data.color ?? "", priceAdjustment: data.priceAdjustment ?? 0, isPreSelected: data.isPreSelected ?? false, disable: data.disable ?? false))
        }
        
        
        self.apiManagerClient = APIManagerClient.sharedInstance
            if AppUtility.isConnectedToNetwork() {
                AppUtility.showLoading(self.view)
                
                
                Alamofire.request(URL(string: URLConstant.updateProductAttributes())!, method: .post, parameters: ["ProductAttributeValues":dictArray], encoding: JSONEncoding.default, headers: aPIManager.getHeaderDic()).responseJSON {
                    response in
                    switch response.result {
                    
                    case .success(let data):
                        let json = JSON(data)
                        println_debug(json)
                        if json["StatusCode"].int == 200 {
                            DispatchQueue.main.async {
                                AppUtility.hideLoading(self.view)
                                self.alertVC = SAlertController()
                                self.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "More Details Saved".localized, onController: self)
                                let YesAction = SAlertAction()
                                YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                    self.saveDelegate?.changeSaveButtonState(callApi: true, sizeAttributeModel: self.sizeAttributeModel)
                                    self.navigationController?.popViewController(animated: true)
                                })
                                self.alertVC?.addAction(action: [YesAction])
                            }
                        }  else {
                            
                            DispatchQueue.main.async {
                                AppUtility.hideLoading(self.view)
                                self.alertVC = SAlertController()
                                self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "Failed to upload Attribute. Please try later!".localized, onController: self)
                                let YesAction = SAlertAction()
                                YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                })
                                self.alertVC?.addAction(action: [YesAction])
                            }
                           
                           // errorBlock(self.displayErrorMessage(json: json))
                        }
                    case .failure(let error):
                        print("\(error.localizedDescription)")
                        DispatchQueue.main.async {
                            AppUtility.hideLoading(self.view)
                        self.alertVC = SAlertController()
                        self.alertVC?.ShowSAlert(title: "Error".localized, withDescription: "\(error.localizedDescription)", onController: self)
                        let YesAction = SAlertAction()
                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                       
                        })
                            self.alertVC?.addAction(action: [YesAction])}
                        //errorBlock("\(error.localizedDescription)")
                    }
                }
            }
    }
    
    
    @IBAction func onClickCancel(_ sender: Any) {
        self.saveDelegate?.changeSaveButtonState(callApi: true, sizeAttributeModel: sizeAttributeModel)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func backToMain() {
        self.saveDelegate?.changeSaveButtonState(callApi: true, sizeAttributeModel: sizeAttributeModel)
        self.navigationController?.popViewController(animated: true)
    }

    
    func setNavigationBar() {

        self.navigationItem.setHidesBackButton(true, animated:false)

        self.title = "More Details".localized
      
        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: -10, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: -15, y: 0, width: 40, height: 40))

        if let imgBackArrow = UIImage(named: "backarrow") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)

        let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMain))
        view.addGestureRecognizer(backTap)

        let leftBarButtonItem = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    

}

extension AttributeViewController: UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productDetailsModel?.data?.productAttributes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if let attributeArray = productDetailsModel?.data?.productAttributes{
                if let type = attributeArray[indexPath.row].attributeControlType {
                    switch type{
                    case 1:
                        guard  let cell = attributeTable.dequeueReusableCell(withIdentifier: "DropDownAttribute") as? DropDownAttribute else {
                            return UITableViewCell()
                        }
                        cell.selectionStyle = .none
                        cell.dropDownTestField.text = dropDownName
                        cell.dropDownTestField.tag = indexPath.row
                        cell.delegate = self
                        cell.attributeView.roundedBorderWithShadowNew(borderColor: .black,shadowColor: .white,backgroundColor: .clear)
                        cell.productID = attributeArray[indexPath.row].id ?? 0
                        cell.nameLabel.text = attributeArray[indexPath.row].name
                        cell.dropDownvalue = attributeArray[indexPath.row].values
    
                        return cell
                        
                    case 2:
                        guard  let cell = attributeTable.dequeueReusableCell(withIdentifier: "AttributeRadioBox") as? AttributeRadioBox else {
                            return UITableViewCell()
                        }
                        cell.selectionStyle = .none
                        cell.radioButtonText.text = attributeArray[indexPath.row].name
                        cell.values = attributeArray[indexPath.row].values
                        cell.delegate = self
                        cell.productID = attributeArray[indexPath.row].id ?? 0
                        cell.attributeView.roundedBorderWithShadowNew(borderColor: .black,shadowColor: .white,backgroundColor: .clear)
                        cell.radioButtonCollection.reloadData()
                        return cell
                       
                    case 3:
                        guard  let cell = attributeTable.dequeueReusableCell(withIdentifier: "AttributeCheckBox") as? AttributeCheckBox else {
                            return UITableViewCell()
                        }
                        if let hasVlaue = attributeArray[indexPath.row].values{
                            cell.setValue(values: hasVlaue)
                        }
                      
                        cell.selectionStyle = .none
                        cell.delegate = self
                        cell.productID = attributeArray[indexPath.row].id ?? 0
                        cell.attributeName.text = attributeArray[indexPath.row].name
                        cell.values = attributeArray[indexPath.row].values
                        
                        cell.attributeView.roundedBorderWithShadowNew(borderColor: .black,shadowColor: .white,backgroundColor: .clear)
                        cell.attributeCollectionView.reloadData()
                        
                        return cell
                        
                    case 4:
                        guard  let cell = attributeTable.dequeueReusableCell(withIdentifier: "AttributeTextBox") as? AttributeTextBox else {
                            return UITableViewCell()
                        }
                        cell.selectionStyle = .none
                        cell.delgate = self
                        cell.newDelegate = self
                        sizeChartMainId = attributeArray[indexPath.row].id ?? 0
                        cell.productID = attributeArray[indexPath.row].id ?? 0
                        cell.mainID = attributeArray[indexPath.row].id ?? 0
                        cell.buttonClickedDelegate = self
                        cell.callGuideDelegate = self
                        sizeGuide = attributeArray[indexPath.row].values
                        cell.values = attributeArray[indexPath.row].values
                        if attributeArray[indexPath.row].name ?? "" == "Size By Measurement"{
                            cell.descriptionTextview.isUserInteractionEnabled = false
                            cell.texBoxLabel.text = "Select \(attributeArray[indexPath.row].name ?? "")"
                            cell.arrowButton.isHidden = false
                        }else{
                            cell.descriptionTextview.text = attributeArray[indexPath.row].values?[0].name ?? ""
                            cell.texBoxLabel.text = "\(attributeArray[indexPath.row].name ?? "")"
                            cell.arrowButton.isHidden = true
                        }
                        
                        cell.name = attributeArray[indexPath.row].name ?? ""
                        cell.descriptionTextview.tag = indexPath.row
                        cell.seperatorLabel.backgroundColor = UIColor.init(hex: Constant.kPrimaryThemeGreen)
                        cell.attributeView.roundedBorderWithShadowNew(borderColor: .black,shadowColor: .white,backgroundColor: .clear)
                        
                        return cell
                       
                    case 20:
                        guard  let cell = attributeTable.dequeueReusableCell(withIdentifier: "DatePickerAttribute") as? DatePickerAttribute else {
                            return UITableViewCell()
                        }
                        cell.titleTopLabel.text = attributeArray[indexPath.row].name ?? ""
                        cell.selectionStyle = .none
                        if let hasValue = attributeArray[indexPath.row].values{
                            cell.value = hasValue
                        }
                        cell.datePickerTextField.text = attributeArray[indexPath.row].values?[0].name ?? ""
                        cell.productID = attributeArray[indexPath.row].id ?? 0
                        cell.mainID = attributeArray[indexPath.row].id ?? 0
                        cell.delegate = self
                        cell.datePickerTextField.tag = indexPath.row
                        cell.attributeView.roundedBorderWithShadowNew(borderColor: .black,shadowColor: .white,backgroundColor: .clear)
                        cell.bottomLabel.backgroundColor = UIColor.init(hex: Constant.kPrimaryThemeGreen)
                        return cell
                        
                    case 40:
                        guard  let cell = attributeTable.dequeueReusableCell(withIdentifier: "ColorPickerAttribute") as? ColorPickerAttribute else {
                            return UITableViewCell()
                        }
                        cell.selectionStyle = .none
                        cell.delegate = self
                        cell.newDelegate = self
                        cell.colorArray = colorList
                        cell.name = attributeArray[indexPath.row].name ?? ""
                        cell.mainID = attributeArray[indexPath.row].id ?? 0
                        cell.values = attributeArray[indexPath.row].values
                        cell.productID = attributeArray[indexPath.row].productAttributeID ?? 0
                        cell.attributeView.roundedBorderWithShadowNew(borderColor: .black,shadowColor: .white,backgroundColor: .clear)
                        cell.colorCollection.reloadData()
                        return cell
                       
                    default:
                        break
                    }
                }
            }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if let attributeArray = productDetailsModel?.data?.productAttributes{
            switch attributeArray[indexPath.row].attributeControlType {
            
            case 2,3:
                return 153.0
                
            case 4:
                if textViewHeight == 0.0 || textViewHeight == 33.0{
                    return 96.0
                }else{
                    return 80.0 + textViewHeight
                }
            case 40:
                if colorList.count>0{
                    return 160.0
                }else{
                    return 105.0
                }
            case 1,20:
                return 96.0
                
            default:
                break
            }
        }
        
        return 0.0
    }
        
}


extension AttributeViewController: HeightForTextView {
    func heightOfTextView(height: CGFloat){
        textViewHeight = height
        self.attributeTable.beginUpdates()
        self.attributeTable.endUpdates()
        
    }
}

extension AttributeViewController: AttributeCheckBoxData{
    
    func attributeCheckBoxData(valueId: Int,attributeId: Int,name: String,colorSquare: String,priceAdjustment: Int,isPreSelect: Bool,disable: Bool,isComingFrom:String){
        
        if isComingFrom == "Color" {
            //we need to check with attribute id
            if let idx = attributeParamModel.firstIndex(where: { $0.productAttributeMappingId == attributeId && $0.color?.lowercased() == colorSquare.lowercased()}) {
                attributeParamModel.remove(at: idx)
               // attributeParamModel.append(AttributeModel(productAttributeValueId: valueId, productAttributeMappingId: attributeId, name: name, color: colorSquare, priceAdjustment: priceAdjustment, isPreSelected: isPreSelect, disable: disable))
            }else{
                attributeParamModel.append(AttributeModel(productAttributeValueId: valueId, productAttributeMappingId: attributeId, name: name, color: colorSquare, priceAdjustment: priceAdjustment, isPreSelected: isPreSelect, disable: disable))
            }
        }
        
        
        //this will work for both the case edit and while adding new product
        if isComingFrom == "radio" || isComingFrom == "checkBox"{
            if let idx = attributeParamModel.firstIndex(where: { $0.productAttributeValueId == valueId }) {
                attributeParamModel.remove(at: idx)
                attributeParamModel.append(AttributeModel(productAttributeValueId: valueId, productAttributeMappingId: attributeId, name: name, color: colorSquare, priceAdjustment: priceAdjustment, isPreSelected: isPreSelect, disable: disable))
            }else{
                attributeParamModel.append(AttributeModel(productAttributeValueId: valueId, productAttributeMappingId: attributeId, name: name, color: colorSquare, priceAdjustment: priceAdjustment, isPreSelected: isPreSelect, disable: disable))
            }
        }
        
        if isComingFrom == "Size By Measurement"{

        }else  if isComingFrom == "textBox" || isComingFrom == "DatePicker"{
            if let idx = attributeParamModel.firstIndex(where: { $0.productAttributeMappingId == attributeId }) {
                attributeParamModel.remove(at: idx)
                //appending because its a new value updating textfield
                attributeParamModel.append(AttributeModel(productAttributeValueId: valueId, productAttributeMappingId: attributeId, name: name, color: colorSquare, priceAdjustment: priceAdjustment, isPreSelected: isPreSelect, disable: disable))
            }else{
                attributeParamModel.append(AttributeModel(productAttributeValueId: valueId, productAttributeMappingId: attributeId, name: name, color: colorSquare, priceAdjustment: priceAdjustment, isPreSelected: isPreSelect, disable: disable))
            }
        }else{
//            if let idx = attributeParamModel.firstIndex(where: { $0.productAttributeValueId == valueId }) {
//                attributeParamModel.remove(at: idx)
//            }else{
//                attributeParamModel.append(AttributeModel(productAttributeValueId: valueId, productAttributeMappingId: attributeId, name: name, color: colorSquare, priceAdjustment: priceAdjustment, isPreSelected: isPreSelect, disable: disable))
//            }
        }
    }
    
}

extension AttributeViewController: CallDropDown {
  
    func showDropDown(field: UITextField,dropDownvalue: [Tushar_Values],productID : Int){
        self.dropDownvalue = dropDownvalue
        self.dropDownProductId = productID
        tag = field.tag
        let indexPath = IndexPath(row: field.tag, section: 0)
        guard let cell = self.attributeTable.cellForRow(at: indexPath) as? DropDownAttribute else{
            return
        }
        dropDown.anchorView = cell.dropDownTestField // UIView or UIBarButtonItem
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.dataSource = dropDownvalue.compactMap({$0.name ?? ""})
        if dropDown.isHidden{
            dropDown.show()
        }else{
            dropDown.hide()
        }
        
    }
}



extension AttributeViewController: EFColorSelectionViewControllerDelegate,EditProductAttributesColorCellDelegate {
    func applySelectedColor(dic: Value, isDisable: Bool) {
//        self.updatColorAPI(dic: dic, disable: isDisable, handler: {isSuccess in
//            if isSuccess {
//                self.getProductDetialsByID(productID: String(self.model?.data?.id ?? 0), reloadFor: "Color")
//            }
//        })
    }
    
    func colorViewController(_ colorViewCntroller: EFColorSelectionViewController, didChangeColor color: UIColor) {
        self.selectedcolor = color
        
        
    }


}

extension AttributeViewController: CallColorPicker {
    func callColorPicker(product: Int){
        
        if product == 0000000000{
            
            print("Coming soon")
//            DispatchQueue.main.async {
//                self.alertVC = SAlertController()
//                self.alertVC?.ShowSAlert(title: "OSK Vendor".localized, withDescription: "This feature is coming soon", onController: self)
//                let YesAction = SAlertAction()
//                YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
//                })
//                self.alertVC?.addAction(action: [YesAction])
//            }
            
            DispatchQueue.main.async {
                var vcObj: AttributesSizeGuideVC?
                vcObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AttributesSizeGuideVC") as? AttributesSizeGuideVC
                if let validObj = vcObj {
                    validObj.mainID = self.sizeChartMainId ?? 0
                    validObj.values = self.sizeGuide
                    validObj.attributeParamModel = self.sizeAttributeModel
                    validObj.newDelegate = self
                    validObj.sizeGuideImage = self.productDetailsModel?.data?.sizeGuideImagePath ?? ""
                    self.navigationController?.pushViewController(validObj, animated: true)
                }
            }
        }else{
            self.colorProductID = product
            DispatchQueue.main.async {
                self.callColorPickerNew()
            }
        }
        
    }
}


extension AttributeViewController :EditProductAttributsDelegate {
    func reloadAPIForGetNewColor(){
        
    }
}


extension AttributeViewController :GetTheSizeDetails {
    func getSizeDetail(obj: [AttributeModel]){
        sizeAttributeModel = obj
    }
}



